/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs;

import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ComentrioFirmaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ComentarioFirmaRowMapper implements RowMapper<ComentrioFirmaBase> {

    private ComentrioFirmaBase comentrioFirmaBase;

    @Override
    public ComentrioFirmaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        comentrioFirmaBase = new ComentrioFirmaBase();
        comentrioFirmaBase.setIdFirma(rs.getInt("FIIDFIRMA"));
        comentrioFirmaBase.setComentario(rs.getString("FCCOMENTARIO"));
        comentrioFirmaBase.setFecha(rs.getString("FDFECHA"));
        comentrioFirmaBase.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        return comentrioFirmaBase;
    }
}
