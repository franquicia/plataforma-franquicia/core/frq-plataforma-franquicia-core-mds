/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Acceso de seguridad de claves asimétricas", value = "AccesoAsimetrico")
public class AccesoAsimetricoDTO {

    @JsonProperty(value = "idAcceso")
    @ApiModelProperty(notes = "Identificador del Acceso", example = "1b634712-3258-4a8e-b8a8-f8e19f11d1e2", position = -1)
    private String idAcceso;

    @JsonProperty(value = "accesoPublico")
    @ApiModelProperty(notes = "Llave asimétrica pública", example = "<Cadena con la llave pública>")
    private String publica;

    @JsonProperty(value = "accesoPrivado")
    @ApiModelProperty(notes = "Llave asimétrica privada", example = "<Cadena con la llave privada>")
    private String privada;

    public AccesoAsimetricoDTO(String idAcceso, String publica, String privada) {
        this.idAcceso = idAcceso;
        this.publica = publica;
        this.privada = privada;
    }

    public AccesoAsimetricoDTO() {
    }

    public String getIdAcceso() {
        return idAcceso;
    }

    public void setIdAcceso(String idAcceso) {
        this.idAcceso = idAcceso;
    }

    public String getPublica() {
        return publica;
    }

    public void setPublica(String publica) {
        this.publica = publica;
    }

    public String getPrivada() {
        return privada;
    }

    public void setPrivada(String privada) {
        this.privada = privada;
    }

}
