/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g013;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g013.DashboardCertificacionG013DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g013.DashboardCertificacionG013DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g013")
public class ServiceDashboardCertificacionG013 {

    @Autowired
    private DashboardCertificacionG013DAOImpl dashboardCertificacionG013DAOImpl;

    @RequestMapping(value = "secure/grafico/get/puntos/certificados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG013DTO> getDatosPuntosCertificadosG013() throws CustomException {
        return dashboardCertificacionG013DAOImpl.selectRowsDashboardCertificacionG013();
    }

    @RequestMapping(value = "secure/grafico/get/puntos/certificados/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG013DTO> getDatoPuntosCertificadosG013(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @RequestParam(value = "idGerente", required = true) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario) throws CustomException {
        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFin == null || fechaFin.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFin"));
        }
        if (idGerente == null || idGerente.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("idGerente"));
        }

        return dashboardCertificacionG013DAOImpl.selectRowDashboardCertificacionG013(fechaInicial, fechaFin, idGerente, idNegocio, idUsuario);
    }
}
