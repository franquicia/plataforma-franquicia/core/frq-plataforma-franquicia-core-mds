/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.estatus.usuario.rest;

import com.gs.baz.estatus.usuario.dao.EstatusUsuarioDAOImpl;
import com.gs.baz.estatus.usuario.dto.EstatusUsuarioDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/estatus/usuario")
public class ServiceEstatusUsuario {

    @Autowired
    private EstatusUsuarioDAOImpl estatusUsuarioDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EstatusUsuarioDTO> getEstatusUsuario() throws CustomException {
        return estatusUsuarioDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_estatus_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstatusUsuarioDTO getTodosEstatusUsuario(@PathVariable("id_estatus_usuario") Long idEstatusUsuario) throws CustomException {
        return estatusUsuarioDAOImpl.selectRow(idEstatusUsuario);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstatusUsuarioDTO postEstatusUsuario(@RequestBody EstatusUsuarioDTO geo) throws CustomException {

        return estatusUsuarioDAOImpl.insertRow(geo);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstatusUsuarioDTO putEstatusUsuario(@RequestBody EstatusUsuarioDTO estUsu) throws CustomException {
        if (estUsu.getIdEstatusUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_estatus_usuario"));
        }
        return estatusUsuarioDAOImpl.updateRow(estUsu);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstatusUsuarioDTO deleteEstatusUsuario(@RequestBody EstatusUsuarioDTO estUsu) throws CustomException {
        if (estUsu.getIdEstatusUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_estatus_usuario"));
        }
        return estatusUsuarioDAOImpl.deleteRow(estUsu);
    }
}
