/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del niveles geograficos", value = "NivelesGeograficos")
public class NivelGeografico {

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Centro de costos", example = "923191")
    private Integer centroCostos;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del centro de costos", example = "TERRITORIAL NORTE")
    private String descripcion;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "Indice al que pertenece el Ceontro de Costos", example = "1")
    private Integer indice;

    public Integer getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(Integer centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    @Override
    public String toString() {
        return "NivelesGeograficos{" + "centroCostos=" + centroCostos + ", descripcion=" + descripcion + ", indice=" + indice + '}';
    }

}
