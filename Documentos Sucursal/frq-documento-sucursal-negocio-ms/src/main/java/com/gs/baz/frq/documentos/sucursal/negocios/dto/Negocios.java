/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de negocio", value = "negocios")
public class Negocios {

    @JsonProperty(value = "negocios")
    @ApiModelProperty(notes = "negocios")
    private List<Negocio> negocios;

    public Negocios(List<Negocio> negocios) {
        this.negocios = negocios;
    }

    public List<Negocio> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<Negocio> negocios) {
        this.negocios = negocios;
    }

    @Override
    public String toString() {
        return "Negocios{" + "negocios=" + negocios + '}';
    }

}
