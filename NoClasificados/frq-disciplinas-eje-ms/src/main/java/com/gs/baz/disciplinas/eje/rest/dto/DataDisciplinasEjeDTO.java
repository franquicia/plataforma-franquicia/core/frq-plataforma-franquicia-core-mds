/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.eje.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.disciplinas.eje.dto.DisciplinaEjeDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import java.util.List;

/**
 *
 * @author B73601
 */
public class DataDisciplinasEjeDTO {

    @JsonProperty(value = "metadata")
    private Metadata metadata;

    @JsonProperty(value = "disciplinas_eje")
    private List<DisciplinaEjeDTO> disciplinasEjeDTO;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<DisciplinaEjeDTO> getDisciplinasEjeDTO() {
        return disciplinasEjeDTO;
    }

    public void setDisciplinaEjeDTO(List<DisciplinaEjeDTO> disciplinasEjeDTO) {
        this.disciplinasEjeDTO = disciplinasEjeDTO;
    }

    @Override
    public String toString() {
        return "DataModeloDTO{" + "metadata=" + metadata + ", disciplinasEjeDTO=" + disciplinasEjeDTO + '}';
    }

}
