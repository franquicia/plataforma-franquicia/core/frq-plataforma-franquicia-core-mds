/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ejes.modelo.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.ejes.modelo.dto.EjeModeloDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class DataEjesModelo {

    @JsonProperty(value = "metadata")
    private Metadata metadata;

    @JsonProperty(value = "ejes_modelo")
    private List<EjeModeloDTO> ejesModelo;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<EjeModeloDTO> getEjesModelo() {
        return ejesModelo;
    }

    public void setEjesModelo(List<EjeModeloDTO> ejesModelo) {
        this.ejesModelo = ejesModelo;
    }

    @Override
    public String toString() {
        return "DataEjesModelo{" + "metadata=" + metadata + ", ejesModelo=" + ejesModelo + '}';
    }

}
