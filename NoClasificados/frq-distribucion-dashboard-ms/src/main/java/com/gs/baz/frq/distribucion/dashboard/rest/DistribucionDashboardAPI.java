/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.rest;

import com.gs.baz.frq.distribucion.dashboard.bi.DistribucionDashboardBI;
import com.gs.baz.frq.distribucion.dashboard.dto.CargaVisitasRequestDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaRequestDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaResponseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaCargaVisitasDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaDepuraVisitasDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaDistriDashCalifDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaMergeDetallePreguntaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasCapaCeroDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasCapaIntermediaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasDistCecoBaseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasHistoricoDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasImperdonableIncumplimientoDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasSieteCapaCeroDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasCapaCeroResponseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasCapaIntermediaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasSieteDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta401;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "distribucion-dashboard", value = "distribucion-dashboard", description = "API de distribución de información de Core a Capa Cero.")
@RestController
@RequestMapping("/api-local/distribucion-dashboard/v1")
public class DistribucionDashboardAPI {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    DistribucionDashboardBI distribucionDashboardBI;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    @ApiOperation(value = "Distribución Visitas Capa Cero", notes = "Realiza la distribución de Core a Capa Cero de las Visitas.", nickname = "postVisitasCapaCero")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/capa-cero/visitas/{idEstatus}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VisitasCapaCeroResponseDTO postVisitasCapaCero(@ApiParam(name = "idEstatus", value = "Identificador del estatus de las visitas", example = "1", required = true) @PathVariable("idEstatus") Integer idEstatus) throws CustomException {
        VisitasCapaCeroResponseDTO respuestaVisitas = new VisitasCapaCeroResponseDTO();

        respuestaVisitas = distribucionDashboardBI.visitasCapaCero(idEstatus);

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución Visitas Capa Cero Siete 'S'", notes = "Realiza la distribución de Core a Capa Cero de las Visitas DE 7 'S'.", nickname = "getVisitasCapaCero")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/capa-cero/sieteS/visitas/{idEstatus}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VisitasSieteDTO postVisitasSieteSCapaCero(@ApiParam(name = "idEstatus", value = "Identificador del estatus de las visitas", example = "1", required = true) @PathVariable("idEstatus") Integer idEstatus) throws CustomException {
        VisitasSieteDTO respuestaVisitas = new VisitasSieteDTO();

        respuestaVisitas = distribucionDashboardBI.visitasCapaCeroSiete(idEstatus);

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución de Respuestas de histórico", notes = "Realiza la distribución de las Respuestas de histórico.", nickname = "getRespuestasCapaCero")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/historicos/respuestas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestasHistoricoDTO getHistoricoRespuestas() throws CustomException {
        RespuestasHistoricoDTO respuestaVisitas = new RespuestasHistoricoDTO();

        respuestaVisitas = distribucionDashboardBI.respuestasHistorico();

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución Respuestas Capa Cero", notes = "Realiza la distribución de Respuestas de Core a Capa Cero.", nickname = "getRespuestasCapaCero")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/capa-cero/respuestas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestasCapaCeroDTO getRespuestasCapaCero() throws CustomException {
        RespuestasCapaCeroDTO respuestaVisitas = new RespuestasCapaCeroDTO();

        respuestaVisitas = distribucionDashboardBI.respuestasCapaCero();

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución Respuestas Capa Cero para 7 'S'", notes = "Realiza la distribución de Core a Capa Cero de las Respuestas para 7'S'.", nickname = "GetRespuestasCapaCeroSieteS")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/capa-cero/siete/respuestas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestasSieteCapaCeroDTO getRespuestasSieteSCapaCero() throws CustomException {
        RespuestasSieteCapaCeroDTO respuestaVisitas = new RespuestasSieteCapaCeroDTO();

        respuestaVisitas = distribucionDashboardBI.respuestasSieteCapaCero();

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución Visitas Capa Intermedia", notes = "Realiza la distribución a Capa intermedia de las Visitas.", nickname = "getVisitasCapaIntermedia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/capa-intermedia/visitas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VisitasCapaIntermediaDTO getVisitasCapaIntermedia() throws CustomException {
        VisitasCapaIntermediaDTO respuestaVisitas = new VisitasCapaIntermediaDTO();

        respuestaVisitas = distribucionDashboardBI.visitasCapaIntermedia();

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución Respuestas Capa Intermedia", notes = "Realiza la distribución de las respuestas de capa intermedia.", nickname = "getRespuestasCapaIntermedia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/capa-intermedia/respuestas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestasCapaIntermediaDTO getRespuestasCapaIntermedia() throws CustomException {
        RespuestasCapaIntermediaDTO respuestaVisitas = new RespuestasCapaIntermediaDTO();

        respuestaVisitas = distribucionDashboardBI.respuestasCapaIntermedia();

        return respuestaVisitas;
    }

    @ApiOperation(value = "Obtiene el estatus de las visitas.", notes = ".", nickname = "postEstatusVisita")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "x-idAcceso", value = "Identificador del Acceso", example = "987cdd90-fd7f-499c-b09d-f9ffa2599696", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class)
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 401, message = "Recurso no autorizado.", response = Respuesta401.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/estatus-visita", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EstatusVisitaResponseDTO postEstatusVisita(@ApiParam(name = "EstatusVisitaEntrada", value = "Obtiene información de estatus visita.", required = true) @RequestBody EstatusVisitaRequestDTO estatusVisitaRequestDTO) throws CustomException {
        EstatusVisitaResponseDTO respuestaVisitas = new EstatusVisitaResponseDTO();
        List<EstatusVisitaDTO> respuesta = new ArrayList();

        respuesta = distribucionDashboardBI.estatusVisitaRespuesta(estatusVisitaRequestDTO);

        respuestaVisitas.setEstatusVisita(respuesta);

        return respuestaVisitas;
    }

    @ApiOperation(value = "Distribución de Ceco Base", notes = "Realiza la distribución de Ceco Base.", nickname = "getDistCecoBase")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/ceco-base", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestasDistCecoBaseDTO getCecoBase() throws CustomException {
        RespuestasDistCecoBaseDTO respuestaCecoBase = new RespuestasDistCecoBaseDTO();

        respuestaCecoBase = distribucionDashboardBI.cecoBaseDistribucion();

        return respuestaCecoBase;
    }

    @ApiOperation(value = "Paso uno para carga control visitas.", notes = "Realiza el paso uno para la carga de visitas.", nickname = "postCargaVisitas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/carga-visitas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaCargaVisitasDTO postCargaVisitas(@ApiParam(name = "CargaVisitasEntrada", value = "Hace la varga de control visita.", required = true) @RequestBody CargaVisitasRequestDTO cargaVisitasRequestDTO) throws CustomException {
        RespuestaCargaVisitasDTO respuestaCargaVisitasDTO = new RespuestaCargaVisitasDTO();

        respuestaCargaVisitasDTO = distribucionDashboardBI.cargaVisitas(cargaVisitasRequestDTO);
        //respuestaCargaVisitasDTO.setRespuestaEjecucion(visit);

        return respuestaCargaVisitasDTO;
    }

    @ApiOperation(value = "Paso dos para depurar control visitas.", notes = "Realiza el paso dos para la depuración de visitas.", nickname = "gettDepuraVisitas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/depura-visitas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaDepuraVisitasDTO getDepuraVisitas() throws CustomException {
        RespuestaDepuraVisitasDTO respuestaDepuraVisitasDTO = new RespuestaDepuraVisitasDTO();

        respuestaDepuraVisitasDTO = distribucionDashboardBI.depuraVisitas();
        //  respuestaDepuraVisitasDTO.setRespuestaEjecucion(visit);

        return respuestaDepuraVisitasDTO;
    }

    @ApiOperation(value = "Paso dos para carga Dashboard y calificación por Disciplinas.", notes = "Paso dos para carga diaria de Dashboard y calificación por Disciplinas.", nickname = "postCargaVisitas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/calificaciones-protocolos/carga-diaria", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaDistriDashCalifDTO getDashboardCalificacion() throws CustomException {
        RespuestaDistriDashCalifDTO respuestaDistriDashCalifDTO = new RespuestaDistriDashCalifDTO();

        respuestaDistriDashCalifDTO = distribucionDashboardBI.distribuyeDashCalif();

        return respuestaDistriDashCalifDTO;
    }

    @ApiOperation(value = "Paso tres distribución respuestas, imperdonables e incumpliento.", notes = "Realiza el paso tres que realiza la distribución de respuestas, imperdonables e incumplimiento.", nickname = "getImperdonableIncumplimiento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/imperdonables-incumplimientos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestasImperdonableIncumplimientoDTO getImperdonableIncumplimiento() throws CustomException {
        RespuestasImperdonableIncumplimientoDTO respuestasImperdonableIncumplimientoDTO = new RespuestasImperdonableIncumplimientoDTO();

        respuestasImperdonableIncumplimientoDTO = distribucionDashboardBI.distribuyeRespImpInc();

        return respuestasImperdonableIncumplimientoDTO;
    }

    @ApiOperation(value = "Distribución de detalle pregunta.", notes = "Realiza la distribución de detalle pregunta", nickname = "getDetallePregunta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
    })
    @RequestMapping(value = "/detalle-pregunta", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaMergeDetallePreguntaDTO getDetallePregunta() throws CustomException {
        RespuestaMergeDetallePreguntaDTO respuestaMergeDetallePreguntaDTO = new RespuestaMergeDetallePreguntaDTO();

        respuestaMergeDetallePreguntaDTO = distribucionDashboardBI.distMergeDetallePregunta();

        return respuestaMergeDetallePreguntaDTO;
    }

}
