/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.pdf.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.validador.Base64;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author cescobarh
 */
public class ParametrosArchivo {

    @JsonProperty(value = "nombre")
    @NotBlank
    @ApiModelProperty(notes = "nombre del archivo", example = "documento.docx", required = true)
    private String nombre;

    @JsonProperty(value = "archivo")
    @ApiModelProperty(notes = "archivo base 64", example = "DIwMTExMDAzMDYxNjc2OS9fLnhsc3g=", required = true)
    @NotBlank
    @Base64
    private String archivo;

    @JsonProperty(value = "parametros")
    @ApiModelProperty(notes = "Parametros para convertir archivo")
    private Map<String, String> parametros;

    @JsonProperty(value = "tipoContenido")
    @ApiModelProperty(notes = "Tipo contenido", example = "application/pdf", required = true)
    @NotBlank
    private String tipoContenido;

    public ParametrosArchivo() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public Map<String, String> getParametros() {
        return parametros;
    }

    public void setParametros(Map<String, String> parametros) {
        this.parametros = parametros;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    @Override
    public String toString() {
        return "ParametrosArchivo{" + "nombre=" + nombre + ", archivo=" + archivo + ", parametros=" + parametros + ", tipoContenido=" + tipoContenido + '}';
    }

}
