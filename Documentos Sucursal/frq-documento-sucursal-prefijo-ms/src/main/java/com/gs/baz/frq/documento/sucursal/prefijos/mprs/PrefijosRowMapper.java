/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.mprs;

import com.gs.baz.frq.documento.sucursal.prefijos.dto.Prefijo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class PrefijosRowMapper implements RowMapper<Prefijo> {

    private Prefijo negocio;

    @Override
    public Prefijo mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocio = new Prefijo();
        negocio.setIdNegocioPrefijo(rs.getInt("FIID_PREFNEGO"));
        negocio.setPrefijo(rs.getString("FCDESCRIPCION"));
        negocio.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        return negocio;
    }
}
