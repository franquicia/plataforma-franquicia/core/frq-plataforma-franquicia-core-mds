package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.AsignacionMttoDTO;

/**
 *
 * @author cescobarh
 */
public class AsignacionSupervisionMttoRowMapper implements RowMapper<AsignacionMttoDTO> {

    private AsignacionMttoDTO status;

    @Override
    public AsignacionMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionMttoDTO();

        status.setIdProgramacion(rs.getInt("FIID_HISTORICO"));
        status.setEco(rs.getString("FCID_CECO"));
        status.setNombreCeco(rs.getString("NOMBRESUC"));
        status.setNombreCertificador(rs.getString("NOMBREUSUARIO"));
        status.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        status.setUsuarioAsigna(rs.getInt("FCUSUARIO"));
        status.setIdUsuario(rs.getInt("FIID_USUARIO"));
        status.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
        status.setIdFase(rs.getInt("FIID_FASEACT"));
        status.setStatus(rs.getInt("FCSTATUS"));
        status.setDescripcion(rs.getString("FCOBSERVACION"));
        status.setFechaProgramada(rs.getString("FECHAPROG"));
        status.setFechaProgramacionAntigua(rs.getString("FECHAPROGANTIGUA"));
        //nombre proyecto
        status.setNegocio(rs.getString("FCNOMBRE"));
        status.setFecha(rs.getString("FDFECHA_MOD"));

        return status;
    }
}
