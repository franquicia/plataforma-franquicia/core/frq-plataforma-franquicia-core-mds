package com.gs.baz.frq.disciplina.dao;

import com.gs.baz.frq.disciplina.dao.util.GenericDAO;
import com.gs.baz.frq.disciplina.dto.DisciplinaDTO;
import com.gs.baz.frq.disciplina.mprs.DisciplinaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DisciplinaDAOImpl extends DefaultDAO implements GenericDAO<DisciplinaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectDisciplinaFranquicia;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMDISCIPLIN");
        jdbcInsert.withProcedureName("SPINSDISCIPLINA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMDISCIPLIN");
        jdbcUpdate.withProcedureName("SPACTDISCIPLINA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMDISCIPLIN");
        jdbcDelete.withProcedureName("SPDELDISCIPLINA");

        jdbcSelectDisciplinaFranquicia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDisciplinaFranquicia.withSchemaName(schema);
        jdbcSelectDisciplinaFranquicia.withCatalogName("PAADMDISCIPLIN");
        jdbcSelectDisciplinaFranquicia.withProcedureName("SPGETDISCIPLINA");
        jdbcSelectDisciplinaFranquicia.returningResultSet("PA_CDATOS", new DisciplinaRowMapper());
    }

    @Override
    public DisciplinaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityID);
        Map<String, Object> out = jdbcSelectDisciplinaFranquicia.execute(mapSqlParameterSource);
        List<DisciplinaDTO> data = (List<DisciplinaDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<DisciplinaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", null);
        Map<String, Object> out = jdbcSelectDisciplinaFranquicia.execute(mapSqlParameterSource);
        return (List<DisciplinaDTO>) out.get("PA_CDATOS");
    }

    @Override
    public DisciplinaDTO insertRow(DisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FCDISCIPLINA", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIBANDERA_GEK", entityDTO.getBanderaGek());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  DisciplinaFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  DisciplinaFranquicia"), ex);
        }
    }

    @Override
    public DisciplinaDTO updateRow(DisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FCDISCIPLINA", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIBANDERA_GEK", entityDTO.getBanderaGek());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of DisciplinaFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of DisciplinaFranquicia"), ex);
        }
    }

    @Override
    public DisciplinaDTO deleteRow(DisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of DisciplinaFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of DisciplinaFranquicia"), ex);
        }
    }

}
