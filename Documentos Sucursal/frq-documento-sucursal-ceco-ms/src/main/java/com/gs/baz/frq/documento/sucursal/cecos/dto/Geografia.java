/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la geografia", value = "Geografia")
public class Geografia extends GeografiaBase {

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Identificador del ceco", example = "920100", position = -4)
    private Integer idCeco;

    @JsonProperty(value = "datosFicha")
    @ApiModelProperty(notes = "Dirección del punto de venta", example = "-99.000", position = -1)
    private Ficha datosFicha;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Ficha getDatosFicha() {
        return datosFicha;
    }

    public void setDatosFicha(Ficha datosFicha) {

        if (datosFicha != null && datosFicha.getId() == 0) {
            this.datosFicha = null;
        } else {
            this.datosFicha = datosFicha;
        }

    }

}
