/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.tipos.informacion.mprs;

import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.TipoInformacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class TiposInformacionRowMapper implements RowMapper<TipoInformacion> {

    private TipoInformacion tipoInformacion;

    @Override
    public TipoInformacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        tipoInformacion = new TipoInformacion();
        tipoInformacion.setIdTipoInformacion(rs.getInt("FIIDTIPOINFOR"));
        tipoInformacion.setDescripcion(rs.getString("FCDESCRIPCION"));
        return tipoInformacion;
    }
}
