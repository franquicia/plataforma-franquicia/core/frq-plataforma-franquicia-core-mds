package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Datos de Archivos del ceco", value = "ArchivosCeco")
public class ArchivoCeco {

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Identificador del ceco", example = "1")
    private Integer idCeco;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del Negocio Layout", example = "1")
    private Integer idNegocio;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Identificador del Tipo de Formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "nombreFormato")
    @ApiModelProperty(notes = "Nombre de Formato", example = "RETAIL")
    private String nombreFormato;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del ceco", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Identificador del ceco", example = "1")
    private String nombreDocumento;

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del id de proyecto", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Nombre de proyecto", example = "ELEKTRA")
    private String nombreProyecto;

    @JsonProperty(value = "idArchivo")
    @ApiModelProperty(notes = "Identificador del id archivo", example = "1")
    private Integer idArchivo;

    @JsonProperty(value = "idArchivoPadre")
    @ApiModelProperty(notes = "Identificador del id archivo Padre", example = "1")
    private Integer idArchivoPadre;

    @JsonProperty(value = "descripcionArchivo")
    @ApiModelProperty(notes = "Descripcion  de archivo", example = "PDF BAZ V1")
    private String descripcionArchivo;

    @JsonProperty(value = "nombreArchivo")
    @ApiModelProperty(notes = "Nombre  de archivo", example = "Licencias")
    private String nombreArchivo;

    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Ruta de archivo", example = "Licencias")
    private String ruta;

    @JsonProperty(value = "extension")
    @ApiModelProperty(notes = "extension de archivo", example = "PDF")
    private String extension;

    @JsonProperty(value = "idUsuario")
    @ApiModelProperty(notes = "Identificador del usuario", example = "191312")
    private Integer idUsuario;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "191312")
    private Integer idFolio;

    @JsonProperty(value = "generaVersion")
    @ApiModelProperty(notes = "Identificador del folio", example = "191312")
    private Integer generaVersion;

    @JsonProperty(value = "idVersion")
    @ApiModelProperty(notes = "Identificador del folio", example = "191312")
    private Integer idVersion;

    @JsonProperty(value = "fecha")
    @ApiModelProperty(notes = "Identificador del folio", example = "191312")
    private String fecha;

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getGeneraVersion() {
        return generaVersion;
    }

    public void setGeneraVersion(Integer generaVersion) {
        this.generaVersion = generaVersion;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdArchivoPadre() {
        return idArchivoPadre;
    }

    public void setIdArchivoPadre(Integer idArchivoPadre) {
        this.idArchivoPadre = idArchivoPadre;
    }

    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

}
