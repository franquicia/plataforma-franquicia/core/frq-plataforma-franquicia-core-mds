/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosArchivoVirtual {

    private transient Integer idArchivo;

    @JsonProperty(value = "idArchivoPadre")
    @ApiModelProperty(notes = "Identificador del archivo padre", example = "0", required = true)
    private Integer idArchivoPadre;

    @JsonProperty(value = "uid")
    @ApiModelProperty(notes = "Ruta en la que se guardó el archivo", example = "/franquicia/modelo-negocio/", required = true)
    private Long uid;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "descripcion del archivo", example = "archivo principal foda", required = true)
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del archivo", example = "foda", required = true)
    private String nombre;

    @JsonProperty(value = "peso")
    @ApiModelProperty(notes = "Peso del archivo", example = "1024", required = true)
    private Integer peso;

    @JsonProperty(value = "extension")
    @ApiModelProperty(notes = "extension del archivo", example = ".pdf", required = true)
    private String extension;

    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Tipo del archivo", example = "1", required = true)
    private Integer tipo;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "indicedel archivo", example = "1", required = true)
    private Integer indice;

    @JsonProperty(value = "tipoContenido")
    @ApiModelProperty(notes = "Tipo de contenido del archivo", example = ".pdf", required = true)
    private String tipoContenido;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1", required = true)
    private Integer idFolio;

    @JsonProperty(value = "usuarioCreador")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1", required = true)
    private Integer usuario;

    @JsonProperty(value = "idVersion")
    @ApiModelProperty(notes = "Identificador de la version ", example = "1", required = true)
    private Integer idVersion;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio  ", example = "1", required = true)
    private Integer idNegocio;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdArchivoPadre() {
        return idArchivoPadre;
    }

    public void setIdArchivoPadre(Integer idArchivoPadre) {
        this.idArchivoPadre = idArchivoPadre;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getIndice() {
        return indice;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    @Override
    public String toString() {
        return "ParametrosArchivoVirtual{" + "idArchivo=" + idArchivo + ", idFolio=" + idArchivoPadre + ", uid=" + uid + ", descripcion=" + descripcion + ", nombre=" + nombre + ", peso=" + peso + ", extension=" + extension + ", tipo=" + tipo + ", indice=" + indice + ", tipoContenido=" + tipoContenido + '}';
    }

}
