/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de puntos de venta pendientes por gerente", value = "PuntosVentaPendientes")
public class PuntosVentaPendientes {

    @JsonProperty(value = "PuntosVentaPendientes")
    @ApiModelProperty(notes = "PuntosVentaPendientes")
    private List<PuntoVentaPendiente> PuntoVentaPendiente;

    public List<PuntoVentaPendiente> getPuntoVentaPendiente() {
        return PuntoVentaPendiente;
    }

    public void setPuntoVentaPendiente(List<PuntoVentaPendiente> PuntoVentaPendiente) {
        this.PuntoVentaPendiente = PuntoVentaPendiente;
    }

    @Override
    public String toString() {
        return "PuntosVentaPendientes{" + "PuntoVentaPendiente=" + PuntoVentaPendiente + '}';
    }

}
