/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 *
 * @author carlos
 */
public class ParametrosComentarios {

    private transient Integer idComentario;

    private transient Integer idFolio;

    private transient Integer numeroEmpleado;

    @JsonProperty(value = "calificacion")
    @ApiModelProperty(notes = "Calificacion del documento", example = "1")
    @Max(5)
    @Min(0)
    private Integer calificacion;

    @JsonProperty(value = "comentario")
    @ApiModelProperty(notes = "Comentario del documento", example = "1")
    @Size(max = 200)
    private String comentario;

    public Integer getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(Integer idComentario) {
        this.idComentario = idComentario;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public String toString() {
        return "ParametrosComentarios{" + "idComentario=" + idComentario + ", idFolio=" + idFolio + ", numeroEmpleado=" + numeroEmpleado + ", calificacion=" + calificacion + ", comentario=" + comentario + '}';
    }

}
