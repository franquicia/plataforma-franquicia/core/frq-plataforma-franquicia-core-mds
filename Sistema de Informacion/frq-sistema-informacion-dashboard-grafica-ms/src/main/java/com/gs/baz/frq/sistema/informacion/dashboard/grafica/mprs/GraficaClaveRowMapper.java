/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.Grafica;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class GraficaClaveRowMapper implements RowMapper<Grafica> {

    @Override
    public Grafica mapRow(ResultSet rs, int rowNum) throws SQLException {
        Grafica grafica = new Grafica();
        grafica.setDescripcion(rs.getString("FCNOMBREGRAFICA"));
        grafica.setIdGrafica(rs.getInt("FIIDGRAFICA"));
        grafica.setNotas(rs.getString("FCNOMBRESP"));
        return grafica;
    }
}
