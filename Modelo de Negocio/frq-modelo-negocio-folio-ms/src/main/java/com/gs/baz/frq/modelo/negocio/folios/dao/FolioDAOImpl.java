/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.folios.dto.AltaFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioDocumento;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioArbol;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioEtiqueta;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioFechas;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioAutorizacion;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioTop;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.FirmaCorreo;
import com.gs.baz.frq.modelo.negocio.folios.dto.Folio;
import com.gs.baz.frq.modelo.negocio.folios.dto.FolioBase;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosActualizaEstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosActualizaIndicePasoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosEstadoFolios;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioResumen;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.ModeloNegocio;
import com.gs.baz.frq.modelo.negocio.folios.dto.Ruta;
import com.gs.baz.frq.modelo.negocio.folios.mprs.DocumentoRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.EstadoFolioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FirmasRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FolioDocumentoRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FolioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FolioRutasModeloNegocioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FolioRutasRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FolioUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosArbolRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosArbolUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosEtiquetaRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosEtiquetaUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosRelacionadosUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.FoliosUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.ResumenDocumentoRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.TopDocumentoRowMapper;
import com.gs.baz.frq.modelo.negocio.folios.mprs.UsuarioRowMapper;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class FolioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcUpdateEstado;
    private DefaultJdbcCall jdbcUpdateIndicePaso;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectEstadoFolios;
    private DefaultJdbcCall jdbcSelectResumenDocumento;
    private DefaultJdbcCall jdbcSelectDocumentoFolio;
    private DefaultJdbcCall jdbcSelectUltimoFolio;
    private DefaultJdbcCall jdbcSelectFoliosEtiqueta;
    private DefaultJdbcCall jdbcSelectFoliosNivel;
    private DefaultJdbcCall jdbcSelectFoliosEtiquetaUsuario;
    private DefaultJdbcCall jdbcSelectFoliosNivelUsuario;
    private DefaultJdbcCall jdbcSelectFoliosAutorizarUsuario;
    private DefaultJdbcCall jdbcSelectDocumento;
    private DefaultJdbcCall jdbcSelectFoliosUsuarioRelacionados;
    private DefaultJdbcCall jdbcSelectTopDocumentosCalificados;
    private DefaultJdbcCall jdbcSelectTopDocumentosRecientes;
    private DefaultJdbcCall jdbcSelectTopDocumentosUltimosSemana;
    private DefaultJdbcCall jdbcSelectTopDocumentosUltimoMes;
    private DefaultJdbcCall jdbcSelectTopDocumentosMasVistos;
    private DefaultJdbcCall jdbcSelectRutasCompuesta;
    private DefaultJdbcCall jdbcSelectRutasCompuestaNivel;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelectAllFirmas;
    private DefaultJdbcCall jdbcSelectUsuario;
    private DefaultJdbcCall jdbcDuplicaFolio;
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNFOLIO";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNFOLIO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNFOLIO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNFOLIO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETMNFOLIO");
        jdbcSelect.returningResultSet("PA_CDATOS", new FolioRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETMNFOLIO");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new FoliosRowMapper());

        jdbcUpdateEstado = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdateEstado.withSchemaName(schema);
        jdbcUpdateEstado.withCatalogName(catalogo);
        jdbcUpdateEstado.withProcedureName("SPACTMNFOLIO");

        jdbcUpdateIndicePaso = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdateIndicePaso.withSchemaName(schema);
        jdbcUpdateIndicePaso.withCatalogName(catalogo);
        jdbcUpdateIndicePaso.withProcedureName("SPACTMNFOLIO");

        jdbcSelectResumenDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectResumenDocumento.withSchemaName(schema);
        jdbcSelectResumenDocumento.withCatalogName("PAADMMNDOCUMEN");
        jdbcSelectResumenDocumento.withProcedureName("SPRECUPERADOCTO");
        jdbcSelectResumenDocumento.returningResultSet("PA_CDATOS", new ResumenDocumentoRowMapper());

        jdbcSelectDocumentoFolio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectDocumentoFolio.withSchemaName(schema);
        jdbcSelectDocumentoFolio.withCatalogName("PAADMMNDOCUMEN");
        jdbcSelectDocumentoFolio.withProcedureName("SPGETDOCTXFOLIO");
        jdbcSelectDocumentoFolio.returningResultSet("PA_CDATOS", new DocumentoRowMapper());

        jdbcSelectUltimoFolio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectUltimoFolio.withSchemaName(schema);
        jdbcSelectUltimoFolio.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectUltimoFolio.withProcedureName("SPULTFOLUSUARIO");
        jdbcSelectUltimoFolio.returningResultSet("PA_CDATOS", new FolioUsuarioRowMapper());

        jdbcSelectEstadoFolios = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectEstadoFolios.withSchemaName(schema);
        jdbcSelectEstadoFolios.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectEstadoFolios.withProcedureName("SPGETESTATUSDOCUMENTOS");
        jdbcSelectEstadoFolios.returningResultSet("PA_CDATOS", new EstadoFolioRowMapper());

        jdbcSelectFoliosEtiqueta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosEtiqueta.withSchemaName(schema);
        jdbcSelectFoliosEtiqueta.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectFoliosEtiqueta.withProcedureName("SPGETVISUADOCTO");
        jdbcSelectFoliosEtiqueta.returningResultSet("PA_CDATOS", new FoliosEtiquetaRowMapper());

        jdbcSelectFoliosNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosNivel.withSchemaName(schema);
        jdbcSelectFoliosNivel.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectFoliosNivel.withProcedureName("SPRECUPERADOCTO_MODNEG");
        jdbcSelectFoliosNivel.returningResultSet("PA_CDATOS", new FoliosArbolRowMapper());

        jdbcSelectFoliosEtiquetaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosEtiquetaUsuario.withSchemaName(schema);
        jdbcSelectFoliosEtiquetaUsuario.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectFoliosEtiquetaUsuario.withProcedureName("SPGETVISDOCTOF_UFE");
        jdbcSelectFoliosEtiquetaUsuario.returningResultSet("PA_CDATOS", new FoliosEtiquetaUsuarioRowMapper());

        jdbcSelectFoliosNivelUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosNivelUsuario.withSchemaName(schema);
        jdbcSelectFoliosNivelUsuario.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectFoliosNivelUsuario.withProcedureName("SPRECUPERADOCUMENT");
        jdbcSelectFoliosNivelUsuario.returningResultSet("PA_CDATOS", new FoliosArbolUsuarioRowMapper());

        jdbcSelectFoliosAutorizarUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosAutorizarUsuario.withSchemaName(schema);
        jdbcSelectFoliosAutorizarUsuario.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectFoliosAutorizarUsuario.withProcedureName("SPFOLIOSXAUT");
        jdbcSelectFoliosAutorizarUsuario.returningResultSet("PA_CDATOS", new FoliosUsuarioRowMapper());

        jdbcSelectFoliosUsuarioRelacionados = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosUsuarioRelacionados.withSchemaName(schema);
        jdbcSelectFoliosUsuarioRelacionados.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectFoliosUsuarioRelacionados.withProcedureName("SPSUGIEREDOCTOS");
        jdbcSelectFoliosUsuarioRelacionados.returningResultSet("PA_CDATOS", new FoliosRelacionadosUsuarioRowMapper());

        jdbcSelectDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectDocumento.withSchemaName(schema);
        jdbcSelectDocumento.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectDocumento.withProcedureName("SPINFODOCTO");
        jdbcSelectDocumento.returningResultSet("PA_CDATOS", new FolioDocumentoRowMapper());

        jdbcSelectTopDocumentosCalificados = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTopDocumentosCalificados.withSchemaName(schema);
        jdbcSelectTopDocumentosCalificados.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectTopDocumentosCalificados.withProcedureName("SPTOPCALIFICA");
        jdbcSelectTopDocumentosCalificados.returningResultSet("PA_CDATOS", new TopDocumentoRowMapper());

        jdbcSelectTopDocumentosRecientes = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTopDocumentosRecientes.withSchemaName(schema);
        jdbcSelectTopDocumentosRecientes.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectTopDocumentosRecientes.withProcedureName("SPTOPRECIENTES");
        jdbcSelectTopDocumentosRecientes.returningResultSet("PA_CDATOS", new TopDocumentoRowMapper());

        jdbcSelectTopDocumentosUltimosSemana = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTopDocumentosUltimosSemana.withSchemaName(schema);
        jdbcSelectTopDocumentosUltimosSemana.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectTopDocumentosUltimosSemana.withProcedureName("SPTOPULTIMOSSEMANA");
        jdbcSelectTopDocumentosUltimosSemana.returningResultSet("PA_CDATOS", new TopDocumentoRowMapper());

        jdbcSelectTopDocumentosUltimoMes = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTopDocumentosUltimoMes.withSchemaName(schema);
        jdbcSelectTopDocumentosUltimoMes.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectTopDocumentosUltimoMes.withProcedureName("SPTOPULTIMOSTMES");
        jdbcSelectTopDocumentosUltimoMes.returningResultSet("PA_CDATOS", new TopDocumentoRowMapper());

        jdbcSelectTopDocumentosMasVistos = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTopDocumentosMasVistos.withSchemaName(schema);
        jdbcSelectTopDocumentosMasVistos.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectTopDocumentosMasVistos.withProcedureName("SPTOPVISTOS");
        jdbcSelectTopDocumentosMasVistos.returningResultSet("PA_CDATOS", new TopDocumentoRowMapper());

        jdbcSelectAllFirmas = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllFirmas.withSchemaName(schema);
        jdbcSelectAllFirmas.withCatalogName("PAADMMNFIRMAS");
        jdbcSelectAllFirmas.withProcedureName("SPGETFIRMASXFOL");
        jdbcSelectAllFirmas.returningResultSet("PA_CDATOS", new FirmasRowMapper());

        jdbcSelectUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectUsuario.withSchemaName(schema);
        jdbcSelectUsuario.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectUsuario.withProcedureName("SPGETUSUARIOS");
        jdbcSelectUsuario.returningResultSet("PA_CDATOS", new UsuarioRowMapper());

        jdbcSelectRutasCompuesta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectRutasCompuesta.withSchemaName(schema);
        jdbcSelectRutasCompuesta.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectRutasCompuesta.withProcedureName("SPRECUPERAFOLIORUTAS");
        jdbcSelectRutasCompuesta.returningResultSet("PA_CDATOS", new FolioRutasRowMapper());

        jdbcSelectRutasCompuestaNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectRutasCompuestaNivel.withSchemaName(schema);
        jdbcSelectRutasCompuestaNivel.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectRutasCompuestaNivel.withProcedureName("SPRECUPERAFOLIORUTASNIVELES");
        jdbcSelectRutasCompuestaNivel.returningResultSet("PA_CDATOS", new FolioRutasModeloNegocioRowMapper());

        jdbcDuplicaFolio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDuplicaFolio.withSchemaName(schema);
        jdbcDuplicaFolio.withCatalogName("PAADMDMNGRUPOS");
        jdbcDuplicaFolio.withProcedureName("SPDUPLICAFOLIO");
    }

    public FolioBase selectRow(Integer idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FolioBase> data = (List<FolioBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public BusquedaFolioResumen selectResumenDocumento(Integer idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", idFolio);
        Map<String, Object> out = jdbcSelectResumenDocumento.execute(mapSqlParameterSource);
        List<BusquedaFolioResumen> data = (List<BusquedaFolioResumen>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            for (BusquedaFolioResumen folio : data) {
                try {
                    FolioBase folioBase = this.selectRow(idFolio);
                    folio.setCreador(folioBase.getCreador());
                    folio.setEstado(folioBase.getEstado());
                    folio.setIndicePaso(folioBase.getIndicePaso());
                    folio.setFechaCreacion(folioBase.getFechaCreacion());
                    folio.setFechaModificacion(folioBase.getFechaModificacion());
                    folio.setRutas(this.selectRutasCompuesta(idFolio));
                    folio.setDocumento(this.selectDocumentoFolioRow(idFolio));
                } catch (SQLException ex) {
                    throw new CustomException(ex);
                }
            }
            return data.get(0);
        } else {
            return null;
        }
    }

    private Folio selectFolio(Integer idFolio) throws SQLException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", idFolio);
        Map<String, Object> out = jdbcSelectResumenDocumento.execute(mapSqlParameterSource);
        List<BusquedaFolioResumen> data = (List<BusquedaFolioResumen>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            BusquedaFolioResumen busquedaFolioResumen = data.get(0);
            busquedaFolioResumen.setIdFolio(idFolio);
            busquedaFolioResumen.setRutas(this.selectRutasCompuesta(idFolio));
            busquedaFolioResumen.setDocumento(this.selectDocumentoFolioRow(idFolio));
            return busquedaFolioResumen;
        } else {
            return null;
        }
    }

    public Documento selectDocumentoFolioRow(Integer idFolio) throws SQLException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", null);
        Map<String, Object> out = jdbcSelectDocumentoFolio.execute(mapSqlParameterSource);
        List<Documento> data = (List<Documento>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    private List<Ruta> selectRutasCompuesta(Integer idFolio) throws SQLException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", idFolio);
        Map<String, Object> out = jdbcSelectRutasCompuesta.execute(mapSqlParameterSource);
        List<Ruta> rutas = (List<Ruta>) out.get("PA_CDATOS");
        for (Ruta ruta : rutas) {
            ruta.setModeloNegocio(this.selectRutasCompuestaModelosNegocio(ruta.getIdRuta()));
        }
        return rutas;
    }

    private List<ModeloNegocio> selectRutasCompuestaModelosNegocio(Integer idRuta) throws SQLException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_RUTA", idRuta);
        Map<String, Object> out = jdbcSelectRutasCompuestaNivel.execute(mapSqlParameterSource);
        return (List<ModeloNegocio>) out.get("PA_CDATOS");
    }

    public List<Folio> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Folio>) out.get("PA_CDATOS");
    }

    public AltaFolio insertRow(ParametrosFolio entityDTO) throws DataNotInsertedException {
        try {
            Date fechaActual = new Date();
            DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String fecha = formatoFecha.format(fechaActual);
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIELIMINADO", 0);
            mapSqlParameterSource.addValue("PA_FIIDEDODOCTO", 1);
            mapSqlParameterSource.addValue("PA_FDFECHACREACION", fecha);
            mapSqlParameterSource.addValue("PA_FIIDCREADOR", entityDTO.getNumeroEmpleadoCreador());
            mapSqlParameterSource.addValue("PA_FIINDICEPASO", 1);
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDFOLIO");
                return new AltaFolio(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosFolio entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FIELIMINADO", null);
            mapSqlParameterSource.addValue("PA_FIIDEDODOCTO", null);
            mapSqlParameterSource.addValue("PA_FDFECHACREACION", null);
            mapSqlParameterSource.addValue("PA_FIIDCREADOR", entityDTO.getNumeroEmpleadoCreador());
            mapSqlParameterSource.addValue("PA_FIINDICEPASO", null);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRowEstado(ParametrosActualizaEstadoFolio entityDTO) throws DataNotUpdatedException, Exception {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FIELIMINADO", null);
            mapSqlParameterSource.addValue("PA_FIIDEDODOCTO", entityDTO.getIdEstadoFolio());
            mapSqlParameterSource.addValue("PA_FDFECHACREACION", null);
            mapSqlParameterSource.addValue("PA_FIIDCREADOR", null);
            mapSqlParameterSource.addValue("PA_FIINDICEPASO", null);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRowIndicePaso(ParametrosActualizaIndicePasoFolio entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FIELIMINADO", null);
            mapSqlParameterSource.addValue("PA_FIIDEDODOCTO", null);
            mapSqlParameterSource.addValue("PA_FDFECHACREACION", null);
            mapSqlParameterSource.addValue("PA_FIIDCREADOR", null);
            mapSqlParameterSource.addValue("PA_FIINDICEPASO", entityDTO.getIndicePaso());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public Folio selectObtieneUltimoFolio(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectUltimoFolio.execute(mapSqlParameterSource);
        List<Folio> data = (List<Folio>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<FirmaCorreo> selectAllFirmas(Integer idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDFIRMA", null);
        Map<String, Object> out = jdbcSelectAllFirmas.execute(mapSqlParameterSource);
        return (List<FirmaCorreo>) out.get("PA_CDATOS");
    }

    public Empleado selectUsuario(String usuario) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_USUARIO", usuario);
        Map<String, Object> out = jdbcSelectUsuario.execute(mapSqlParameterSource);
        List<Empleado> data = (List<Empleado>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<BusquedaFolioFechas> selectEstadoFolios(ParametrosEstadoFolios entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource2 = new MapSqlParameterSource();
        mapSqlParameterSource2.addValue("PA_FIIDFOLIO", null);
        mapSqlParameterSource2.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource2.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        Map<String, Object> out = jdbcSelectEstadoFolios.execute(mapSqlParameterSource2);
        return (List<BusquedaFolioFechas>) out.get("PA_CDATOS");
    }

    public List<BusquedaFolioEtiqueta> selectFoliosEtiqueta(Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ETIQUETA", idEtiqueta);
        Map<String, Object> out = jdbcSelectFoliosEtiqueta.execute(mapSqlParameterSource);
        List<BusquedaFolioEtiqueta> lista = (List<BusquedaFolioEtiqueta>) out.get("PA_CDATOS");
        return lista;
    }

    public List<BusquedaFolioArbol> selectFoliosNivel(Long idModeloNegocio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDMODNEG", idModeloNegocio);
        Map<String, Object> out = jdbcSelectFoliosNivel.execute(mapSqlParameterSource);
        List<BusquedaFolioArbol> lista = (List<BusquedaFolioArbol>) out.get("PA_CDATOS");
        return lista;
    }

    public List<BusquedaFolioEtiqueta> selectFoliosEtiquetaEmpleado(Long numeroEmpleado, Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ETIQUETA", idEtiqueta);
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectFoliosEtiquetaUsuario.execute(mapSqlParameterSource);
        List<BusquedaFolioEtiqueta> lista = (List<BusquedaFolioEtiqueta>) out.get("PA_CDATOS");
        for (BusquedaFolioEtiqueta folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public List<BusquedaFolioArbol> selectFoliosNivelEmpleado(Long numeroEmpleado, Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDMODNEG", idEtiqueta);
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectFoliosNivelUsuario.execute(mapSqlParameterSource);
        List<BusquedaFolioArbol> lista = (List<BusquedaFolioArbol>) out.get("PA_CDATOS");
        for (BusquedaFolioArbol folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public List<BusquedaFolioAutorizacion> selectFoliosPorAutorizarUsuario(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectFoliosAutorizarUsuario.execute(mapSqlParameterSource);
        return (List<BusquedaFolioAutorizacion>) out.get("PA_CDATOS");
    }

    public BusquedaFolioDocumento selectDocumento(Long idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        Map<String, Object> out = jdbcSelectDocumento.execute(mapSqlParameterSource);
        List<BusquedaFolioDocumento> data = (List<BusquedaFolioDocumento>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<BusquedaFolioAutorizacion> selectFoliosUsuarioRelacionados(Long idFolio, Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectFoliosUsuarioRelacionados.execute(mapSqlParameterSource);
        List<BusquedaFolioAutorizacion> data = (List<BusquedaFolioAutorizacion>) out.get("PA_CDATOS");
        return data;
    }

    public List<BusquedaFolioTop> selectTopDocumentosCalificados(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectTopDocumentosCalificados.execute(mapSqlParameterSource);
        List<BusquedaFolioTop> lista = (List<BusquedaFolioTop>) out.get("PA_CDATOS");
        for (BusquedaFolioTop folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public List<BusquedaFolioTop> selectTopDocumentosRecientes(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectTopDocumentosRecientes.execute(mapSqlParameterSource);
        List<BusquedaFolioTop> lista = (List<BusquedaFolioTop>) out.get("PA_CDATOS");
        for (BusquedaFolioTop folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public List<BusquedaFolioTop> selectTopDocumentosUltimaSemana(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_SEMANA", null);
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectTopDocumentosUltimosSemana.execute(mapSqlParameterSource);
        List<BusquedaFolioTop> lista = (List<BusquedaFolioTop>) out.get("PA_CDATOS");
        for (BusquedaFolioTop folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public List<BusquedaFolioTop> selectTopDocumentosUltimoMes(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_MES", null);
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectTopDocumentosUltimoMes.execute(mapSqlParameterSource);
        List<BusquedaFolioTop> lista = (List<BusquedaFolioTop>) out.get("PA_CDATOS");
        for (BusquedaFolioTop folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public List<BusquedaFolioTop> selectTopDocumentosMasVistos(Long numeroEmpleado) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectTopDocumentosMasVistos.execute(mapSqlParameterSource);
        List<BusquedaFolioTop> lista = (List<BusquedaFolioTop>) out.get("PA_CDATOS");
        for (BusquedaFolioTop folio : lista) {
            try {
                folio.builFolio(this.selectFolio(folio.getIdFolio()));
            } catch (SQLException ex) {
                throw new CustomException(ex);
            }
        }
        return lista;
    }

    public AltaFolio duplicaFolio(Long idFolio) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
            Map<String, Object> out = jdbcDuplicaFolio.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else if (success == 1) {
                BigDecimal dato = (BigDecimal) out.get("PA_NUEVOFOLIO");
                return new AltaFolio(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
        return null;
    }
}
