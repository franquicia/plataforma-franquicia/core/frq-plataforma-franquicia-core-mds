package com.gs.baz.security.access.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import com.gs.baz.security.access.mprs.SecurityAccessRowMapper;
import com.gs.baz.security.access.util.SecurityUtils;
import com.gs.baz.security.access.util.SecurityUtilsException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SecurityAccessDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINACCESORSA");
        jdbcInsert.withProcedureName("SP_INS_ACCRSA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINACCESORSA");
        jdbcUpdate.withProcedureName("SP_ACT_ACCRSA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINACCESORSA");
        jdbcDelete.withProcedureName("SP_DEL_ACCRSA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINACCESORSA");
        jdbcSelect.withProcedureName("SP_SEL_ACCRSA");
        jdbcSelect.returningResultSet("RCL_INFO", new SecurityAccessRowMapper());
    }

    public AccesoAsimetricoDTO selectRow(String idAcceso) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCIDACCESO", idAcceso);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<AccesoAsimetricoDTO> data = (List<AccesoAsimetricoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    private AccesoAsimetricoDTO keyRandomPair() throws SecurityUtilsException {
        SecurityUtils.Generator generator = new SecurityUtils.Generator();
        String idAcceso = String.valueOf(UUID.randomUUID());
        String privateKeyStr = generator.privateKeyToString();
        String publicKeyStr = generator.publicKeyToString();
        return new AccesoAsimetricoDTO(idAcceso, publicKeyStr, privateKeyStr);
    }

    public AccesoAsimetricoDTO insertRow() throws CustomException {
        try {
            Calendar date = Calendar.getInstance();
            Calendar expired = Calendar.getInstance();
            expired.add(Calendar.HOUR_OF_DAY, 24);
            AccesoAsimetricoDTO entityDTO = this.keyRandomPair();
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCIDACCESO", entityDTO.getIdAcceso());
            mapSqlParameterSource.addValue("PA_FCACCPUBLICO", entityDTO.getPublica());
            mapSqlParameterSource.addValue("PA_FCACCPRIVADO", entityDTO.getPrivada());
            mapSqlParameterSource.addValue("PA_FDFECCREACION", date.getTime());
            mapSqlParameterSource.addValue("PA_FDEXPIRACION", expired.getTime());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of Access RSA"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of Access RSA"), ex);
        } catch (SecurityUtilsException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to generate RSA keys"), ex);
        }
    }

    public Boolean deleteRow(String idAcceso) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCIDACCESO", idAcceso);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                return true;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error  not success to delete row of Access RSA"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error  to delete row of Access RSA"), ex);
        }
    }
}
