/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class MetadataSheetDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "description")
    private String description;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "columns")
    private List<MetadataColumnDTO> columns;

    public MetadataSheetDTO(String description) {
        this.description = description;
    }

    public MetadataSheetDTO() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MetadataColumnDTO> getColumns() {
        return columns;
    }

    public void setColumns(List<MetadataColumnDTO> columns) {
        this.columns = columns;
    }

    public void addColumn(MetadataColumnDTO metadataColumnDTO) {
        if (this.columns == null) {
            this.columns = new ArrayList<>();
        }
        this.columns.add(metadataColumnDTO);
    }

    @Override
    public String toString() {
        return "SheetDTO{" + "description=" + description + ", columns=" + columns + '}';
    }

}
