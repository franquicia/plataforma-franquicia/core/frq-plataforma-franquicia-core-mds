/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CalificacionCeco;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionCecoRowMapper implements RowMapper<CalificacionCeco> {

    @Override
    public CalificacionCeco mapRow(ResultSet rs, int rowNum) throws SQLException {
        CalificacionCeco calificacionCeco = new CalificacionCeco();
        calificacionCeco.setCentroCostosBase(rs.getString("FICECOBASE_ID"));
        calificacionCeco.setCentroCostos(rs.getString("FCID_CECO"));
        calificacionCeco.setCalificacion(rs.getInt("CALIFICACION"));
        calificacionCeco.setIdRango(rs.getInt("RANGO_ID"));
        calificacionCeco.setRangoColor(rs.getString("RANGOCOLOR"));
        return calificacionCeco;
    }
}
