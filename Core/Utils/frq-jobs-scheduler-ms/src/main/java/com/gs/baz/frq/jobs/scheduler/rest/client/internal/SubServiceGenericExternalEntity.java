/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.rest.client.internal;

import com.gs.baz.frq.jobs.scheduler.rest.client.util.DefaultRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class SubServiceGenericExternalEntity extends DefaultRestTemplate {

    public String getEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                return responseString.getBody();
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public String updateEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.PUT, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                return responseString.getBody();
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public String addEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.POST, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                return responseString.getBody();
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public String deleteEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.DELETE, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                return responseString.getBody();
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

}
