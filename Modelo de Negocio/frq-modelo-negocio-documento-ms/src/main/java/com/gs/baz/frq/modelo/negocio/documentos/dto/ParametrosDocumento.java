/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosDocumento {

    @JsonProperty(value = "idDocumento", required = true)
    private transient Integer idDocumento;

    @JsonProperty(value = "idFolio", required = true)
    @ApiModelProperty(notes = "Titulo del documento", example = "Documento", required = true)
    private Integer idFolio;

    @JsonProperty(value = "titulo", required = true)
    @ApiModelProperty(notes = "Titulo del documento", example = "Documento", required = true)
    private String titulo;

    @JsonProperty(value = "objetivo", required = true)
    @ApiModelProperty(notes = "Objetivo del documento", example = "Objetivo", required = true)
    private String objetivo;

    @JsonProperty(value = "version", required = true)
    @ApiModelProperty(notes = "version del documento", example = "Version", required = true)
    private String version;

    @JsonProperty(value = "controlCambios", required = true)
    @ApiModelProperty(notes = "control cambios", example = "ejemplo", required = true)
    private String controlCambios;

    @JsonProperty(value = "fechaInicio", required = true)
    @ApiModelProperty(notes = "fecha inicio de vigencia", example = "2021-01-10", required = true)
    private String fechaInicio;

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getControlCambios() {
        return controlCambios;
    }

    public void setControlCambios(String controlCambios) {
        this.controlCambios = controlCambios;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Override
    public String toString() {
        return "ParametrosDocumento{" + "idDocumento=" + idDocumento + ", idFolio=" + idFolio + ", titulo=" + titulo + ", objetivo=" + objetivo + ", version=" + version + ", controlCambios=" + controlCambios + ", fechaInicio=" + fechaInicio + '}';
    }

}
