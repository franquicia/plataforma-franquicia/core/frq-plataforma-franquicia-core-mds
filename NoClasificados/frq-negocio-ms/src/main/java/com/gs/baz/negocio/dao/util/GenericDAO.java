/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.negocio.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public dto selectRow(Long entityID) throws CustomException;

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public List<dto> selectRowsIdFranquicia(Long entityID) throws CustomException;

    /**
     *
     * @param name
     * @return
     * @throws CustomException
     */
    public List<dto> selectRowsByName(String name) throws CustomException;

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public dto selectRowLogo(Long entityID) throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<dto> selectRows() throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto cudRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
