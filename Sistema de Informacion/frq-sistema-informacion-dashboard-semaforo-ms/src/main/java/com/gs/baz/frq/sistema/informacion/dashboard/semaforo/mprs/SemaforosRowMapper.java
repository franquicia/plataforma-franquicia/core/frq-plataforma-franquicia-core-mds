/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.Semaforo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SemaforosRowMapper implements RowMapper<Semaforo> {

    @Override
    public Semaforo mapRow(ResultSet rs, int rowNum) throws SQLException {
        Semaforo semaforo = new Semaforo();
        semaforo.setIdSemaforo(rs.getInt("FIIDSEMAFORO"));
        semaforo.setDescripcion(rs.getString("FCDESCRIPCION"));
        semaforo.setClave(rs.getString("FCCLAVE"));
        return semaforo;
    }
}
