/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public interface Scenario {

    public String getName();

    public void setName(String name);

    public List<Step> getSteps();

    public Step When(String description);

    public Step Given(String description);

    public Step Then(String description);

    public List<Examples> getExamples();

    public Examples Examples(List<String> headers, List<List<String>> rowData, int identation);

}
