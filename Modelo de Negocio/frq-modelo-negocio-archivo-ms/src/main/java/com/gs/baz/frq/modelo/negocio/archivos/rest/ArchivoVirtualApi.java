/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor
 */
package com.gs.baz.frq.modelo.negocio.archivos.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.archivos.bi.ArchivoBI;
import com.gs.baz.frq.modelo.negocio.archivos.dao.ArchivoDAOImpl;
import com.gs.baz.frq.modelo.negocio.archivos.dto.AltaArchivoVirtual;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoVirtualBase;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivosVirtuales;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ParametrosArchivoVirtual;
import com.gs.baz.frq.modelo.negocio.archivos.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "archivos", value = "archivos", description = "Gestión de archivos")
@RestController
@RequestMapping("/api-local/modelo-negocio/folio-virtuales-archivos/v1")
public class ArchivoVirtualApi {

    @Autowired
    private ArchivoDAOImpl archivoDAOImpl;

    @Autowired
    private ArchivoBI archivoBI;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @param idArchivo
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivo", notes = "Obtiene un archivo", nickname = "obtieneArchivo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/virtuales/archivos/{idArchivo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivoVirtualBase obtieneArchivo(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idArchivo", value = "Identificador del archivo", example = "1", required = true) @PathVariable("idArchivo") Long idArchivo) throws CustomException, DataNotFoundException {
        ArchivoVirtualBase archivoBase = archivoDAOImpl.selectRow(idFolio, idArchivo);
        if (archivoBase == null) {
            throw new DataNotFoundException();
        }
        return archivoBase;
    }

    /**
     *
     * @param idFolio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivos", notes = "Obtiene todos los archivos", nickname = "obtieneArchivos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/virtuales/archivos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivosVirtuales obtieneArchivos(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        ArchivosVirtuales archivos = new ArchivosVirtuales(archivoDAOImpl.selectAllRows(idFolio));
        if (archivos.getArchivos() != null && archivos.getArchivos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return archivos;
    }

    /**
     *
     * @param idFolio
     * @param parametrosArchivo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear archivo", notes = "Agrega un archivo", nickname = "creaArchivo")
    @RequestMapping(value = "/{idFolio}/virtuales/archivos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaArchivoVirtual creaArchivo(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "ParametrosArchivo", value = "Paramentros para el alta del archivo", required = true) @RequestBody ParametrosArchivoVirtual parametrosArchivo) throws DataNotInsertedException {
        parametrosArchivo.setIdFolio(idFolio.intValue());
        return archivoDAOImpl.insertRow(parametrosArchivo);
    }

    /**
     *
     * @param idFolio
     * @param parametrosArchivo
     * @param idArchivo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar archivo", notes = "Actualiza un archivo", nickname = "actualizaArchivo")
    @RequestMapping(value = "/{idFolio}/virtuales/archivos/{idArchivo}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaArchivo(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "ParametrosArchivo", value = "Paramentros para la actualización del archivo", required = true) @RequestBody ParametrosArchivoVirtual parametrosArchivo, @ApiParam(name = "idArchivo", value = "Identificador del archivo", example = "1", required = true) @PathVariable("idArchivo") Integer idArchivo) throws DataNotUpdatedException {
        parametrosArchivo.setIdArchivo(idArchivo);
        parametrosArchivo.setIdFolio(idFolio.intValue());
        archivoDAOImpl.updateRow(parametrosArchivo);
        return new SinResultado();
    }

    /**
     *
     * @param idArchivo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar archivo", notes = "Elimina un item de los archivos", nickname = "eliminaArchivo")
    @RequestMapping(value = "/{idFolio}/virtuales/archivos/{idArchivo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaArchivo(@ApiParam(name = "idArchivo", value = "Identificador del archivo", example = "1", required = true) @PathVariable("idArchivo") Integer idArchivo) throws DataNotDeletedException {
        archivoDAOImpl.deleteRow(idArchivo);
        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivos", notes = "Obtiene todos los archivos", nickname = "obtieneArchivos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/virtuales/copia-archivos/{idFolioNuevo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivosVirtuales copiaArchivos(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idFolioNuevo", value = "Identificador de folio", example = "2", required = true) @PathVariable("idFolioNuevo") Long idFolioNuevo) throws CustomException, DataNotFoundException, DataNotInsertedException, DataNotUpdatedException {
        ArchivosVirtuales archivos = new ArchivosVirtuales(archivoDAOImpl.selectAllRows(idFolioNuevo));
        if (archivos.getArchivos() != null && archivos.getArchivos().isEmpty()) {
            throw new DataNotFoundException();
        } else {
            archivoBI.duplicaArchivos(idFolio, idFolioNuevo);
        }
        return archivos;
    }

}
