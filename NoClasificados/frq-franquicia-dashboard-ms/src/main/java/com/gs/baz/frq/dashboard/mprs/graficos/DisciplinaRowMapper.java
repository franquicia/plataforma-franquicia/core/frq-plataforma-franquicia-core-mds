package com.gs.baz.frq.dashboard.mprs.graficos;

import com.gs.baz.frq.dashboard.dto.graficos.DisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DisciplinaRowMapper implements RowMapper<DisciplinaDTO> {

    private DisciplinaDTO disciplinaDTO;

    @Override
    public DisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        disciplinaDTO = new DisciplinaDTO();
        disciplinaDTO.setIdDisciplina((BigDecimal) rs.getObject("FIDISCIPLINA_ID"));
        disciplinaDTO.setDisciplina(rs.getString("FCDISCIPLINA"));
        disciplinaDTO.setIdFrecuenciaMedicion((BigDecimal) rs.getObject("FIFRECUENCIA_ID"));
        return disciplinaDTO;
    }
}
