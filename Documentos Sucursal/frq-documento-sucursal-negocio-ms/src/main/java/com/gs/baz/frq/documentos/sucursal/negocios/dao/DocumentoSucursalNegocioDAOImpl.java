/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocios.dao;

import com.gs.baz.frq.documentos.sucursal.negocios.dto.AltaNegocio;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.Negocio;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.NegocioBase;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.ParametrosNegocio;
import com.gs.baz.frq.documentos.sucursal.negocios.mprs.NegocioRowMapper;
import com.gs.baz.frq.documentos.sucursal.negocios.mprs.NegociosRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalNegocioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PA_ADNEGO";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_NEGO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_NEGO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_NEGO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SP_SEL_NEGO");
        jdbcSelect.returningResultSet("RCL_NEGO", new NegocioRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_NEGO");
        jdbcSelectAll.returningResultSet("RCL_NEGO", new NegociosRowMapper());

    }

    public NegocioBase selectRow(Long idCampoSensible) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDNEGO", idCampoSensible);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<NegocioBase> data = (List<NegocioBase>) out.get("RCL_NEGO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Negocio> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDNEGO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Negocio>) out.get("RCL_NEGO");
    }

    public AltaNegocio insertRow(ParametrosNegocio entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_IDNEGO");
                return new AltaNegocio(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosNegocio entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
