/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.file.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class FileGenericDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "path")
    private String path;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "name_file")
    private String nameFile;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "content_b64")
    private String contentB64;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "content_type")
    private String contentType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "is_file")
    private Boolean isFile;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "key_path")
    private String keyPath;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "key_b64_path")
    private String keyB64Path;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "parent")
    private String parent;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "level")
    private Integer level;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "writed")
    private Boolean writed;

    public FileGenericDTO() {
    }

    public FileGenericDTO(String nameFile, String keyPath) {
        this.nameFile = nameFile;
        this.keyPath = keyPath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public String getContentB64() {
        return contentB64;
    }

    public void setContentB64(String contentB64) {
        this.contentB64 = contentB64;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Boolean getIsFile() {
        return isFile;
    }

    public void setIsFile(Boolean isFile) {
        this.isFile = isFile;
    }

    public String getKeyPath() {
        return keyPath;
    }

    public void setKeyPath(String keyPath) {
        this.keyPath = keyPath;
    }

    public String getKeyB64Path() {
        return keyB64Path;
    }

    public void setKeyB64Path(String keyB64Path) {
        this.keyB64Path = keyB64Path;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getWrited() {
        return writed;
    }

    public void setWrited(Boolean writed) {
        this.writed = writed;
    }

    @Override
    public String toString() {
        return "FileGenericDTO{" + "path=" + path + ", nameFile=" + nameFile + ", contentB64=" + contentB64 + ", contentType=" + contentType + ", isFile=" + isFile + ", keyPath=" + keyPath + ", keyB64Path=" + keyB64Path + ", parent=" + parent + ", level=" + level + ", writed=" + writed + '}';
    }

}
