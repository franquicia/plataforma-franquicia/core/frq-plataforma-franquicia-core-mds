package com.gs.baz.frq.secciones.protocolo.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.secciones.protocolo.dao.util.GenericDAO;
import com.gs.baz.frq.secciones.protocolo.dto.SeccionesProtocoloDTO;
import com.gs.baz.frq.secciones.protocolo.mprs.SeccionesProtocoloRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SeccionesProtocoloDAOImpl extends DefaultDAO implements GenericDAO<SeccionesProtocoloDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectProtocoloFranquicia;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMSECCIONXP");
        jdbcInsert.withProcedureName("SPINSSECCIONXPRO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMSECCIONXP");
        jdbcUpdate.withProcedureName("SPACTSECCIONXPRO");

        jdbcSelectProtocoloFranquicia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectProtocoloFranquicia.withSchemaName(schema);
        jdbcSelectProtocoloFranquicia.withCatalogName("PAADMSECCIONXP");
        jdbcSelectProtocoloFranquicia.withProcedureName("SPGETSECCIONXPRO");
        jdbcSelectProtocoloFranquicia.returningResultSet("PA_CDATOS", new SeccionesProtocoloRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMSECCIONXP");
        jdbcSelectByName.withProcedureName("SPGETSECCIONXPRO");
        jdbcSelectByName.returningResultSet("PA_CDATOS", new SeccionesProtocoloRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMSECCIONXP");
        jdbcDelete.withProcedureName("SPDELSECCIONXPRO");
    }

    @Override
    public SeccionesProtocoloDTO insertRow(SeccionesProtocoloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FINUMPREGUNTAS", entityDTO.getNumeroPregunta());
            mapSqlParameterSource.addValue("PA_FIPORCENTAJE", entityDTO.getPorcentaje());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  ProtocoloFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  ProtocoloFranquicia"), ex);
        }
    }

    @Override
    public SeccionesProtocoloDTO updateRow(SeccionesProtocoloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FINUMPREGUNTAS", entityDTO.getNumeroPregunta());
            mapSqlParameterSource.addValue("PA_FIPORCENTAJE", entityDTO.getPorcentaje());
            mapSqlParameterSource.addValue("PA_FIVERSION", entityDTO.getVersion());
            mapSqlParameterSource.addValue("PA_FDFECHAVERSION", entityDTO.getFechaVersion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of ProtocoloFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of ProtocoloFranquicia"), ex);
        }
    }

    @Override
    public List<SeccionesProtocoloDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", null);
        Map<String, Object> out = jdbcSelectProtocoloFranquicia.execute(mapSqlParameterSource);
        return (List<SeccionesProtocoloDTO>) out.get("PA_CDATOS");
    }

    @Override
    public List<SeccionesProtocoloDTO> selectRows(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityID);
        Map<String, Object> out = jdbcSelectProtocoloFranquicia.execute(mapSqlParameterSource);
        return (List<SeccionesProtocoloDTO>) out.get("PA_CDATOS");

    }

    @Override
    public SeccionesProtocoloDTO deleteRow(SeccionesProtocoloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of ProtocoloFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of ProtocoloFranquicia"), ex);
        }
    }

}
