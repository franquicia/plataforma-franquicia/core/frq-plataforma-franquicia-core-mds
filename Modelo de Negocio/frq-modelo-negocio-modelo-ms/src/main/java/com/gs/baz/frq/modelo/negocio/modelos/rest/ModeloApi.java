/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.modelos.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.modelos.dao.ModeloDAOImpl;
import com.gs.baz.frq.modelo.negocio.modelos.dto.AltaModelo;
import com.gs.baz.frq.modelo.negocio.modelos.dto.ModeloBase;
import com.gs.baz.frq.modelo.negocio.modelos.dto.Modelos;
import com.gs.baz.frq.modelo.negocio.modelos.dto.ParametrosModelo;
import com.gs.baz.frq.modelo.negocio.modelos.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "catalogos-modelos", value = "catalogos-modelos", description = "Gestión del catalogo de modelos")
@RestController
@RequestMapping("/api-local/modelo-negocio/catalogos/modelos/v1")
public class ModeloApi {

    @Autowired
    private ModeloDAOImpl modeloDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idModelo
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene modelo", notes = "Obtiene un modelo", nickname = "obtieneModelo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idModelo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModeloBase obtieneModelo(@ApiParam(name = "idModelo", value = "Identificador del modelo", example = "1", required = true) @PathVariable("idModelo") Long idModelo) throws CustomException, DataNotFoundException {
        ModeloBase modeloBase = modeloDAOImpl.selectRow(idModelo);
        if (modeloBase == null) {
            throw new DataNotFoundException();
        }
        return modeloBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene modelos", notes = "Obtiene todos los modelos", nickname = "obtieneModelos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Modelos obtieneModelos() throws CustomException, DataNotFoundException {
        Modelos modelos = new Modelos(modeloDAOImpl.selectAllRows());
        if (modelos.getModelos() != null && modelos.getModelos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return modelos;
    }

    /**
     *
     * @param parametrosModelo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear modelo", notes = "Agrega un modelo", nickname = "creaModelo")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaModelo creaModelo(@ApiParam(name = "ParametrosModelo", value = "Paramentros para el alta del modelo", required = true) @RequestBody ParametrosModelo parametrosModelo) throws DataNotInsertedException {
        return modeloDAOImpl.insertRow(parametrosModelo);
    }

    /**
     *
     * @param parametrosModelo
     * @param idModelo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar modelo", notes = "Actualiza un modelo", nickname = "actualizaModelo")
    @RequestMapping(value = "/{idModelo}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaModelo(@ApiParam(name = "ParametrosModelo", value = "Paramentros para la actualización del modelo", required = true) @RequestBody ParametrosModelo parametrosModelo, @ApiParam(name = "idModelo", value = "Identificador del modelo", example = "1", required = true) @PathVariable("idModelo") Integer idModelo) throws DataNotUpdatedException {
        parametrosModelo.setIdModelo(idModelo);
        modeloDAOImpl.updateRow(parametrosModelo);
        return new SinResultado();
    }

    /**
     *
     * @param idModelo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar modelo", notes = "Elimina un item de los modelos", nickname = "eliminaModelo")
    @RequestMapping(value = "/{idModelo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaModelo(@ApiParam(name = "idModelo", value = "Identificador del modelo", example = "1", required = true) @PathVariable("idModelo") Integer idModelo) throws DataNotDeletedException {
        modeloDAOImpl.deleteRow(idModelo);
        return new SinResultado();
    }

}
