/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.envia.correo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de conteo de Informacion de sucursales por fecha", value = "ConteoFechas")
public class ConteoNegocioSemanalLayoutsDTO {

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador negocio", example = "1", position = -2)
    private String idNegocio;

    @JsonProperty(value = "valorConteo")
    @ApiModelProperty(notes = "Identificador valor conteo", example = "1", position = -4)
    private Integer valorConteo;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Descripcion del nombreProyecto", example = "Reconstrucciones")
    private String nombreProyecto;

    @JsonProperty(value = "fechaInicio")
    @ApiModelProperty(notes = " fecha Inicio reporte", example = "23/08/21")
    private String fechaInicio;

    @JsonProperty(value = "fechaFin")
    @ApiModelProperty(notes = " fecha fin reporte", example = "23/08/21")
    private String fechaFin;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador idProyecto", example = "1", position = -1)
    private Integer idProyecto;

    public Integer getValorConteo() {
        return valorConteo;
    }

    public void setValorConteo(Integer valorConteo) {
        this.valorConteo = valorConteo;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

}
