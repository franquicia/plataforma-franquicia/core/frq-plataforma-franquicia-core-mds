package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g001;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g001.DashboardCertificacionG001DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG001RowMapper implements RowMapper<DashboardCertificacionG001DTO> {

    private DashboardCertificacionG001DTO dashboardCertificacionG001DTO;

    @Override
    public DashboardCertificacionG001DTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        dashboardCertificacionG001DTO = new DashboardCertificacionG001DTO();
        dashboardCertificacionG001DTO.setCoberturaTotal((BigDecimal) rs.getObject("TOTAL_ASIGNADOS"));
        dashboardCertificacionG001DTO.setCoberturaCertificacion((BigDecimal) rs.getObject("TOTAL_CERTIFICADOS"));
        dashboardCertificacionG001DTO.setRango((BigDecimal) rs.getObject("RANGO"));
        dashboardCertificacionG001DTO.setRangoColor(rs.getString("RANGOCOLOR"));
        dashboardCertificacionG001DTO.setDiasRestantes((BigDecimal) rs.getObject("DIAS_RESTANTES"));
        return dashboardCertificacionG001DTO;
    }
}
