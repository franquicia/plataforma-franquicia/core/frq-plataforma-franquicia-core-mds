/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de estado de la firma del modelo de negocio", value = "estado de la firma")
public class EstadosFirma {

    @JsonProperty(value = "estadoFirma")
    @ApiModelProperty(notes = "estadoFirma")
    private List<EstadoFirma> estadoFirma;

    public EstadosFirma(List<EstadoFirma> estadoFirma) {
        this.estadoFirma = estadoFirma;
    }

    public List<EstadoFirma> getEstadosFirma() {
        return estadoFirma;
    }

    public void setEstadosFirma(List<EstadoFirma> estadoFirma) {
        this.estadoFirma = estadoFirma;
    }

    @Override
    public String toString() {
        return "EstadosFirma{" + "estadoFirma=" + estadoFirma + '}';
    }

}
