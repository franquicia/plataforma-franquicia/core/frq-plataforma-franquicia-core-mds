package com.gs.baz.frq.negocio.version.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.negocio.version.dto.NegocioVersionDTO;
import com.gs.baz.frq.negocio.version.mprs.CecosCertificacionRowMapper;
import com.gs.baz.frq.negocio.version.mprs.NegociosCertificacionRowMapper;
import com.gs.baz.frq.negocio.version.mprs.UsuariosCertificacionRowMapper;
import com.gs.baz.frq.negocio.version.mprs.VersionCertificacionRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class NegocioVersionDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertNegocioSup;
    private DefaultJdbcCall jdbcUpdateNegocioSup;
    private DefaultJdbcCall jdbcDeleteNegocioSup;
    private DefaultJdbcCall jdbcObtieneNegocioSup;

    private DefaultJdbcCall jdbcInsertVersionSup;
    private DefaultJdbcCall jdbcUpdatetVersionSup;
    private DefaultJdbcCall jdbcDeletetVersionSup;
    private DefaultJdbcCall jdbcObtienetVersionSup;

    private DefaultJdbcCall jdbcObtieneCecos;
    private DefaultJdbcCall jdbcObtieneUsuarios;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertNegocioSup.withSchemaName(schema);
        jdbcInsertNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcInsertNegocioSup.withProcedureName("SP_INS_NEGOSUP");

        jdbcUpdateNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateNegocioSup.withSchemaName(schema);
        jdbcUpdateNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcUpdateNegocioSup.withProcedureName("SP_ACT_NEGOSUP");

        jdbcDeleteNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteNegocioSup.withSchemaName(schema);
        jdbcDeleteNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcDeleteNegocioSup.withProcedureName("SP_DEL_NEGO");

        jdbcObtieneNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneNegocioSup.withSchemaName(schema);
        jdbcObtieneNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcObtieneNegocioSup.withProcedureName("SP_SELNEGOSUP");
        jdbcObtieneNegocioSup.returningResultSet("RCL_NEGO", new NegociosCertificacionRowMapper());
        //version
        jdbcInsertVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertVersionSup.withSchemaName(schema);
        jdbcInsertVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcInsertVersionSup.withProcedureName("SP_INS_VERSUP");

        jdbcUpdatetVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdatetVersionSup.withSchemaName(schema);
        jdbcUpdatetVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcUpdatetVersionSup.withProcedureName("SP_ACT_VERSUP");

        jdbcDeletetVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeletetVersionSup.withSchemaName(schema);
        jdbcDeletetVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcDeletetVersionSup.withProcedureName("SP_DEL_VERSUP");

        jdbcObtienetVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtienetVersionSup.withSchemaName(schema);
        jdbcObtienetVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcObtienetVersionSup.withProcedureName("SP_SELVERSUP");
        jdbcObtienetVersionSup.returningResultSet("RCL_ASIGN", new VersionCertificacionRowMapper());

        //consultas especificas  Cecos
        jdbcObtieneCecos = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneCecos.withSchemaName(schema);
        jdbcObtieneCecos.withCatalogName("PAASIGN_SUP");
        jdbcObtieneCecos.withProcedureName("SP_SEL_CECO");
        jdbcObtieneCecos.returningResultSet("RCL_CECO", new CecosCertificacionRowMapper());

        //usuarios
        jdbcObtieneUsuarios = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneUsuarios.withSchemaName(schema);
        jdbcObtieneUsuarios.withCatalogName("PAASIGN_SUP");
        jdbcObtieneUsuarios.withProcedureName("SP_SEL_USUARIOS");
        jdbcObtieneUsuarios.returningResultSet("RCL_USUARIOS", new UsuariosCertificacionRowMapper());

    }

    public int insertRow(NegocioVersionDTO entityDTO) throws CustomException {
        try {

            int error = 0;
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_ACRONIMO", entityDTO.getAcronimo());

            Map<String, Object> out = jdbcInsertNegocioSup.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  negocio"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  negocio"), ex);
        }
    }

    public NegocioVersionDTO updateRow(NegocioVersionDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getStatus());
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_ACRONIMO", entityDTO.getAcronimo());

            Map<String, Object> out = jdbcUpdateNegocioSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of negocio"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of negocio"), ex);
        }
    }

    public int eliminaRows(NegocioVersionDTO entityDTO) throws CustomException {
        try {
            int error = 0;
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());

            Map<String, Object> out = jdbcDeleteNegocioSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of negocio"));

            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of negocio"), ex);

        }
    }

    //trae negocios tabla negociossup
    public List<NegocioVersionDTO> selectRowDTO(NegocioVersionDTO entityID) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_NEGO", entityID.getIdNegocio());
        Map<String, Object> out = jdbcObtieneNegocioSup.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<NegocioVersionDTO> data = (List<NegocioVersionDTO>) out.get("RCL_NEGO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    //version

    public int insertVersionSup(NegocioVersionDTO entityDTO) throws CustomException {
        try {

            int error = 0;
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CHEKLIST", entityDTO.getIdCheckList());
            mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_OBSERVACION", entityDTO.getDescripcion());

            Map<String, Object> out = jdbcInsertVersionSup.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  version sup"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  version sup"), ex);
        }
    }

    public NegocioVersionDTO updateVersionSup(NegocioVersionDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CHEKLIST", entityDTO.getIdCheckList());
            mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_IDVERS", entityDTO.getIdVersion());

            Map<String, Object> out = jdbcUpdatetVersionSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of version sup"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of version sup"), ex);
        }
    }

    public int eliminaVersionSup(NegocioVersionDTO entityDTO) throws CustomException {
        try {
            int error = 0;
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_IDVERS", entityDTO.getIdVersion());
            mapSqlParameterSource.addValue("PA_PARAM", entityDTO.getParametro());

            Map<String, Object> out = jdbcDeletetVersionSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of version sup"));

            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of version sup"), ex);

        }
    }

    public List<NegocioVersionDTO> selectVersionesSup(NegocioVersionDTO entityID) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_NEGO", entityID.getIdNegocio());
        Map<String, Object> out = jdbcObtienetVersionSup.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<NegocioVersionDTO> data = (List<NegocioVersionDTO>) out.get("RCL_VERS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    //consultas especificas
    public List<NegocioVersionDTO> selectCecos(NegocioVersionDTO entityID) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_PREFIJO", entityID.getPrefijo());
        mapSqlParameterSource.addValue("PA_CECO", entityID.getIdCeco());
        mapSqlParameterSource.addValue("PA_NOMBRECECO", entityID.getNombreCeco());

        Map<String, Object> out = jdbcObtieneCecos.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<NegocioVersionDTO> data = (List<NegocioVersionDTO>) out.get("RCL_CECO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<NegocioVersionDTO> selectUsuarios(NegocioVersionDTO entityID) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_PUESTO", entityID.getIdPuesto());
        mapSqlParameterSource.addValue("PA_USUARIO", entityID.getIdUsuario());
        mapSqlParameterSource.addValue("PA_NOMBRE", entityID.getNombreUsuario());

        Map<String, Object> out = jdbcObtieneUsuarios.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<NegocioVersionDTO> data = (List<NegocioVersionDTO>) out.get("RCL_USUARIOS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    /*

    //fases proyecto
    @SuppressWarnings("unchecked")
    public  Map<String, Object> selectCalendarioRows(AsignacionSupDTO entityDTO) throws CustomException {
    	int error=0;
    	Map<String, Object> out = null;
		Map<String, Object> lista = null;
		List<AsignacionSupDTO> listaAsignaciones = null;
		List<AsignacionSupDTO> listaSoftHistorico = null;
        mapSqlParameterSource = new MapSqlParameterSource();
        //dia mes año
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getFecha());  
        out = jdbcObtieneDatosCalendario.execute(mapSqlParameterSource);
        
        listaAsignaciones = (List<AsignacionSupDTO>) out.get("RCL_ASIGN");
        listaSoftHistorico = (List<AsignacionSupDTO>) out.get("RCL_SOFT");
        
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		error = resultado.intValue();
		if (error != 1) {
			logger.info("No se cargo la informacion Correctamente");
			return null;
		}
        lista = new HashMap<String, Object>();
        lista.put("listaAsignaciones", listaAsignaciones);
        lista.put("listaSoftHistorico", listaSoftHistorico);
        
        return lista;
    }
     */

}
