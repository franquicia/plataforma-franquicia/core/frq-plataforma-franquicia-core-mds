/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.paises.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class PaisBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del pais", example = "México")
    private String descripcion;

    @JsonProperty(value = "acronimo")
    @ApiModelProperty(notes = "Aronimo del pais", example = "MEX")
    private String acronimo;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    @Override
    public String toString() {
        return "PaisBase{" + "descripcion=" + descripcion + ", acronimo=" + acronimo + '}';
    }

}
