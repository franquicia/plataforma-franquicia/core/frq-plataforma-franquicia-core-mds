/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cescobarh
 */
public class ParametrosRango {

    @JsonProperty(value = "idSemaforo")
    private transient Integer idSemaforo;

    @JsonProperty(value = "idRango")
    private transient Integer idRango;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion del rango", example = "Rango", required = true)
    @NotEmpty
    private String descripcion;

    @JsonProperty(value = "limiteInferior")
    @ApiModelProperty(notes = "Limite inferior del rango", example = "0")
    @NotNull
    private Integer limiteInferior;

    @JsonProperty(value = "limiteSuperior")
    @ApiModelProperty(notes = "Limite superior del rango", example = "100")
    @NotNull
    private Integer limiteSuperior;

    @JsonProperty(value = "color")
    @ApiModelProperty(notes = "Color del rango", example = "red")
    @NotEmpty
    private String color;

    public Integer getIdSemaforo() {
        return idSemaforo;
    }

    public void setIdSemaforo(Integer idSemaforo) {
        this.idSemaforo = idSemaforo;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(Integer limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public Integer getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(Integer limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "ParametrosRango{" + "idSemaforo=" + idSemaforo + ", idRango=" + idRango + ", descripcion=" + descripcion + ", limiteInferior=" + limiteInferior + ", limiteSuperior=" + limiteSuperior + ", color=" + color + '}';
    }

}
