package com.gs.baz.frq.punto.contacto.protocolo.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.punto.contacto.protocolo.dao.util.GenericDAO;
import com.gs.baz.frq.punto.contacto.protocolo.dto.PuntoContactoProtocoloDTO;
import com.gs.baz.frq.punto.contacto.protocolo.mprs.PuntoContactoProtocoloRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoProtocoloDAOImpl extends DefaultDAO implements GenericDAO<PuntoContactoProtocoloDTO> {

    private DefaultJdbcCall jdbcSelect;

    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAPCPROTOCOLO");
        jdbcSelect.withProcedureName("SP_SEL_PCPROTOCOLOS");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoProtocoloRowMapper());

    }

    @Override
    public List<PuntoContactoProtocoloDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<PuntoContactoProtocoloDTO>) out.get("RCL_INFO");
    }

    @Override
    public List<PuntoContactoProtocoloDTO> selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<PuntoContactoProtocoloDTO>) out.get("RCL_INFO");
    }

}
