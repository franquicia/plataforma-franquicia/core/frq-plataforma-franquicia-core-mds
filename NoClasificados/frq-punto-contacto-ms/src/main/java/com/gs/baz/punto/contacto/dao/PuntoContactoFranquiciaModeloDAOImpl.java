package com.gs.baz.punto.contacto.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.punto.contacto.dto.PuntoContactoCategoriaDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFormatoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFranquiciaModeloDTO;
import com.gs.baz.punto.contacto.dto.TipoFormatoDTO;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoFranquiciaModeloDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcUpdate;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    private TipoFormatoDAOImpl tipoFormatoDAOImpl;

    @Autowired
    private PuntoContactoCategoriaDAOImpl puntoContactoCategoriaDAOImpl;

    @Autowired
    private PuntoContactoFormatoDAOImpl puntoContactoFormatoDAOImpl;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINPCFRQMOD");
        jdbcInsert.withProcedureName("SP_INS_PCFRQMODC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINPCFRQMOD");
        jdbcDelete.withProcedureName("SP_DEL_PCFRQMOD");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINPCFRQMOD");
        jdbcUpdate.withProcedureName("SP_ACT_PCFRQMOD");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPCFRQMOD");
        jdbcSelect.withProcedureName("SP_SEL_PCFRQMOD");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoRowMapper().new PuntoContactoFranquiciaModeloRowMapper());
    }

    public PuntoContactoFranquiciaModeloDTO selectRow(Integer idPuntoContacto) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", null);
        mapSqlParameterSource.addValue("PA_FIIDCECO", idPuntoContacto);
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", null);
        mapSqlParameterSource.addValue("PA_FIIDMODELO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoFranquiciaModeloDTO> data = (List<PuntoContactoFranquiciaModeloDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO = data.get(0);
            Integer idPuntoContactoFranquiciaModelo = puntoContactoFranquiciaModeloDTO.getIdPuntoContactoFranquiciaModelo();
            puntoContactoFranquiciaModeloDTO.setCategorias(puntoContactoCategoriaDAOImpl.selectRows(idPuntoContactoFranquiciaModelo));
            puntoContactoFranquiciaModeloDTO.setTipoFormatos(tipoFormatoDAOImpl.selectRows(idPuntoContactoFranquiciaModelo));
            return puntoContactoFranquiciaModeloDTO;
        } else {
            PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO = new PuntoContactoFranquiciaModeloDTO();
            puntoContactoFranquiciaModeloDTO.setCategorias(puntoContactoCategoriaDAOImpl.selectRows(-1));
            puntoContactoFranquiciaModeloDTO.setTipoFormatos(tipoFormatoDAOImpl.selectRows(-1));
            return puntoContactoFranquiciaModeloDTO;
        }
    }

    public PuntoContactoFranquiciaModeloDTO insertRow(PuntoContactoFranquiciaModeloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCECO", entityDTO.getIdPuntoContacto());
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getFranquiciaDTO().getIdFranquicia());
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getModeloDTO().getIdModelo());
            mapSqlParameterSource.addValue("PA_FIIDEMPLEADO", entityDTO.getIdEmpleado());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                BigDecimal idPuntoContactoFranquiciaModelo = (BigDecimal) out.get("PA_FIIDPCFRQMOD");
                entityDTO.setIdPuntoContactoFranquiciaModelo(idPuntoContactoFranquiciaModelo);
                List<PuntoContactoCategoriaDTO> categorias = entityDTO.getCategorias();
                List<TipoFormatoDTO> tipoFormatos = entityDTO.getTipoFormatos();
                for (PuntoContactoCategoriaDTO categoria : categorias) {
                    categoria.setIdPuntoContactoFranquiciaModelo(idPuntoContactoFranquiciaModelo);
                    if (categoria.getIdCategoria() == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>categorias>id_categoria"));
                    }
                }
                categorias = puntoContactoCategoriaDAOImpl.cdRows(idPuntoContactoFranquiciaModelo.intValue(), categorias);
                entityDTO.setCategorias(categorias);
                for (TipoFormatoDTO tipoFormatoDTO : tipoFormatos) {
                    if (tipoFormatoDTO.getIdTipoFormato() == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>tipo_formatos>id_tipo_formato"));
                    }
                    PuntoContactoFormatoDTO puntoContactoFormatoDTO = tipoFormatoDTO.getPuntoContactoFormatoDTO();
                    if (puntoContactoFormatoDTO == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>tipo_formatos>formato"));
                    }
                    if (puntoContactoFormatoDTO.getIdFormato() == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>tipo_formatos>formato>id_formato"));
                    }
                    puntoContactoFormatoDTO.setIdPuntoContactoFranquiciaModelo(idPuntoContactoFranquiciaModelo);
                    puntoContactoFormatoDTO.setIdTipoFormato(new BigDecimal(tipoFormatoDTO.getIdTipoFormato()));
                    puntoContactoFormatoDTO = puntoContactoFormatoDAOImpl.insertRow(puntoContactoFormatoDTO);
                    puntoContactoFormatoDTO.setIdPuntoContactoFranquiciaModelo(null);
                    puntoContactoFormatoDTO.setIdTipoFormato(null);
                    tipoFormatoDTO.setPuntoContactoFormatoDTO(puntoContactoFormatoDTO);
                }
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  PuntoContactoFranquiciaModelo "));
            }
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

    public PuntoContactoFranquiciaModeloDTO updateRow(PuntoContactoFranquiciaModeloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", entityDTO.getIdPuntoContactoFranquiciaModelo());
            mapSqlParameterSource.addValue("PA_FIIDCECO", entityDTO.getIdPuntoContacto());
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getFranquiciaDTO().getIdFranquicia());
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getModeloDTO().getIdModelo());
            mapSqlParameterSource.addValue("PA_FIIDEMPLEADO", entityDTO.getIdEmpleado());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                BigDecimal idPuntoContactoFranquiciaModelo = new BigDecimal(entityDTO.getIdPuntoContactoFranquiciaModelo());
                entityDTO.setIdPuntoContactoFranquiciaModelo(idPuntoContactoFranquiciaModelo);
                List<PuntoContactoCategoriaDTO> categorias = entityDTO.getCategorias();
                List<TipoFormatoDTO> tipoFormatos = entityDTO.getTipoFormatos();
                for (PuntoContactoCategoriaDTO categoria : categorias) {
                    categoria.setIdPuntoContactoFranquiciaModelo(idPuntoContactoFranquiciaModelo);
                    if (categoria.getIdCategoria() == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>categorias>id_categoria"));
                    }
                }
                categorias = puntoContactoCategoriaDAOImpl.cdRows(idPuntoContactoFranquiciaModelo.intValue(), categorias);
                entityDTO.setCategorias(categorias);
                for (TipoFormatoDTO tipoFormatoDTO : tipoFormatos) {
                    if (tipoFormatoDTO.getIdTipoFormato() == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>tipo_formatos>id_tipo_formato"));
                    }
                    PuntoContactoFormatoDTO puntoContactoFormatoDTO = tipoFormatoDTO.getPuntoContactoFormatoDTO();
                    if (puntoContactoFormatoDTO == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>tipo_formatos>formato"));
                    }
                    if (puntoContactoFormatoDTO.getIdFormato() == null) {
                        throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion>tipo_formatos>formato>id_formato"));
                    }
                    puntoContactoFormatoDTO.setIdPuntoContactoFranquiciaModelo(idPuntoContactoFranquiciaModelo);
                    puntoContactoFormatoDTO.setIdTipoFormato(new BigDecimal(tipoFormatoDTO.getIdTipoFormato()));
                    puntoContactoFormatoDTO = puntoContactoFormatoDAOImpl.updateRow(puntoContactoFormatoDTO);
                    puntoContactoFormatoDTO.setIdPuntoContactoFranquiciaModelo(null);
                    puntoContactoFormatoDTO.setIdTipoFormato(null);
                    tipoFormatoDTO.setPuntoContactoFormatoDTO(puntoContactoFormatoDTO);
                }
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  PuntoContactoFranquiciaModelo "));
            }
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

    public PuntoContactoFranquiciaModeloDTO deleteRow(PuntoContactoFranquiciaModeloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", entityDTO.getIdPuntoContactoFranquiciaModelo());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error  not success to delete row of PuntoContactoFranquiciaModelo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error  to delete row of PuntoContactoFranquiciaModelo"), ex);
        }
    }

}
