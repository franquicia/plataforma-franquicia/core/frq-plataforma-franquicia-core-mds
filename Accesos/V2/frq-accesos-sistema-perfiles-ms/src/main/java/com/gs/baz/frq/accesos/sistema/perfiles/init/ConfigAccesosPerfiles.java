package com.gs.baz.frq.accesos.sistema.perfiles.init;

import com.gs.baz.frq.accesos.sistema.perfiles.dao.AccesosPerfilesDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.accesos.sistema.perfiles")
public class ConfigAccesosPerfiles {

    private final Logger logger = LogManager.getLogger();

    public ConfigAccesosPerfiles() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public AccesosPerfilesDAOImpl accesosPerfilesDAOImpl() {
        return new AccesosPerfilesDAOImpl();
    }

}
