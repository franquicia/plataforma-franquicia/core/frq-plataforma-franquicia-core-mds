package com.gs.baz.frq.regla7s.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.regla7s.dao.util.GenericDAO;
import com.gs.baz.frq.regla7s.dto.Regla7SDTO;
import com.gs.baz.frq.regla7s.mprs.Regla7SRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cescobarh
 */
public class Regla7SDAOImpl extends DefaultDAO implements GenericDAO<Regla7SDTO> {

    private DefaultJdbcCall jdbcInsertRegla7s;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectRegla7S;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcDeletePreguntaResponde;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcDeletePreguntaResponde = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeletePreguntaResponde.withSchemaName(schema);
        jdbcDeletePreguntaResponde.withCatalogName("PAADMRELREGLAS");
        jdbcDeletePreguntaResponde.withProcedureName("SPDELRELREGLAS");

        jdbcInsertRegla7s = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertRegla7s.withSchemaName(schema);
        jdbcInsertRegla7s.withCatalogName("PAADMREGLAS");
        jdbcInsertRegla7s.withProcedureName("SPINSREGLAS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMREGLAS");
        jdbcUpdate.withProcedureName("SPACTREGLAS");

        jdbcSelectRegla7S = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectRegla7S.withSchemaName(schema);
        jdbcSelectRegla7S.withCatalogName("PAADMREGLAS");
        jdbcSelectRegla7S.withProcedureName("SPGETREGLAS");
        jdbcSelectRegla7S.returningResultSet("PA_CDATOS", new Regla7SRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMREGLAS");
        jdbcDelete.withProcedureName("SPDELREGLAS");
    }

    @Override
    @Transactional(rollbackFor = CustomException.class)
    public Regla7SDTO insertRow(Regla7SDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIPREGUNTA_BASE", entityDTO.getPreguntaBase());
            mapSqlParameterSource.addValue("PA_FIPROTOCOLO", entityDTO.getProtocolo());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FINCUMPLIMIENTO", entityDTO.getIncumplimiento());
            mapSqlParameterSource.addValue("PA_FIREGLANUEVE", entityDTO.getReglaNueve());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FIMPERDONABLE", entityDTO.getImperdonable());
            mapSqlParameterSource.addValue("PA_FIOPORTUNIDAD", entityDTO.getOportunidad());
            Map<String, Object> out = jdbcInsertRegla7s.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdClave((BigDecimal) out.get("PA_FICLAVE_ID"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Regla7S"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Regla7S"), ex);
        }
    }

    @Override
    @Transactional(rollbackFor = CustomException.class)
    public Regla7SDTO updateRow(Regla7SDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICLAVE_ID", entityDTO.getIdClave());
            mapSqlParameterSource.addValue("PA_FIPREGUNTA_BASE", entityDTO.getPreguntaBase());
            mapSqlParameterSource.addValue("PA_FIPROTOCOLO", entityDTO.getProtocolo());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FINCUMPLIMIENTO", entityDTO.getIncumplimiento());
            mapSqlParameterSource.addValue("PA_FIREGLANUEVE", entityDTO.getReglaNueve());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FIMPERDONABLE", entityDTO.getImperdonable());
            mapSqlParameterSource.addValue("PA_FIOPORTUNIDAD", entityDTO.getOportunidad());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Regla7S"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Regla7S"), ex);
        }
    }

    @Override
    public List<Regla7SDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FICLAVE_ID", null);
        mapSqlParameterSource.addValue("PA_FIPROTOCOLO", null);
        Map<String, Object> out = jdbcSelectRegla7S.execute(mapSqlParameterSource);
        return (List<Regla7SDTO>) out.get("PA_CDATOS");
    }

    @Override
    public Regla7SDTO selectRow(Long idClave, Long protocolo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FICLAVE_ID", idClave);
        mapSqlParameterSource.addValue("PA_FIPROTOCOLO", protocolo);
        Map<String, Object> out = jdbcSelectRegla7S.execute(mapSqlParameterSource);
        List<Regla7SDTO> data = (List<Regla7SDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Regla7SDTO deleteRow(Regla7SDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICLAVE_ID", entityDTO.getIdClave());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Regla7S"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Regla7S"), ex);
        }
    }

}
