/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.modelo.negocio.acceso.dao.UsuarioDAOImpl;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.SinResultado;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.AltaDocumentoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.AltaGrupoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.DocumentosUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.ParametrosGrupoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.GruposUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.ParametrosDocumentoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.Usuarios;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "accesos", value = "accesos", description = "Gestión de consultores")
@RestController
@RequestMapping("/api-local/modelo-negocio/accesos/usuarios/v1")
public class ModeloNegocioAccesoUsuarioApi {

    @Autowired
    private UsuarioDAOImpl usuarioDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios con perfil consultor", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuarios() throws CustomException, DataNotFoundException {
        Usuarios usuarios = new Usuarios(usuarioDAOImpl.selectAllRows());
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grupos usuarios", notes = "Obtiene todos los grupos usuarios", nickname = "obtieneGruposUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{numeroEmpleado}/grupos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GruposUsuario obtieneGruposUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws CustomException, DataNotFoundException {
        GruposUsuario gruposUsuario = new GruposUsuario(usuarioDAOImpl.selectAllGrupos(numeroEmpleado));
        if (gruposUsuario.getGrupos() != null && gruposUsuario.getGrupos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return gruposUsuario;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grupos disponibles usuarios", notes = "Obtiene todos los grupos disponibles usuarios", nickname = "obtieneGruposDisponiblesUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{numeroEmpleado}/grupos/disponibles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GruposUsuario obtieneGruposDisponiblesUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws CustomException, DataNotFoundException {
        GruposUsuario gruposUsuario = new GruposUsuario(usuarioDAOImpl.selectAllGruposDisponibles(numeroEmpleado));
        if (gruposUsuario.getGrupos() != null && gruposUsuario.getGrupos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return gruposUsuario;
    }

    /**
     *
     * @param numeroEmpleado
     * @param parametrosGrupoUsuario
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crea grupo usuario", notes = "Agrega un grupo usuario", nickname = "creaGrupoUsuario")
    @RequestMapping(value = "/{numeroEmpleado}/grupos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaGrupoUsuario creaGrupoUsuario(
            @ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado,
            @ApiParam(name = "ParametrosGrupoUsuario", value = "Paramentros para el alta del grupo usuario", required = true) @RequestBody ParametrosGrupoUsuario parametrosGrupoUsuario) throws DataNotInsertedException {
        parametrosGrupoUsuario.setNumeroEmpleado(numeroEmpleado);
        return usuarioDAOImpl.insertGrupoUsuario(parametrosGrupoUsuario);
    }

    /**
     *
     * @param idGrupoUsuario
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grupo usuario", notes = "Elimina un item de los grupos del usuario", nickname = "eliminaGrupoUsuario")
    @RequestMapping(value = "/{numeroEmpleado}/grupos/{idGrupoUsuario}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaGrupoUsuario(@ApiParam(name = "idGrupoUsuario", value = "Identificador del grupo usuario", example = "1", required = true) @PathVariable("idGrupoUsuario") Integer idGrupoUsuario) throws DataNotDeletedException {
        usuarioDAOImpl.deleteGrupo(idGrupoUsuario);
        return new SinResultado();
    }

    /**
     *
     * @param headers
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documentos por usuario", notes = "Obtiene documentos por usuario", nickname = "obtieneDocumentosUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{numeroEmpleado}/documentos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentosUsuario obtieneDocumentosUsuario(@RequestHeader HttpHeaders headers, @ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws CustomException, DataNotFoundException {
        DocumentosUsuario documentosUsuario = new DocumentosUsuario(usuarioDAOImpl.selectAllDocumentos(headers, numeroEmpleado));
        if (documentosUsuario.getDocumentos() != null && documentosUsuario.getDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return documentosUsuario;
    }

    /**
     *
     * @param headers
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documentos disponibles por usuario", notes = "Obtiene documentos disponibles por usuario", nickname = "obtieneDocumentosDisponiblesUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{numeroEmpleado}/documentos/disponibles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentosUsuario obtieneDocumentosDisponiblesUsuario(@RequestHeader HttpHeaders headers, @ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws CustomException, DataNotFoundException {
        DocumentosUsuario documentosUsuario = new DocumentosUsuario(usuarioDAOImpl.selectAllDocumentosDisponibles(headers, numeroEmpleado));
        if (documentosUsuario.getDocumentos() != null && documentosUsuario.getDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return documentosUsuario;
    }

    /**
     *
     * @param numeroEmpleado
     * @param parametrosDocumentoUsuario
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crea documento usuario", notes = "Crea documento usuario", nickname = "creaDocumentoUsuario")
    @RequestMapping(value = "/{numeroEmpleado}/documentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaDocumentoUsuario creaDocumentoUsuario(
            @ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado,
            @ApiParam(name = "ParametrosGrupoUsuario", value = "Paramentros para el alta del grupo usuario", required = true) @RequestBody ParametrosDocumentoUsuario parametrosDocumentoUsuario) throws DataNotInsertedException {
        parametrosDocumentoUsuario.setNumeroEmpleado(numeroEmpleado);
        return usuarioDAOImpl.insertDocumentoUsuario(parametrosDocumentoUsuario);
    }

    /**
     *
     * @param numeroEmpleado
     * @param idDocumentoEmpleado
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Elimina documento usuario", notes = "Elimina documento usuario", nickname = "eliminaDocumentoUsuario")
    @RequestMapping(value = "/{numeroEmpleado}/documentos/{idDocumentoEmpleado}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaDocumentoUsuario(
            @ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado,
            @ApiParam(name = "idDocumentoEmpleado", value = "Identificador del documento empleado", example = "1", required = true) @PathVariable("idDocumentoEmpleado") Integer idDocumentoEmpleado) throws DataNotDeletedException {
        usuarioDAOImpl.deleteDocumento(idDocumentoEmpleado);
        return new SinResultado();
    }

}
