/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionCecosLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostos;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostosParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentrosCostos;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "visitas pendientes", value = "visitas pendientes", description = "Api para la gestión de visitas pendientes")
@RestController
@RequestMapping("/api-local/sistema-informacion/dashboard/disenos/v1")
public class SistemaInformacionCecoLayoutApi {

    @Autowired
    private SistemaInformacionCecosLayoutDAOImpl sistemaInformacionCecosLayoutDAOImpl;

    
//estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param centroCostos
     * @param centroCostosParametros
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/centro-costos/{centroCostos}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CentroCostos obtieneGrafica(@ApiParam(name = "centroCostos", value = "Centro de Costos", example = "920100", required = true) @PathVariable("centroCostos") Integer centroCostos,
@ApiParam(name = "CentroCostosParametros", value = "Parámetros para la consulta de un centro de costos", required = true) @RequestBody CentroCostosParametros centroCostosParametros ) throws CustomException, DataNotFoundException {
        centroCostosParametros.setIdCeco(centroCostos);
        CentroCostos centrosCostos = sistemaInformacionCecosLayoutDAOImpl.selectRow(centroCostosParametros);
        if (centrosCostos == null) {
            throw new DataNotFoundException();
        }
        return centrosCostos;
    }

    

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/centro-costos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CentrosCostos obtieneGraficas() throws CustomException, DataNotFoundException {
        CentrosCostos centrosCostos = new CentrosCostos(sistemaInformacionCecosLayoutDAOImpl.selectAllRows());
        if (centrosCostos.getCentroCostos()!= null && centrosCostos.getCentroCostos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return centrosCostos;
    }

    /**
     *
     * @param centroCostos
     * @param centroCostosParam
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/centro-costos/{centroCostos}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public SinResultado creaGrafica(@ApiParam(name = "centroCostos", value = "Centro de Costos", example = "920100", required = true) @PathVariable("centroCostos") Integer centroCostos,
@ApiParam(name = "CentroCostosParametros", value = "Parámetros para la consulta de un centro de costos", required = true) @RequestBody CentroCostos centroCostosParam ) throws DataNotInsertedException {
        centroCostosParam.setIdCeco(centroCostos);
        sistemaInformacionCecosLayoutDAOImpl.insertRow(centroCostosParam);
        return new SinResultado();
    }

    /**
     *
     * @param centroCostos
     * @param centroCostosParam
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar grafica", notes = "Actualiza un grafica", nickname = "actualizaGrafica")
    @RequestMapping(value = "/centro-costos/{centroCostos}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaGrafica(@ApiParam(name = "centroCostos", value = "Centro de Costos", example = "920100", required = true) @PathVariable("centroCostos") Integer centroCostos,
@ApiParam(name = "CentroCostosParametros", value = "Parámetros para la consulta de un centro de costos", required = true) @RequestBody CentroCostos centroCostosParam ) throws DataNotUpdatedException {
        centroCostosParam.setIdCeco(centroCostos);
        sistemaInformacionCecosLayoutDAOImpl.updateRow(centroCostosParam);
        return new SinResultado();
    }

    /**
     *
     * 
     * @param centroCostos
     * @param numeroEmpleadoCertificador
     * @param idProveedor
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grafica", notes = "Elimina un item de los graficas", nickname = "eliminaGrafica")
    @RequestMapping(value = "/centro-costos/{centroCostos}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaGrafica(@ApiParam(name = "centroCostos", value = "Centro de Costos", example = "920100", required = true) @PathVariable("centroCostos") Integer centroCostos,
            @ApiParam(name = "idProveedor", value = "Identificador del proveedor", example = "202622", required = true) @RequestParam("idProveedor") Integer idProveedor,
            @ApiParam(name = "numeroEmpleadoCertificador", value = "Número de empleado del certificador", example = "920100", required = true) @RequestParam("numeroEmpleadoCertificador") Integer numeroEmpleadoCertificador)  throws DataNotDeletedException {
        sistemaInformacionCecosLayoutDAOImpl.deleteRow(centroCostos, idProveedor, numeroEmpleadoCertificador);
        return new SinResultado();
    }

    
}
