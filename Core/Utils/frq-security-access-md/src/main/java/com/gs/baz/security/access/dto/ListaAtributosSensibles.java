/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de datos de atributo sensible agrupados", value = "ListaAtributosSensibles")
public class ListaAtributosSensibles {

    @JsonProperty(value = "atributosSensibles")
    private List<AtributoSensibleAgrupado> atributosSensibles;

    public ListaAtributosSensibles(List<AtributoSensibleAgrupado> atributosSensibles) {
        this.atributosSensibles = atributosSensibles;
    }

    public List<AtributoSensibleAgrupado> getAtributosSensibles() {
        return atributosSensibles;
    }

    public void setAtributosSensibles(List<AtributoSensibleAgrupado> atributosSensibles) {
        this.atributosSensibles = atributosSensibles;
    }

}
