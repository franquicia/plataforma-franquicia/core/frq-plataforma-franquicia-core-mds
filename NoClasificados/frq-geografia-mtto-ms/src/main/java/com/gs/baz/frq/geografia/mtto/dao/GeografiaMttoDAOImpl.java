package com.gs.baz.frq.geografia.mtto.dao;

import com.gs.baz.frq.geografia.mtto.dto.GeografiaMttoDTO;
import com.gs.baz.frq.geografia.mtto.dto.JefeSuperiorMttoDTO;
import com.gs.baz.frq.geografia.mtto.mprs.GeografiaMttoRowMapper;
import com.gs.baz.frq.geografia.mtto.mprs.JefeSuperiorMttoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class GeografiaMttoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertGografia;
    private DefaultJdbcCall jdbcUpdateGeografia;
    private DefaultJdbcCall jdbcDeleteGeografia;
    private DefaultJdbcCall jdbcObtieneGeografia;

    private DefaultJdbcCall jdbcObtieneJefeUsuario;
    private DefaultJdbcCall jdbcUpdateJefeUsuario;
    private DefaultJdbcCall jdbcDeleteJefeUsuario;
    private DefaultJdbcCall jdbcInsertaJefeUsuario;

    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertGografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertGografia.withSchemaName(schema);
        jdbcInsertGografia.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcInsertGografia.withProcedureName("SP_INS_GEOGRAFIA");

        jdbcUpdateGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateGeografia.withSchemaName(schema);
        jdbcUpdateGeografia.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcUpdateGeografia.withProcedureName("SP_ACT_GEOGRAFIA");

        jdbcDeleteGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteGeografia.withSchemaName(schema);
        jdbcDeleteGeografia.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcDeleteGeografia.withProcedureName("SP_DEL_GEOGRAFIA");

        jdbcObtieneGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneGeografia.withSchemaName(schema);
        jdbcObtieneGeografia.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcObtieneGeografia.withProcedureName("SP_GEOGRAFIA");
        jdbcObtieneGeografia.returningResultSet("RCL_GEOGRA", new GeografiaMttoRowMapper());

        jdbcObtieneJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneJefeUsuario.withSchemaName(schema);
        jdbcObtieneJefeUsuario.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcObtieneJefeUsuario.withProcedureName("SP_SEL_JEFE");
        jdbcObtieneJefeUsuario.returningResultSet("RCL_USUARIOS", new JefeSuperiorMttoRowMapper());

        jdbcUpdateJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateJefeUsuario.withSchemaName(schema);
        jdbcUpdateJefeUsuario.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcUpdateJefeUsuario.withProcedureName("SP_ACT_JEFE");
        // jdbcObtieneAsignacionCeco.returningResultSet("RCL_ASIGN", new AsignacionRowMapper());

        jdbcDeleteJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteJefeUsuario.withSchemaName(schema);
        jdbcDeleteJefeUsuario.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcDeleteJefeUsuario.withProcedureName("SP_DEL_JEFE");
        // jdbcObtieneDesbloqueaFase.returningResultSet("SP_SEL_ASIG", new AsignacionRowMapper());

        jdbcInsertaJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertaJefeUsuario.withSchemaName(schema);
        jdbcInsertaJefeUsuario.withCatalogName("PA_GEOMTTO_JEFE");
        jdbcInsertaJefeUsuario.withProcedureName("SP_INS_JEFE");

    }

    public int insertRow(GeografiaMttoDTO entityDTO) throws CustomException {
        try {

            int error = 0;
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_IDNIVEL", entityDTO.getIdNivel());
            mapSqlParameterSource.addValue("PA_NIVELPADRE", entityDTO.getIdNivelPadre());
            mapSqlParameterSource.addValue("PA_RESPONSABLE", entityDTO.getIdResponsable());
            mapSqlParameterSource.addValue("PA_OBSERVA", entityDTO.getObservaciones());
            mapSqlParameterSource.addValue("PA_AUX2", entityDTO.getDetalle());
            mapSqlParameterSource.addValue("PA_AUX", entityDTO.getStatus());

            Map<String, Object> out = jdbcInsertGografia.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Geografia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Geografia"), ex);
        }
    }

    public GeografiaMttoDTO updateRow(GeografiaMttoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_IDNIVEL", entityDTO.getIdNivel());
            mapSqlParameterSource.addValue("PA_NIVELPADRE", entityDTO.getIdNivelPadre());
            mapSqlParameterSource.addValue("PA_RESPONSABLE", entityDTO.getIdResponsable());
            mapSqlParameterSource.addValue("PA_OBSERVA", entityDTO.getObservaciones());
            mapSqlParameterSource.addValue("PA_AUX2", entityDTO.getDetalle());
            mapSqlParameterSource.addValue("PA_AUX", entityDTO.getStatus());
            mapSqlParameterSource.addValue("PA_IDGEOGRAFIA", entityDTO.getIdGeografia());

            Map<String, Object> out = jdbcUpdateGeografia.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Geografia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Geografia"), ex);
        }
    }

    public int eliminaRows(GeografiaMttoDTO entityDTO) throws CustomException {
        try {
            int error = 0;
            mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_GEOGRAFIA", entityDTO.getIdGeografia());
            //
            mapSqlParameterSource.addValue("PA_PARAM", entityDTO.getStatus());

            Map<String, Object> out = jdbcDeleteGeografia.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Geografia"));

            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Geografia"), ex);

        }
    }

//trae Geografias 
    public List<GeografiaMttoDTO> selectRowDTO(GeografiaMttoDTO entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDGEO", entityID.getIdGeografia());
        Map<String, Object> out = jdbcObtieneGeografia.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<GeografiaMttoDTO> data = (List<GeografiaMttoDTO>) out.get("RCL_GEOGRA");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public int insertJefeRow(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        try {

            int error = 0;
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
            mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getIdNivel());
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_OBSERVA", entityDTO.getObservaciones());

            Map<String, Object> out = jdbcInsertaJefeUsuario.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  JefeSuperior"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  JefeSuperior"), ex);
        }
    }

    public JefeSuperiorMttoDTO updateJefeRow(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
            mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getIdNivel());
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_OBSERVA", entityDTO.getObservaciones());

            Map<String, Object> out = jdbcUpdateJefeUsuario.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of JefeSuperior"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of JefeSuperior"), ex);
        }
    }

    public int eliminaJefeRows(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        try {
            int error = 0;
            mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());

            Map<String, Object> out = jdbcDeleteJefeUsuario.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of JefeSuperior"));

            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of JefeSuperior"), ex);

        }
    }

//trae Geografias 
    public List<JefeSuperiorMttoDTO> selectRowJefe(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
        mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
        Map<String, Object> out = jdbcObtieneJefeUsuario.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<JefeSuperiorMttoDTO> data = (List<JefeSuperiorMttoDTO>) out.get("RCL_USUARIOS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
