/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del proyectoDocumento", value = "AltaProyectoDocumento")
public class AltaProyectoDocumento {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador del proyectoDocumento", example = "1")
    private Integer id;

    public AltaProyectoDocumento(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
