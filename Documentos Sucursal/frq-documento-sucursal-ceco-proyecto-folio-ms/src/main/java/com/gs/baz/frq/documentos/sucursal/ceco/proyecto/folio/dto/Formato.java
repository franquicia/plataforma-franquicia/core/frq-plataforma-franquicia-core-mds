/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Negocio", value = "Negocio")
public class Formato extends NegocioBase {

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Identificador del formato", example = "1", position = -1)
    private Integer idFormato;

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Identificador del ceco", example = "920100", position = -2)
    private Integer idCeco;

    @JsonProperty(value = "Idnegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1", position = -3)
    private Integer idNegocio;

    @JsonProperty(value = "nombreFormato")
    @ApiModelProperty(notes = "Nombre del formato", example = "Retail", position = -4)
    private String nombreFormato;

    @JsonProperty(value = "nombreNegocio")
    @ApiModelProperty(notes = "Nombre del Negocio", example = "EKT", position = -5)
    private String nombreNegocio;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

}
