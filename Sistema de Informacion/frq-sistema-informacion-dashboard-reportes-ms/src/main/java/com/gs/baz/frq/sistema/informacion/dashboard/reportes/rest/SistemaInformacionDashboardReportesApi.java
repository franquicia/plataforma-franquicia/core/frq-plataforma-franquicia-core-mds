/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dao.SistemaInformacionDashboardReportesDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.ParametrosReporte;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Reportes;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "reportes", value = "reportes", description = "Api de reportes")
@RestController
@RequestMapping("/api-local/sistema-informacion/tablero/reportes/v1")
public class SistemaInformacionDashboardReportesApi {

    @Autowired
    private SistemaInformacionDashboardReportesDAOImpl sistemaInformacionDashboardReportesDAOImpl;

    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idDisciplina
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene los negocios de una disciplina", notes = "Obtiene los negocios de una disciplina", nickname = "obtieneNegocios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/filtros/negocios/busquedas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneNegocios(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina) throws CustomException, DataNotFoundException {
        System.out.println("idDisciplina: " + idDisciplina);
        Reportes reportes = new Reportes();
        reportes.setNegocios(sistemaInformacionDashboardReportesDAOImpl.selectNegocios(idDisciplina));
        if (reportes.getNegocios() != null && reportes.getNegocios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     *
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene los niveles geograficos", notes = "Obtiene los niveles geograficos", nickname = "obtieneNivelesGeograficos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/filtros/geografia/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneNivelesGeograficos(@ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        reportes.setNivelesGeograficos(sistemaInformacionDashboardReportesDAOImpl.selectNivelesGeograficos(parametrosReporte));
        if (reportes.getNivelesGeograficos() != null && reportes.getNivelesGeograficos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene disciplinas", notes = "Obtiene todas las disciplinas", nickname = "obtieneDisciplinas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/busquedas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneDisciplinas() throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        reportes.setDisciplinas(sistemaInformacionDashboardReportesDAOImpl.selectDisciplinas());
        if (reportes.getDisciplinas() != null && reportes.getDisciplinas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     * @param idDisciplina
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene disciplinas", notes = "Obtiene todas las disciplinas", nickname = "obtieneDisciplinas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/negocios/busquedas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneDisciplinas(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina) throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        reportes.setNegociosDisciplina(sistemaInformacionDashboardReportesDAOImpl.selectNegociosDisciplina(idDisciplina));
        if (reportes.getNegociosDisciplina() != null && reportes.getNegociosDisciplina().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     *
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene calificaciones promedio", notes = "Obtiene todas las calificaciones promedio", nickname = "obtieneCalificacionesPromedio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/calificacion-promedio/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneCalificacionesPromedio(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reportes.setCalificacionesPromedio(sistemaInformacionDashboardReportesDAOImpl.selectCalificacionPromedioGenerico(parametrosReporte));
        if (reportes.getCalificacionesPromedio() != null && reportes.getCalificacionesPromedio().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene los puntos de venta aprobados", notes = "Obtiene todos los puntos de venta aprobados y el porcentaje correspondiente.", nickname = "obtienePuntosVentaAprobados")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/porcentaje-puntos-venta-aprobados/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtienePuntosVentaAprobados(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reportes.setPorcentajePuntosVentaAprobados(sistemaInformacionDashboardReportesDAOImpl.selectPorcentajePuntosVentaAprobados(parametrosReporte));
        if (reportes.getPorcentajePuntosVentaAprobados() != null && reportes.getPorcentajePuntosVentaAprobados().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene el niveles de cumplimiento", notes = "Obtiene el niveles de cumplimiento de los puntos de venta evaluados.", nickname = "obtieneNivelesCumplimiento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/niveles-cumplimiento/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneNivelesCumplimiento(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setNivelCumplimientoTotal(sistemaInformacionDashboardReportesDAOImpl.selectNivelesCumplimiento(parametrosReporte));
        if (reporte.getNivelCumplimientoTotal().getNivelesCumplimiento() != null && reporte.getNivelCumplimientoTotal().getNivelesCumplimiento().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene calificaciones por geografia", notes = "Obtiene calificaciones por geografia.", nickname = "obtieneCalificacionesGeografia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/calificaciones-geografia/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneCalificacionesGeografia(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setCalificacionesGeografia(sistemaInformacionDashboardReportesDAOImpl.selectCalificacionesGeografia(parametrosReporte));
        if (reporte.getCalificacionesGeografia() != null && reporte.getCalificacionesGeografia().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene puntos de venta aprobados por geografia", notes = "Obtiene puntos de venta aprobados porgeografia.", nickname = "obtienePuntosVentaAprobadosGeografia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/puntos-venta-aprobados-geografia/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtienePuntosVentaAprobadosGeografia(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setPuntosVentaAprobadosGeografia(sistemaInformacionDashboardReportesDAOImpl.selectPuntosVentaAprobadosGeografia(parametrosReporte));
        if (reporte.getPuntosVentaAprobadosGeografia() != null && reporte.getPuntosVentaAprobadosGeografia().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene top de incumplimientos", notes = "Obtiene top de incumplimientos.", nickname = "obtieneIncumplimientos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/incumplimientos/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneIncumplimientos(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setIncumplimientos(sistemaInformacionDashboardReportesDAOImpl.selectIncumplimientos(parametrosReporte));
        if (reporte.getIncumplimientos() != null && reporte.getIncumplimientos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene imperdonables", notes = "Obtiene imperdonables.", nickname = "obtieneImperdonables")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/generico/imperdonables/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneImperdonables(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setImperdonableTotal(sistemaInformacionDashboardReportesDAOImpl.selectImperdonables(parametrosReporte));
        if (reporte.getImperdonableTotal().getImperdonables() != null && reporte.getImperdonableTotal().getImperdonables().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     *
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene calificaciones promedio red unica", notes = "Obtiene todas las calificaciones promedio red unica", nickname = "obtieneCalificacionesPromedioRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/calificacion-promedio/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneCalificacionesPromedioRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reportes.setCalificacionesPromedio(sistemaInformacionDashboardReportesDAOImpl.selectCalificacionPromedioGenerico(parametrosReporte));
        if (reportes.getCalificacionesPromedio() != null && reportes.getCalificacionesPromedio().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene los puntos de venta aprobados red unica", notes = "Obtiene todos los puntos de venta aprobados y el porcentaje correspondiente red unica.", nickname = "obtienePuntosVentaAprobadosRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/porcentaje-puntos-venta-aprobados/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtienePuntosVentaAprobadosRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reportes = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reportes.setPorcentajePuntosVentaAprobados(sistemaInformacionDashboardReportesDAOImpl.selectPorcentajePuntosVentaAprobados(parametrosReporte));
        if (reportes.getPorcentajePuntosVentaAprobados() != null && reportes.getPorcentajePuntosVentaAprobados().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reportes;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene el niveles de cumplimiento red unica", notes = "Obtiene el niveles de cumplimiento de los puntos de venta evaluados red unica.", nickname = "obtieneNivelesCumplimientoRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/niveles-cumplimiento/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneNivelesCumplimientoRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setNivelCumplimientoTotal(sistemaInformacionDashboardReportesDAOImpl.selectNivelesCumplimiento(parametrosReporte));
        if (reporte.getNivelCumplimientoTotal().getNivelesCumplimiento() != null && reporte.getNivelCumplimientoTotal().getNivelesCumplimiento().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene calificaciones por geografia red unica", notes = "Obtiene calificaciones por geografia red unica.", nickname = "obtieneCalificacionesGeografiaRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/calificaciones-geografia/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneCalificacionesGeografiaRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setCalificacionGeografiaRedUnica(sistemaInformacionDashboardReportesDAOImpl.selectCalificacionesGeografiaRedUnica(parametrosReporte));
        if (reporte.getCalificacionGeografiaRedUnica().getCalificacionesGeografia() != null && reporte.getCalificacionGeografiaRedUnica().getCalificacionesGeografia().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene puntos de venta aprobados por geografia red unica", notes = "Obtiene puntos de venta aprobados porgeografia red unica.", nickname = "obtienePuntosVentaAprobadosGeografiaRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/puntos-venta-aprobados-geografia/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtienePuntosVentaAprobadosGeografiaRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setPuntoVentaAprobadoGeografiaRedUnica(sistemaInformacionDashboardReportesDAOImpl.selectPuntosVentaAprobadosGeografiaRedUnica(parametrosReporte));
        if (reporte.getPuntoVentaAprobadoGeografiaRedUnica().getPuntosVentaAprobadosGeografia() != null && reporte.getPuntoVentaAprobadoGeografiaRedUnica().getPuntosVentaAprobadosGeografia().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene incumplimientos negocios red unica", notes = "Obtiene incumplimientos negocios red unica.", nickname = "obtieneIncumplimientosNegocioRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/negocios/incumplimientos/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneIncumplimientosNegocioRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setIncumplimientosNegocioRedUnica(sistemaInformacionDashboardReportesDAOImpl.selectIncumplimientosNegocioRedUnica(parametrosReporte));
        if (reporte.getIncumplimientosNegocioRedUnica() != null && reporte.getIncumplimientosNegocioRedUnica().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene negocios de incumplimientos red unica", notes = "Obtiene negocios de incumplimientos red unica.", nickname = "obtieneNegociosIncumplimientoRedUnica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/filtros/negocios/incumplimientos/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneNegociosIncumplimientoRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setNegociosIncumplimiento(sistemaInformacionDashboardReportesDAOImpl.selectNegociosIncumplimientos(parametrosReporte));
        if (reporte.getNegociosIncumplimiento() != null && reporte.getNegociosIncumplimiento().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene incumplimientos por seccion responsable", notes = "Obtiene incumplimientos por seccion responsable.", nickname = "obtieneIncumplimientosSeccionResponsable")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/secciones/responsable/incumplimientos/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneIncumplimientosSeccionResponsable(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setIncumplimientosSeccionResponsableRedUnica(sistemaInformacionDashboardReportesDAOImpl.selectIncumplimientosSeccionResponsableRedUnica(parametrosReporte));
        if (reporte.getIncumplimientosSeccionResponsableRedUnica() != null && reporte.getIncumplimientosSeccionResponsableRedUnica().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene incumplimientos por seccion responsable", notes = "Obtiene incumplimientos por seccion responsable.", nickname = "obtieneIncumplimientosSeccionResponsable")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/filtros/secciones/incumplimientos/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneSeccionesIncumplimientosRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setSeccionesIncumplimiento(sistemaInformacionDashboardReportesDAOImpl.selectSeccionesIncumplimiento(parametrosReporte));
        if (reporte.getSeccionesIncumplimiento() != null && reporte.getSeccionesIncumplimiento().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }

    /**
     * @param idDisciplina
     * @param parametrosReporte
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene incumplimientos por seccion responsable", notes = "Obtiene incumplimientos por seccion responsable.", nickname = "obtieneIncumplimientosSeccionResponsable")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disciplinas/{idDisciplina}/red-unica/filtros/responsables/incumplimientos/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reportes obtieneResponsablesIncumplimientosRedUnica(@ApiParam(name = "idDisciplina", value = "Identificador de la disciplina", example = "1") @PathVariable Integer idDisciplina,
            @ApiParam(name = "ParametrosReporte", value = "Paramentros para la consulta de datos", required = true) @RequestBody ParametrosReporte parametrosReporte) throws CustomException, DataNotFoundException {
        Reportes reporte = new Reportes();
        parametrosReporte.setIdDisciplina(idDisciplina);
        reporte.setResponsablesIncumplimiento(sistemaInformacionDashboardReportesDAOImpl.selectResponsablesIncumplimiento(parametrosReporte));
        if (reporte.getResponsablesIncumplimiento() != null && reporte.getResponsablesIncumplimiento().isEmpty()) {
            throw new DataNotFoundException();
        }
        return reporte;
    }
}
