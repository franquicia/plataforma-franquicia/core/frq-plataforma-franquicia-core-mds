/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g012;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class IncumplimientosImperdonablesDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocios")
    private List<NegocioDTO> negocios;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public List<NegocioDTO> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<NegocioDTO> negocios) {
        this.negocios = negocios;
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "IncumplimientoImperdonablesDTO{" + "negocios=" + negocios + ", filtros=" + filtros + '}';
    }

}
