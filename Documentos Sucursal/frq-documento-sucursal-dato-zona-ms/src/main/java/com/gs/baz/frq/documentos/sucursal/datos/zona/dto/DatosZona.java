/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de Datos Zona", value = "datosZona")
public class DatosZona {

    @JsonProperty(value = "datosZona")
    @ApiModelProperty(notes = "datos Zona")
    private List<Datozona> datosZona;

    public DatosZona(List<Datozona> datosZona) {
        this.datosZona = datosZona;
    }

    public List<Datozona> getDatosDatozona() {
        return datosZona;
    }

    public void setDatosDatozona(List<Datozona> datosZona) {
        this.datosZona = datosZona;
    }

}
