/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.respuesta.reporte.medicion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author jlfr
 */
public class RespuestaReporteMedicionDTO {

    @JsonProperty(value = "id_dashboard")
    private Integer idDashboard;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_pregunta")
    private Integer idPregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "respuesta")
    private String respuesta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_config_dis")
    private Integer idConfigDis;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ponderacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "imperdonable")
    private Integer imperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "usuario_mod")
    private String usuarioMod;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_mod")
    private String fechaMod;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numSerie")
    private Integer numSerie;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public RespuestaReporteMedicionDTO() {
    }

    public Integer getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(BigDecimal idDashboard) {
        this.idDashboard = (idDashboard == null ? null : idDashboard.intValue());
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(BigDecimal idPregunta) {
        this.idPregunta = (idPregunta == null ? null : idPregunta.intValue());
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public Integer getImperdonable() {
        return imperdonable;
    }

    public void setImperdonable(BigDecimal imperdonable) {
        this.imperdonable = (imperdonable == null ? null : imperdonable.intValue());
    }

    public Integer getIdConfigDis() {
        return idConfigDis;
    }

    public void setIdConfigDis(BigDecimal idConfigDis) {
        this.idConfigDis = (idConfigDis == null ? null : idConfigDis.intValue());
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(BigDecimal numSerie) {
        this.numSerie = (numSerie == null ? null : numSerie.intValue());
    }

    public String getUsuarioMod() {
        return usuarioMod;
    }

    public void setUsuarioMod(String usuarioMod) {
        this.usuarioMod = usuarioMod;
    }

    public String getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(String fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "RespuestaReporteMedicionDTO{" + "idDashboard=" + idDashboard + ", idPregunta=" + idPregunta + ", respuesta=" + respuesta + ", idProtocolo=" + idProtocolo + ", idConfigDis=" + idConfigDis + ", ponderacion=" + ponderacion + ", modulo=" + modulo + ", usuarioMod=" + usuarioMod + ", fechaMod=" + fechaMod + ", numSerie=" + numSerie + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
