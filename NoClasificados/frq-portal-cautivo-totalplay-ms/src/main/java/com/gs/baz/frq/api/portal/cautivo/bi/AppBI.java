/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.bi;

import com.gs.baz.frq.api.portal.cautivo.dao.ClienteDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dto.AppDTO;
import com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente.ECServicioApp;
import com.gs.baz.frq.api.portal.cautivo.util.Util;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author MADA
 */
public class AppBI {

    @Autowired
    ECServicioApp ecServicioClientes;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    ClienteDAOImpl clienteDAOImpl;

    private final Logger logger = LogManager.getLogger();

    public Boolean insertarClientesEC(String tipo, String fechaInicio, String fechaFin, String noEco) throws CustomException {

        ecServicioClientes.init();
        // Parametros parametros = new Parametros();

        // List<FechasParametrosDTO> fechasIntervalos = parametros.getFechasParametros(fecha);
        final long startInsercion = System.currentTimeMillis();

        // for (int index = 0; index < fechasIntervalos.size(); index++) {
        // String fechaInicio = fechasIntervalos.get(index).getFechaInicio();
        //String fechaFin = fechasIntervalos.get(index).getFechaFin();
        //  int paginas = obtenerNumeroPaginas(tipo, Util.convertCTSDateToUTCDate(fechaInicio), Util.convertCTSDateToUTCDate(fechaFin));
        // logger.debug("Paginas = " + paginas + " & " + " Fecha Inicio = " + fechaInicio + " & " + " FechaFin = " + fechaFin);
        //Como el servicio esta segmentado para el consumo, se hace la petición por cada página
        //for (int i = 0; i <= paginas; i++) {
        final long startConsumo = System.currentTimeMillis();
        logger.debug("Consumo de Servicio ecServicioClientes.getListaClientes ? idPropietario = " + "&" + "page = " + "&" + "pageSize  = " + " & paginas = " + " & fechaInicio = " + fechaInicio + " & fechaFin = " + fechaFin, noEco);
        List<AppDTO> clientes = ecServicioClientes.getListaClientes(tipo, Util.convertCTSDateToUTCDate(fechaInicio), Util.convertCTSDateToUTCDate(fechaFin), noEco);

        if (clientes != null) {
            for (AppDTO cliente : clientes) {
                if (cliente.getMac() != null && !"".equals(cliente.getMac())) {
                    cliente.setMac(cliente.getMac().replace(":", ""));
                    clienteDAOImpl.insertRow((cliente));
                }
            }
        }
        logger.debug("Insercion page = " + (System.currentTimeMillis() - startConsumo));
        // }
        //}

        logger.debug("Insercion completa ===== ", (System.currentTimeMillis() - startInsercion));

        return true;
    }

    /*

    public ClienteDTO cambiarFecha(ClienteDTO cliente) throws CustomException {
        String fechaStart = cliente.getSessionStart();
        if (fechaStart != null) {
            fechaStart = Util.convertUTCDateToCTSDate(fechaStart);
            cliente.setSessionStart(fechaStart);
        }

        String fechaEnd = cliente.getSessionEnd();
        if (fechaEnd != null) {
            fechaEnd = Util.convertUTCDateToCTSDate(fechaEnd);
            cliente.setSessionEnd(fechaEnd);
        }
        return cliente;
    }
/*
    public int obtenerNumeroPaginas(Long idPropietario, String fechaInicio, String fechaFin) throws CustomException {
        Parametros parametros = new Parametros();
        //Se obtiene el paginado para calcular el número de peticiones (paginas)
        PaginacionDTO paginacionDTO = ecServicioClientes.getPaginacionClientes(idPropietario, 1, 1, fechaInicio, fechaFin);
        int paginas = 0;

        if (paginacionDTO != null) {
            paginas = paginacionDTO.getTotalCount() / parametros.getCantidadElementos();
        }

        if (paginas == 0) {
            paginas++;
        }
        return paginas;
    }

     */
}
