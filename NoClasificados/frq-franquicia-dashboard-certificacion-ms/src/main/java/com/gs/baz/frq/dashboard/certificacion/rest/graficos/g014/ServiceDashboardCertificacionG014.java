/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g014;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g014.DashboardCertificacionG014DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g014.DashboardCertificacionG014DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g014")
public class ServiceDashboardCertificacionG014 {

    @Autowired
    private DashboardCertificacionG014DAOImpl dashboardCertificacionG014DAOImpl;

    @RequestMapping(value = "secure/grafico/get/certificaciones/previas/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG014DTO> getCertiPreviasG014(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "idCecoBase", required = false) Long idCecoBase,
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFinal", required = true) String fechaFinal,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario,
            @RequestParam(value = "idInvitado", required = false) Long idInvitado) throws CustomException {

        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFinal == null || fechaFinal.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFinal"));
        } else {
            return dashboardCertificacionG014DAOImpl.selectRowDashboardCertificacionG014(idCeco, idCecoBase, fechaInicial, fechaFinal, idUsuario, idInvitado);
        }
    }

}
