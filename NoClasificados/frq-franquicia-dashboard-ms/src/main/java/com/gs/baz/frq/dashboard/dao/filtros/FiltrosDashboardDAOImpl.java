package com.gs.baz.frq.dashboard.dao.filtros;

import com.gs.baz.frq.dashboard.dao.DashboardDAOImpl;
import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroAnioDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroCecoDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroNegocioDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroPeriodoDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.DisciplinaDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.filtro.FiltroAnioRowMapper;
import com.gs.baz.frq.dashboard.mprs.filtro.FiltroCecoRowMapper;
import com.gs.baz.frq.dashboard.mprs.filtro.FiltroNegocioRowMapper;
import com.gs.baz.frq.dashboard.mprs.filtro.FiltroPeriodoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class FiltrosDashboardDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectAnio;
    private DefaultJdbcCall jdbcSelectPeriodo;
    private DefaultJdbcCall jdbcSelectNegocio;
    private DefaultJdbcCall jdbcSelectCeco;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    @Qualifier("dashboardDAOImpl")
    DashboardDAOImpl dashboardDaoImpl;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectAnio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAnio.withSchemaName(schema);
        jdbcSelectAnio.withCatalogName("PAADMINFILTRODASH");
        jdbcSelectAnio.withProcedureName("SP_GET_ANIO");
        jdbcSelectAnio.returningResultSet("RCL_INFO", new FiltroAnioRowMapper());

        jdbcSelectPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectPeriodo.withSchemaName(schema);
        jdbcSelectPeriodo.withCatalogName("PAADMINFILTRODASH");
        jdbcSelectPeriodo.withProcedureName("SP_GET_PERIODO");
        jdbcSelectPeriodo.returningResultSet("RCL_INFO", new FiltroPeriodoRowMapper());

        jdbcSelectNegocio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegocio.withSchemaName(schema);
        jdbcSelectNegocio.withCatalogName("PAADMINFILTRODASH");
        jdbcSelectNegocio.withProcedureName("SP_GET_NEGOCIO");
        jdbcSelectNegocio.returningResultSet("RCL_INFO", new FiltroNegocioRowMapper());

        jdbcSelectCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCeco.withSchemaName(schema);
        jdbcSelectCeco.withCatalogName("PAADMINFILTRODASH");
        jdbcSelectCeco.withProcedureName("SP_GETXNIVEL");
        jdbcSelectCeco.returningResultSet("RCL_INFO", new FiltroCecoRowMapper());
    }

    public List<FiltroAnioDTO> selectRowsAnio(Long idDisciplina) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISC", idDisciplina);
        Map<String, Object> out = jdbcSelectAnio.execute(mapSqlParameterSource);
        return (List<FiltroAnioDTO>) out.get("RCL_INFO");
    }

    public List<FiltrosDTO> selectRowsPeriodo(FiltrosDTO entityDTO) throws CustomException {
        List<FiltroPeriodoDTO> periodos = new ArrayList<>();
        List<FiltroPeriodoDTO> ABorrarPeriodos = new ArrayList<>();
        List<FiltrosDTO> retorno = new ArrayList<>();
        int cont = 0;
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityDTO.getIdFrecuenciaMedicion());
        mapSqlParameterSource.addValue("PA_ANIO", entityDTO.getAnio());
        Map<String, Object> out = jdbcSelectPeriodo.execute(mapSqlParameterSource);
        //YA QUE TENGO LA LISTA DE DATOS DEBO VALIDAR SI EXISTEN CONTEO PARA ESOS POR LA DISCIPLINA

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;
        if (success) {
            periodos = (List<FiltroPeriodoDTO>) out.get("RCL_INFO");
            for (FiltroPeriodoDTO per : periodos) {

                entityDTO.setNumeroPeriodo(per.getNumero());
                DisciplinaDTO disc = new DisciplinaDTO();
                disc.setIdDisciplina(new BigDecimal(entityDTO.getIdDisciplina()));
                disc.setIdFrecuenciaMedicion(new BigDecimal(entityDTO.getIdFrecuenciaMedicion()));
                disc.setFiltros(entityDTO);
                FechasDTO fechas = periodoDAOImpl.getFechas(entityDTO);

                if (dashboardDaoImpl.selectRowConteoDatos(disc, fechas).equals(0)) {
                    ABorrarPeriodos.add(per);
                }

            }

            for (FiltroPeriodoDTO borr : ABorrarPeriodos) {
                periodos.removeIf(p -> Objects.equals(p.getNumero(), borr.getNumero()));
            }
            for (FiltroPeriodoDTO fin : periodos) {
                FiltrosDTO fil = new FiltrosDTO();
                fil.setNumeroPeriodo(fin.getNumero());
                fil.setDescripcion(fin.getDescripcion());
                retorno.add(fil);

            }

        } else {
            throw new CustomException(ModelCodes.ATTRIBUTE_DATA_NOT_FOUND.ERROR_TO_UPDATE_DATA.detalle("Error not success to get row of selectRowsPeriodo"));
        }

        return retorno;
    }

    public List<FiltroNegocioDTO> selectRowsNegocios(Long idDisciplina) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        Long bandera = 0L;
        if (idDisciplina == 5) {
            bandera = 1L;
        }
        if (idDisciplina == 18) {
            bandera = 2L;
        }
        mapSqlParameterSource.addValue("PA_IDBANDERA", bandera);
        Map<String, Object> out = jdbcSelectNegocio.execute(mapSqlParameterSource);
        return (List<FiltroNegocioDTO>) out.get("RCL_INFO");
    }

    public List<FiltroCecoDTO> selectRowsNivelGeograficoHijos(FiltroCecoDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        // primero obtener las fechas de inicio y fin
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDNIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_IDNEGOCIO", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CADENA", filtros.getDescripcion());
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());

        Map<String, Object> out = jdbcSelectCeco.execute(mapSqlParameterSource);
        return (List<FiltroCecoDTO>) out.get("RCL_INFO");
    }

}
