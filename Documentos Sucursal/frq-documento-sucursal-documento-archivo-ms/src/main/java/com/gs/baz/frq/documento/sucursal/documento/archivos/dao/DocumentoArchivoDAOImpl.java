/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.DocumentoFolio;
import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.ParametrosDocumentoFolio;
import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.SinResultado;
import com.gs.baz.frq.documento.sucursal.documento.archivos.mprs.DocumentoFolioRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoArchivoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PA_ARCHIVODOC";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_DOCFOLIO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_DOCFOLIO");

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_DOCFOLIO");
        jdbcSelectAll.returningResultSet("RCL_ARCH", new DocumentoFolioRowMapper());

    }

    public List<DocumentoFolio> selectRow(Long idDocumento, Long idFolio) throws CustomException {

        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_DOCUMENTO", idDocumento);
        mapSqlParameterSource.addValue("PA_IDFOLIO", idDocumento);

        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<DocumentoFolio> data = (List<DocumentoFolio>) out.get("RCL_NEGO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public SinResultado insertRow(ParametrosDocumentoFolio entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_FOLIO", entityDTO.getFolio());
            mapSqlParameterSource.addValue("PA_IDDOCUMENTO", entityDTO.getIdDocumento());
            mapSqlParameterSource.addValue("PA_IDARCHIVO", entityDTO.getIdArchivo());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -2) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                return new SinResultado();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(ParametrosDocumentoFolio entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FOLIO", entityDTO.getFolio());
            mapSqlParameterSource.addValue("PA_IDDOCUMENTO", entityDTO.getIdDocumento());
            mapSqlParameterSource.addValue("PA_IDARCHIVO", entityDTO.getIdArchivo());

            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);

            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -2) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
