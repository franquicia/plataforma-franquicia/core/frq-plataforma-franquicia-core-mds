/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NegocioDisciplina;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegociosDisciplinaRowMapper implements RowMapper<NegocioDisciplina> {

    @Override
    public NegocioDisciplina mapRow(ResultSet rs, int rowNum) throws SQLException {
        NegocioDisciplina negocio = new NegocioDisciplina();
        negocio.setIdNegocio(rs.getInt("FINEGOCIODISCID"));
        negocio.setCalificacion(rs.getInt("CALIFICACION"));
        return negocio;
    }
}
