/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Detalle de las Aprobaciones", value = "RespuestaAprobaciones")
public class RespuestaAprobacionesDTO {

    @JsonProperty(value = "estadoAprobacion")
    @ApiModelProperty(notes = "Estado de la aprobación", example = "Activo")
    private String estadoAprobacion;

    @JsonProperty(value = "fechaAprobacion")
    @ApiModelProperty(notes = "Fecha en que se realizó la aprobación", example = "0001-01-01 00:00:00")
    private String fecha;

    @JsonProperty(value = "justificacionAprobacion")
    @ApiModelProperty(notes = "Justificación de la aprobación", example = "Se realiza la aprobación correctamente.")
    private String justificacionAprobacion;

    @JsonProperty(value = "primerArchivoEvidencia")
    @ApiModelProperty(notes = "Archivo Uno para la aprobación", example = "<Archivo Generado en base64>")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String archivo1Base64;

    @JsonProperty(value = "segundoArchivoEvidencia")
    @ApiModelProperty(notes = "Archivo Dos para la aprobación", example = "<Archivo Generado en base64>")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String archivo2Base64;

    public RespuestaAprobacionesDTO() {
        super();

    }

    public String getArchivo1Base64() {
        return archivo1Base64;
    }

    public void setArchivo1Base64(String archivo1Base64) {
        this.archivo1Base64 = archivo1Base64;
    }

    public String getArchivo2Base64() {
        return archivo2Base64;
    }

    public void setArchivo2Base64(String archivo2Base64) {
        this.archivo2Base64 = archivo2Base64;
    }

    public String getJustificacionAprobacion() {
        return justificacionAprobacion;
    }

    public void setJustificacionAprobacion(String justificacionAprobacion) {
        this.justificacionAprobacion = justificacionAprobacion;
    }

    public String getEstadoAprobacion() {
        return estadoAprobacion;
    }

    public void setEstadoAprobacion(String estadoAprobacion) {
        this.estadoAprobacion = estadoAprobacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "RespuestaAprobacionesDTO{" + "estadoAprobacion=" + estadoAprobacion
                + ", fecha=" + fecha + ", justificacionAprobacion=" + justificacionAprobacion
                + ", archivo1Base64=" + archivo1Base64 + ", archivo2Base64=" + archivo2Base64 + '}';
    }

}
