/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.perfiles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de perfiles", value = "Perfiles")
public class Perfiles {

    @JsonProperty(value = "perfiles")
    @ApiModelProperty(notes = "perfiles")
    private List<Perfil> perfiles;

    public Perfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public List<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    @Override
    public String toString() {
        return "Perfiles{" + "perfiles=" + perfiles + '}';
    }

}
