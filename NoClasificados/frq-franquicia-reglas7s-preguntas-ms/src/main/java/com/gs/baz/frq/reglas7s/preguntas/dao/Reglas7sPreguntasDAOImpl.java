package com.gs.baz.frq.reglas7s.preguntas.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.reglas7s.preguntas.dao.util.GenericDAO;
import com.gs.baz.frq.reglas7s.preguntas.dto.Reglas7sPreguntasDTO;
import com.gs.baz.frq.reglas7s.preguntas.mprs.Reglas7sPreguntasRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class Reglas7sPreguntasDAOImpl extends DefaultDAO implements GenericDAO<Reglas7sPreguntasDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectReglas7sPreguntas;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMRELREGLAS");
        jdbcInsert.withProcedureName("SPINSRELREGLAS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMRELREGLAS");
        jdbcUpdate.withProcedureName("SPACTRELREGLAS");

        jdbcSelectReglas7sPreguntas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectReglas7sPreguntas.withSchemaName(schema);
        jdbcSelectReglas7sPreguntas.withCatalogName("PAADMRELREGLAS");
        jdbcSelectReglas7sPreguntas.withProcedureName("SPGETRELREGLAS");
        jdbcSelectReglas7sPreguntas.returningResultSet("PA_CDATOS", new Reglas7sPreguntasRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMRELREGLAS");
        jdbcSelectByName.withProcedureName("SPGETRELREGLAS");
        jdbcSelectByName.returningResultSet("PA_CDATOS", new Reglas7sPreguntasRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMRELREGLAS");
        jdbcDelete.withProcedureName("SPDELRELREGLAS");
    }

    @Override
    public Reglas7sPreguntasDTO insertRow(Reglas7sPreguntasDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIPREGUNTACORRE", entityDTO.getIdPreguntaCorresponde());
            mapSqlParameterSource.addValue("PA_FIPROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FICLAVE_ID", entityDTO.getIdClave());;
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Reglas7sPreguntas"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Reglas7sPreguntas"), ex);
        }
    }

    @Override
    public Reglas7sPreguntasDTO updateRow(Reglas7sPreguntasDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICLAVE_ID", entityDTO.getIdClave());
            mapSqlParameterSource.addValue("PA_FIPREGUNTACORRE", entityDTO.getIdPreguntaCorresponde());
            mapSqlParameterSource.addValue("PA_FIPROTOCOLO", entityDTO.getIdProtocolo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Reglas7sPreguntas"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Reglas7sPreguntas"), ex);
        }
    }

    @Override
    public List<Reglas7sPreguntasDTO> selectRows(Reglas7sPreguntasDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FICLAVE_ID", null);
        mapSqlParameterSource.addValue("PA_FIPREGUNTACORRE", null);
        mapSqlParameterSource.addValue("PA_FIPROTOCOLO", null);
        Map<String, Object> out = jdbcSelectReglas7sPreguntas.execute(mapSqlParameterSource);
        return (List<Reglas7sPreguntasDTO>) out.get("PA_CDATOS");
    }

    @Override
    public Reglas7sPreguntasDTO selectRowsParametros(Reglas7sPreguntasDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FICLAVE_ID", entityDTO.getIdClave());
        mapSqlParameterSource.addValue("PA_FIPREGUNTACORRE", entityDTO.getIdPreguntaCorresponde());
        mapSqlParameterSource.addValue("PA_FIPROTOCOLO", entityDTO.getIdProtocolo());
        Map<String, Object> out = jdbcSelectReglas7sPreguntas.execute(mapSqlParameterSource);
        List<Reglas7sPreguntasDTO> data = (List<Reglas7sPreguntasDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Reglas7sPreguntasDTO deleteRow(Reglas7sPreguntasDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIPREGUNTACORRE", entityDTO.getIdPreguntaCorresponde());
            mapSqlParameterSource.addValue("PA_FIPROTOCOLO", entityDTO.getIdProtocolo());;
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Reglas7sPreguntas"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Reglas7sPreguntas"), ex);
        }
    }

}
