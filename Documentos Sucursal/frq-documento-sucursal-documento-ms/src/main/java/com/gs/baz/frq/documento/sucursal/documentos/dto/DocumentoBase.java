/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de documento", value = "DocumentoBase")
public class DocumentoBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del documento", example = "EKT")
    private String descripcion;

    @JsonProperty(value = "estadoHistorico")
    @ApiModelProperty(notes = "Estado de historico del documento", example = "1")
    private Integer estadoHistorico;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstadoHistorico() {
        return estadoHistorico;
    }

    public void setEstadoHistorico(Integer estadoHistorico) {
        this.estadoHistorico = estadoHistorico;
    }
}
