/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class Empleado {

    @JsonProperty(value = "numeroEmpleado")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Número del empleado", example = "202622")
    private Integer numeroEmpleado;

    @JsonProperty(value = "nombre")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Nombre del empleado", example = "Carlos Antonio Escobar Hernandez")
    private String nombre;

    @JsonProperty(value = "correo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "correo del empleado", example = "cescobarh@ekeltra.com.mx")
    private String correo;

    public Empleado() {
    }

    public Empleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Empleado(String nombre) {
        this.nombre = nombre;
    }

    public Empleado(Integer numeroEmpleado, String nombre) {
        this.numeroEmpleado = numeroEmpleado;
        this.nombre = nombre;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "numeroEmpleado=" + numeroEmpleado + ", nombre=" + nombre + ", correo=" + correo + '}';
    }
}
