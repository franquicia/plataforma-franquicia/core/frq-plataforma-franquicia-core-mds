package com.gs.baz.autorization.server.init;

import com.gs.baz.autorization.server.token.InfoAdicionalToken;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@RefreshScope
@Configuration
@EnableAuthorizationServer
public class ConfigAuthorizationServerAdapter extends AuthorizationServerConfigurerAdapter {

    private final Logger logger = LogManager.getLogger();

    public ConfigAuthorizationServerAdapter() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Autowired
    private Environment env;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private InfoAdicionalToken infoAdicionalToken;

    /**
     *
     * @param security
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }

    /**
     *
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        int hourSecond = (int) Math.pow(60, 2);//1 Horas
        int secondsExpireAccessToken = hourSecond * 2; //2 Horas
        int secondsExpireRefreshToken = hourSecond * 24; //24 Horas
        final String clientID = env.getProperty("config.security.oauth.client.id");
        final String clientSecret = env.getProperty("config.security.oauth.client.secret");
        clients.inMemory().withClient(clientID).secret(passwordEncoder.encode(clientSecret)).scopes("read", "write").authorizedGrantTypes("client_credentials", "password", "refresh_token", "authorization_code").accessTokenValiditySeconds(secondsExpireAccessToken).refreshTokenValiditySeconds(secondsExpireRefreshToken);
    }

    /**
     *
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        JwtAccessTokenConverter jwtAccessTokenConverter = accessTokenConverter();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(infoAdicionalToken, jwtAccessTokenConverter));
        endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore()).accessTokenConverter(accessTokenConverter()).tokenEnhancer(tokenEnhancerChain);
    }

    /**
     *
     * @return
     */
    @Bean
    public JwtTokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    /**
     *
     * @return
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        String jwtKey = env.getProperty("config.security.oauth.jwt.key");
        JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
        tokenConverter.setSigningKey(jwtKey);
        return tokenConverter;
    }

}
