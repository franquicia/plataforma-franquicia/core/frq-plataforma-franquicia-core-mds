/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.handler.reqres.bi;

import com.gs.baz.frq.model.commons.ErrorMessageDTO;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorClient;
import com.gs.baz.frq.swagger.responses.RespuestaErrorServer;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@RestController
public class CustomGlobalErrorHandler implements ErrorController {

    private final String path = "/error";
    private ResponseEntity responseEntity;
    private ResponseEntity.BodyBuilder bodyBuilder;
    private Integer statusCode;
    private String contextPath;

    @RequestMapping(value = path)
    @ResponseBody
    public ResponseEntity handleError(HttpServletRequest httpServletRequest) {
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        contextPath = (String) httpServletRequest.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
        statusCode = (Integer) httpServletRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (contextPath.contains("/service/")) {
            ErrorMessageDTO errorMessage = new ErrorMessageDTO();
            errorMessage.setStatusCode(statusCode);
            errorMessage.setMessage(HttpStatus.resolve(statusCode).toString());
            errorMessage.setTimestamp(new Date());
            String[] extension = contextPath.split("\\.");
            if (extension.length > 1) {
                errorMessage.setDetails("Resource not found");
            } else {
                errorMessage.setDetails("Service not registered");
            }
            bodyBuilder = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON);
            responseEntity = bodyBuilder.body(errorMessage);
        } else {
            if (statusCode != null) {
                RespuestaAPI errorMessage = null;
                ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
                if (statusCode >= 400 && statusCode <= 499) {
                    String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
                    errorMessage = new RespuestaErrorClient(codeError, responseMessage.getMessage(), requestID);
                }
                if (statusCode >= 500 && statusCode <= 599) {
                    String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
                    errorMessage = new RespuestaErrorServer(codeError, responseMessage.getMessage(), requestID);
                }
                bodyBuilder = ResponseEntity.status(responseMessage.getCode()).contentType(MediaType.APPLICATION_JSON);
                responseEntity = bodyBuilder.body(errorMessage);
            }
        }
        return responseEntity;
    }

    @Override
    public String getErrorPath() {
        return path;
    }
}
