package com.gs.baz.frq.validador.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.validador.filters.RequestValidatiorFilter;
import com.gs.baz.frq.validador.filters.RequestValidatiorAdditionalFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.validador")
public class ConfigValidador {

    private final Logger logger = LogManager.getLogger();

    public ConfigValidador() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public FilterRegistrationBean<RequestValidatiorFilter> filterRegistrationBeanRequestValidatiorFilter() {
        FilterRegistrationBean<RequestValidatiorFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new RequestValidatiorFilter());
        registrationBean.addUrlPatterns("/api/*");
        registrationBean.addUrlPatterns("/api-local/*");
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE - 11);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<RequestValidatiorAdditionalFilter> filterRegistrationBeanRequestValidatiorFilterV2() {
        FilterRegistrationBean<RequestValidatiorAdditionalFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new RequestValidatiorAdditionalFilter());
        registrationBean.addUrlPatterns("/api/*");
        registrationBean.addUrlPatterns("/api-local/*");
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE - 12);
        return registrationBean;
    }
}
