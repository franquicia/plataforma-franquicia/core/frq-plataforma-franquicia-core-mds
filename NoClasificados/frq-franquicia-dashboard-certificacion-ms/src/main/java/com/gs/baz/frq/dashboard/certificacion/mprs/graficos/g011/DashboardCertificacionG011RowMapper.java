package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g011;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g011.DashboardCertificacionG011DTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG011RowMapper implements RowMapper<DashboardCertificacionG011DTO> {

    private DashboardCertificacionG011DTO dashboardCertificacionG011DTO;

    @Override
    public DashboardCertificacionG011DTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionG011DTO = new DashboardCertificacionG011DTO();
        dashboardCertificacionG011DTO.setFecha(rs.getString("FECHA"));
        dashboardCertificacionG011DTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionG011DTO.setIdCecoBase(rs.getString("FICECOBASE_ID"));
        dashboardCertificacionG011DTO.setNombre(rs.getString("NOMBRE"));
        dashboardCertificacionG011DTO.setHoraEntrada(rs.getString("ENTRADA"));
        dashboardCertificacionG011DTO.setHoraSalida(rs.getString("SALIDA"));
        return dashboardCertificacionG011DTO;
    }
}
