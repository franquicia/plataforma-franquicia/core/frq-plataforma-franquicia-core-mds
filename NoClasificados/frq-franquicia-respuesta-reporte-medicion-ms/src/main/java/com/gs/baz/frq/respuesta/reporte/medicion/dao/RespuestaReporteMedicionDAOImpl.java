package com.gs.baz.frq.respuesta.reporte.medicion.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.respuesta.reporte.medicion.dao.util.GenericDAO;
import com.gs.baz.frq.respuesta.reporte.medicion.dto.RespuestaReporteMedicionDTO;
import com.gs.baz.frq.respuesta.reporte.medicion.mprs.RespuestaReporteMedicionRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author jlfr
 */
public class RespuestaReporteMedicionDAOImpl extends DefaultDAO implements GenericDAO<RespuestaReporteMedicionDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMMEDRESP");
        jdbcInsert.withProcedureName("SPINSMEDRESP");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMMEDRESP");
        jdbcUpdate.withProcedureName("SPACTMEDRESP");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMMEDRESP");
        jdbcDelete.withProcedureName("SPDELMEDRESP");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMMEDRESP");
        jdbcSelect.withProcedureName("SPGETMEDRESP");
        jdbcSelect.returningResultSet("PA_CDATOS", new RespuestaReporteMedicionRowMapper());
    }

    @Override
    public List<RespuestaReporteMedicionDTO> selectRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", null);
        mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<RespuestaReporteMedicionDTO>) out.get("PA_CDATOS");
    }

    @Override
    public List<RespuestaReporteMedicionDTO> selectRow(Long entityID, Long entityID2) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityID);
        mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityID2);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RespuestaReporteMedicionDTO> data = (List<RespuestaReporteMedicionDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public List<RespuestaReporteMedicionDTO> selectRowsRespuestaReporteMedicion(RespuestaReporteMedicionDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
        mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<RespuestaReporteMedicionDTO>) out.get("PA_CDATOS");

    }

    @Override
    public RespuestaReporteMedicionDTO insertRow(RespuestaReporteMedicionDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
            mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
            mapSqlParameterSource.addValue("PA_FCRESPUESTA", entityDTO.getRespuesta());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FINUMSERIE", entityDTO.getNumSerie());
            mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfigDis());
            mapSqlParameterSource.addValue("PA_FIMPERDONABLE", entityDTO.getImperdonable());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            System.out.println("success: " + success);
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of respuesta de reporte de medicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of respuesta de reporte de medicion"), ex);
        }
    }

    @Override
    public RespuestaReporteMedicionDTO updateRow(RespuestaReporteMedicionDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
            mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
            mapSqlParameterSource.addValue("PA_FCRESPUESTA", entityDTO.getRespuesta());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfigDis());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FINUMSERIE", entityDTO.getNumSerie());
            mapSqlParameterSource.addValue("PA_FIMPERDONABLE", entityDTO.getImperdonable());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of respuesta de reporte medicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of respuesta de reporte medicion"), ex);
        }
    }

    @Override
    public RespuestaReporteMedicionDTO deleteRow(RespuestaReporteMedicionDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
            mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of respuesta de reporte medicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of respuesta de reporte medicion"), ex);
        }
    }

}
