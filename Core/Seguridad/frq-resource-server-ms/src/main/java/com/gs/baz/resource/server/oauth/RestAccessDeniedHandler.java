/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.resource.server.oauth;

/**
 *
 * @author cescobarh
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.RespuestaErrorClient;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import springfox.documentation.service.ResponseMessage;

@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        ResponseMessage responseMessage = ResponseMensages.getByStatus(403);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "104003";
        RespuestaErrorClient respuestaErrorClient = new RespuestaErrorClient(codeError, responseMessage.getMessage(), requestID);
        httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.getStatus();
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        OutputStream out = httpServletResponse.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, respuestaErrorClient);
        out.flush();
    }
}
