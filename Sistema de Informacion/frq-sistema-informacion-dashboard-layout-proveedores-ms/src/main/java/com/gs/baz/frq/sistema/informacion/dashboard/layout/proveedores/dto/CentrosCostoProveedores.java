/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de centros de costo asociados a un proveedor", value = "CentrosCostoProveedores")
public class CentrosCostoProveedores {

    @JsonProperty(value = "centrosCostoProveedores")
    @ApiModelProperty(notes = "centrosCostoProveedores")
    private List<CentroCostosProveedor> centroCostoProveedores;

    public CentrosCostoProveedores(List<CentroCostosProveedor> centroCostoProveedores) {
        this.centroCostoProveedores = centroCostoProveedores;
    }

    public List<CentroCostosProveedor> getCentroCostoProveedores() {
        return centroCostoProveedores;
    }

    public void setCentroCostoProveedores(List<CentroCostosProveedor> centroCostoProveedores) {
        this.centroCostoProveedores = centroCostoProveedores;
    }

    @Override
    public String toString() {
        return "CentrosCostoProveedores{" + "centroCostoProveedores=" + centroCostoProveedores + '}';
    }

}
