/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.mprs;

import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.VistaPropiedadBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class VistaPropiedadRowMapper implements RowMapper<VistaPropiedadBase> {

    private VistaPropiedadBase vistaPropiedadBase;

    @Override
    public VistaPropiedadBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPropiedadBase = new VistaPropiedadBase();
        vistaPropiedadBase.setNombre(rs.getString("FCNOMBRE"));
        vistaPropiedadBase.setValor(rs.getString("FCVALOR"));
        vistaPropiedadBase.setTipo(rs.getString("FCTIPO"));
        return vistaPropiedadBase;
    }
}
