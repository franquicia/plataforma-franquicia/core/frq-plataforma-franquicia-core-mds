/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosGrupoUsuario {

    @JsonProperty(value = "idGrupo")
    @ApiModelProperty(notes = "Identificador del grupo", example = "1", position = -1)
    private Integer idGrupo;

    private transient Integer numeroEmpleado;

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    @Override
    public String toString() {
        return "ParametrosGrupoUsuario{" + "idGrupo=" + idGrupo + ", numeroEmpleado=" + numeroEmpleado + '}';
    }

}
