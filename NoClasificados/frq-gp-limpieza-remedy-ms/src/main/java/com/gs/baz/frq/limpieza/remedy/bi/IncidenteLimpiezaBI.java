package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dto.BandejaIncidentesResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.CuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.DatosEmpMttoDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteCuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteLimpiezaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfInformacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfTracking;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Informacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Tracking;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._Calificacion;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;

public class IncidenteLimpiezaBI {

    @Autowired
    private DatosEmpMttoBI datosEmpMttoBI;
    @Autowired
    private IncidenteCuadrillaLimpiezaBI incidenteCuadrillaLimpiezaBI;
    @Autowired
    private CuadrillaBI cuadrillaBI;
    @Autowired
    private ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI;
    @Autowired
    private ProveedoresBI proveedoresBI;
    @Autowired
    private IncidenteLimpiezaDosBI incidenteLimpiezaDosBI;
    @Autowired
    private ServiciosRemedyLimpiezaDosBI serviciosRemedyLimpiezaDosBI;

    public static final Integer SUPERVISOR = 2;
    private static final String SININFO = "* Sin información";
    private static final String FECHADEFAULT = "0001-01-01 00:00:00";
    private static final String FORMATOFECHA = "yyyy-MM-dd HH:mm:ss";
    private static final Integer UNO = 1, DOS = 2, TRES = 3, CUATRO = 4, CINCO = 5;

    @SuppressWarnings("all")
    public BandejaIncidentesResponseDTO getBandejaTickets(String usuario, int tipoUsuario) {

        BandejaIncidentesResponseDTO envoltorioJsonObj = new BandejaIncidentesResponseDTO();

        ArrayOfInformacion arrInformacions;
        ArrayOfInformacion arrInformacionsCerrados;
        if (tipoUsuario == SUPERVISOR) {// Supervisor
            arrInformacions = serviciosRemedyLimpiezaDosBI.consultaFoliosSupervidor(Integer.parseInt(usuario), "1");
            arrInformacionsCerrados = serviciosRemedyLimpiezaDosBI.consultaFoliosSupervidor(Integer.parseInt(usuario), "2");
        } else {
            arrInformacions = serviciosRemedyLimpiezaDosBI.consultaFoliosProveedor(usuario.trim(), "1");
            arrInformacionsCerrados = serviciosRemedyLimpiezaDosBI.consultaFoliosProveedor(usuario.trim(), "2");
        }

        // ----------------------Agrupar folios por tipo de
        // estado-----------------------
        ArrayList<Informacion> recibidosPorAtender = new ArrayList<>();
        ArrayList<Informacion> enProceso = new ArrayList<>();
        ArrayList<Informacion> enProcesoDeCierre = new ArrayList<>();
        ArrayList<Informacion> atendidos = new ArrayList<>();
        ArrayList<Informacion> pendientesPorAutorizar = new ArrayList<>();

        separaBandejas(arrInformacions, tipoUsuario, null, recibidosPorAtender, enProceso, enProcesoDeCierre,
                atendidos, pendientesPorAutorizar);
        separaBandejas(arrInformacionsCerrados, tipoUsuario, null, recibidosPorAtender, enProceso,
                enProcesoDeCierre, atendidos, pendientesPorAutorizar);


        /*
		 * Poner nombres de los supervisores en base al ID
         */
        List<IncidenteLimpiezaDTO> arrayRPA = incidenteLimpiezaDosBI.trabajaBandeja(recibidosPorAtender);
        List<IncidenteLimpiezaDTO> arrayEP = incidenteLimpiezaDosBI.trabajaBandeja(enProceso);
        List<IncidenteLimpiezaDTO> arrayEPC = incidenteLimpiezaDosBI.trabajaBandeja(enProcesoDeCierre);
        List<IncidenteLimpiezaDTO> arrayAtendidos = incidenteLimpiezaDosBI.trabajaBandeja(atendidos);
        List<IncidenteLimpiezaDTO> arrayPPA = incidenteLimpiezaDosBI.trabajaBandeja(pendientesPorAutorizar);

        if (arrInformacions != null) {

            envoltorioJsonObj.setRecibidoPorAtender(arrayRPA == null ? new ArrayList<>() : arrayRPA);
            envoltorioJsonObj.setEnProceso(arrayEP == null ? new ArrayList<>() : arrayEP);
            envoltorioJsonObj.setEnProcesoDeCierre(arrayEPC == null ? new ArrayList<>() : arrayEPC);
            envoltorioJsonObj.setAtendidos(arrayAtendidos == null ? new ArrayList<>() : arrayAtendidos);
            envoltorioJsonObj.setPendientesPorAutorizar(arrayPPA == null ? new ArrayList<>() : arrayPPA);

        }

        return envoltorioJsonObj;

    }

    public Boolean condicion2(Informacion aux) {
        Boolean respuesta = false;
        if (aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("abierto")
                || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("revisado")
                || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("por asignar")
                || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("rechazado proveedor")) {
            respuesta = true;
        }
        return respuesta;

    }

    @SuppressWarnings("all")
    public void separaBandejas(ArrayOfInformacion bandejaPrincipal, int tipoUsuario, ArrayList<String> supervisores,
            ArrayList<Informacion> recibidosPorAtender,
            ArrayList<Informacion> enProceso,
            ArrayList<Informacion> enProcesoDeCierre,
            ArrayList<Informacion> atendidos,
            ArrayList<Informacion> pendientePorAtender
    ) {

        if (bandejaPrincipal != null) {
            Informacion[] arregloInc = bandejaPrincipal.getInformacion();
            if (arregloInc != null) {

                for (Informacion aux : arregloInc) {

                    if (aux.getEstado().contains("Recibido por atender") && (Boolean.TRUE.equals(condicion2(aux)))) {
                        if (tipoUsuario != 3 && !(aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("rechazado proveedor"))) {
                            recibidosPorAtender.add(aux);
                        }

                    }
                    if (aux.getEstado().contains("En Recepción")
                            && (aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("en validacion")
                            || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("cierre técnico")
                            || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("rechazado por cliente"))) {
                        enProcesoDeCierre.add(aux);
                    }
                    if (aux.getEstado().contains("Atendido") && (aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("atendido")
                            || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("cancelado correctivo menor")
                            || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("cancelado no aplica"))) {
                        if (tipoUsuario == 3 && (aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("cancelado correctivo menor")
                                || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("cancelado no aplica"))) {

                        } else {
                            atendidos.add(aux);
                        }

                    }
                    if (supervisores.isEmpty()) {
                        supervisores.add(aux.getIdCorpSupervisor());
                    } else {
                        boolean flagSup = false;
                        for (String sup : supervisores) {
                            if (aux.getIdCorpSupervisor() != null) {
                                if (sup != null && sup.equals(aux.getIdCorpSupervisor())) {
                                    flagSup = true;
                                    break;
                                }
                            }
                        }
                        if (!flagSup) {
                            if (aux.getIdCorpSupervisor() != null) {
                                supervisores.add(aux.getIdCorpSupervisor());
                            }
                        }
                    }

                }
            }
        }

    }

    @SuppressWarnings("all")
    public ArrayList<IncidenteLimpiezaDTO> arrOrdenadoSupervisor(ArrayList<Informacion> critica, ArrayList<Informacion> normal) {

        ArrayList<IncidenteLimpiezaDTO> arrJsonArray = new ArrayList<>();

        incidenteLimpiezaDosBI.armaJson(critica, arrJsonArray);
        incidenteLimpiezaDosBI.armaJson(normal, arrJsonArray);

        return arrJsonArray;
    }

    @SuppressWarnings("all")
    public IncidenteLimpiezaDTO arrOrdenado(Informacion incidente) {

        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
        HashMap<String, DatosEmpMttoDTO> empleadosHashMap = new HashMap<>();

        Informacion aux = incidente;

        IncidenteLimpiezaDTO incJsonObj = new IncidenteLimpiezaDTO();

        incJsonObj.setIdIncidencia(aux.getIdIncidencia());
        incJsonObj.setEstado(aux.getEstado());
        incJsonObj.setFalla(aux.getFalla());
        incJsonObj.setIncidencia(aux.getIncidencia());
        incJsonObj.setMotivoEstado(aux.getMotivoEstado());
        incJsonObj.setNombreCliente(aux.getNombreCliente());
        incJsonObj.setIdCliente(aux.getIdCorpCliente());
        incJsonObj.setIdSupervisor(aux.getIdCorpSupervisor());
        incJsonObj.setIdCoordinador("0");

        String idCliente = aux.getIdCorpCliente();
        if (idCliente != null) {
            if (!empleadosHashMap.containsKey(idCliente.trim())) {
                List<DatosEmpMttoDTO> datosCliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                if (!datosCliente.isEmpty() && datosCliente != null) {
                    for (DatosEmpMttoDTO emp_aux : datosCliente) {
                        incJsonObj.setPuestoCliente(emp_aux.getDescripcion());
                        incJsonObj.setTelefonoCliente(emp_aux.getTelefono());
                        empleadosHashMap.put(idCliente.trim(), emp_aux);
                    }
                } else {
                    incJsonObj.setPuestoCliente(SININFO);
                    incJsonObj.setTelefonoCliente(SININFO);
                }
            } else {
                DatosEmpMttoDTO empAux = empleadosHashMap.get(idCliente.trim());
                incJsonObj.setPuestoCliente(empAux.getDescripcion());
                incJsonObj.setTelefonoCliente(empAux.getTelefono());
            }
        } else {
            incJsonObj.setPuestoCliente(SININFO);
            incJsonObj.setTelefonoCliente(SININFO);
        }

        incJsonObj.setTelefonoCoordinador(SININFO);
        incJsonObj.setNombreCoordinador(SININFO);

        String idSup = null;

        if (idSup == null) {
            incJsonObj.setTelefonoSupervisor(SININFO);
        }
        if (idSup == null) {
            incJsonObj.setNombreSupervisor(SININFO);
            incJsonObj.setNombreSupervisorAuxiliar(SININFO);

        }
        incJsonObj.setNotas(aux.getNotas());
        incJsonObj.setNumeroIncidenteProveedor(aux.getNoTicketProveedor());
        incJsonObj.setProveedor(aux.getProveedor());

        if (aux.getProveedor() != null) {
            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux3 : lista) {
                    if (aux3.getStatus().contains("Activo")) {
                        if (aux.getProveedor().toLowerCase(Locale.ROOT).trim()
                                .contains(aux3.getNombreCorto().toLowerCase(Locale.ROOT).trim())) {
                            incJsonObj.setNombreCortoProveedor(aux3.getNombreCorto());
                            incJsonObj.setMenu(aux3.getMenu());
                            incJsonObj.setRazonSocial(aux3.getRazonSocial());
                            incJsonObj.setIdProveedor(aux3.getIdProveedor() + "");
                            break;
                        }

                    }
                }
            }
        } else {
            String nomCorto = null;
            incJsonObj.setNombreCortoProveedor(nomCorto);
            incJsonObj.setMenu(nomCorto);
            incJsonObj.setRazonSocial(nomCorto);
            incJsonObj.setIdProveedor(nomCorto);
        }

        incJsonObj.setPuestoAlterno(aux.getPuestoAlterno());
        incJsonObj.setNombreSucursal(aux.getSucursal());
        incJsonObj.setTipoFalla(aux.getTipoFalla());
        incJsonObj.setUsuarioAlterno(aux.getUsrAlterno());
        incJsonObj.setTelefonoContactoAlterno(aux.getTelCliente());

        incJsonObj.setMontoEstimado(aux.getMontoEstimado().toString());
        incJsonObj.setNumeroSucursal(aux.getNoSucursal());
        Date date;
        SimpleDateFormat format1 = new SimpleDateFormat(FORMATOFECHA);
        String date1;
        incJsonObj.setFechaCierreTecnicoDos(FECHADEFAULT);
        incJsonObj.setFechaCreacion(format1.format(aux.getFechaModificacion().getTime()));
        date = aux.getFechaModificacion().getTime();
        date1 = format1.format(date);
        incJsonObj.setFechaModificacion(date1);
        incJsonObj.setFechaEstimada(FECHADEFAULT);
        incJsonObj.setPrioridad(aux.getPrioridad().getValue());
        incJsonObj.setTipoCierreProvedor(aux.getTipoCierreProveedor());
        String autCleinte = aux.getAutorizacionCliente();
        if (autCleinte != null && !autCleinte.equals("")) {
            if (autCleinte.contains("Rechazado")) {
                incJsonObj.setAutorizacionCliente("2");
            } else {
                incJsonObj.setAutorizacionCliente("1");
            }
        } else {
            incJsonObj.setAutorizacionCliente("0");
        }

        if (aux.getTipoAtencion() != null) {
            incJsonObj.setTipoAtencion(aux.getTipoAtencion().getValue());
        } else {
            String x = null;
            incJsonObj.setTipoAtencion(x);
        }
        if (aux.getOrigen() != null) {
            incJsonObj.setOrigen(aux.getOrigen().getValue());
        } else {
            String x = null;
            incJsonObj.setOrigen(x);
        }
        //-----------------Obtiene coordenadas de inicio y fin de incidente -------------------
        incJsonObj.setLatitudFin("");
        incJsonObj.setLongitudFin("");

        //----------------------------------------------------------------------------------
        incJsonObj.setFechaInicioAtencion(FECHADEFAULT);

        int idIncidente = aux.getIdIncidencia();

        List<IncidenteCuadrillaDTO> lista2 = incidenteCuadrillaLimpiezaBI.obtieneDatos(idIncidente);
        int idProvedor = 0;
        int idCuadrilla = 0;
        int idZona = 0;
        for (IncidenteCuadrillaDTO aux2 : lista2) {
            if (aux2.getStatus() != 0) {
                idProvedor = aux2.getIdProveedor();
                idCuadrilla = aux2.getIdCuadrilla();
                idZona = aux2.getZona();
            }

        }
        if (idProvedor != 0) {
            List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
            for (CuadrillaDTO aux2 : lista3) {
                if (aux2.getZona() == idZona) {
                    if (aux2.getIdCuadrilla() == idCuadrilla) {
                        incJsonObj.setIdProveedor(aux2.getIdProveedor() + "");
                        incJsonObj.setIdCuadrilla(aux2.getIdCuadrilla());
                        incJsonObj.setLider(aux2.getLiderCuad());
                        incJsonObj.setCorreo(aux2.getCorreo());
                        incJsonObj.setSegundoMando(aux2.getSegundo());
                        incJsonObj.setIdZona(aux2.getZona());
                    }
                }
            }
        } else {
            String auxiliar = null;
            incJsonObj.setIdCuadrilla(0);
            incJsonObj.setLider(auxiliar);
            incJsonObj.setCorreo(auxiliar);
            incJsonObj.setSegundoMando(auxiliar);
            incJsonObj.setIdZona(0);
        }

        if (aux.getMotivoEstado().trim().toLowerCase(Locale.ROOT).equals("asignado a proveedor")
                || aux.getMotivoEstado().trim().toLowerCase(Locale.ROOT).equals("duración no autorizada")
                || aux.getMotivoEstado().trim().toLowerCase(Locale.ROOT).equals("cotización no autorizada")) {

            boolean flag1 = false;
            boolean flag2 = false;

            incJsonObj.setBanderaMontoAutorizado(0);

            incJsonObj.setBanderaAplazamiento(0);

            if (flag1 == true && flag2 == true) {
                incJsonObj.setBanderas(1);
            } else {
                incJsonObj.setBanderas(0);
            }
        } else {
            incJsonObj.setBanderaMontoAutorizado(0);
            incJsonObj.setBanderaAplazamiento(0);
            incJsonObj.setBanderas(0);
        }

        //-----------------------  Consulta Traking  --------------------
        ArrayOfTracking arrTraking = serviciosRemedyLimpiezaBI.ConsultaBitacora(aux.getIdIncidencia());

        ArrayList<Date> rechazoCliente = new ArrayList<Date>();
        ArrayList<Date> inicioAtencion = new ArrayList<Date>();
        ArrayList<Date> cierreTecnico = new ArrayList<Date>();
        ArrayList<Date> cancelados = new ArrayList<Date>();
        ArrayList<Date> atencionGarantia = new ArrayList<Date>();
        ArrayList<Date> cierreGarantia = new ArrayList<Date>();
        ArrayList<Date> rechazoProveedor = new ArrayList<Date>();

        if (arrTraking != null) {
            Tracking[] traking = arrTraking.getTracking();
            if (traking != null) {
                for (Tracking aux2 : traking) {
                    String detalle = aux2.getDetalle();

                    if (detalle.contains("||")) {
                        String arrAux[] = detalle.split("\\|\\|");
                        String estado = detalle.split("\\|\\|")[1];
                        String motivoEstado = detalle.split("\\|\\|")[3];
                        Date date2 = aux2.getFechaEnvio().getTime();
                        if (estado.toLowerCase(Locale.ROOT).trim().equals("en recepción")
                                && motivoEstado.trim().toLowerCase(Locale.ROOT).equals("rechazado por cliente")) {
                            rechazoCliente.add(date2);
                        }
                        if (estado.toLowerCase(Locale.ROOT).trim().equals("en proceso")
                                && motivoEstado.trim().toLowerCase(Locale.ROOT).equals("asignado a proveedor")) {
                            inicioAtencion.add(date2);
                        }
                        if (estado.toLowerCase(Locale.ROOT).trim().equals("en recepción")
                                && motivoEstado.trim().toLowerCase(Locale.ROOT).equals("cierre técnico")) {
                            cierreTecnico.add(date2);
                        }

                        if (estado.toLowerCase(Locale.ROOT).trim().equals("atendido")
                                && (motivoEstado.trim().toLowerCase(Locale.ROOT).equals("cancelado duplicado")
                                || motivoEstado.trim().toLowerCase(Locale.ROOT).equals("cancelado no aplica")
                                || motivoEstado.trim().toLowerCase(Locale.ROOT).equals("cancelado correctivo menor"))) {
                            cancelados.add(date2);
                        }

                        if (estado.toLowerCase(Locale.ROOT).trim().equals("en proceso")
                                && motivoEstado.trim().toLowerCase(Locale.ROOT).equals("en curso garantía")) {
                            atencionGarantia.add(date2);
                        }

                        if (estado.toLowerCase(Locale.ROOT).trim().equals("atendido")
                                && motivoEstado.trim().toLowerCase(Locale.ROOT).equals("cerrado garantía")) {
                            cierreGarantia.add(date2);
                        }

                        if (estado.toLowerCase(Locale.ROOT).trim().equals("recibido por atender")
                                && motivoEstado.trim().toLowerCase(Locale.ROOT).equals("rechazado proveedor")) {
                            rechazoProveedor.add(date2);
                        }
                    }

                }
            }
        }

        int posRC = 0, posRP = 0;

        if (rechazoCliente != null && !rechazoCliente.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < rechazoCliente.size(); i++) {
                if (rechazoCliente.get(pos2).compareTo(rechazoCliente.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = rechazoCliente.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setRechazoCliente(date3);
            posRC = pos2;
        } else {

            incJsonObj.setRechazoCliente(FECHADEFAULT);
        }

        if (inicioAtencion != null && !inicioAtencion.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < inicioAtencion.size(); i++) {
                if (inicioAtencion.get(pos2).compareTo(inicioAtencion.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = inicioAtencion.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setFechaAtencion(date3);
        } else {
            incJsonObj.setFechaAtencion(FECHADEFAULT);
        }

        if (cierreTecnico != null && !cierreTecnico.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cierreTecnico.size(); i++) {
                if (cierreTecnico.get(pos2).compareTo(cierreTecnico.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cierreTecnico.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setFechaCierreTecnico(date3);
            incJsonObj.setFechaCierre(date3);
        } else {
            incJsonObj.setFechaCierreTecnico(FECHADEFAULT);
            incJsonObj.setFechaCierre(FECHADEFAULT);
        }

        if (cancelados != null && !cancelados.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cancelados.size(); i++) {
                if (cancelados.get(pos2).compareTo(cancelados.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cancelados.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setFechaCancelacion(date3);
        } else {
            incJsonObj.setFechaCancelacion(FECHADEFAULT);
        }
        //
        if (atencionGarantia != null && !atencionGarantia.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < atencionGarantia.size(); i++) {
                if (atencionGarantia.get(pos2).compareTo(atencionGarantia.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = atencionGarantia.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setFechaAtencionGarantia(date3);
        } else {
            incJsonObj.setFechaAtencionGarantia(FECHADEFAULT);
        }

        if (cierreGarantia != null && !cierreGarantia.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < cierreGarantia.size(); i++) {
                if (cierreGarantia.get(pos2).compareTo(cierreGarantia.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = cierreGarantia.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setFechaCierreGarantia(date3);
        } else {
            incJsonObj.setFechaCierreGarantia(FECHADEFAULT);
        }

        if (rechazoProveedor != null && !rechazoProveedor.isEmpty()) {
            int pos2 = 0;
            for (int i = 0; i < rechazoProveedor.size(); i++) {
                if (rechazoProveedor.get(pos2).compareTo(rechazoProveedor.get(i)) >= 0) {
                    pos2 = i;
                }
            }
            Date date2 = rechazoProveedor.get(pos2);
            SimpleDateFormat format2 = new SimpleDateFormat(FORMATOFECHA);
            String date3 = format2.format(date2);
            incJsonObj.setFechaRechazoProveedor(date3);
            posRP = pos2;
        } else {
            incJsonObj.setFechaRechazoProveedor(FECHADEFAULT);
        }

        //BANDERA_RECHAZO_CIERRE_PREVIO
        if (rechazoProveedor != null && !rechazoProveedor.isEmpty() && rechazoCliente != null && !rechazoCliente.isEmpty()) {
            if (rechazoCliente.get(posRC).compareTo(rechazoProveedor.get(posRP)) <= 0) {
                incJsonObj.setBanderaRechazoCierrePrevio(true);
            } else {
                incJsonObj.setBanderaRechazoCierrePrevio(false);
            }
        } else {
            incJsonObj.setBanderaRechazoCierrePrevio(false);
        }

        //------------------------------------------------------------------
        int cal = 0;
        _Calificacion auxEval = aux.getEvalCliente();
        if (auxEval != null) {

            if (auxEval.getValue().contains("NA")) {
                cal = UNO;
            }
            if (auxEval.getValue().contains("E0a50")) {
                cal = DOS;
            }
            if (auxEval.getValue().contains("E51a69")) {
                cal = TRES;
            }
            if (auxEval.getValue().contains("E70a89")) {
                cal = CUATRO;
            }
            if (auxEval.getValue().contains("E90a100")) {
                cal = CINCO;
            }
        }

        incJsonObj.setEvaluacionCliente(cal + "");

        incJsonObj.setMontoReal("0.0");
        incJsonObj.setFechaCierreAdministrativo(FECHADEFAULT);
        incJsonObj.setDiasAplazammiento("0");

        incJsonObj.setLatitudInicio("");
        incJsonObj.setLongitudInicio("");
        {
            String auxApr = null;
            incJsonObj.setMensajeProveedor(auxApr);
            incJsonObj.setAutorizacionProveedor(auxApr);
            incJsonObj.setFechaAutorizaProveedor(auxApr);
            incJsonObj.setDeclinarProveedor(auxApr);
        }

        incJsonObj.setResolucion("");
        incJsonObj.setMontoEstimadoAutorizado("0.0");
        incJsonObj.setClienteAvisado(aux.getClienteAvisado());

        return incJsonObj;

    }
}
