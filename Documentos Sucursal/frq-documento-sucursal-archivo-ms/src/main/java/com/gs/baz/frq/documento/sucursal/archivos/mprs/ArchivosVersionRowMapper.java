/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.mprs;

import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoVersionVirtual;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ArchivosVersionRowMapper implements RowMapper<ArchivoVersionVirtual> {

    private ArchivoVersionVirtual archivo;

    @Override
    public ArchivoVersionVirtual mapRow(ResultSet rs, int rowNum) throws SQLException {

        archivo = new ArchivoVersionVirtual();
        archivo.setIdArchivo(rs.getInt("FIID_ARCHIVO"));
        archivo.setIdArchivoPadre(rs.getInt("FIID_ARCHPADRE"));
        archivo.setUid(rs.getLong("FCRUTA"));
        archivo.setDescripcion(rs.getString("FCDESCRIPCION"));
        archivo.setNombre(rs.getString("FCNOMBRE"));
        archivo.setExtension(rs.getString("FCEXTENSION"));
        archivo.setTipo(rs.getInt("FCTIPO"));
        archivo.setIndice(rs.getInt("FIID_VERSION"));
        archivo.setPeso(rs.getInt("FCPESO"));
        archivo.setTipoContenido(rs.getString("FCCONTENTYPE"));
        archivo.setIdFolio(rs.getInt("FIID_FOLIO"));

        archivo.setFecha(rs.getString("FCPERIODO"));
        archivo.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        archivo.setNombreProyecto(rs.getString("NOM_PROY"));
        archivo.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        archivo.setNombreDocumento(rs.getString("FCDESCRIPCION"));
        archivo.setGeneraVersion(rs.getInt("FIID_GENHISTO"));

        return archivo;
    }
}
