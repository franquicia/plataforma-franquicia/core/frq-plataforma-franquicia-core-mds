package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.DatosEmpMttoDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.DatosEmpMttoRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DatosEmpMttoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcBuscaFila;
    private DefaultJdbcCall jdbcBuscaInfoFilas;

    private List<DatosEmpMttoDTO> listaDetU;
    private static final Logger LOGGER = LogManager.getLogger();

    public DatosEmpMttoDAOImpl() {
        super();
    }

    @SuppressWarnings("all")
    public void init() {

        jdbcBuscaInfoFilas = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILREM")
                .withProcedureName("SP_INFOEMPEXT")
                .returningResultSet("RCL_EMPLEADOEXT", new DatosEmpMttoRowMapper());

        jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERFILREM")
                .withProcedureName("SP_PARAMETRO")
                .returningResultSet("RCL_EMPLEADOEXT", new DatosEmpMttoRowMapper());

    }

    @SuppressWarnings("all")
    //con un parametro
    public List<DatosEmpMttoDTO> obtieneDatos(int idUsuario) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSU", idUsuario);

        out = jdbcBuscaFila.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPERFILREM.SP_PARAMETRO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<DatosEmpMttoDTO>) out.get("RCL_EMPLEADOEXT");

        if (respuesta != 1) {
            LOGGER.info("Algo paso al consular la informacion del Empleado externo");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("all")
    public List<DatosEmpMttoDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<DatosEmpMttoDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoFilas.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPERFILREM.SP_INFOEMPEXT}");
        listaDetU = (List<DatosEmpMttoDTO>) out.get("RCL_EMPLEADOEXT");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            LOGGER.info("Algo paso al obtener la informacion de los empleados externos");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

}
