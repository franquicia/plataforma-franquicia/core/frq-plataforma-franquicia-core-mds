/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dao;

import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ReporteMedicionDAO extends DefaultDAO {

    private final Logger logger = LogManager.getLogger();
    private String schema;

    private DefaultJdbcCall jdbcSelectVisitas;
    private DefaultJdbcCall jdbcSelectPreguntas;
    private DefaultJdbcCall jdbcSelectRespuestas;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectVisitas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectVisitas.withSchemaName(schema);
        jdbcSelectVisitas.withCatalogName("PAREPORTEMEDINT");
        jdbcSelectVisitas.withProcedureName("SPREP_VISITAS_NO7S");

        jdbcSelectPreguntas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectPreguntas.withSchemaName(schema);
        jdbcSelectPreguntas.withCatalogName("PAREPORTEMEDINT");
        jdbcSelectPreguntas.withProcedureName("SPREP_PREGUNTAS");

        jdbcSelectRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectRespuestas.withSchemaName(schema);
        jdbcSelectRespuestas.withCatalogName("PAREPORTEMEDINT");
        jdbcSelectRespuestas.withProcedureName("SPREP_RESPUESTAS");

    }

    public Object selectVisitas(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA_INI", filtersDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHA_FIN", filtersDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FCID_CECO", filtersDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", filtersDTO.getIdNegocio());

        Map<String, Object> out = jdbcSelectVisitas.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return out.get("PA_VISITAS");

        } else {
            return null;
        }
    }

    public Object selectPreguntas(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_CHECKLIST_ID", filtersDTO.getIdChecklist());

        Map<String, Object> out = jdbcSelectPreguntas.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return out.get("PA_PREGUNTAS");

        } else {
            return null;
        }
    }

    public Object selectRespuestas(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA_INI", filtersDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHA_FIN", filtersDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FCID_CECO", filtersDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", filtersDTO.getIdNegocio());

        Map<String, Object> out = jdbcSelectRespuestas.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return out.get("PA_RESPUESTAS");

        } else {
            return null;
        }
    }

}
