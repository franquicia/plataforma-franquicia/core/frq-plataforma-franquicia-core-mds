/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Base64Exception extends Exception {

    private String field;
    private List<String> fields;

    public Base64Exception(String message) {
        super(message);
    }

    public Base64Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public Base64Exception(Throwable cause) {
        super(cause);
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

}
