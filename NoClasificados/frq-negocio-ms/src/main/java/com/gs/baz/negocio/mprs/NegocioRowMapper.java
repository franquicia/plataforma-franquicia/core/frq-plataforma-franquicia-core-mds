package com.gs.baz.negocio.mprs;

import com.gs.baz.negocio.dto.NegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegocioRowMapper implements RowMapper<NegocioDTO> {

    private NegocioDTO negocio;

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocio = new NegocioDTO();
        negocio.setIdNegocio(((BigDecimal) rs.getObject("FIIDNEGO")));
        negocio.setDescripcion(rs.getString("FCDESCRIPCION"));
        negocio.setStatus(((BigDecimal) rs.getObject("FIIDSTATUS")));
        negocio.setPathLogo(rs.getString("FCLOGO"));
        return negocio;
    }
}
