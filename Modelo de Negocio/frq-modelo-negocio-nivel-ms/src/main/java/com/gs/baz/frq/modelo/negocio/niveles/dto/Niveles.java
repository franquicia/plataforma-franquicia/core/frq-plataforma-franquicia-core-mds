/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.niveles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de niveles del modelo de negocio", value = "niveles")
public class Niveles {

    @JsonProperty(value = "niveles")
    @ApiModelProperty(notes = "niveles")
    private List<Nivel> niveles;

    public Niveles(List<Nivel> niveles) {
        this.niveles = niveles;
    }

    public List<Nivel> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<Nivel> modelos) {
        this.niveles = modelos;
    }

    @Override
    public String toString() {
        return "Niveles{" + "niveles=" + niveles + '}';
    }

}
