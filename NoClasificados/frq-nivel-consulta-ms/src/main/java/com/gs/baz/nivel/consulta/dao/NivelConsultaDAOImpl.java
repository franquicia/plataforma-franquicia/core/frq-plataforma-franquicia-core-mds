package com.gs.baz.nivel.consulta.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.nivel.consulta.dao.util.GenericDAO;
import com.gs.baz.nivel.consulta.dto.NivelConsultaDTO;
import com.gs.baz.nivel.consulta.mprs.NivelConsultaRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class NivelConsultaDAOImpl extends DefaultDAO implements GenericDAO<NivelConsultaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINNIVCONSU");
        jdbcInsert.withProcedureName("SP_INS_NIVCONS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINNIVCONSU");
        jdbcUpdate.withProcedureName("SP_ACT_NIVCONS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINNIVCONSU");
        jdbcDelete.withProcedureName("SP_DEL_NIVCONS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINNIVCONSU");
        jdbcSelect.withProcedureName("SP_SEL_NIVCONS");
        jdbcSelect.returningResultSet("RCL_INFO", new NivelConsultaRowMapper());
    }

    @Override
    public NivelConsultaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNIVELC", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<NivelConsultaDTO> data = (List<NivelConsultaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<NivelConsultaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNIVELC", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<NivelConsultaDTO> data = (List<NivelConsultaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public NivelConsultaDTO insertRow(NivelConsultaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDESTATUS", entityDTO.getIdStatus());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdNivelConsulta((BigDecimal) out.get("PA_FIIDNIVELC"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Nivel de Consulta "));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Nivel de Consulta "), ex);
        }
    }

    @Override
    public NivelConsultaDTO updateRow(NivelConsultaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDNIVELC", entityDTO.getIdNivelConsulta());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDESTATUS", entityDTO.getIdStatus());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error  not success to update row of Nivel de Consulta"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Nivel de Consulta"), ex);
        }
    }

    @Override
    public NivelConsultaDTO deleteRow(NivelConsultaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDNIVELC", entityDTO.getIdNivelConsulta());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error  not success to delete row of Nivel de Consulta"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error  to delete row of Nivel de Consulta"), ex);
        }
    }

}
