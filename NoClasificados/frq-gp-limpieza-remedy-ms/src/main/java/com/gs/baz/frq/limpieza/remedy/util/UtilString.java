package com.gs.baz.frq.limpieza.remedy.util;

public class UtilString {

    public UtilString() {
        super();

    }

    public static String cleanParameter(String parameter) {
        if (parameter != null) {
            if (parameter.matches("\\d{4}/\\d{2}/\\d{2}") || parameter.matches("\\d{2}/\\d{2}/\\d{4}")) {
                parameter = parameter.replace(":", "");
                parameter = parameter.replace("\\\\", "");
                parameter = parameter.replace("\\.", "");
                parameter = parameter.replace("%2e", "");
                parameter = parameter.replace("%2E", "");
                parameter = parameter.replace("%2f", "");
                parameter = parameter.replace("%2F", "");
                parameter = parameter.replace("%5c", "");
                parameter = parameter.replace("%5C", "");

            } else {
                parameter = parameter.replace(":", "");
                parameter = parameter.replace("/", "");
                parameter = parameter.replace("\\\\", "");
                parameter = parameter.replace("\\.", "");
                parameter = parameter.replace("%2e", "");
                parameter = parameter.replace("%2E", "");
                parameter = parameter.replace("%2f", "");
                parameter = parameter.replace("%2F", "");
                parameter = parameter.replace("%5c", "");
                parameter = parameter.replace("%5C", "");
            }
        }

        return parameter;
    }

    public static String cleanFilename(String filename) {
        if (filename != null) {
            filename = filename.replace(":", "").replace("/", "").replace("\\\\", "").replace("\\.", "");
            filename = filename.replace("%2e", "").replace("%2E", "").replace("%2f", "").replace("%2F", "").replace("%5c", "").replace("%5C", "");
            filename = filename.replace('\n', '_').replace('\r', '_');
        }

        return filename;
    }

    public static String normalize(String input) {
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output;
    }

    public static String getFileExtension(String fileName) {
        String extensionArchivo;
        int dotPos = fileName.lastIndexOf(".");
        extensionArchivo = fileName.substring(dotPos);
        return extensionArchivo;
    }

    public static String validaStrNull(String cadena) {

        return cadena != null ? cadena : null;

    }

}
