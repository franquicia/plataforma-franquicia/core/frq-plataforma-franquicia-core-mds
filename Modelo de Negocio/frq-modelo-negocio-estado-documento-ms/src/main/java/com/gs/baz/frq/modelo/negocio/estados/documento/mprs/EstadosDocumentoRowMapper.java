/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.mprs;

import com.gs.baz.frq.modelo.negocio.estados.documento.dto.EstadoDocumento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EstadosDocumentoRowMapper implements RowMapper<EstadoDocumento> {

    private EstadoDocumento modelo;

    @Override
    public EstadoDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
        modelo = new EstadoDocumento();
        modelo.setIdEstadoDocumento(rs.getInt("FIIDEDODOCTO"));
        modelo.setDescripcion(rs.getString("FCDESCRIPCION"));
        modelo.setNombre(rs.getString("FCNOMBRE"));
        return modelo;
    }
}
