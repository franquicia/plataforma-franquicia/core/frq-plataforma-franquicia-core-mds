package com.gs.baz.disciplinas.eje.dao;

import com.gs.baz.disciplinas.eje.dao.util.GenericDAO;
import com.gs.baz.disciplinas.eje.dto.DisciplinaEjeDTO;
import com.gs.baz.disciplinas.eje.mprs.DisciplinasEjeRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DisciplinasEjeDAOImpl extends DefaultDAO implements GenericDAO<DisciplinaEjeDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINEJEDISC");
        jdbcInsert.withProcedureName("SP_INS_EJEDISC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINEJEDISC");
        jdbcUpdate.withProcedureName("SP_ACT_EJEDISC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINEJEDISC");
        jdbcDelete.withProcedureName("SP_DEL_EJEDISC");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINEJEDISC");
        jdbcSelect.withProcedureName("SP_SEL_EJEDISC");
        jdbcSelect.returningResultSet("RCL_INFO", new DisciplinasEjeRowMapper());
    }

    @Override
    public DisciplinaEjeDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODEJE", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<DisciplinaEjeDTO> data = (List<DisciplinaEjeDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<DisciplinaEjeDTO> selectRowsDisciplinasEje(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODEJE", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<DisciplinaEjeDTO> data = (List<DisciplinaEjeDTO>) out.get("RCL_INFO");
        return data;
    }

    @Override
    public List<DisciplinaEjeDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODEJE", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<DisciplinaEjeDTO> data = (List<DisciplinaEjeDTO>) out.get("RCL_INFO");
        return data;
    }

    @Override
    public DisciplinaEjeDTO insertRow(DisciplinaEjeDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODEJE", entityDTO.getIdModeloEje());
            mapSqlParameterSource.addValue("PA_FIIDDISC", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FIIDTPMEDICION", entityDTO.getIdTipoMedicion());
            mapSqlParameterSource.addValue("PA_FIIDFRECMED", entityDTO.getIdFrecuenciaMedicion());
            mapSqlParameterSource.addValue("PA_FIIDEJECUTA", entityDTO.getIdEjecuta());
            mapSqlParameterSource.addValue("PA_FBEXPERIENCIA", entityDTO.getDriverExperiencia());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdDisciplinaEje((BigDecimal) out.get("PA_FIIDEJEDIS"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Disciplina Modelo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Disciplina Modelo"), ex);
        }
    }

    @Override
    public DisciplinaEjeDTO updateRow(DisciplinaEjeDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDEJEDIS", entityDTO.getIdDisciplinaEje());
            mapSqlParameterSource.addValue("PA_FIIDMODEJE", entityDTO.getIdModeloEje());
            mapSqlParameterSource.addValue("PA_FIIDDISC", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FIIDTPMEDICION", entityDTO.getIdTipoMedicion());
            mapSqlParameterSource.addValue("PA_FIIDFRECMED", entityDTO.getIdFrecuenciaMedicion());
            mapSqlParameterSource.addValue("PA_FIIDEJECUTA", entityDTO.getIdEjecuta());
            mapSqlParameterSource.addValue("PA_FBEXPERIENCIA", entityDTO.getDriverExperiencia());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Disciplina Modelo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Disciplina Modelo"), ex);
        }
    }

    @Override
    public DisciplinaEjeDTO deleteRow(DisciplinaEjeDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDEJEDIS", entityDTO.getIdDisciplinaEje());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Disciplina Modelo Success"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception to delete row of Disciplina Modelo"), ex);
        }
    }
}
