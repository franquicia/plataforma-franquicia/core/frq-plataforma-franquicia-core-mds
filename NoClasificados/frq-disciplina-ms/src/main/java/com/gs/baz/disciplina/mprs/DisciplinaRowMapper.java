package com.gs.baz.disciplina.mprs;

import com.gs.baz.disciplina.dto.DisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DisciplinaRowMapper implements RowMapper<DisciplinaDTO> {

    private DisciplinaDTO status;

    @Override
    public DisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new DisciplinaDTO();
        status.setIdDisciplina(((BigDecimal) rs.getObject("FIIDDISCI")));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        return status;
    }
}
