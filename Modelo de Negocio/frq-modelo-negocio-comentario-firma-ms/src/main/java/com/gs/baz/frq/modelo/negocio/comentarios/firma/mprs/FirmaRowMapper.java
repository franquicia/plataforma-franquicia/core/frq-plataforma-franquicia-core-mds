/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs;

import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.FirmaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FirmaRowMapper implements RowMapper<FirmaBase> {

    private FirmaBase firmaBase;

    @Override
    public FirmaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        firmaBase = new FirmaBase();
        firmaBase.setIdFolio(rs.getInt("FIIDFOLIO"));
        firmaBase.setNumeroEmpleado(rs.getInt("FINUMEROEMPLEADO"));
        firmaBase.setIdJerarquia(rs.getInt("FIIDJERARQUIA"));
        firmaBase.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        firmaBase.setEliminado(rs.getInt("FIELIMINADO"));
        return firmaBase;
    }
}
