/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del ProyectoDocumento", value = "ProyectoDocumento")
public class ProyectoDocumento extends ProyectoDocumentoBase {

    @JsonProperty(value = "idProyectoDocumento")
    @ApiModelProperty(notes = "Identificador del ProyectoDocumento", example = "1", position = -1)
    private Integer idProyectoDocumento;

    public Integer getIdProyectoDocumento() {
        return idProyectoDocumento;
    }

    public void setIdProyectoDocumento(Integer idProyectoDocumento) {
        this.idProyectoDocumento = idProyectoDocumento;
    }
}
