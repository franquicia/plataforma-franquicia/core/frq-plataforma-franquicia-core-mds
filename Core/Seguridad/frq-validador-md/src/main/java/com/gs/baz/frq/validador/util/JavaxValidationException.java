/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class JavaxValidationException extends Exception {

    private List<String> violations;

    public JavaxValidationException() {
    }

    public JavaxValidationException(String message) {
        super(message);
    }

    public JavaxValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public JavaxValidationException(Throwable cause) {
        super(cause);
    }

    public List<String> getViolations() {
        return violations;
    }

    public void setViolations(List<String> violations) {
        this.violations = violations;
    }

}
