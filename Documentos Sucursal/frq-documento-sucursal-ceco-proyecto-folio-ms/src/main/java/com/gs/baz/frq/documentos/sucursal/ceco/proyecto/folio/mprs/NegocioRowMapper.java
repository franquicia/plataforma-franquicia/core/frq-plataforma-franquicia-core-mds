/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.NegocioBase;

/**
 *
 * @author carlos
 */
public class NegocioRowMapper implements RowMapper<NegocioBase> {

    private NegocioBase negocioBase;

    @Override
    public NegocioBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocioBase = new NegocioBase();
        negocioBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        return negocioBase;
    }
}
