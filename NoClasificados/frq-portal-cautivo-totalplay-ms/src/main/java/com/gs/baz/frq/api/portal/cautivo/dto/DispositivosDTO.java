/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;

/**
 *
 * @author MADA
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DispositivosDTO {

    @JsonProperty("dispositivos")
    @ApiModelProperty(notes = "Dispositivos AP registrados en Extreme Cloud")
    private ArrayList<IdentificadorDTO> dispositivoDTO;

    public ArrayList<IdentificadorDTO> getDispositivoDTO() {
        return dispositivoDTO;
    }

    public void setDispositivoDTO(ArrayList<IdentificadorDTO> dispositivoDTO) {
        this.dispositivoDTO = dispositivoDTO;
    }

}
