package com.gs.baz.frq.dashboard.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.dashboard.dao.DashboardDAOImpl;
import com.gs.baz.frq.dashboard.dao.filtros.FiltrosDashboardDAOImpl;
import com.gs.baz.frq.dashboard.dao.graficos.g001.DashboardDAOImpl001;
import com.gs.baz.frq.dashboard.dao.graficos.g002.DashboardDAOImpl002;
import com.gs.baz.frq.dashboard.dao.graficos.g003.DashboardDAOImpl003;
import com.gs.baz.frq.dashboard.dao.graficos.g004.DashboardDAOImpl004;
import com.gs.baz.frq.dashboard.dao.graficos.g005.DashboardDAOImpl005;
import com.gs.baz.frq.dashboard.dao.graficos.g006.DashboardDAOImpl006;
import com.gs.baz.frq.dashboard.dao.graficos.g007.DashboardDAOImpl007;
import com.gs.baz.frq.dashboard.dao.graficos.g008.DashboardDAOImpl008;
import com.gs.baz.frq.dashboard.dao.graficos.g009.DashboardDAOImpl009;
import com.gs.baz.frq.dashboard.dao.graficos.g010.DashboardDAOImpl010;
import com.gs.baz.frq.dashboard.dao.graficos.g011.DashboardDAOImpl011;
import com.gs.baz.frq.dashboard.dao.graficos.g012.DashboardDAOImpl012;
import com.gs.baz.frq.dashboard.dao.graficos.g013.DashboardDAOImpl013;
import com.gs.baz.frq.dashboard.dao.graficos.g014.DashboardDAOImpl014;
import com.gs.baz.frq.dashboard.dao.graficos.g015.DashboardDAOImpl015;
import com.gs.baz.frq.dashboard.dao.graficos.g016.DashboardDAOImpl016;
import com.gs.baz.frq.dashboard.dao.parametros.ParametrosDAOImpl;
import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.dashboard")
public class ConfigDashboard {

    private final Logger logger = LogManager.getLogger();

    public ConfigDashboard() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "dashboardDAOImpl")
    public DashboardDAOImpl dashboardDAOImpl() {
        return new DashboardDAOImpl();
    }

    @Bean(initMethod = "init")
    public FiltrosDashboardDAOImpl filtrosDashboardDAOImpl() {
        return new FiltrosDashboardDAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl001 dashboardDAOImpl001() {
        return new DashboardDAOImpl001();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl002 dashboardDAOImpl002() {
        return new DashboardDAOImpl002();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl003 dashboardDAOImpl003() {
        return new DashboardDAOImpl003();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl004 dashboardDAOImpl004() {
        return new DashboardDAOImpl004();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl005 dashboardDAOImpl005() {
        return new DashboardDAOImpl005();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl006 dashboardDAOImpl006() {
        return new DashboardDAOImpl006();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl007 dashboardDAOImpl007() {
        return new DashboardDAOImpl007();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl008 dashboardDAOImpl008() {
        return new DashboardDAOImpl008();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl009 dashboardDAOImpl009() {
        return new DashboardDAOImpl009();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl010 dashboardDAOImpl010() {
        return new DashboardDAOImpl010();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl011 dashboardDAOImpl011() {
        return new DashboardDAOImpl011();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl012 dashboardDAOImpl012() {
        return new DashboardDAOImpl012();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl013 dashboardDAOImpl013() {
        return new DashboardDAOImpl013();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl014 dashboardDAOImpl014() {
        return new DashboardDAOImpl014();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl015 dashboardDAOImpl015() {
        return new DashboardDAOImpl015();
    }

    @Bean(initMethod = "init")
    public DashboardDAOImpl016 dashboardDAOImpl016() {
        return new DashboardDAOImpl016();
    }

    @Bean(initMethod = "init")
    public ParametrosDAOImpl parametrosDAOImpl() {
        return new ParametrosDAOImpl();
    }

    @Bean(initMethod = "init")
    public PeriodoDAOImpl PeriodoDAOImpl() {
        return new PeriodoDAOImpl();
    }

}
