package com.gs.baz.frq.dashboard.mprs.filtro;

import com.gs.baz.frq.dashboard.dto.filtros.FiltroCecoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FiltroCecoRowMapper implements RowMapper<FiltroCecoDTO> {

    private FiltroCecoDTO cecoDTO;

    @Override
    public FiltroCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cecoDTO = new FiltroCecoDTO();
        cecoDTO.setIdCeco(rs.getString("IDGENERAL"));
        cecoDTO.setDescripcion(rs.getString("DESCRIPCION"));
        return cecoDTO;
    }
}
