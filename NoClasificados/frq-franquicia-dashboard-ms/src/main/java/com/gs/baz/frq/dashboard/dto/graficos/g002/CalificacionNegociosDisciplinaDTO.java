/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g002;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author B73601
 */
public class CalificacionNegociosDisciplinaDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_final")
    private Integer calificacionFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocios")
    private List<NegocioDTO> negocios;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public CalificacionNegociosDisciplinaDTO() {
    }

    public CalificacionNegociosDisciplinaDTO(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getCalificacionFinal() {
        return calificacionFinal;
    }

    public void setCalificacionFinal(BigDecimal calificacionFinal) {
        this.calificacionFinal = (calificacionFinal == null ? null : calificacionFinal.intValue());
    }

    public List<NegocioDTO> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<NegocioDTO> negocios) {
        this.negocios = negocios;
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "DisciplinaDTO{" + "idDisciplina=" + idDisciplina + ", calificacionFinal=" + calificacionFinal + ", negocios=" + negocios + ", filtros=" + filtros + '}';
    }

}
