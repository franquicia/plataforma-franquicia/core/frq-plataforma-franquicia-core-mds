/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author b191312
 */
public class ParametrosDocumentoFolio {

    @JsonProperty(value = "folio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private Integer folio;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del documento", example = "1", position = -1)
    private Integer idDocumento;

    @JsonProperty(value = "idArchivo")
    @ApiModelProperty(notes = "Identificador del archivo", example = "1", position = -1)
    private Integer idArchivo;

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

}
