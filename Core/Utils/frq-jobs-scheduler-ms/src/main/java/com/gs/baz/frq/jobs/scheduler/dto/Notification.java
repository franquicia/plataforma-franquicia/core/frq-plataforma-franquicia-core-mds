/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author cescobarh
 */
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "on_error_mails")
    private String onErrorMails;

    @JsonProperty(value = "on_success_mails")
    private String onSuccessMails;

    @JsonProperty(value = "on_always_mails")
    private String onAlwaysMails;

    public Notification(String onErrorMails, String onSuccessMails, String onAlwaysMails) {
        this.onErrorMails = onErrorMails;
        this.onSuccessMails = onSuccessMails;
        this.onAlwaysMails = onAlwaysMails;
    }

    public String getOnErrorMails() {
        return onErrorMails;
    }

    public void setOnErrorMails(String onErrorMails) {
        this.onErrorMails = onErrorMails;
    }

    public String getOnSuccessMails() {
        return onSuccessMails;
    }

    public void setOnSuccessMails(String onSuccessMails) {
        this.onSuccessMails = onSuccessMails;
    }

    public String getOnAlwaysMails() {
        return onAlwaysMails;
    }

    public void setOnAlwaysMails(String onAlwaysMails) {
        this.onAlwaysMails = onAlwaysMails;
    }

    @Override
    public String toString() {
        return "Notification{" + "onErrorMails=" + onErrorMails + ", onSuccessMails=" + onSuccessMails + ", onAlwaysMails=" + onAlwaysMails + '}';
    }

}
