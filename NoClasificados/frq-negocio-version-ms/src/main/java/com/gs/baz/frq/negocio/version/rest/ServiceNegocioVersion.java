/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.negocio.version.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.negocio.version.dao.NegocioVersionDAOImpl;
import com.gs.baz.frq.negocio.version.dto.NegocioVersionDTO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/negoVers")
@Component("FRQServiceNegocioVersion")
public class ServiceNegocioVersion {

    @Autowired
    private NegocioVersionDAOImpl negocioVersionDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    //negocio
    @RequestMapping(value = "secure/post/negocio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioVersionDTO> getProyectosCeco(@RequestBody NegocioVersionDTO negocio) throws CustomException {

        return (List<NegocioVersionDTO>) negocioVersionDAOImpl.selectRowDTO(negocio);

    }

    @RequestMapping(value = "secure/post/alta/negocio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaNegocio(@RequestBody NegocioVersionDTO asignacion) throws CustomException {

        if (asignacion.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Negocio-Supervison"));
        }
        return negocioVersionDAOImpl.insertRow(asignacion);
    }

    @RequestMapping(value = "secure/put/negocio", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioVersionDTO putUpdateNegocio(@RequestBody NegocioVersionDTO updateNego) throws CustomException {
        if (updateNego.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update Negocio-Supervison"));
        }

        return negocioVersionDAOImpl.updateRow(updateNego);
    }

    @RequestMapping(value = "secure/delete/negocio", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaNegocio(@RequestBody NegocioVersionDTO elimina) throws CustomException {
        if (elimina.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Dep Negocio-Supervison"));
        }

        return negocioVersionDAOImpl.eliminaRows(elimina);
    }
    //termina negocio

    //Versiones Sup
    @RequestMapping(value = "secure/post/version", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioVersionDTO> getVersionSup(@RequestBody NegocioVersionDTO versiones) throws CustomException {

        return (List<NegocioVersionDTO>) negocioVersionDAOImpl.selectVersionesSup(versiones);

    }

    @RequestMapping(value = "secure/post/alta/version", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaVersionSup(@RequestBody NegocioVersionDTO version) throws CustomException {

        if (version.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta version-Supervison"));
        }
        return negocioVersionDAOImpl.insertVersionSup(version);
    }

    @RequestMapping(value = "secure/put/version", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioVersionDTO putVersionSup(@RequestBody NegocioVersionDTO updateVersion) throws CustomException {
        if (updateVersion.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update version-Supervison"));
        }

        return negocioVersionDAOImpl.updateVersionSup(updateVersion);
    }

    @RequestMapping(value = "secure/delete/version", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaVersionSup(@RequestBody NegocioVersionDTO elimina) throws CustomException {
        if (elimina.getParametro() == null && elimina.getIdVersion() != null) {
            return negocioVersionDAOImpl.eliminaVersionSup(elimina);

        } else if (elimina.getIdVersion() == null && elimina.getParametro() != null) {
            return negocioVersionDAOImpl.eliminaVersionSup(elimina);

        } else {

            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("delete version-Supervison"));
        }
    }
    //consultas especificas ceco

    @RequestMapping(value = "secure/post/cecos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioVersionDTO> getCecos(@RequestBody NegocioVersionDTO cecos) throws CustomException {

        return (List<NegocioVersionDTO>) negocioVersionDAOImpl.selectCecos(cecos);

    }

    @RequestMapping(value = "secure/post/usuarios", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioVersionDTO> getUsuarios(@RequestBody NegocioVersionDTO usuarios) throws CustomException {

        return (List<NegocioVersionDTO>) negocioVersionDAOImpl.selectUsuarios(usuarios);

    }

}
