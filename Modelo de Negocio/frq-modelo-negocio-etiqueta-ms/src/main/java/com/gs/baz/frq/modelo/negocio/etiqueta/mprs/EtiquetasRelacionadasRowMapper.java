/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.mprs;

import com.gs.baz.frq.modelo.negocio.etiqueta.dto.Etiqueta;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EtiquetasRelacionadasRowMapper implements RowMapper<Etiqueta> {

    private Etiqueta nivel;

    @Override
    public Etiqueta mapRow(ResultSet rs, int rowNum) throws SQLException {
        nivel = new Etiqueta();
        nivel.setIdEtiqueta(rs.getInt("FIIDETIQUETA"));
        nivel.setDescripcion(rs.getString("FCDESCRIPCION"));
        return nivel;
    }
}
