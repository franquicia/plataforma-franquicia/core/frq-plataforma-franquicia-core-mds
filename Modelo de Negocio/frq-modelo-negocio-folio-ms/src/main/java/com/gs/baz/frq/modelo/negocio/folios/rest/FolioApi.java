/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.folios.bi.CorreoBI;
import com.gs.baz.frq.modelo.negocio.folios.client.bi.ArchivosVirtualesBI;
import com.gs.baz.frq.modelo.negocio.folios.client.bi.FirmasBI;
import com.gs.baz.frq.modelo.negocio.folios.client.bi.UsuariosBI;
import com.gs.baz.frq.modelo.negocio.folios.dao.FolioComentarioDAOImpl;
import com.gs.baz.frq.modelo.negocio.folios.dao.FolioDAOImpl;
import com.gs.baz.frq.modelo.negocio.folios.dto.AltaFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioDocumento;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioResumen;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFoliosArbol;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFoliosAutorizacion;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFoliosEtiqueta;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFoliosFechas;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFoliosTop;
import com.gs.baz.frq.modelo.negocio.folios.dto.Comentario;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firmas;
import com.gs.baz.frq.modelo.negocio.folios.dto.Folio;
import com.gs.baz.frq.modelo.negocio.folios.dto.FolioBase;
import com.gs.baz.frq.modelo.negocio.folios.dto.Folios;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosActualizaEstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosActualizaIndicePasoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosComentarios;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosEnviaCorreoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosEstadoFolios;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "folios", value = "folios", description = "Gestión de folios")
@RestController
@RequestMapping("/api-local/modelo-negocio/folios/v1")
public class FolioApi {

    @Autowired
    private FolioDAOImpl folioDAOImpl;

    @Autowired
    private FolioComentarioDAOImpl folioComentarioDAOImpl;

    @Autowired
    private CorreoBI correoBI;

    @Autowired
    private ArchivosVirtualesBI folioBI;

    @Autowired
    private UsuariosBI usuariosBI;

    @Autowired
    private FirmasBI firmasBI;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folio", notes = "Obtiene un folio", nickname = "obtieneFolio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FolioBase obtieneFolio(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws CustomException, DataNotFoundException {
        FolioBase folioBase = folioDAOImpl.selectRow(idFolio);
        if (folioBase == null) {
            throw new DataNotFoundException();
        }
        return folioBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los folios", nickname = "obtieneFolios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Folios obtieneFolios() throws CustomException, DataNotFoundException {
        Folios folios = new Folios(folioDAOImpl.selectAllRows());
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param parametrosFolio
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear folio", notes = "Agrega un folio", nickname = "creaFolio")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFolio creaFolio(@ApiParam(name = "ParametrosFolio", value = "Paramentros para el alta del folio", required = true) @RequestBody ParametrosFolio parametrosFolio) throws DataNotInsertedException {
        return folioDAOImpl.insertRow(parametrosFolio);
    }

    /**
     *
     * @param parametrosFolio
     * @param idFolio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar folio", notes = "Actualiza un folio", nickname = "actualizaFolio")
    @RequestMapping(value = "/{idFolio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFolio(@ApiParam(name = "ParametrosFolio", value = "Paramentros para la actualización del folio", required = true) @RequestBody ParametrosFolio parametrosFolio, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotUpdatedException {
        parametrosFolio.setIdFolio(idFolio);
        folioDAOImpl.updateRow(parametrosFolio);
        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar folio", notes = "Elimina un item de los folios", nickname = "eliminaFolio")
    @RequestMapping(value = "/{idFolio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaFolio(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotDeletedException {
        folioDAOImpl.deleteRow(idFolio);
        return new SinResultado();
    }

    /**
     *
     * @param headers
     * @param parametrosFolio
     * @param idFolio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar estado folio", notes = "Actualiza estado de un folio", nickname = "actualizaEstadoFolio")
    @RequestMapping(value = "/{idFolio}/estados", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaEstadoFolio(@RequestHeader HttpHeaders headers, @ApiParam(name = "ParametrosEstadoFolio", value = "Paramentros para la actualización del estado del folio", required = true) @RequestBody ParametrosActualizaEstadoFolio parametrosFolio, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotUpdatedException, Exception {
        parametrosFolio.setIdFolio(idFolio);
        folioDAOImpl.updateRowEstado(parametrosFolio);
        correoBI.enviaCorreo(headers, idFolio, parametrosFolio.getIdEstadoFolio(), true);
        return new SinResultado();
    }

    /**
     *
     * @param headers
     * @param parametrosFolio
     * @param idFolio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar estado folio", notes = "Actualiza estado de un folio", nickname = "actualizaEstadoFolio")
    @RequestMapping(value = "/{idFolio}/estados/correo", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado enviaCorreo(
            @RequestHeader HttpHeaders headers,
            @ApiParam(name = "ParametrosEstadoFolio", value = "Paramentros para la actualización del estado del folio", required = true) @RequestBody ParametrosEnviaCorreoFolio parametrosFolio,
            @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotUpdatedException, Exception {
        parametrosFolio.setIdFolio(idFolio);
        correoBI.enviaCorreo(headers, idFolio, parametrosFolio.getIdEstadoFolio(), parametrosFolio.getNumeroEmpleado());
        return new SinResultado();
    }

    /**
     *
     * @param parametrosFolio
     * @param idFolio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar indice de paso del folio", notes = "Actualiza indice del paso de un folio", nickname = "actualizaIndicePasoFolio")
    @RequestMapping(value = "/{idFolio}/indice-paso", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaIndicePasoFolio(@ApiParam(name = "ParametrosIndicePasoFolio", value = "Paramentros para la actualización del indice del paso del folio", required = true) @RequestBody ParametrosActualizaIndicePasoFolio parametrosFolio, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotUpdatedException {
        parametrosFolio.setIdFolio(idFolio);
        folioDAOImpl.updateRowIndicePaso(parametrosFolio);
        return new SinResultado();
    }

    /**
     *
     * @param headers
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene resumen documento", notes = "Obtiene un resumen documento", nickname = "obtieneResumenDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/resumen", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFolioResumen obtieneResumenDocumento(@RequestHeader HttpHeaders headers, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws CustomException, DataNotFoundException {
        BusquedaFolioResumen resumenDocumento = folioDAOImpl.selectResumenDocumento(idFolio);
        try {
            Empleado empleado = usuariosBI.getUsuario(headers, resumenDocumento.getCreador().getNumeroEmpleado());
            Firmas firmas = firmasBI.getFirmasFolio(headers, idFolio);
            resumenDocumento.setCreador(empleado);
            resumenDocumento.setFirmas(firmas);
        } catch (Exception ex) {
            resumenDocumento = null;
        }
        if (resumenDocumento == null) {
            throw new DataNotFoundException();
        }
        return resumenDocumento;
    }

    /**
     *
     * @param numeroEmpleado
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ultimo folio por usuario", notes = "Obtiene ultimo folio por usuario", nickname = "obtieneUltimoFolio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Folio obtieneUltimoFolio(@ApiParam(name = "numeroEmpleado", value = "Numero de empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        Folio folio = folioDAOImpl.selectObtieneUltimoFolio(numeroEmpleado);
        if (folio == null) {
            throw new DataNotFoundException();
        }
        return folio;
    }

    /**
     *
     * @param idFolio
     * @param numeroEmpleado
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.CustomException
     */
    @ApiOperation(value = "Obtiene comentarios del folio por usuario", notes = "Obtiene comentarios del folio por usuario", nickname = "obtieneComentarioUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/comentarios/usuarios/{numeroEmpleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Comentario obtieneComentarioUsuario(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio, @ApiParam(name = "numeroEmpleado", value = "Numero de empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws DataNotFoundException, CustomException {
        Comentario comentario = folioComentarioDAOImpl.selectRow(idFolio, numeroEmpleado);
        if (comentario == null) {
            throw new DataNotFoundException();
        }
        return comentario;
    }

    /**
     *
     * @param idFolio
     * @param idComentario
     * @param numeroEmpleado
     * @param parametrosComentarios
     * @return
     * @throws com.gs.baz.frq.model.commons.CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @RequestMapping(value = "/{idFolio}/comentarios/{idComentario}/usuarios/{numeroEmpleado}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaComentarioUsuario(
            @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "idComentario", value = "Identificador del comentario", example = "1", required = true) @PathVariable("idComentario") Integer idComentario,
            @ApiParam(name = "numeroEmpleado", value = "Número de empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado,
            @ApiParam(name = "parametrosComentarios", value = "Paramentros para la actualización del comentario", required = true) @RequestBody ParametrosComentarios parametrosComentarios) throws CustomException, DataNotUpdatedException {
        parametrosComentarios.setIdFolio(idFolio);
        parametrosComentarios.setNumeroEmpleado(numeroEmpleado);
        parametrosComentarios.setIdComentario(idComentario);
        folioComentarioDAOImpl.updateRow(parametrosComentarios);
        return new SinResultado();
    }

    /**
     *
     * @param parametrosFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por fecha", notes = "Obtiene un folios por fecha", nickname = "obtieneFoliosPorFecha")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/fechas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosFechas obtieneFoliosPorFecha(@ApiParam(name = "ParametrosEstadoFolios", value = "Paramentros para consulta el estado de los folios por fecha", required = true) @RequestBody ParametrosEstadoFolios parametrosFolio) throws CustomException, DataNotFoundException {
        BusquedaFoliosFechas folios = new BusquedaFoliosFechas(folioDAOImpl.selectEstadoFolios(parametrosFolio));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param idEtiqueta
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por etiquetas", notes = "Obtiene todos los folios por etiqueta", nickname = "obtieneFoliosEtiqueta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/etiquetas/{idEtiqueta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosEtiqueta obtieneFoliosEtiqueta(@ApiParam(name = "idEtiqueta", value = "Identificador del etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Long idEtiqueta) throws CustomException, DataNotFoundException {
        BusquedaFoliosEtiqueta folios = new BusquedaFoliosEtiqueta(folioDAOImpl.selectFoliosEtiqueta(idEtiqueta));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param idModeloNegocio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por modelo de negocio", notes = "Obtiene todos los folios por modelo de negocio", nickname = "obtieneFoliosNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/modelos-negocio/{idModeloNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosArbol obtieneFoliosNivel(@ApiParam(name = "idModeloNegocio", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idModeloNegocio) throws CustomException, DataNotFoundException {
        BusquedaFoliosArbol busquedaFoliosArbol = new BusquedaFoliosArbol(folioDAOImpl.selectFoliosNivel(idModeloNegocio));
        if (busquedaFoliosArbol.getFolios() != null && busquedaFoliosArbol.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return busquedaFoliosArbol;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por modelo de negocio", notes = "Obtiene todos los folios por modelo de negocio", nickname = "obtieneFoliosNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/documentos/autorizacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosAutorizacion obtieneFoliosUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosAutorizacion busquedaFoliosUsuario = new BusquedaFoliosAutorizacion(folioDAOImpl.selectFoliosPorAutorizarUsuario(numeroEmpleado));
        if (busquedaFoliosUsuario.getFolios() != null && busquedaFoliosUsuario.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return busquedaFoliosUsuario;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los folios", nickname = "obtieneFolios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/documentos/publicados/mejor-calificados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosTop obtieneTopDocumentosCalificaciones(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosTop folios = new BusquedaFoliosTop(folioDAOImpl.selectTopDocumentosCalificados(numeroEmpleado));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los documentos recientes", nickname = "obtieneTopDocumentosRecientes")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/documentos/publicados/recientes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosTop obtieneTopDocumentosRecientes(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosTop folios = new BusquedaFoliosTop(folioDAOImpl.selectTopDocumentosRecientes(numeroEmpleado));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los documentos ultima semana", nickname = "obtieneTopDocumentosUltimaSemana")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/documentos/publicados/ultima-semana", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosTop obtieneTopDocumentosUltimaSemana(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosTop folios = new BusquedaFoliosTop(folioDAOImpl.selectTopDocumentosUltimaSemana(numeroEmpleado));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los documentos ultima semana", nickname = "obtieneTopDocumentosUltmiosSemana")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/documentos/publicados/ultimo-mes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosTop obtieneTopDocumentosUltimoMes(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosTop folios = new BusquedaFoliosTop(folioDAOImpl.selectTopDocumentosUltimoMes(numeroEmpleado));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los documentos ultima semana", nickname = "obtieneTopDocumentosMasVistos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/documentos/publicados/mas-vistos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosTop obtieneTopDocumentosMasVistos(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosTop folios = new BusquedaFoliosTop(folioDAOImpl.selectTopDocumentosMasVistos(numeroEmpleado));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param numeroEmpleado
     * @param idEtiqueta
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por etiquetas", notes = "Obtiene todos los folios por etiqueta", nickname = "obtieneFoliosEtiqueta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/etiquetas/{idEtiqueta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosEtiqueta obtieneFoliosEtiquetaEmpleado(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado, @ApiParam(name = "idEtiqueta", value = "Identificador del etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Long idEtiqueta) throws CustomException, DataNotFoundException {
        BusquedaFoliosEtiqueta folios = new BusquedaFoliosEtiqueta(folioDAOImpl.selectFoliosEtiquetaEmpleado(numeroEmpleado, idEtiqueta));
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param numeroEmpleado
     * @param idModeloNegocio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por modelo de negocio", notes = "Obtiene todos los folios por modelo de negocio", nickname = "obtieneFoliosNivelEmpleado")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/usuarios/{numeroEmpleado}/modelos-negocio/{idModeloNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosArbol obtieneFoliosNivelEmpleado(@ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado, @ApiParam(name = "idModeloNegocio", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idModeloNegocio) throws CustomException, DataNotFoundException {
        BusquedaFoliosArbol busquedaFoliosArbol = new BusquedaFoliosArbol(folioDAOImpl.selectFoliosNivelEmpleado(numeroEmpleado, idModeloNegocio));
        if (busquedaFoliosArbol.getFolios() != null && busquedaFoliosArbol.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return busquedaFoliosArbol;
    }

    /**
     *
     * @param idFolio
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por modelo de negocio", notes = "Obtiene todos los folios por modelo de negocio", nickname = "obtieneFoliosNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/busquedas/usuarios/{numeroEmpleado}/documentos/relacionados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFoliosAutorizacion obtieneFoliosUsuarioRelacionadados(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio, @ApiParam(name = "numeroEmpleado", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws CustomException, DataNotFoundException {
        BusquedaFoliosAutorizacion busquedaFoliosUsuario = new BusquedaFoliosAutorizacion(folioDAOImpl.selectFoliosUsuarioRelacionados(idFolio, numeroEmpleado));
        if (busquedaFoliosUsuario.getFolios() != null && busquedaFoliosUsuario.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return busquedaFoliosUsuario;
    }

    /**
     *
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene informacion del documento", notes = "Obtiene informacion del documento", nickname = "obtieneInformacionDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/documentos/{idFolio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BusquedaFolioDocumento obtieneInformacionDocumento(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        BusquedaFolioDocumento busquedaDocumento = folioDAOImpl.selectDocumento(idFolio);
        if (busquedaDocumento == null) {
            throw new DataNotFoundException();
        }
        return busquedaDocumento;
    }

    /**
     *
     * @param headers
     * @param idFolio
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Duplica folio", notes = "Duplica un folio", nickname = "duplicaFolio")
    @RequestMapping(value = "/{idFolio}/duplica-folio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFolio duplicaFolio(@RequestHeader HttpHeaders headers, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotInsertedException, Exception {
        AltaFolio altaFolio = folioDAOImpl.duplicaFolio(new Long("" + idFolio));
        if (altaFolio != null) {
            folioBI.duplicaArchivos(headers, idFolio, altaFolio.getId());
        }
        return altaFolio;
    }
}
