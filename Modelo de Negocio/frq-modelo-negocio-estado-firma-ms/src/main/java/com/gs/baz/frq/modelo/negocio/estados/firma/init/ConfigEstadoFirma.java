package com.gs.baz.frq.modelo.negocio.estados.firma.init;

import com.gs.baz.frq.modelo.negocio.estados.firma.dao.EstadoFirmaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.estados.firma")
public class ConfigEstadoFirma {

    private final Logger logger = LogManager.getLogger();

    public ConfigEstadoFirma() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public EstadoFirmaDAOImpl estadoFirmaDAOImpl() {
        return new EstadoFirmaDAOImpl();
    }

}
