/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de la tabla dato Zona", value = "DatoZonaBase")
public class DatoZonaBase {

    @JsonProperty(value = "observaciones")
    @ApiModelProperty(notes = "Observaciones de la zona base", example = "N/A")
    private String observaciones;

    @JsonProperty(value = "valor")
    @ApiModelProperty(notes = "Valor de la zona base", example = "100 mts")
    private String valor;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador de Folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "idCatalogoZona")
    @ApiModelProperty(notes = "Identificador de catalogo zona", example = "1")
    private Integer idCatalogoZona;

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdCatalogoZona() {
        return idCatalogoZona;
    }

    public void setIdCatalogoZona(Integer idCatalogoZona) {
        this.idCatalogoZona = idCatalogoZona;
    }

}
