package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

@ApiModel(value = "ResponseConsulta", description = "Objeto que contiene una lista con los resultados de la consulta")
public class ResponseConsulta {

    public ResponseConsulta(List<CecoProyectoFolio> data) {

        this.data = data;

    }

    @JsonProperty(value = "proyectosCentroCosto")
    @ApiModelProperty(notes = "Lista con el resultado de la busqueda de la tabla FRTAPROYFOLIO")
    private List<CecoProyectoFolio> data;

    public List<CecoProyectoFolio> getData() {
        return data;
    }

    public void setData(List<CecoProyectoFolio> data) {
        this.data = data;
    }

}
