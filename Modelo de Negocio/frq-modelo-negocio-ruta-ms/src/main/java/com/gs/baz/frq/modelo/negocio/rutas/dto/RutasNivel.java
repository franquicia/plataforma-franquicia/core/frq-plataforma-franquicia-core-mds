/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de rutasNivel del documento", value = "rutasNivel")
public class RutasNivel {

    @JsonProperty(value = "rutasNivel")
    @ApiModelProperty(notes = "rutasNivel")
    private List<RutaNivel> rutasNivel;

    public RutasNivel(List<RutaNivel> rutasNivel) {
        this.rutasNivel = rutasNivel;
    }

    public List<RutaNivel> getRutasNivel() {
        return rutasNivel;
    }

    public void setRutasNivel(List<RutaNivel> rutasNivel) {
        this.rutasNivel = rutasNivel;
    }

    @Override
    public String toString() {
        return "RutasNivel{" + "rutasNivel=" + rutasNivel + '}';
    }

}
