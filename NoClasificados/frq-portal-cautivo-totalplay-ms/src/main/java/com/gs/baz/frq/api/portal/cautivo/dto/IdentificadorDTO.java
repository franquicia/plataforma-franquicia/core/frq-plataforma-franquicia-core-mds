/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class IdentificadorDTO {

    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "fechaEvento")
    private String fechaEvento;

    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonProperty(value = "tienda")
    private String tienda;

    @JsonProperty(value = "mac")
    private String mac;

    @JsonProperty(value = "identificadorCliente")
    private String identificadorCliente;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(String fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIdentificadorCliente() {
        return identificadorCliente;
    }

    public void setIdentificadorCliente(String identificadorCliente) {
        this.identificadorCliente = identificadorCliente;
    }

}
