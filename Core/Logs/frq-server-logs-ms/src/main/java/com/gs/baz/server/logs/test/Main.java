package com.gs.baz.server.logs.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.ProcessMonitor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author karlo
 */
public class Main {

    private int lineCount;
    private String separator;
    private final static ProcessMonitor processMonitor = new ProcessMonitor("Main");
    private ObjectMapper objectMapper = new ObjectMapper();
    private final Logger logger = LogManager.getLogger();
    private static final String[] logsWarn = {"El parametro es opcional", "Se cargaron las configuraciones por defecto", "La contraseña no se logro desencriptar"};
    private static final String[] logsInfo = {"Todo correcto, se insertaron registros en base", "No se encontraron resultados para esta consulta", "Todo ok en el proceso de sesion"};
    private static final String[] logsError = {"Hay un error warning en la le ejecucion del proceso", "Ocurrio un error de tipo warning", "Algo paso al ejecutar el proceso"};
    private static final String[] logsDebug = {"La lista contiene 5 elementos", "Entro al proceso de actualizacion de registros", "Se elimino correctamente el registro"};
    private static final String[] logsFatal = {"No fue posible ejecutar la sentencia", "El proceso se detuvo y no se completo", "Ocurrio un error inesperado"};

    public static void main(String[] args) throws IOException, URISyntaxException, Throwable {
        //URL file = Main.class.getResource("/data2.txt");
        Main m = new Main();
        m.testLog(1000000);
        //processMonitor.startWatch("readFileFromInputStream");
        //String content = m.readFileFromInputStream(file.openStream());
        //processMonitor.stopWatch();

        //System.out.println(content);
        //System.out.println("Metodo 2\n");
        // processMonitor.startWatch("readFileLines");
        //String content2 = m.readFileLines(file.toURI());
        //processMonitor.stopWatch();
        //processMonitor.startWatch("readAllLines");
        //String content3 = m.readAllLines(file.toURI());
        //processMonitor.stopWatch();
        //processMonitor.startWatch("writing");
        //m.writeFile3(content3);
        //processMonitor.stopWatch();
        //processMonitor.resumeInfo();
        //System.out.println(content3);
        //m.writeFile(content3);
        //System.out.println(content2);
        /*String nl = "\n";
         int lines = 100;
         for (int i = 0; i < lines; i++) {
         if (i == lines - 1) {
         nl = "";
         }
         m.writeFile2(content2 + nl);
         }*/
    }

    public void testLog(int totalLogs) throws Throwable {
        logger.debug("Hello from Log4j 2");
        logger.debug("This is a Debug Message!");
        logger.info("This is an Info Message!");

        for (int i = 0; i < totalLogs; i++) {
            try {
                double randomDouble = Math.random();
                randomDouble = randomDouble * 7 + 1;
                int randomInt = (int) randomDouble;

                if (randomInt == 1) {
                    randomDouble = Math.random();
                    randomDouble = randomDouble * 3;
                    randomInt = (int) randomDouble;
                    logger.log(Level.WARN, logsWarn[randomInt]);
                }
                if (randomInt == 2) {
                    randomDouble = Math.random();
                    randomDouble = randomDouble * 3;
                    randomInt = (int) randomDouble;
                    logger.log(Level.INFO, logsInfo[randomInt]);
                }
                if (randomInt == 3) {
                    randomDouble = Math.random();
                    randomDouble = randomDouble * 3;
                    randomInt = (int) randomDouble;
                    logger.log(Level.ERROR, logsError[randomInt]);
                }
                if (randomInt == 4) {
                    randomDouble = Math.random();
                    randomDouble = randomDouble * 3;
                    randomInt = (int) randomDouble;
                    logger.log(Level.DEBUG, logsDebug[randomInt]);
                }
                if (randomInt == 5) {
                    randomDouble = Math.random();
                    randomDouble = randomDouble * 3;
                    randomInt = (int) randomDouble;
                    logger.log(Level.FATAL, logsFatal[randomInt]);
                }
                if (randomInt == 6) {
                    try {
                        throw new Exception("Algo serio realmente paso...!");
                    } catch (Exception ex) {
                        logger.log(Level.TRACE, ex);
                    }
                }

                if (randomInt == 7) {
                    try {
                        throw new Exception("Algo muy malo paso...!");
                    } catch (Exception ex) {
                        logger.log(Level.ERROR, ex);
                    }
                }

                if (randomInt == 8) {
                    try {
                        throw new Exception("Throwable");
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }

                randomDouble = Math.random();
                randomDouble = randomDouble * 3;
                randomInt = (int) randomDouble;
            } catch (Throwable ex) {
            }
        }

    }

    private String readFileFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            lineCount = 0;
            separator = "";
            while ((line = br.readLine()) != null) {
                if (lineCount == 0) {
                    separator = "";
                } else {
                    separator = "\n";
                }
                resultStringBuilder.append(line + separator);
                lineCount++;
            }
        }
        return resultStringBuilder.toString();
    }

    private String readFileLines(URI uri) {
        StringBuilder resultStringBuilder = new StringBuilder();
        Path filePath = Paths.get(uri);
        try (Stream<String> lines = Files.lines(filePath)) {
            lineCount = 0;
            separator = "";
            lines.forEach(line -> {
                if (lineCount == 0) {
                    separator = "";
                } else {
                    separator = "\n";
                }
                resultStringBuilder.append(line + separator);
                lineCount++;
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultStringBuilder.toString();
    }

    private String readAllLines(URI uri) throws IOException {
        Path path = Paths.get(uri);
        List<String> lines = Files.readAllLines(path);
        Collections.sort(lines, (String o1, String o2) -> {
            long lo1 = 0, lo2 = 0;
            try {
                ObjectNode objectNode1 = objectMapper.readValue(o1, ObjectNode.class);
                ObjectNode objectNode2 = objectMapper.readValue(o2, ObjectNode.class);
                lo1 = objectNode1.get("id").asLong();
                lo2 = objectNode2.get("id").asLong();
            } catch (JsonProcessingException ex) {
            }
            return Long.compare(lo2, lo1);
        });
        return lines.toString();
    }

    public void writeFile2(String content) throws IOException, URISyntaxException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File("/Volumes/RESPALDO/GIT/Java/Plataforma Franquicia/frq-server-logs-ms/src/main/resources/data2.txt"), true))) {
            writer.append(content);
        }
    }

    public void writeFile3(String content) throws IOException, URISyntaxException {
        File file = new File("/Volumes/RESPALDO/GIT/Java/Plataforma Franquicia/frq-server-logs-ms/src/main/resources/data3.txt");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(content);
        }
    }
}
