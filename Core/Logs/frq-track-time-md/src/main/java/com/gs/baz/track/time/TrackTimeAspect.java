/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.track.time;

/**
 *
 * @author cescobarh
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class TrackTimeAspect {

    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    @Pointcut("execution(* **..rest..*(..)) || execution(* **..dao..*(..)) || execution(* **..dto..*(..)) || execution(* **..mprs..*(..)) || execution(* org.springframework.web.client..*(..))")
    public void allExections() {
    }

    @Around("allExections()")
    public Object trackTimeObserver(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        final StopWatch stopWatch = new StopWatch();
        Object result = null;
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        if (methodSignature != null && methodSignature.getDeclaringType() != null) {
            stopWatch.start();
            //Measure method execution time
            result = proceedingJoinPoint.proceed();
            stopWatch.stop();
            //Get intercepted method details
            String packageName = methodSignature.getDeclaringType().getPackage().getName();
            String className = methodSignature.getDeclaringType().getSimpleName();
            String methodName = methodSignature.getName();
            long time = stopWatch.getTotalTimeMillis();
            String infoTracking = packageName + "." + className + "." + methodName;
            ObjectNode info = new ObjectMapper().createObjectNode();
            try {
                HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
                List<ObjectNode> traceTrackTime = (List<ObjectNode>) httpServletRequest.getAttribute("traceTrackTimeMethods");
                info.put("method", infoTracking);
                info.put("time", time);
                traceTrackTime.add(info);
                LOGGER.log(Level.INFO, infoTracking);
            } catch (Exception ex) {

            }
        }
        return result;
    }

}
