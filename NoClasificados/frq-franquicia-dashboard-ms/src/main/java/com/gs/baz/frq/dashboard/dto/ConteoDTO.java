/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author kramireza
 */
public class ConteoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo")
    private Integer conteo;

    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(BigDecimal conteo) {
        this.conteo = (conteo == null ? null : conteo.intValue());

    }

    @Override
    public String toString() {
        return "Conteo{" + "conteo=" + conteo + '}';
    }

}
