/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.calificacion.disciplina.rest;

import com.gs.baz.frq.calificacion.disciplina.dao.CalificacionDisciplinaDAOImpl;
import com.gs.baz.frq.calificacion.disciplina.dto.CalificacionDisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/calificacion/disciplina")
public class ServiceCalificacionDisciplina {

    @Autowired
    private CalificacionDisciplinaDAOImpl calificacionDisciplinaDaoImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CalificacionDisciplinaDTO> getCalificacionesDisciplina() throws CustomException {
        return calificacionDisciplinaDaoImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/calificacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CalificacionDisciplinaDTO> getCalificacionDisciplina(@RequestBody CalificacionDisciplinaDTO calificacionDisc) throws CustomException {
        return calificacionDisciplinaDaoImpl.selectRowsCalificacionDisciplina(calificacionDisc);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CalificacionDisciplinaDTO post(@RequestBody CalificacionDisciplinaDTO calificacionDisc) throws CustomException {
        if (calificacionDisc.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (calificacionDisc.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (calificacionDisc.getCalificacionDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("calif_disciplina"));
        }
        if (calificacionDisc.getFecha() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fecha"));
        }
        if (calificacionDisc.getPuntosObtenidos() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("puntos_obtenidos"));
        }
        if (calificacionDisc.getPuntosEsperados() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("puntos_esperados"));
        }
        if (calificacionDisc.getNumeroDefectos() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_defectos"));
        }
        if (calificacionDisc.getNumeroOportunidades() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_oportunidades"));
        }
        if (calificacionDisc.getIdRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_rango"));
        }
        return calificacionDisciplinaDaoImpl.insertRow(calificacionDisc);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public CalificacionDisciplinaDTO putCalificacionDisciplina(@RequestBody CalificacionDisciplinaDTO calificacionDisc) throws CustomException {
        if (calificacionDisc.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (calificacionDisc.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }

        return calificacionDisciplinaDaoImpl.updateRow(calificacionDisc);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CalificacionDisciplinaDTO deleteCalificacionDisciplina(@RequestBody CalificacionDisciplinaDTO calificacionDisc) throws CustomException {
        if (calificacionDisc.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (calificacionDisc.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        return calificacionDisciplinaDaoImpl.deleteRow(calificacionDisc);
    }
}
