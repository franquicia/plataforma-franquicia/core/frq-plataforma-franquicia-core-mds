package com.gs.baz.frq.secciones.protocolo.mprs;

import com.gs.baz.frq.secciones.protocolo.dto.SeccionesProtocoloDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SeccionesProtocoloRowMapper implements RowMapper<SeccionesProtocoloDTO> {

    private SeccionesProtocoloDTO status;

    @Override
    public SeccionesProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new SeccionesProtocoloDTO();
        status.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        status.setModulo(rs.getString("FCMODULO"));
        status.setNumeroPregunta(((BigDecimal) rs.getObject("FINUMPREGUNTAS")));
        status.setPorcentaje((BigDecimal) rs.getObject("FIPORCENTAJE"));
        status.setVersion((BigDecimal) rs.getObject("FIVERSION"));
        status.setFechaVersion(rs.getString("FDFECHAVERSION"));
        return status;
    }
}
