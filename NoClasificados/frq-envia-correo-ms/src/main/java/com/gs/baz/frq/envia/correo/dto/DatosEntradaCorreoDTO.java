package com.gs.baz.frq.envia.correo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

public class DatosEntradaCorreoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "adjunto")
    private String adjunto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "asunto")
    private String asunto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "contactosCopiados")
    private ArrayList<String> contactosCopiados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "correoRemitente")
    private String correoRemitente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cuerpoCorreo")
    private String cuerpoCorreo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "destinatario")
    private String destinatario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "extensionAdjunto")
    private String extensionAdjunto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreAdjunto")
    private String nombreAdjunto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreRemitente")
    private String remitente;

    public String getExtensionAdjunto() {
        return extensionAdjunto;
    }

    public void setExtensionAdjunto(String extensionAdjunto) {
        this.extensionAdjunto = extensionAdjunto;
    }

    public String getNombreAdjunto() {
        return nombreAdjunto;
    }

    public void setNombreAdjunto(String nombreAdjunto) {
        this.nombreAdjunto = nombreAdjunto;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public ArrayList<String> getContactosCopiados() {
        return contactosCopiados;
    }

    public void setContactosCopiados(ArrayList<String> contactosCopiados) {
        this.contactosCopiados = contactosCopiados;
    }

    public String getCorreoRemitente() {
        return correoRemitente;
    }

    public void setCorreoRemitente(String correoRemitente) {
        this.correoRemitente = correoRemitente;
    }

    public String getCuerpoCorreo() {
        return cuerpoCorreo;
    }

    public void setCuerpoCorreo(String cuerpoCorreo) {
        this.cuerpoCorreo = cuerpoCorreo;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

}
