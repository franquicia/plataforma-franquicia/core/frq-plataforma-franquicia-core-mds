/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Ficha", value = "Ficha")
public class Ficha extends FichaBase {

    @JsonProperty(value = "idFicha")
    @ApiModelProperty(notes = "Identificador del Ficha", example = "1", position = -1)
    private Integer idFicha;

    public Integer getIdFicha() {
        return idFicha;
    }

    public void setIdFicha(Integer idFicha) {
        this.idFicha = idFicha;
    }

    @Override
    public String toString() {
        return "Ficha{" + "idFicha=" + idFicha + '}';
    }

}
