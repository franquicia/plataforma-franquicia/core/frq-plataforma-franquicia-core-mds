/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosEstadoFolios {

    @JsonProperty(value = "idFolio")
    private transient Integer idFolio;

    @JsonProperty(value = "fechaInicio")
    @ApiModelProperty(notes = "Fecha de inicio de busqueda de folios", example = "01/03/2021", required = true)
    private String fechaInicio;

    @JsonProperty(value = "fechaFin")
    @ApiModelProperty(notes = "Fecha de fin de busqueda de folios", example = "11/03/2021", required = true)
    private String fechaFin;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

}
