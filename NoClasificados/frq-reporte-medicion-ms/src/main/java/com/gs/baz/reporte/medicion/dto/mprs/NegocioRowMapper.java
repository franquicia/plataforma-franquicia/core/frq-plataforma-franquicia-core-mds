package com.gs.baz.reporte.medicion.dto.mprs;

import com.gs.baz.reporte.medicion.dto.NegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegocioRowMapper implements RowMapper<NegocioDTO> {

    private NegocioDTO negocioDTO;

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocioDTO = new NegocioDTO();
        negocioDTO.setIdNegocio(((BigDecimal) rs.getObject("FINEGOCIODISCID")));
        negocioDTO.setDescripcion(rs.getString("FCNEGOCIODISC"));
        return negocioDTO;
    }
}
