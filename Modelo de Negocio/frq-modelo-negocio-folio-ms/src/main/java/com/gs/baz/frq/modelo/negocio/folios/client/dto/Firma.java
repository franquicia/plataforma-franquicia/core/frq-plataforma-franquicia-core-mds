/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class Firma {

    @JsonProperty(value = "idFirma")
    @ApiModelProperty(notes = "Identificador del Firma", example = "1", position = -1)
    private Integer idFirma;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Numero de Empleado de la firma", example = "331952")
    private Integer numeroEmpleado;

    @JsonProperty(value = "idJerarquia")
    @ApiModelProperty(notes = "Identificados de la jerarquia de la firma", example = "1")
    private Integer idJerarquia;

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1")
    private Integer idEstadoFirma;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1")
    private Integer indice;

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Firma() {
    }

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdJerarquia() {
        return idJerarquia;
    }

    public void setIdJerarquia(Integer idJerarquia) {
        this.idJerarquia = idJerarquia;
    }

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    @Override
    public String toString() {
        return "Firma{" + "idFirma=" + idFirma + ", numeroEmpleado=" + numeroEmpleado + ", idJerarquia=" + idJerarquia + ", idEstadoFirma=" + idEstadoFirma + '}';
    }

}
