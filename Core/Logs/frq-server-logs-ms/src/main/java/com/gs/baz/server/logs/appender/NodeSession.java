/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.appender;

import org.springframework.messaging.simp.stomp.StompSession;

/**
 *
 * @author cescobarh
 */
public class NodeSession {

    private String node;
    private StompSession stompSession;

    public NodeSession(String node, StompSession stompSession) {
        this.node = node;
        this.stompSession = stompSession;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public StompSession getStompSession() {
        return stompSession;
    }

    public void setStompSession(StompSession stompSession) {
        this.stompSession = stompSession;
    }

    @Override
    public String toString() {
        return "NodeSession{" + "node=" + node + ", stompSession=" + stompSession + '}';
    }

}
