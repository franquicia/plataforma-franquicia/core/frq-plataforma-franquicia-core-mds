/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocio.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de tipo de Informacion", value = "negociosZona")
public class NegociosZona {

    @JsonProperty(value = "negociosZona")
    @ApiModelProperty(notes = "tipos negociosZona")
    private List<NegocioZona> negociosZona;

    public NegociosZona(List<NegocioZona> negociosZona) {
        this.negociosZona = negociosZona;
    }

    public List<NegocioZona> getNegocioZona() {
        return negociosZona;
    }

    public void setNegocioZona(List<NegocioZona> negociosZona) {
        this.negociosZona = negociosZona;
    }

}
