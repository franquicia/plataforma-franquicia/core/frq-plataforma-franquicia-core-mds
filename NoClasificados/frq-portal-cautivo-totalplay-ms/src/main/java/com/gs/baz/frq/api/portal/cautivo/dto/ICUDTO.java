/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class ICUDTO {

    @JsonProperty(value = "sucursal")
    private String sucursal;

    @JsonProperty(value = "mac")
    private String mac;

    @JsonProperty(value = "idClienteUnico")
    private String idClienteUnico;

    @JsonProperty(value = "inicioSesion")
    private String inicioSesion;

    @JsonProperty(value = "username")
    private String username;

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIdClienteUnico() {
        return idClienteUnico;
    }

    public void setIdClienteUnico(String idClienteUnico) {
        this.idClienteUnico = idClienteUnico;
    }

    public String getInicioSesion() {
        return inicioSesion;
    }

    public void setInicioSesion(String inicioSesion) {
        this.inicioSesion = inicioSesion;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "ICUDTO{" + "sucursal=" + sucursal + ", mac=" + mac + ", idClienteUnico=" + idClienteUnico + ", inicioSesion=" + inicioSesion + ", username=" + username + '}';
    }

}
