/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.mprs;

import com.gs.baz.frq.accesos.sistema.vistas.dto.VistaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class VistaRowMapper implements RowMapper<VistaBase> {

    private VistaBase vistaBase;

    @Override
    public VistaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaBase = new VistaBase();
        vistaBase.setIdVistaPadre(rs.getInt("FIIDPADRE"));
        vistaBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        vistaBase.setNombre(rs.getString("FCNOMBRE"));
        vistaBase.setIdRuta(rs.getInt("FIIDRUTA"));
        vistaBase.setImagen(rs.getString("FCIMAGEN"));
        return vistaBase;
    }
}
