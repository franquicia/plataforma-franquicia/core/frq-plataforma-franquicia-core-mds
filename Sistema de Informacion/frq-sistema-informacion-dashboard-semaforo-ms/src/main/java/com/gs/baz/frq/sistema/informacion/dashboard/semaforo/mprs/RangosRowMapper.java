/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.Rango;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RangosRowMapper implements RowMapper<Rango> {

    @Override
    public Rango mapRow(ResultSet rs, int rowNum) throws SQLException {
        Rango rango = new Rango();
        rango.setIdRango(rs.getInt("FIRANGO_ID"));
        rango.setColor(rs.getString("FCDETALLE"));
        rango.setLimiteInferior(rs.getInt("FILIMITE_INF"));
        rango.setLimiteSuperior(rs.getInt("FILIMITE_SUP"));
        rango.setDescripcion(rs.getString("FCVALOR_RANGO"));
        return rango;
    }
}
