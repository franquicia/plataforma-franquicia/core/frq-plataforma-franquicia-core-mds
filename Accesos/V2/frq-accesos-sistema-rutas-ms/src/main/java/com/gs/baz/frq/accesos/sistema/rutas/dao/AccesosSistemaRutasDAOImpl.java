/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.rutas.dao;

import com.gs.baz.frq.accesos.sistema.rutas.dto.AltaRuta;
import com.gs.baz.frq.accesos.sistema.rutas.dto.ParametrosRuta;
import com.gs.baz.frq.accesos.sistema.rutas.dto.Ruta;
import com.gs.baz.frq.accesos.sistema.rutas.dto.RutaBase;
import com.gs.baz.frq.accesos.sistema.rutas.mprs.RutaRowMapper;
import com.gs.baz.frq.accesos.sistema.rutas.mprs.RutasBorradasRowMapper;
import com.gs.baz.frq.accesos.sistema.rutas.mprs.RutasRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class AccesosSistemaRutasDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAllDeleted;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectAllDisponibles;
    private DefaultJdbcCall jdbcSelectAllDisponiblesByRuta;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcDeleteAll;
    private final String schema = "MODFRANQ";

    public void init() {
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMACRUTAS");
        jdbcInsert.withProcedureName("SPINSTAMOACRUTAS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMACRUTAS");
        jdbcSelect.withProcedureName("SPGETAMOACRUTAS");
        jdbcSelect.returningResultSet("PA_CDATOS", new RutaRowMapper());

        jdbcSelectAllDeleted = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDeleted.withSchemaName(schema);
        jdbcSelectAllDeleted.withCatalogName("PAADMACRUTAS");
        jdbcSelectAllDeleted.withProcedureName("SPGETAMOACRUTAS_ALL");
        jdbcSelectAllDeleted.returningResultSet("PA_CDATOS", new RutasBorradasRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAADMACRUTAS");
        jdbcSelectAll.withProcedureName("SPGETAMOACRUTAS");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new RutasRowMapper());

        jdbcSelectAllDisponiblesByRuta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDisponiblesByRuta.withSchemaName(schema);
        jdbcSelectAllDisponiblesByRuta.withCatalogName("PAADMACRUTAS");
        jdbcSelectAllDisponiblesByRuta.withProcedureName("SPGETRUTASDISPXRUTA");
        jdbcSelectAllDisponiblesByRuta.returningResultSet("PA_CDATOS", new RutasRowMapper());

        jdbcSelectAllDisponibles = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDisponibles.withSchemaName(schema);
        jdbcSelectAllDisponibles.withCatalogName("PAADMACRUTAS");
        jdbcSelectAllDisponibles.withProcedureName("SPGETRUTASDISP");
        jdbcSelectAllDisponibles.returningResultSet("PA_CDATOS", new RutasRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMACRUTAS");
        jdbcUpdate.withProcedureName("SPACTTAMOACRUTAS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMACRUTAS");
        jdbcDelete.withProcedureName("SPDELTAMOACRUTAS");

        jdbcDeleteAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteAll.withSchemaName(schema);
        jdbcDeleteAll.withCatalogName("PAADMACRUTAS");
        jdbcDeleteAll.withProcedureName("SPDEALLTAMOACRUTAS");
    }

    public RutaBase selectRow(Long entityID) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_RUTA", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RutaBase> data = (List<RutaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Ruta> selectRows() throws CustomException {
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_RUTA", null);
        Map<String, Object> out = jdbcSelectAll.execute(in);
        return (List<Ruta>) out.get("PA_CDATOS");
    }

    public List<Ruta> selectRowsAllDisponibles() throws CustomException {
        MapSqlParameterSource in = new MapSqlParameterSource();
        Map<String, Object> out = jdbcSelectAllDisponibles.execute(in);
        return (List<Ruta>) out.get("PA_CDATOS");
    }

    public List<Ruta> selectRowsAllDisponibles(Long entityID) throws CustomException {
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_RUTA", entityID);
        Map<String, Object> out = jdbcSelectAllDisponiblesByRuta.execute(in);
        return (List<Ruta>) out.get("PA_CDATOS");
    }

    public List<Ruta> selectRowsAll() throws CustomException {
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_RUTA", null);
        Map<String, Object> out = jdbcSelectAllDeleted.execute(in);
        List<Ruta> rutas = (List<Ruta>) out.get("PA_CDATOS");
        return rutas.stream().filter(ruta -> ruta.getBorrado()).map(ruta -> {
            ruta.setBorrado(null);
            return ruta;
        }).collect(Collectors.toList());
    }

    @Transactional(rollbackFor = {DataNotInsertedException.class, DataNotDeletedException.class})
    public List<AltaRuta> insertRow(List<ParametrosRuta> entitiesDTO) throws DataNotInsertedException {
        List<AltaRuta> listId = new ArrayList<>();
        try {
            this.deleteRowAll();
            for (ParametrosRuta entityDTO : entitiesDTO) {
                listId.add(this.insertRow(entityDTO));
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
        return listId;
    }

    public AltaRuta insertRow(ParametrosRuta entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_RUTA", entityDTO.getIdRuta());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCRUTA", entityDTO.getRutaAbsoluta());
            mapSqlParameterSource.addValue("PA_FIESTATUS", 1);
            mapSqlParameterSource.addValue("PA_FIESWEB", 1);
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                BigDecimal id = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaRuta(id.intValue());
            } else {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosRuta entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_RUTA", entityDTO.getIdRuta());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCRUTA", entityDTO.getRutaAbsoluta());
            mapSqlParameterSource.addValue("PA_FIESTATUS", 1);
            mapSqlParameterSource.addValue("PA_FIESWEB", 1);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotUpdatedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_RUTA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRowAll() throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            Map<String, Object> out = jdbcDeleteAll.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

}
