/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos base de grafica semaforo", value = "GraficaSemaforoBase")
public class GraficaSemaforoBase {

    @JsonProperty(value = "idSemaforo")
    @ApiModelProperty(notes = "Identificador del semaforo", example = "1")
    private Integer idSemaforo;

    @JsonProperty(value = "regla")
    @ApiModelProperty(notes = "Regla de la grafica semaforo", example = "Defecto")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String regla;

    public Integer getIdSemaforo() {
        return idSemaforo;
    }

    public void setIdSemaforo(Integer idSemaforo) {
        this.idSemaforo = idSemaforo;
    }

    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    @Override
    public String toString() {
        return "GraficaSemaforoBase{" + "idSemaforo=" + idSemaforo + ", regla=" + regla + '}';
    }

}
