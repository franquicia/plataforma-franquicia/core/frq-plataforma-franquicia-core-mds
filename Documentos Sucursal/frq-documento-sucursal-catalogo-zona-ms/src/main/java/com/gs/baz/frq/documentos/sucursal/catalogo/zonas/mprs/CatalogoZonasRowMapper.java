/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.mprs;

import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.CatalogoZona;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class CatalogoZonasRowMapper implements RowMapper<CatalogoZona> {

    private CatalogoZona catalogoZona;

    @Override
    public CatalogoZona mapRow(ResultSet rs, int rowNum) throws SQLException {
        catalogoZona = new CatalogoZona();
        catalogoZona.setIdCatalogoZona(rs.getInt("FIIDLYZONAS"));
        catalogoZona.setDescripcion(rs.getString("FCDESCRIPCION"));
        catalogoZona.setIdTipoInformacion(rs.getInt("FIIDTIPOINFOR"));
        catalogoZona.setEstatus(rs.getInt("FIESTADO"));
        catalogoZona.setPeriodo(rs.getString("FCPERIODO"));

        return catalogoZona;
    }
}
