/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EjeModeloNegocioDTO {

    @JsonProperty(value = "id_eje_modelo")
    private Integer idModEje;

    @JsonProperty(value = "id_eje")
    private Integer idEje;

    @JsonProperty(value = "disciplinas_eje_negocio")
    private List<DisciplinaNegocioDTO> disciplinaEjeDTO;

    public EjeModeloNegocioDTO() {
    }

    public Integer getIdModEje() {
        return idModEje;
    }

    public void setIdModEje(BigDecimal idModEje) {
        this.idModEje = (idModEje == null ? null : idModEje.intValue());
    }

    public Integer getIdEje() {
        return idEje;
    }

    public void setIdEje(BigDecimal idEje) {
        this.idEje = (idEje == null ? null : idEje.intValue());
    }

    public List<DisciplinaNegocioDTO> getDisciplinaEjeDTO() {
        return disciplinaEjeDTO;
    }

    public void setDisciplinaEjeDTO(List<DisciplinaNegocioDTO> disciplinaEjeDTO) {
        this.disciplinaEjeDTO = disciplinaEjeDTO;
    }

    @Override
    public String toString() {
        return "EjeModeloNegocioDTO{" + "idModEje=" + idModEje + ", idEje=" + idEje + ", disciplinaEjeDTO=" + disciplinaEjeDTO + '}';
    }

}
