package com.gs.baz.frq.accesos.sistema.perfiles.mprs;

import com.gs.baz.frq.accesos.sistema.perfiles.dto.PerfilBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PerfilRowMapper implements RowMapper<PerfilBase> {

    @Override
    public PerfilBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        PerfilBase perfil = new PerfilBase();
        perfil.setDescripcion(rs.getString("FCDESCRIPCION"));
        return perfil;
    }
}
