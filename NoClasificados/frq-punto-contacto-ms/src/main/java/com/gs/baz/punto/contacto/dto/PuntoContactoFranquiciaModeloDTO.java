/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoFranquiciaModeloDTO {

    @JsonProperty(value = "id_punto_contacto_franquicia_modelo")
    private Integer idPuntoContactoFranquiciaModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_punto_contacto")
    private Integer idPuntoContacto;

    @JsonProperty(value = "franquicia")
    private FranquiciaDTO franquiciaDTO;

    @JsonProperty(value = "modelo")
    private ModeloDTO modeloDTO;

    @JsonProperty(value = "categorias")
    private List<PuntoContactoCategoriaDTO> categorias;

    @JsonProperty(value = "tipo_formatos")
    private List<TipoFormatoDTO> tipoFormatos;

    @JsonProperty(value = "id_empleado")
    private Integer idEmpleado;

    @JsonProperty(value = "fecha_asignacion")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public PuntoContactoFranquiciaModeloDTO() {
    }

    public Integer getIdPuntoContactoFranquiciaModelo() {
        return idPuntoContactoFranquiciaModelo;
    }

    public void setIdPuntoContactoFranquiciaModelo(BigDecimal idPuntoContactoFranquiciaModelo) {
        this.idPuntoContactoFranquiciaModelo = (idPuntoContactoFranquiciaModelo == null ? null : idPuntoContactoFranquiciaModelo.intValue());
    }

    public Integer getIdPuntoContacto() {
        return idPuntoContacto;
    }

    public void setIdPuntoContacto(BigDecimal idPuntoContacto) {
        this.idPuntoContacto = (idPuntoContacto == null ? null : idPuntoContacto.intValue());
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(BigDecimal IdEmpleado) {
        this.idEmpleado = (IdEmpleado == null ? null : IdEmpleado.intValue());
    }

    public List<PuntoContactoCategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<PuntoContactoCategoriaDTO> categorias) {
        this.categorias = categorias;
    }

    public List<TipoFormatoDTO> getTipoFormatos() {
        return tipoFormatos;
    }

    public void setTipoFormatos(List<TipoFormatoDTO> tipoFormatos) {
        this.tipoFormatos = tipoFormatos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public FranquiciaDTO getFranquiciaDTO() {
        return franquiciaDTO;
    }

    public void setFranquiciaDTO(FranquiciaDTO franquiciaDTO) {
        this.franquiciaDTO = franquiciaDTO;
    }

    public ModeloDTO getModeloDTO() {
        return modeloDTO;
    }

    public void setModeloDTO(ModeloDTO modeloDTO) {
        this.modeloDTO = modeloDTO;
    }

    @Override
    public String toString() {
        return "PuntoContactoFranquiciaModeloDTO{" + "idPuntoContactoFranquiciaModelo=" + idPuntoContactoFranquiciaModelo + ", idPuntoContacto=" + idPuntoContacto + ", franquiciaDTO=" + franquiciaDTO + ", modeloDTO=" + modeloDTO + ", categorias=" + categorias + ", tipoFormatos=" + tipoFormatos + ", idEmpleado=" + idEmpleado + ", fecha=" + fecha + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
