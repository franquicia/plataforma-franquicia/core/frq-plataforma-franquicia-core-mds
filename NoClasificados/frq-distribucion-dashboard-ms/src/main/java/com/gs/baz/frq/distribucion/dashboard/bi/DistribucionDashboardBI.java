package com.gs.baz.frq.distribucion.dashboard.bi;

import com.gs.baz.frq.distribucion.dashboard.dao.DistribucionDashboardDAOImpl;
import com.gs.baz.frq.distribucion.dashboard.dto.CargaVisitasRequestDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaRequestDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaResponseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaCargaVisitasDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaDepuraVisitasDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaDistriDashCalifDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaMergeDetallePreguntaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasCapaCeroDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasCapaIntermediaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasDistCecoBaseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasHistoricoDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasImperdonableIncumplimientoDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasSieteCapaCeroDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasCapaCeroResponseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasCapaIntermediaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasSieteDTO;
import com.gs.baz.frq.distribucion.dashboard.util.UtilMail;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DistribucionDashboardBI {

    @Autowired
    DistribucionDashboardDAOImpl distribucionDashboardDAOImpl;

    private final Logger logger = LogManager.getLogger(DistribucionDashboardBI.class);

    public VisitasCapaCeroResponseDTO visitasCapaCero(Integer idEstatus) throws CustomException {

        try {

            VisitasCapaCeroResponseDTO visitas = distribucionDashboardDAOImpl.visitasCapaCero(idEstatus);
            return visitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir Visitas Capa Cero");
            logger.error(e);
            return null;
        }

    }

    public VisitasSieteDTO visitasCapaCeroSiete(Integer idEstatus) throws CustomException {

        try {
            VisitasSieteDTO respuestaVisitas = distribucionDashboardDAOImpl.visitasSieteCapaCero(idEstatus);
            return respuestaVisitas;

        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;

        }

    }

    public RespuestasHistoricoDTO respuestasHistorico() throws CustomException {

        try {

            RespuestasHistoricoDTO respuestaVisitas = distribucionDashboardDAOImpl.respuestasHistorico();
            return respuestaVisitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }

    }

    public RespuestasCapaCeroDTO respuestasCapaCero() throws CustomException {

        try {
            RespuestasCapaCeroDTO respuestaVisitas = distribucionDashboardDAOImpl.respuestaCapaCero();
            return respuestaVisitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }
    }

    public RespuestasSieteCapaCeroDTO respuestasSieteCapaCero() throws CustomException {

        try {
            RespuestasSieteCapaCeroDTO respuestaVisitas = distribucionDashboardDAOImpl.respuestaSieteCapaCero();
            return respuestaVisitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;

        }

    }

    public VisitasCapaIntermediaDTO visitasCapaIntermedia() throws CustomException {

        try {
            VisitasCapaIntermediaDTO respuestaVisitas = distribucionDashboardDAOImpl.visitasCapaIntermedia();
            return respuestaVisitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }
    }

    public RespuestasCapaIntermediaDTO respuestasCapaIntermedia() throws CustomException {

        try {
            RespuestasCapaIntermediaDTO respuestaVisitas;
            respuestaVisitas = distribucionDashboardDAOImpl.respuestasCapaIntermedia();
            return respuestaVisitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }
    }

    public List<EstatusVisitaDTO> estatusVisitaRespuesta(EstatusVisitaRequestDTO estatusVisitaRequestDTO) throws CustomException {

        try {
            List<EstatusVisitaDTO> visitas = distribucionDashboardDAOImpl.estatusVisita(estatusVisitaRequestDTO);
            return visitas;
        } catch (CustomException e) {
            logger.info("No fue obtener estatus visita");
            logger.error(e);
            return null;
        }
    }

    public RespuestasDistCecoBaseDTO cecoBaseDistribucion() throws CustomException {

        try {
            RespuestasDistCecoBaseDTO respuestaCecoBase = distribucionDashboardDAOImpl.cecoBaseDistribucion();
            return respuestaCecoBase;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }

    }

    public RespuestaCargaVisitasDTO cargaVisitas(CargaVisitasRequestDTO cargaVisitasRequestDTO) throws CustomException {

        try {
            RespuestaCargaVisitasDTO visitas = distribucionDashboardDAOImpl.cargaVisitas(cargaVisitasRequestDTO);
            return visitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }

    }

    public RespuestaDepuraVisitasDTO depuraVisitas() throws CustomException {

        try {
            RespuestaDepuraVisitasDTO visitas = distribucionDashboardDAOImpl.depuraVisitas();
            return visitas;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }
    }

    public RespuestaDistriDashCalifDTO distribuyeDashCalif() throws CustomException {

        try {
            RespuestaDistriDashCalifDTO respuestaDistriDashCalifDTO = distribucionDashboardDAOImpl.distribuyeDashCalif();
            return respuestaDistriDashCalifDTO;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }
    }

    public RespuestasImperdonableIncumplimientoDTO distribuyeRespImpInc() throws CustomException {

        try {
            RespuestasImperdonableIncumplimientoDTO respuestasImperdonableIncumplimientoDTO = distribucionDashboardDAOImpl.distribuyeRespImpInc();
            return respuestasImperdonableIncumplimientoDTO;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }

    }

    public RespuestaMergeDetallePreguntaDTO distMergeDetallePregunta() throws CustomException {

        try {
            RespuestaMergeDetallePreguntaDTO respuestaMergeDetallePreguntaDTO = distribucionDashboardDAOImpl.distMergeDetallePregunta();
            return respuestaMergeDetallePreguntaDTO;
        } catch (CustomException e) {
            logger.info("No fue posible distribuir ");
            logger.error(e);
            return null;
        }

    }
}
