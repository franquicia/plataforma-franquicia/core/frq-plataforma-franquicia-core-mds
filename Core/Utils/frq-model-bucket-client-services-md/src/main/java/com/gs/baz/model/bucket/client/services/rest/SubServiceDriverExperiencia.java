/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.model.bucket.client.services.dto.DriverExperienciaDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author B73601
 */
public class SubServiceDriverExperiencia {

    public List<DriverExperienciaDTO> getDriverExperiencia() throws CustomException {
        List<DriverExperienciaDTO> driverExperiencia = new ArrayList<>();
        DriverExperienciaDTO driverExperienciaSi = new DriverExperienciaDTO();
        driverExperienciaSi.setDescripcion("Si");
        driverExperienciaSi.setValue(1);
        DriverExperienciaDTO driverExperienciaNo = new DriverExperienciaDTO();
        driverExperienciaNo.setDescripcion("No");
        driverExperienciaNo.setValue(2);
        driverExperiencia.add(driverExperienciaSi);
        driverExperiencia.add(driverExperienciaNo);
        return driverExperiencia;
    }
}
