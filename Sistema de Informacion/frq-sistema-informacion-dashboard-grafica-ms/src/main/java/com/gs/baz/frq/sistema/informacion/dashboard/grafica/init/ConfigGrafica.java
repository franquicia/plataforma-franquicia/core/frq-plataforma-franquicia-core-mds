package com.gs.baz.frq.sistema.informacion.dashboard.grafica.init;

import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dao.SistemaInformacionDashboardGraficaDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dao.SistemaInformacionDashboardGraficaSemaforoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.sistema.informacion.dashboard.grafica")
public class ConfigGrafica {

    private final Logger logger = LogManager.getLogger();

    public ConfigGrafica() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SistemaInformacionDashboardGraficaDAOImpl sistemaInformacionDashboardGraficaDAOImpl() {
        return new SistemaInformacionDashboardGraficaDAOImpl();
    }

    @Bean(initMethod = "init")
    public SistemaInformacionDashboardGraficaSemaforoDAOImpl sistemaInformacionDashboardGraficaSemaforoDAOImpl() {
        return new SistemaInformacionDashboardGraficaSemaforoDAOImpl();
    }

}
