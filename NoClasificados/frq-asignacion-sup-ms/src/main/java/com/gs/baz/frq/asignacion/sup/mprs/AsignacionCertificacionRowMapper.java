package com.gs.baz.frq.asignacion.sup.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.sup.dto.AsignacionSupDTO;

/**
 *
 * @author cescobarh
 */
public class AsignacionCertificacionRowMapper implements RowMapper<AsignacionSupDTO> {

    private AsignacionSupDTO status;

    @Override
    public AsignacionSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionSupDTO();

        status.setIdUsuario(rs.getInt("FIID_USUARIO"));
        status.setNombreUsuario(rs.getString("NOMBREUSU"));
        status.setIdPerfil(rs.getInt("FIPERFIL_ID"));
        status.setIdCeco(rs.getInt("FCID_CECO"));
        status.setEco(rs.getString("ECO"));
        status.setNombreCeco(rs.getString("NOMBRECECO"));
        status.setIdGerente(rs.getInt("FIGERENTEID"));
        status.setFechaInicio(rs.getString("FDINICIO_ASIGN"));
        status.setFechaFin(rs.getString("FDFIN_ASIGN"));
        status.setIdNegocio(rs.getInt("FINEGOCIO_ID"));
        status.setAcronimo(rs.getString("NEGOCIO"));

        return status;
    }
}
