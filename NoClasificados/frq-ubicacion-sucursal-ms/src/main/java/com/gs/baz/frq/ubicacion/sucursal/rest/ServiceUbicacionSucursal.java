/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.ubicacion.sucursal.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.ubicacion.sucursal.dao.UbicacionCecoDAOImpl;
import com.gs.baz.frq.ubicacion.sucursal.dto.CecoDatosEntradaDTO;
import com.gs.baz.frq.ubicacion.sucursal.dto.UbicacionCecoDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/ubicacion-sucursal/")
@Component("FRQServiceUbicacionSucursal")

public class ServiceUbicacionSucursal {

    @Autowired
    private UbicacionCecoDAOImpl ubicacionCecoDAO;
    private final Logger logger = LogManager.getLogger();

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "Consulta ubicación de un centro de costos", notes = "Consulta ubicación centro de costos", nickname = "consultaUbicacionCentroCostos")
    @RequestMapping(value = "/ubicacion/ceco/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getUbicacionCeco(@ApiParam(name = "InformacionCentroCostos", value = "Objeto de información de Centro de Costos", required = true) @RequestBody CecoDatosEntradaDTO cecoDatosEntrada) throws CustomException, DataNotFoundException {

        List<UbicacionCecoDTO> listaUbicaciones = new ArrayList<UbicacionCecoDTO>();

        CecoDatosEntradaDTO obj = new CecoDatosEntradaDTO();
        obj.setCentroCostos(cecoDatosEntrada.getCentroCostos());

        try {
            //response.put("listaUbicaciones", listaUbicaciones);

            listaUbicaciones = (List<UbicacionCecoDTO>) ubicacionCecoDAO.obtieneUbicacionSucursalesCeco(obj).get("listaUbicaciones");

            if (listaUbicaciones.size() > 0) {

                return listaUbicaciones.get(0).toString();

            } else {

                logger.info("AP - ServiceUbicacionSucursal - Sin Datos");
                throw new DataNotFoundException("Información no encontrada");

            }

        } catch (CustomException e) {

            logger.info("AP - ServiceUbicacionSucursal - getUbicacionCeco");
            throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE.detalle("getUbicacionCeco"));

        }

    }

}
