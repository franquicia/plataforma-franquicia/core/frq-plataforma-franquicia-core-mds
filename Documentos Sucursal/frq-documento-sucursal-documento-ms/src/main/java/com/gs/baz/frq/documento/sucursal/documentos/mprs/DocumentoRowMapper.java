/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.mprs;

import com.gs.baz.frq.documento.sucursal.documentos.dto.DocumentoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoRowMapper implements RowMapper<DocumentoBase> {

    private DocumentoBase documentoBase;

    @Override
    public DocumentoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        documentoBase = new DocumentoBase();
        documentoBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        documentoBase.setEstadoHistorico(rs.getInt("FIID_GENHISTO"));
        return documentoBase;
    }
}
