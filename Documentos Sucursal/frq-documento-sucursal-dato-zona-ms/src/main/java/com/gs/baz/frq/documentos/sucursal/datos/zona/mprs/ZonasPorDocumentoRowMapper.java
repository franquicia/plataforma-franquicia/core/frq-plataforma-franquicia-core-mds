/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorDocumento;

/**
 *
 * @author carlos
 */
public class ZonasPorDocumentoRowMapper implements RowMapper<ZonasPorDocumento> {

    private ZonasPorDocumento zonasPorDocumento;

    @Override
    public ZonasPorDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
        zonasPorDocumento = new ZonasPorDocumento();
        zonasPorDocumento.setIdDatoZona(rs.getInt("FIIDDATOSZONA"));
        zonasPorDocumento.setIdZona(rs.getInt("FIIDLYZONAS"));
        zonasPorDocumento.setValorMetros(rs.getString("FCVALOR"));
        zonasPorDocumento.setObservaciones(rs.getString("FCOBSERVACIONES"));
        zonasPorDocumento.setIdVersion(rs.getInt("FIID_VERSION"));
        zonasPorDocumento.setDescripcion(rs.getString("FCDESCRIPCION"));
        zonasPorDocumento.setIdTipoInformacion(rs.getInt("FIIDTIPOINFOR"));
        zonasPorDocumento.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        zonasPorDocumento.setNombreProyecto(rs.getString("NOMPROY"));

        return zonasPorDocumento;
    }
}
