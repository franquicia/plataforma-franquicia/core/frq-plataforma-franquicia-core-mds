package com.gs.baz.frq.modelo.negocio.estados.documento.init;

import com.gs.baz.frq.modelo.negocio.estados.documento.dao.EstadoDocumentoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.estados.documento")
public class ConfigModeloNegocioEstadoDocumento {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioEstadoDocumento() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public EstadoDocumentoDAOImpl estadoDocumentoDAOImpl() {
        return new EstadoDocumentoDAOImpl();
    }

}
