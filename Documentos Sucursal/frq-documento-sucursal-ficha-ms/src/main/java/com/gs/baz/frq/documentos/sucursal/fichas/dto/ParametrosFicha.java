/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosFicha {

    @JsonProperty(value = "idFicha")
    private transient Integer idFicha;

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Identificador del ceco", example = "920100", required = true)
    private Integer idCeco;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1", required = true)
    private Integer idNegocio;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Identificador del formato", example = "1", required = true)
    private Integer idFormato;

    @JsonProperty(value = "idRegion")
    @ApiModelProperty(notes = "Identificador de la region", example = "230000", required = true)
    private Integer idRegion;

    @JsonProperty(value = "region")
    @ApiModelProperty(notes = "Descripcion de la region", example = "REGIONAL SB TLALPAN", required = true)
    private String region;

    @JsonProperty(value = "idZona")
    @ApiModelProperty(notes = "Identificador de la zona", example = "230000", required = true)
    private Integer idZona;

    @JsonProperty(value = "zona")
    @ApiModelProperty(notes = "Descripcion de la zona", example = "ZONA SB CENTRO", required = true)
    private String zona;

    @JsonProperty(value = "idTerritorio")
    @ApiModelProperty(notes = "Identificador del territorio", example = "230000", required = true)
    private Integer idTerritorio;

    @JsonProperty(value = "territorio")
    @ApiModelProperty(notes = "Descripcion del territorio", example = "TERRITORIAL SB SUR", required = true)
    private String territorio;

    @JsonProperty(value = "latitud")
    @ApiModelProperty(notes = "latitud de la sucursal", example = "19.43344", required = true)
    private String latitud;

    @JsonProperty(value = "longitud")
    @ApiModelProperty(notes = "latitud de la sucursal", example = "99.234232", required = true)
    private String longitud;

    @JsonProperty(value = "direccion")
    @ApiModelProperty(notes = "Direccion de la sucursal", example = "Av. Insurgentes sur 2434 delegacion tlalpan", required = true)
    private String direccion;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", required = true)
    private Integer idFolio;

    public Integer getIdFicha() {
        return idFicha;
    }

    public void setIdFicha(Integer idFicha) {
        this.idFicha = idFicha;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Integer idRegion) {
        this.idRegion = idRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(Integer idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

}
