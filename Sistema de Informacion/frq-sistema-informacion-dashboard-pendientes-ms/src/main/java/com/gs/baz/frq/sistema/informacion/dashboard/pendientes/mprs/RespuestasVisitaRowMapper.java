/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CalificacionCeco;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.DetalleVisita;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.InformacionVisita;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.RespuestaVisita;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RespuestasVisitaRowMapper implements RowMapper<RespuestaVisita> {

    @Override
    public RespuestaVisita mapRow(ResultSet rs, int rowNum) throws SQLException {
        RespuestaVisita respuestaVisita = new RespuestaVisita();
        respuestaVisita.setIdVisita(rs.getInt("ID_VISITA"));
        respuestaVisita.setIdPregunta(rs.getInt("ID_PREGUNTA"));
        respuestaVisita.setDescripcionPregunta(rs.getString("DESCRIPCION_PREGUNTA"));
        respuestaVisita.setRespuesta(rs.getString("RESPUESTA"));
        respuestaVisita.setNegocio(rs.getInt("NEGOCIO"));
        respuestaVisita.setPonderacion(rs.getInt("PONDERACION"));
        respuestaVisita.setModulo(rs.getString("MODULO"));
        respuestaVisita.setIncumplimiento(rs.getInt("INCUMPLIMIENTO"));
        respuestaVisita.setIdChecklist(rs.getInt("CHECKLIST"));
        respuestaVisita.setImperdonable(rs.getInt("IMPERDONABLE"));
        respuestaVisita.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
        respuestaVisita.setTipoEvidencia(rs.getInt("TIPO_EVIDENCIA"));
        respuestaVisita.setRutaEvidencia(rs.getString("RUTA_EVIDENCIA"));
        respuestaVisita.setObservaciones(rs.getString("FCOBSERVACIONES"));
        return respuestaVisita;
    }
}
