/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosPadreVista {

    @JsonProperty(value = "idVista")
    private transient Integer idVista;

    @JsonProperty(value = "idVistaPadre")
    @ApiModelProperty(notes = "Identificador de la vista padre", example = "1")
    private Integer idVistaPadre;

    public Integer getIdVista() {
        return idVista;
    }

    public void setIdVista(Integer idVista) {
        this.idVista = idVista;
    }

    public Integer getIdVistaPadre() {
        return idVistaPadre;
    }

    public void setIdVistaPadre(Integer idVistaPadre) {
        this.idVistaPadre = idVistaPadre;
    }

    @Override
    public String toString() {
        return "ParametrosPadreVista{" + "idVista=" + idVista + ", idVistaPadre=" + idVistaPadre + '}';
    }

}
