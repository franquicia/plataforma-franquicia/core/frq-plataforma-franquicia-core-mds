/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author cescobarh
 */
public class CustomDateDeserializer extends StdDeserializer<Date> {

    protected CustomDateDeserializer() {
        this(null);
    }

    protected CustomDateDeserializer(Class<Date> t) {
        super(t);
    }

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm.ss.SSS aa");
        dateFormat.setTimeZone(TimeZone.getDefault());
        try {
            return dateFormat.parse(jp.getValueAsString());
        } catch (ParseException ex) {
        }
        return null;
    }
}
