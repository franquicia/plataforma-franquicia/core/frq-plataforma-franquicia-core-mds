/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ddl.oracle.process;

import com.gs.baz.ddl.oracle.dao.DDLOracleDAO;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 *
 * @author cescobarh
 */
public class UtilZip {

    @Autowired
    DDLOracleDAO ddlOracleDAO;

    private final String definitionExt = ".pks";
    private final String bodyExt = ".pkb";
    private final Path path = Paths.get("/franquicia/temp.zip");
    private String nameFile;
    private String contentFile;

    public byte[] packages(DataSource dataSource, String schema, String objectName) {
        byte[] fileContent = null;
        schema = schema.toUpperCase();
        try {
            this.zipPackagesDefinition(dataSource, schema, objectName);
            this.zipPackagesBody(dataSource, schema, objectName);
            fileContent = Files.readAllBytes(path);
        } catch (IOException ex) {

        } finally {
            path.toFile().delete();
        }
        return fileContent;
    }

    private void zipPackagesDefinition(DataSource dataSource, String schema, String objectName) {
        Map<String, Object> resultMap = ddlOracleDAO.ddlPackagesDefinition(dataSource, schema, objectName);
        ArrayList pakages = (ArrayList) resultMap.get("OUT_PACKAGE_DEFINITION");
        pakages.forEach(itemPackage -> {
            LinkedCaseInsensitiveMap value = (LinkedCaseInsensitiveMap) itemPackage;
            String packageName = (String) value.get("PACKAGE_NAME");
            String packageBody = (String) value.get("PACKAGE_SPEC");
            nameFile = packageName + definitionExt;
            contentFile = packageBody.trim().replace("EDITIONABLE ", "");
            this.zip(nameFile, contentFile);
        });
    }

    private void zipPackagesBody(DataSource dataSource, String schema, String objectName) {
        Map<String, Object> resultMap = ddlOracleDAO.ddlPackagesBody(dataSource, schema, objectName);
        ArrayList pakages = (ArrayList) resultMap.get("OUT_PACKAGES_BODY");
        pakages.forEach(itemPackage -> {
            LinkedCaseInsensitiveMap value = (LinkedCaseInsensitiveMap) itemPackage;
            String packageName = (String) value.get("PACKAGE_NAME");
            String packageBody = (String) value.get("PACKAGE_BODY");
            nameFile = packageName + bodyExt;
            contentFile = packageBody.trim().replace("EDITIONABLE ", "");
            this.zip(nameFile, contentFile);
        });
    }

    private void zip(String fileName, String content) {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        URI uri = URI.create("jar:" + path.toUri());
        try (FileSystem fs = FileSystems.newFileSystem(uri, env)) {
            Path nf = fs.getPath(fileName);
            try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
                writer.write(content);
            }
        } catch (IOException ex) {
            Logger.getLogger(UtilZip.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
