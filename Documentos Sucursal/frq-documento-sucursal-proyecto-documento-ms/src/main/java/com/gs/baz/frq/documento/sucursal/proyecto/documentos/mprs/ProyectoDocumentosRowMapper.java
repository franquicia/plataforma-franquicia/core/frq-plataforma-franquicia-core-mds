/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyecto.documentos.mprs;

import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.ProyectoDocumento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ProyectoDocumentosRowMapper implements RowMapper<ProyectoDocumento> {

    private ProyectoDocumento proyecto;

    @Override
    public ProyectoDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
        proyecto = new ProyectoDocumento();
        proyecto.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        proyecto.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        proyecto.setNombreDoc(rs.getString("FCNOMBRE"));

        return proyecto;
    }
}
