/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.perfiles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosPerfil {

    private transient Integer idPerfil;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del perfil", example = "Sistemas")
    @NotNull
    private String descripcion;

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ParametrosPerfil{" + "idPerfil=" + idPerfil + ", descripcion=" + descripcion + '}';
    }

}
