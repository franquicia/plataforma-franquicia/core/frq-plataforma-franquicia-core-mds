package com.gs.baz.frq.dashboard.mprs.graficos.g006;

import com.gs.baz.frq.dashboard.dto.graficos.g006.NegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoPuntajeNegocioNacionalRowMapper implements RowMapper<NegocioDTO> {

    private NegocioDTO cumplimientoPuntajeNacionalNegocioDTO;

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoPuntajeNacionalNegocioDTO = new NegocioDTO();
        cumplimientoPuntajeNacionalNegocioDTO.setIdNegocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        cumplimientoPuntajeNacionalNegocioDTO.setTotales((BigDecimal) rs.getObject("PCTOTALES"));
        cumplimientoPuntajeNacionalNegocioDTO.setCumple((BigDecimal) rs.getObject("CUMPLE"));
        cumplimientoPuntajeNacionalNegocioDTO.setPorcentaje((BigDecimal) rs.getObject("PORCENTAJE"));
        return cumplimientoPuntajeNacionalNegocioDTO;
    }
}
