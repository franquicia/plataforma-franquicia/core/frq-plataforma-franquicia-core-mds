/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de prefijo", value = "NegocioPrefijoBase")
public class PrefijoBase {

    @JsonProperty(value = "prefijo")
    @ApiModelProperty(notes = "Prefijo nombre del negocio", example = "23")
    private String prefijo;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1")
    private Integer idNegocio;

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }
}
