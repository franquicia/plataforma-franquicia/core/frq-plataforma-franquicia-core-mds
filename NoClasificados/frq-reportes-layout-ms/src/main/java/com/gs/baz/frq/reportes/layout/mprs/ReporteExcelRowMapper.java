/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reportes.layout.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.reportes.layout.dto.ReporteExcel;

/**
 *
 * @author carlos
 */
public class ReporteExcelRowMapper implements RowMapper<ReporteExcel> {

    private ReporteExcel reporteCorreo;

    @Override
    public ReporteExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
        reporteCorreo = new ReporteExcel();
        reporteCorreo.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        reporteCorreo.setNombreProyecto(rs.getString("NOMPROY"));
        reporteCorreo.setIdFolio(rs.getInt("FIID_FOLIO"));
        reporteCorreo.setIdCeco(rs.getInt("FCID_CECO"));
        reporteCorreo.setIdFormato(rs.getInt("FIID_FORMATO"));
        reporteCorreo.setIdNegocio(rs.getString("FIID_NEGOCIO"));
        reporteCorreo.setNombreFormato(rs.getString("NOMFORMATO"));
        reporteCorreo.setNombreNegocio(rs.getString("NOMBNEGO"));
        reporteCorreo.setNombreCeco(rs.getString("NOMCECO"));
        reporteCorreo.setConteoProyecto(rs.getInt("CONTEO_PROY"));
        reporteCorreo.setIdZona(rs.getInt("FIIDLYZONAS"));
        reporteCorreo.setIdVersion(rs.getInt("FIID_VERSION"));
        reporteCorreo.setObservaciones(rs.getString("FCOBSERVACIONES"));
        reporteCorreo.setValor(rs.getString("FCVALOR"));
        reporteCorreo.setNombreZona(rs.getString("ZONA"));
        reporteCorreo.setFecha(rs.getString("FCPERIODO"));

        return reporteCorreo;
    }
}
