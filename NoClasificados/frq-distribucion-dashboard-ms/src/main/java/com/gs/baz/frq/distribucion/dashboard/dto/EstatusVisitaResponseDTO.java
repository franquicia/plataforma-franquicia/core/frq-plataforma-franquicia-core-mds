/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de Respuesta de Estatus Visitas", value = "ListaDatosSalida")
public class EstatusVisitaResponseDTO {

    @JsonProperty(value = "consultaEstatusVisitas")
    @ApiModelProperty(notes = "Consulta de los estatus de las visitas")
    private List<EstatusVisitaDTO> estatusVisita;

    public List<EstatusVisitaDTO> getEstatusVisita() {
        return estatusVisita;
    }

    public void setEstatusVisita(List<EstatusVisitaDTO> estatusVisita) {
        this.estatusVisita = estatusVisita;
    }

    @Override
    public String toString() {
        return "EstatusVisitaResponseDTO{" + "estatusVisita=" + estatusVisita + '}';
    }

}
