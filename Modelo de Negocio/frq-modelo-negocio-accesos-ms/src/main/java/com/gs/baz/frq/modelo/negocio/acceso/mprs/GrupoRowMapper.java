/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.mprs;

import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.GrupoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class GrupoRowMapper implements RowMapper<GrupoBase> {

    private GrupoBase documentoBase;

    @Override
    public GrupoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        documentoBase = new GrupoBase();
        documentoBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        documentoBase.setTotalDocumentos(rs.getInt("TOTAL_DOCTOS"));
        documentoBase.setTotalUsuarios(rs.getInt("TOTAL_USUARIO"));
        return documentoBase;
    }
}
