/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.poblacion.rest.s015;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.poblacion.dao.d015.PoblacionDAOImpl015;
import com.gs.baz.frq.poblacion.dto.PoblacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/poblacion/")
@Component("ServicePoblacion015")
public class ServicePoblacion015 {

    @Autowired
    private PoblacionDAOImpl015 poblacionDAOImpl015;

    @RequestMapping(value = "secure/visita/protocolo/7s/merge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PoblacionDTO poblacionDTO(@RequestBody PoblacionDTO poblacionDTO) throws CustomException {
        return poblacionDAOImpl015.executeStore(poblacionDTO);
    }

}
