/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g008;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class CalificacionDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "row_number")
    private Integer rowNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_obtenidos")
    private Integer puntosObtenidos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    public CalificacionDTO() {
    }

    public CalificacionDTO(Integer rowNumber, Integer puntosObtenidos, Integer calificacion) {
        this.rowNumber = rowNumber;
        this.puntosObtenidos = puntosObtenidos;
        this.calificacion = calificacion;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getPuntosObtenidos() {
        return puntosObtenidos;
    }

    public void setPuntosObtenidos(Integer puntosObtenidos) {
        this.puntosObtenidos = puntosObtenidos;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "CalificacionDTO{" + "puntosObtenidos=" + puntosObtenidos + ", calificacion=" + calificacion + '}';
    }

}
