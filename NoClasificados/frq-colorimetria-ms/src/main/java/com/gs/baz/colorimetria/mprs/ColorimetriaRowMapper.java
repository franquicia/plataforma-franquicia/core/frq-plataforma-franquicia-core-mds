package com.gs.baz.colorimetria.mprs;

import com.gs.baz.colorimetria.dto.ColorimetriaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ColorimetriaRowMapper implements RowMapper<ColorimetriaDTO> {

    private ColorimetriaDTO colorimetriaDTO;

    @Override
    public ColorimetriaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        colorimetriaDTO = new ColorimetriaDTO();
        colorimetriaDTO.setIdColorimetria(((BigDecimal) rs.getObject("FIIDCOLOR")));
        colorimetriaDTO.setTemaWeb(rs.getString("FCTEMAWEB"));
        colorimetriaDTO.setColorPrim(rs.getString("FCCOPRIMARIO"));
        colorimetriaDTO.setColorSegund(rs.getString("FCCOSECUNDARIO"));
        colorimetriaDTO.setColorTerc(rs.getString("FCCOTERCIARIO"));
        colorimetriaDTO.setIcono(rs.getString("FCICONO"));
        return colorimetriaDTO;
    }
}
