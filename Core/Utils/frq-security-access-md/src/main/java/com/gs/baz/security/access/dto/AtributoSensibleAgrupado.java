/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author kramireza
 */
@ApiModel(description = "Datos de atributo sensible agrupados", value = "AtributoSensibleAgrupado")
public class AtributoSensibleAgrupado {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador del atributo sensible", example = "1", position = -1)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idAtributo;

    @JsonProperty(value = "apiOrigen", required = true)
    @ApiModelProperty(notes = "Nombre del api al que pertenece el atributo", example = "remedy-limpieza", required = true)
    private String apiOrigen;

    @JsonProperty(value = "nombre", required = true)
    @ApiModelProperty(notes = "Nombre del atributo sensible", example = "telefonoSucursal", required = true)
    private String nombre;

    @JsonProperty(value = "nombreRecurso", required = true)
    @ApiModelProperty(notes = "Nombre del recurso al que pertenece el atributo", example = "/incidencias/busquedas", required = true)
    private String nombreRecurso;

    @JsonProperty(value = "tipo", required = true)
    @ApiModelProperty(notes = "Es tipo de atributo a cifrar", example = "entrada", required = true)
    private TipoAtributo tipo;

    public enum TipoAtributo {

        entrada,
        salida
    }

    public Integer getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(Integer idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getApiOrigen() {
        return apiOrigen;
    }

    public void setApiOrigen(String apiOrigen) {
        this.apiOrigen = apiOrigen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreRecurso() {
        return nombreRecurso;
    }

    public void setNombreRecurso(String nombreRecurso) {
        this.nombreRecurso = nombreRecurso;
    }

    public TipoAtributo getTipo() {
        return tipo;
    }

    public void setTipo(TipoAtributo tipo) {
        this.tipo = tipo;
    }

}
