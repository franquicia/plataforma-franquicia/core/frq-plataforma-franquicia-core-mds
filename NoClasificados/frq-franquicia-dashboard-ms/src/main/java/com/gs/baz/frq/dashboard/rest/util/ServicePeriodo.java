/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.util;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.FechasPeriodoDTO.FechasPeriodoDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.dto.util.PeriodoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/periodo")
public class ServicePeriodo {

    @Autowired
    private PeriodoDAOImpl periodoDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @param filtrosDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PeriodoDTO> getPeriodos(@RequestBody FiltrosDTO filtrosDTO) throws CustomException {
        return periodoDAOImpl.selectRows(filtrosDTO);
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/actual", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PeriodoDTO getPeriodoActual() throws CustomException {
        return periodoDAOImpl.selectRowActual();
    }

    /**
     *
     * @param periodoDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/fechas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FechasPeriodoDTO getFechasPeriodo(@RequestBody PeriodoDTO periodoDTO) throws CustomException {
        return periodoDAOImpl.selectRowFechaPeriodo(periodoDTO);
    }

    @RequestMapping(value = "secure/get/anterior", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PeriodoDTO getPeriodoAnterior() throws CustomException {
        return periodoDAOImpl.selectRowPeriodosAnteriores();
    }

    /**
     *
     * @param filtrosDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/fechas/general", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FechasDTO getFechasGeneral(@RequestBody FiltrosDTO filtrosDTO) throws CustomException {
        return periodoDAOImpl.getFechas(filtrosDTO);
    }
}
