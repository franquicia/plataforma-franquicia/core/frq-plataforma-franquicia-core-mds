/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la etiqueta folio", value = "AltaEtiquetaFolio")
public class AltaFolioEtiqueta {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador de la etiqueta folio", example = "1")
    private Integer id;

    public AltaFolioEtiqueta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
