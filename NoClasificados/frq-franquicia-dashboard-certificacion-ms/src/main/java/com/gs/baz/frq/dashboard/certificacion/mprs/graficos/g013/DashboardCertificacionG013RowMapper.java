package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g013;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g013.DashboardCertificacionG013DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG013RowMapper implements RowMapper<DashboardCertificacionG013DTO> {

    private DashboardCertificacionG013DTO dashboardCertificacionG013DTO;

    @Override
    public DashboardCertificacionG013DTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionG013DTO = new DashboardCertificacionG013DTO();
        dashboardCertificacionG013DTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionG013DTO.setPuntoContacto(rs.getString("PUNTO_CONTACTO"));
        dashboardCertificacionG013DTO.setCertificador((BigDecimal) rs.getObject("CERTIFICADOR"));
        dashboardCertificacionG013DTO.setNombreCertificador(rs.getString("NOMBRE_CERTIFICADOR"));
        dashboardCertificacionG013DTO.setUltimaCertificacion(rs.getString("ULTIMA_CERTIFICACION"));
        dashboardCertificacionG013DTO.setNombreCertificador(rs.getString("HORA_ENTRADA"));
        dashboardCertificacionG013DTO.setUltimaCertificacion(rs.getString("HORA_SALIDA"));
        dashboardCertificacionG013DTO.setCertificador((BigDecimal) rs.getObject("FIVISITA_ID"));
        return dashboardCertificacionG013DTO;
    }
}
