/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocio.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del tipo informacion", value = "NegocioZonaBase")
public class NegocioZonaBase {

    @JsonProperty(value = "estado")
    @ApiModelProperty(notes = "Estado Negocio Zona", example = "1")
    private Integer estado;

    @JsonProperty(value = "idZona")
    @ApiModelProperty(notes = "Identificador de Zona", example = "1")
    private Integer idZona;

    @JsonProperty(value = "descripcionZonaLayout")
    @ApiModelProperty(notes = "Descripcion de la zona", example = "Patio Bancario")
    private String descripcionZonaLayout;

    public String getDescripcionZonaLayout() {
        return descripcionZonaLayout;
    }

    public void setDescripcionZonaLayout(String descripcionZonaLayout) {
        this.descripcionZonaLayout = descripcionZonaLayout;
    }

    public Integer getEstado() {
        return estado;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

}
