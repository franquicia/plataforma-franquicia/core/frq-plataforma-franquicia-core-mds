/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.configuracion.disciplina.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class ConfiguracionDisciplinaDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_configuracion")
    private Integer idConfiguracion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_frecuencia")
    private Integer idFrecuencia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "protocolo")
    private String protocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "registro_activo")
    private Integer registroActivo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_disciplina")
    private Integer idNegocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public ConfiguracionDisciplinaDTO() {
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(BigDecimal idConfiguracion) {
        this.idConfiguracion = (idConfiguracion == null ? null : idConfiguracion.intValue());
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public Integer getIdFrecuencia() {
        return idFrecuencia;
    }

    public void setIdFrecuencia(BigDecimal idFrecuencia) {
        this.idFrecuencia = (idFrecuencia == null ? null : idFrecuencia.intValue());
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public Integer getRegistroActivo() {
        return registroActivo;
    }

    public void setRegistroActivo(BigDecimal registroActivo) {
        this.registroActivo = (registroActivo == null ? null : registroActivo.intValue());
    }

    public Integer getIdNegocioDisciplina() {
        return idNegocioDisciplina;
    }

    public void setIdNegocioDisciplina(BigDecimal idNegocioDisciplina) {
        this.idNegocioDisciplina = (idNegocioDisciplina == null ? null : idNegocioDisciplina.intValue());;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());

    }

    @Override
    public String toString() {
        return "ConfiguracionDisciplinaDTO{" + "idConfiguracion=" + idConfiguracion + ", idProtocolo=" + idProtocolo + ", idDisciplina=" + idDisciplina + ", idFrecuencia=" + idFrecuencia + ", protocolo=" + protocolo + ", registroActivo=" + registroActivo + ", idNegocioDisciplina=" + idNegocioDisciplina + ", modFecha=" + modFecha + ", modUsuario=" + modUsuario + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
