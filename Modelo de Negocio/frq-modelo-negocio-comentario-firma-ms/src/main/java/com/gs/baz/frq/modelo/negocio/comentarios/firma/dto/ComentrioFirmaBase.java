/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de la ruta firma", value = "RutaFirmaBase")
public class ComentrioFirmaBase {

    @JsonProperty(value = "idRuta")
    @ApiModelProperty(notes = "Identificador de la ruta", example = "1")
    private Integer idFirma;

    @JsonProperty(value = "comentario")
    @ApiModelProperty(notes = "Identificados del nivel", example = "El documento tiene faltas de ortografia")
    private String comentario;

    @JsonProperty(value = "fecha")
    @ApiModelProperty(notes = "fecha del comentario", example = "05-03-2021")
    private String fecha;

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificador del estado de la firma", example = "1")
    private Integer idEstadoFirma;

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    @Override
    public String toString() {
        return "ComentrioFirmaBase{" + "idFirma=" + idFirma + ", comentario=" + comentario + ", fecha=" + fecha + ", idEstadoFirma=" + idEstadoFirma + '}';
    }

}
