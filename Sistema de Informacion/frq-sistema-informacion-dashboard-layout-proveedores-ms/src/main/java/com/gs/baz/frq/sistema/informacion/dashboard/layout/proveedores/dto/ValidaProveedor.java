/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de acceso a Proveedores", value = "ValidaProveedor")
public class ValidaProveedor {

    @JsonProperty(value = "idProveedor")
    @ApiModelProperty(notes = "Identificador del proveedor", example = "1", position = -1)
    private Integer idProveedor;

    @JsonProperty(value = "contrasena")
    @ApiModelProperty(notes = "Contrasena del proveedor", example = "12345678", position = -2)
    private String contrasena;

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Override
    public String toString() {
        return "ValidaProveedor{" + "idProveedor=" + idProveedor + ", contrasena=" + contrasena + '}';
    }
    
}
