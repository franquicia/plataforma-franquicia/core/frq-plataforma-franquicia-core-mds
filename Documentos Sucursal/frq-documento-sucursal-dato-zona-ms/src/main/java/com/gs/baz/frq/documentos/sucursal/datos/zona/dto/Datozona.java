/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de datos y zona ", value = "DatoZona")
public class Datozona extends DatoZonaBase {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador de la tabla dato zona", example = "1", position = -1)
    private Integer id;

    public Integer getIdDatoZona() {
        return id;
    }

    public void setIdDatoZona(Integer idDatoZona) {
        this.id = idDatoZona;
    }

}
