/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del tipo de informacion del ceco", value = "TipoInformacion")
public class TipoInformacion extends TipoInformacionBase {

    @JsonProperty(value = "idTipoInformacion")
    @ApiModelProperty(notes = "Identificador del Tipo de Informacion", example = "1", position = -1)
    private Integer idTipoInformacion;

    public Integer getIdTipoInformacion() {
        return idTipoInformacion;
    }

    public void setIdTipoInformacion(Integer idTipoInformacion) {
        this.idTipoInformacion = idTipoInformacion;
    }

}
