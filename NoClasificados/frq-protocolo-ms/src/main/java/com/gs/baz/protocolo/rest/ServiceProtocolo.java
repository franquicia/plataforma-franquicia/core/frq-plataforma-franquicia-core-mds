/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.protocolo.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.protocolo.dao.ProtocoloDAOImpl;
import com.gs.baz.protocolo.dto.ProtocoloDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/protocolo")
public class ServiceProtocolo {

    @Autowired
    private ProtocoloDAOImpl protocoloDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProtocoloDTO> getProtocolo() throws CustomException {
        return protocoloDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_protocolo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProtocoloDTO getProtocolo(@PathVariable("id_protocolo") Long id_protocolo) throws CustomException {
        return protocoloDAOImpl.selectRow(id_protocolo);
    }

    @RequestMapping(value = "secure/disciplina/get/{id_disciplina_negocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProtocoloDTO> getProtocolosDisciplas(@PathVariable("id_disciplina_negocio") Long idDisciplinaNegocio) throws CustomException {
        return protocoloDAOImpl.selectRowsProtocolosDisciplina(idDisciplinaNegocio);
    }
}
