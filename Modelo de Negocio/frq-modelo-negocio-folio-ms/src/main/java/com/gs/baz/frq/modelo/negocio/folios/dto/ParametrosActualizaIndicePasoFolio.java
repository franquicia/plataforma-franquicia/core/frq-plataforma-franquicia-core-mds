/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosActualizaIndicePasoFolio {

    @JsonProperty(value = "idFolio")
    private transient Integer idFolio;

    @JsonProperty(value = "indicePaso")
    @ApiModelProperty(notes = "Indice del paso de la creacion del documento", example = "1", required = true)
    private Integer indicePaso;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIndicePaso() {
        return indicePaso;
    }

    public void setIndicePaso(Integer indicePaso) {
        this.indicePaso = indicePaso;
    }

}
