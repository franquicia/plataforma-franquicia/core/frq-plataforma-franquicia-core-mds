/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.util;

import com.gs.baz.frq.api.portal.cautivo.dto.FechasParametrosDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MADA
 */
public class Parametros {

    // private Integer cantidadElementos = 500;
    //en caso de cambiar este metodo a servicio parametrizado, debemos modificar el BI para no consumirlo en todo momento.
    // public Integer getCantidadElementos() {
    //     return cantidadElementos;
    //}
    //Para servicios que consumen fechaInicio y fecha Fin, se segmentaron las 24 horas.
    public List<FechasParametrosDTO> getFechasParametros(String fecha) {

        int intervalo = 4; // n horas entre cada intervalo
        String inicioFecha = fecha + " 00:00:00";
        String finFecha = "";

        List<FechasParametrosDTO> fechas = new ArrayList<FechasParametrosDTO>();

        for (int i = intervalo; i <= 24; i = i + intervalo) {

            FechasParametrosDTO fechasParametrosDTO = new FechasParametrosDTO();
            fechasParametrosDTO.setFechaInicio(inicioFecha);
            String hora = i + ":00:00";
            if (i < 10) { //hora con dos digitos
                hora = "0" + hora;
            }

            finFecha = fecha + " " + hora;

            /*SimpleDateFormat sdfDestination = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = sdfDestination.parse(finFecha);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.add(Calendar.SECOND, -1);
                date = cal.getTime();

                finFecha = sdfDestination.format(date);*/
            fechasParametrosDTO.setFechaFin(finFecha);

            inicioFecha = fecha + " " + hora;;

            fechas.add(fechasParametrosDTO);

        }

        return fechas;

    }

    /*public static void main(String[] args) throws CustomException {
        List<FechasParametrosDTO> fechas = new Parametros().getFechasParametros("09/03/2021");

        System.out.println("Fechas");
        for (FechasParametrosDTO fecha : fechas) {
            System.out.println(fecha.getFechaInicio() + " - " + fecha.getFechaFin());
        }
    }*/
}
