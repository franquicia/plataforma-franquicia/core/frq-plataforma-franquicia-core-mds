package com.gs.baz.frq.asignacion.sup.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.sup.dto.AsignacionSupDTO;

/**
 *
 * @author cescobarh
 */
public class VersionCertificacionRowMapper implements RowMapper<AsignacionSupDTO> {

    private AsignacionSupDTO status;

    @Override
    public AsignacionSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionSupDTO();

        status.setIdVersion(rs.getInt("FIID_VERSUP"));
        status.setIdCheckList(rs.getInt("FIID_CHECKLIST"));
        status.setIdNegocio(rs.getInt("FIID_NEGOCIO"));;
        status.setDescripcion(rs.getString("NEGOCIO"));
        status.setAcronimo(rs.getString("FCDESCRIPCION"));
        status.setFecha(rs.getString("FCPERIODO"));

        return status;
    }
}
