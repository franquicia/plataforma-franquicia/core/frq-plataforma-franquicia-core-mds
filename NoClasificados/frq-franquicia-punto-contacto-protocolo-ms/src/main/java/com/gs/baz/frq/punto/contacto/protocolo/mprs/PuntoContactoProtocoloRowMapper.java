package com.gs.baz.frq.punto.contacto.protocolo.mprs;

import com.gs.baz.frq.punto.contacto.protocolo.dto.PuntoContactoProtocoloDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoProtocoloRowMapper implements RowMapper<PuntoContactoProtocoloDTO> {

    private PuntoContactoProtocoloDTO puntoContactoProtocolo;

    @Override
    public PuntoContactoProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        puntoContactoProtocolo = new PuntoContactoProtocoloDTO();
        puntoContactoProtocolo.setIdProt(((Integer) rs.getInt("IDPROT")));

        return puntoContactoProtocolo;
    }

}
