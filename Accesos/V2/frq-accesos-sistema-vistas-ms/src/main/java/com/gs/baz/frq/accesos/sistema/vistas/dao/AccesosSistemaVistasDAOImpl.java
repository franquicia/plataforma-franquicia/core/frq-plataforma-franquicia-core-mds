/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.accesos.sistema.vistas.dto.AltaVista;
import com.gs.baz.frq.accesos.sistema.vistas.dto.ParametrosPadreVista;
import com.gs.baz.frq.accesos.sistema.vistas.dto.Vista;
import com.gs.baz.frq.accesos.sistema.vistas.dto.VistaBase;
import com.gs.baz.frq.accesos.sistema.vistas.dto.ParametrosVista;
import com.gs.baz.frq.accesos.sistema.vistas.mprs.VistaRowMapper;
import com.gs.baz.frq.accesos.sistema.vistas.mprs.VistasFinalesRowMapper;
import com.gs.baz.frq.accesos.sistema.vistas.mprs.VistasPerUsuRowMapper;
import com.gs.baz.frq.accesos.sistema.vistas.mprs.VistasRowMapper;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dao.AccesosSistemaVistasPropiedadesDAOImpl;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.VistaPropiedad;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class AccesosSistemaVistasDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectAllVistasPerfil;
    private DefaultJdbcCall jdbcSelectAllVistasEmpleado;
    private DefaultJdbcCall jdbcSelectAllVistasFinalesEmpleado;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final String schema = "MODFRANQ";

    @Autowired
    private AccesosSistemaVistasPropiedadesDAOImpl accesosSistemaVistasPropiedadesDAOImpl;

    public void init() {
        String catalogo = "PAADMDVISTAS";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSDVISTAS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTDVISTAS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELDVISTAS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETDVISTAS");
        jdbcSelect.returningResultSet("PA_CDATOS", new VistaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETDVISTAS");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new VistasRowMapper());

        jdbcSelectAllVistasPerfil = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllVistasPerfil.withSchemaName(schema);
        jdbcSelectAllVistasPerfil.withCatalogName("PAADMDPERVISTA");
        jdbcSelectAllVistasPerfil.withProcedureName("SPGETDPERVISTA");
        jdbcSelectAllVistasPerfil.returningResultSet("PA_CDATOS", new VistasPerUsuRowMapper());

        jdbcSelectAllVistasEmpleado = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllVistasEmpleado.withSchemaName(schema);
        jdbcSelectAllVistasEmpleado.withCatalogName("PAADMDVISUS");
        jdbcSelectAllVistasEmpleado.withProcedureName("SPGETVISTAXUSUA");
        jdbcSelectAllVistasEmpleado.returningResultSet("PA_CDATOS", new VistasPerUsuRowMapper());

        jdbcSelectAllVistasFinalesEmpleado = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllVistasFinalesEmpleado.withSchemaName(schema);
        jdbcSelectAllVistasFinalesEmpleado.withCatalogName("PAADMDVISUS");
        jdbcSelectAllVistasFinalesEmpleado.withProcedureName("SPGETVISTAFINXUSUA");
        jdbcSelectAllVistasFinalesEmpleado.returningResultSet("PA_CDATOS", new VistasFinalesRowMapper());

    }

    public VistaBase selectRow(Long idEntity) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDVISTA", idEntity);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<VistaBase> data = (List<VistaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Vista> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDVISTA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Vista>) out.get("PA_CDATOS");
    }

    public AltaVista insertRow(ParametrosVista entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPADRE", entityDTO.getIdVistaPadre());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCIMAGEN", entityDTO.getImagen());
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO.getIdRuta());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDVISTA");
                return new AltaVista(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosVista entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource.addValue("PA_FIIDVISTA", entityDTO.getIdVista());
            mapSqlParameterSource.addValue("PA_FIIDPADRE", entityDTO.getIdVistaPadre());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCIMAGEN", entityDTO.getImagen());
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO.getIdRuta());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updatePadreVista(ParametrosPadreVista entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource.addValue("PA_FIIDVISTA", entityDTO.getIdVista());
            mapSqlParameterSource.addValue("PA_FIIDPADRE", entityDTO.getIdVistaPadre());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", null);
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", null);
            mapSqlParameterSource.addValue("PA_FCIMAGEN", null);
            mapSqlParameterSource.addValue("PA_FIIDRUTA", null);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer idEntity) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDVISTA", idEntity);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<Vista> selectAllRowsVistasPerfil(Integer idPerfil) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPERFIL", idPerfil);
        Map<String, Object> out = jdbcSelectAllVistasPerfil.execute(mapSqlParameterSource);
        return (List<Vista>) out.get("PA_CDATOS");
    }

    public List<Vista> selectAllRowsVistasEmpleado(Integer numeroEmpleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllVistasEmpleado.execute(mapSqlParameterSource);
        return (List<Vista>) out.get("PA_CDATOS");
    }

    public List<Vista> selectAllRowsVistasFinalesEmpleado(Integer numeroEmpleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllVistasFinalesEmpleado.execute(mapSqlParameterSource);
        List<Vista> vistas = (List<Vista>) out.get("PA_CDATOS");
        for (Vista vista : vistas) {
            List<VistaPropiedad> propiedadesVista = accesosSistemaVistasPropiedadesDAOImpl.selectAllRows(vista.getIdVista());
            if (!propiedadesVista.isEmpty()) {
                Map<String, Object> propiedades = new HashMap<>();
                propiedadesVista.forEach(propiedad -> {
                    Object valor = propiedad.getValor();
                    if (propiedad.getTipo().toLowerCase().equals("boolean")) {
                        if (valor.equals("true") || valor.equals("false")) {
                            valor = propiedad.getValor().equals("true");
                        }
                    } else if (propiedad.getTipo().toLowerCase().equals("number")) {
                        try {
                            valor = Integer.parseInt((String) valor);
                        } catch (NumberFormatException ex) {

                        }
                    }
                    propiedades.put(propiedad.getNombre(), valor);
                });
                vista.setPropiedades(propiedades);
            }
        }
        return vistas;
    }

}
