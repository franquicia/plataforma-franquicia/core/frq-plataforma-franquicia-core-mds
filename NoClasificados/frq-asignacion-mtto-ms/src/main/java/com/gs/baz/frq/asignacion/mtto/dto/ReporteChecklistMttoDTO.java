/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReporteChecklistMttoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCheckInn")
    private Integer idCheckInn;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private String idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaEntrada")
    private String horaEntrada;

    //hora salida del 2do nivel
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaSalida")
    private String horaSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horasTrabajadas")
    private String horasTrabajadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idBitacora")
    private Integer idBitacora;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreChecklist")
    private String nombreChecklist;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idChecklist")
    private Integer idChecklist;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idFase")
    private Integer idFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreFase")
    private String nombreFase;

    public Integer getIdCheckInn() {
        return idCheckInn;
    }

    public void setIdCheckInn(Integer idCheckInn) {
        this.idCheckInn = idCheckInn;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(String horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Integer idBitacora) {
        this.idBitacora = idBitacora;
    }

    public String getNombreChecklist() {
        return nombreChecklist;
    }

    public void setNombreChecklist(String nombreChecklist) {
        this.nombreChecklist = nombreChecklist;
    }

    public Integer getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(Integer idChecklist) {
        this.idChecklist = idChecklist;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdFase() {
        return idFase;
    }

    public void setIdFase(Integer idFase) {
        this.idFase = idFase;
    }

    public String getNombreFase() {
        return nombreFase;
    }

    public void setNombreFase(String nombreFase) {
        this.nombreFase = nombreFase;
    }

    @Override
    public String toString() {
        return "ReporteChecklistMttoDTO [idCheckInn=" + idCheckInn + ", idUsuario=" + idUsuario + ", nombreUsuario="
                + nombreUsuario + ", idCeco=" + idCeco + ", horaEntrada=" + horaEntrada + ", horaSalida="
                + horaSalida + ", horasTrabajadas=" + horasTrabajadas + ", idBitacora=" + idBitacora
                + ", nombreChecklist=" + nombreChecklist + ", idChecklist=" + idChecklist + ", fecha=" + fecha
                + ", idFase=" + idFase + ", nombreFase=" + nombreFase + "]";
    }

}
