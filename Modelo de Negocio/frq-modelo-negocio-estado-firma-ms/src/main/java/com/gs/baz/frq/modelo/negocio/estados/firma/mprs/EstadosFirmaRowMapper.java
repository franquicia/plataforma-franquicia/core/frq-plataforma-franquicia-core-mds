/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.mprs;

import com.gs.baz.frq.modelo.negocio.estados.firma.dto.EstadoFirma;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EstadosFirmaRowMapper implements RowMapper<EstadoFirma> {

    private EstadoFirma estadoFirma;

    @Override
    public EstadoFirma mapRow(ResultSet rs, int rowNum) throws SQLException {
        estadoFirma = new EstadoFirma();
        estadoFirma.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        estadoFirma.setDescripcion(rs.getString("FCDESCRIPCION"));
        estadoFirma.setNombre(rs.getString("FCNOMBRE"));
        return estadoFirma;
    }
}
