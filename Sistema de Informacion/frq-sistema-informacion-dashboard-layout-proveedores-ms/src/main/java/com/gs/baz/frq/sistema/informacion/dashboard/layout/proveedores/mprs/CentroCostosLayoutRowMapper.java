/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostos;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CentroCostosLayoutRowMapper implements RowMapper<CentroCostos> {

    @Override
    public CentroCostos mapRow(ResultSet rs, int rowNum) throws SQLException {
        CentroCostos centroCostos = new CentroCostos();
        centroCostos.setIdCeco(rs.getInt("FIID_CECO"));
        centroCostos.setIdProveedor(rs.getInt("FIIDPROVEEDOR"));
        centroCostos.setNumeroEmpleadoCertificador(rs.getInt("FIIDCERTIFICADOR"));
        centroCostos.setAuxiliarUno(rs.getString("FCAUXILIAR1"));
        centroCostos.setAuxiliarDos(rs.getString("FCAUXILIAR2"));
        return centroCostos;
    }
}
