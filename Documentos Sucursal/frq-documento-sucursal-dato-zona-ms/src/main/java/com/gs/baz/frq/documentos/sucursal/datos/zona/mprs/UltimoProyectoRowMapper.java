/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ArchivoUltimos;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UltimoProyectoRowMapper implements RowMapper<ArchivoUltimos> {

    private ArchivoUltimos archivoUltimos;

    @Override
    public ArchivoUltimos mapRow(ResultSet rs, int rowNum) throws SQLException {
        archivoUltimos = new ArchivoUltimos();
        archivoUltimos.setIdFolio(rs.getInt("FIID_FOLIO"));
        archivoUltimos.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        archivoUltimos.setNombreProyecto(rs.getString("NOMPROY"));
        archivoUltimos.setIdFormato(rs.getInt("FIID_FORMATO"));
        archivoUltimos.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        archivoUltimos.setNombreFormato(rs.getString("NOMFORMATO"));
        archivoUltimos.setNombreDocumento(rs.getString("NOMDOC"));

        return archivoUltimos;
    }
}
