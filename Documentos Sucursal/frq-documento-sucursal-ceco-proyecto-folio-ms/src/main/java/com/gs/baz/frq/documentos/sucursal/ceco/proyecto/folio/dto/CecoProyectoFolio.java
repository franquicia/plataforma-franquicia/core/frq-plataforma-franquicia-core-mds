package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion sobre la relacion entre centro de costos, proyecto y numero de Folio", value = "CecoProyectoFolio")
public class CecoProyectoFolio {

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costo", example = "1")
    private String idCeco;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1")
    private String nombreProyecto;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "folio")
    @ApiModelProperty(notes = "Número de folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Número de idFormato", example = "1")
    private Integer idFormato;

    public CecoProyectoFolio() {

    }

    public CecoProyectoFolio(String idCeco, Integer idProyecto, Integer idFolio, Integer idFormato) {
        this.idCeco = idCeco;
        this.idProyecto = idProyecto;
        this.idFolio = idFolio;
        this.idFormato = idFormato;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

}
