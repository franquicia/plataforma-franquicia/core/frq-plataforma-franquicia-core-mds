package com.gs.baz.frq.accesos.sistema.vistas.init;

import com.gs.baz.frq.accesos.sistema.vistas.dao.AccesosSistemaVistasDAOImpl;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dao.AccesosSistemaVistasPropiedadesDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.accesos.sistema.vistas")
public class ConfigAccesosSistemaVistas {

    private final Logger logger = LogManager.getLogger();

    public ConfigAccesosSistemaVistas() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public AccesosSistemaVistasDAOImpl accesosSistemaVistaDAOImpl() {
        return new AccesosSistemaVistasDAOImpl();
    }

    @Bean(initMethod = "init")
    public AccesosSistemaVistasPropiedadesDAOImpl accesosSistemaVistasPropiedadesDAOImpl() {
        return new AccesosSistemaVistasPropiedadesDAOImpl();
    }

}
