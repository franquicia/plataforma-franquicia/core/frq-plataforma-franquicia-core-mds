/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Calificación del Ceco", value = "CalificacionCeco")
public class CalificacionCeco {

    @JsonProperty(value = "centroCostosBase")
    @ApiModelProperty(notes = "Número del ceco base", example = "397050", position = -1)
    private String centroCostosBase;

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del ceco", example = "922884", position = -2)
    private String centroCostos;
    
    @JsonProperty(value = "calificacion")
    @ApiModelProperty(notes = "Calificación obtenida", example = "95", position = -3)
    private Integer calificacion;
    
    @JsonProperty(value = "idRango")
    @ApiModelProperty(notes = "Identificador del rango de color", example = "123", position = -4)
    private Integer idRango;
    
    @JsonProperty(value = "rangoColor")
    @ApiModelProperty(notes = "Rango de color hexadecimal", example = "##000000FF", position = -5)
    private String rangoColor;

    public String getCentroCostosBase() {
        return centroCostosBase;
    }

    public void setCentroCostosBase(String centroCostosBase) {
        this.centroCostosBase = centroCostosBase;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    @Override
    public String toString() {
        return "CalificacionCeco{" + "centroCostosBase=" + centroCostosBase + ", centroCostos=" + centroCostos + ", calificacion=" + calificacion + ", idRango=" + idRango + ", rangoColor=" + rangoColor + '}';
    }
    
}
