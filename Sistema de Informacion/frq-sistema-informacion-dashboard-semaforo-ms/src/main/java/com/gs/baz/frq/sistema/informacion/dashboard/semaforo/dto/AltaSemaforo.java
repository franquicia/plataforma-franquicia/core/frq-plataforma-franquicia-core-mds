/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del semaforo", value = "AltaSemaforo")
public class AltaSemaforo {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador del semaforo", example = "1")
    private Integer id;

    public AltaSemaforo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AltaSemaforo{" + "id=" + id + '}';
    }

}
