package com.gs.baz.frq.negocio.version.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.negocio.version.dto.NegocioVersionDTO;

/**
 *
 * @author cescobarh
 */
public class CecosCertificacionRowMapper implements RowMapper<NegocioVersionDTO> {

    private NegocioVersionDTO status;

    @Override
    public NegocioVersionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new NegocioVersionDTO();

        status.setIdCeco(rs.getInt("FCID_CECO"));
        status.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        status.setIdCanal(rs.getInt("FIID_CANAL"));
        status.setNombreCeco(rs.getString("FCNOMBRE"));;
        status.setCecoSuperior(rs.getInt("FICECO_SUPERIOR"));
        status.setStatus(rs.getInt("FIACTIVO"));

        return status;
    }
}
