/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DisciplinaDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String disciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_frecuencia_medicion")
    private Integer idFrecuenciaMedicion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_datos")
    private Integer conteoDatos;

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public Integer getIdFrecuenciaMedicion() {
        return idFrecuenciaMedicion;
    }

    public void setIdFrecuenciaMedicion(BigDecimal idFrecuenciaMedicion) {
        this.idFrecuenciaMedicion = (idFrecuenciaMedicion == null ? null : idFrecuenciaMedicion.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    public Integer getConteoDatos() {
        return conteoDatos;
    }

    public void setConteoDatos(Integer conteoDatos) {
        this.conteoDatos = conteoDatos.intValue();

    }

    @Override
    public String toString() {
        return "DisciplinaDTO{" + "idDisciplina=" + idDisciplina + ", disciplina=" + disciplina + ", idFrecuenciaMedicion=" + idFrecuenciaMedicion + ", filtros=" + filtros.toString() + ", conteoDatos=" + conteoDatos + '}';
    }
}
