package com.gs.baz.ejecuta.mprs;

import com.gs.baz.ejecuta.dto.EjecutaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class EjecutaRowMapper implements RowMapper<EjecutaDTO> {

    private EjecutaDTO status;

    @Override
    public EjecutaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new EjecutaDTO();
        status.setIdEjecuta(((BigDecimal) rs.getObject("FIIDEJECUTA")));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        return status;
    }
}
