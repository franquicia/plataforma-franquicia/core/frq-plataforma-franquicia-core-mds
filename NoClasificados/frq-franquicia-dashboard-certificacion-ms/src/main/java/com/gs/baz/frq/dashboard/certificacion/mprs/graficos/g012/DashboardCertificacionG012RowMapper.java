package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g012;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g012.DashboardCertificacionG012DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG012RowMapper implements RowMapper<DashboardCertificacionG012DTO> {

    private DashboardCertificacionG012DTO dashboardCertificacionG012DTO;

    @Override
    public DashboardCertificacionG012DTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionG012DTO = new DashboardCertificacionG012DTO();
        dashboardCertificacionG012DTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionG012DTO.setPuntoContacto(rs.getString("PUNTO_CONTACTO"));
        dashboardCertificacionG012DTO.setCertificador((BigDecimal) rs.getObject("CERTIFICADOR"));
        dashboardCertificacionG012DTO.setNombreCertificador(rs.getString("NOMBRE_CERTIFICADOR"));
        dashboardCertificacionG012DTO.setUltimaCertificacion(rs.getString("ULTIMA_CERTIFICACION"));
        dashboardCertificacionG012DTO.setIdVisita((BigDecimal) rs.getObject("FIVISITA_ID"));

        return dashboardCertificacionG012DTO;
    }
}
