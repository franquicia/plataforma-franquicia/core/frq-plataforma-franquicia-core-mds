package com.gs.baz.negocio.mprs;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegocioFranquiciasRowMapper implements RowMapper<Integer> {

    @Override
    public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ((BigDecimal) rs.getObject("FIIDFRANQ")).intValue();
    }
}
