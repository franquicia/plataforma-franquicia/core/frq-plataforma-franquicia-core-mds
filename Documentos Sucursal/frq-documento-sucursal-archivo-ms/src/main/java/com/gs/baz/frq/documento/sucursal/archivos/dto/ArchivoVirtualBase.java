/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class ArchivoVirtualBase {

    @JsonProperty(value = "idArchivoPadre")
    @ApiModelProperty(notes = "Identificador del archivo padre", example = "0")
    private Integer idArchivoPadre;

    @JsonProperty(value = "uid")
    @ApiModelProperty(notes = "Ruta en la que se guardó el archivo", example = "/franquicia/modelo-negocio/")
    private Long uid;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "descripcion del archivo", example = "archivo principal foda")
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del archivo", example = "foda")
    private String nombre;

    @JsonProperty(value = "extension")
    @ApiModelProperty(notes = "extension del archivo", example = ".pdf")
    private String extension;

    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Tipo del archivo", example = "1")
    private Integer tipo;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "indicedel archivo", example = "1")
    private Integer indice;

    @JsonProperty(value = "peso")
    @ApiModelProperty(notes = "peso del archivo", example = "1024")
    private Integer peso;

    @JsonProperty(value = "tipoContenido")
    @ApiModelProperty(notes = "Tipo de contenido del archivo", example = ".pdf")
    private String tipoContenido;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1")
    private Integer idNegocio;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdArchivoPadre() {
        return idArchivoPadre;
    }

    public void setIdArchivoPadre(Integer idArchivoPadre) {
        this.idArchivoPadre = idArchivoPadre;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

}
