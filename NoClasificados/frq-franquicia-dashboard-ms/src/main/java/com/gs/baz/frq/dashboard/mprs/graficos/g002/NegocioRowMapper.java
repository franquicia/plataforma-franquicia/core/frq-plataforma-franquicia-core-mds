package com.gs.baz.frq.dashboard.mprs.graficos.g002;

import com.gs.baz.frq.dashboard.dto.graficos.g002.NegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegocioRowMapper implements RowMapper<NegocioDTO> {

    private NegocioDTO negocioDTO;

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocioDTO = new NegocioDTO();
        negocioDTO.setIdNegocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        negocioDTO.setCalificacion((BigDecimal) rs.getObject("RESULTADO"));
        return negocioDTO;
    }
}
