/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.mprs;

import com.gs.baz.frq.modelo.negocio.etiqueta.dto.EtiquetaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EtiquetaRowMapper implements RowMapper<EtiquetaBase> {

    private EtiquetaBase nivelBase;

    @Override
    public EtiquetaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        nivelBase = new EtiquetaBase();
        nivelBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        return nivelBase;
    }
}
