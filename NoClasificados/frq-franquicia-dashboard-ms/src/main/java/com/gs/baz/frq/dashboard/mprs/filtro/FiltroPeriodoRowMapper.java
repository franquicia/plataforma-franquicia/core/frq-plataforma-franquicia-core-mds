package com.gs.baz.frq.dashboard.mprs.filtro;

import com.gs.baz.frq.dashboard.dto.filtros.FiltroPeriodoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FiltroPeriodoRowMapper implements RowMapper<FiltroPeriodoDTO> {

    private FiltroPeriodoDTO periodoDTO;

    @Override
    public FiltroPeriodoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        periodoDTO = new FiltroPeriodoDTO();
        periodoDTO.setNumero((BigDecimal) rs.getObject("PERIODO"));
        periodoDTO.setDescripcion(rs.getString("DESCRIPCION"));
        return periodoDTO;
    }
}
