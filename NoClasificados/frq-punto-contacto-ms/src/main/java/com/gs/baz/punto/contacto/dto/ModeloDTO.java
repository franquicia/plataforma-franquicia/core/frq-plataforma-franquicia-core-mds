/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class ModeloDTO {

    @JsonProperty(value = "id_modelo")
    private Integer idModelo;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public ModeloDTO() {
    }

    public ModeloDTO(BigDecimal idModelo) {
        this.idModelo = (idModelo == null ? null : idModelo.intValue());
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(BigDecimal idModelo) {
        this.idModelo = (idModelo == null ? null : idModelo.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ModeloDTO{" + "idModelo=" + idModelo + ", descripcion=" + descripcion + '}';
    }

}
