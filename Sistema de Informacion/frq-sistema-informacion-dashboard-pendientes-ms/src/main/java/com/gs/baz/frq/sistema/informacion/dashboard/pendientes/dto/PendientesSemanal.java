/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de Pendientes semanalas", value = "PendientesSemanal")
public class PendientesSemanal {

    @JsonProperty(value = "semana")
    @ApiModelProperty(notes = "Número de la semana", example = "45", position = -1)
    private Integer semana;

    @JsonProperty(value = "anio")
    @ApiModelProperty(notes = "Año de la semana", example = "2021", position = -2)
    private Integer anio;
    
    @JsonProperty(value = "totalCertificaciones")
    @ApiModelProperty(notes = "Total de de certificaciones de la semana", example = "99", position = -3)
    private Integer totalCertificaciones;

    public Integer getSemana() {
        return semana;
    }

    public void setSemana(Integer semana) {
        this.semana = semana;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getTotalCertificaciones() {
        return totalCertificaciones;
    }

    public void setTotalCertificaciones(Integer totalCertificaciones) {
        this.totalCertificaciones = totalCertificaciones;
    }

    @Override
    public String toString() {
        return "PendientesSemanal{" + "semana=" + semana + ", anio=" + anio + ", totalCertificaciones=" + totalCertificaciones + '}';
    }

}
