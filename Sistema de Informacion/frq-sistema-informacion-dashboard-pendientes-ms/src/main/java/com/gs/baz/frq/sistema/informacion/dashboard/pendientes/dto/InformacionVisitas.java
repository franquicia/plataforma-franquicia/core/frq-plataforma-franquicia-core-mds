/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de la información de las visitas", value = "InformacionVisitas")
public class InformacionVisitas {

    @JsonProperty(value = "informacionVisitas")
    @ApiModelProperty(notes = "informacionVisitas")
    private List<InformacionVisita> informacionVisita;

    public InformacionVisitas(List<InformacionVisita> informacionVisita) {
        this.informacionVisita = informacionVisita;
    }

    public List<InformacionVisita> getInformacionVisita() {
        return informacionVisita;
    }

    public void setInformacionVisita(List<InformacionVisita> informacionVisita) {
        this.informacionVisita = informacionVisita;
    }

    @Override
    public String toString() {
        return "InformacionVisitas{" + "informacionVisita=" + informacionVisita + '}';
    }

}
