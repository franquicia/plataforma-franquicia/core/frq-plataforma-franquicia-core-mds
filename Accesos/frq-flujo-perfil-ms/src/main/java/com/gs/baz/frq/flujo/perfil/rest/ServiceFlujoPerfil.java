/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.flujo.perfil.rest;

import com.gs.baz.frq.flujo.perfil.dao.FlujoPerfilDAOImpl;
import com.gs.baz.frq.flujo.perfil.dto.FlujoPerfilDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/flujo/perfil")
public class ServiceFlujoPerfil {

    @Autowired
    private FlujoPerfilDAOImpl flujoPerfilDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilDTO> getFlujoPerfiles() throws CustomException {
        return flujoPerfilDAOImpl.selectRows();
    }

    /**
     *
     * @param idFlujoPerfil
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_flujo_perfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoPerfilDTO getFlujoPerfil(@PathVariable("id_flujo_perfil") Long idFlujoPerfil) throws CustomException {
        return flujoPerfilDAOImpl.selectRow(idFlujoPerfil);
    }

    /**
     *
     * @param flujoPerfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/perfil", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilDTO> getFlujoPerfil(@RequestBody FlujoPerfilDTO flujoPerfilDTO) throws CustomException {
        return flujoPerfilDAOImpl.selectRows(flujoPerfilDTO.getIdFlujoPerfil(), flujoPerfilDTO.getIdPerfil());
    }

    /**
     *
     * @param idPerfil
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/available/{id_perfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilDTO> getFlujosPerfilAvailable(@PathVariable("id_perfil") Integer idPerfil) throws CustomException {
        return flujoPerfilDAOImpl.selectRowsAvailable(idPerfil);
    }

    /**
     *
     * @param idPerfil
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/associated/{id_perfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilDTO> getFlujosPerfilAssociated(@PathVariable("id_perfil") Integer idPerfil) throws CustomException {
        return flujoPerfilDAOImpl.selectRowsAssociated(idPerfil);
    }

    /**
     *
     * @param flujoPerfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoPerfilDTO postFlujoPerfil(@RequestBody FlujoPerfilDTO flujoPerfilDTO) throws CustomException {
        if (flujoPerfilDTO.getFlujoDTO().getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("flujo>id_flujo"));
        }
        if (flujoPerfilDTO.getIdPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_perfil"));
        }
        return flujoPerfilDAOImpl.insertRow(flujoPerfilDTO);
    }

    /**
     *
     * @param flujoPerfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoPerfilDTO putFlujoPerfil(@RequestBody FlujoPerfilDTO flujoPerfilDTO) throws CustomException {
        if (flujoPerfilDTO.getIdFlujoPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo_perfil"));
        }
        if (flujoPerfilDTO.getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo"));
        }
        if (flujoPerfilDTO.getIdPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_perfil"));
        }
        return flujoPerfilDAOImpl.updateRow(flujoPerfilDTO);
    }

    /**
     *
     * @param flujoPerfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoPerfilDTO deleteFlujoPerfil(@RequestBody FlujoPerfilDTO flujoPerfilDTO) throws CustomException {
        if (flujoPerfilDTO.getIdFlujoPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo_perfil"));
        }
        return flujoPerfilDAOImpl.deleteRow(flujoPerfilDTO);
    }

}
