/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de ", value = "ArbolBase")
public class ArbolBase {

    @JsonProperty(value = "idModeloNegocioPadre")
    @ApiModelProperty(notes = "identificador del padre del modelo de negocio", example = "1")
    private Integer idModeloNegocioPadre;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del pais", example = "ModeloNegocio 1")
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del arbol del modelo de negocio", example = "ModeloNegocio 1")
    private String nombre;

    @JsonProperty(value = "icono")
    @ApiModelProperty(notes = "icono del arbol del modelo de negocio", example = "/franquicia/modelo-negocio/iconos/prueba.png")
    private String icono;

    @JsonProperty(value = "idNivel")
    @ApiModelProperty(notes = "identificador del nivel del modelo de negocio", example = "1")
    private Integer idNivel;

    public Integer getIdModeloNegocioPadre() {
        return idModeloNegocioPadre;
    }

    public void setIdModeloNegocioPadre(Integer idModeloNegocioPadre) {
        this.idModeloNegocioPadre = idModeloNegocioPadre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

}
