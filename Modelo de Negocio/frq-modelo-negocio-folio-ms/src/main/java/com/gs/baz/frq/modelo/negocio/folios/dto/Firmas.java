/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Firmas {

    @JsonProperty(value = "todas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "todas")
    private List<Firma> todas;

    @JsonProperty(value = "pendientes")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "pendientes")
    private List<Firma> pendientes;

    @JsonProperty(value = "ultima")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Datos del empleado")
    private Firma ultima;

    public Firmas() {
    }

    public List<Firma> getTodas() {
        return todas;
    }

    public void setTodas(List<Firma> todas) {
        this.todas = todas;
    }

    public List<Firma> getPendientes() {
        return pendientes;
    }

    public void setPendientes(List<Firma> pendientes) {
        this.pendientes = pendientes;
    }

    public Firma getUltima() {
        return ultima;
    }

    public void setUltima(Firma ultima) {
        this.ultima = ultima;
    }

    @Override
    public String toString() {
        return "Firmas{" + "todas=" + todas + ", pendientes=" + pendientes + ", ultima=" + ultima + '}';
    }

}
