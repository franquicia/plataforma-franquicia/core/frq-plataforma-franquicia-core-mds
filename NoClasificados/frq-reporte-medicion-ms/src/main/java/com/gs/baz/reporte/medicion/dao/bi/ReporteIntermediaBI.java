/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dao.bi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ProcessMonitor;
import com.gs.baz.model.bucket.client.services.dto.DisciplinaDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinaFranquicia;
import com.gs.baz.reporte.medicion.dao.NegocioProtocoloDAOImpl;
import com.gs.baz.reporte.medicion.dao.ProtocoloDisciplinaDAOImpl;
import com.gs.baz.reporte.medicion.dao.ReporteMedicionDAO;
import com.gs.baz.reporte.medicion.dao.VersionChecklistDAOImpl;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import com.gs.baz.reporte.medicion.dto.NegocioDTO;
import com.gs.baz.reporte.medicion.dto.ProtocoloDisciplinaDTO;
import com.gs.baz.reporte.medicion.dto.VersionChecklistDTO;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author MADA
 */
public class ReporteIntermediaBI {

    private ProcessMonitor processMonitor;

    @Autowired
    ProtocoloDisciplinaDAOImpl protocoloDisciplinaDAOImpl;
    @Autowired
    NegocioProtocoloDAOImpl negocioProtocoloDAOImpl;
    @Autowired
    VersionChecklistDAOImpl versionChecklistDAOImpl;
    @Autowired
    ReporteMedicionDAO reporteMedicionDAO;

    public void init() throws Exception {
    }

    public ObjectNode reporteMedicionIntermedia(SubServiceDisciplinaFranquicia subServiceDisciplinaFranquicia, FiltersDTO filtersDTO) throws CustomException {
        ObjectMapper objectMapper = new ObjectMapper();

        ObjectNode visitasPreguntaRespuestas = objectMapper.createObjectNode();
        DisciplinaDTO disciplinaDTO = subServiceDisciplinaFranquicia.getDisciplina(filtersDTO.getIdDisciplina());
        String disciplinaDescripcion = "Reporte Medición";
        if (disciplinaDTO != null) {
            disciplinaDescripcion = disciplinaDTO.getDescripcion();
        }
        visitasPreguntaRespuestas.put("name_file", disciplinaDescripcion);
        processMonitor = new ProcessMonitor("Reporte Medición");
        processMonitor.startWatch("Consulta protocolos disciplina");

        //Protocolos
        List<ProtocoloDisciplinaDTO> protocolos = protocoloDisciplinaDAOImpl.selectProtocoloDisciplina(filtersDTO);

        for (ProtocoloDisciplinaDTO protocoloDisciplinaDTO : protocolos) {

            filtersDTO.setIdProtocolo(protocoloDisciplinaDTO.getIdProtocolo());
            filtersDTO.setProtocoloDescripcion(protocoloDisciplinaDTO.getDescripcion());

            List<NegocioDTO> negocios = negocioProtocoloDAOImpl.selectNegocioProtocolo(filtersDTO);

            for (NegocioDTO negocio : negocios) {

                filtersDTO.setIdNegocio(negocio.getIdNegocio());
                filtersDTO.setNegocioDescripcion(negocio.getDescripcion());

                List<VersionChecklistDTO> versionesChecklist = versionChecklistDAOImpl.selectVersionChecklist(filtersDTO);

                for (VersionChecklistDTO checklist : versionesChecklist) {

                    FiltersDTO filterDTOChecklist = new FiltersDTO();
                    filterDTOChecklist.setAnio(filtersDTO.getAnio());
                    filterDTOChecklist.setDisciplinaDescripcion(filtersDTO.getDisciplinaDescripcion());
                    filterDTOChecklist.setIdCeco(filtersDTO.getIdCeco());
                    filterDTOChecklist.setFechaInicio(checklist.getFechaInicio());
                    filterDTOChecklist.setFechaFin(checklist.getFechaFin());
                    filterDTOChecklist.setIdChecklist(checklist.getIdChecklist());
                    filterDTOChecklist.setIdDashboard(filtersDTO.getIdDashboard());
                    filterDTOChecklist.setIdNegocio(filtersDTO.getIdNegocio());
                    filterDTOChecklist.setIdPeriodo(filtersDTO.getIdPeriodo());
                    filterDTOChecklist.setIdProtocolo(filtersDTO.getIdProtocolo());
                    filterDTOChecklist.setNegocioDescripcion(filtersDTO.getNegocioDescripcion());
                    filterDTOChecklist.setNivel(filtersDTO.getNivel());
                    filterDTOChecklist.setProtocoloDescripcion(filtersDTO.getProtocoloDescripcion());

                    visitasPreguntaRespuestas.setAll(reporteMedicionProcess(filterDTOChecklist));

                }

            }

        }
        processMonitor.resumeInfo();
        return visitasPreguntaRespuestas;

    }

    public ObjectNode reporteMedicionProcess(FiltersDTO filtersDTO) {

        ObjectMapper objectMapper = new ObjectMapper();

        List<ObjectNode> respuestasVisita;
        List<ObjectNode> respuestas;
        List<ObjectNode> preguntas;
        ArrayNode visitas;

        processMonitor.startWatch("Consulta informacion reporte medicion intermedia");
        processMonitor.stopWatch();
        processMonitor.startWatch("Convierte informacion");
        visitas = objectMapper.convertValue(reporteMedicionDAO.selectVisitas(filtersDTO), ArrayNode.class);
        respuestas = convertValue(reporteMedicionDAO.selectRespuestas(filtersDTO));
        preguntas = convertValue(reporteMedicionDAO.selectPreguntas(filtersDTO));
        processMonitor.stopWatch();
        processMonitor.startWatch("Procesa informacion");
        visitas.forEach(item -> {
            String idVisita = item.get("FIVISITA_ID").asText();
            ((ObjectNode) item).setAll(pregutasRespuestaByVisita(respuestas, preguntas, idVisita));
        });
        processMonitor.stopWatch();

        ObjectNode visitasPreguntaRespuestas = objectMapper.createObjectNode();
        String nameTab = filtersDTO.getIdProtocolo() + " - " + filtersDTO.getNegocioDescripcion() + " - " + filtersDTO.getFechaInicio().replace("/", "-");
        processMonitor.startWatch("Crea objeto final " + nameTab);
        visitasPreguntaRespuestas.set(nameTab, visitas);
        processMonitor.stopWatch();
        return visitasPreguntaRespuestas;
    }

    private ObjectNode pregutasRespuestaByVisita(List<ObjectNode> respuestas, List<ObjectNode> preguntas, String idVisita) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode preguntasItem;
        List<ObjectNode> respuestasVisita;
        preguntasItem = objectMapper.createObjectNode();
        respuestasVisita = respuestas.stream().filter(item -> item.get("FIVISITA_ID").asText().equals(idVisita)).collect(Collectors.toList());
        preguntas.forEach(itemPregunta -> {
            String noSerie = itemPregunta.get("FINUMSERIE").asText();
            ObjectNode itemRespuestaPregunta = respuestasVisita.stream().filter(item -> item.get("FINUMSERIE").asText().equals(noSerie)).findFirst().orElse(null);
            String respuesta = (itemRespuestaPregunta != null ? itemRespuestaPregunta.get("FCRESPUESTA").asText() : "");
            preguntasItem.setAll(objectMapper.createObjectNode().put(itemPregunta.get("FCDESCRIPCION").asText(), respuesta));
            respuestasVisita.removeIf(item -> item.get("FINUMSERIE").asText().equals(noSerie));
        });
        respuestas.removeIf(item -> item.get("FIVISITA_ID").asText().equals(idVisita));
        return preguntasItem;
    }

    private List<ObjectNode> convertValue(Object value) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(value, objectMapper.getTypeFactory().constructCollectionLikeType(List.class, ObjectNode.class));
    }

}
