/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.mprs;

import com.gs.baz.frq.modelo.negocio.estados.documento.dto.EstadoDocumentoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EstadoDocumentoRowMapper implements RowMapper<EstadoDocumentoBase> {

    private EstadoDocumentoBase modeloBase;

    @Override
    public EstadoDocumentoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        modeloBase = new EstadoDocumentoBase();
        modeloBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        modeloBase.setNombre(rs.getString("FCNOMBRE"));
        return modeloBase;
    }
}
