/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cescobarh
 */
public class SecurityJsonReaderUtil {

    private final SecurityUtils.Decrypter decrypter;
    private final SecurityUtils.Encrypter encrypter;
    private List<String> sensibleAttributes;

    public SecurityJsonReaderUtil(AccesoAsimetricoDTO accesoAsimetricoDTO, List<String> sensibleAttributes) throws SecurityUtilsException {
        if (sensibleAttributes == null) {
            this.sensibleAttributes = new ArrayList<>();
        }
        this.sensibleAttributes = sensibleAttributes;
        decrypter = new SecurityUtils.Decrypter(accesoAsimetricoDTO.getPrivada());
        encrypter = new SecurityUtils.Encrypter(accesoAsimetricoDTO.getPublica());
    }

    private TextNode newTextNode(boolean decrypt, String breadcrum, String value) throws DecryptException, EncryptException {
        if (decrypt) {
            try {
                return new TextNode(decrypter.decrypt(value));
            } catch (SecurityUtilsException ex) {
                DecryptException decryptException = new DecryptException(ex);
                decryptException.setField(breadcrum);
                throw decryptException;
            }
        } else {
            try {
                return new TextNode(encrypter.encrypt(value));
            } catch (SecurityUtilsException ex) {
                EncryptException encryptException = new EncryptException(ex);
                encryptException.setField(breadcrum);
                throw new EncryptException(ex);
            }
        }
    }

    private JsonNode overrideArrayValue(boolean decrypt, String breadcrum, ArrayNode arrayNode) throws DecryptException, EncryptException {
        int index = 0;
        for (JsonNode itemParent : arrayNode) {
            if (itemParent.isTextual()) {
                arrayNode.set(index, newTextNode(decrypt, breadcrum, itemParent.textValue()));
            }
            if (itemParent.isArray()) {
                this.overrideArrayValue(decrypt, breadcrum, (ArrayNode) itemParent);
            }
            Iterator<Map.Entry<String, JsonNode>> fields = itemParent.fields();
            while (fields.hasNext()) {
                Map.Entry<String, JsonNode> jsonNode = fields.next();
                JsonNode item = jsonNode.getValue();
                String key = jsonNode.getKey();
                if (breadcrum.isEmpty()) {
                    breadcrum = key;
                } else {
                    breadcrum = breadcrum + "." + key;
                }
                breadcrum += ".[" + index + "]";
                if (item.isObject()) {
                    this.overrideJsonNodeValues(decrypt, breadcrum, (ObjectNode) item);
                } else if (item.isArray()) {
                    this.overrideArrayValue(decrypt, breadcrum, (ArrayNode) item);
                } else if (!item.isNull() && !item.isPojo()) {
                    if (sensibleAttributes.contains(key)) {
                        jsonNode.setValue(newTextNode(decrypt, breadcrum, item.asText()));
                    }
                }
            }
            index++;
        }
        return arrayNode;
    }

    public JsonNode decryptJsonNodeValues(JsonNode itemNode) throws DecryptException, SecurityUtilsException {
        try {
            return this.overrideJsonNodeValues(true, "", itemNode);
        } catch (EncryptException ex) {
            throw new SecurityUtilsException(ex);
        }
    }

    public JsonNode encryptJsonNodeValues(JsonNode itemNode) throws EncryptException, SecurityUtilsException {
        try {
            return this.overrideJsonNodeValues(false, "", itemNode);
        } catch (DecryptException ex) {
            throw new SecurityUtilsException(ex);
        }
    }

    private JsonNode overrideJsonNodeValues(boolean decrypt, String breadcrum, JsonNode itemNode) throws DecryptException, EncryptException {
        if (itemNode.isArray()) {
            return this.overrideArrayValue(decrypt, breadcrum, (ArrayNode) itemNode);
        }
        Iterator<Map.Entry<String, JsonNode>> fields = itemNode.fields();
        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> jsonNode = fields.next();
            JsonNode item = jsonNode.getValue();
            String key = jsonNode.getKey();
            if (breadcrum.isEmpty()) {
                breadcrum = "$";
            } else {
                breadcrum = breadcrum + "." + key;
            }
            if (item.isObject()) {
                this.overrideJsonNodeValues(decrypt, breadcrum, (ObjectNode) item);
            } else if (item.isArray()) {
                this.overrideArrayValue(decrypt, breadcrum, (ArrayNode) item);
            } else {
                if (sensibleAttributes.contains(key)) {
                    jsonNode.setValue(newTextNode(decrypt, breadcrum, item.asText()));
                }
            }
        }
        return itemNode;
    }
}
