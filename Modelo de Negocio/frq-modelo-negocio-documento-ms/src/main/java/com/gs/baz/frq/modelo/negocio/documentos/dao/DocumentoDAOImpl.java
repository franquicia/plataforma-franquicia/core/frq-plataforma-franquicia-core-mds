/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.documentos.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.documentos.dto.AltaDocumento;
import com.gs.baz.frq.modelo.negocio.documentos.dto.Documento;
import com.gs.baz.frq.modelo.negocio.documentos.dto.DocumentoBase;
import com.gs.baz.frq.modelo.negocio.documentos.dto.ParametrosDocumento;
import com.gs.baz.frq.modelo.negocio.documentos.mprs.DocumentoRowMapper;
import com.gs.baz.frq.modelo.negocio.documentos.mprs.DocumentosRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNDOCUMEN";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNDOCUMENTO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNDOCUMENTO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNDOCUMENTO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETDOCTXFOLIO");
        jdbcSelect.returningResultSet("PA_CDATOS", new DocumentoRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETDOCTXFOLIO");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new DocumentosRowMapper());

    }

    public DocumentoBase selectRow(Long idFolio, Long idDocumento) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", idDocumento);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<DocumentoBase> data = (List<DocumentoBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Documento> selectAllRows(Long idFolio) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Documento>) out.get("PA_CDATOS");
    }

    public AltaDocumento insertRow(ParametrosDocumento entityDTO) throws DataNotInsertedException {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(entityDTO.getFechaInicio());
            String dateFormat = new SimpleDateFormat("dd/MM/yyyy").format(date);
            entityDTO.setFechaInicio(dateFormat);
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCTITULO", entityDTO.getTitulo());
            mapSqlParameterSource.addValue("PA_FCOBJETIVO", entityDTO.getObjetivo());
            mapSqlParameterSource.addValue("PA_FCVERSION", entityDTO.getVersion());
            mapSqlParameterSource.addValue("PA_FCCONTROLCAMBIOS", entityDTO.getControlCambios());
            mapSqlParameterSource.addValue("PA_FDVIGENTE", entityDTO.getFechaInicio());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDDOCUMENTO");
                return new AltaDocumento(dato.intValue());
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
    }

    public void updateRow(ParametrosDocumento entityDTO) throws DataNotUpdatedException {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(entityDTO.getFechaInicio());
            String dateFormat = new SimpleDateFormat("dd/MM/yyyy").format(date);
            entityDTO.setFechaInicio(dateFormat);
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", entityDTO.getIdDocumento());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCTITULO", entityDTO.getTitulo());
            mapSqlParameterSource.addValue("PA_FCOBJETIVO", entityDTO.getObjetivo());
            mapSqlParameterSource.addValue("PA_FCVERSION", entityDTO.getVersion());
            mapSqlParameterSource.addValue("PA_FCCONTROLCAMBIOS", entityDTO.getControlCambios());
            mapSqlParameterSource.addValue("PA_FDVIGENTE", entityDTO.getFechaInicio());
            mapSqlParameterSource.addValue("PA_FIELIMINADO", null);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw new DataNotUpdatedException(ex);
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
