/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.mprs;

import com.gs.baz.frq.modelo.negocio.rutas.dto.Ruta;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class RutasRowMapper implements RowMapper<Ruta> {

    private Ruta ruta;

    @Override
    public Ruta mapRow(ResultSet rs, int rowNum) throws SQLException {
        ruta = new Ruta();
        ruta.setIdRuta(rs.getInt("FIIDRUTA"));
        ruta.setCodificacion(rs.getString("FCCODIFICACION"));
        ruta.setIdPais(rs.getInt("FIIDPAIS"));
        ruta.setIdModelo(rs.getInt("FIIDMODELO"));
        ruta.setIdUnidadNegocio(rs.getInt("FIIDUNIDADNEGOCIO"));
        return ruta;
    }
}
