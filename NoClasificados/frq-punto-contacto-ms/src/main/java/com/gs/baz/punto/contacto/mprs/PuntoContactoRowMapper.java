package com.gs.baz.punto.contacto.mprs;

import com.gs.baz.punto.contacto.dto.FranquiciaDTO;
import com.gs.baz.punto.contacto.dto.ModeloDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoCategoriaDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoDetalleDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFormatoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFranquiciaModeloDTO;
import com.gs.baz.punto.contacto.dto.TipoFormatoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoRowMapper implements RowMapper<PuntoContactoDTO> {

    private PuntoContactoDTO puntoContacto;

    @Override
    public PuntoContactoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        puntoContacto = new PuntoContactoDTO();
        puntoContacto.setIdPuntoContacto(((BigDecimal) rs.getObject("FIIDCECO")));
        puntoContacto.setCeco(rs.getString("FCCECO"));
        puntoContacto.setIdEstatus((BigDecimal) rs.getObject("FIIDESTATUS"));
        puntoContacto.setDescripcion(rs.getString("FCDESCRIPCION"));
        return puntoContacto;
    }

    public class PuntoContactoDetalleRowMapper implements RowMapper<PuntoContactoDetalleDTO> {

        private PuntoContactoDetalleDTO puntoContacto;

        @Override
        public PuntoContactoDetalleDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            puntoContacto = new PuntoContactoDetalleDTO();
            puntoContacto.setCeco(rs.getString("FCCECO"));
            puntoContacto.setReferenciaCeco((BigDecimal) rs.getObject("FIPREFCC"));
            puntoContacto.setNumeroEconomico((BigDecimal) rs.getObject("FINUMECO"));
            puntoContacto.setIdEstatus((BigDecimal) rs.getObject("FIDESTATUS"));
            puntoContacto.setEstatus(rs.getString("FCESTATUS"));
            puntoContacto.setIdTerritorio(new BigDecimal(rs.getString("FCIDTERRITORIO")));
            puntoContacto.setTerritorio(rs.getString("FCTERITORIO"));
            puntoContacto.setIdZona(new BigDecimal(rs.getString("FCIDZONA")));
            puntoContacto.setZona(rs.getString("FCZONA"));
            puntoContacto.setIdRegion(new BigDecimal(rs.getString("FCIDREGION")));
            puntoContacto.setRegion(rs.getString("FCREGION"));
            puntoContacto.setNumeroResponsable(rs.getString("FCNUMRESPONS"));
            puntoContacto.setResponsable(rs.getString("FCNOMBRERESP"));
            puntoContacto.setIdPais((BigDecimal) rs.getObject("FIIDPAIS"));
            puntoContacto.setPais(rs.getString("FCPAIS"));
            puntoContacto.setIdEstado((BigDecimal) rs.getObject("FIIDESTADO"));
            puntoContacto.setEstado(rs.getString("FCESTADO"));
            puntoContacto.setMunicipio(rs.getString("FCMUNICIPIO"));
            puntoContacto.setFechaCarga(rs.getString("FDFECHA_CARGA"));
            puntoContacto.setDireccion(rs.getString("FCDIRECCION"));
            return puntoContacto;
        }
    }

    public class PuntoContactoFranquiciaModeloRowMapper implements RowMapper<PuntoContactoFranquiciaModeloDTO> {

        private PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModelo;

        @Override
        public PuntoContactoFranquiciaModeloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            puntoContactoFranquiciaModelo = new PuntoContactoFranquiciaModeloDTO();
            puntoContactoFranquiciaModelo.setIdPuntoContactoFranquiciaModelo(((BigDecimal) rs.getObject("FIIDPCFRQMOD")));
            puntoContactoFranquiciaModelo.setIdPuntoContacto((BigDecimal) rs.getObject("FIIDCECO"));
            puntoContactoFranquiciaModelo.setFranquiciaDTO(new FranquiciaDTO((BigDecimal) rs.getObject("FIIDFRANQ")));
            puntoContactoFranquiciaModelo.setModeloDTO(new ModeloDTO((BigDecimal) rs.getObject("FIIDMODELO")));
            puntoContactoFranquiciaModelo.setIdEmpleado((BigDecimal) rs.getObject("FIIDEMPLEADO"));
            puntoContactoFranquiciaModelo.setFecha(rs.getString("FDFECHA"));
            return puntoContactoFranquiciaModelo;
        }
    }

    public class PuntoContactoCategoriaRowMapper implements RowMapper<PuntoContactoCategoriaDTO> {

        private PuntoContactoCategoriaDTO puntoContactoCategoria;

        @Override
        public PuntoContactoCategoriaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            puntoContactoCategoria = new PuntoContactoCategoriaDTO();
            puntoContactoCategoria.setIdCategoriaPuntoContacto(((BigDecimal) rs.getObject("FIIDPCCATE")));
            puntoContactoCategoria.setIdPuntoContactoFranquiciaModelo((BigDecimal) rs.getObject("FIIDPCFRQMOD"));
            puntoContactoCategoria.setIdCategoria((BigDecimal) rs.getObject("FIIDCATEG"));
            return puntoContactoCategoria;
        }
    }

    public class PuntoContactoFormatoRowMapper implements RowMapper<PuntoContactoFormatoDTO> {

        private PuntoContactoFormatoDTO puntoContactoFormato;

        @Override
        public PuntoContactoFormatoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            puntoContactoFormato = new PuntoContactoFormatoDTO();
            puntoContactoFormato.setIdFormato(((BigDecimal) rs.getObject("FIIDPCFRMT")));
            puntoContactoFormato.setIdPuntoContactoFranquiciaModelo((BigDecimal) rs.getObject("FIIDPCFRQMOD"));
            return puntoContactoFormato;
        }
    }

    public class TipoFormatoRowMapper implements RowMapper<TipoFormatoDTO> {

        private TipoFormatoDTO status;

        @Override
        public TipoFormatoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            status = new TipoFormatoDTO();
            status.setIdTipoFormato(((BigDecimal) rs.getObject("FIIDTIFORM")));
            status.setDescripcion(rs.getString("FCDESCRIPCION"));
            return status;
        }
    }

}
