/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.modelos.franquicia.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.DisciplinaDTO;
import com.gs.baz.model.bucket.client.services.dto.DriverExperienciaDTO;
import com.gs.baz.model.bucket.client.services.dto.EjeDTO;
import com.gs.baz.model.bucket.client.services.dto.EjecutaDTO;
import com.gs.baz.model.bucket.client.services.dto.FrecuenciaMedicionDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import com.gs.baz.model.bucket.client.services.dto.NegocioDTO;
import com.gs.baz.model.bucket.client.services.dto.ProtocoloDTO;
import com.gs.baz.model.bucket.client.services.dto.StatusDTO;
import com.gs.baz.model.bucket.client.services.dto.TipoMedicionDTO;
import com.gs.baz.model.bucket.client.services.dto.UsuarioDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplina;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinasEje;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinasNegocio;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDriverExperiencia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEje;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjecuta;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjesModelo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFrecuenciaMedicion;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModelosFranquicia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceNegocio;
import com.gs.baz.model.bucket.client.services.rest.SubServiceProtocolo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceStatus;
import com.gs.baz.model.bucket.client.services.rest.SubServiceTipoMedicion;
import com.gs.baz.model.bucket.client.services.rest.SubServiceUsuario;
import com.gs.baz.modelos.franquicia.dao.ModeloFranquiciaDAOImpl;
import com.gs.baz.modelos.franquicia.dto.DisciplinaEjeDTO;
import com.gs.baz.modelos.franquicia.dto.EjeModeloDTO;
import com.gs.baz.modelos.franquicia.dto.ModeloFranquiciaDTO;
import com.gs.baz.modelos.franquicia.dto.NegocioModeloFranquiciaDTO;
import com.gs.baz.modelos.franquicia.rest.dto.DataModeloFranquiciaDTO;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/modelos/franquicia")
public class ServiceModelosFranquicia {

    @Autowired
    private ModeloFranquiciaDAOImpl modeloFranquiciaDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceEje subServiceEje;

    @Autowired
    private SubServiceDisciplina subServiceDisciplina;

    @Autowired
    private SubServiceStatus subServiceStatus;

    @Autowired
    private SubServiceTipoMedicion subServiceTipoMedicion;

    @Autowired
    private SubServiceFrecuenciaMedicion subServiceFrecuenciaMedicion;

    @Autowired
    private SubServiceEjecuta subServiceEjecuta;

    @Autowired
    private SubServiceEjesModelo subServiceEjesModelo;

    @Autowired
    private SubServiceDisciplinasEje subServiceDisciplinasEje;

    @Autowired
    private SubServiceUsuario subServiceUsuario;

    @Autowired
    private SubServiceDriverExperiencia subServiceDriverExperiencia;

    @Autowired
    private SubServiceProtocolo subServiceProtocolo;

    @Autowired
    private SubServiceNegocio subServiceNegocio;

    @Autowired
    private SubServiceDisciplinasNegocio subServiceDisciplinasNegocio;

    @Autowired
    private SubServiceModelosFranquicia subServiceModelosFranquicia;

    final private VersionBI versionBI = new VersionBI();

    final private ObjectMapper objectMapper = new ObjectMapper();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @param headers
     * @param idFranquicia
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModeloFranquiciaDTO> getModelos(@RequestHeader HttpHeaders headers, @PathVariable("id_franquicia") Long idFranquicia) throws CustomException {
        final List<ModeloFranquiciaDTO> modeloFranquiciasDTO;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            modeloFranquiciasDTO = modeloFranquiciaDAOImpl.selectRows(idFranquicia);
            if (modeloFranquiciasDTO != null) {
                subServiceUsuario.init(basePath, httpEntity);
                for (ModeloFranquiciaDTO modeloFranquicia : modeloFranquiciasDTO) {
                    this.setEmpleado(modeloFranquicia);
                }
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return modeloFranquiciasDTO;
    }

    @RequestMapping(value = "secure/get/{id_franquicia}/data", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModeloFranquiciaDTO getModelosData(@RequestHeader HttpHeaders headers, @PathVariable("id_franquicia") Long idFranquicia) throws CustomException {
        final DataModeloFranquiciaDTO dataModeloDTO = new DataModeloFranquiciaDTO();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            List<ModeloFranquiciaDTO> modelosFranquiciasDTO = modeloFranquiciaDAOImpl.selectRows(idFranquicia);
            if (modelosFranquiciasDTO != null) {
                subServiceEje.init(basePath, httpEntity);
                List<EjeDTO> ejes = subServiceEje.getEjes();
                metadata.setEjes(ejes);
                subServiceUsuario.init(basePath, httpEntity);
                for (ModeloFranquiciaDTO modeloFranquicia : modelosFranquiciasDTO) {
                    this.setEmpleado(modeloFranquicia);
                }
                subServiceEjesModelo.init(basePath, httpEntity);
                List<EjeModeloDTO> ejesModelo;
                Object ejesModeloBucket;
                for (ModeloFranquiciaDTO modeloFranquiciaDTO : modelosFranquiciasDTO) {
                    ejesModeloBucket = subServiceEjesModelo.getEjesModelo(modeloFranquiciaDTO.getIdModelo());
                    if (ejesModeloBucket != null) {
                        ejesModelo = objectMapper.convertValue(ejesModeloBucket, objectMapper.getTypeFactory().constructCollectionType(List.class, EjeModeloDTO.class));
                        modeloFranquiciaDTO.setEjesModelo(ejesModelo);
                    }
                }
                dataModeloDTO.setMetadata(metadata);
                dataModeloDTO.setModelosFranquicia(modelosFranquiciasDTO);
            }
            dataModeloDTO.setMetadata(metadata);

        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return dataModeloDTO;
    }

    /**
     *
     * @param headers
     * @param id_modelo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/data/{id_modelo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModeloFranquiciaDTO getDataModelo(@RequestHeader HttpHeaders headers, @PathVariable("id_modelo") Long id_modelo) throws CustomException {
        final DataModeloFranquiciaDTO dataModeloDTO = new DataModeloFranquiciaDTO();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            ModeloFranquiciaDTO modelo = modeloFranquiciaDAOImpl.selectRow(id_modelo);
            if (modelo != null) {
                subServiceEje.init(basePath, httpEntity);
                subServiceDisciplina.init(basePath, httpEntity);
                subServiceStatus.init(basePath, httpEntity);
                subServiceTipoMedicion.init(basePath, httpEntity);
                subServiceFrecuenciaMedicion.init(basePath, httpEntity);
                subServiceEjecuta.init(basePath, httpEntity);
                subServiceEjesModelo.init(basePath, httpEntity);
                subServiceDisciplinasEje.init(basePath, httpEntity);
                subServiceProtocolo.init(basePath, httpEntity);
                subServiceNegocio.init(basePath, httpEntity);
                subServiceModelosFranquicia.init(basePath, httpEntity);
                subServiceDisciplinasNegocio.init(basePath, httpEntity);

                List<EjeDTO> ejes = subServiceEje.getEjes();
                List<DisciplinaDTO> disciplinas = subServiceDisciplina.getDisciplina();
                List<StatusDTO> status = subServiceStatus.getStatus();
                List<TipoMedicionDTO> tipoMedicion = subServiceTipoMedicion.getTipoMedicion();
                List<FrecuenciaMedicionDTO> frecuenciaMedicion = subServiceFrecuenciaMedicion.getFrecuenciaMedicion();
                List<EjecutaDTO> ejecuta = subServiceEjecuta.getEjecuta();
                List<DriverExperienciaDTO> driverExperiencia = subServiceDriverExperiencia.getDriverExperiencia();
                List<ProtocoloDTO> protocolos = subServiceProtocolo.getProtocolos();
                List<NegocioDTO> negocios = subServiceNegocio.getNegocios();
                negocios.forEach(negocioDTO -> {
                    negocioDTO.setCategorias(null);
                });

                metadata.setEjes(ejes);
                metadata.setDisciplinas(disciplinas);
                metadata.setStatus(status);
                metadata.setTipoMedicion(tipoMedicion);
                metadata.setFrecuenciaMedicion(frecuenciaMedicion);
                metadata.setEjecuta(ejecuta);
                metadata.setDriverExperienciaDTO(driverExperiencia);
                metadata.setProtocolos(protocolos);
                metadata.setNegocios(negocios);

                Integer idModeloEje;
                List<EjeModeloDTO> ejesModelo;
                Object dataModeloFranquiciaDTO = subServiceModelosFranquicia.getModelo(id_modelo.intValue());
                ModeloFranquiciaDTO modeloFranquiciaDTO = objectMapper.convertValue(dataModeloFranquiciaDTO, ModeloFranquiciaDTO.class);
                Object dataNegocioModeloDTO = subServiceDisciplinasNegocio.getNegociosModelo(modeloFranquiciaDTO.getIdModelo());
                List<NegocioModeloFranquiciaDTO> negocioModeloDTO;
                Object ejesModeloBucket = subServiceEjesModelo.getEjesModelo(modelo.getIdModelo());
                if (ejesModeloBucket != null) {
                    ejesModelo = objectMapper.convertValue(ejesModeloBucket, objectMapper.getTypeFactory().constructCollectionType(List.class, EjeModeloDTO.class));
                    if (ejesModelo != null) {
                        List<DisciplinaEjeDTO> disciplinasEje;
                        Object disciplinasEjeBucket;
                        for (EjeModeloDTO ejeModelo : ejesModelo) {
                            idModeloEje = ejeModelo.getIdModeloEje();
                            disciplinasEjeBucket = subServiceDisciplinasEje.getDisciplinasEje(idModeloEje);
                            if (disciplinasEjeBucket != null) {
                                disciplinasEje = objectMapper.convertValue(disciplinasEjeBucket, objectMapper.getTypeFactory().constructCollectionType(List.class, DisciplinaEjeDTO.class));
                                ejeModelo.setDisciplinasEje(disciplinasEje);
                            }
                        }
                        modelo.setEjesModelo(ejesModelo);
                    }
                    if (dataNegocioModeloDTO != null) {
                        negocioModeloDTO = objectMapper.convertValue(dataNegocioModeloDTO, objectMapper.getTypeFactory().constructCollectionType(List.class, NegocioModeloFranquiciaDTO.class));
                        modelo.setNegociosModelo(negocioModeloDTO);
                    }
                }
                dataModeloDTO.setMetadata(metadata);
                dataModeloDTO.setModeloDTO(modelo);
            } else {
                throw new CustomException(ModelCodes.DATA_NOT_FOUND);
            }
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
        return dataModeloDTO;
    }

    private void setEmpleado(ModeloFranquiciaDTO modeloFranquiciaDTO) throws CustomException {
        if (modeloFranquiciaDTO.getIdEmpleadoCrea() != null) {
            UsuarioDTO usuarioCrea = subServiceUsuario.getUsuario(modeloFranquiciaDTO.getIdEmpleadoCrea());
            modeloFranquiciaDTO.setEmpleadoCrea(usuarioCrea);
            modeloFranquiciaDTO.setIdEmpleadoCrea(null);
        }
        if (modeloFranquiciaDTO.getIdEmpleadoModifica() != null) {
            UsuarioDTO usuarioModifica = subServiceUsuario.getUsuario(modeloFranquiciaDTO.getIdEmpleadoModifica());
            modeloFranquiciaDTO.setEmpleadoModificacion(usuarioModifica);
            modeloFranquiciaDTO.setIdEmpleadoModifica(null);
        }
    }

    /**
     *
     * @param modeloFranquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModeloFranquiciaDTO deleteModelo(@RequestBody ModeloFranquiciaDTO modeloFranquicia) throws CustomException {
        if (modeloFranquicia.getIdModelo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modelo"));
        }
        if (modeloFranquicia.getIdEmpleadoModifica() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_empleado_modifica"));
        }
        return modeloFranquiciaDAOImpl.deleteRowModelo(modeloFranquicia);
    }

    /**
     *
     * @param modeloFranquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/cud", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModeloFranquiciaDTO cudModelo(@RequestBody ModeloFranquiciaDTO modeloFranquicia) throws CustomException {
        if (modeloFranquicia.getIdEmpleadoModifica() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_empleado_modifica"));
        }
        if (modeloFranquicia.getIdFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_franquicia"));
        }
        return modeloFranquiciaDAOImpl.cudModelo(modeloFranquicia);
    }

    /**
     *
     * @param headers
     * @param idFranquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/data/franquicia/{id_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModeloFranquiciaDTO getData(@RequestHeader HttpHeaders headers, @PathVariable("id_franquicia") Long idFranquicia) throws CustomException {
        final DataModeloFranquiciaDTO dataModeloDTO = new DataModeloFranquiciaDTO();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            ModeloFranquiciaDTO modelo = new ModeloFranquiciaDTO();
            subServiceEje.init(basePath, httpEntity);
            subServiceDisciplina.init(basePath, httpEntity);
            subServiceStatus.init(basePath, httpEntity);
            subServiceTipoMedicion.init(basePath, httpEntity);
            subServiceFrecuenciaMedicion.init(basePath, httpEntity);
            subServiceEjecuta.init(basePath, httpEntity);
            subServiceEjesModelo.init(basePath, httpEntity);
            subServiceDisciplinasEje.init(basePath, httpEntity);
            subServiceProtocolo.init(basePath, httpEntity);
            subServiceDisciplinasNegocio.init(basePath, httpEntity);
            subServiceNegocio.init(basePath, httpEntity);

            List<EjeDTO> ejes = subServiceEje.getEjes();
            List<DisciplinaDTO> disciplinas = subServiceDisciplina.getDisciplina();
            List<StatusDTO> status = subServiceStatus.getStatus();
            List<TipoMedicionDTO> tipoMedicion = subServiceTipoMedicion.getTipoMedicion();
            List<FrecuenciaMedicionDTO> frecuenciaMedicion = subServiceFrecuenciaMedicion.getFrecuenciaMedicion();
            List<EjecutaDTO> ejecuta = subServiceEjecuta.getEjecuta();
            List<DriverExperienciaDTO> driverExperiencia = subServiceDriverExperiencia.getDriverExperiencia();
            List<ProtocoloDTO> protocolos = subServiceProtocolo.getProtocolos();
            List<NegocioDTO> negocios = subServiceNegocio.getNegocios();
            negocios.forEach(negocioDTO -> {
                negocioDTO.setCategorias(null);
            });

            metadata.setEjes(ejes);
            metadata.setDisciplinas(disciplinas);
            metadata.setStatus(status);
            metadata.setTipoMedicion(tipoMedicion);
            metadata.setFrecuenciaMedicion(frecuenciaMedicion);
            metadata.setEjecuta(ejecuta);
            metadata.setDriverExperienciaDTO(driverExperiencia);
            metadata.setProtocolos(protocolos);
            metadata.setNegocios(negocios);

            Object dataNegociosFranquiciaDTO = subServiceNegocio.getNegociosFranquicia(idFranquicia.intValue());
            if (dataNegociosFranquiciaDTO != null) {
                List<NegocioModeloFranquiciaDTO> negocioModeloDTO = objectMapper.convertValue(dataNegociosFranquiciaDTO, objectMapper.getTypeFactory().constructCollectionType(List.class, NegocioModeloFranquiciaDTO.class));
                negocioModeloDTO.forEach((NegocioModeloFranquiciaDTO negocioModelo) -> {
                    negocioModelo.setEjesNegocio(new ArrayList<>());
                });
                modelo.setNegociosModelo(negocioModeloDTO);
                modelo.setEjesModelo(new ArrayList<>());
            }
            dataModeloDTO.setMetadata(metadata);
            dataModeloDTO.setModeloDTO(modelo);
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return dataModeloDTO;
    }

}
