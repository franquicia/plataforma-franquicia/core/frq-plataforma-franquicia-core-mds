/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.cecos.mtto.rest;

import com.gs.baz.frq.cecos.mtto.dao.CecosMttoDAOImpl;
import com.gs.baz.frq.cecos.mtto.dto.CecosMttoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/cecos-mtto")
@Component("FRQServiceCecosMtto")
public class ServiceCecosMtto {

    @Autowired
    private CecosMttoDAOImpl cecosMttoDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

//negocio
    @RequestMapping(value = "secure/get/cecosMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecosMttoDTO> getcecosMtto(@RequestBody CecosMttoDTO negocio) throws CustomException {

        return (List<CecosMttoDTO>) cecosMttoDAOImpl.selectRowDTO(negocio);

    }

    @RequestMapping(value = "secure/post/alta/cecosMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltacecosMtto(@RequestBody CecosMttoDTO asignacion) throws CustomException {

        if (asignacion.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta ceco mtto"));
        }
        return cecosMttoDAOImpl.insertRow(asignacion);
    }

    @RequestMapping(value = "secure/put/cecosMtto", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public CecosMttoDTO putUpdateCecosMtto(@RequestBody CecosMttoDTO updateNego) throws CustomException {
        if (updateNego.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update ceco mtto"));
        }

        return cecosMttoDAOImpl.updateRow(updateNego);
    }

    @RequestMapping(value = "secure/delete/cecosMtto", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaCecosMtto(@RequestBody CecosMttoDTO elimina) throws CustomException {
        if (elimina.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Dep cecos mtto"));
        }

        return cecosMttoDAOImpl.eliminaRows(elimina);
    }

    @RequestMapping(value = "secure/post/ejecuta/asignacionesMasivaCecos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postEjecutaAsignaciones(@RequestBody List<CecosMttoDTO> asignacion) throws Exception {
        ArrayList<CecosMttoDTO> listaNoCargados = new ArrayList<CecosMttoDTO>();

        int estatusAsignaciones = 0;

        if (asignacion == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta cecos-mtto"));
        } else {

            for (CecosMttoDTO aux : asignacion) {
                CecosMttoDTO asignacion1 = new CecosMttoDTO();
                asignacion1.setIdCeco(aux.getIdCeco());
                asignacion1.setIdCecoSuperior(aux.getIdCecoSuperior());
                asignacion1.setNegocio(aux.getNegocio());
                asignacion1.setObservaciones(aux.getObservaciones());

                estatusAsignaciones = cecosMttoDAOImpl.insertRow(asignacion1);

            }
        }
        return estatusAsignaciones;
    }

    /*
  //Jefe-Usuario
  	@RequestMapping(value = "secure/get/jefeResponsable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
      public List<JefeSuperiorMttoDTO > getJefeResponsable(@RequestBody JefeSuperiorMttoDTO negocio) throws CustomException {
  		 

          return (List<JefeSuperiorMttoDTO>) cecosMttoDAOImpl.selectRowJefe(negocio);
          
      }
  	
      @RequestMapping(value = "secure/post/alta/jefeResponsable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
      public int postJefeResponsable(@RequestBody JefeSuperiorMttoDTO asignacion) throws CustomException {

          if (asignacion.getIdUsuarioJefe() == null) {
              throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Jefe-Usuario"));
          }
          return cecosMttoDAOImpl.insertJefeRow(asignacion);
      }
      
      @RequestMapping(value = "secure/put/jefeResponsable", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
      public JefeSuperiorMttoDTO putJefeResponsable(@RequestBody JefeSuperiorMttoDTO updateNego) throws CustomException {
          if (updateNego.getIdUsuarioJefe() == null) {
              throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update Jefe-Usuario"));
          }
          
          return cecosMttoDAOImpl.updateJefeRow(updateNego);
      } 
      @RequestMapping(value = "secure/delete/jefeResponsable", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
      public int eliminaJefeResponsable(@RequestBody JefeSuperiorMttoDTO elimina) throws CustomException {
          if (elimina.getIdUsuarioJefe() == null) {
              throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Dep Jefe-Usuario"));
          }
         
          return cecosMttoDAOImpl.eliminaJefeRows(elimina);
      }
     */
}
