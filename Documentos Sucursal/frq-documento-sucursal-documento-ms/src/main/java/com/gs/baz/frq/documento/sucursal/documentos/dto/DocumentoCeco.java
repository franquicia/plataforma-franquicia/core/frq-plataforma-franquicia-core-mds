/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author b191312
 */
@ApiModel(description = "Datos del documento por ceco", value = "DocumentoCeco")
public class DocumentoCeco {

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Identificador del ceco", example = "920100")
    private Integer idCeco;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del Negocio", example = "1")
    private Integer idNegocio;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Identificador del Formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "nombreFormato")
    @ApiModelProperty(notes = "Identificador del Formato", example = "RETAIL")
    private String nombreFormato;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del idProyecto", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Identificador del nombre proyecto", example = "ELEKTRA")
    private String nombreProyecto;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del idDocumento", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Identificador del nombre Documento", example = "1")
    private String nombreDocumento;

    @JsonProperty(value = "idGeneraHistorico")
    @ApiModelProperty(notes = "Identificador si genera historico o no", example = "1")
    private Integer idGeneraHistorico;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public Integer getIdGeneraHistorico() {
        return idGeneraHistorico;
    }

    public void setIdGeneraHistorico(Integer idGeneraHistorico) {
        this.idGeneraHistorico = idGeneraHistorico;
    }
}
