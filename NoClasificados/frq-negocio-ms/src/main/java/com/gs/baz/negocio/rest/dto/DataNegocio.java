/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.negocio.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import com.gs.baz.negocio.dto.NegocioDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class DataNegocio {

    @JsonProperty(value = "metadata")
    private Metadata metadata;

    @JsonProperty(value = "negocios")
    private List<NegocioDTO> negocios;

    @JsonProperty(value = "negocio")
    private NegocioDTO negocio;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<NegocioDTO> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<NegocioDTO> negocios) {
        this.negocios = negocios;
    }

    public NegocioDTO getNegocio() {
        return negocio;
    }

    public void setNegocio(NegocioDTO negocio) {
        this.negocio = negocio;
    }

    @Override
    public String toString() {
        return "DataNegocio{" + "metadata=" + metadata + ", negocios=" + negocios + ", negocio=" + negocio + '}';
    }

}
