/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaSemaforoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class GraficaSemaforoRowMapper implements RowMapper<GraficaSemaforoBase> {

    @Override
    public GraficaSemaforoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        GraficaSemaforoBase graficaBase = new GraficaSemaforoBase();
        graficaBase.setIdSemaforo(rs.getInt("FIIDSEMAFORO"));
        return graficaBase;
    }
}
