/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modulo.rest;

import com.gs.baz.frq.modulo.dao.ModuloDAOImpl;
import com.gs.baz.frq.modulo.dto.ModuloDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/modulo")
public class ServiceModulo {

    @Autowired
    private ModuloDAOImpl moduloFlujoDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloDTO> getModulos() throws CustomException {
        return moduloFlujoDAOImpl.selectRows();
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloDTO> getModulosAll() throws CustomException {
        return moduloFlujoDAOImpl.selectRowsAll();
    }

    /**
     *
     * @param idModulo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_modulo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloDTO getModulo(@PathVariable("id_modulo") Long idModulo) throws CustomException {
        return moduloFlujoDAOImpl.selectRow(idModulo);
    }

    /**
     *
     * @param moduloFlujoDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloDTO postModulo(@RequestBody ModuloDTO moduloFlujoDTO) throws CustomException {
        if (moduloFlujoDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (moduloFlujoDTO.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (moduloFlujoDTO.getEsWeb() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("es_web"));
        }
        return moduloFlujoDAOImpl.insertRow(moduloFlujoDTO);
    }

    /**
     *
     * @param moduloFlujoDTOs
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/cu", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloDTO> cuModulos(@RequestBody List<ModuloDTO> moduloFlujoDTOs) throws CustomException {
        int index = 0;
        for (ModuloDTO moduloFlujoDTO : moduloFlujoDTOs) {
            index++;
            if (moduloFlujoDTO.getDescripcion() == null) {
                throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion index " + index));
            }
            if (moduloFlujoDTO.getModulo() == null) {
                throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo index " + index));
            }
            if (moduloFlujoDTO.getEsWeb() == null) {
                throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("es_web index " + index));
            }
        }
        return moduloFlujoDAOImpl.insertRow(moduloFlujoDTOs);
    }

    /**
     *
     * @param moduloFlujoDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloDTO putModulo(@RequestBody ModuloDTO moduloFlujoDTO) throws CustomException {
        if (moduloFlujoDTO.getIdModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modulo"));
        }
        if (moduloFlujoDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (moduloFlujoDTO.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (moduloFlujoDTO.getEsWeb() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("es_web"));
        }
        return moduloFlujoDAOImpl.updateRow(moduloFlujoDTO);
    }

    /**
     *
     * @param moduloFlujoDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloDTO deleteModulo(@RequestBody ModuloDTO moduloFlujoDTO) throws CustomException {
        if (moduloFlujoDTO.getIdModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modulo"));
        }
        return moduloFlujoDAOImpl.deleteRow(moduloFlujoDTO);
    }

}
