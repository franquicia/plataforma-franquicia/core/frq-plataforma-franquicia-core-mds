package com.gs.baz.frq.documentos.sucursal.folio.init;

import com.gs.baz.frq.documentos.sucursal.folio.dao.DocumentoSucursalFolioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documentos.sucursal.folio")
public class ConfigDocumentoSucursalFolio {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalFolio() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalFolioDAOImpl documentoSucursalFolioDAOImpl() {
        return new DocumentoSucursalFolioDAOImpl();
    }

}
