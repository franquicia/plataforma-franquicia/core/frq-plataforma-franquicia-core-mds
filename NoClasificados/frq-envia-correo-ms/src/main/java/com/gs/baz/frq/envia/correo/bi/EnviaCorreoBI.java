package com.gs.baz.frq.envia.correo.bi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gs.baz.frq.envia.correo.dao.EnviaCorreoDAOImpl;
import com.gs.baz.frq.envia.correo.dto.ConteoNegocioSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.dto.DatosEntradaCorreoDTO;
import com.gs.baz.frq.envia.correo.dto.DatosEntradaCorreoSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.dto.ObtieneConteoNegocioSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.dto.ResponseReporteLayout;
import com.gs.baz.frq.model.commons.CustomException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class EnviaCorreoBI {

    @Autowired
    private EnviaCorreoDAOImpl enviaCorreoDAO;

    private final Logger logger = LogManager.getLogger();

    public void init() {
    }

    //Complementar servicio con adjunto de Base64
    @SuppressWarnings("unchecked")
    public boolean enviaCorreo(DatosEntradaCorreoDTO datosEntradaCorreo) throws CustomException {

        boolean response = true;

        ArrayList<String> copiados = datosEntradaCorreo.getContactosCopiados();

        File adjunto = null;

        try {
            // Propiedades de la conexion
            String host = "10.63.200.79";
            Properties props = new Properties();
            props.put("mail.smtp.host", host);
            try {
                Session session = Session.getInstance(props, null);
                MimeMessage msg = new MimeMessage(session);
                // Se setea la direccion de la que viene el correo
                msg.setFrom(new InternetAddress(datosEntradaCorreo.getCorreoRemitente(), datosEntradaCorreo.getRemitente()));
                // Se setea para quién va el correo
                msg.setRecipients(Message.RecipientType.TO, datosEntradaCorreo.getDestinatario());
                // Se setea para quienes se copia el correo
                String copia = "";

                for (String dest : copiados) {
                    copia += dest + ",";
                }
                if (copia != null && copia != "") {
                    msg.setRecipients(Message.RecipientType.BCC, copia);
                }

                msg.setSubject(MimeUtility.encodeText(datosEntradaCorreo.getAsunto(), "utf-8", "B"));
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setContent(datosEntradaCorreo.getCuerpoCorreo(), "text/html");
                msg.setContent(datosEntradaCorreo.getCuerpoCorreo(), "text/html; charset=iso-8859-1");

                // Adjuntar documento
                // JavaMail 1.4
                if (adjunto != null) {
                    Multipart multipart = new MimeMultipart();
                    // creates body part for the attachment
                    MimeBodyPart attachPart = new MimeBodyPart();
                    // code to add attachment...will be revealed later
                    // adds parts to the multipart
                    String attachFile = adjunto.getAbsolutePath();
                    // multipart.addBodyPart(attachPart);
                    multipart.addBodyPart(messageBodyPart);
                    attachPart.attachFile(attachFile);
                    // attachPart.attachFile(adjunto.getName().substring(0, 10));
                    multipart.addBodyPart(attachPart);
                    // sets the multipart as message's content
                    msg.setContent(multipart);
                }
                // Se envía el correo
                Transport.send(msg);

                //Se elimina el adjunto
                if (adjunto != null) {
                    adjunto.delete();
                    adjunto.deleteOnExit();
                }

            } catch (Exception e) {
                logger.info("Algo paso CATCH ANIDADO... " + e);
                e.printStackTrace();
                return false;
            }
        } catch (Exception e) {
            logger.info("Algo paso CATCH GENERAL... " + e);
            e.printStackTrace();
            return false;
        }

        return response;

    }

    @SuppressWarnings("unchecked")
    public ResponseReporteLayout enviaCorreoSemanalLayouts(DatosEntradaCorreoSemanalLayoutsDTO datosEntradaCorreoSemanalLayouts) throws CustomException {

        ResponseReporteLayout response = new ResponseReporteLayout();
        response.response = false;

        List<ConteoNegocioSemanalLayoutsDTO> listaConteos = (List<ConteoNegocioSemanalLayoutsDTO>) enviaCorreoDAO.ObtieneConteoNegocioSemanalLayouts(datosEntradaCorreoSemanalLayouts).get("listaConteos");
        List<ObtieneConteoNegocioSemanalLayoutsDTO> listaConsulta = (List<ObtieneConteoNegocioSemanalLayoutsDTO>) enviaCorreoDAO.ObtieneInformacionGeneralSemanalLayouts(datosEntradaCorreoSemanalLayouts).get("listaConsulta");

        String fechaInicioString = datosEntradaCorreoSemanalLayouts.getFechaInicialReporte().substring(0, 2) + "/" + datosEntradaCorreoSemanalLayouts.getFechaInicialReporte().substring(2, 4) + "/" + datosEntradaCorreoSemanalLayouts.getFechaInicialReporte().substring(4, 6);
        String fechaFinString = datosEntradaCorreoSemanalLayouts.getFechaFinalReporte().substring(0, 2) + "/" + datosEntradaCorreoSemanalLayouts.getFechaFinalReporte().substring(2, 4) + "/" + datosEntradaCorreoSemanalLayouts.getFechaFinalReporte().substring(4, 6);

        DatosEntradaCorreoDTO objGeneralCorreo = new DatosEntradaCorreoDTO();
        objGeneralCorreo.setAdjunto("");
        objGeneralCorreo.setAsunto(datosEntradaCorreoSemanalLayouts.getAsunto());

        ArrayList<String> contactosCopiados = new ArrayList<String>();
        contactosCopiados = datosEntradaCorreoSemanalLayouts.getContactosCopiados();

        objGeneralCorreo.setContactosCopiados(contactosCopiados);
        objGeneralCorreo.setCorreoRemitente("layoutsExpansion@elektra.com.mx");
        objGeneralCorreo.setCuerpoCorreo("<html><body><p>Se comparte el listado de las publicaciones o actualizaciones agregadas al Portal de Layouts realizadas del d&iacute;a " + fechaInicioString + " al d&iacute;a " + fechaFinString + "</p><br></br><p>Quedamos al pendiente de cualquier duda comentario o duda.</p><br></br><p><b>Saludos Cordiales.</b></p><br></br><div><img src='https://www.gruposalinas.com/images/logo.png' height='70' width='90'></img></div></body></html>");
        objGeneralCorreo.setDestinatario(datosEntradaCorreoSemanalLayouts.getDestinatario());
        objGeneralCorreo.setExtensionAdjunto(".pdf");
        objGeneralCorreo.setNombreAdjunto("reporteCargaLayoutsExpansion");
        objGeneralCorreo.setRemitente("Reporte de Carga Layouts Expansión " + fechaInicioString + " - " + fechaFinString);

        File pathFile = writePDF(objGeneralCorreo, datosEntradaCorreoSemanalLayouts, listaConteos, listaConsulta);

        response.response = enviaCorreoSemanaLayout(objGeneralCorreo, pathFile);

        if (pathFile != null) {
            pathFile.delete();
            pathFile.deleteOnExit();
        }
        return response;

    }

    private static File writePDF(DatosEntradaCorreoDTO obCorreoDTO, DatosEntradaCorreoSemanalLayoutsDTO datosEntradaCorreoSemanalLayouts, List<ConteoNegocioSemanalLayoutsDTO> listaConteos, List<ObtieneConteoNegocioSemanalLayoutsDTO> listaConsulta) {

        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        File pathFile = new File(obCorreoDTO.getNombreAdjunto() + obCorreoDTO.getExtensionAdjunto());

        try {
            OutputStream output = new FileOutputStream(pathFile);

            PdfWriter.getInstance(document, output);

            document.open();

            String fechaInicioString = datosEntradaCorreoSemanalLayouts.getFechaInicialReporte().substring(0, 2) + "/" + datosEntradaCorreoSemanalLayouts.getFechaInicialReporte().substring(2, 4) + "/" + datosEntradaCorreoSemanalLayouts.getFechaInicialReporte().substring(4, 6);
            String fechaFinString = datosEntradaCorreoSemanalLayouts.getFechaFinalReporte().substring(0, 2) + "/" + datosEntradaCorreoSemanalLayouts.getFechaFinalReporte().substring(2, 4) + "/" + datosEntradaCorreoSemanalLayouts.getFechaFinalReporte().substring(4, 6);

            //Image imagen = Image.getInstance("/Users/carlos/Downloads/cabecera.png");
            Image imagen = Image.getInstance("http://10.53.33.82/franquicia/cabeceraLayoutSemana.png");
            imagen.scalePercent(50);//esto es opcional para definir el tamaño de la imagen.
            imagen.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            document.add(imagen);

            Font f1Bold = new Font();
            f1Bold.setFamily(FontFamily.TIMES_ROMAN.name());
            f1Bold.setStyle(Font.BOLD);
            f1Bold.setSize(8);

            Font f2 = new Font();
            f2.setFamily(FontFamily.TIMES_ROMAN.name());
            f2.setSize(8);

            Paragraph parrafoEspacio = new Paragraph();
            parrafoEspacio.setFont(f1Bold);
            parrafoEspacio.add(" ");
            parrafoEspacio.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            document.add(parrafoEspacio);

            Paragraph parrafoTexto = new Paragraph();
            parrafoTexto.setFont(f2);
            parrafoTexto.add("Se comparte el listado de las publicaciones o actualizaciones agregadas al Portal de Layouts realizadas del día " + fechaInicioString + " al día " + fechaFinString); // ---- Nombre sucursal
            parrafoTexto.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            document.add(parrafoTexto);

            Paragraph parrafoResumen = new Paragraph();
            parrafoResumen.setFont(f1Bold);
            parrafoResumen.add("Resumen:");
            parrafoResumen.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            document.add(parrafoResumen);
            document.add(parrafoEspacio);

            PdfPTable tablaResumen = new PdfPTable(1);
            tablaResumen.setWidthPercentage(100);
            tablaResumen.getDefaultCell().setBorder(0);

            Paragraph parrafoDivide = new Paragraph();
            parrafoDivide.setFont(f1Bold);
            parrafoDivide.add(" ");
            parrafoDivide.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            PdfPCell cellDivide = new PdfPCell(parrafoDivide);
            cellDivide.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
            cellDivide.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            cellDivide.setBackgroundColor(WebColors.getRGBColor("#FFC300"));
            cellDivide.setFixedHeight(1);
            tablaResumen.addCell(cellDivide);

            for (ConteoNegocioSemanalLayoutsDTO obj : listaConteos) {

                Paragraph parrafo = new Paragraph();
                parrafo.setFont(f2);
                parrafo.add("Se agregaron " + obj.getValorConteo() + " sucursales referentes al proyecto " + obj.getNombreProyecto() + " del negocio " + obj.getIdNegocio()); // ---- Nombre sucursal
                parrafo.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);

                PdfPCell cell2 = new PdfPCell(parrafo);
                cell2.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
                cell2.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                cell2.setBackgroundColor(WebColors.getRGBColor("#efefef"));
                cell2.setExtraParagraphSpace(3);

                tablaResumen.addCell(cell2);

            }

            document.add(tablaResumen);
            document.add(parrafoEspacio);

            //--------------Tabla respuestas ------------------
            PdfPTable tableResp = new PdfPTable(5);
            tableResp.setWidthPercentage(100);
            tableResp.getDefaultCell().setBorder(0);
            float[] medidaCeldasResp = {10.0f, 10.0f, 10.0f, 10.0f, 10.0f};//
            tableResp.setWidths(medidaCeldasResp);

            Paragraph columnaCeco = new Paragraph("CECO");////----------Nombre de CECO
            columnaCeco.getFont().setStyle(Font.BOLD);
            columnaCeco.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            columnaCeco.getFont().setSize(7);

            PdfPCell cell = new PdfPCell(columnaCeco);
            cell.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            cell.setBackgroundColor(WebColors.getRGBColor("#efefef"));
            tableResp.addCell(cell);

            Paragraph columnaNegocio = new Paragraph("Negocio");////----------Nombre de CECO
            columnaNegocio.getFont().setStyle(Font.BOLD);
            columnaNegocio.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            columnaNegocio.getFont().setSize(7);

            PdfPCell cellNegocio = new PdfPCell(columnaNegocio);
            cellNegocio.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
            cellNegocio.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            cellNegocio.setBackgroundColor(WebColors.getRGBColor("#efefef"));
            tableResp.addCell(cellNegocio);

            Paragraph columnaSucursal = new Paragraph("Sucursal");
            columnaSucursal.getFont().setStyle(Font.BOLD);
            columnaSucursal.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            columnaSucursal.getFont().setSize(7);

            PdfPCell cell2 = new PdfPCell(columnaSucursal);
            cell2.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            cell2.setBackgroundColor(WebColors.getRGBColor("#efefef"));
            tableResp.addCell(cell2);

            Paragraph columnaProyecto = new Paragraph("Proyecto");
            columnaProyecto.getFont().setStyle(Font.BOLD);
            columnaProyecto.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            columnaProyecto.getFont().setSize(7);

            PdfPCell cell3 = new PdfPCell(columnaProyecto);
            cell3.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
            cell3.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            cell3.setBackgroundColor(WebColors.getRGBColor("#efefef"));
            tableResp.addCell(cell3);

            Paragraph columnaFecha = new Paragraph("Fecha Publicación");
            columnaFecha.getFont().setStyle(Font.BOLD);
            columnaFecha.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            columnaFecha.getFont().setSize(7);

            PdfPCell cell4 = new PdfPCell(columnaFecha);
            cell4.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
            cell4.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            cell4.setBackgroundColor(WebColors.getRGBColor("#efefef"));
            tableResp.addCell(cell4);

            PdfPTable tableDataGeneral = new PdfPTable(5);
            tableDataGeneral.setWidthPercentage(100);
            tableDataGeneral.getDefaultCell().setBorder(0);
            float[] medidaCeldas = {10.0f, 10.0f, 10.0f, 10.0f, 10.0f};//
            tableDataGeneral.setWidths(medidaCeldas);

            for (int i = 0; i < listaConsulta.size(); i++) {

                ObtieneConteoNegocioSemanalLayoutsDTO obj = listaConsulta.get(i);

                String idCecoString = (obj.getIdCeco() != null) ? obj.getIdCeco().toString() : "";

                Paragraph parrafoCecoParam = new Paragraph(idCecoString);////----------Nombre de CECO
                parrafoCecoParam.getFont().setStyle(Font.NORMAL);
                parrafoCecoParam.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                parrafoCecoParam.getFont().setSize(7);

                PdfPCell cellCeco = new PdfPCell(parrafoCecoParam);
                cellCeco.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
                cellCeco.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                cellCeco.setExtraParagraphSpace(2);

                String negocioCecoString = getNegocioCeco(idCecoString);

                Paragraph parrafoNegocioParam = new Paragraph(negocioCecoString);////----------Nombre de CECO
                parrafoNegocioParam.getFont().setStyle(Font.NORMAL);
                parrafoNegocioParam.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                parrafoNegocioParam.getFont().setSize(7);

                PdfPCell cellNombreNegocio = new PdfPCell(parrafoNegocioParam);
                cellNombreNegocio.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
                cellNombreNegocio.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                cellNombreNegocio.setExtraParagraphSpace(2);

                String nombreCecoString = (obj.getNombreCeco() != null) ? obj.getNombreCeco() : "";

                Paragraph columnaSucursalParam = new Paragraph(nombreCecoString);
                columnaSucursalParam.getFont().setStyle(Font.NORMAL);
                columnaSucursalParam.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);

                columnaSucursalParam.getFont().setSize(7);

                PdfPCell cellSucursal = new PdfPCell(columnaSucursalParam);
                cellSucursal.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
                cellSucursal.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                cellSucursal.setExtraParagraphSpace(2);

                String nombreProyectoString = (obj.getNombreProyecto() != null) ? obj.getNombreProyecto() : "";

                Paragraph columnaProyectoParam = new Paragraph(nombreProyectoString);
                columnaProyectoParam.getFont().setStyle(Font.NORMAL);
                columnaProyectoParam.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                columnaProyectoParam.getFont().setSize(7);

                PdfPCell cellProyecto = new PdfPCell(columnaProyectoParam);
                cellProyecto.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
                cellProyecto.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                cellProyecto.setExtraParagraphSpace(2);

                String fechaString = (obj.getFecha() != null) ? obj.getFecha() : "";

                Paragraph columnaFechaParam = new Paragraph(fechaString);
                columnaFechaParam.getFont().setStyle(Font.NORMAL);
                columnaFechaParam.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                columnaFechaParam.getFont().setSize(7);

                PdfPCell cellFecha = new PdfPCell(columnaFechaParam);
                cellFecha.setBorder(com.itextpdf.text.Rectangle.NO_BORDER);
                cellFecha.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                cellFecha.setExtraParagraphSpace(2);

                if (i % 2 != 0) {
                    cellCeco.setBackgroundColor(WebColors.getRGBColor("#efefef"));
                    cellNombreNegocio.setBackgroundColor(WebColors.getRGBColor("#efefef"));
                    cellSucursal.setBackgroundColor(WebColors.getRGBColor("#efefef"));
                    cellProyecto.setBackgroundColor(WebColors.getRGBColor("#efefef"));
                    cellFecha.setBackgroundColor(WebColors.getRGBColor("#efefef"));
                }

                if (i == 0) {
                    tableDataGeneral.addCell(cellDivide);
                    tableDataGeneral.addCell(cellDivide);
                    tableDataGeneral.addCell(cellDivide);
                    tableDataGeneral.addCell(cellDivide);
                    tableDataGeneral.addCell(cellDivide);
                }

                tableDataGeneral.addCell(cellCeco);
                tableDataGeneral.addCell(cellNombreNegocio);
                tableDataGeneral.addCell(cellSucursal);
                tableDataGeneral.addCell(cellProyecto);
                tableDataGeneral.addCell(cellFecha);
            }

            document.add(tableResp);
            document.add(tableDataGeneral);
            document.add(parrafoEspacio);

            Image imagenPie = Image.getInstance("http://10.53.33.82/franquicia/pieFirmaLayoutSemana.png");
            imagenPie.scalePercent(50);//esto es opcional para definir el tamaño de la imagen.
            imagenPie.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            document.add(imagenPie);

            document.close();
            output.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pathFile;

    }

    public boolean enviaCorreoSemanaLayout(DatosEntradaCorreoDTO datosEntradaCorreo, File adjunto) throws CustomException {

        boolean response = true;

        ArrayList<String> copiados = datosEntradaCorreo.getContactosCopiados();

        try {
            String host = "10.63.200.79";
            Properties props = new Properties();
            props.put("mail.smtp.host", host);
            try {
                Session session = Session.getInstance(props, null);
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom(new InternetAddress(datosEntradaCorreo.getCorreoRemitente(), datosEntradaCorreo.getRemitente()));
                msg.setRecipients(Message.RecipientType.TO, datosEntradaCorreo.getDestinatario());
                String copia = "";

                for (String dest : copiados) {
                    copia += dest + ",";
                }
                if (copia != null && copia != "") {
                    msg.setRecipients(Message.RecipientType.BCC, copia);
                }

                msg.setSubject(MimeUtility.encodeText(datosEntradaCorreo.getAsunto(), "utf-8", "B"));
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setContent(datosEntradaCorreo.getCuerpoCorreo(), "text/html");
                msg.setContent(datosEntradaCorreo.getCuerpoCorreo(), "text/html; charset=iso-8859-1");

                // JavaMail 1.4
                if (adjunto != null) {
                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart attachPart = new MimeBodyPart();
                    String attachFile = adjunto.getAbsolutePath();
                    multipart.addBodyPart(messageBodyPart);
                    attachPart.attachFile(attachFile);
                    multipart.addBodyPart(attachPart);
                    msg.setContent(multipart);
                }

                Transport.send(msg);

            } catch (Exception e) {
                logger.info("Algo paso CATCH ANIDADO... " + e);
                e.printStackTrace();
                return false;
            }
        } catch (Exception e) {
            logger.info("Algo paso CATCH GENERAL... " + e);
            e.printStackTrace();
            return false;
        }

        return response;

    }

    public static String getNegocioCeco(String ceco) {

        switch (ceco.substring(0, 2)) {

            case "92":
                return "EKT";
            case "48":
                return "BAZ";
            case "23":
                return "OCC";
            case "62":
                return "PPR";
            case "67":
                return "ITK";
            default:
                return "-";
        }

    }
}
