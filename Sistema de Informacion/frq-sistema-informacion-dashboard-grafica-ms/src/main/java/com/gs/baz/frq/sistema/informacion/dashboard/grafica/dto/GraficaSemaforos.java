/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de semaforos grafica", value = "GraficaSemaforos")
public class GraficaSemaforos {

    @JsonProperty(value = "semaforos")
    @ApiModelProperty(notes = "semaforos")
    private List<GraficaSemaforo> semaforos;

    public GraficaSemaforos(List<GraficaSemaforo> semaforos) {
        this.semaforos = semaforos;
    }

    public List<GraficaSemaforo> getSemaforos() {
        return semaforos;
    }

    public void setSemaforos(List<GraficaSemaforo> semaforos) {
        this.semaforos = semaforos;
    }

    @Override
    public String toString() {
        return "GraficaSemaforos{" + "semaforos=" + semaforos + '}';
    }

}
