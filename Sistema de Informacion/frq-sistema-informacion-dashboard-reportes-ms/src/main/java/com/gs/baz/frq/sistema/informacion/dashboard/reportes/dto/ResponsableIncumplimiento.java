/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del responsable incumplimiento", value = "ResponsableIncumplimiento")
public class ResponsableIncumplimiento {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del responsable", example = "Red Única")
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ResponsableIncumplimiento{" + "descripcion=" + descripcion + '}';
    }

}
