/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folio.etiquetas.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dao.FolioEtiquetaDAOImpl;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.AltaFolioEtiqueta;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.FolioEtiquetaBase;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.FolioEtiquetas;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.ParametrosFolioEtiqueta;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "folioEtiquetas", value = "folioEtiquetas", description = "Gestión de folioEtiquetas")
@RestController
@RequestMapping("/api-local/modelo-negocio/folio-etiquetas/v1")
public class FolioEtiquetaApi {

    @Autowired
    private FolioEtiquetaDAOImpl folioEtiquetaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @param idEtiqueta
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folioEtiqueta", notes = "Obtiene un folioEtiqueta", nickname = "obtieneFolioEtiqueta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/etiquetas/{idEtiqueta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FolioEtiquetaBase obtieneFolioEtiqueta(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "idEtiqueta", value = "Identificador del Etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Integer idEtiqueta) throws CustomException, DataNotFoundException {
        FolioEtiquetaBase folioEtiquetaBase = folioEtiquetaDAOImpl.selectRow(idFolio, idEtiqueta);
        if (folioEtiquetaBase == null) {
            throw new DataNotFoundException();
        }
        return folioEtiquetaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folioEtiquetas", notes = "Obtiene todos los folioEtiquetas", nickname = "obtieneFolioEtiquetas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/etiquetas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FolioEtiquetas obtieneFolioEtiquetas(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws CustomException, DataNotFoundException {
        FolioEtiquetas folioEtiquetas = new FolioEtiquetas(folioEtiquetaDAOImpl.selectAllRows(idFolio));
        if (folioEtiquetas.getFolioEtiquetas() != null && folioEtiquetas.getFolioEtiquetas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folioEtiquetas;
    }

    /**
     *
     * @param idFolio
     * @param parametrosFolioEtiqueta
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear folioEtiqueta", notes = "Agrega un folioEtiqueta", nickname = "creaFolioEtiqueta")
    @RequestMapping(value = "/{idFolio}/etiquetas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFolioEtiqueta creaFolioEtiqueta(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio, @ApiParam(name = "ParametrosFolioEtiqueta", value = "Paramentros para el alta del folioEtiqueta", required = true) @RequestBody ParametrosFolioEtiqueta parametrosFolioEtiqueta) throws DataNotInsertedException {
        parametrosFolioEtiqueta.setIdFolio(idFolio);
        return folioEtiquetaDAOImpl.insertRow(parametrosFolioEtiqueta);
    }

    /**
     *
     * @param parametrosFolioEtiqueta
     * @param idFolio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar folioEtiqueta", notes = "Actualiza un folioEtiqueta", nickname = "actualizaFolioEtiqueta")
    @RequestMapping(value = "/{idFolio}/etiquetas/{idEtiqueta}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFolioEtiqueta(@ApiParam(name = "ParametrosFolioEtiqueta", value = "Paramentros para la actualización del folio", required = true) @RequestBody ParametrosFolioEtiqueta parametrosFolioEtiqueta, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio) throws DataNotUpdatedException {
        parametrosFolioEtiqueta.setIdFolio(idFolio);
        folioEtiquetaDAOImpl.updateRow(parametrosFolioEtiqueta);
        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @param idFolioEtiqueta
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar folioEtiqueta", notes = "Elimina un item de las folioEtiquetas", nickname = "eliminaFolioEtiqueta")
    @RequestMapping(value = "/{idFolio}/etiquetas/{idEtiqueta}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaFolioEtiqueta(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "idFolioEtiqueta", value = "Identificador de la folioEtiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Integer idFolioEtiqueta) throws DataNotDeletedException {
        folioEtiquetaDAOImpl.deleteRow(idFolio, idFolioEtiqueta);
        return new SinResultado();
    }

}
