/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dao;

import com.gs.baz.frq.api.portal.cautivo.dto.IdentificadorDTO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author MADA
 */
public class IdentificadorDAOImpl extends DefaultDAO {

    private String schema;
    private DefaultJdbcCall jdbcInsertLocalizacion;
    private DefaultJdbcCall jdbcSelectLocalizacion;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertLocalizacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertLocalizacion.withSchemaName(schema);
        jdbcInsertLocalizacion.withCatalogName("PAADMCLIENTEICU");
        jdbcInsertLocalizacion.withProcedureName("SPINSUSRBDIGITAL");

    }

    public boolean insertRow(IdentificadorDTO identificadorDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCUSERNAME", identificadorDTO.getUsername());
        mapSqlParameterSource.addValue("PA_FDINICIO_SESION", identificadorDTO.getFechaEvento());
        mapSqlParameterSource.addValue("PA_FCSUCURSAL", identificadorDTO.getTienda());
        mapSqlParameterSource.addValue("PA_FCMAC", identificadorDTO.getMac());
        mapSqlParameterSource.addValue("PA_FCCLIENTE_ID", identificadorDTO.getIdentificadorCliente());

        Map<String, Object> out = jdbcInsertLocalizacion.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

        return success;

    }

}
