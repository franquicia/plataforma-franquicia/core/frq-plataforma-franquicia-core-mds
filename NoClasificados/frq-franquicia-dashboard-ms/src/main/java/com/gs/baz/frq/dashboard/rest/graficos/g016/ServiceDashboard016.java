/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g016;

import com.gs.baz.frq.dashboard.dao.graficos.g016.DashboardDAOImpl016;
import com.gs.baz.frq.dashboard.dto.graficos.g016.DistribucionCumplimientoCecoNegocioNivelDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g016.DistribucionCumplimientoNegocioNivelDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard016 {

    @Autowired
    private DashboardDAOImpl016 dashboardDAOImpl016;

    @RequestMapping(value = "secure/grafico/get/distribucion/cumplimiento/ceco/negocio/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DistribucionCumplimientoNegocioNivelDTO getDistribucionCumplimientoNegocioNivel(@RequestBody DistribucionCumplimientoCecoNegocioNivelDTO distribucionCumplimientoCecoNegocioNivelDTO) throws CustomException {
        return dashboardDAOImpl016.getDistribucionCumplimientoNivel(distribucionCumplimientoCecoNegocioNivelDTO);
    }
}
