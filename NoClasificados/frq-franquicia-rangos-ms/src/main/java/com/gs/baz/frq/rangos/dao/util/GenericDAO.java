/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.rangos.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.rangos.dto.RangosDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param entityId
     * @param rangoTypeId
     * @return @throws CustomException
     */
    public List<RangosDTO> selectRows(Long entityId, Long rangoTypeId) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
