/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.niveles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del nivel", value = "Nivel")
public class Nivel extends NivelBase {

    @JsonProperty(value = "idNivel")
    @ApiModelProperty(notes = "Identificador del nivel", example = "1", position = -1)
    private Integer idNivel;

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    @Override
    public String toString() {
        return "Nivel{" + "idModelo=" + idNivel + '}';
    }

}
