/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g006;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g006.DashboardCertificacionG006DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g006.DashboardCertificacionG006DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g006")
public class ServiceDashboardCertificacionG006 {

    @Autowired
    private DashboardCertificacionG006DAOImpl dashboardCertificacionG006DAOImpl;

    @RequestMapping(value = "secure/grafico/get/pendientes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG006DTO> getDatosPuntosPendientesG006() throws CustomException {

        return dashboardCertificacionG006DAOImpl.selectRowsDashboardCertificacionG006();
    }

    @RequestMapping(value = "secure/grafico/get/pendientes/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG006DTO> getDatoPuntosPendientesG006(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @RequestParam(value = "idGerente", required = false) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario) throws CustomException {

        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFin == null || fechaFin.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFin"));
        }

        return dashboardCertificacionG006DAOImpl.selectRowDashboardCertificacionG006(fechaInicial, fechaFin, idGerente, idNegocio, idUsuario);
    }
}
