/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.perfiles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ParametrosPerfilVistas {

    private transient Integer idPerfil;

    @JsonProperty(value = "vistas")
    @ApiModelProperty(notes = "Datos de las vistas")
    private List<ParametroVista> vistas;

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public List<ParametroVista> getVistas() {
        return vistas;
    }

    public void setVistas(List<ParametroVista> vistas) {
        this.vistas = vistas;
    }

    @Override
    public String toString() {
        return "ParametrosPerfilVistas{" + "idPerfil=" + idPerfil + ", vistas=" + vistas + '}';
    }

}
