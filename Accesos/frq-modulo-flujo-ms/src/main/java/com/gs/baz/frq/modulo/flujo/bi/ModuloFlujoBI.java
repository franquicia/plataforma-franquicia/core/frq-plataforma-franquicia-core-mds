/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modulo.flujo.bi;

import com.gs.baz.frq.modulo.flujo.dto.ModuloFlujoDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ModuloFlujoBI {

    public List<ModuloFlujoDTO> toTree(List<ModuloFlujoDTO> nodes) {
        Map<Integer, ModuloFlujoDTO> mapTmp = new HashMap<>();
        nodes.stream().forEach((current) -> {
            mapTmp.put(current.getIdFlujoModulo(), current);
        });
        nodes.stream().forEach((current) -> {
            Integer parentId = current.getIdParent();
            if (parentId != null) {
                ModuloFlujoDTO parent = mapTmp.get(parentId);
                if (parent != null) {
                    current.setParent(parent);
                    parent.addChild(current);
                    mapTmp.put(parentId, parent);
                    mapTmp.put(current.getIdFlujoModulo(), current);
                }
            }
        });
        mapTmp.forEach((key, value) -> {
            value.setParent(null);
            value.setIdFlujo(null);
            value.setIdModulo(null);
        });
        List<ModuloFlujoDTO> roots = new ArrayList<>();
        for (ModuloFlujoDTO node : mapTmp.values()) {
            if (node.getIdParent() == null) {
                roots.add(node);
                break;
            }
        }
        return roots;
    }

}
