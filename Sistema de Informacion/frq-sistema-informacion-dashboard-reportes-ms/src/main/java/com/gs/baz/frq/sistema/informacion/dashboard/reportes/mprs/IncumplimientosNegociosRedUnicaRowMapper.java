/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.IncumplimientoNegocioRedUnica;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class IncumplimientosNegociosRedUnicaRowMapper implements RowMapper<IncumplimientoNegocioRedUnica> {

    @Override
    public IncumplimientoNegocioRedUnica mapRow(ResultSet rs, int rowNum) throws SQLException {
        IncumplimientoNegocioRedUnica incumplimientoNegocioRedUnica = new IncumplimientoNegocioRedUnica();
        incumplimientoNegocioRedUnica.setIdNegocio(rs.getInt("FINEGOCIODISCID"));
        incumplimientoNegocioRedUnica.setIdPregunta(rs.getInt("FIPREGUNTA_ID"));
        incumplimientoNegocioRedUnica.setDescripcion(rs.getString("FCDESCRIPCION"));
        incumplimientoNegocioRedUnica.setPorcentaje(rs.getInt("PORCENTAJE"));
        incumplimientoNegocioRedUnica.setPuntosVenta(rs.getInt("TOTALPDVINCUMPLIO"));
        return incumplimientoNegocioRedUnica;
    }
}
