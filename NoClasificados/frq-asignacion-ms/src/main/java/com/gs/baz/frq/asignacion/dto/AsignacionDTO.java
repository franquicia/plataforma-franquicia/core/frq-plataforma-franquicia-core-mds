/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

public class AsignacionDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idProyecto")
    private Integer proyecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ceco")
    private String ceco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreProyecto")
    private String nombreProyecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio")
    private String negocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipoProyecto")
    private Integer tipoProyecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCargaInicial")
    private Integer idCargaInicial;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCargaFin")
    private Integer idCargaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "faseActual")
    private Integer faseActual;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status")
    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idAgrupa")
    private Integer idAgrupa;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "responsableAsigno")
    private Integer responsableAsigno;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreResponsable")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "obsAsignacion")
    private String obsAsignacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idFase")
    private Integer idFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ordenFase")
    private Integer ordenFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreFase")
    private String nombreFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Integer inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "version")
    private Integer version;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "banderaFase")
    private Boolean banderaFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numSemana")
    private Integer semana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "start")
    private String fechaTermino;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "bitacora")
    private Integer bitacora;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idNegocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    public AsignacionDTO() {
    }

    public Integer getProyecto() {
        return proyecto;
    }

    public void setProyecto(BigDecimal proyecto) {
        this.proyecto = (proyecto == null ? null : proyecto.intValue());
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public Integer getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(Integer tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public Integer getIdCargaInicial() {
        return idCargaInicial;
    }

    public void setIdCargaInicial(Integer idCargaInicial) {
        this.idCargaInicial = idCargaInicial;
    }

    public Integer getIdCargaFin() {
        return idCargaFin;
    }

    public void setIdCargaFin(Integer idCargaFin) {
        this.idCargaFin = idCargaFin;
    }

    public Integer getFaseActual() {
        return faseActual;
    }

    public void setFaseActual(Integer faseActual) {
        this.faseActual = faseActual;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdAgrupa() {
        return idAgrupa;
    }

    public void setIdAgrupa(Integer idAgrupa) {
        this.idAgrupa = idAgrupa;
    }

    public Integer getResponsableAsigno() {
        return responsableAsigno;
    }

    public void setResponsableAsigno(Integer responsableAsigno) {
        this.responsableAsigno = responsableAsigno;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getObsAsignacion() {
        return obsAsignacion;
    }

    public void setObsAsignacion(String obsAsignacion) {
        this.obsAsignacion = obsAsignacion;
    }

    public Integer getIdFase() {
        return idFase;
    }

    public void setIdFase(Integer idFase) {
        this.idFase = idFase;
    }

    public Integer getOrdenFase() {
        return ordenFase;
    }

    public void setOrdenFase(Integer ordenFase) {
        this.ordenFase = ordenFase;
    }

    public String getNombreFase() {
        return nombreFase;
    }

    public void setNombreFase(String nombreFase) {
        this.nombreFase = nombreFase;
    }

    public Integer getInserted() {
        return inserted;
    }

    public void setInserted(Integer inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Boolean getBanderaFase() {
        return banderaFase;
    }

    public void setBanderaFase(Boolean banderaFase) {
        this.banderaFase = banderaFase;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getSemana() {
        return semana;
    }

    public void setSemana(Integer semana) {
        this.semana = semana;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public Integer getBitacora() {
        return bitacora;
    }

    public void setBitacora(Integer bitacora) {
        this.bitacora = bitacora;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    @Override
    public String toString() {
        return "AsignacionDTO [proyecto=" + proyecto + ", ceco=" + ceco + ", nombreProyecto=" + nombreProyecto
                + ", negocio=" + negocio + ", tipoProyecto=" + tipoProyecto + ", idCargaInicial=" + idCargaInicial
                + ", idCargaFin=" + idCargaFin + ", faseActual=" + faseActual + ", status=" + status + ", idAgrupa="
                + idAgrupa + ", responsableAsigno=" + responsableAsigno + ", idUsuario=" + idUsuario
                + ", nombreUsuario=" + nombreUsuario + ", obsAsignacion=" + obsAsignacion + ", idFase=" + idFase
                + ", ordenFase=" + ordenFase + ", nombreFase=" + nombreFase + ", inserted=" + inserted + ", updated="
                + updated + ", deleted=" + deleted + ", periodo=" + periodo + ", version=" + version + ", banderaFase="
                + banderaFase + ", idCeco=" + idCeco + ", semana=" + semana + ", fechaTermino=" + fechaTermino
                + ", bitacora=" + bitacora + ", idNegocio=" + idNegocio + ", fecha=" + fecha + "]";
    }

    /*
 * @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_tipo_formato")
    private Integer idTipoFormato;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;
    
    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdTipoFormato() {
        return idTipoFormato;
    }

    public void setIdTipoFormato(BigDecimal idTipoFormato) {
        this.idTipoFormato = (idTipoFormato == null ? null : idTipoFormato.intValue());
    }

    @Override
    public String toString() {
        return "TipoFormatoDTO{" + "idTipoFormato=" + idTipoFormato + ", descripcion=" + descripcion + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }
     */
}
