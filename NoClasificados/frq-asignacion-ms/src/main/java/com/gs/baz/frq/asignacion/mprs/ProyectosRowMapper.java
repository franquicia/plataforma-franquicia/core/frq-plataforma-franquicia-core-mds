package com.gs.baz.frq.asignacion.mprs;

import com.gs.baz.frq.asignacion.dto.AsignacionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ProyectosRowMapper implements RowMapper<AsignacionDTO> {

    private AsignacionDTO status;

    @Override
    public AsignacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionDTO();

        status.setProyecto((BigDecimal) rs.getObject("FIID_PROYECTO"));
        status.setNombreProyecto(rs.getString("NOMBRE_PROYECTO"));
        status.setNegocio(rs.getString("NEGOCIO"));
        status.setTipoProyecto(rs.getInt("FIIDTIPOPROY"));
        status.setIdCargaInicial(rs.getInt("FIID_EDOCARGAINI"));
        status.setIdCargaFin(rs.getInt("FCEDOHALLAFIN"));
        status.setCeco(rs.getString("FCID_CECO"));
        status.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
        status.setPeriodo(rs.getString("FCPERIODO"));
        status.setIdNegocio(rs.getInt("CANAL"));

        return status;
    }
}
