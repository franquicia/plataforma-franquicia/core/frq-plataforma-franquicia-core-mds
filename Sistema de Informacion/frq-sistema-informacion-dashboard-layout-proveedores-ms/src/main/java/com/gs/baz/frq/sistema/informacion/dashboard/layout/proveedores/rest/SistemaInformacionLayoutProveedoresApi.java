/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionArchivosLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionCecosLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionFirmasLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionProveedoresLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.AltaArchivo;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.AltaArchivoParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.AltaComentario;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.AltaFirma;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.AltaTransaccion;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.ArchivosEliminar;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.BandejasProveedores;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentrosCostoProveedores;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.DashboardProveedores;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.FolioCargaParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.GeneraFolio;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.InsertaComentarioFirmas;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.InsertaFirmas;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.InsertaTransaccion;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.SinResultado;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.ValidaProveedor;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.ValidaProveedorRespuesta;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "visitas pendientes", value = "visitas pendientes", description = "Api para la gestión de visitas pendientes")
@RestController
@RequestMapping("/api-local/sistema-informacion/dashboard/disenos/v1/proveedores")
public class SistemaInformacionLayoutProveedoresApi {

    @Autowired
    private SistemaInformacionCecosLayoutDAOImpl sistemaInformacionCecosLayoutDAOImpl;
    
    @Autowired
    private SistemaInformacionProveedoresLayoutDAOImpl sistemaInformacionProveedoresLayoutDAOImpl;

        
    @Autowired
    private SistemaInformacionArchivosLayoutDAOImpl sistemaInformacionArchivosLayoutDAOImpl;
    
    @Autowired
    private SistemaInformacionFirmasLayoutDAOImpl sistemaInformacionFirmasLayoutDAOImpl;

//estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idProveedor
     * @param idEstatus
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idProveedor}/conteos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DashboardProveedores obtieneConteo(@ApiParam(name = "idProveedor", value = "Identificador del proveedores", example = "123456") @PathVariable("idProveedor") Integer idProveedor,
            @ApiParam(name = "idEstatus", value = "Identificador del proveedores", example = "123456") @RequestParam("idEstatus") String idEstatus) throws CustomException, DataNotFoundException {
       Integer estatus;
        if(idEstatus.equals("-1") || idEstatus.isEmpty()){
          estatus = null;
        }else {
            estatus = Integer.getInteger(idEstatus);
        }
        
        DashboardProveedores dashboardProveedores = new DashboardProveedores(sistemaInformacionCecosLayoutDAOImpl.selectDashboardProveedores(idProveedor, estatus));
        if (dashboardProveedores == null || dashboardProveedores.getConteoProveedores().isEmpty()) {
            throw new DataNotFoundException();
        }
        return dashboardProveedores;
    }

     /**
     *
     * @param idProveedor
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idProveedor}/bandejas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BandejasProveedores obtieneBandejas(@ApiParam(name = "idProveedor", value = "Identificador del proveedores", example = "123456") @PathVariable("idProveedor") Integer idProveedor) throws CustomException, DataNotFoundException {
        BandejasProveedores bandejasProveedores = new BandejasProveedores(sistemaInformacionCecosLayoutDAOImpl.selectBandejasProveedores(idProveedor));
        if (bandejasProveedores == null) {
            throw new DataNotFoundException();
        }
        return bandejasProveedores;
    }

    

     /**
     *
     * @param validaProveedor
     * @param idProveedor
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/valida-accesos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidaProveedorRespuesta validaProveedor(@ApiParam(name = "Datos Proveedor", value = "Datos de las credenciales del proveedor", required = true) @RequestBody ValidaProveedor validaProveedor ) throws CustomException, DataNotFoundException {

ValidaProveedorRespuesta respuesta = new ValidaProveedorRespuesta();
        respuesta.setAccesoExitoso(sistemaInformacionProveedoresLayoutDAOImpl.loginProveedor(validaProveedor));
      
         return respuesta;
    }

       /**
     *
     * @param idProveedor
     * @param idEstatus
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idProveedor}/centros-costos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CentrosCostoProveedores obtieneCecosProveedor(@ApiParam(name = "idProveedor", value = "Identificador del proveedores", example = "123456") @PathVariable("idProveedor") Integer idProveedor,
            @ApiParam(name = "idEstatus", value = "Identificador del proveedores", example = "123456") @RequestParam("idEstatus") String idEstatus) throws CustomException, DataNotFoundException {
         Integer estatus;
            if(idEstatus.equals("-1") || idEstatus.isEmpty()){
          estatus = null;
        }else {
            estatus = Integer.getInteger(idEstatus);
        }
        
        CentrosCostoProveedores centrosCostoProveedores = new CentrosCostoProveedores(sistemaInformacionCecosLayoutDAOImpl.selectCecosProveedores(idProveedor, estatus));
        if (centrosCostoProveedores == null || centrosCostoProveedores.getCentroCostoProveedores().isEmpty()) {
            throw new DataNotFoundException();
        }
        return centrosCostoProveedores;
    }
    
    
            /**
     *
     * @param centroCostos
     * @param idProveedor
     * @param centroCostosParam
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/folios", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public GeneraFolio generaFolio(@ApiParam(name = "centroCostos", value = "Centro de Costos", example = "920100", required = true) @RequestParam("centroCostos") Integer centroCostos,
             @ApiParam(name = "idProveedor", value = "Identificador del proveedor", example = "202622", required = true) @RequestParam("idProveedor") Integer idProveedor) throws DataNotInsertedException {
                   
        GeneraFolio banderaFolio = new GeneraFolio();
                banderaFolio = sistemaInformacionArchivosLayoutDAOImpl.generaFolio(centroCostos, idProveedor);
        return banderaFolio;
    }
    
           /**
     *
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/archivos/{idFolio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivosEliminar obtieneArchivosEliminar(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "123456") @PathVariable("idFolio") Integer idFolio) throws CustomException, DataNotFoundException {
        ArchivosEliminar archivosEliminar = new ArchivosEliminar(sistemaInformacionArchivosLayoutDAOImpl.obtieneArchivosEliminar(idFolio));
        if (archivosEliminar == null || archivosEliminar.getListaArchivos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return archivosEliminar;
    }
    
        /**
     *
     * 
     * @param idFolio
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grafica", notes = "Elimina un item de los graficas", nickname = "eliminaGrafica")
    @RequestMapping(value = "/archivos/{idFolio}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaArchivo(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "920100", required = true) @PathVariable("idFolio") Integer idFolio)  throws DataNotDeletedException {
        sistemaInformacionArchivosLayoutDAOImpl.eliminaArchivos(idFolio);
        return new SinResultado();
    }
    
    
             /**
     *
     * @param entityDTO
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/archivos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaArchivo altaDocumento(@ApiParam(name = "Datos Archivo", value = "Datos para insertar un archivo", required = true) @RequestBody AltaArchivoParametros entityDTO ) throws DataNotInsertedException {
                   
        AltaArchivo altaArchivo = new AltaArchivo();
                altaArchivo.setIdFolio(sistemaInformacionArchivosLayoutDAOImpl.insertRow(entityDTO));
        return altaArchivo;
    }

       /**
     *
     * @param idFolio
     * @param entityDTO
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar grafica", notes = "Actualiza un grafica", nickname = "actualizaGrafica")
    @RequestMapping(value = "/folios/{idFolio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFolioCarga(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "920100", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "", value = "", required = true) @RequestBody FolioCargaParametros entityDTO ) throws DataNotUpdatedException {
        entityDTO.setIdFolio(idFolio);
        sistemaInformacionProveedoresLayoutDAOImpl.actualizaFolioCarga(entityDTO);
        return new SinResultado();
    }
    
                 /**
     *
     * @param entityDTO
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/firmas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFirma insertaFirmas(@ApiParam(name = "Datos Firma", value = "Datos para insertar una firma", required = true) @RequestBody InsertaFirmas entityDTO ) throws DataNotInsertedException {
                   
        AltaFirma altaFirma = new AltaFirma();
                altaFirma.setIdFirmante(sistemaInformacionFirmasLayoutDAOImpl.insertaFirmas(entityDTO));
        return altaFirma;
    }
    
    
                    /**
     *
     * @param entityDTO
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/comentarios", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaComentario insertaComentarios(@ApiParam(name = "Datos Firma", value = "Datos para insertar una firma", required = true) @RequestBody InsertaComentarioFirmas entityDTO) throws DataNotInsertedException {
                   
        AltaComentario altaComentario = new AltaComentario();
                altaComentario.setIdComentario(sistemaInformacionFirmasLayoutDAOImpl.insertaComentarioFirmas(entityDTO));
        return altaComentario;
    }
    
    
        
                    /**
     *
     * @param entityDTO
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/transacciones", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaTransaccion insertaTransaccion(@ApiParam(name = "Datos Firma", value = "Datos para insertar una firma", required = true) @RequestBody InsertaTransaccion entityDTO) throws DataNotInsertedException {
                   
        AltaTransaccion altaTransaccion = new AltaTransaccion();
                altaTransaccion.setIdTransaccion(sistemaInformacionArchivosLayoutDAOImpl.insertTransaccion(entityDTO));
        return altaTransaccion;
    }
}
