package com.gs.baz.frq.punto.contacto.protocolo.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.punto.contacto.protocolo.dao.PuntoContactoProtocoloDAOImpl;
import com.gs.baz.read.propiedades.commons.Propiedades;
import com.gs.baz.read.propiedades.dao.ReadPropiedadesDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.punto.contacto.protocolo")
public class ConfigPuntoContactoProtocolo {

    private final Logger logger = LogManager.getLogger();

    public ConfigPuntoContactoProtocolo() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "franquiciaPuntoContactoProtocoloDAOImpl")
    public PuntoContactoProtocoloDAOImpl puntoContactoProtocoloDAOImpl() {
        return new PuntoContactoProtocoloDAOImpl();
    }

    @Bean(initMethod = "init")
    public ReadPropiedadesDAOImpl readPropiedadesDAOImpl() {
        return new ReadPropiedadesDAOImpl();
    }

    @Bean
    public Propiedades propiedades() {
        return new Propiedades();
    }

}
