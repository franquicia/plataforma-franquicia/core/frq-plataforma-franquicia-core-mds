/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.poblacion.dao.d009;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.poblacion.dto.PoblacionDTO;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author luisgomez
 */
public class PoblacionDAOImpl009 extends DefaultDAO {

    private DefaultJdbcCall jdbcExecute;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcExecute = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcExecute.withSchemaName(schema);
        jdbcExecute.withCatalogName("PAADMDASHBOARD");
        jdbcExecute.withProcedureName("SPMERGEDASHBOARD");
    }

    public PoblacionDTO executeStore(PoblacionDTO poblacionDTO) throws CustomException {
        PoblacionDTO salida = new PoblacionDTO();
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FECHAINI", poblacionDTO.getFechaInicio());
            mapSqlParameterSource.addValue("PA_FECHAFIN", poblacionDTO.getFechaFin());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", poblacionDTO.getIdProtocolo());
            Map<String, Object> out = jdbcExecute.execute(mapSqlParameterSource);
            salida.setEjecucion((BigDecimal) out.get("PA_NRESEJECUCION"));
            salida.setTabla((BigDecimal) out.get("PA_CONTABLA"));
            salida.setRegistro((BigDecimal) out.get("PA_CONTEREG"));
            return salida;
        } catch (Exception ex) {
            logger.info(ex);
            throw new CustomException(ex);
        }
    }
}
