/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Estructura de salida para cifrar algun valor", value = "CifraResponse")
public class CifraResponseDTO {

    @ApiModelProperty(notes = "Valor cifrado", example = "<Cadena cifrada en Base64>")
    @JsonProperty(value = "valor")
    private String valor;

    public CifraResponseDTO(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
