/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author kramireza
 */
public class SucursalFecha {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "Sucursal")
    private Integer sucursal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "Fecha")
    private String fecha;

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "SucursalFecha{" + "sucursal=" + sucursal + ", fecha=" + fecha + '}';
    }

}
