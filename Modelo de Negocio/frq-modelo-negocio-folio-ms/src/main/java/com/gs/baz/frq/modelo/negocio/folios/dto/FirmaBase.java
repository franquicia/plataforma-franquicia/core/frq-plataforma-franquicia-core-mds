/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class FirmaBase {

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del Folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Numero de Empleado de la firma", example = "331952")
    private Integer numeroEmpleado;

    @JsonProperty(value = "idJerarquia")
    @ApiModelProperty(notes = "Identificados de la jerarquia de la firma", example = "1")
    private Integer idJerarquia;

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1")
    private Integer idEstadoFirma;

    @JsonProperty(value = "eliminado")
    @ApiModelProperty(notes = "estatus de eliminacion de la firma", example = "0")
    private Integer eliminado;

    @JsonProperty(value = "correo")
    @ApiModelProperty(notes = "Correo firmante", example = "0")
    private String correo;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "estatus de eliminacion de la firma", example = "0")
    private Integer indice;

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdJerarquia() {
        return idJerarquia;
    }

    public void setIdJerarquia(Integer idJerarquia) {
        this.idJerarquia = idJerarquia;
    }

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    public Integer getEliminado() {
        return eliminado;
    }

    public void setEliminado(Integer eliminado) {
        this.eliminado = eliminado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

}
