/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.folio.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.documentos.sucursal.folio.dto.AltaFolio;
import com.gs.baz.frq.documentos.sucursal.folio.dto.Folio;
import com.gs.baz.frq.documentos.sucursal.folio.dto.ParametrosFolio;
import com.gs.baz.frq.documentos.sucursal.folio.mprs.FolioRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalFolioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;

    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PA_ARCHIVODOC";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_FOLIO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_FOLIO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_FOLIO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SP_SEL_FOLIO");
        jdbcSelect.returningResultSet("RCL_ARCH", new FolioRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_FOLIO");
        jdbcSelectAll.returningResultSet("RCL_ARCH", new FolioRowMapper());

    }

    public Folio selectRow(Long idCampoSensible) throws CustomException, DataNotFoundException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", idCampoSensible);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<Folio> data = (List<Folio>) out.get("RCL_ARCH");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            throw new DataNotFoundException();
        }
    }

    public List<Folio> selectAllRows() throws CustomException, DataNotFoundException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        List<Folio> data = (List<Folio>) out.get("RCL_ARCH");
        if (data != null & data.size() > 0) {
            return data;
        } else {
            throw new DataNotFoundException();
        }
    }

    public AltaFolio insertRow(ParametrosFolio entityDTO) throws DataNotInsertedException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCREADOR", entityDTO.getNumeroUsuario());
        mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getEstatus());

        Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);

        int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();

        switch (success) {
            case 0:
                throw new DataNotInsertedException();
            case -2:
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            default:
                BigDecimal dato = (BigDecimal) out.get("PA_FOLIO");
                return new AltaFolio(dato.intValue());

        }

    }

    public void updateRow(Integer folio, ParametrosFolio entityDTO) throws DataNotUpdatedException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", folio);
        mapSqlParameterSource.addValue("PA_IDCREADOR", entityDTO.getNumeroUsuario());
        mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getEstatus());
        mapSqlParameterSource.addValue("PA_ELIMINADO", entityDTO.getEliminadoLogico());

        Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
        int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
        if (success == 0) {
            throw new DataNotUpdatedException();
        } else if (success == -2) {
            throw new DataNotUpdatedException(DataNotUpdatedException.Cause.ROW_NOT_FOUND);
        }

    }

    public void deleteRow(Integer folio) throws DataNotDeletedException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", folio);
        Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
        int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
        if (success == 0) {
            throw new DataNotDeletedException();
        } else if (success == -2) {
            throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
        }

    }

}
