package com.gs.baz.modelos.franquicia.mprs;

import com.gs.baz.modelos.franquicia.dto.ModeloFranquiciaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ModelosFranquiciaRowMapper implements RowMapper<ModeloFranquiciaDTO> {

    private ModeloFranquiciaDTO franquiciaModelo;

    @Override
    public ModeloFranquiciaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        franquiciaModelo = new ModeloFranquiciaDTO();
        franquiciaModelo.setIdModelo((BigDecimal) rs.getObject("FIIDMODELO"));
        franquiciaModelo.setNombreModelo(rs.getString("FCNOMBREMODELO"));
        franquiciaModelo.setFechaCreacion(rs.getDate("FDFECHACREACION"));
        franquiciaModelo.setIdEmpleadoCrea(((BigDecimal) rs.getObject("FIIDEMCREA")));
        franquiciaModelo.setFechaModificacion(rs.getDate("FDFMODIFIC"));
        franquiciaModelo.setIdEmpleadoModifica(((BigDecimal) rs.getObject("FIIDEMMOD")));
        franquiciaModelo.setIdFranquicia(((BigDecimal) rs.getObject("FIIDFRANQ")));
        franquiciaModelo.setIdStatus(((BigDecimal) rs.getObject("FIIDSTATUS")));
        return franquiciaModelo;
    }
}
