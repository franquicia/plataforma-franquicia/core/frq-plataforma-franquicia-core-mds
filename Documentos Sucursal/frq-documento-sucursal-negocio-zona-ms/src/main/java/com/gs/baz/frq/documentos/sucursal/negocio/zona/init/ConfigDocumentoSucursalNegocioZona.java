package com.gs.baz.frq.documentos.sucursal.negocio.zona.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.baz.frq.documentos.sucursal.negocio.zona.dao.DocumentoSucursalNegocioZonasDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documentos.sucursal.negocio.zona")
public class ConfigDocumentoSucursalNegocioZona {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalNegocioZona() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalNegocioZonasDAOImpl documentoSucursalNegocioZonasDAOImpl() {
        return new DocumentoSucursalNegocioZonasDAOImpl();
    }

}
