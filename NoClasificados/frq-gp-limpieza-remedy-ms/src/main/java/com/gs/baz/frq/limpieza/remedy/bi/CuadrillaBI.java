package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.CuadrillaDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.CuadrillaDTO;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CuadrillaBI {

    @Autowired
    private CuadrillaDAOImpl cuadrillaDAO;
    private static final Logger LOGGER = LogManager.getLogger();

    public CuadrillaBI() {
        super();

    }

    public int inserta(CuadrillaDTO bean) {
        int respuesta = 0;

        try {
            respuesta = cuadrillaDAO.inserta(bean);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }

        return respuesta;
    }

    public boolean elimina(String idCuadrilla) {
        boolean respuesta = false;

        try {
            respuesta = cuadrillaDAO.elimina(idCuadrilla);
        } catch (Exception e) {
            LOGGER.info("No fue posible eliminar");

        }

        return respuesta;
    }

    public List<CuadrillaDTO> obtieneDatos(int idCuadrilla) {
        List<CuadrillaDTO> listafila = null;
        try {
            listafila = cuadrillaDAO.obtieneDatos(idCuadrilla);
        } catch (Exception e) {
            LOGGER.info(e);

        }

        return listafila;
    }

    public List<CuadrillaDTO> obtieneInfo() {
        List<CuadrillaDTO> listafila = null;
        try {
            listafila = cuadrillaDAO.obtieneInfo();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla cuadrilla", e);

        }

        return listafila;
    }

    public boolean actualiza(CuadrillaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = cuadrillaDAO.actualiza(bean);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible actualizar", e);

        }

        return respuesta;
    }

    public List<CuadrillaDTO> obtienepass(int idCuadrilla, int idProveedor, int zona) {
        List<CuadrillaDTO> listafila = null;
        try {
            listafila = cuadrillaDAO.obtienepass(idCuadrilla, idProveedor, zona);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener los datos", e);

        }

        return listafila;
    }

    public boolean cargapass() {
        boolean respuesta = false;
        try {
            respuesta = cuadrillaDAO.cargapass();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible Cargar Los Passwords de Las cuadrillas", e);

        }
        return respuesta;
    }

    public boolean eliminaCuadrillaProveedor(int cuadrilla, int proveedor, int zona) {

        boolean respuesta = false;

        try {

            respuesta = cuadrillaDAO.eliminaCuadrillaProveedor(cuadrilla, proveedor, zona);

        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible ejecutar el proceso de eliminar cuadrilla por proveedor ", e);
        }

        return respuesta;

    }

}
