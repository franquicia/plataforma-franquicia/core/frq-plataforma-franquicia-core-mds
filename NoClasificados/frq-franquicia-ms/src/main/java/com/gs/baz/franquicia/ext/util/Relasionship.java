/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.ext.util;

/**
 *
 * @author cescobarh
 */
public enum Relasionship {

    /**
     *
     */
    USUARIO("/modelo-franquicia/service/usuario/secure/get");

    private final String urlService;

    private Relasionship(String urlService) {
        this.urlService = urlService;
    }

    /**
     *
     * @param path
     * @return
     */
    public String url(String path) {
        return path + urlService;
    }

}
