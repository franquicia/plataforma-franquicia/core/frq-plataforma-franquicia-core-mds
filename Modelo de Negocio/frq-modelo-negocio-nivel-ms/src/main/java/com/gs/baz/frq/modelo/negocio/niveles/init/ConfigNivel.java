package com.gs.baz.frq.modelo.negocio.niveles.init;

import com.gs.baz.frq.modelo.negocio.niveles.dao.NivelDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.niveles")
public class ConfigNivel {

    private final Logger logger = LogManager.getLogger();

    public ConfigNivel() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public NivelDAOImpl nivelDAOImpl() {
        return new NivelDAOImpl();
    }

}
