/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosEstadoDocumento {

    @JsonProperty(value = "idEstadoDocumento")
    private transient Integer idEstadoDocumento;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion del estado documento", example = "Cuando un documento se puede editar", required = true)
    private String descripcion;

    @JsonProperty(value = "acronimo", required = true)
    @ApiModelProperty(notes = "Nombre del estatus del documento", example = "Por autorizar", required = true)
    private String nombre;

    public Integer getIdEstadoDocumento() {
        return idEstadoDocumento;
    }

    public void setIdEstadoDocumento(Integer idEstadoDocumento) {
        this.idEstadoDocumento = idEstadoDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
