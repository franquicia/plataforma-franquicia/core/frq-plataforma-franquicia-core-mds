/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.bi;

import com.gs.baz.frq.api.portal.cautivo.dao.IdentificadorDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dto.IdentificadorDTO;
import com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente.ECServicioIdentificador;
import com.gs.baz.frq.api.portal.cautivo.util.Util;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author MADA
 */
public class IdentificadorBI {

    public final Logger logger = LogManager.getLogger();

    @Autowired
    ECServicioIdentificador ecServicioIdentificador;

    @Autowired
    IdentificadorDAOImpl identificadorDAOImpl;

    public Boolean insertarClientesEC(String fechaInicio, String fechaFin, String noEco) throws CustomException {

        ecServicioIdentificador.init();
        // Parametros parametros = new Parametros();

        // List<FechasParametrosDTO> fechasIntervalos = parametros.getFechasParametros(fecha);
        final long startInsercion = System.currentTimeMillis();

        // for (int index = 0; index < fechasIntervalos.size(); index++) {
        // String fechaInicio = fechasIntervalos.get(index).getFechaInicio();
        //String fechaFin = fechasIntervalos.get(index).getFechaFin();
        //  int paginas = obtenerNumeroPaginas(tipo, Util.convertCTSDateToUTCDate(fechaInicio), Util.convertCTSDateToUTCDate(fechaFin));
        // logger.debug("Paginas = " + paginas + " & " + " Fecha Inicio = " + fechaInicio + " & " + " FechaFin = " + fechaFin);
        //Como el servicio esta segmentado para el consumo, se hace la petición por cada página
        //for (int i = 0; i <= paginas; i++) {
        final long startConsumo = System.currentTimeMillis();
        logger.debug("Consumo de Servicio ecServicioClientes.getListaClientes ? idPropietario = " + "&" + "page = " + "&" + "pageSize  = " + " & paginas = " + " & fechaInicio = " + fechaInicio + " & fechaFin = " + fechaFin, noEco);
        List<IdentificadorDTO> clientes = ecServicioIdentificador.getListaClientes(Util.convertCTSDateToUTCDate(fechaInicio), Util.convertCTSDateToUTCDate(fechaFin), noEco);

        if (clientes != null) {
            for (IdentificadorDTO cliente : clientes) {
                if (cliente.getMac() != null && !"".equals(cliente.getMac())) {
                    cliente.setMac(cliente.getMac().replace(":", ""));
                    identificadorDAOImpl.insertRow((cliente));
                }
            }
        }
        logger.debug("Insercion page = " + (System.currentTimeMillis() - startConsumo));
        // }
        //}

        logger.debug("Insercion completa ===== ", (System.currentTimeMillis() - startInsercion));

        return true;
    }
}
