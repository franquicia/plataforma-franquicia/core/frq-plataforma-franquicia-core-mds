/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dao;

import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import com.gs.baz.reporte.medicion.dto.ProtocoloDisciplinaDTO;
import com.gs.baz.reporte.medicion.dto.mprs.ProtocoloDisciplinaRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDisciplinaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectProtocoloDisciplina;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectProtocoloDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectProtocoloDisciplina.withSchemaName(schema);
        jdbcSelectProtocoloDisciplina.withCatalogName("PAREPORTEMEDEXT");
        jdbcSelectProtocoloDisciplina.withProcedureName("SPGETROTCXDISC");
        jdbcSelectProtocoloDisciplina.returningResultSet("PA_PROTOCOLOS", new ProtocoloDisciplinaRowMapper());

    }

    public List<ProtocoloDisciplinaDTO> selectProtocoloDisciplina(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_DISCID", filtersDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGDISCID", filtersDTO.getIdNegocio());
        Map<String, Object> out = jdbcSelectProtocoloDisciplina.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<ProtocoloDisciplinaDTO>) out.get("PA_PROTOCOLOS");

        } else {
            return null;
        }

    }

}
