package com.gs.baz.modelos.franquicia.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.modelos.franquicia.dao.util.GenericProtocoloDisciplinaDAO;
import com.gs.baz.modelos.franquicia.dto.ProtocoloDisciplinaDTO;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDisciplinaDAOImpl extends DefaultDAO implements GenericProtocoloDisciplinaDAO<ProtocoloDisciplinaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINPROTSDISC");
        jdbcInsert.withProcedureName("SP_INS_PROTSDISC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINPROTSDISC");
        jdbcDelete.withProcedureName("SP_DEL_PROTSDISC");
    }

    @Override
    public ProtocoloDisciplinaDTO insertRow(ProtocoloDisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDDISNEG", entityDTO.getIdDisciplinaNegocio());
            mapSqlParameterSource.addValue("PA_FIIDPROT", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIIDESTATUS", entityDTO.getStatus());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdProtocoloDisciplina((BigDecimal) out.get("PA_FIIPROTDIS"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Protocolos Disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Protocolos Disciplina"), ex);
        }
    }

    @Override
    public ProtocoloDisciplinaDTO deleteRow(ProtocoloDisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIPROTDIS", entityDTO.getIdProtocoloDisciplina());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Protocolos Disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Protocolos Disciplina"), ex);
        }
    }

}
