/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.formatos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de formato", value = "FormatoBase")
public class FormatoBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del formato", example = "EKT")
    private String descripcion;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Id del negocio", example = "1")
    private Integer idNegocio;

    @JsonProperty(value = "nombreNegocio")
    @ApiModelProperty(notes = "Descripcion nombre del nombre Negocio", example = "EKT")
    private String nombreNegocio;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
