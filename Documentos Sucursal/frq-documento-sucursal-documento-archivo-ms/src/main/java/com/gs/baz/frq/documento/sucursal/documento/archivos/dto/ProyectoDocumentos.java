/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de proyectoDocumento", value = "proyectoDocumentos")
public class ProyectoDocumentos {

    @JsonProperty(value = "proyectoDocumentos")
    @ApiModelProperty(notes = "proyectoDocumentos")
    private List<ProyectoDocumento> proyectoDocumentos;

    public ProyectoDocumentos(List<ProyectoDocumento> proyectoDocumentos) {
        this.proyectoDocumentos = proyectoDocumentos;
    }

    public List<ProyectoDocumento> getProyectoDocumentos() {
        return proyectoDocumentos;
    }

    public void setProyectoDocumentos(List<ProyectoDocumento> proyectoDocumentos) {
        this.proyectoDocumentos = proyectoDocumentos;
    }

    @Override
    public String toString() {
        return "ProyectoDocumentos{" + "proyectoDocumentos=" + proyectoDocumentos + '}';
    }

}
