/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.ResponsableIncumplimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ResponsablesIncumplimientoRowMapper implements RowMapper<ResponsableIncumplimiento> {

    @Override
    public ResponsableIncumplimiento mapRow(ResultSet rs, int rowNum) throws SQLException {
        ResponsableIncumplimiento responsableIncumplimiento = new ResponsableIncumplimiento();
        responsableIncumplimiento.setDescripcion(rs.getString("FCRESPONSABLE"));
        return responsableIncumplimiento;
    }
}
