/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.categoria.rest;

import com.gs.baz.categoria.dao.CategoriaDAOImpl;
import com.gs.baz.categoria.dto.CategoriaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/categoria")
public class ServiceCategoria {

    @Autowired
    private CategoriaDAOImpl ejeDAOImpl;

    final VersionBI versionBI = new VersionBI();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoriaDTO> getCategoria() throws CustomException {
        return ejeDAOImpl.selectRows();
    }

    /**
     *
     * @param id_categoria
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_categoria}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoriaDTO getCategocia(@PathVariable("id_categoria") Long id_categoria) throws CustomException {
        return ejeDAOImpl.selectRow(id_categoria);
    }

    /**
     *
     * @param id_negocio
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/negocio/{id_negocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoriaDTO> getCategociaPorNegocio(@PathVariable("id_negocio") Long id_negocio) throws CustomException {
        return ejeDAOImpl.selectRowsPorNegocio(id_negocio);
    }

    /**
     *
     * @param categoria
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoriaDTO postCategoria(@RequestBody CategoriaDTO categoria) throws CustomException {
        if (categoria.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (categoria.getIdStatus() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_status"));
        }
        if (categoria.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio"));
        }

        return ejeDAOImpl.cudRow(categoria);
    }

    /**
     *
     * @param categoria
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoriaDTO putCategoria(@RequestBody CategoriaDTO categoria) throws CustomException {
        if (categoria.getIdCategoria() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_categoria"));
        }
        if (categoria.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (categoria.getIdStatus() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_status"));
        }

        if (categoria.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio"));
        }
        return ejeDAOImpl.updateRow(categoria);
    }

    /**
     *
     * @param idCategoria
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete/negocio/{id_categoria}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean deleteCategoria(@PathVariable("id_categoria") Long idCategoria) throws CustomException {
        if (idCategoria == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_categoria"));
        }
        return ejeDAOImpl.deleteRow(idCategoria);

    }
}
