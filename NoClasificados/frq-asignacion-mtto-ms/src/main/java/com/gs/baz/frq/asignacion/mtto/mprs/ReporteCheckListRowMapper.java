package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteChecklistMttoDTO;

/**
 *
 * @author cescobarh
 */
public class ReporteCheckListRowMapper implements RowMapper<ReporteChecklistMttoDTO> {

    private ReporteChecklistMttoDTO status;

    @Override
    public ReporteChecklistMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteChecklistMttoDTO();

        status.setIdCheckInn(rs.getInt("FIID_ID"));
        status.setIdUsuario(rs.getString("FCID_USUARIO"));
        //status.setNombreUsuario(rs.getString("NOMBRE_USU"));
        status.setIdCeco(rs.getInt("FCID_CECO"));
        status.setHoraEntrada(rs.getString("FECHA_ENT_CHECK"));
        status.setHoraSalida(rs.getString("FECHA_SAL_CHECK"));
        status.setHorasTrabajadas(rs.getString("HORAS_TRABAJADAS"));
        status.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        status.setNombreChecklist(rs.getString("NOMBRE_CHECK"));

        return status;
    }
}
