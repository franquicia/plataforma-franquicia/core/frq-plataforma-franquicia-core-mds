/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de archivo", value = "IdentificadorAltaArchivo")
public class AltaArchivo {


    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -2)
    private Integer idFolio;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    @Override
    public String toString() {
        return "AltaArchivo{" + "idFolio=" + idFolio + '}';
    }
    
}
