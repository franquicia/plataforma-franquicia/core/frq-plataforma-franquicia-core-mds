/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.responses;

import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class ErrorClienteDetalle implements RespuestaAPI {

    @ApiModelProperty(notes = "Mensaje de salida", example = "El x-idAcceso está expirado o no es válido, favor de solicitar uno nuevo.")
    private String mensaje;

    public ErrorClienteDetalle(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ErrorDetalle{" + "mensaje=" + mensaje + '}';
    }

}
