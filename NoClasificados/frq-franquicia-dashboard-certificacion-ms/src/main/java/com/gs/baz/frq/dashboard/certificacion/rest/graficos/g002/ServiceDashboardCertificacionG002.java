/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g002;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g002.DashboardCertificacionG002DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g002.DashboardCertificacionG002DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g002")
public class ServiceDashboardCertificacionG002 {

    @Autowired
    private DashboardCertificacionG002DAOImpl dashboardCertificacionG002DAOImpl;

    @RequestMapping(value = "secure/grafico/get/barras/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG002DTO> getDatoOdometroG002(
            @RequestParam(value = "idGerente", required = false) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio,
            @RequestParam(value = "semanasAtras", required = true) Long semanasAtras,
            @RequestParam(value = "fechaActual", required = true) String fechaActual) throws CustomException {

        if (semanasAtras == null || semanasAtras.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("semanasAtras"));
        }
        if (fechaActual == null || fechaActual.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaActual"));
        }

        return dashboardCertificacionG002DAOImpl.selectRowDashboardCertificacionG002(idGerente, idNegocio, semanasAtras, fechaActual);
    }
}
