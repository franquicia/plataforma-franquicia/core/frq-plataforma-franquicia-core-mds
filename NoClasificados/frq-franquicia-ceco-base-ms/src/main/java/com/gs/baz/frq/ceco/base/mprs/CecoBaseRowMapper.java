package com.gs.baz.frq.ceco.base.mprs;

import com.gs.baz.frq.ceco.base.dto.CecoBaseDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CecoBaseRowMapper implements RowMapper<CecoBaseDTO> {

    private CecoBaseDTO vistaPuntoContactoDTO;

    @Override
    public CecoBaseDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPuntoContactoDTO = new CecoBaseDTO();
        vistaPuntoContactoDTO.setIdCecoBase(((BigDecimal) rs.getObject("FICECOBASE_ID")));
        vistaPuntoContactoDTO.setIdPais(((BigDecimal) rs.getObject("FIID_PAIS")));
        vistaPuntoContactoDTO.setIdCeco(rs.getString("FCID_CECO"));
        vistaPuntoContactoDTO.setDescripcion(rs.getString("FCNOMBRE"));
        vistaPuntoContactoDTO.setIdTerritorio(rs.getString("FCID_TERRITORIO"));
        vistaPuntoContactoDTO.setTerritorio(rs.getString("FCTERRITORIO"));
        vistaPuntoContactoDTO.setIdZona(rs.getString("FCID_ZONA"));
        vistaPuntoContactoDTO.setZona(rs.getString("FCZONA"));
        vistaPuntoContactoDTO.setIdRegion(rs.getString("FCID_REGION"));
        vistaPuntoContactoDTO.setRegion(rs.getString("FCREGION"));
        vistaPuntoContactoDTO.setCanal(rs.getString("FCCANAL"));
        vistaPuntoContactoDTO.setFechaCreacion(rs.getString("FDCREACION"));
        vistaPuntoContactoDTO.setRegistroActivo(((BigDecimal) rs.getObject("FIREG_ACTIVO")));
        return vistaPuntoContactoDTO;
    }
}
