/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.respuesta.visita.mprs;

import com.gs.baz.frq.respuesta.visita.dto.RespuestaVisitaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author luisgomez
 */
public class DetalleRowMapper implements RowMapper<RespuestaVisitaDTO> {

    private RespuestaVisitaDTO vistaPuntoContactoDTO;

    @Override
    public RespuestaVisitaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPuntoContactoDTO = new RespuestaVisitaDTO();
        vistaPuntoContactoDTO.setConteo((BigDecimal) rs.getObject("FICONTEO"));
        vistaPuntoContactoDTO.setIdProtocolo((BigDecimal) rs.getObject("FIID_PROTOCOLO"));
        vistaPuntoContactoDTO.setIdNegocioDisciplina(((BigDecimal) rs.getObject("FINEGOCIODISCID")));

        return vistaPuntoContactoDTO;
    }
}
