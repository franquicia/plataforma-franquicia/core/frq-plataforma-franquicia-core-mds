/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cescobarh
 */
public class ParamentrosArchivoDTO {

    private String destino;
    
    private MultipartFile file;

    public ParamentrosArchivoDTO(String destino, MultipartFile file) {
        this.destino = destino;
        this.file = file;
    }

    public ParamentrosArchivoDTO() {
    }   

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "ParamentrosArchivoDTO{" + "destino=" + destino + ", file=" + file + '}';
    }

}
