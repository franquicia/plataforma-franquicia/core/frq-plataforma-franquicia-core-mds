/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ddl.oracle.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Oracle {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "properties")
    private Properties properties;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "register_parameters")
    private List<Parameter> registerParameters;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "plsql")
    private String plSQL;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public List<Parameter> getRegisterParameters() {
        return registerParameters;
    }

    public void setRegisterParameters(List<Parameter> registerParameters) {
        this.registerParameters = registerParameters;
    }

    public void addParameter(Parameter parameter) {
        if (this.registerParameters == null) {
            this.registerParameters = new ArrayList<>();
        }
        this.registerParameters.add(parameter);
    }

    public String getPlSQL() {
        return plSQL;
    }

    public void setPlSQL(String plSQL) {
        this.plSQL = plSQL;
    }

    @Override
    public String toString() {
        return "AnonymusPLSQL{" + "properties=" + properties + ", registerParameters=" + registerParameters + ", plSQL=" + plSQL + '}';
    }

}
