/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.rutas.dao.RutaDAOImpl;
import com.gs.baz.frq.modelo.negocio.rutas.dto.AltaRuta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.ParametrosRuta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaBase;
import com.gs.baz.frq.modelo.negocio.rutas.dto.Rutas;
import com.gs.baz.frq.modelo.negocio.rutas.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "rutas", value = "rutas", description = "Gestión de rutas")
@RestController
@RequestMapping("/api-local/modelo-negocio/folio-rutas/v1")
public class RutaApi {

    @Autowired
    private RutaDAOImpl rutaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutas por folio", notes = "Obtiene rutas por folio", nickname = "obtieneRutaFolio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/rutas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rutas obtieneRutaFolio(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        Rutas rutas = new Rutas(rutaDAOImpl.selectFolio(idFolio));
        if (rutas.getRutas() != null && rutas.getRutas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutas;
    }

    /**
     *
     * @param idFolio
     * @param idRuta
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ruta por folio", notes = "Obtiene ruta por folio", nickname = "obtieneFolioRuta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RutaBase obtieneFolioRuta(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Long idRuta) throws CustomException, DataNotFoundException {
        RutaBase rutaBase = rutaDAOImpl.selectFolioRuta(idRuta, idFolio);
        if (rutaBase == null) {
            throw new DataNotFoundException();
        }
        return rutaBase;
    }

    /**
     *
     * @param idFolio
     * @param parametrosRuta
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear ruta", notes = "Agrega un ruta", nickname = "creaRuta")
    @RequestMapping(value = "/{idFolio}/rutas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaRuta creaRuta(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "ParametrosRuta", value = "Paramentros para el alta del ruta", required = true) @RequestBody ParametrosRuta parametrosRuta) throws DataNotInsertedException {
        parametrosRuta.setIdFolio(idFolio.intValue());
        return rutaDAOImpl.insertRow(parametrosRuta);
    }

    /**
     *
     * @param idFolio
     * @param parametrosRuta
     * @param idRuta
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar ruta", notes = "Actualiza un ruta", nickname = "actualizaRuta")
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaRuta(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio, @ApiParam(name = "ParametrosRuta", value = "Paramentros para la actualización del ruta", required = true) @RequestBody ParametrosRuta parametrosRuta, @ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Integer idRuta) throws DataNotUpdatedException {
        parametrosRuta.setIdRuta(idRuta);
        parametrosRuta.setIdFolio(idFolio.intValue());
        rutaDAOImpl.updateRow(parametrosRuta);
        return new SinResultado();
    }

    /**
     *
     * @param idRuta
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar ruta", notes = "Elimina un item de las rutas", nickname = "eliminaRuta")
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaRuta(@ApiParam(name = "idRuta", value = "Identificador de la ruta", example = "1", required = true) @PathVariable("idRuta") Integer idRuta) throws DataNotDeletedException {
        rutaDAOImpl.deleteRow(idRuta);
        return new SinResultado();
    }

}
