package com.gs.baz.frq.dashboard.mprs.graficos.g009;

import com.gs.baz.frq.dashboard.dto.graficos.g009.CumplimientoCalificacionDisciplinaPuntoContactoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoCalificacionDisciplinaPuntoContactoRowMapper implements RowMapper<CumplimientoCalificacionDisciplinaPuntoContactoDTO> {

    private CumplimientoCalificacionDisciplinaPuntoContactoDTO cumplimientoCalificacionDisciplinaPuntoContactoDTO;

    @Override
    public CumplimientoCalificacionDisciplinaPuntoContactoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoCalificacionDisciplinaPuntoContactoDTO = new CumplimientoCalificacionDisciplinaPuntoContactoDTO();
        cumplimientoCalificacionDisciplinaPuntoContactoDTO.setIdCeco(rs.getString("CECO"));
        cumplimientoCalificacionDisciplinaPuntoContactoDTO.setDescripcion(rs.getString("DESCRIPCION"));
        cumplimientoCalificacionDisciplinaPuntoContactoDTO.setCalificacion((BigDecimal) rs.getObject("CALIFICACION"));
        cumplimientoCalificacionDisciplinaPuntoContactoDTO.setIdRango((BigDecimal) rs.getObject("RANGO"));
        return cumplimientoCalificacionDisciplinaPuntoContactoDTO;
    }
}
