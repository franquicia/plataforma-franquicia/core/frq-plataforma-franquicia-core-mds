package com.gs.baz.frq.dashboard.mprs.graficos.g001;

import com.gs.baz.frq.dashboard.dto.graficos.g001.CalificacionFinalDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionFinalDisciplinaPuntoContactoRowMapper implements RowMapper<CalificacionFinalDisciplinaDTO> {

    private CalificacionFinalDisciplinaDTO calificacionFinalDisciplinaDTO;

    @Override
    public CalificacionFinalDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        calificacionFinalDisciplinaDTO = new CalificacionFinalDisciplinaDTO();
        calificacionFinalDisciplinaDTO.setIdCeco(new BigDecimal(rs.getString("FCID_CECO")));
        calificacionFinalDisciplinaDTO.setCalificacionFinal((BigDecimal) rs.getObject("CALPUCON"));
        return calificacionFinalDisciplinaDTO;
    }
}
