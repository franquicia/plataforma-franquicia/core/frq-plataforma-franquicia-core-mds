/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author cescobarh
 */
public class Contenido {

    @JsonProperty(value = "cabezeras")
    @Valid
    private List<Cabezera> cabezeras;

    @JsonProperty(value = "registros")
    @NotEmpty
    private List<ObjectNode> registros;

    public Contenido() {
    }

    public List<Cabezera> getCabezeras() {
        return cabezeras;
    }

    public void setCabezeras(List<Cabezera> cabezeras) {
        this.cabezeras = cabezeras;
    }

    public List<ObjectNode> getRegistros() {
        return registros;
    }

    public void setRegistros(List<ObjectNode> registros) {
        this.registros = registros;
    }

    public void addCabezera(Cabezera cabezera) {
        if (cabezeras == null) {
            cabezeras = new ArrayList<>();
        }
        cabezeras.add(cabezera);
    }

    public Integer getIndexCabezeraByValue(String value) {
        Cabezera encabezado = cabezeras.stream().filter(item -> item.getValor().equals(value)).findFirst().orElse(null);
        if (encabezado != null) {
            return encabezado.getIndex();
        } else {
            return null;
        }
    }

    public void addRegistro(ObjectNode registro) {
        if (registros == null) {
            registros = new ArrayList<>();
        }
        registros.add(registro);
    }

    @Override
    public String toString() {
        return "Contenido{" + "cabezeras=" + cabezeras + ", registros=" + registros + '}';
    }

}
