/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Alta de nuevo Incidente", value = "RespuestaNuevoIncidente")
public class RespuestaNuevoIncidenteDTO {

    @JsonProperty(value = "idIncidente")
    @ApiModelProperty(notes = "Nuevo incidente generado", example = "13567082", position = -1)
    private Integer idIncidente;

    public RespuestaNuevoIncidenteDTO() {
        super();

    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

}
