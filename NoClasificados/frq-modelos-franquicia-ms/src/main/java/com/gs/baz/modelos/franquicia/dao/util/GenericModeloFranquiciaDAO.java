/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.modelos.franquicia.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericModeloFranquiciaDAO<dto> {

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public dto selectRow(Long entityID) throws CustomException;

    /**
     *
     * @param name
     * @param idFranquicia
     * @return
     * @throws CustomException
     */
    public List<dto> selectRowsByName(String name, Long idFranquicia) throws CustomException;

    /**
     *
     * @param franquiciaID
     * @return
     * @throws CustomException
     */
    public List<dto> selectRows(Long franquiciaID) throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<dto> selectRows() throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRowModelo(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto cudModelo(dto entityDTO) throws CustomException;

}
