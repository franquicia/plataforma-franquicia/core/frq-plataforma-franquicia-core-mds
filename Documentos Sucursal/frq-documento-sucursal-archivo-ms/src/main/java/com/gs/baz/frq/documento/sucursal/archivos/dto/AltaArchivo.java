/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del archivo", value = "AltaArchivo")
public class AltaArchivo {

    @JsonProperty(value = "uid")
    @ApiModelProperty(notes = "identificador unico del archivo", example = "1")
    private Long uid;

    public AltaArchivo(Long uid) {
        this.uid = uid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

}
