/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del usuario", value = "Usuario")
public class Usuario {

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Numero de empleado", example = "1", position = -1)
    private Integer numeroEmpleado;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del empleado", example = "1")
    private String nombre;

    @JsonProperty(value = "totalDocumentos")
    @ApiModelProperty(notes = "Total de documentos", example = "1")
    private Integer totalDocumentos;

    @JsonProperty(value = "totalGrupos")
    @ApiModelProperty(notes = "Total de grupos", example = "1")
    private Integer totalGrupos;

    public Usuario() {
    }

    public Usuario(Integer numeroEmpleado, String nombre) {
        this.numeroEmpleado = numeroEmpleado;
        this.nombre = nombre;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTotalDocumentos() {
        return totalDocumentos;
    }

    public void setTotalDocumentos(Integer totalDocumentos) {
        this.totalDocumentos = totalDocumentos;
    }

    public Integer getTotalGrupos() {
        return totalGrupos;
    }

    public void setTotalGrupos(Integer totalGrupos) {
        this.totalGrupos = totalGrupos;
    }

    @Override
    public String toString() {
        return "Usuario{" + "numeroEmpleado=" + numeroEmpleado + ", nombre=" + nombre + ", totalDocumentos=" + totalDocumentos + ", totalGrupos=" + totalGrupos + '}';
    }

}
