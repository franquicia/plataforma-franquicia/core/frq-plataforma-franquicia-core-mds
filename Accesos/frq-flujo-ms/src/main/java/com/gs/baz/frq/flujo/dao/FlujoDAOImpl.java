/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.flujo.dao;

/**
 *
 * @author jfernandor
 */
import com.gs.baz.frq.flujo.dto.FlujoDTO;
import com.gs.baz.frq.flujo.mprs.FlujoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class FlujoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAFLUJO");
        jdbcInsert.withProcedureName("SPINSAFLUJO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAFLUJO");
        jdbcSelect.withProcedureName("SPGETAFLUJO");
        jdbcSelect.returningResultSet("PA_CDATOS", new FlujoRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAFLUJO");
        jdbcUpdate.withProcedureName("SPACTAFLUJO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAFLUJO");
        jdbcDelete.withProcedureName("SPDELAFLUJO");
    }

    public FlujoDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FlujoDTO> data = (List<FlujoDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<FlujoDTO> selectRows() throws CustomException {
        List<FlujoDTO> lista;
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_FLUJO", null);
        Map<String, Object> out = jdbcSelect.execute(in);
        lista = (List<FlujoDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public FlujoDTO insertRow(FlujoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FCTIPOAPP", entityDTO.getTipoApp());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setIdFlujo(((BigDecimal) out.get("PA_NRESEJECUCION")));
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Flujo"), ex);
        }
    }

    public FlujoDTO updateRow(FlujoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FCTIPOAPP", entityDTO.getTipoApp());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Flujo"), ex);
        }
    }

    public FlujoDTO deleteRow(FlujoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Flujo"), ex);
        }
    }

}
