package com.gs.baz.frq.dashboard.mprs.util;

import com.gs.baz.frq.dashboard.dto.util.PeriodoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PeriodoRowMapper implements RowMapper<PeriodoDTO> {

    private PeriodoDTO periodoDTO;

    @Override
    public PeriodoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        periodoDTO = new PeriodoDTO();
        periodoDTO.setFecha(rs.getString("FDFECHA"));
        periodoDTO.setDiaSemana((BigDecimal) rs.getObject("FINUMDIASEM"));
        periodoDTO.setDiaMes((BigDecimal) rs.getObject("FINUMDIA"));
        periodoDTO.setSemana((BigDecimal) rs.getObject("FISEMANA"));
        periodoDTO.setMes((BigDecimal) rs.getObject("FIMES"));
        periodoDTO.setBimestre((BigDecimal) rs.getObject("FIBIMESTRE"));
        periodoDTO.setTrimestre((BigDecimal) rs.getObject("FITRIMESTRE"));
        periodoDTO.setFestivo((BigDecimal) rs.getObject("FIDIAFESTIVO"));
        periodoDTO.setAnio((BigDecimal) rs.getObject("FIANIO"));
        return periodoDTO;
    }
}
