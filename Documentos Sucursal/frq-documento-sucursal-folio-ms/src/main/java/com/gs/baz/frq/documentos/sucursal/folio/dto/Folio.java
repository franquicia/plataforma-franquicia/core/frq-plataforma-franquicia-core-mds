/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author fjaramillo
 */
@ApiModel(description = "Informacion de un folio", value = "Folio")
public class Folio {

    @JsonProperty(value = "folio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -3)
    private Integer id;

    @JsonProperty(value = "numeroEmpleadoCreador")
    @ApiModelProperty(notes = "Identificador del ceco", example = "920100", position = -2)
    private String idCreador;

    @JsonProperty(value = "estatus")
    @ApiModelProperty(notes = "Estado del folio", example = "2", position = -1)
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdCreador() {
        return idCreador;
    }

    public void setIdCreador(String idCreador) {
        this.idCreador = idCreador;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
