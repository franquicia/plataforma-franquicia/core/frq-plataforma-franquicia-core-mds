/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.niveles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosNivel {

    @JsonProperty(value = "idNivel")
    private transient Integer idNivel;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion del nivel", example = "Nivel 1", required = true)
    private String descripcion;

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ParametrosNivel{" + "descripcion=" + descripcion + '}';
    }

}
