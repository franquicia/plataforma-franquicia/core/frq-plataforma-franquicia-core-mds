/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class Respuesta7sDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_visita")
    private Integer idVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_pregunta")
    private Integer idPregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "respuesta")
    private String respuesta;

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(BigDecimal idPregunta) {
        this.idPregunta = (idPregunta == null ? null : idPregunta.intValue());
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(BigDecimal idVisita) {
        this.idVisita = (idVisita == null ? null : idVisita.intValue());
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public String toString() {
        return "Respuesta7sDTO{" + "idVisita=" + idVisita + ", idPregunta=" + idPregunta + ", respuesta=" + respuesta + '}';
    }

}
