package com.gs.baz.frq.asignacion.certificacion.calidad.mprs;

import com.gs.baz.frq.asignacion.certificacion.calidad.dto.AsignacionCertiCalidadDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class AsignacionCertiCalidadRowMapper implements RowMapper<AsignacionCertiCalidadDTO> {

    private AsignacionCertiCalidadDTO asignacionCertiCalidad;

    @Override
    public AsignacionCertiCalidadDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        asignacionCertiCalidad = new AsignacionCertiCalidadDTO();
        asignacionCertiCalidad.setIdUsuario(((BigDecimal) rs.getObject("FIID_USUARIO")));
        asignacionCertiCalidad.setIdPerfil(((BigDecimal) rs.getObject("FIPERFIL_ID")));
        asignacionCertiCalidad.setIdCeco(rs.getString("FCID_CECO"));
        asignacionCertiCalidad.setIdGerente(((BigDecimal) rs.getObject("FIGERENTEID")));
        asignacionCertiCalidad.setActivo(((BigDecimal) rs.getObject("FIACTIVO")));
        asignacionCertiCalidad.setInicioAsignacion(rs.getString("FDINICIO_ASIGN"));
        asignacionCertiCalidad.setFinAsignacion(rs.getString("FDFIN_ASIGN"));
        asignacionCertiCalidad.setIdNegocio(((BigDecimal) rs.getObject("FINEGOCIO_ID")));
        return asignacionCertiCalidad;
    }

    public class AsignacionCertiCalidadGeoRowMapper implements RowMapper<AsignacionCertiCalidadDTO> {

        private AsignacionCertiCalidadDTO asignacionCertiGeoCalidad;

        @Override
        public AsignacionCertiCalidadDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            asignacionCertiGeoCalidad = new AsignacionCertiCalidadDTO();
            asignacionCertiGeoCalidad.setIdCeco(rs.getString("FCID_CECO"));
            asignacionCertiGeoCalidad.setNombre(rs.getString("FCNOMBRE"));
            asignacionCertiGeoCalidad.setTerritorio(rs.getString("FCTERRITORIO"));
            asignacionCertiGeoCalidad.setZona(rs.getString("FCZONA"));
            asignacionCertiGeoCalidad.setActivo(((BigDecimal) rs.getObject("FIACTIVO")));
            return asignacionCertiGeoCalidad;
        }
    }
}
