/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.Territorio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class TerritoriosLayoutRowMapper implements RowMapper<Territorio> {

    @Override
    public Territorio mapRow(ResultSet rs, int rowNum) throws SQLException {
        Territorio territorio = new Territorio();
        territorio.setIdTerritorio(rs.getInt("FCID_TERRITORIO"));
        territorio.setNombreTerritorio(rs.getString("FCTERRITORIO"));
        return territorio;
    }
}
