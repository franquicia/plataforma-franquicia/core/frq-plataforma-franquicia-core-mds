package com.gs.baz.frq.modelo.negocio.modelos.init;

import com.gs.baz.frq.modelo.negocio.modelos.dao.ModeloDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.modelos")
public class ConfigModeloNegocioModelo {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioModelo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ModeloDAOImpl modeloDAOImpl() {
        return new ModeloDAOImpl();
    }

}
