/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.rest;

import com.gs.baz.frq.documento.sucursal.cecos.dao.DocumentoSucursalCecoDAOImpl;
import com.gs.baz.frq.documento.sucursal.cecos.dto.Cecos;
import com.gs.baz.frq.documento.sucursal.cecos.dto.Geografias;
import com.gs.baz.frq.documento.sucursal.cecos.dto.ParametrosCeco;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "cecos-negocio", value = "cecos-negocio", description = "Api para busqueda de cecos")
@RestController
@RequestMapping("/api-local/documento-sucursal/ceco/v1")
public class DocumentoSucursalCecoApi {

    @Autowired
    private DocumentoSucursalCecoDAOImpl cecoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param ceco
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene cecos", notes = "Obtiene todos los cecos", nickname = "obtieneCecos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/{idCeco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Cecos obtieneCecos(@ApiParam(name = "idCeco", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idCeco") Integer idCeco,
            @ApiParam(name = "idCeco", value = "idCeco", example = "1", required = true) @PathVariable("idCeco") String ceco) throws CustomException, DataNotFoundException {
        Cecos cecos = new Cecos(cecoDAOImpl.selectAllRows(idCeco, ceco));
        if (cecos.getCecos() != null && cecos.getCecos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return cecos;
    }

    /**
     *
     * @param parametrosCeco parametro de entrada con idNegocio y/o ceco
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene cecos", notes = "Obtiene todos los cecos", nickname = "obtieneCecos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Cecos obtieneCecosParam(@ApiParam(name = "ParametrosCeco", value = "Paramentros para la consulta de cecos", required = true) @RequestBody ParametrosCeco parametrosCeco) throws CustomException, DataNotFoundException {

        Cecos cecos = new Cecos(cecoDAOImpl.selectAllRows(
                parametrosCeco.getIdNegocio(),
                parametrosCeco.getCeco()));

        if (cecos.getCecos() != null && cecos.getCecos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return cecos;
    }

    /**
     *
     * @param ceco
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene geografia", notes = "Obtiene toda la geografia", nickname = "obtieneGeografia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/geografia/{ceco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Geografias obtieneGeografia(@ApiParam(name = "ceco", value = "ceco", example = "1", required = true) @PathVariable("ceco") String ceco) throws CustomException, DataNotFoundException {
        Geografias cecos = new Geografias(cecoDAOImpl.selectAllRowsGeografia(ceco));
        if (cecos.getGeografias() != null && cecos.getGeografias().isEmpty()) {
            throw new DataNotFoundException();
        }
        return cecos;
    }

}
