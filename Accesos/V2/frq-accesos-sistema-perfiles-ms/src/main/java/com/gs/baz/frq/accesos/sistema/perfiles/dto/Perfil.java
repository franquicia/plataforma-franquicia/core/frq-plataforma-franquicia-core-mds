/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.perfiles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Perfil", value = "Perfil")
public class Perfil extends PerfilBase {

    @JsonProperty(value = "idPerfil")
    @ApiModelProperty(notes = "Identificador del Perfil", example = "1", position = -1)
    private Integer idPerfil;

    public Perfil() {
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    @Override
    public String toString() {
        return "Perfil{" + "idPerfil=" + idPerfil + '}';
    }

}
