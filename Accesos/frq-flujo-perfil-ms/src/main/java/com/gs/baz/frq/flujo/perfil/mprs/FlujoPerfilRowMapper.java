package com.gs.baz.frq.flujo.perfil.mprs;

import com.gs.baz.frq.flujo.perfil.dto.FlujoPerfilDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FlujoPerfilRowMapper implements RowMapper<FlujoPerfilDTO> {

    private FlujoPerfilDTO flujoPerfilDTO;

    @Override
    public FlujoPerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        flujoPerfilDTO = new FlujoPerfilDTO();
        flujoPerfilDTO.setIdFlujoPerfil(((BigDecimal) rs.getObject("FIIDFLUJO_PERFIL")));
        flujoPerfilDTO.setIdFlujo((BigDecimal) rs.getObject("FIID_FLUJO"));
        flujoPerfilDTO.setIdPerfil((BigDecimal) rs.getObject("FIID_PERFIL"));
        return flujoPerfilDTO;
    }
}
