/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.mprs;

import com.gs.baz.frq.documento.sucursal.documentos.dto.Documento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentosRowMapper implements RowMapper<Documento> {

    private Documento documento;

    @Override
    public Documento mapRow(ResultSet rs, int rowNum) throws SQLException {
        documento = new Documento();
        documento.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        documento.setDescripcion(rs.getString("FCDESCRIPCION"));
        documento.setEstadoHistorico(rs.getInt("FIID_GENHISTO"));
        return documento;
    }
}
