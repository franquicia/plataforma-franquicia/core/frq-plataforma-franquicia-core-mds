package com.gs.baz.frecuencia.medicion.dao;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frecuencia.medicion.dao.util.GenericDAO;
import com.gs.baz.frecuencia.medicion.dto.FrecuenciaMedicionDTO;
import com.gs.baz.frecuencia.medicion.mprs.FrecuenciaMedicionRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class FrecuenciaMedicionDAOImpl extends DefaultDAO implements GenericDAO<FrecuenciaMedicionDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINFRECMED");
        jdbcInsert.withProcedureName("SP_INS_FRECMED");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINFRECMED");
        jdbcUpdate.withProcedureName("SP_ACT_FRECMED");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINFRECMED");
        jdbcDelete.withProcedureName("SP_DEL_FRECMED");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINFRECMED");
        jdbcSelect.withProcedureName("SP_SEL_FRECMED");
        jdbcSelect.returningResultSet("RCL_INFO", new FrecuenciaMedicionRowMapper());

    }

    @Override
    public FrecuenciaMedicionDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRECMED", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FrecuenciaMedicionDTO> data = (List<FrecuenciaMedicionDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<FrecuenciaMedicionDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRECMED", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FrecuenciaMedicionDTO> data = (List<FrecuenciaMedicionDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public FrecuenciaMedicionDTO selectRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<FrecuenciaMedicionDTO> selectRows(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FrecuenciaMedicionDTO insertRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FrecuenciaMedicionDTO insertRow(FrecuenciaMedicionDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdFrecuenciaMedicion((BigDecimal) out.get("PA_FIIDFRECMED"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Frecuencia Medicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Frecuencia Medicion"), ex);
        }
    }

    @Override
    public FrecuenciaMedicionDTO updateRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FrecuenciaMedicionDTO updateRow(FrecuenciaMedicionDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRECMED", entityDTO.getIdFrecuenciaMedicion());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Frecuencia Medicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Frecuencia Medicion"), ex);
        }
    }

    @Override
    public FrecuenciaMedicionDTO deleteRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FrecuenciaMedicionDTO deleteRow(FrecuenciaMedicionDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRECMED", entityDTO.getIdFrecuenciaMedicion());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Frecuencia Medicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Frecuencia Medicion"), ex);
        }
    }

}
