/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author carlos
 */
public class ParametrosClaveVistaPropiedad {

    @JsonProperty(value = "idVista")
    private transient Integer idVista;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre de la propiedad", example = "menu.hide")
    @NotEmpty
    private String nombre;

    public Integer getIdVista() {
        return idVista;
    }

    public void setIdVista(Integer idVista) {
        this.idVista = idVista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "ParametrosClaveVistaPropiedad{" + "idVista=" + idVista + ", nombre=" + nombre + '}';
    }

}
