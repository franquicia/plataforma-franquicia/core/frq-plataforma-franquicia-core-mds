/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.dao;

import com.gs.baz.frq.documentos.sucursal.fichas.dto.AltaFicha;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.Ficha;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.FichaBase;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.ParametrosFicha;
import com.gs.baz.frq.documentos.sucursal.fichas.mprs.FichaRowMapper;
import com.gs.baz.frq.documentos.sucursal.fichas.mprs.FichasRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalFichaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PA_ADFICHA";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_FICHA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_FICHA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_FICHA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SP_SEL_FICHA");
        jdbcSelect.returningResultSet("RCL_FICHA", new FichaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_FICHA");
        jdbcSelectAll.returningResultSet("RCL_FICHA", new FichasRowMapper());

    }

    public FichaBase selectRow(Long idCampoSensible) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDFICHA", idCampoSensible);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FichaBase> data = (List<FichaBase>) out.get("RCL_FICHA");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Ficha> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDFICHA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Ficha>) out.get("RCL_FICHA");
    }

    public AltaFicha insertRow(ParametrosFicha entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_IDNEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_IDFORMATO", entityDTO.getIdFormato());
            mapSqlParameterSource.addValue("PA_IDREGION", entityDTO.getIdRegion());
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_IDTERRI", entityDTO.getIdTerritorio());
            mapSqlParameterSource.addValue("PA_REGION", entityDTO.getRegion());
            mapSqlParameterSource.addValue("PA_ZONA", entityDTO.getZona());
            mapSqlParameterSource.addValue("PA_TERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_LATITUD", entityDTO.getLatitud());
            mapSqlParameterSource.addValue("PA_LONGITUD", entityDTO.getLongitud());
            mapSqlParameterSource.addValue("PA_DIRECCION", entityDTO.getDireccion());
            mapSqlParameterSource.addValue("PA_FOLIO", entityDTO.getIdFolio());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_IDFICHA");
                return new AltaFicha(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosFicha entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDFICHA", entityDTO.getIdFicha());
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_IDNEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_IDFORMATO", entityDTO.getIdFormato());
            mapSqlParameterSource.addValue("PA_IDREGION", entityDTO.getIdRegion());
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_IDTERRI", entityDTO.getIdTerritorio());
            mapSqlParameterSource.addValue("PA_REGION", entityDTO.getRegion());
            mapSqlParameterSource.addValue("PA_ZONA", entityDTO.getZona());
            mapSqlParameterSource.addValue("PA_TERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_LATITUD", entityDTO.getLatitud());
            mapSqlParameterSource.addValue("PA_LONGITUD", entityDTO.getLongitud());
            mapSqlParameterSource.addValue("PA_DIRECCION", entityDTO.getDireccion());
            mapSqlParameterSource.addValue("PA_FOLIO", entityDTO.getIdFolio());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDFICHA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
