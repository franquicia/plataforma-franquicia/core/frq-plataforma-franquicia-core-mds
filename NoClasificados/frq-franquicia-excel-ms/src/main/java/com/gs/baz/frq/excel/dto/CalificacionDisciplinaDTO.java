/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author MADA
 */
public class CalificacionDisciplinaDTO {

    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calif_disciplina")
    private Float calificacionDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_obtenidos")
    private Integer puntosObtenidos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_esperados")
    private Integer puntosEsperados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_defectos")
    private Integer numeroDefectos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_oportunidades")
    private Integer numeroOportunidades;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public CalificacionDisciplinaDTO() {
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Float getCalificacionDisciplina() {
        return calificacionDisciplina;
    }

    public void setCalificacionDisciplina(BigDecimal calificacionDisciplina) {
        this.calificacionDisciplina = (calificacionDisciplina == null ? null : calificacionDisciplina.floatValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getPuntosObtenidos() {
        return puntosObtenidos;
    }

    public void setPuntosObtenidos(Integer puntosObtenidos) {
        this.puntosObtenidos = puntosObtenidos;
    }

    public Integer getPuntosEsperados() {
        return puntosEsperados;
    }

    public void setPuntosEsperados(Integer puntosEsperados) {
        this.puntosEsperados = puntosEsperados;
    }

    public Integer getNumeroDefectos() {
        return numeroDefectos;
    }

    public void setNumeroDefectos(Integer numeroDefectos) {
        this.numeroDefectos = numeroDefectos;
    }

    public Integer getNumeroOportunidades() {
        return numeroOportunidades;
    }

    public void setNumeroOportunidades(Integer numeroOportunidades) {
        this.numeroOportunidades = numeroOportunidades;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "CalificacionDisciplinaDTO{" + "idDisciplina=" + idDisciplina + ", idCeco=" + idCeco + ", calificacionDisciplina=" + calificacionDisciplina + ", fecha=" + fecha + ", puntosObtenidos=" + puntosObtenidos + ", puntosEsperados=" + puntosEsperados + ", numeroDefectos=" + numeroDefectos + ", numeroOportunidades=" + numeroOportunidades + ", idRango=" + idRango + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }
}
