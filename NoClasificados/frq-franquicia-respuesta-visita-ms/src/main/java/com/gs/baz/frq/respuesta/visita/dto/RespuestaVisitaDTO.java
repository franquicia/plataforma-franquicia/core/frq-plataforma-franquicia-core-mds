/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.respuesta.visita.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class RespuestaVisitaDTO {

    @JsonProperty(value = "id_visita")
    private Integer idVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_base")
    private Integer idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pre_calificacion")
    private Integer preCalificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_lineal")
    private Integer calificacionLineal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_final")
    private Integer calificacionFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "contro_si")
    private Integer conteoSi;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_no")
    private Integer conteoNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_na")
    private Integer conteoNa;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_imperdonable")
    private Integer conteoImperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre")
    private String nombre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_puesto")
    private Integer idPuesto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_incumplimiento")
    private Integer conteoIncumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_disciplina")
    private Integer idNegocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_defectos")
    private Integer idDefectos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oportunidades")
    private Integer oportunidades;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "punto_esperado")
    private Integer puntoEsperado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo")
    private Integer conteo;

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(BigDecimal idVisita) {
        this.idVisita = (idVisita == null ? null : idVisita.intValue());
    }

    public Integer getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(BigDecimal idCecoBase) {
        this.idCecoBase = (idCecoBase == null ? null : idCecoBase.intValue());
    }

    public Integer getPreCalificacion() {
        return preCalificacion;
    }

    public void setPreCalificacion(BigDecimal preCalificacion) {
        this.preCalificacion = (preCalificacion == null ? null : preCalificacion.intValue());
    }

    public Integer getCalificacionLineal() {
        return calificacionLineal;
    }

    public void setCalificacionLineal(BigDecimal calificacionLineal) {
        this.calificacionLineal = (calificacionLineal == null ? null : calificacionLineal.intValue());
    }

    public Integer getCalificacionFinal() {
        return calificacionFinal;
    }

    public void setCalificacionFinal(BigDecimal calificacionFinal) {
        this.calificacionFinal = (calificacionFinal == null ? null : calificacionFinal.intValue());
    }

    public Integer getConteoSi() {
        return conteoSi;
    }

    public void setConteoSi(BigDecimal conteoSi) {
        this.conteoSi = (conteoSi == null ? null : conteoSi.intValue());
    }

    public Integer getConteoNo() {
        return conteoNo;
    }

    public void setConteoNo(BigDecimal conteoNo) {
        this.conteoNo = (conteoNo == null ? null : conteoNo.intValue());
    }

    public Integer getConteoNa() {
        return conteoNa;
    }

    public void setConteoNa(BigDecimal conteoNa) {
        this.conteoNa = (conteoNa == null ? null : conteoNa.intValue());
    }

    public Integer getConteoImperdonable() {
        return conteoImperdonable;
    }

    public void setConteoImperdonable(BigDecimal conteoImperdonable) {
        this.conteoImperdonable = (conteoImperdonable == null ? null : conteoImperdonable.intValue());
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(BigDecimal idPuesto) {
        this.idPuesto = (idPuesto == null ? null : idPuesto.intValue());
    }

    public Integer getConteoIncumplimiento() {
        return conteoIncumplimiento;
    }

    public void setConteoIncumplimiento(BigDecimal conteoIncumplimiento) {
        this.conteoIncumplimiento = (conteoIncumplimiento == null ? null : conteoIncumplimiento.intValue());
    }

    public Integer getIdNegocioDisciplina() {
        return idNegocioDisciplina;
    }

    public void setIdNegocioDisciplina(BigDecimal idNegocioDisciplina) {
        this.idNegocioDisciplina = (idNegocioDisciplina == null ? null : idNegocioDisciplina.intValue());
    }

    public Integer getIdDefectos() {
        return idDefectos;
    }

    public void setIdDefectos(BigDecimal idDefectos) {
        this.idDefectos = (idDefectos == null ? null : idDefectos.intValue());
    }

    public Integer getOportunidades() {
        return oportunidades;
    }

    public void setOportunidades(BigDecimal oportunidades) {
        this.oportunidades = (oportunidades == null ? null : oportunidades.intValue());
    }

    public Integer getPuntoEsperado() {
        return puntoEsperado;
    }

    public void setPuntoEsperado(BigDecimal puntoEsperado) {
        this.puntoEsperado = (puntoEsperado == null ? null : puntoEsperado.intValue());
    }

    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(BigDecimal conteo) {
        this.conteo = (conteo == null ? null : conteo.intValue());
    }

    @Override
    public String toString() {
        return "RespuestaVisitaDTO{" + "idVisita=" + idVisita + ", idCecoBase=" + idCecoBase + ", preCalificacion=" + preCalificacion + ", calificacionLineal=" + calificacionLineal + ", calificacionFinal=" + calificacionFinal + ", conteoSi=" + conteoSi + ", conteoNo=" + conteoNo + ", conteoNa=" + conteoNa + ", conteoImperdonable=" + conteoImperdonable + ", idProtocolo=" + idProtocolo + ", fecha=" + fecha + ", idUsuario=" + idUsuario + ", nombre=" + nombre + ", idPuesto=" + idPuesto + ", conteoIncumplimiento=" + conteoIncumplimiento + ", idNegocioDisciplina=" + idNegocioDisciplina + ", idDefectos=" + idDefectos + ", oportunidades=" + oportunidades + ", puntoEsperado=" + puntoEsperado + ", conteo=" + conteo + '}';
    }

}
