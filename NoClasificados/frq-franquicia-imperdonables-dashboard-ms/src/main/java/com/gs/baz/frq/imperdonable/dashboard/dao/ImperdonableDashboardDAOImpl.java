package com.gs.baz.frq.imperdonable.dashboard.dao;

import com.gs.baz.frq.imperdonable.dashboard.dto.ImperdonableDashboardDTO;
import com.gs.baz.frq.imperdonable.dashboard.mprs.ImperdonableDashboardRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ImperdonableDashboardDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectImperdonablesDashboard;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMIMPERDASH");
        jdbcInsert.withProcedureName("SPINSIMPERDASHB");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMIMPERDASH");
        jdbcUpdate.withProcedureName("SPACTIMPERDASHB");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMIMPERDASH");
        jdbcDelete.withProcedureName("SPDELIMPERDASHB");

        jdbcSelectImperdonablesDashboard = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectImperdonablesDashboard.withSchemaName(schema);
        jdbcSelectImperdonablesDashboard.withCatalogName("PAADMIMPERDASH");
        jdbcSelectImperdonablesDashboard.withProcedureName("SPGETIMPERDASHB");
        jdbcSelectImperdonablesDashboard.returningResultSet("PA_CDATOS", new ImperdonableDashboardRowMapper());

    }

    public ImperdonableDashboardDTO insertRow(ImperdonableDashboardDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
            mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
            mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", entityDTO.getIdChecklist());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  ImperdonablesDashboard"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  ImperdonablesDashboard"), ex);
        }
    }

    public ImperdonableDashboardDTO updateRow(ImperdonableDashboardDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
            mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
            mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", entityDTO.getIdChecklist());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of ImperdonablesDashboard"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of ImperdonablesDashboard"), ex);
        }
    }

    public ImperdonableDashboardDTO selectRow(Long idDash, Long idPregunta) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", idDash);
        mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", idPregunta);
        Map<String, Object> out = jdbcSelectImperdonablesDashboard.execute(mapSqlParameterSource);
        List<ImperdonableDashboardDTO> data = (List<ImperdonableDashboardDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<ImperdonableDashboardDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", null);
        mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", null);
        Map<String, Object> out = jdbcSelectImperdonablesDashboard.execute(mapSqlParameterSource);
        return (List<ImperdonableDashboardDTO>) out.get("PA_CDATOS");
    }

    public ImperdonableDashboardDTO deleteRow(ImperdonableDashboardDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
            mapSqlParameterSource.addValue("PA_FIID_PREGUNTA", entityDTO.getIdPregunta());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of ImperdonablesDashboard"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of ImperdonablesDashboard"), ex);
        }
    }

}
