/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta Distribución Detalle Pregunta", value = "RespuestaMergeDetallePregunta")
public class RespuestaMergeDetallePreguntaDTO {

    @JsonProperty(value = "indicadorEjecucion")
    @ApiModelProperty(notes = "Indicador de la ejecución.", example = "1")
    private Integer ejecucion;

    @JsonProperty(value = "registrosAfectados")
    @ApiModelProperty(notes = "Registros afectados de la distribución.", example = "10")
    private Integer registrosAfectados;

    @JsonProperty(value = "envioCorreo")
    @ApiModelProperty(notes = "Indicador del envío de notificación por correo electrónico.", example = "true")
    private Boolean envioCorreo;

    public Integer getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(Integer ejecucion) {
        this.ejecucion = ejecucion;
    }

    public Integer getRegistrosAfectados() {
        return registrosAfectados;
    }

    public void setRegistrosAfectados(Integer registrosAfectados) {
        this.registrosAfectados = registrosAfectados;
    }

    public Boolean getEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(Boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    @Override
    public String toString() {
        return "RespuestaMergeDetallePreguntaDTO{" + "ejecucion=" + ejecucion + ", registrosAfectados=" + registrosAfectados + ", envioCorreo=" + envioCorreo + '}';
    }

}
