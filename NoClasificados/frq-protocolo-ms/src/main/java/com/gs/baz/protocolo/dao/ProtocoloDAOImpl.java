package com.gs.baz.protocolo.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.protocolo.dao.util.GenericDAO;
import com.gs.baz.protocolo.dto.ProtocoloDTO;
import com.gs.baz.protocolo.mprs.ProtocoloDisciplinaRowMapper;
import com.gs.baz.protocolo.mprs.ProtocoloRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDAOImpl extends DefaultDAO implements GenericDAO<ProtocoloDTO> {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectProtocolosDisciplina;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPROTOCOLO");
        jdbcSelect.withProcedureName("SP_SEL_PROTOCOLO");
        jdbcSelect.returningResultSet("RCL_INFO", new ProtocoloRowMapper());

        jdbcSelectProtocolosDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectProtocolosDisciplina.withSchemaName(schema);
        jdbcSelectProtocolosDisciplina.withCatalogName("PAADMINPROTSDISC");
        jdbcSelectProtocolosDisciplina.withProcedureName("SP_SEL_PROTSDISC");
        jdbcSelectProtocolosDisciplina.returningResultSet("RCL_INFO", new ProtocoloDisciplinaRowMapper());
    }

    @Override
    public ProtocoloDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPROTOCOLO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ProtocoloDTO> data = (List<ProtocoloDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<ProtocoloDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPROTOCOLO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<ProtocoloDTO>) out.get("RCL_INFO");

    }

    @Override
    public List<ProtocoloDTO> selectRowsProtocolosDisciplina(Long idDisciplinaNegocio) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDDISNEG", idDisciplinaNegocio);
        Map<String, Object> out = jdbcSelectProtocolosDisciplina.execute(mapSqlParameterSource);
        return (List<ProtocoloDTO>) out.get("RCL_INFO");

    }

}
