/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.init;

import com.gs.baz.frq.api.portal.cautivo.bi.AppBI;
import com.gs.baz.frq.api.portal.cautivo.bi.ConexionesBI;
import com.gs.baz.frq.api.portal.cautivo.bi.GobiernoDatosBI;
import com.gs.baz.frq.api.portal.cautivo.bi.IdentificadorBI;
import com.gs.baz.frq.api.portal.cautivo.dao.ClienteDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dao.ConexionesDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dao.GobiernoDatosDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dao.IdentificadorDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente.ECServicioApp;
import com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente.ECServicioConexiones;
import com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente.ECServicioIdentificador;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author MADA
 */
@Configuration
@ComponentScan("com.gs.baz.frq.api.portal.cautivo")
public class ConfigPortalCautivo {

    private final Logger logger = LogManager.getLogger();

    public ConfigPortalCautivo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public ConexionesBI obtenerConexionesBI() throws Exception {
        return new ConexionesBI();
    }

    @Bean
    public GobiernoDatosBI gobiernoDatosBI() throws Exception {
        return new GobiernoDatosBI();
    }

    @Bean
    public ECServicioConexiones ecServicioConexiones() throws Exception {
        return new ECServicioConexiones();
    }

    @Bean(initMethod = "init")
    public ConexionesDAOImpl conexionesDAOImpl() throws Exception {
        return new ConexionesDAOImpl();
    }

    @Bean(initMethod = "init")
    public GobiernoDatosDAOImpl gobiernoDatosDAOImpl() throws Exception {
        return new GobiernoDatosDAOImpl();
    }

    @Bean
    public AppBI appBI() throws Exception {
        return new AppBI();
    }

    @Bean
    public ECServicioApp ecServicioApp() throws Exception {
        return new ECServicioApp();
    }

    @Bean(initMethod = "init")
    public ClienteDAOImpl clienteDAOImpl() throws Exception {
        return new ClienteDAOImpl();
    }

    @Bean
    public IdentificadorBI identificadorBI() throws Exception {
        return new IdentificadorBI();
    }

    @Bean
    public ECServicioIdentificador ecServicioIdentificador() throws Exception {
        return new ECServicioIdentificador();
    }

    @Bean(initMethod = "init")
    public IdentificadorDAOImpl identificadorDAOImpl() throws Exception {
        return new IdentificadorDAOImpl();
    }

}
