package com.gs.baz.frq.modelo.negocio.arboles.init;

import com.gs.baz.frq.modelo.negocio.arboles.dao.ArbolDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.arboles")
public class ConfigArbol {

    private final Logger logger = LogManager.getLogger();

    public ConfigArbol() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ArbolDAOImpl arbolDAOImpl() {
        return new ArbolDAOImpl();
    }

}
