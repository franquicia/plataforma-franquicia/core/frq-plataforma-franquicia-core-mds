/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de la propiedad", value = "VistaPropiedadBase")
public class VistaPropiedadBase {

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre de la propiedad", example = "menu.hide")
    private String nombre;

    @JsonProperty(value = "valor")
    @ApiModelProperty(notes = "Valor de la propiedad", example = "1")
    private String valor;

    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Tipo de valor de la propiedad", example = "String")
    private String tipo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "VistaPropiedadBase{" + "nombre=" + nombre + ", valor=" + valor + ", tipo=" + tipo + '}';
    }

}
