package com.gs.baz.frq.dashboard.dao.graficos.g011;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g011.CumplimientoPuntajeNegociosTerritorialDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g011.CumplimientoPuntajeNegociosTerritorialRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl011 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s;
    private DefaultJdbcCall jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s.withSchemaName(schema);
        jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s.withCatalogName("PACONSDASHNAC");
        jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s.withProcedureName("SPCUMP_PUNTGEN");
        jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s.returningResultSet("RCL_INFO", new CumplimientoPuntajeNegociosTerritorialRowMapper());

        jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s.withSchemaName(schema);
        jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s.withCatalogName("PACONSDASHNAC2");
        jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s.withProcedureName("SPCUMP_PUNTGEN");
        jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s.returningResultSet("RCL_INFO", new CumplimientoPuntajeNegociosTerritorialRowMapper());
    }

    public List<CumplimientoPuntajeNegociosTerritorialDTO> selectRowsCumplimientoPuntajeNegociosNivelGeografico(CumplimientoPuntajeNegociosTerritorialDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        if (filtros.getIdDisciplina().equals(5)) {
            return this.selectRowsCumplimientoPuntajeNegociosNivelGeografico7s(entityDTO);
        } else {
            return this.selectRowsCumplimientoPuntajeNegociosNivelGeograficoNo7s(entityDTO);
        }
    }

    private List<CumplimientoPuntajeNegociosTerritorialDTO> selectRowsCumplimientoPuntajeNegociosNivelGeografico7s(CumplimientoPuntajeNegociosTerritorialDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEGOCIO", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoPuntajeNegociosNivelGeografico7s.execute(mapSqlParameterSource);
        return (List<CumplimientoPuntajeNegociosTerritorialDTO>) out.get("RCL_INFO");
    }

    private List<CumplimientoPuntajeNegociosTerritorialDTO> selectRowsCumplimientoPuntajeNegociosNivelGeograficoNo7s(CumplimientoPuntajeNegociosTerritorialDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEGOCIO", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoPuntajeNegociosNivelGeograficoNo7s.execute(mapSqlParameterSource);
        return (List<CumplimientoPuntajeNegociosTerritorialDTO>) out.get("RCL_INFO");
    }

}
