/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.rest.client.internal;

import com.gs.baz.frq.jobs.scheduler.rest.client.util.DefaultRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class SubServiceToken extends DefaultRestTemplate {

    public String getToken() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.POST, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                if (responseString.getBody() != null) {
                    return responseString.getBody().toString();
                } else {
                    throw new Exception("token response body data attribute is null");
                }
            } else {
                throw new Exception("token response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

}
