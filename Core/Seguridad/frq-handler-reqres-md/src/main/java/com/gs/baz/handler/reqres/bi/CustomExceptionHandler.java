/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.handler.reqres.bi;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.gs.baz.autorization.server.apigee.bi.ApigeeJWTException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.model.commons.ErrorMessageDTO;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorClient;
import com.gs.baz.frq.swagger.responses.RespuestaErrorValidationClient;
import com.gs.baz.handler.reqres.ex.AttributeMissingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private String details;
    private final Logger logger2 = LogManager.getLogger();
    private String contextPath;
    @Autowired
    HttpServletRequest httpServletRequest;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        Throwable throwableLastOrigin = ex.getCause();
        if (throwableLastOrigin instanceof InvalidFormatException) {
            InvalidFormatException invalidFormatException = (InvalidFormatException) ex.getCause();
            invalidFormatException.getPath().forEach(ref -> {
                String[] typeOriginSplit = invalidFormatException.getTargetType().getName().split("\\.");
                String[] typeTargetSplit = invalidFormatException.getValue().getClass().getName().split("\\.");
                String typeOrigin = typeOriginSplit[typeOriginSplit.length - 1];
                String typeTarget = typeTargetSplit[typeTargetSplit.length - 1];
                erroresDetalles.add(new ErrorDetalle("El tipo de dato del atributo '" + ref.getFieldName() + "' es '" + typeOrigin.toLowerCase() + "', pero se encontró un tipo '" + typeTarget.toLowerCase() + "'"));
            });
        } else if (throwableLastOrigin instanceof UnrecognizedPropertyException) {
            UnrecognizedPropertyException unrecognizedPropertyException = (UnrecognizedPropertyException) throwableLastOrigin;
            unrecognizedPropertyException.getKnownPropertyIds();
            unrecognizedPropertyException.getPropertyName();
            unrecognizedPropertyException.getPath().forEach(ref -> {
                erroresDetalles.add(new ErrorDetalle("El atributo '" + unrecognizedPropertyException.getPropertyName() + "' no esta permitido en el cuerpo de la petición"));
            });
        } else if (throwableLastOrigin instanceof AttributeMissingException) {
            AttributeMissingException attributeMissingException = (AttributeMissingException) throwableLastOrigin;
            attributeMissingException.getAttributes();
            attributeMissingException.getAttributes().stream().forEach((attribute) -> {
                erroresDetalles.add(new ErrorDetalle("Atributo '" + attribute + "' es faltante o requiere un valor no nulo"));
            });
        } else {
            erroresDetalles.add(new ErrorDetalle("Error al deserializar los tipos de datos presentes en el cuerpo de la petición"));
        }
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        Exception genericEx = ex;
        MethodArgumentTypeMismatchException methodArgumentTypeMismatchException = (MethodArgumentTypeMismatchException) genericEx;
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        if (methodArgumentTypeMismatchException != null) {
            String[] typeOriginSplit = ex.getRequiredType().getName().split("\\.");
            String[] typeTargetSplit = ex.getValue().getClass().getName().split("\\.");
            String typeOrigin = typeOriginSplit[typeOriginSplit.length - 1];
            String typeTarget = typeTargetSplit[typeTargetSplit.length - 1];
            erroresDetalles.add(new ErrorDetalle("El valor del atributo '" + methodArgumentTypeMismatchException.getName() + "' requiere un valor de tipo '" + typeOrigin.toLowerCase() + "', pero se encontró un valor '" + ex.getValue() + "' de tipo '" + typeTarget.toLowerCase() + "'"));
        }
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        errors.forEach(item -> {
            erroresDetalles.add(new ErrorDetalle(item));
        });
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    public ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        erroresDetalles.add(new ErrorDetalle("Parámetro '" + ex.getParameterName() + "' es requerido."));
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<MediaType> mediaTypes = ex.getSupportedMediaTypes();
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        mediaTypes.forEach(item -> {
            erroresDetalles.add(new ErrorDetalle("Solo " + item + " es soportado."));
        });
        if (mediaTypes.isEmpty()) {
            erroresDetalles.add(new ErrorDetalle("Tipo de contenido no soportado"));
        }
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        if (supportedMethods != null) {
            supportedMethods.forEach(item -> {
                erroresDetalles.add(new ErrorDetalle("Solo método " + item.name() + " es soportado."));
            });
        }
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        int statusCode = status.value();
        ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
        List<ErrorDetalle> erroresDetalles = new ArrayList<>();
        erroresDetalles.add(new ErrorDetalle("Parámetro '" + ex.getRequestPartName() + "' es requerido."));
        RespuestaAPI errorMessage = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
        return handleExceptionInternal(ex, errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    public ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger2.log(Level.TRACE, "REST All ExceptionInternal", ex);
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }
        contextPath = httpServletRequest.getRequestURI();
        if (!contextPath.contains("/api/") && !contextPath.contains("/api-local/")) {
            return this.handleAllExceptions(httpServletRequest, ex, request);
        }
        return new ResponseEntity<>(body, headers, status);
    }

    @ExceptionHandler({ApigeeJWTException.class})
    public final ResponseEntity<Object> handleApigeeJWTException(HttpServletRequest httpServletRequest, Exception ex, WebRequest request) {
        ResponseApiCodes apiCode;
        String detalleError;
        if (ex.getMessage().equals("SignatureException")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04041;
            detalleError = apiCode.description();
        } else if (ex.getMessage().equals("MalformedJwtException")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04001;
            detalleError = apiCode.description();
        } else if (ex.getMessage().equals("ExpiredJwtException")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04011;
            detalleError = apiCode.description();
        } else if (ex.getMessage().equals("SecurityUtilsException")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_00000;
            detalleError = apiCode.description();
        } else if (ex.getMessage().equals("NotUserMatchException")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04051;
            detalleError = apiCode.description();
        } else if (ex.getMessage().equals("AuthorizationMissing")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04061;
            detalleError = "Atributo token es requerido.";
        } else if (ex.getMessage().equals("UserMissing")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04061;
            detalleError = "Atributo usuario es requerido.";
        } else if (ex.getMessage().equals("PublicKeyMissing")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04061;
            detalleError = "Atributo llavePublica es requerido.";
        } else {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_00000;
            detalleError = apiCode.description();
        }
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        ResponseMessage responseMessage = ResponseMensages.getByStatus(400);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
        List<ErrorDetalle> detalles = new ArrayList<>();
        detalles.add(new ErrorDetalle(detalleError));
        RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
        return new ResponseEntity(respuestaErrorClient, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DataNotFoundException.class})
    public final ResponseEntity<Object> handleDataNotFoundException(HttpServletRequest httpServletRequest, Exception ex, WebRequest request) {
        ResponseApiCodes apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04040;
        String detalleError = apiCode.description();
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        ResponseMessage responseMessage = ResponseMensages.getByStatus(404);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
        List<ErrorDetalle> detalles = new ArrayList<>();
        detalles.add(new ErrorDetalle(detalleError));
        RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
        return new ResponseEntity(respuestaErrorClient, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DataNotInsertedException.class})
    public final ResponseEntity<Object> handleDataNotInsertedException(HttpServletRequest httpServletRequest, Exception ex, WebRequest request) {
        DataNotInsertedException dataNotInsertedException = (DataNotInsertedException) ex;
        ResponseApiCodes apiCode;
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        DataNotInsertedException.Cause cause = dataNotInsertedException.getCustomCause();
        if (cause != null && cause.equals(DataNotInsertedException.Cause.VIOLATION_KEY)) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04007;
        } else {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05001;
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (httpStatus.equals(HttpStatus.BAD_REQUEST)) {
            String detalleError = apiCode.description();
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(detalleError));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            return new ResponseEntity(respuestaErrorClient, httpStatus);
        } else {
            String detalleError = apiCode.description();
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(500);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(detalleError));
            RespuestaErrorValidationClient respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            return new ResponseEntity(respuestaErrorClient, httpStatus);
        }
    }

    @ExceptionHandler({DataNotUpdatedException.class})
    public final ResponseEntity<Object> handleDataNotUpdatedException(HttpServletRequest httpServletRequest, Exception ex, WebRequest request) {
        DataNotUpdatedException dataNotUpdatedException = (DataNotUpdatedException) ex;
        ResponseApiCodes apiCode;
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        DataNotUpdatedException.Cause cause = dataNotUpdatedException.getCustomCause();
        if (cause != null && cause.equals(DataNotUpdatedException.Cause.VIOLATION_KEY)) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04006;
        } else if (cause != null && cause.equals(DataNotUpdatedException.Cause.ROW_NOT_FOUND)) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04008;
        } else {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05002;
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (httpStatus.equals(HttpStatus.BAD_REQUEST)) {
            String detalleError = apiCode.description();
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(detalleError));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            return new ResponseEntity(respuestaErrorClient, httpStatus);
        } else {
            String detalleError = apiCode.description();
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(500);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(detalleError));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            return new ResponseEntity(respuestaErrorClient, httpStatus);
        }
    }

    @ExceptionHandler({DataNotDeletedException.class})
    public final ResponseEntity<Object> handleDataNotDeletedException(HttpServletRequest httpServletRequest, Exception ex, WebRequest request) {
        DataNotDeletedException dataNotDeletedException = (DataNotDeletedException) ex;
        ResponseApiCodes apiCode;
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        if (dataNotDeletedException.getCustomCause() != null && dataNotDeletedException.getCustomCause().equals(DataNotDeletedException.Cause.ROW_NOT_FOUND)) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04009;
        } else {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05003;
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (httpStatus.equals(HttpStatus.BAD_REQUEST)) {
            String detalleError = apiCode.description();
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(detalleError));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            return new ResponseEntity(respuestaErrorClient, httpStatus);
        } else {
            String detalleError = apiCode.description();
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(500);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(detalleError));
            RespuestaErrorValidationClient respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            return new ResponseEntity(respuestaErrorClient, httpStatus);
        }
    }

    /**
     *
     * @param httpServletRequest
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler
    public final ResponseEntity<Object> handleAllExceptions(HttpServletRequest httpServletRequest, Exception ex, WebRequest request) {
        logger2.log(Level.TRACE, "REST All Exceptions", ex);
        contextPath = httpServletRequest.getRequestURI();
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        if (contextPath.contains("/api/") || contextPath.contains("/api-local/")) {
            int statusCode = 500;
            ResponseMessage responseMessage = ResponseMensages.getByStatus(statusCode);
            if (contextPath.contains("oauth/token")) {
                String responseMessageError = ex.getMessage();
                if (responseMessageError != null) {
                    if (statusCode == 0) {
                        statusCode = new Integer(responseMessageError.substring(0, 3));
                        responseMessage = ResponseMensages.getByStatus(statusCode);
                    }
                }
            }
            String codeError = responseMessage.getCode() + "." + nameApi + "." + "10" + statusCode + "1";
            RespuestaAPI errorMessage = new RespuestaErrorClient(codeError, responseMessage.getMessage(), requestID);
            return new ResponseEntity(errorMessage, HttpStatus.resolve(statusCode));
        }
        final Integer statusCode = 500;
        details = HttpStatus.resolve(statusCode) + " for -> " + request.getDescription(false);
        ErrorMessageDTO errorMessage = new ErrorMessageDTO(new Date(), ex.getMessage(), details);
        errorMessage.setStatusCode(statusCode);
        if (ex.getMessage() != null && ex.getMessage().contains("d3d23d")) {
            String exceptionMessage = ex.getMessage();
            String[] errorExceptionMessage = exceptionMessage.split("d3d23d");
            exceptionMessage = errorExceptionMessage[1];
            exceptionMessage = exceptionMessage.replace("[", "");
            exceptionMessage = exceptionMessage.replace("]", "");
            String[] error = exceptionMessage.split(",");
            errorMessage.setTrackCode(new Integer(error[0]));
            errorMessage.setMessage(error[1]);
            errorMessage.setDetails((error[2].equals("null") ? null : error[2]));
        }
        return new ResponseEntity<>(errorMessage, HttpStatus.OK);
    }
}
