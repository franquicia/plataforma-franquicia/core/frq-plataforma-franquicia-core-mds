/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.grupo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de grupo", value = "GrupoBase")
public class GrupoBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Titulo del grupo", example = "Grupo")
    protected String descripcion;

    @JsonProperty(value = "totalDocumentos")
    @ApiModelProperty(notes = "Total de documentos", example = "1")
    private Integer totalDocumentos;

    @JsonProperty(value = "totalUsuarios")
    @ApiModelProperty(notes = "Total de usuarios", example = "1")
    private Integer totalUsuarios;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getTotalDocumentos() {
        return totalDocumentos;
    }

    public void setTotalDocumentos(Integer totalDocumentos) {
        this.totalDocumentos = totalDocumentos;
    }

    public Integer getTotalUsuarios() {
        return totalUsuarios;
    }

    public void setTotalUsuarios(Integer totalUsuarios) {
        this.totalUsuarios = totalUsuarios;
    }

    @Override
    public String toString() {
        return "GrupoBase{" + "descripcion=" + descripcion + ", totalDocumentos=" + totalDocumentos + ", totalUsuarios=" + totalUsuarios + '}';
    }

}
