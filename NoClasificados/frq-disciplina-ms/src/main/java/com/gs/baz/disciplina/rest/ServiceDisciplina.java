/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplina.rest;

import com.gs.baz.disciplina.dao.DisciplinaDAOImpl;
import com.gs.baz.disciplina.dto.DisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/disciplina")
public class ServiceDisciplina {

    @Autowired
    private DisciplinaDAOImpl disciplinaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DisciplinaDTO> getDisciplina() throws CustomException {
        return disciplinaDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_disciplina}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO getDisciplina(@PathVariable("id_disciplina") Long id_disciplina) throws CustomException {
        return disciplinaDAOImpl.selectRow(id_disciplina);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO postDisciplina(@RequestBody DisciplinaDTO disciplina) throws CustomException {
        if (disciplina.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return disciplinaDAOImpl.insertRow(disciplina);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO putDisciplina(@RequestBody DisciplinaDTO disciplina) throws CustomException {
        if (disciplina.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (disciplina.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return disciplinaDAOImpl.updateRow(disciplina);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO deleteDisciplina(@RequestBody DisciplinaDTO disciplina) throws CustomException {
        if (disciplina.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        return disciplinaDAOImpl.deleteRow(disciplina);
    }

    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DisciplinaDTO> getDisciplinaByNombre(@RequestHeader HttpHeaders headers, @QueryParam("value") String value) throws CustomException {
        return disciplinaDAOImpl.selectRowsByName(value);
    }
}
