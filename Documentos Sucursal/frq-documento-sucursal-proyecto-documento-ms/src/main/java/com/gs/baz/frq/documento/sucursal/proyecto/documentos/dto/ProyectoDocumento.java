/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Proyecto", value = "Proyecto")
public class ProyectoDocumento extends ProyectoDocumentoBase {

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del Proyecto", example = "1", position = -1)
    private Integer idProyecto;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Nombre del documento", example = "Nombre", position = -1)
    private String nombreDoc;

    public String getNombreDoc() {
        return nombreDoc;
    }

    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }
}
