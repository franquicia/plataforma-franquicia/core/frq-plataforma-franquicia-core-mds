/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dao.SemaforoRangoDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.AltaRango;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.ParametrosRango;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.RangoBase;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.Rangos;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "semaforo-rangos", value = "semaforo-rangos", description = "Api para la gestión del catalogo de rangos de un semaforo")
@RestController
@RequestMapping("/api-local/sistema-informacion/tablero/semaforos/v1")
public class SistemaInformacionDashboardSemaforoRangoApi {

    @Autowired
    private SemaforoRangoDAOImpl semaforoRangoDAOImpl;

    /**
     *
     * @param idSemaforo
     * @param idRango
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rango", notes = "Obtiene un rango", nickname = "obtieneRango")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idSemaforo}/rangos/{idRango}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RangoBase obtieneRango(
            @ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo,
            @ApiParam(name = "idRango", value = "Identificador del rango", example = "1", required = true) @PathVariable("idRango") Integer idRango) throws CustomException, DataNotFoundException {
        RangoBase rangoBase = semaforoRangoDAOImpl.selectRow(idSemaforo, idRango);
        if (rangoBase == null) {
            throw new DataNotFoundException();
        }
        return rangoBase;
    }

    /**
     *
     * @param idSemaforo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rangos", notes = "Obtiene todos los rangos", nickname = "obtieneRangos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idSemaforo}/rangos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rangos obtieneRangos(@ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Long idSemaforo) throws CustomException, DataNotFoundException {
        Rangos rangos = new Rangos(semaforoRangoDAOImpl.selectAllRows(idSemaforo));
        if (rangos.getRangos() != null && rangos.getRangos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rangos;
    }

    /**
     *
     * @param idSemaforo
     * @param parametrosRango
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear rango", notes = "Agrega un rango", nickname = "creaRango")
    @RequestMapping(value = "/{idSemaforo}/rangos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaRango creaRango(
            @ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo,
            @ApiParam(name = "ParametrosRango", value = "Paramentros para el alta del rango", required = true) @RequestBody ParametrosRango parametrosRango) throws DataNotInsertedException {
        parametrosRango.setIdSemaforo(idSemaforo);
        return semaforoRangoDAOImpl.insertRow(parametrosRango);
    }

    /**
     *
     * @param idSemaforo
     * @param parametrosRango
     * @param idRango
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar rango", notes = "Actualiza un rango", nickname = "actualizaRango")
    @RequestMapping(value = "/{idSemaforo}/rangos/{idRango}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaRango(
            @ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo,
            @ApiParam(name = "ParametrosRango", value = "Paramentros para la actualización del rango", required = true) @RequestBody ParametrosRango parametrosRango, @ApiParam(name = "idRango", value = "Identificador del rango", example = "1", required = true) @PathVariable("idRango") Integer idRango) throws DataNotUpdatedException {
        parametrosRango.setIdSemaforo(idSemaforo);
        parametrosRango.setIdRango(idRango);
        semaforoRangoDAOImpl.updateRow(parametrosRango);
        return new SinResultado();
    }

    /**
     *
     * @param idSemaforo
     * @param idRango
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar rango", notes = "Elimina un item de los rangos", nickname = "eliminaRango")
    @RequestMapping(value = "/{idSemaforo}/rangos/{idRango}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaRango(
            @ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo,
            @ApiParam(name = "idRango", value = "Identificador del rango", example = "1", required = true) @PathVariable("idRango") Integer idRango) throws DataNotDeletedException {
        semaforoRangoDAOImpl.deleteRow(idRango);
        return new SinResultado();
    }

}
