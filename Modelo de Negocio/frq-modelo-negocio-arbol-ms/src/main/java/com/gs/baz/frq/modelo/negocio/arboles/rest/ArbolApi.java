/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.arboles.dao.ArbolDAOImpl;
import com.gs.baz.frq.modelo.negocio.arboles.dto.AltaArbol;
import com.gs.baz.frq.modelo.negocio.arboles.dto.ArbolBase;
import com.gs.baz.frq.modelo.negocio.arboles.dto.Arboles;
import com.gs.baz.frq.modelo.negocio.arboles.dto.FolioNivel;
import com.gs.baz.frq.modelo.negocio.arboles.dto.FoliosNivel;
import com.gs.baz.frq.modelo.negocio.arboles.dto.ParametrosArbol;
import com.gs.baz.frq.modelo.negocio.arboles.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "arbol-negocio", value = "arbol-negocio", description = "Gestión del arbol negocio")
@RestController
@RequestMapping("/api-local/modelo-negocio/arbol-negocio/v1")
public class ArbolApi {

    @Autowired
    private ArbolDAOImpl arbolDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idArbol
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene arbol", notes = "Obtiene un arbol", nickname = "obtieneArbol")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idModeloNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArbolBase obtieneArbol(@ApiParam(name = "idModeloNegocio", value = "Identificador del modelo negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idArbol) throws CustomException, DataNotFoundException {
        ArbolBase arbolBase = arbolDAOImpl.selectRow(idArbol);
        if (arbolBase == null) {
            throw new DataNotFoundException();
        }
        return arbolBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene arboles", notes = "Obtiene todos los arboles", nickname = "obtieneArboles")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Arboles obtieneArboles() throws CustomException, DataNotFoundException {
        Arboles arboles = new Arboles(arbolDAOImpl.selectAllRows());
        if (arboles.getArboles() != null && arboles.getArboles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return arboles;
    }

    /**
     *
     * @param parametrosArbol
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear arbol", notes = "Agrega un arbol", nickname = "creaArbol")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaArbol creaArbol(@ApiParam(name = "ParametrosArbol", value = "Paramentros para el alta del arbol", required = true) @RequestBody ParametrosArbol parametrosArbol) throws DataNotInsertedException {
        return arbolDAOImpl.insertRow(parametrosArbol);
    }

    /**
     *
     * @param parametrosArbol
     * @param idArbol
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar arbol", notes = "Actualiza un arbol", nickname = "actualizaArbol")
    @RequestMapping(value = "/{idModeloNegocio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaArbol(@ApiParam(name = "ParametrosArbol", value = "Paramentros para la actualización del arbol", required = true) @RequestBody ParametrosArbol parametrosArbol, @ApiParam(name = "idModeloNegocio", value = "Identificador del modelo negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Integer idModeloNegocio) throws DataNotUpdatedException {
        parametrosArbol.setIdModeloNegocio(idModeloNegocio);
        arbolDAOImpl.updateRow(parametrosArbol);
        return new SinResultado();
    }

    /**
     *
     * @param idArbol
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar arbol", notes = "Elimina un item de los arboles", nickname = "eliminaArbol")
    @RequestMapping(value = "/{idModeloNegocio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaArbol(@ApiParam(name = "idModeloNegocio", value = "Identificador del modelo negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Integer idArbol) throws DataNotDeletedException {
        arbolDAOImpl.deleteRow(idArbol);
        return new SinResultado();
    }

    /**
     *
     * @param idModeloNegocioPadre
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene arboles por padre y nivel", notes = "Obtiene todos los arboles por el padre y nivel", nickname = "obtieneArbolesPorPadreNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idModeloNegocioPadre}/niveles/{idNivel}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Arboles obtieneArbolesPorPadreNivel(@ApiParam(name = "idModeloNegocioPadre", value = "Identificador del arbol por padre", example = "1", required = true) @PathVariable("idModeloNegocioPadre") Long idModeloNegocioPadre,
            @ApiParam(name = "idNivel", value = "Identificador del nivel", example = "1", required = true) @PathVariable("idNivel") Long idNivel) throws CustomException, DataNotFoundException {
        Arboles arboles = new Arboles(arbolDAOImpl.selectAllRowsParent(idModeloNegocioPadre, idNivel));
        if (arboles.getArboles() != null && arboles.getArboles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return arboles;
    }

    /**
     *
     * @param idModeloNegocio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios por modelo de negocio", notes = "Obtiene todos los folios por modelo de negocio", nickname = "obtieneFoliosNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idModeloNegocio}/folios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FoliosNivel obtieneFoliosNivel(@ApiParam(name = "idModeloNegocio", value = "Identificador del modelo de negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idModeloNegocio) throws CustomException, DataNotFoundException {
        List<FolioNivel> foliosNivel = arbolDAOImpl.selectFoliosNivel(idModeloNegocio);
        FoliosNivel folios = new FoliosNivel(foliosNivel);
        if (folios.getFolioEtiqueta() != null && folios.getFolioEtiqueta().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }
}
