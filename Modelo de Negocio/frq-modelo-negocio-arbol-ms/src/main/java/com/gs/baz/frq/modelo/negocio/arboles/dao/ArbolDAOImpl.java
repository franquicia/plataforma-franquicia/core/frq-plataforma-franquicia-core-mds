/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.arboles.dto.AltaArbol;
import com.gs.baz.frq.modelo.negocio.arboles.dto.Arbol;
import com.gs.baz.frq.modelo.negocio.arboles.dto.ArbolBase;
import com.gs.baz.frq.modelo.negocio.arboles.dto.FolioNivel;
import com.gs.baz.frq.modelo.negocio.arboles.dto.ParametrosArbol;
import com.gs.baz.frq.modelo.negocio.arboles.mprs.ArbolRowMapper;
import com.gs.baz.frq.modelo.negocio.arboles.mprs.ArbolesNivelRowMapper;
import com.gs.baz.frq.modelo.negocio.arboles.mprs.ArbolesRowMapper;
import com.gs.baz.frq.modelo.negocio.arboles.mprs.FoliosNivelRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class ArbolDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectAllParent;
    private DefaultJdbcCall jdbcSelectFoliosNivel;

    private DefaultJdbcCall jdbcDelete;
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNARCHIVO";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNMODENEGOC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNMODENEGOC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNMODENEGOC");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETMNMODENEGOC");
        jdbcSelect.returningResultSet("PA_CDATOS", new ArbolRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETMNMODENEGOC");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new ArbolesRowMapper());

        jdbcSelectAllParent = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllParent.withSchemaName(schema);
        jdbcSelectAllParent.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectAllParent.withProcedureName("SPGETMNEGOC_NIV");
        jdbcSelectAllParent.returningResultSet("PA_CDATOS", new ArbolesNivelRowMapper());

        jdbcSelectFoliosNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosNivel.withSchemaName(schema);
        jdbcSelectFoliosNivel.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectFoliosNivel.withProcedureName("SPRECUPERADOCTO_MODNEG");
        jdbcSelectFoliosNivel.returningResultSet("PA_CDATOS", new FoliosNivelRowMapper());
    }

    public ArbolBase selectRow(Long entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", entityDTO);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ArbolBase> data = (List<ArbolBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Arbol> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Arbol>) out.get("PA_CDATOS");
    }

    public List<Arbol> selectAllRowsParent(Long modeloNegocioPadre, Long idNivel) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", modeloNegocioPadre);
        mapSqlParameterSource.addValue("PA_FIIDNIVEL", idNivel);
        Map<String, Object> out = jdbcSelectAllParent.execute(mapSqlParameterSource);
        return (List<Arbol>) out.get("PA_CDATOS");
    }

    public AltaArbol insertRow(ParametrosArbol entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPADRE", entityDTO.getIdPadre());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCICONO", entityDTO.getIcono());
            mapSqlParameterSource.addValue("PA_FIIDNIVEL", entityDTO.getIdNivel());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDMODENEGOC");
                return new AltaArbol(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosArbol entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", entityDTO.getIdModeloNegocio());
            mapSqlParameterSource.addValue("PA_FIIDPADRE", entityDTO.getIdPadre());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCICONO", entityDTO.getIcono());
            mapSqlParameterSource.addValue("PA_FIIDNIVEL", entityDTO.getIdNivel());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<FolioNivel> selectFoliosNivel(Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ETIQUETA", idEtiqueta);
        Map<String, Object> out = jdbcSelectFoliosNivel.execute(mapSqlParameterSource);
        return (List<FolioNivel>) out.get("PA_CDATOS");
    }
}
