/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Asigna a un Proveedor", value = "RespuestaAsignaProveedor")
public class AsignaProveedorResponseDTO {

    @JsonProperty(value = "respuestaAsignacion")
    @ApiModelProperty(notes = "Estatus de la respuesta", example = "1")
    private Integer estatus;

    public AsignaProveedorResponseDTO() {
        super();

    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

}
