/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao.SistemaInformacionVisitasPendientesGraficasDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CertificacionesGerentes;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.Gerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.GraficaGeneral;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesGerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesSemanales;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PuntosVentaPendientes;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "visitas pendientes", value = "visitas pendientes", description = "Api para la gestión de visitas pendientes")
@RestController
@RequestMapping("/api-local/sistema-informacion/visitas/pendientes/graficas/v1")
public class SistemaInformacionDashboardVisitasPendientesGraficasApi {

    @Autowired
    private SistemaInformacionVisitasPendientesGraficasDAOImpl sistemaInformacionVisitasPendientesGraficasDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GraficaGeneral obtieneGrafica() throws CustomException, DataNotFoundException {
        GraficaGeneral grafica01 = sistemaInformacionVisitasPendientesGraficasDAOImpl.selectGrafica01();
        if (grafica01 == null) {
            throw new DataNotFoundException();
        }
        return grafica01;
    }

    
    /**
     *
     * @param semana
     * @param fecha
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene pendientes semanales", notes = "Obtiene pendientes semanales", nickname = "obtienePendientesSemanales")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
        ,
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/pendientes-semanales", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PendientesSemanales obtieneGraficaSemanal(@ApiParam(name = "semana", value = "Semana de corte hacia atrás", example = "45") @RequestParam("semana") Integer semana,
            @ApiParam(name = "fechaActual", value = "Fecha actual de corte", example = "14/11/2021") @RequestParam("fechaActual") String fecha) throws CustomException, DataNotFoundException {
        PendientesSemanales pendientesSemanales = new PendientesSemanales();
        pendientesSemanales.setVisitasPendientesSemanales(sistemaInformacionVisitasPendientesGraficasDAOImpl.selectGrafica02(semana, fecha));
        if (pendientesSemanales == null) {
            throw new DataNotFoundException();
        }
        return pendientesSemanales;
    }

    /**
     *
     * @param numeroEmpleadoGerente
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene pendientes por gerente", notes = "Obtiene pendientes por gerente", nickname = "obtienePendientesGerente")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
        ,
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/gerentes/{numeroEmpleadoGerente}/certificaciones", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CertificacionesGerentes obtieneGraficaPorGerente(@ApiParam(name = "numeroEmpleadoGerente", value = "Número de empleado del gerente", example = "84539") @PathVariable("numeroEmpleadoGerente") Integer numeroEmpleadoGerente) throws CustomException, DataNotFoundException {
        CertificacionesGerentes certificacionesGerentes = new CertificacionesGerentes();
        certificacionesGerentes.setCertificacionesGerente(sistemaInformacionVisitasPendientesGraficasDAOImpl.selectGrafica03(numeroEmpleadoGerente));
        if (certificacionesGerentes == null) {
            throw new DataNotFoundException();
        }
        return certificacionesGerentes;
    }
    
    
    /**
     *
     * @param numeroEmpleadoGerente
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/gerentes/{numeroEmpleadoGerente}/pendientes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PendientesGerente obtieneCertificacionesPendientes(@ApiParam(name = "numeroEmpleadoGerente", value = "Número de empleado del gerente", example = "84539") @PathVariable("numeroEmpleadoGerente") Integer numeroEmpleadoGerente) throws CustomException, DataNotFoundException {
           PendientesGerente grafica04 = sistemaInformacionVisitasPendientesGraficasDAOImpl.selectGrafica04(numeroEmpleadoGerente);
        if (grafica04 == null) {
            throw new DataNotFoundException();
        }
        return grafica04;
    }

        
    /**
     *
     * @param numeroEmpleadoGerente
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/gerentes/{numeroEmpleadoGerente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Gerente obtieneDatosGerente(@ApiParam(name = "numeroEmpleadoGerente", value = "Número de empleado del gerente", example = "84539") @PathVariable("numeroEmpleadoGerente") Integer numeroEmpleadoGerente) throws CustomException, DataNotFoundException {
           Gerente grafica04 = sistemaInformacionVisitasPendientesGraficasDAOImpl.selectGraficaGerente04(numeroEmpleadoGerente);
        if (grafica04 == null) {
            throw new DataNotFoundException();
        }
        return grafica04;
    }

      /**
     *
     * @param numeroEmpleadoGerente
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene pendientes por gerente", notes = "Obtiene pendientes por gerente", nickname = "obtienePendientesGerente")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.")
        ,
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/gerentes/{numeroEmpleadoGerente}/puntos-venta", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PuntosVentaPendientes obtienePuntosVentaPendientes(@ApiParam(name = "numeroEmpleadoGerente", value = "Número de empleado del gerente", example = "84539") @PathVariable("numeroEmpleadoGerente") Integer numeroEmpleadoGerente) throws CustomException, DataNotFoundException {
        PuntosVentaPendientes puntosVentaPendientes = new PuntosVentaPendientes();
        puntosVentaPendientes.setPuntoVentaPendiente(sistemaInformacionVisitasPendientesGraficasDAOImpl.selectGrafica05(numeroEmpleadoGerente));
        if (puntosVentaPendientes == null) {
            throw new DataNotFoundException();
        }
        return puntosVentaPendientes;
    }
}
