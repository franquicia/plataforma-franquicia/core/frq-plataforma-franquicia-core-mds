/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.modelos.franquicia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProtocoloDisciplinaDTO {

    @JsonProperty(value = "id_protocolo_disciplina")
    private Integer idProtocoloDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina_negocio")
    private Integer idDisciplinaNegocio;

    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status")
    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public ProtocoloDisciplinaDTO() {
    }

    public Integer getIdProtocoloDisciplina() {
        return idProtocoloDisciplina;
    }

    public void setIdProtocoloDisciplina(BigDecimal idProtocoloDisciplina) {
        this.idProtocoloDisciplina = (idProtocoloDisciplina == null ? null : idProtocoloDisciplina.intValue());
    }

    public Integer getIdDisciplinaNegocio() {
        return idDisciplinaNegocio;
    }

    public void setIdDisciplinaNegocio(BigDecimal idDisciplinaNegocio) {
        this.idDisciplinaNegocio = (idDisciplinaNegocio == null ? null : idDisciplinaNegocio.intValue());
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(BigDecimal status) {
        this.status = (status == null ? null : status.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "ProtocoloDisciplinaDTO{" + "idProtocoloDisciplina=" + idProtocoloDisciplina + ", idDisciplinaNegocio=" + idDisciplinaNegocio + ", idProtocolo=" + idProtocolo + ", status=" + status + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
