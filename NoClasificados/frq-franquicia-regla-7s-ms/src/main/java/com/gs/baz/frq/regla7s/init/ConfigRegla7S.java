package com.gs.baz.frq.regla7s.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.regla7s.dao.Regla7SDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.regla7s")
public class ConfigRegla7S {

    private final Logger logger = LogManager.getLogger();

    public ConfigRegla7S() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "FRQreglas7SDAOImpl")
    public Regla7SDAOImpl regla7SDAOImpl() {
        return new Regla7SDAOImpl();
    }
}
