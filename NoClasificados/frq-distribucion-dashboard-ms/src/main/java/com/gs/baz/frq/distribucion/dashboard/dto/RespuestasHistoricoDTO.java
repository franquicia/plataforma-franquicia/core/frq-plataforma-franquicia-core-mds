package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Respuesta de Histórico", value = "RespuestaHistorico")
public class RespuestasHistoricoDTO {

    @JsonProperty(value = "respuestaEjecucion")
    @ApiModelProperty(notes = "Indicador de respuesta de la ejecución.", example = "1")
    private Integer respuestaEjecucion;

    @JsonProperty(value = "numeroRespuestas")
    @ApiModelProperty(notes = "Número de respuestas de histórico.", example = "10")
    private Integer numeroRespuestas;

    @JsonProperty(value = "envioCorreo")
    @ApiModelProperty(notes = "Indicador del envío de notificación por correo electrónico.", example = "true")
    private Boolean envioCorreo;

    public Integer getRespuestaEjecucion() {
        return respuestaEjecucion;
    }

    public void setRespuestaEjecucion(Integer respuestaEjecucion) {
        this.respuestaEjecucion = respuestaEjecucion;
    }

    public Integer getNumeroRespuestas() {
        return numeroRespuestas;
    }

    public void setNumeroRespuestas(Integer numeroRespuestas) {
        this.numeroRespuestas = numeroRespuestas;
    }

    public Boolean getEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(Boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    @Override
    public String toString() {
        return "RespuestasHistoricoDTO{" + "respuestaEjecucion=" + respuestaEjecucion + ", numeroRespuestas=" + numeroRespuestas + ", envioCorreo=" + envioCorreo + '}';
    }

}
