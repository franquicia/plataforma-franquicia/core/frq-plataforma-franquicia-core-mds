/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de Comentario de Firmas", value = "InsertaComentarioFirmas")
public class InsertaComentarioFirmas {


    @JsonProperty(value = "numeroEmpleadoFirmante")
    @ApiModelProperty(notes = "Identificador de", example = "1", position = -1)
    private Integer idFirmante;
    
    @JsonProperty(value = "comentario")
    @ApiModelProperty(notes = "", example = "1", position = -1)
    private String comentario;
    
    @JsonProperty(value = "fechaComentario")
    @ApiModelProperty(notes = "Fecha de creación del comentario", example = "", position = -1)
    private String fechaComentario;
    
    @JsonProperty(value = "auxiliarUno")
    @ApiModelProperty(notes = "Nombre del auxiliar uno", example = "1", position = -1)
    private String auxiliarUno;
    
     @JsonProperty(value = "auxiliarDos")
    @ApiModelProperty(notes = "Nombre del auxiliar dos", example = "1", position = -1)
    private String auxiliarDos;
     
     @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private Integer idFolio;

    public Integer getIdFirmante() {
        return idFirmante;
    }

    public void setIdFirmante(Integer idFirmante) {
        this.idFirmante = idFirmante;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(String fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    public String getAuxiliarUno() {
        return auxiliarUno;
    }

    public void setAuxiliarUno(String auxiliarUno) {
        this.auxiliarUno = auxiliarUno;
    }

    public String getAuxiliarDos() {
        return auxiliarDos;
    }

    public void setAuxiliarDos(String auxiliarDos) {
        this.auxiliarDos = auxiliarDos;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    @Override
    public String toString() {
        return "InsertaComentarioFirmas{" + "idFirmante=" + idFirmante + ", comentario=" + comentario + ", fechaComentario=" + fechaComentario + ", auxiliarUno=" + auxiliarUno + ", auxiliarDos=" + auxiliarDos + ", idFolio=" + idFolio + '}';
    }

}
