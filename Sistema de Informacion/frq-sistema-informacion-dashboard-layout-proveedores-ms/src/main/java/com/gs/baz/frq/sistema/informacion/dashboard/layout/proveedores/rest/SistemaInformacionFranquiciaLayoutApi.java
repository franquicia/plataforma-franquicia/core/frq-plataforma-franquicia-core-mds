/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionCecosLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.BandejasFranquicia;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.DashboardProveedores;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.Territorios;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "visitas pendientes", value = "visitas pendientes", description = "Api para la gestión de visitas pendientes")
@RestController
@RequestMapping("/api-local/sistema-informacion/dashboard/disenos/v1/franquicia")
public class SistemaInformacionFranquiciaLayoutApi {

    @Autowired
    private SistemaInformacionCecosLayoutDAOImpl sistemaInformacionCecosLayoutDAOImpl;

    
//estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/conteos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
     public DashboardProveedores getDashboardFranquicia() throws CustomException, DataNotFoundException {
        DashboardProveedores dashboardProveedores = new DashboardProveedores(sistemaInformacionCecosLayoutDAOImpl.selectDashboardFranquicia());
        if (dashboardProveedores == null || dashboardProveedores.getConteoProveedores().isEmpty()) {
            throw new DataNotFoundException();
        }
        return dashboardProveedores;
    }

    
     /**
     *
     * @param idProveedor
     * @param centroCostos
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/bandejas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BandejasFranquicia obtieneBandejas(@ApiParam(name = "idProveedor", value = "Identificador del proveedores", example = "123456") @RequestParam("idProveedor") Integer idProveedor,
            @ApiParam(name = "centroCostos", value = "Identificador del centro de costos", example = "920100") @RequestParam("centroCostos") Integer centroCostos) throws CustomException, DataNotFoundException {
        BandejasFranquicia bandejasFranquicia = new BandejasFranquicia(sistemaInformacionCecosLayoutDAOImpl.selectBandejasFranquicia(centroCostos,idProveedor));
        if (bandejasFranquicia == null || bandejasFranquicia.getBandejasFranquicia().isEmpty()) {
            throw new DataNotFoundException();
        }
        return bandejasFranquicia;
    }

      /**
     *
     * @param idTerritorio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/territorios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Territorios obtieneTerritorios() throws CustomException, DataNotFoundException {

        Territorios territorios = new Territorios(sistemaInformacionCecosLayoutDAOImpl.selectTerritorios());
        
        if (territorios == null || territorios.getTerritorios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return territorios;
    }
    
      /**
     *
     * @param idTerritorio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/territorios/{idTerritorio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Territorios obtieneTerritorio(@ApiParam(name = "Territorios", value = "Identificador del territorio", example = "920100") @PathVariable("idTerritorio") String idTerritorio) throws CustomException, DataNotFoundException {
        System.out.println("idTerritorio: "+ idTerritorio);
        Integer territorio;
            if(idTerritorio.equals("-1") || idTerritorio.isEmpty()){
          territorio = null;
        }else {
            territorio = Integer.getInteger(idTerritorio);
        }
            System.out.println("territorio: "+ territorio);
        Territorios territorios = new Territorios(sistemaInformacionCecosLayoutDAOImpl.selectTerritorio(territorio));
        
        if (territorios == null || territorios.getTerritorios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return territorios;
    }
}
