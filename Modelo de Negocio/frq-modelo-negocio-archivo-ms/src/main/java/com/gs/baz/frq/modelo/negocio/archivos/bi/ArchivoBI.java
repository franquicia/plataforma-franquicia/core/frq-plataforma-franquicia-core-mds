/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.bi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.archivos.dao.ArchivoDAOImpl;
import com.gs.baz.frq.modelo.negocio.archivos.dto.AltaArchivo;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoDirectorioBase;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoVirtual;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivosVirtuales;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ParametrosArchivo;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ParametrosArchivoAConvertir;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ParametrosArchivoVirtual;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author carlos
 */
@Component
public class ArchivoBI {

    @Autowired
    private ArchivoDAOImpl archivoDAOImpl;

    @Autowired
    private CreateWatermarkedPDF createWatermarkedPDF;

    private final RestTemplate restTemplate = new RestTemplate();

    private byte[] convertirArchivo(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, ParametrosArchivoAConvertir archivo) throws Exception {
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/utilerias/archivos/convertir/a/pdf";
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(archivo, httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.POST, httpEntityUsuario, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);
            String archivoPDF = objectNode.get("resultado").get("archivo").asText();
            byte[] fileArray = Base64.getDecoder().decode(archivoPDF);
            return fileArray;
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        } catch (IOException ex) {
            throw new Exception(ex);
        }
    }

    public AltaArchivo creaArchivo(HttpServletRequest httpServletRequest, HttpHeaders headers, ParametrosArchivo entityDTO) throws DataNotInsertedException {
        try {
            String rootPath = File.listRoots()[0].getAbsolutePath();
            Calendar cal = Calendar.getInstance();
            Long uId = cal.getTimeInMillis();
            String rutaArchivo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/";
            rutaArchivo = rutaArchivo + entityDTO.getIdFolio() + "/";
            String ruta = rootPath + rutaArchivo;
            File r = new File(ruta);
            if (!r.exists()) {
                r.mkdirs();
            }
            byte[] fileArrayOrignal = Base64.getDecoder().decode(entityDTO.getArchivo());
            String[] nameSplited = entityDTO.getNombre().split("\\.");
            String nameFileConverted;
            if (nameSplited.length > 0) {
                nameFileConverted = nameSplited[0] + ".pdf";
            } else {
                nameFileConverted = entityDTO.getNombre() + ".pdf";
            }
            String rutaTotal = ruta + uId + "[@-@]" + nameFileConverted;
            String rutaArchivoOriginal = ruta + uId + "-original[@-@]" + entityDTO.getNombre();
            if (entityDTO.getTipoContenido().contains("officedocument")) {
                byte[] fileArrayConverted = this.convertirArchivo(httpServletRequest, headers, new ParametrosArchivoAConvertir(entityDTO.getNombre(), entityDTO.getArchivo(), entityDTO.getTipoContenido()));
                File fileConverted = new File(rutaTotal);
                if (fileConverted.exists()) {
                    fileConverted.mkdir();
                }
                Files.write(fileConverted.toPath(), fileArrayConverted);
            } else {
                rutaArchivoOriginal = rutaTotal;
            }
            File fileOriginal = new File(rutaArchivoOriginal);
            if (fileOriginal.exists()) {
                fileOriginal.mkdir();
            }
            Files.write(fileOriginal.toPath(), fileArrayOrignal);
            return new AltaArchivo(uId);
        } catch (Exception ex) {
            throw new DataNotInsertedException();
        }
    }

    public ArchivoDirectorioBase consultaArchivo(Long idFolio, Long uid) throws CustomException {
        ArchivoDirectorioBase archivoBase = new ArchivoDirectorioBase();
        String rutaArchivo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/" + idFolio + "/";
        String nombreReal = encuentraNombreArchivo(rutaArchivo, "" + uid);
        if (!nombreReal.equals("")) {
            rutaArchivo = rutaArchivo + uid + "[@-@]" + nombreReal;
            try {
                Path path = Paths.get(rutaArchivo);
                URLConnection connection = path.toUri().toURL().openConnection();
                String mimeType = connection.getContentType();
                byte[] file = createWatermarkedPDF.addWatermark(path);
                String encodedArchivo = Base64.getEncoder().encodeToString(file);
                archivoBase.setArchivo("data:" + mimeType + ";base64," + encodedArchivo);
                archivoBase.setNombre(nombreReal);
                return archivoBase;
            } catch (IOException e) {
                rutaArchivo = null;
                return null;
            }
        } else {
            return null;
        }
    }

    public ArchivoDirectorioBase consultaArchivoOrignal(Long idFolio, Long uid) throws CustomException {
        ArchivoDirectorioBase archivoBase = new ArchivoDirectorioBase();
        String rutaArchivo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/" + idFolio + "/";
        String nombreReal = encuentraNombreArchivo(rutaArchivo, "" + uid + "-original");
        if (!nombreReal.equals("")) {
            rutaArchivo = rutaArchivo + uid + "-original[@-@]" + nombreReal;
            File fileTemp = new File(rutaArchivo);
            try {
                Path path = fileTemp.toPath();
                Detector detector = new DefaultDetector();
                Metadata md = new Metadata();
                md.add(Metadata.RESOURCE_NAME_KEY, fileTemp.getName());
                MediaType mediaType = detector.detect(TikaInputStream.get(path), md);
                String mimeType = mediaType.toString();
                byte[] file = Files.readAllBytes(path);
                String encodedArchivo = Base64.getEncoder().encodeToString(file);
                archivoBase.setArchivo("data:" + mimeType + ";base64," + encodedArchivo);
                archivoBase.setNombre(nombreReal);
                return archivoBase;
            } catch (IOException e) {
                return null;
            }
        } else {
            return this.consultaArchivo(idFolio, uid);
        }
    }

    public void eliminaArchivo(Long idFolio, Long uid) throws DataNotDeletedException {
        try {
            String rutaArchivo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/" + idFolio + "/";
            String nombreReal = encuentraNombreArchivo(rutaArchivo, "" + uid);
            if (!nombreReal.equals("")) {
                rutaArchivo = rutaArchivo + uid + "[@-@]" + nombreReal;
                if (rutaArchivo != null) {
                    File route = new File(rutaArchivo);
                    if (route.exists()) {
                        route.delete();
                    }
                }
            }
        } catch (Exception ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public void actualizaArchivo(HttpServletRequest httpServletRequest, HttpHeaders headers, ParametrosArchivo entityDTO) throws DataNotUpdatedException {
        try {
            String rutaArchivo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/" + entityDTO.getIdFolio() + "/";
            String nombreReal = encuentraNombreArchivo(rutaArchivo, "" + entityDTO.getUid());
            if (!nombreReal.equals("")) {
                rutaArchivo = rutaArchivo + entityDTO.getUid() + "-" + nombreReal;
                if (rutaArchivo != null) {
                    File route = new File(rutaArchivo);
                    if (route.exists()) {
                        route.delete();
                        this.creaArchivo(httpServletRequest, headers, entityDTO);
                    }
                }
            }
        } catch (Exception ex) {
            throw new DataNotUpdatedException();
        }
    }

    public AltaArchivo copiaArchivo(Long idFolio, Long idFolioNuevo, Long uid) throws CustomException, DataNotInsertedException {
        try {
            System.out.println("LLego a copiar archivos");
            ArchivoDirectorioBase archivoBase = new ArchivoDirectorioBase();
            String rutaArchivo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/" + idFolio + "/";
            String nombreReal = encuentraNombreArchivo(rutaArchivo, "" + uid);
            if (!nombreReal.equals("")) {
                try {

                    String rutaArchivoNuevo = "/franquicia/plataforma_franquicia/modelo-negocio/documentos/" + idFolioNuevo + "/";

                    Calendar cal = Calendar.getInstance();
                    Long uIdNuevo = cal.getTimeInMillis();

                    String fichero = uid + "[@-@]" + nombreReal;
                    String ficheroNuevo = uIdNuevo + "[@-@]" + nombreReal;
                    File ficheroCopiar = new File(rutaArchivo, fichero);
                    File ficheroDestino = new File(rutaArchivoNuevo, ficheroNuevo);
                    if (ficheroCopiar.exists()) {
                        String rootPath = File.listRoots()[0].getAbsolutePath();
                        String ruta = rootPath + rutaArchivoNuevo;
                        File r = new File(ruta);
                        if (!r.exists()) {
                            r.mkdirs();
                        }
                        Files.copy(Paths.get(ficheroCopiar.getAbsolutePath()), Paths.get(ficheroDestino.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
                    } else {
                        System.out.println("El fichero " + fichero + " no existe en el directorio " + rutaArchivo);
                    }
                    return new AltaArchivo(uIdNuevo);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                throw new DataNotInsertedException();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DataNotInsertedException();
        }

    }

    public String encuentraNombreArchivo(String ruta, String uid) {
        String retorno = "";
        String[] archivos = new File(ruta).list();
        for (String NombreArvhivo : archivos) {
            if (NombreArvhivo.split("\\[@-@\\]")[0].equals(uid)) {
                retorno = NombreArvhivo.trim().split("\\[@-@\\]")[1];
                break;
            }
        }
        return retorno;
    }

    public ArchivoDirectorioBase encuentraIcono(String ruta, Long uid) {
        String retorno = "";
        System.out.println("Llego al metodo de busqueda");
        String dir = "/franquicia/plataforma_franquicia/iconos/";
        String txt = "" + uid;
        //File archivo = encuentraArchivo2(new File(dir), txt, null);
        Filter f = new Filter();
        f.encuentraArchivo(new File(dir), txt);
        //System.out.println("Archivo clase: " + f.archivo.getAbsolutePath());
        File archivo = f.archivo;
        System.out.println(archivo.getAbsolutePath());
        String rutaReal = archivo.getAbsolutePath();
        String nombreReal = archivo.getName().split("\\[@-@\\]")[1];
        ArchivoDirectorioBase archivoBase = new ArchivoDirectorioBase();
        try {
            Path path = Paths.get(rutaReal);
            URLConnection connection = path.toUri().toURL().openConnection();
            String mimeType = connection.getContentType();
            byte[] file = Files.readAllBytes(path);
            String encodedArchivo = Base64.getEncoder().encodeToString(file);
            archivoBase.setArchivo("data:" + mimeType + ";base64," + encodedArchivo);
            archivoBase.setNombre(nombreReal);
            return archivoBase;
        } catch (IOException e) {
            nombreReal = null;
            return null;
        }

    }

    public ArchivoDirectorioBase encuentraArchivoGenerica(String ruta, Long uid) {
        String retorno = "";
        System.out.println("Llego al metodo de busqueda");
        String dir = "/franquicia/";
        String txt = "" + uid;
        //File archivo = encuentraArchivo2(new File(dir), txt, null);
        Filter f = new Filter();
        f.encuentraArchivo(new File(dir), txt);
        //System.out.println("Archivo clase: " + f.archivo.getAbsolutePath());
        File archivo = f.archivo;
        System.out.println(archivo.getAbsolutePath());
        String rutaReal = archivo.getAbsolutePath();
        ArchivoDirectorioBase archivoBase = new ArchivoDirectorioBase();
        try {
            Path path = Paths.get(rutaReal);
            URLConnection connection = path.toUri().toURL().openConnection();
            String mimeType = connection.getContentType();
            byte[] file = Files.readAllBytes(path);
            String encodedArchivo = Base64.getEncoder().encodeToString(file);
            archivoBase.setArchivo("data:" + mimeType + ";base64," + encodedArchivo);
            archivoBase.setNombre(rutaReal);
            return archivoBase;
        } catch (IOException e) {
            rutaReal = null;
            return null;
        }

    }

    public class Filter {

        File archivo = null;

        /**
         *
         * @param dir
         * @param uid
         * @param archivo
         * @return
         */
        public File encuentraArchivo(File dir, String uid) {
            File retorno = null;
            File listFile[] = dir.listFiles();
            if (retorno == null) {
                for (int i = 0; i < listFile.length; i++) {
                    if (listFile[i].isDirectory()) {
                        encuentraArchivo(listFile[i], uid);
                    } else {

                        //System.out.println(listFile[i].getPath());
                        if (listFile[i].getPath().toString().contains(uid)) {
                            System.out.println("Encontrado " + listFile[i].getPath());
                            retorno = listFile[i];
                            archivo = listFile[i];
                            return retorno;
                        }
                    }
                }
            }

            return retorno;
        }

    }

    public void duplicaArchivos(Long idFolio, Long idFolioNuevo) throws CustomException, DataNotInsertedException, DataNotUpdatedException {
        System.out.println("LLego a duplicar archivos");
        ArchivosVirtuales archivos = new ArchivosVirtuales(archivoDAOImpl.selectAllRows(idFolioNuevo));
        if (archivos.getArchivos() != null && !archivos.getArchivos().isEmpty()) {
            for (ArchivoVirtual archivo : archivos.getArchivos()) {
                AltaArchivo altaArchivoFisico = copiaArchivo(idFolio, idFolioNuevo, new Long("" + archivo.getUid()));
                ParametrosArchivoVirtual entityDTO = new ParametrosArchivoVirtual();
                entityDTO.setUid(altaArchivoFisico.getUid());
                entityDTO.setIdFolio(archivo.getIdFolio());
                entityDTO.setIdArchivo(archivo.getIdArchivo());
                /*entityDTO.setDescripcion(archivo.getDescripcion());
                 entityDTO.setExtension(archivo.getDescripcion());
                 entityDTO.setIndice(archivo.getIndice());
                 entityDTO.setNombre(archivo.getNombre());
                 entityDTO.setPeso(archivo.getPeso());
                 entityDTO.setTipo(archivo.getTipo());
                 entityDTO.setTipoContenido(archivo.getTipoContenido());*/
                archivoDAOImpl.updateRow(entityDTO);
            }
        }
    }

}
