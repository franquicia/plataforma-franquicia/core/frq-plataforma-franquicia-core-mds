/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g008;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class ModuloDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "row_number")
    private Integer rowNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "valor_seccion")
    private Integer valorSeccion;

    public ModuloDTO() {
    }

    public ModuloDTO(Integer rowNumber, String descripcion, Integer valorSeccion) {
        this.rowNumber = rowNumber;
        this.descripcion = descripcion;
        this.valorSeccion = valorSeccion;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getValorSeccion() {
        return valorSeccion;
    }

    public void setValorSeccion(Integer valorSeccion) {
        this.valorSeccion = valorSeccion;
    }

    @Override
    public String toString() {
        return "ModuloDTO{" + "rowNumber=" + rowNumber + ", descripcion=" + descripcion + ", valorSeccion=" + valorSeccion + '}';
    }

}
