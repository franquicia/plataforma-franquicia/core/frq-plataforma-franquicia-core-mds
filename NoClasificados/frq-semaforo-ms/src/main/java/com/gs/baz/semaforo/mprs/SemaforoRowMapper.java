package com.gs.baz.semaforo.mprs;

import com.gs.baz.semaforo.dto.SemaforoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SemaforoRowMapper implements RowMapper<SemaforoDTO> {

    private SemaforoDTO semaforo;

    @Override
    public SemaforoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        semaforo = new SemaforoDTO();
        semaforo.setIdSemaforo(((BigDecimal) rs.getObject("FIIDSEMA")));
        semaforo.setDescripcion(rs.getString("FCDESCRIPCION"));
        semaforo.setColor(rs.getString("FCCOLOR"));
        semaforo.setRango(rs.getString("FCRANGO"));
        return semaforo;
    }
}
