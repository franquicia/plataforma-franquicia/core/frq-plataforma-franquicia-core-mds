package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.ParametroDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ParametroRowMapper implements RowMapper<ParametroDTO> {

    public ParametroDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ParametroDTO parametroDTO = new ParametroDTO();

        parametroDTO.setCl(rs.getString("FCCVE_PARAM"));
        parametroDTO.setValor(rs.getString("FCVALOR"));
        parametroDTO.setActivo(rs.getInt("FIACTIVO"));

        return parametroDTO;
    }
}
