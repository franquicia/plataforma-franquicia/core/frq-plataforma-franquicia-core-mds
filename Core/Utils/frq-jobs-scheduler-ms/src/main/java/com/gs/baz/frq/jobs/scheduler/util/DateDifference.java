/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author cescobarh
 */
public class DateDifference {

    private final Date beginDate;
    private final Date endDate;

    public DateDifference(Date beginDate, Date endDate) {
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public ObjectNode getDiff() {
        Date d1 = this.beginDate;
        Date d2 = this.endDate;
        long difference_In_Time
                = d2.getTime() - d1.getTime();
        long difference_In_Seconds
                = TimeUnit.MILLISECONDS
                        .toSeconds(difference_In_Time)
                % 60;
        long difference_In_Minutes
                = TimeUnit.MILLISECONDS
                        .toMinutes(difference_In_Time)
                % 60;
        long difference_In_Hours
                = TimeUnit.MILLISECONDS
                        .toHours(difference_In_Time)
                % 24;
        long difference_In_Days
                = TimeUnit.MILLISECONDS
                        .toDays(difference_In_Time)
                % 365;
        long difference_In_Months
                = TimeUnit.MILLISECONDS
                        .toDays(difference_In_Time)
                % 12;
        long difference_In_Years
                = TimeUnit.MILLISECONDS
                        .toDays(difference_In_Time)
                / 365l;
        ObjectNode infoDiff = new ObjectMapper().createObjectNode();
        infoDiff.put("years", difference_In_Years);
        infoDiff.put("months", difference_In_Months);
        infoDiff.put("days", difference_In_Days);
        infoDiff.put("hours", difference_In_Hours);
        infoDiff.put("minutes", difference_In_Minutes);
        infoDiff.put("seconds", difference_In_Seconds);
        return infoDiff;
    }

}
