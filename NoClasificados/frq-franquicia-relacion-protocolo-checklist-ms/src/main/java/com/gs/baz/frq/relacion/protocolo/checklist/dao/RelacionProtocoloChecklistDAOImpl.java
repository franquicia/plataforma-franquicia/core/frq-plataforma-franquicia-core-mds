package com.gs.baz.frq.relacion.protocolo.checklist.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.relacion.protocolo.checklist.dao.util.GenericDAO;
import com.gs.baz.frq.relacion.protocolo.checklist.dto.RelacionProtocoloChecklistDTO;
import com.gs.baz.frq.relacion.protocolo.checklist.mprs.RelacionProtocoloChecklistRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class RelacionProtocoloChecklistDAOImpl extends DefaultDAO implements GenericDAO<RelacionProtocoloChecklistDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMCHECKXPRO");
        jdbcInsert.withProcedureName("SPINSCHECKXPROTC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMCHECKXPRO");
        jdbcUpdate.withProcedureName("SPACTCHECKXPROTC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMCHECKXPRO");
        jdbcDelete.withProcedureName("SPDELCHECKXPROTC");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMCHECKXPRO");
        jdbcSelect.withProcedureName("SPGETCHECKXPROTC");
        jdbcSelect.returningResultSet("PA_CDATOS", new RelacionProtocoloChecklistRowMapper());
    }

    @Override
    public List<RelacionProtocoloChecklistDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", null);
        mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<RelacionProtocoloChecklistDTO>) out.get("PA_CDATOS");
    }

    @Override
    public List<RelacionProtocoloChecklistDTO> selectRow(Long entityID, Long entityID2) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityID);
        mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", entityID2);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RelacionProtocoloChecklistDTO> data = (List<RelacionProtocoloChecklistDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public RelacionProtocoloChecklistDTO insertRow(RelacionProtocoloChecklistDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", entityDTO.getIdChecklist());
            mapSqlParameterSource.addValue("PA_FDFECHAINICIO", entityDTO.getFechaInicio());
            mapSqlParameterSource.addValue("PA_FIVIGENTE", entityDTO.getVigente());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of relacion protocolo checklist"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of relacion protocolo checklist"), ex);
        }
    }

    @Override
    public RelacionProtocoloChecklistDTO updateRow(RelacionProtocoloChecklistDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", entityDTO.getIdChecklist());
            mapSqlParameterSource.addValue("PA_FDFECHAINICIO", entityDTO.getFechaInicio());
            mapSqlParameterSource.addValue("PA_FIVIGENTE", entityDTO.getVigente());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of relacion protocolo checklist"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of relacion protocolo checklist"), ex);
        }
    }

    @Override
    public RelacionProtocoloChecklistDTO deleteRow(RelacionProtocoloChecklistDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIID_CHECKLIST", entityDTO.getIdChecklist());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Relacion Protocolo Checklist"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Relacion Protocolo Checklist"), ex);
        }
    }

}
