/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de grupos del usuario", value = "GruposUsuario")
public class GruposUsuario {

    @JsonProperty(value = "grupos")
    @ApiModelProperty(notes = "grupos")
    private List<GrupoUsuario> grupos;

    public GruposUsuario() {
    }

    public GruposUsuario(List<GrupoUsuario> grupos) {
        this.grupos = grupos;
    }

    public List<GrupoUsuario> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoUsuario> grupos) {
        this.grupos = grupos;
    }

    @Override
    public String toString() {
        return "GruposUsuario{" + "grupos=" + grupos + '}';
    }

}
