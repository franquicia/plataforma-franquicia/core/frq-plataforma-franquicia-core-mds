/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.categoria.dao.util;

import com.gs.baz.categoria.dto.CategoriaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public dto selectRow(Long entityID) throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<CategoriaDTO> selectRows() throws CustomException;

    /**
     *
     * @param entityID
     * @return @throws CustomException
     */
    public List<CategoriaDTO> selectRowsPorNegocio(Long entityID) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public Boolean deleteRow(Long entityID) throws CustomException;
}
