/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de bandejas del proveedor", value = "BandejasProveedores")
public class BandejasProveedores {

    @JsonProperty(value = "bandejasProveedores")
    @ApiModelProperty(notes = "bandejasProveedores")
    private List<BandejaProveedor> bandejaProveedor;

    public BandejasProveedores(List<BandejaProveedor> bandejaProveedor) {
        this.bandejaProveedor = bandejaProveedor;
    }

    public List<BandejaProveedor> getBandejaProveedor() {
        return bandejaProveedor;
    }

    public void setBandejaProveedor(List<BandejaProveedor> bandejaProveedor) {
        this.bandejaProveedor = bandejaProveedor;
    }

    @Override
    public String toString() {
        return "BandejasProveedores{" + "bandejaProveedor=" + bandejaProveedor + '}';
    }

}
