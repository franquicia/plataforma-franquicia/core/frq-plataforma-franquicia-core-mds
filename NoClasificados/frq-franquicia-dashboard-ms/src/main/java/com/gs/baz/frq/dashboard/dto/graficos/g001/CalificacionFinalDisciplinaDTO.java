/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g001;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class CalificacionFinalDisciplinaDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_final")
    private Integer calificacionFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getCalificacionFinal() {
        return calificacionFinal;
    }

    public void setCalificacionFinal(BigDecimal calificacionFinal) {
        this.calificacionFinal = (calificacionFinal == null ? null : calificacionFinal.intValue());
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(BigDecimal idCeco) {
        this.idCeco = (idCeco == null ? null : idCeco.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "CalificacionFinalDisciplinaDTO{" + "idDisciplina=" + idDisciplina + ", calificacionFinal=" + calificacionFinal + ", idCeco=" + idCeco + ", filtros=" + filtros + '}';
    }

}
