package com.gs.baz.frq.modelo.negocio.acceso.init;

import com.gs.baz.frq.modelo.negocio.acceso.dao.GrupoDAOImpl;
import com.gs.baz.frq.modelo.negocio.acceso.dao.UsuarioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.acceso")
public class ConfigModeloNegocioAcceso {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioAcceso() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean(initMethod = "init")
    public GrupoDAOImpl modeloNegocioAccesoGrupoDAOImpl() {
        return new GrupoDAOImpl();
    }

    @Bean(initMethod = "init")
    public UsuarioDAOImpl usuariosDAOImpl() {
        return new UsuarioDAOImpl();
    }

}
