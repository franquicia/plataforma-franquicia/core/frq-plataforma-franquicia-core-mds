/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.DashboardProveedor;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardProveedoresLayoutRowMapper implements RowMapper<DashboardProveedor> {

    @Override
    public DashboardProveedor mapRow(ResultSet rs, int rowNum) throws SQLException {
        DashboardProveedor dashboardProveedor = new DashboardProveedor();
        dashboardProveedor.setTotal(rs.getInt("TOTAL"));
        dashboardProveedor.setIdEstado(rs.getInt("FIESTADOCARGA"));
        dashboardProveedor.setNombreEstado(rs.getString("ESTADO"));
        return dashboardProveedor;
    }
}
