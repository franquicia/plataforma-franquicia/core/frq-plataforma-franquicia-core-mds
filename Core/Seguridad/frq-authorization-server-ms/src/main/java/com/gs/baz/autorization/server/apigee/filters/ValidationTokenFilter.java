/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorValidationClient;
import com.gs.baz.autorization.server.apigee.bi.ApigeeJWT;
import com.gs.baz.autorization.server.apigee.bi.ApigeeJWTException;
import com.gs.baz.frq.model.commons.Environment;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE - 15)
public class ValidationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private ApigeeJWT apigeeJWT;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
                String token = httpServletRequest.getHeader("token");
                if (token == null) {
                    throw new ApigeeJWTException("AuthorizationMissing");
                }
                String nameApi = (String) httpServletRequest.getAttribute("nameApi");
                String publicKey = PublicKey.PUBLIC_KEY.value(nameApi);
                apigeeJWT.validateJWTApigeeUser(token, "apigee", publicKey);
            } else if (Environment.CURRENT.valueReal().equals(Environment.QUALITY)) {
                String token = httpServletRequest.getHeader("token");
                if (token == null) {
                    throw new ApigeeJWTException("AuthorizationMissing");
                }
                String nameApi = (String) httpServletRequest.getAttribute("nameApi");
                String publicKey = PublicKey.PUBLIC_KEY.value(nameApi);
                apigeeJWT.validateJWTApigeeUser(token, "apigee", publicKey);
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (ApigeeJWTException ex) {
            ResponseApiCodes apiCode;
            if (ex.getMessage().equals("SignatureException")) {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04041;
            } else if (ex.getMessage().equals("MalformedJwtException")) {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04001;
            } else if (ex.getMessage().equals("ExpiredJwtException")) {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04011;
            } else if (ex.getMessage().equals("SecurityUtilsException")) {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_00000;
            } else if (ex.getMessage().equals("AuthorizationMissing")) {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04031;
            } else if (ex.getMessage().equals("NotUserMatchException")) {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04051;
            } else {
                apiCode = ResponseApiCodes.RESPONSE_MESSAGE_00000;
            }
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(401);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> detalles = new ArrayList<>();
            detalles.add(new ErrorDetalle(apiCode.description()));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, detalles);
            httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            OutputStream out = httpServletResponse.getOutputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, respuestaErrorClient);
            out.flush();
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        if (apigeeJWT == null) {
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            if (webApplicationContext != null) {
                apigeeJWT = webApplicationContext.getBean(ApigeeJWT.class);
            }
        }
        List<String> excludeUrlPatterns = new ArrayList<>();
        excludeUrlPatterns.add(".*/oauth/.*");
        excludeUrlPatterns.add(".*/status");
        excludeUrlPatterns.add(".*/cifra");
        excludeUrlPatterns.add(".*/descifra");
        return excludeUrlPatterns.stream().anyMatch(p -> Pattern.matches(p, request.getRequestURI()));
    }

}
