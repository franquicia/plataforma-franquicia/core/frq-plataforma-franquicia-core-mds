package com.gs.baz.frq.dashboard.certificacion.mprs.consulta.buscador;

import com.gs.baz.frq.dashboard.certificacion.dto.consulta.buscador.DashboardCertificacionConsultaBuscadorDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionGerenteConsultaBuscadorRowMapper implements RowMapper<DashboardCertificacionConsultaBuscadorDTO> {

    private DashboardCertificacionConsultaBuscadorDTO dashboardCertificacionConsultaBuscadorDTO;

    @Override
    public DashboardCertificacionConsultaBuscadorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionConsultaBuscadorDTO = new DashboardCertificacionConsultaBuscadorDTO();
        dashboardCertificacionConsultaBuscadorDTO.setIdGerente((BigDecimal) (rs.getObject("GERENTE_ID")));
        dashboardCertificacionConsultaBuscadorDTO.setNombreGerente(rs.getString("NOMBRE_GERENTE"));

        return dashboardCertificacionConsultaBuscadorDTO;
    }

    public class DashboardCertificacionCertificadorConsultaBuscadorRowMapper implements RowMapper<DashboardCertificacionConsultaBuscadorDTO> {

        private DashboardCertificacionConsultaBuscadorDTO dashboardCertificacionConsultaBuscadorDTO;

        @Override
        public DashboardCertificacionConsultaBuscadorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionConsultaBuscadorDTO = new DashboardCertificacionConsultaBuscadorDTO();
            dashboardCertificacionConsultaBuscadorDTO.setIdCertificador((BigDecimal) rs.getObject("CERTIFICADOR_ID"));
            dashboardCertificacionConsultaBuscadorDTO.setNombreCertificador(rs.getString("NOMBRE_CERTIFICADOR"));

            return dashboardCertificacionConsultaBuscadorDTO;
        }
    }

    public class DashboardCertificacionCecoConsultaBuscadorRowMapper implements RowMapper<DashboardCertificacionConsultaBuscadorDTO> {

        private DashboardCertificacionConsultaBuscadorDTO dashboardCertificacionConsultaBuscadorDTO;

        @Override
        public DashboardCertificacionConsultaBuscadorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionConsultaBuscadorDTO = new DashboardCertificacionConsultaBuscadorDTO();
            dashboardCertificacionConsultaBuscadorDTO.setIdCeco(rs.getString("FCID_CECO"));
            dashboardCertificacionConsultaBuscadorDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
            dashboardCertificacionConsultaBuscadorDTO.setPuntoContacto(rs.getString("PUNTO_CONTACTO"));
            dashboardCertificacionConsultaBuscadorDTO.setRegion(rs.getString("FCREGION"));
            dashboardCertificacionConsultaBuscadorDTO.setZona(rs.getString("FCZONA"));
            dashboardCertificacionConsultaBuscadorDTO.setTerritorio(rs.getString("FCTERRITORIO"));
            dashboardCertificacionConsultaBuscadorDTO.setDireccion(rs.getString("DIRECCION"));
            dashboardCertificacionConsultaBuscadorDTO.setIdGerente((BigDecimal) rs.getObject("FIGERENTEID"));
            dashboardCertificacionConsultaBuscadorDTO.setNombreGerente(rs.getString("GERENTE_CERTIFICACION"));
            return dashboardCertificacionConsultaBuscadorDTO;
        }
    }
}
