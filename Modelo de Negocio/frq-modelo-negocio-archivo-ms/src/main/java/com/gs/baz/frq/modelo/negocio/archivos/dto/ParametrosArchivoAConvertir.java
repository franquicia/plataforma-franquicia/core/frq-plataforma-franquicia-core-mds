/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.dto;

/**
 *
 * @author carlos
 */
public class ParametrosArchivoAConvertir {

    private String nombre;

    private String archivo;

    private String tipoContenido;

    public ParametrosArchivoAConvertir() {
    }

    public ParametrosArchivoAConvertir(String nombre, String archivo, String tipoContenido) {
        this.nombre = nombre;
        this.archivo = archivo;
        this.tipoContenido = tipoContenido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    @Override
    public String toString() {
        return "ParametrosArchivoAConvertir{" + "nombre=" + nombre + ", archivo=" + archivo + ", tipoContenido=" + tipoContenido + '}';
    }

}
