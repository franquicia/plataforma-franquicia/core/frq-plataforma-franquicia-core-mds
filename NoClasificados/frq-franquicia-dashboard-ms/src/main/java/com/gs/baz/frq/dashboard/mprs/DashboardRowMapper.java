package com.gs.baz.frq.dashboard.mprs;

import com.gs.baz.frq.dashboard.dto.DashboardDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardRowMapper implements RowMapper<DashboardDTO> {

    private DashboardDTO vistaPuntoContactoDTO;

    @Override
    public DashboardDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPuntoContactoDTO = new DashboardDTO();
        vistaPuntoContactoDTO.setIdDashboard((BigDecimal) rs.getObject("FIDASHBOARD_ID"));
        vistaPuntoContactoDTO.setIdConfiguracionDisciplina((BigDecimal) rs.getObject("FICONFIGDIS_ID"));
        vistaPuntoContactoDTO.setIdDisciplina((BigDecimal) rs.getObject("FIDISCIPLINA_ID"));
        vistaPuntoContactoDTO.setIdProotocolo((BigDecimal) rs.getObject("FIID_PROTOCOLO"));
        vistaPuntoContactoDTO.setIdCeco(rs.getString("FCID_CECO"));
        vistaPuntoContactoDTO.setDescripcion(rs.getString("FCNOMBRE"));
        vistaPuntoContactoDTO.setIdTerritorio(rs.getString("FCID_TERRITORIO"));
        vistaPuntoContactoDTO.setTerritorio(rs.getString("FCTERRITORIO"));
        vistaPuntoContactoDTO.setIdZona(rs.getString("FCID_ZONA"));
        vistaPuntoContactoDTO.setZona(rs.getString("FCZONA"));
        vistaPuntoContactoDTO.setIdRegion(rs.getString("FCID_REGION"));
        vistaPuntoContactoDTO.setRegion(rs.getString("FCREGION"));
        vistaPuntoContactoDTO.setIdNegocioDisciplina((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        vistaPuntoContactoDTO.setCalificacion((BigDecimal) rs.getObject("FICALIFICACION"));
        vistaPuntoContactoDTO.setNumeroImperdonable((BigDecimal) rs.getObject("FINUM_IMPERDON"));
        vistaPuntoContactoDTO.setNumeroIncumplimiento((BigDecimal) rs.getObject("FINUM_INCUMPLM"));
        vistaPuntoContactoDTO.setFechaTermino(rs.getString("FDTERMINO"));
        vistaPuntoContactoDTO.setPuntosObtenidos((BigDecimal) rs.getObject("FIPUNTOSOBT"));
        vistaPuntoContactoDTO.setPuntosEsperados((BigDecimal) rs.getObject("FIPUNTOSESP"));
        vistaPuntoContactoDTO.setPuntosObtenidos((BigDecimal) rs.getObject("FIPUNTOSOBT"));
        vistaPuntoContactoDTO.setPuntosEsperados((BigDecimal) rs.getObject("FIPUNTOSESP"));
        vistaPuntoContactoDTO.setIdEfectos((BigDecimal) rs.getObject("FIDEFECTOS"));
        vistaPuntoContactoDTO.setOportunidades((BigDecimal) rs.getObject("FIOPORTUNIDADES"));
        vistaPuntoContactoDTO.setIdCecoNegocio(rs.getString("FCCECONEG_ID"));
        vistaPuntoContactoDTO.setIdUsuario((BigDecimal) rs.getObject("FIID_USUARIO"));
        vistaPuntoContactoDTO.setNombreUsuario(rs.getString("FCNOMBREUSR"));
        vistaPuntoContactoDTO.setIdPuesto((BigDecimal) rs.getObject("FIID_PUESTO"));
        vistaPuntoContactoDTO.setPrecalificacion((BigDecimal) rs.getObject("FIPRECALIFICA"));
        vistaPuntoContactoDTO.setCalificacionLineal((BigDecimal) rs.getObject("FICALIF_LINEAL"));

        return vistaPuntoContactoDTO;
    }
}
