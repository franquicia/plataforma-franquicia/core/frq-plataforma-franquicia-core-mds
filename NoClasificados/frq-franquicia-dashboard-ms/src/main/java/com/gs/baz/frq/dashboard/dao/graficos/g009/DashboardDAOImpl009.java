package com.gs.baz.frq.dashboard.dao.graficos.g009;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g009.CumplimientoCalificacionDisciplinaPuntoContactoDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g009.CumplimientoCalificacionDisciplinaPuntoContactoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl009 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCumplimientoPuntajeGeneral;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCumplimientoPuntajeGeneral = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoPuntajeGeneral.withSchemaName(schema);
        jdbcSelectCumplimientoPuntajeGeneral.withCatalogName("PAREPOCALIFPTOC");
        jdbcSelectCumplimientoPuntajeGeneral.withProcedureName("SPCALXPTOCON");
        jdbcSelectCumplimientoPuntajeGeneral.returningResultSet("RCL_INFO", new CumplimientoCalificacionDisciplinaPuntoContactoRowMapper());
    }

    public List<CumplimientoCalificacionDisciplinaPuntoContactoDTO> selectRowsCumplimientoCalificacionDisciplinaPuntoContacto(CumplimientoCalificacionDisciplinaPuntoContactoDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoPuntajeGeneral.execute(mapSqlParameterSource);
        return (List<CumplimientoCalificacionDisciplinaPuntoContactoDTO>) out.get("RCL_INFO");
    }

}
