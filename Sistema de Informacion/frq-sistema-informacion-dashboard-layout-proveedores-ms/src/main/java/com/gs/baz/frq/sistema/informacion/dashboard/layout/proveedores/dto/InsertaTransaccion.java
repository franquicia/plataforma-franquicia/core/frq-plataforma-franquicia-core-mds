/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de Transaccion", value = "InsertaTransaccion")
public class InsertaTransaccion {


    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private Integer idFolio;
    
    @JsonProperty(value = "fecha")
    @ApiModelProperty(notes = "Fecha de la transacción", example = "1", position = -1)
    private String fecha;
    
    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Nombre del auxiliar uno", example = "", position = -1)
    private Integer tipo;
    
     @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripción del comentario", example = "", position = -1)
    private String descripcion;
     
    @JsonProperty(value = "auxiliarUno")
    @ApiModelProperty(notes = "Nombre del auxiliar uno", example = "", position = -1)
    private String auxiliarUno;
    
     @JsonProperty(value = "auxiliarDos")
    @ApiModelProperty(notes = "Nombre del auxiliar dos", example = "", position = -1)
    private String auxiliarDos;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAuxiliarUno() {
        return auxiliarUno;
    }

    public void setAuxiliarUno(String auxiliarUno) {
        this.auxiliarUno = auxiliarUno;
    }

    public String getAuxiliarDos() {
        return auxiliarDos;
    }

    public void setAuxiliarDos(String auxiliarDos) {
        this.auxiliarDos = auxiliarDos;
    }

    @Override
    public String toString() {
        return "InsertaTransaccion{" + "idFolio=" + idFolio + ", fecha=" + fecha + ", tipo=" + tipo + ", descripcion=" + descripcion + ", auxiliarUno=" + auxiliarUno + ", auxiliarDos=" + auxiliarDos + '}';
    }
}
