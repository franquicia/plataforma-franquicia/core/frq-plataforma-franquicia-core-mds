/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

/**
 *
 * @author cescobarh
 */
public class ClientSocketLogs {

    private final Logger logger = LogManager.getLogger();

    static public class CustomStompSessionHandler extends StompSessionHandlerAdapter {

        private void showHeaders(StompHeaders headers) {
            System.out.println("showHeaders");
            headers.entrySet().stream().map((e) -> {
                System.out.print("  " + e.getKey() + ": ");
                return e;
            }).map((e) -> {
                boolean first = true;
                for (String v : e.getValue()) {
                    if (!first) {
                        System.out.print(", ");
                    }
                    System.out.print(v);
                    first = false;
                }
                return e;
            }).forEach(System.out::println);
        }

        private void subscribeTopic(String topic, StompSession session) {
            session.subscribe(topic, new StompFrameHandler() {
                @Override
                public Type getPayloadType(StompHeaders headers) {
                    return ObjectNode.class;
                }

                @Override
                public void handleFrame(StompHeaders headers, Object payload) {
                    //System.out.println(payload.toString());
                }
            });
        }

        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            System.out.println("Connected! Headers:");
            showHeaders(connectedHeaders);
            //subscribeTopic("/topic/logs", session);
        }
    }

    public static void main(String args[]) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode;
        WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(simpleWebSocketClient));

        SockJsClient sockJsClient = new SockJsClient(transports);
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        String url = "ws://localhost:8080/plataforma-franquicia-core/websocket/";
        String userId = "spring-" + ThreadLocalRandom.current().nextInt(1, 99);

        StompSessionHandler sessionHandler = new CustomStompSessionHandler();
        StompSession session = stompClient.connect(url, sessionHandler).get(2, TimeUnit.SECONDS);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        objectNode = objectMapper.createObjectNode();
        objectNode.put("line_console", "holaa");
        session.send("/app/logs", objectNode);

        for (;;) {
            //System.out.print(userId + " >> ");
            //System.out.flush();
            String line = in.readLine();
            if (line == null) {
                break;
            }
            if (line.length() == 0) {
                continue;
            }
            objectNode = objectMapper.createObjectNode();
            objectNode.put("line_console", line);
            session.send("/app/logs", objectNode);
        }
    }
}
