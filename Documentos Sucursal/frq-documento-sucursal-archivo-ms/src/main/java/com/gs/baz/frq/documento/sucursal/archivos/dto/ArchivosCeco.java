package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

@ApiModel(description = "Datos de Archivos del ceco", value = "ArchivosCeco")
public class ArchivosCeco {

    @JsonProperty(value = "archivosCentroCosto")
    @ApiModelProperty(notes = "Identificador del ceco", example = "1")
    private List<ArchivoCeco> archivos;

    public ArchivosCeco(List<ArchivoCeco> archivos) {

        this.archivos = archivos;

    }

    public List<ArchivoCeco> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<ArchivoCeco> archivos) {
        this.archivos = archivos;
    }

}
