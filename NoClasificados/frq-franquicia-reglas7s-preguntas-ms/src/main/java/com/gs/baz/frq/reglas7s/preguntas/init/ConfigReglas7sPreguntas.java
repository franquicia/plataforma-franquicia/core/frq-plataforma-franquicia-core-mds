package com.gs.baz.frq.reglas7s.preguntas.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.reglas7s.preguntas.dao.Reglas7sPreguntasDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.reglas7s.preguntas")
public class ConfigReglas7sPreguntas {

    private final Logger logger = LogManager.getLogger();

    public ConfigReglas7sPreguntas() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "franquiciaConfiguracionDisciplinaDAOImpl")
    public Reglas7sPreguntasDAOImpl configuracionDisciplinaDAOImpl() {
        return new Reglas7sPreguntasDAOImpl();
    }
}
