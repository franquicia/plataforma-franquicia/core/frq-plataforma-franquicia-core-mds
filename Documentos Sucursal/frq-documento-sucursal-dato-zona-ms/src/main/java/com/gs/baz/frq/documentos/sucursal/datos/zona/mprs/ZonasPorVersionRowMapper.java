/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorVersion;

/**
 *
 * @author carlos
 */
public class ZonasPorVersionRowMapper implements RowMapper<ZonasPorVersion> {

    private ZonasPorVersion zonasPorVersion;

    @Override
    public ZonasPorVersion mapRow(ResultSet rs, int rowNum) throws SQLException {
        zonasPorVersion = new ZonasPorVersion();
        zonasPorVersion.setIdDatoZona(rs.getInt("FIIDDATOSZONA"));
        zonasPorVersion.setIdZona(rs.getInt("FIIDLYZONAS"));
        zonasPorVersion.setValorMetros(rs.getString("FCVALOR"));
        zonasPorVersion.setObservaciones(rs.getString("FCOBSERVACIONES"));
        //zonasPorVersion.setIdVersion(rs.getInt("FIID_VERSION"));
        zonasPorVersion.setDescripcion(rs.getString("FCDESCRIPCION"));
        zonasPorVersion.setIdTipoInformacion(rs.getInt("FIIDTIPOINFOR"));
        zonasPorVersion.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        zonasPorVersion.setNombreProyecto(rs.getString("NOMPROY"));

        return zonasPorVersion;
    }
}
