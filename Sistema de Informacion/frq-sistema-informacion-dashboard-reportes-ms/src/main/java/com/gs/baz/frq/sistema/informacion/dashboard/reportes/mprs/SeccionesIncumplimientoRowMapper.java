/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.SeccionIncumplimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SeccionesIncumplimientoRowMapper implements RowMapper<SeccionIncumplimiento> {

    @Override
    public SeccionIncumplimiento mapRow(ResultSet rs, int rowNum) throws SQLException {
        SeccionIncumplimiento seccionIncumplimiento = new SeccionIncumplimiento();
        seccionIncumplimiento.setIdSeccion(rs.getInt("FIID_MODULO"));
        seccionIncumplimiento.setDescripcion(rs.getString("FCMODULO"));
        return seccionIncumplimiento;
    }
}
