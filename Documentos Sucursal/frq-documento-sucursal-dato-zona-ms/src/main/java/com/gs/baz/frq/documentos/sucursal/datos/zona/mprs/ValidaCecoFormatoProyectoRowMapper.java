/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ValidaCecoFormatoProyecto;

/**
 *
 * @author carlos
 */
public class ValidaCecoFormatoProyectoRowMapper implements RowMapper<ValidaCecoFormatoProyecto> {

    private ValidaCecoFormatoProyecto validaCecoFormatoProyecto;

    @Override
    public ValidaCecoFormatoProyecto mapRow(ResultSet rs, int rowNum) throws SQLException {
        validaCecoFormatoProyecto = new ValidaCecoFormatoProyecto();
        validaCecoFormatoProyecto.setIdEstatusEdicion(rs.getInt("FISTATUS_EDICION"));

        return validaCecoFormatoProyecto;
    }
}
