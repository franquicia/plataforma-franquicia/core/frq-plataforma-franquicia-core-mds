/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.vavr.collection.List;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Detalle del Incidente", value = "RespuestaDetalleIncidente")
public class DetalleIncidenteResponseDTO {

    @JsonProperty(value = "imagenIncidente")
    @ApiModelProperty(notes = "Imagen en Base64", example = "**Cadena de caracteres en Base64**")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String imagen;

    @JsonProperty(value = "seguimiento")
    @ApiModelProperty(notes = "Arreglo de datos del detalle seguimiento del incidente")
    private List<TrackingDetalleDTO> tracking;

    public DetalleIncidenteResponseDTO() {
        super();
    }

    public List<TrackingDetalleDTO> getTracking() {
        return tracking;
    }

    public void setTracking(List<TrackingDetalleDTO> tracking) {
        this.tracking = tracking;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

}
