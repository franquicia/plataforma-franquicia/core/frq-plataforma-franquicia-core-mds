package com.gs.baz.frq.secciones.protocolo.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.secciones.protocolo.dao.SeccionesProtocoloDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.secciones.protocolo")
public class ConfigSeccionesProtocolo {

    private final Logger logger = LogManager.getLogger();

    public ConfigSeccionesProtocolo() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "FRQSeccionesProtocoloDAOImpl")
    public SeccionesProtocoloDAOImpl seccionesProtocoloDAOImpl() {
        return new SeccionesProtocoloDAOImpl();
    }
}
