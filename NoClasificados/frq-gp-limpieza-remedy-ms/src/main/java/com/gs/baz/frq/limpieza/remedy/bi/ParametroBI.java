package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.ParametroDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.ParametroDTO;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ParametroBI {

    @Autowired
    private ParametroDAOImpl parametroDAO;

    private static final Logger LOGGER = LogManager.getLogger();

    public ParametroBI() {
        super();
    }

    public List<ParametroDTO> obtieneParametro() {
        List<ParametroDTO> listaParametros = null;
        try {
            listaParametros = parametroDAO.obtieneParametro();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla Parametros: ", e);
        }

        return listaParametros;
    }

    public List<ParametroDTO> obtieneParametros(String clave) {
        List<ParametroDTO> listaParametro = null;
        try {
            listaParametro = parametroDAO.obtieneParametro(clave);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla Parametro: ", e);
        }

        return listaParametro;
    }

    public boolean insertaPrametros(ParametroDTO parametro) {
        boolean respuesta = false;
        try {
            respuesta = parametroDAO.insertaParametro(parametro);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible insertar el Parametro: ", e);
        }

        return respuesta;
    }

    public boolean actualizaParametro(ParametroDTO parametro) {
        boolean respuesta = false;
        try {
            respuesta = parametroDAO.actualizaParametro(parametro);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible actualizar el Parametro: ", e);
        }

        return respuesta;
    }

    public boolean eliminaParametro(String clave) {
        boolean respuesta = false;
        try {
            respuesta = parametroDAO.eliminaParametros(clave);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible borrar el Parametro: ", e);
        }

        return respuesta;
    }
}
