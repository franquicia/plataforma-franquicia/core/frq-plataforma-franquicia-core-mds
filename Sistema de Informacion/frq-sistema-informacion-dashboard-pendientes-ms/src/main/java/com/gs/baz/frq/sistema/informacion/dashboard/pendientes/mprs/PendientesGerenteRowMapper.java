/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CertificacionesGerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesSemanal;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PendientesGerenteRowMapper implements RowMapper<CertificacionesGerente> {

    @Override
    public CertificacionesGerente mapRow(ResultSet rs, int rowNum) throws SQLException {
        CertificacionesGerente certificacionesGerente = new CertificacionesGerente();
        certificacionesGerente.setNumeroEmpleadoGerente(rs.getInt("FIGERENTEID"));
        certificacionesGerente.setNombreGerente(rs.getString("NOMBRE"));
        certificacionesGerente.setTerritorio(rs.getString("FCTERRITORIO"));
        certificacionesGerente.setTotalAsignados(rs.getInt("TOTAL_ASIGNADOS"));
        certificacionesGerente.setTotalPendientes(rs.getInt("TOTAL_PENDIENTES"));
        return certificacionesGerente;
    }
}
