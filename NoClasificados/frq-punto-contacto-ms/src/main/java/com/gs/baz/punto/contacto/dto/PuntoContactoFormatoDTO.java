/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoFormatoDTO {

    @JsonProperty(value = "id_formato")
    private Integer idFormato;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_punto_contacto_franquicia_modelo")
    private Integer idPuntoContactoFranquiciaModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_tipo_formato")
    private Integer idTipoFormato;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public PuntoContactoFormatoDTO(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public PuntoContactoFormatoDTO() {
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(BigDecimal idFormato) {
        this.idFormato = (idFormato == null ? null : idFormato.intValue());
    }

    public Integer getIdPuntoContactoFranquiciaModelo() {
        return idPuntoContactoFranquiciaModelo;
    }

    public void setIdPuntoContactoFranquiciaModelo(BigDecimal idPuntoContactoFranquiciaModelo) {
        this.idPuntoContactoFranquiciaModelo = (idPuntoContactoFranquiciaModelo == null ? null : idPuntoContactoFranquiciaModelo.intValue());
    }

    public Integer getIdTipoFormato() {
        return idTipoFormato;
    }

    public void setIdTipoFormato(BigDecimal idTipoFormato) {
        this.idTipoFormato = (idTipoFormato == null ? null : idTipoFormato.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "PuntoContactoFormatoDTO{" + "idFormato=" + idFormato + ", idPuntoContactoFranquiciaModelo=" + idPuntoContactoFranquiciaModelo + ", idTipoFormato=" + idTipoFormato + ", fecha=" + fecha + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
