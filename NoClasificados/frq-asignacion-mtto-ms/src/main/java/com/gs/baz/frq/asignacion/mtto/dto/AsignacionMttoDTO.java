/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AsignacionMttoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "archivoAsignaciones")
    private String archivoAsignaciones;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "eco")
    private Integer numeroEconomico;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreSucursal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipoVisita")
    private String tipoVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numeroEmpleado")
    private Integer numeroEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreCertificador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numeroGerente")
    private Integer numeroGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreGerente")
    private String nombreGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "usuarioAsigna")
    private int usuarioAsigna;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idFase")
    private int idFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idProyecto")
    private int idProyecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idAgrupa")
    private int idAgrupa;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "week_n")
    private String week_n;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaProgramada")
    private String fechaProgramada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ordenActivos")
    private String ordenActivos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "comentarioTicketRelacionados")
    private String comentarioTicketRelacionados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "comentarioReprogramacion")
    private String comentarioReprogramacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idProgramacion")
    private int idProgramacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaProgramacionAntigua")
    private String fechaProgramacionAntigua;

    public int getIdProgramacion() {
        return idProgramacion;
    }

    public void setIdProgramacion(int idProgramacion) {
        this.idProgramacion = idProgramacion;
    }

    public String getFechaProgramacionAntigua() {
        return fechaProgramacionAntigua;
    }

    public void setFechaProgramacionAntigua(String fechaProgramacionAntigua) {
        this.fechaProgramacionAntigua = fechaProgramacionAntigua;
    }

    public String getOrdenActivos() {
        return ordenActivos;
    }

    public void setOrdenActivos(String ordenActivos) {
        this.ordenActivos = ordenActivos;
    }

    public String getComentarioTicketRelacionados() {
        return comentarioTicketRelacionados;
    }

    public void setComentarioTicketRelacionados(String comentarioTicketRelacionados) {
        this.comentarioTicketRelacionados = comentarioTicketRelacionados;
    }

    public String getComentarioReprogramacion() {
        return comentarioReprogramacion;
    }

    public void setComentarioReprogramacion(String comentarioReprogramacion) {
        this.comentarioReprogramacion = comentarioReprogramacion;
    }

    public String getFechaProgramada() {
        return fechaProgramada;
    }

    public void setFechaProgramada(String fechaProgramada) {
        this.fechaProgramada = fechaProgramada;
    }

    public String getWeek_n() {
        return week_n;
    }

    public void setWeek_n(String week_n) {
        this.week_n = week_n;
    }

    public int getIdAgrupa() {
        return idAgrupa;
    }

    public void setIdAgrupa(int idAgrupa) {
        this.idAgrupa = idAgrupa;
    }

    public int getUsuarioAsigna() {
        return usuarioAsigna;
    }

    public void setUsuarioAsigna(int usuarioAsigna) {
        this.usuarioAsigna = usuarioAsigna;
    }

    public int getIdFase() {
        return idFase;
    }

    public void setIdFase(int idFase) {
        this.idFase = idFase;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getArchivoAsignaciones() {
        return archivoAsignaciones;
    }

    public void setArchivoAsignaciones(String archivoAsignaciones) {
        this.archivoAsignaciones = archivoAsignaciones;
    }

    public Integer getNumeroEconomico() {
        return numeroEconomico;
    }

    public void setNumeroEconomico(Integer numeroEconomico) {
        this.numeroEconomico = numeroEconomico;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getTipoVisita() {
        return tipoVisita;
    }

    public void setTipoVisita(String tipoVisita) {
        this.tipoVisita = tipoVisita;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombreCertificador() {
        return nombreCertificador;
    }

    public void setNombreCertificador(String nombreCertificador) {
        this.nombreCertificador = nombreCertificador;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getNumeroGerente() {
        return numeroGerente;
    }

    public void setNumeroGerente(Integer numeroGerente) {
        this.numeroGerente = numeroGerente;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Integer inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    //nuevos eliminar lo anterior
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status")
    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCheckList")
    private Integer idCheckList;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idVersion")
    private Integer idVersion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idNegocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "parametro")
    private Integer parametro;
    //tabla version Supervision

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idGerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idPerfil")
    private Integer idPerfil;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsu")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreSucursal")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "econom")
    private String eco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio")
    private String negocio;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getEco() {
        return eco;
    }

    public void setEco(String eco) {
        this.eco = eco;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(Integer idCheckList) {
        this.idCheckList = idCheckList;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public Integer getInserted() {
        return inserted;
    }

    public void setInserted(Integer inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getParametro() {
        return parametro;
    }

    public void setParametro(Integer parametro) {
        this.parametro = parametro;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(Integer idGerente) {
        this.idGerente = idGerente;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    @Override
    public String toString() {
        return "AsignacionMttoDTO [archivoAsignaciones=" + archivoAsignaciones + ", numeroEconomico="
                + numeroEconomico + ", nombreSucursal=" + nombreSucursal + ", tipoVisita=" + tipoVisita
                + ", numeroEmpleado=" + numeroEmpleado + ", nombreCertificador=" + nombreCertificador + ", zona="
                + zona + ", numeroGerente=" + numeroGerente + ", nombreGerente=" + nombreGerente
                + ", usuarioAsigna=" + usuarioAsigna + ", idFase=" + idFase + ", idProyecto=" + idProyecto
                + ", idAgrupa=" + idAgrupa + ", week_n=" + week_n + ", fechaProgramada=" + fechaProgramada
                + ", ordenActivos=" + ordenActivos + ", comentarioTicketRelacionados="
                + comentarioTicketRelacionados + ", comentarioReprogramacion=" + comentarioReprogramacion
                + ", idProgramacion=" + idProgramacion + ", fechaProgramacionAntigua=" + fechaProgramacionAntigua
                + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + ", periodo="
                + periodo + ", fecha=" + fecha + ", descripcion=" + descripcion + ", status=" + status
                + ", idCheckList=" + idCheckList + ", idVersion=" + idVersion + ", idNegocio=" + idNegocio
                + ", parametro=" + parametro + ", idUsuario=" + idUsuario + ", idGerente=" + idGerente + ", idCeco="
                + idCeco + ", idPerfil=" + idPerfil + ", nombreUsuario=" + nombreUsuario + ", nombreCeco="
                + nombreCeco + ", eco=" + eco + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
                + ", negocio=" + negocio + "]";
    }

}
