/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class MakeDirectoryException extends Exception{

    public MakeDirectoryException() {
    }

    public MakeDirectoryException(String string) {
        super(string);
    }

    public MakeDirectoryException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public MakeDirectoryException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
