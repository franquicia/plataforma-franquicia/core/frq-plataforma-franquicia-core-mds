package com.gs.baz.frq.dashboard.mprs.graficos.g005;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoDisciplinaNegocioRowMapper implements RowMapper<CumplimientoDisciplinaNegocioDTO> {

    private CumplimientoDisciplinaNegocioDTO cumplimientoNacionalNegocioDTO;

    @Override
    public CumplimientoDisciplinaNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoNacionalNegocioDTO = new CumplimientoDisciplinaNegocioDTO();
        cumplimientoNacionalNegocioDTO.setIdNegocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        cumplimientoNacionalNegocioDTO.setPorcentaje((BigDecimal) rs.getObject("PORCENTAJE"));
        cumplimientoNacionalNegocioDTO.setIdRango((BigDecimal) rs.getObject("RANGOID"));
        cumplimientoNacionalNegocioDTO.setTotalEvaluadas((BigDecimal) rs.getObject("TOTALEVALUADAS"));
        return cumplimientoNacionalNegocioDTO;
    }
}
