package com.gs.baz.perfil.mprs;

import com.gs.baz.perfil.dto.PerfilDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PerfilRowMapper implements RowMapper<PerfilDTO> {

    private PerfilDTO perfil;

    @Override
    public PerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        perfil = new PerfilDTO();
        perfil.setIdPerfil(((BigDecimal) rs.getObject("FIID_PERFIL")));
        perfil.setDescripcion(rs.getString("FCDESCRIPCION"));
        perfil.setIdEmpleado(((BigDecimal) rs.getObject("FIID_EMPLEADO")));
        perfil.setEscritura((BigDecimal) rs.getObject("FIESCRITURA"));
        perfil.setFechaCreacion(rs.getString("FDFECHACREACION"));
        return perfil;
    }
}
