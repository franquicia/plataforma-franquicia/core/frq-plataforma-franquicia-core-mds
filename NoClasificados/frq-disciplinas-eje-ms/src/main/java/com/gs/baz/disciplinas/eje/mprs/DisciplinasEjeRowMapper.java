package com.gs.baz.disciplinas.eje.mprs;

import com.gs.baz.disciplinas.eje.dto.DisciplinaEjeDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DisciplinasEjeRowMapper implements RowMapper<DisciplinaEjeDTO> {

    private DisciplinaEjeDTO disciplinaEje;

    @Override
    public DisciplinaEjeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        disciplinaEje = new DisciplinaEjeDTO();
        disciplinaEje.setIdDisciplinaEje(((BigDecimal) rs.getObject("FIIDEJEDIS")));
        disciplinaEje.setIdModeloEje(((BigDecimal) rs.getObject("FIIDMODEJE")));
        disciplinaEje.setIdDisciplina(((BigDecimal) rs.getObject("FIIDDISC")));
        disciplinaEje.setIdStatus(((BigDecimal) rs.getObject("FIIDSTATUS")));
        disciplinaEje.setIdTipoMedicion(((BigDecimal) rs.getObject("FIIDTPMEDICION")));
        disciplinaEje.setIdFrecuenciaMedicion(((BigDecimal) rs.getObject("FIIDFRECMED")));
        disciplinaEje.setIdEjecuta(((BigDecimal) rs.getObject("FIIDEJECUTA")));
        disciplinaEje.setDriverExperiencia(((BigDecimal) rs.getObject("FBEXPERIENCIA")));
        disciplinaEje.setPonderacion(((BigDecimal) rs.getObject("FIPONDERACION")));
        return disciplinaEje;
    }
}
