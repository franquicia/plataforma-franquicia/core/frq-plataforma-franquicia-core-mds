/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.json.converter.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class ExcelFileDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "name_file")
    private String nameFile;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "autor")
    private String autor;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "metadata")
    private MetadataDTO metadata;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "sheets")
    private List<SheetDTO> sheets;

    public ExcelFileDTO() {
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public MetadataDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(MetadataDTO metadata) {
        this.metadata = metadata;
    }

    public List<SheetDTO> getSheets() {
        return sheets;
    }

    public void setSheets(List<SheetDTO> sheets) {
        this.sheets = sheets;
    }

    public void addSheet(SheetDTO sheet) {
        if (this.sheets == null) {
            this.sheets = new ArrayList<>();
        }
        this.sheets.add(sheet);
    }

    @Override
    public String toString() {
        return "ExcelFileDTO{" + "nameFile=" + nameFile + ", autor=" + autor + ", metadata=" + metadata + ", sheets=" + sheets + '}';
    }

}
