/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.oracle.dto;

import java.sql.Types;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author cescobarh
 */
public enum OracleDataType {

    VARCHAR,
    NUMBER,
    DATE,
    TIME,
    TIMESTAMP,
    CURSOR;

    public int indexValue() {
        int type = -1;
        if (this.equals(OracleDataType.VARCHAR)) {
            type = Types.VARCHAR;
        }
        if (this.equals(OracleDataType.NUMBER)) {
            type = Types.NUMERIC;
        }
        if (this.equals(OracleDataType.DATE)) {
            type = Types.DATE;
        }
        if (this.equals(OracleDataType.TIME)) {
            type = Types.TIME;
        }
        if (this.equals(OracleDataType.TIMESTAMP)) {
            type = Types.TIMESTAMP;
        }
        if (this.equals(OracleDataType.CURSOR)) {
            type = OracleTypes.CURSOR;
        }
        return type;
    }
}
