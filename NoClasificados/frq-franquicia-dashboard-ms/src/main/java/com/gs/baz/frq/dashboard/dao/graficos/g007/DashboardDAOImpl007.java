package com.gs.baz.frq.dashboard.dao.graficos.g007;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g007.CecoDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g007.DistribucionCumplimientoNivelDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g007.RangoDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g007.DistribucionCumplimientoCecoNivelDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g007.DistribucionCumplimientoCecoNivelRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl007 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectDistribucionCumplimiento;
    private DefaultJdbcCall jdbcSelectDistribucionCumplimientoNivel;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectDistribucionCumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDistribucionCumplimiento.withSchemaName(schema);
        jdbcSelectDistribucionCumplimiento.withCatalogName("PACONSDASHBOARD");
        jdbcSelectDistribucionCumplimiento.withProcedureName("SPDISTCUMPLIM");
        jdbcSelectDistribucionCumplimiento.returningResultSet("RCL_INFO", new DistribucionCumplimientoCecoNivelRowMapper());

        jdbcSelectDistribucionCumplimientoNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDistribucionCumplimientoNivel.withSchemaName(schema);
        jdbcSelectDistribucionCumplimientoNivel.withCatalogName("PAADMINCUMPARB");
        jdbcSelectDistribucionCumplimientoNivel.withProcedureName("SPCUMPARB");
        jdbcSelectDistribucionCumplimientoNivel.returningResultSet("RCL_INFO", new DistribucionCumplimientoCecoNivelRowMapper());
    }

    private List<DistribucionCumplimientoCecoNivelDTO> selectRowsDistribucionCumplimientoNacional(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEGOCIO", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectDistribucionCumplimiento.execute(mapSqlParameterSource);
        return (List<DistribucionCumplimientoCecoNivelDTO>) out.get("RCL_INFO");
    }

    private List<DistribucionCumplimientoCecoNivelDTO> selectRowsDistribucionCumplimientoNivel(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_NIVEL", filtrosDTO.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_CECO", filtrosDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectDistribucionCumplimientoNivel.execute(mapSqlParameterSource);
        return (List<DistribucionCumplimientoCecoNivelDTO>) out.get("RCL_INFO");
    }

    private DistribucionCumplimientoNivelDTO getDistribucionCumplimientoNivel(List<DistribucionCumplimientoCecoNivelDTO> distribucionesCumplimientoCecoNacional) throws CustomException {
        DistribucionCumplimientoNivelDTO distribucionCumplimientoDTO = new DistribucionCumplimientoNivelDTO();
        Map<Integer, List<DistribucionCumplimientoCecoNivelDTO>> temp = distribucionesCumplimientoCecoNacional.stream().collect(Collectors.groupingBy(DistribucionCumplimientoCecoNivelDTO::getIdCeco));
        if (temp != null) {
            List<CecoDTO> cumplimientoCecos = new ArrayList<>();
            temp.forEach((idCeco, cumplimientosNegocioDTO) -> {
                List<RangoDTO> rangos = new ArrayList<>();
                cumplimientosNegocioDTO.forEach(item -> {
                    rangos.add(new RangoDTO(item.getIdRango(), item.getPorcentaje(), item.getPuntosContacto()));
                });
                DistribucionCumplimientoCecoNivelDTO ceco = distribucionesCumplimientoCecoNacional.stream().filter(item -> item.getIdCeco().equals(idCeco)).collect(Collectors.toList()).get(0);
                cumplimientoCecos.add(new CecoDTO(ceco.getIdCeco(), ceco.getDescripcion(), rangos));
            });
            distribucionCumplimientoDTO.setCecos(cumplimientoCecos);
        }
        return distribucionCumplimientoDTO;
    }

    public DistribucionCumplimientoNivelDTO getDistribucionCumplimientoNivel(DistribucionCumplimientoCecoNivelDTO distribucionCumplimientoCecoNacionalDTO) throws CustomException {
        FiltrosDTO filtrosDTO = distribucionCumplimientoCecoNacionalDTO.getFiltros();
        if (filtrosDTO.getNivelGeografico().equals(4)) {
            return this.getDistribucionCumplimientoNivel(this.selectRowsDistribucionCumplimientoNacional(filtrosDTO));
        } else {
            return this.getDistribucionCumplimientoNivel(this.selectRowsDistribucionCumplimientoNivel(filtrosDTO));
        }
    }

}
