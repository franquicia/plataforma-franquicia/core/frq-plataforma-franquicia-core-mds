/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class GeografiaDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String id_ceco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_region")
    private String id_region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_zona")
    private String id_zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_territorio")
    private String id_territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    public String getId_territorio() {
        return id_territorio;
    }

    public void setId_territorio(String id_territorio) {
        this.id_territorio = id_territorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getId_zona() {
        return id_zona;
    }

    public void setId_zona(String id_zona) {
        this.id_zona = id_zona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getId_region() {
        return id_region;
    }

    public void setId_region(String id_region) {
        this.id_region = id_region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getId_ceco() {
        return id_ceco;
    }

    public void setId_ceco(String id_ceco) {
        this.id_ceco = id_ceco;
    }

    @Override
    public String toString() {
        return "GeografiaDTO{" + "id_territorio=" + id_territorio + ", territorio=" + territorio + ", id_zona=" + id_zona + ", zona=" + zona + ", id_region=" + id_region + ", region=" + region + ", id_ceco=" + id_ceco + '}';
    }

}
