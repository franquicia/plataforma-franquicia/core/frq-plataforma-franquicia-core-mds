package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteCuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.IncidenteCuadrillaRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class IncidenteCuadrillaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertatkCuadrilla;
    private DefaultJdbcCall jdbcEliminatkCuadrilla;
    private DefaultJdbcCall jdbcBuscaFila;
    private DefaultJdbcCall jdbcBuscaInfotkCuadrilla;
    private DefaultJdbcCall jdbcBuscaCuadrillaTK;
    private DefaultJdbcCall jdbcActualizatkCuadrilla;

    private static final Logger LOGGER = LogManager.getLogger();

    public IncidenteCuadrillaDAOImpl() {
        super();
    }

    @SuppressWarnings("all")
    public void init() {

        jdbcInsertatkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_INS_TK");

        jdbcEliminatkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_DEL_TK");

        jdbcBuscaInfotkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_TK", new IncidenteCuadrillaRowMapper());

        jdbcBuscaFila = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_TK", new IncidenteCuadrillaRowMapper());

        jdbcBuscaCuadrillaTK = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_SEL_CUADDET")
                .returningResultSet("RCL_TK", new IncidenteCuadrillaRowMapper());

        jdbcActualizatkCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMTKCUADRI")
                .withProcedureName("SP_ACT_TK");

    }

    @SuppressWarnings("all")
    public int inserta(IncidenteCuadrillaDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRI", bean.getIdCuadrilla())
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_TICKET", bean.getIncidente())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDER", bean.getLiderCuadrilla())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertatkCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_INS_TK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_TKCUA");
        idNota = idbloc.intValue();

        if (respuesta == 0) {
            LOGGER.info("Algo paso al insertar el evento");
        } else {
            return idNota;
        }

        return idNota;
    }

    @SuppressWarnings("all")
    public boolean elimina(String incidente) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TICKET", incidente);

        out = jdbcEliminatkCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_DEL_TK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("all")
    public List<IncidenteCuadrillaDTO> obtieneDatos(int idCuadrilla) throws Exception {
        int respuesta = 0;
        List<IncidenteCuadrillaDTO> listaDetU;
        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", idCuadrilla);

        out = jdbcBuscaFila.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<IncidenteCuadrillaDTO>) out.get("RCL_TK");

        if (respuesta != 1) {
            LOGGER.info("Algo paso al consular la cuadrilla ");
        }

        return listaDetU;
    }

    //busca tk cuadrillas
    @SuppressWarnings("all")
    public List<IncidenteCuadrillaDTO> obtieneCuadrillaTK(int idCuadrilla, int idProveedor, int zona) throws Exception {
        int respuesta = 0;
        List<IncidenteCuadrillaDTO> listaDetU;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", idCuadrilla)
                .addValue("PA_PROVEEDOR", idProveedor)
                .addValue("PA_ZONA", zona);

        out = jdbcBuscaCuadrillaTK.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_SEL_CUADDET}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<IncidenteCuadrillaDTO>) out.get("RCL_TK");

        if (respuesta != 1) {
            LOGGER.info("Algo paso al consular los incidentes de la cuadrilla ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("all")
    public List<IncidenteCuadrillaDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<IncidenteCuadrillaDTO> listaDetU;
        int error = 0;

        out = jdbcBuscaInfotkCuadrilla.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_SEL_DETALLE}");
        listaDetU = (List<IncidenteCuadrillaDTO>) out.get("RCL_TK");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            LOGGER.info("Algo paso al obtener los tk de la Cuadrilla");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @SuppressWarnings("all")
    public boolean actualiza(IncidenteCuadrillaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRI", bean.getIdCuadrilla())
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_TICKET", bean.getIncidente())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDER", bean.getLiderCuadrilla())
                .addValue("PA_STATUS", bean.getStatus());
        out = jdbcActualizatkCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMTKCUADRI.SP_ACT_TK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            LOGGER.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

}
