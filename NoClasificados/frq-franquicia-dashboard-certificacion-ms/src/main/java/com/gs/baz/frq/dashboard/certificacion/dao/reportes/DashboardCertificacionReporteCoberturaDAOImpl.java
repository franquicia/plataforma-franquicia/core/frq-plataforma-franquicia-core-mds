package com.gs.baz.frq.dashboard.certificacion.dao.reportes;

import com.gs.baz.frq.dashboard.certificacion.dto.reportes.DashboardCertificacionReporteCoberturaDTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.reportes.DashboardCertificacionReporteCoberturaRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionReporteCoberturaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PADASHCCREPORTE");
        jdbcSelect.withProcedureName("SPREPOCOBNAC");
        jdbcSelect.returningResultSet("PA_CREPOCOBNAC", new DashboardCertificacionReporteCoberturaRowMapper());

    }

    public List<DashboardCertificacionReporteCoberturaDTO> selectRowDashboardCertificacionReporteCob(String entityDTO1, String entityDTO2, Long entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO1);
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO2);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO3);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO4);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionReporteCoberturaDTO>) out.get("PA_CREPOCOBNAC");

    }

    public List<DashboardCertificacionReporteCoberturaDTO> selectRowsDashboardCertificacionReporteCob() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", null);
        mapSqlParameterSource.addValue("PA_FECHAFIN", null);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", null);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionReporteCoberturaDTO>) out.get("PA_CREPOCOBNAC");
        } else {
            return null;
        }

    }

}
