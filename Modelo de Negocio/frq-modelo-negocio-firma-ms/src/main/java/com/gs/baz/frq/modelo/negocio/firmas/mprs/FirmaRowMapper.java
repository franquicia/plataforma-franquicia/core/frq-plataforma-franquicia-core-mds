/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.mprs;

import com.gs.baz.frq.modelo.negocio.firmas.dto.FirmaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FirmaRowMapper implements RowMapper<FirmaBase> {

    private FirmaBase firmaBase;

    @Override
    public FirmaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        firmaBase = new FirmaBase();
        firmaBase.setNumeroEmpleado(rs.getInt("FINUMEROEMPLEADO"));
        firmaBase.setIdJerarquia(rs.getInt("FIIDJERARQUIA"));
        firmaBase.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        return firmaBase;
    }
}
