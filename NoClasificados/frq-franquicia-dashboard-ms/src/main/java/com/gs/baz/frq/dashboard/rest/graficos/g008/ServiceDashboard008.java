/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g008;

import com.gs.baz.frq.dashboard.dao.graficos.g008.DashboardDAOImpl008;
import com.gs.baz.frq.dashboard.dto.graficos.g008.NegocioDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard008 {

    @Autowired
    private DashboardDAOImpl008 dashboardDAOImpl008;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/seccion/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getCumplimientoSeccionNegocioNivel(@RequestBody NegocioDTO negocioDTO) throws CustomException {
        return dashboardDAOImpl008.getCumplimientoSeccionNivel(negocioDTO);
    }

    @RequestMapping(value = "secure/grafico/get/cumplimiento/seccion/negocio/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getCumplimientoSeccionNegocioNivelNegocio(@RequestBody NegocioDTO negocioDTO) throws CustomException {
        return dashboardDAOImpl008.getCumplimientoSeccionNivelNegocio(negocioDTO);
    }
}
