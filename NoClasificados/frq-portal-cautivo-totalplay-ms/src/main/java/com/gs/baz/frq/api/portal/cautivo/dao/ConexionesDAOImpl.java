/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dao;

import com.gs.baz.frq.api.portal.cautivo.dto.ConexionesDTO;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author MADA
 */
public class ConexionesDAOImpl extends DefaultDAO {

    private String schema;

    private DefaultJdbcCall jdbcInsertDevice;
    private DefaultJdbcCall jdbcSelectDevice;

    private final Logger logger = LogManager.getLogger();

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertDevice = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertDevice.withSchemaName(schema);
        jdbcInsertDevice.withCatalogName("PAADMUSRPORTAL");
        jdbcInsertDevice.withProcedureName("SPINSUSRPORTAL");
        /*
        jdbcSelectDevice = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDevice.withSchemaName(schema);
        jdbcSelectDevice.withCatalogName("PAADMDETDEVICE");
        jdbcSelectDevice.withProcedureName("SPGETDETDEVICE");
        jdbcSelectDevice.returningResultSet("PA_CDATOS", new DispositivoRowMapper());
         */
    }

    public boolean insertRow(ConexionesDTO dispositivoDTO) {

        try {

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FISUSUARIO_ID", dispositivoDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FCIP", dispositivoDTO.getIp());
            mapSqlParameterSource.addValue("PA_FCMAC", dispositivoDTO.getMac());
            mapSqlParameterSource.addValue("PA_FCUSERNAME", dispositivoDTO.getUsername());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", dispositivoDTO.getFbNombre());
            mapSqlParameterSource.addValue("PA_FCAPELLIDO_PATERNO", dispositivoDTO.getFbApellido());
            mapSqlParameterSource.addValue("PA_FCAPELLIDO_MATERNO", dispositivoDTO.getApellidoMaterno());
            mapSqlParameterSource.addValue("PA_FCNOMBRE_COMPLETO", dispositivoDTO.getUsername());
            mapSqlParameterSource.addValue("PA_FDINICIO_SESION", dispositivoDTO.getFecha());
            mapSqlParameterSource.addValue("PA_FCCORREO", dispositivoDTO.getCorreo());
            mapSqlParameterSource.addValue("PA_FCENVIAR", dispositivoDTO.getEnviar());
            mapSqlParameterSource.addValue("PA_FCSUCURSAL", dispositivoDTO.getTienda());
            mapSqlParameterSource.addValue("PA_FCRANGO_EDAD", dispositivoDTO.getEdadCuestionario());
            mapSqlParameterSource.addValue("PA_FCGENERO", dispositivoDTO.getFbGenero());
            mapSqlParameterSource.addValue("PA_FCURLFOTO", dispositivoDTO.getFbFotoPerfil());
            mapSqlParameterSource.addValue("PA_FCTIPO_SESION", dispositivoDTO.getTipoCorreo());
            mapSqlParameterSource.addValue("PA_FCFASE_SESION", dispositivoDTO.getFaseSesion());

            Map<String, Object> out = jdbcInsertDevice.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

            return success;
        } catch (Exception e) {
            logger.debug("CATCH - " + dispositivoDTO.toString());
            e.printStackTrace();
        }
        return false;
    }

}
