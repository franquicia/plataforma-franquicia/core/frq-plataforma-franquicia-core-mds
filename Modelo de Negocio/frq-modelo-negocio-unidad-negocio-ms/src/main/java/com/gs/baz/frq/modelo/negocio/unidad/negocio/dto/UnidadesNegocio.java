/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de unidades de Negocio del modelo de negocio", value = "UnidadesNegocio")
public class UnidadesNegocio {

    @JsonProperty(value = "unidadesNegocio")
    @ApiModelProperty(notes = "unidadesNegocio")
    private List<UnidadNegocio> unidadesNegocio;

    public UnidadesNegocio(List<UnidadNegocio> unidadesNegocio) {
        this.unidadesNegocio = unidadesNegocio;
    }

    public List<UnidadNegocio> getUnidadesNegocio() {
        return unidadesNegocio;
    }

    public void setUnidadesNegocio(List<UnidadNegocio> unidadesNegocio) {
        this.unidadesNegocio = unidadesNegocio;
    }

}
