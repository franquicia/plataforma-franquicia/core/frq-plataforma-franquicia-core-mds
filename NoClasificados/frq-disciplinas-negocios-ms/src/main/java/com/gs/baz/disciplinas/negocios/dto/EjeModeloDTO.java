/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EjeModeloDTO {

    @JsonProperty(value = "id_eje_modelo")
    private Integer idModEje;

    @JsonProperty(value = "id_eje")
    private Integer idEje;

    @JsonProperty(value = "id_modelo")
    private Integer idModelo;

    @JsonProperty(value = "disciplinas_eje")
    private List<DisciplinaEjeDTO> disciplinasEje;

    public EjeModeloDTO() {
    }

    public Integer getIdModEje() {
        return idModEje;
    }

    public void setIdModEje(Integer idModEje) {
        this.idModEje = idModEje;
    }

    public Integer getIdEje() {
        return idEje;
    }

    public void setIdEje(Integer idEje) {
        this.idEje = idEje;
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Integer idModelo) {
        this.idModelo = idModelo;
    }

    public List<DisciplinaEjeDTO> getDisciplinasEje() {
        return disciplinasEje;
    }

    public void setDisciplinasEje(List<DisciplinaEjeDTO> disciplinasEje) {
        this.disciplinasEje = disciplinasEje;
    }

    @Override
    public String toString() {
        return "EjeModeloDTO{" + "idModEje=" + idModEje + ", idEje=" + idEje + ", idModelo=" + idModelo + ", disciplinasEje=" + disciplinasEje + '}';
    }

}
