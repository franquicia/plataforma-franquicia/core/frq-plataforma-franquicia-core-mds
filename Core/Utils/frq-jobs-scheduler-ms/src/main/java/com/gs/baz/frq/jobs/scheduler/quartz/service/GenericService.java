package com.gs.baz.frq.jobs.scheduler.quartz.service;

import com.gs.baz.frq.jobs.scheduler.dto.Execution;
import com.gs.baz.frq.jobs.scheduler.dto.ScheduleJob;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class GenericService {

    private final Logger logger = LogManager.getLogger();

    public void run(ScheduleJob scheduleJob) {
        Execution execution = scheduleJob.getExecution();
        if (execution == null) {
            execution = new Execution();
        }
        execution.setLastExecutionSucccess(new Date());
        execution.setLastExecutionSucccessResult("Ok");
        scheduleJob.setExecution(execution);
    }
}
