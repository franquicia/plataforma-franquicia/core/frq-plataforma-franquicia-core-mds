/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyectos.mprs;

import com.gs.baz.frq.documento.sucursal.proyectos.dto.ProyectoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ProyectoRowMapper implements RowMapper<ProyectoBase> {

    private ProyectoBase proyectoBase;

    @Override
    public ProyectoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        proyectoBase = new ProyectoBase();
        proyectoBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        return proyectoBase;
    }
}
