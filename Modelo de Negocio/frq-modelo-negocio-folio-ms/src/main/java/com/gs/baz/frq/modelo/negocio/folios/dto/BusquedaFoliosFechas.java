/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de folios del modelo de negocio", value = "folios")
public class BusquedaFoliosFechas {

    @JsonProperty(value = "folios")
    @ApiModelProperty(notes = "folios")
    private List<BusquedaFolioFechas> folios;

    public BusquedaFoliosFechas(List<BusquedaFolioFechas> folios) {
        this.folios = folios;
    }

    public List<BusquedaFolioFechas> getFolios() {
        return folios;
    }

    public void setFolios(List<BusquedaFolioFechas> folios) {
        this.folios = folios;
    }

    @Override
    public String toString() {
        return "EstadoFolios{" + "folios=" + folios + '}';
    }

}
