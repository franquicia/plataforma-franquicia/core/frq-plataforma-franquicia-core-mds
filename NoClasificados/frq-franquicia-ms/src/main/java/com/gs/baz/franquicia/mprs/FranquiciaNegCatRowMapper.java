/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.mprs;

import com.gs.baz.franquicia.dto.FranquiciaNegCatDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author MADA
 */
public class FranquiciaNegCatRowMapper implements RowMapper<FranquiciaNegCatDTO> {

    private FranquiciaNegCatDTO franquiciaNegCat;

    @Override
    public FranquiciaNegCatDTO mapRow(ResultSet rs, int i) throws SQLException {
        franquiciaNegCat = new FranquiciaNegCatDTO();
        franquiciaNegCat.setIdFranquiciaNeg((BigDecimal) rs.getObject("FIIDFRGN"));
        franquiciaNegCat.setIdFranquicia((BigDecimal) rs.getObject("FIIDFRANQ"));
        franquiciaNegCat.setIdNegocio((BigDecimal) rs.getObject("FIIDNEGO"));
        franquiciaNegCat.setIdCategoria((BigDecimal) rs.getObject("FIIDCATEG"));
        return franquiciaNegCat;
    }

}
