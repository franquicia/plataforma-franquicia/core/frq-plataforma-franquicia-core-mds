/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.mprs;

import com.gs.baz.frq.modelo.negocio.firmas.dto.Firma;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FirmasRowMapper implements RowMapper<Firma> {

    private Firma firma;

    @Override
    public Firma mapRow(ResultSet rs, int rowNum) throws SQLException {
        firma = new Firma();
        firma.setIdFirma(rs.getInt("FIIDFIRMA"));
        firma.setNumeroEmpleado(rs.getInt("FINUMEROEMPLEADO"));
        firma.setIdJerarquia(rs.getInt("FIIDJERARQUIA"));
        firma.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        firma.setIndice(rs.getInt("FIINDICE"));
        return firma;
    }
}
