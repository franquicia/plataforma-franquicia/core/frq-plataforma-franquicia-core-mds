/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.mprs;

import com.gs.baz.frq.api.portal.cautivo.dto.PortalCautivoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author MADA
 */
public class RespuestaPortalCautivoRowMapper implements RowMapper<PortalCautivoDTO> {

    @Override
    public PortalCautivoDTO mapRow(ResultSet rs, int i) throws SQLException {
        PortalCautivoDTO gobiernoDatosDTO = new PortalCautivoDTO();

        gobiernoDatosDTO.setIdUsuario(rs.getInt("FISUSUARIO_ID"));
        gobiernoDatosDTO.setIp(rs.getString("FCIP"));
        gobiernoDatosDTO.setMac(rs.getString("FCMAC"));
        gobiernoDatosDTO.setNombreUsuario(rs.getString("FCUSERNAME"));
        gobiernoDatosDTO.setNombre(rs.getString("FCNOMBRE") == null ? "" : rs.getString("FCNOMBRE"));
        gobiernoDatosDTO.setApellidoPaterno(rs.getString("FCAPELLIDO_PATERNO") == null ? "" : rs.getString("FCAPELLIDO_PATERNO"));
        gobiernoDatosDTO.setApellidoMaterno(rs.getString("FCAPELLIDO_MATERNO") == null ? "" : rs.getString("FCAPELLIDO_MATERNO"));
        gobiernoDatosDTO.setNombreCompleto(rs.getString("FCNOMBRE_COMPLETO") == null ? "" : rs.getString("FCNOMBRE_COMPLETO"));
        gobiernoDatosDTO.setInicioSesion(rs.getString("FDINICIO_SESION"));
        gobiernoDatosDTO.setCorreo(rs.getString("FCCORREO") == null ? "" : rs.getString("FCCORREO"));
        gobiernoDatosDTO.setEnviar(rs.getString("FCENVIAR") == null ? "" : rs.getString("FCENVIAR"));
        gobiernoDatosDTO.setSucursal(rs.getString("FCSUCURSAL"));
        gobiernoDatosDTO.setRangoEdad(rs.getString("FCRANGO_EDAD") == null ? "" : rs.getString("FCRANGO_EDAD"));
        gobiernoDatosDTO.setGenero(rs.getString("FCGENERO") == null ? "" : rs.getString("FCGENERO"));
        gobiernoDatosDTO.setUrlFoto(rs.getString("FCURLFOTO") == null ? "" : rs.getString("FCURLFOTO"));
        gobiernoDatosDTO.setTipoSesion(rs.getString("FCTIPO_SESION") == null ? "" : rs.getString("FCTIPO_SESION"));
        gobiernoDatosDTO.setFaseSesion(rs.getString("FCFASE_SESION") == null ? "" : rs.getString("FCFASE_SESION"));

        return gobiernoDatosDTO;
    }

}
