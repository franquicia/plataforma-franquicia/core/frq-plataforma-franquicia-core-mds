/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.AltaEstadoFirma;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.EstadoFirma;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.EstadoFirmaBase;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.ParametrosEstadoFirma;
import com.gs.baz.frq.modelo.negocio.estados.firma.mprs.EstadoFirmaRowMapper;
import com.gs.baz.frq.modelo.negocio.estados.firma.mprs.EstadosFirmaRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class EstadoFirmaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNEDOFIRM";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNEDOFIRMA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNEDOFIRMA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNEDOFIRMA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETMNEDOFIRMA");
        jdbcSelect.returningResultSet("PA_CDATOS", new EstadoFirmaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETMNEDOFIRMA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new EstadosFirmaRowMapper());

    }

    public EstadoFirmaBase selectRow(Long idCampoSensible) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", idCampoSensible);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<EstadoFirmaBase> data = (List<EstadoFirmaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<EstadoFirma> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<EstadoFirma>) out.get("PA_CDATOS");
    }

    public AltaEstadoFirma insertRow(ParametrosEstadoFirma entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDESTADOFIRMA");
                return new AltaEstadoFirma(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosEstadoFirma entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", entityDTO.getIdEstadoFirma());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
