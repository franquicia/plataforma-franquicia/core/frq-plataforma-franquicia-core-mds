/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reglas7s.preguntas.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.reglas7s.preguntas.dao.Reglas7sPreguntasDAOImpl;
import com.gs.baz.frq.reglas7s.preguntas.dto.Reglas7sPreguntasDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/reglas7s/preguntas")
@Component("FRQServiceReglas7sPreguntas")
public class ServiceReglas7sPreguntas {

    @Autowired
    private Reglas7sPreguntasDAOImpl reglas7sPreguntasDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reglas7sPreguntasDTO postReglas7sPreguntas(@RequestBody Reglas7sPreguntasDTO reglas7sPreguntas) throws CustomException {
        if (reglas7sPreguntas.getIdPreguntaCorresponde() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta_corresponde"));
        }
        if (reglas7sPreguntas.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (reglas7sPreguntas.getIdClave() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_clave"));
        }

        return reglas7sPreguntasDAOImpl.insertRow(reglas7sPreguntas);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reglas7sPreguntasDTO putReglas7sPreguntas(@RequestBody Reglas7sPreguntasDTO reglas7sPreguntas) throws CustomException {
        if (reglas7sPreguntas.getIdPreguntaCorresponde() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta_corresponde"));
        }
        if (reglas7sPreguntas.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (reglas7sPreguntas.getIdClave() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_clave"));
        }
        return reglas7sPreguntasDAOImpl.updateRow(reglas7sPreguntas);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reglas7sPreguntasDTO> getReglas7sPreguntas(@RequestBody Reglas7sPreguntasDTO reglas7sPreguntas) throws CustomException {
        return reglas7sPreguntasDAOImpl.selectRows(reglas7sPreguntas);
    }

    @RequestMapping(value = "secure/get/parametros", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reglas7sPreguntasDTO getReglas7sPreguntasParametros(@RequestBody Reglas7sPreguntasDTO reglas7sPreguntas) throws CustomException {
        return reglas7sPreguntasDAOImpl.selectRowsParametros(reglas7sPreguntas);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reglas7sPreguntasDTO deleteReglas7sPreguntas(@RequestBody Reglas7sPreguntasDTO reglas7sPreguntas) throws CustomException {
        if (reglas7sPreguntas.getIdPreguntaCorresponde() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta_corresponde"));
        }
        if (reglas7sPreguntas.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        return reglas7sPreguntasDAOImpl.deleteRow(reglas7sPreguntas);
    }
}
