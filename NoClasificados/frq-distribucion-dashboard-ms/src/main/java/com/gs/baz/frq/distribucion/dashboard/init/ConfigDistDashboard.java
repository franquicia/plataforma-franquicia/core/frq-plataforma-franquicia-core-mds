package com.gs.baz.frq.distribucion.dashboard.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.distribucion.dashboard.bi.DistribucionDashboardBI;
import com.gs.baz.frq.distribucion.dashboard.dao.DistribucionDashboardDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import com.gs.baz.frq.data.sources.config.GenericConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Import(GenericConfig.class)
@Configuration
@ComponentScan("com.gs.baz.frq.distribucion.dashboard")
public class ConfigDistDashboard {

    private final Logger logger = LogManager.getLogger();

    public ConfigDistDashboard() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DistribucionDashboardDAOImpl distribucionDashboardDAOImpl() {
        return new DistribucionDashboardDAOImpl();
    }

    @Bean()
    public DistribucionDashboardBI distribucionDashboardBI() {
        return new DistribucionDashboardBI();
    }

}
