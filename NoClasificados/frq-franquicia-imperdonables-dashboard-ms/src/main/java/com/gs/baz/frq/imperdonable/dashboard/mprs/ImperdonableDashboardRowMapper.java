package com.gs.baz.frq.imperdonable.dashboard.mprs;

import com.gs.baz.frq.imperdonable.dashboard.dto.ImperdonableDashboardDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ImperdonableDashboardRowMapper implements RowMapper<ImperdonableDashboardDTO> {

    private ImperdonableDashboardDTO status;

    @Override
    public ImperdonableDashboardDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ImperdonableDashboardDTO();
        status.setIdCeco(rs.getString("FCID_CECO"));
        status.setIdChecklist(((BigDecimal) rs.getObject("FIID_CHECKLIST")));
        status.setIdDashboard(((BigDecimal) rs.getObject("FIDASHBOARD_ID")));
        status.setIdPregunta(((BigDecimal) rs.getObject("FIID_PREGUNTA")));
        status.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        status.setModUsuario(rs.getString("FCUSUARIO_MOD"));
        status.setModFecha(rs.getString("FDFECHA_MOD"));
        return status;
    }
}
