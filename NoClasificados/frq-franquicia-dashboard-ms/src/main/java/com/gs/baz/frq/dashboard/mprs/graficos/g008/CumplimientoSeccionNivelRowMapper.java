package com.gs.baz.frq.dashboard.mprs.graficos.g008;

import com.gs.baz.frq.dashboard.dto.graficos.g008.CumplimientoSeccionNivelDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoSeccionNivelRowMapper implements RowMapper<CumplimientoSeccionNivelDTO> {

    private CumplimientoSeccionNivelDTO cumplimientoSeccionNivelDTO;

    @Override
    public CumplimientoSeccionNivelDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoSeccionNivelDTO = new CumplimientoSeccionNivelDTO();
        cumplimientoSeccionNivelDTO.setRowNumber(rowNum);
        cumplimientoSeccionNivelDTO.setModulo(rs.getString("FCMODULO"));
        cumplimientoSeccionNivelDTO.setPuntoObtenido((BigDecimal) rs.getObject("PUNTOBT"));
        cumplimientoSeccionNivelDTO.setValorSeccion((BigDecimal) rs.getObject("VALORSECCION"));
        cumplimientoSeccionNivelDTO.setCalificacion((BigDecimal) rs.getObject("CALIFICACION"));
        return cumplimientoSeccionNivelDTO;
    }
}
