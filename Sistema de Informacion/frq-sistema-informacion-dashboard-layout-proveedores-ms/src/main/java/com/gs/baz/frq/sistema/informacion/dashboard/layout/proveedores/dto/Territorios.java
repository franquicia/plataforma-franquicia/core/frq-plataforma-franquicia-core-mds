/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de Territorios disponibles", value = "Territorios")
public class Territorios {

    @JsonProperty(value = "territorios")
    @ApiModelProperty(notes = "territorios")
    private List<Territorio> territorios;

    public Territorios(List<Territorio> territorios) {
        this.territorios = territorios;
    }

    public List<Territorio> getTerritorios() {
        return territorios;
    }

    public void setTerritorios(List<Territorio> territorios) {
        this.territorios = territorios;
    }

    @Override
    public String toString() {
        return "Territorios{" + "territorios=" + territorios + '}';
    }

}
