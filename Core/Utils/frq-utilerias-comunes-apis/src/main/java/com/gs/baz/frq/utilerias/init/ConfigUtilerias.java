package com.gs.baz.frq.utilerias.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.bi.WriteExcelUtil;
import javax.servlet.MultipartConfigElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.utilerias")
public class ConfigUtilerias {

    private final Logger logger = LogManager.getLogger();

    public ConfigUtilerias() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        DataSize dataSize = DataSize.of(100, DataUnit.MEGABYTES);
        factory.setMaxFileSize(dataSize);
        factory.setMaxRequestSize(dataSize);
        return factory.createMultipartConfig();
    }

    @Bean(name = "writeExcelUtilV2")
    public WriteExcelUtil writeExcelUtil() {
        return new WriteExcelUtil();
    }
}
