/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.punto.venta;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionPuntoVentaDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_base")
    private Integer idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_ceco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_visita")
    private String fechaVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_empleado")
    private Integer numeroEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_empleado")
    private String nombreEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_usuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_protocolo")
    private String nombreProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_protocolo")
    private Integer calificacionProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_lineal")
    private Integer calificacionLineal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_esperados")
    private Integer puntosEsperados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango_color")
    private String rangoColor;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_visita")
    private Integer idVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_pregunta")
    private Integer idPregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion_pregunta")
    private String descripcionPregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "respuesta")
    private String respuesta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio")
    private Integer negocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ponderacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "incumplimiento")
    private Integer incumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "checklist")
    private Integer checklist;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "imperdonable")
    private Integer imperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipo_evidencia")
    private Integer tipoEvidencia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ruta_evidencia")
    private String rutaEvidencia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ruta_informe_general")
    private String rutaInformeGeneral;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ruta_retro")
    private String rutaRetro;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_respuesta")
    private Integer idRespuesta;

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(BigDecimal idCecoBase) {
        this.idCecoBase = (idCecoBase == null ? null : idCecoBase.intValue());
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(BigDecimal numeroEmpleado) {
        this.numeroEmpleado = (numeroEmpleado == null ? null : numeroEmpleado.intValue());
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombreProtocolo() {
        return nombreProtocolo;
    }

    public void setNombreProtocolo(String nombreProtocolo) {
        this.nombreProtocolo = nombreProtocolo;
    }

    public Integer getCalificacionProtocolo() {
        return calificacionProtocolo;
    }

    public void setCalificacionProtocolo(BigDecimal calificacionProtocolo) {
        this.calificacionProtocolo = (calificacionProtocolo == null ? null : calificacionProtocolo.intValue());
    }

    public Integer getCalificacionLineal() {
        return calificacionLineal;
    }

    public void setCalificacionLineal(BigDecimal calificacionLineal) {
        this.calificacionLineal = (calificacionLineal == null ? null : calificacionLineal.intValue());
    }

    public Integer getPuntosEsperados() {
        return puntosEsperados;
    }

    public void setPuntosEsperados(BigDecimal puntosEsperados) {
        this.puntosEsperados = (puntosEsperados == null ? null : puntosEsperados.intValue());
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(BigDecimal idRango) {
        this.idRango = (idRango == null ? null : idRango.intValue());
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(BigDecimal idVisita) {
        this.idVisita = (idVisita == null ? null : idVisita.intValue());
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(BigDecimal idPregunta) {
        this.idPregunta = (idPregunta == null ? null : idPregunta.intValue());
    }

    public String getDescripcionPregunta() {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta) {
        this.descripcionPregunta = descripcionPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getNegocio() {
        return negocio;
    }

    public void setNegocio(BigDecimal negocio) {
        this.negocio = (negocio == null ? null : negocio.intValue());
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getIncumplimiento() {
        return incumplimiento;
    }

    public void setIncumplimiento(BigDecimal incumplimiento) {
        this.incumplimiento = (incumplimiento == null ? null : incumplimiento.intValue());
    }

    public Integer getChecklist() {
        return checklist;
    }

    public void setChecklist(BigDecimal checklist) {
        this.checklist = (checklist == null ? null : checklist.intValue());
    }

    public Integer getImperdonable() {
        return imperdonable;
    }

    public void setImperdonable(BigDecimal imperdonable) {
        this.imperdonable = (imperdonable == null ? null : imperdonable.intValue());
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(BigDecimal calificacion) {
        this.calificacion = (calificacion == null ? null : calificacion.intValue());
    }

    public Integer getTipoEvidencia() {
        return tipoEvidencia;
    }

    public void setTipoEvidencia(BigDecimal tipoEvidencia) {
        this.tipoEvidencia = (tipoEvidencia == null ? null : tipoEvidencia.intValue());
    }

    public String getRutaEvidencia() {
        return rutaEvidencia;
    }

    public void setRutaEvidencia(String rutaEvidencia) {
        this.rutaEvidencia = rutaEvidencia;
    }

    public String getRutaInformeGeneral() {
        return rutaInformeGeneral;
    }

    public void setRutaInformeGeneral(String rutaInformeGeneral) {
        this.rutaInformeGeneral = rutaInformeGeneral;
    }

    public String getRutaRetro() {
        return rutaRetro;
    }

    public void setRutaRetro(String rutaRetro) {
        this.rutaRetro = rutaRetro;
    }

    public Integer getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(BigDecimal idRespuesta) {
        this.idRespuesta = (idRespuesta == null ? null : idRespuesta.intValue());
    }

    @Override
    public String toString() {
        return "DashboardCertificacionPuntoVentaDTO{" + "idCeco=" + idCeco + ", idCecoBase=" + idCecoBase + ", nombreCeco=" + nombreCeco + ", fechaVisita=" + fechaVisita + ", numeroEmpleado=" + numeroEmpleado + ", nombreEmpleado=" + nombreEmpleado + ", fecha=" + fecha + ", idProtocolo=" + idProtocolo + ", idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", nombreProtocolo=" + nombreProtocolo + ", calificacionProtocolo=" + calificacionProtocolo + ", idRango=" + idRango + ", rangoColor=" + rangoColor + ", idVisita=" + idVisita + ", idPregunta=" + idPregunta + ", descripcionPregunta=" + descripcionPregunta + ", respuesta=" + respuesta + ", negocio=" + negocio + ", ponderacion=" + ponderacion + ", modulo=" + modulo + ", incumplimiento=" + incumplimiento + ", checklist=" + checklist + ", imperdonable=" + imperdonable + ", calificacion=" + calificacion + ", tipoEvidencia=" + tipoEvidencia + ", rutaEvidencia=" + rutaEvidencia + ", rutaInformeGeneral=" + rutaInformeGeneral + ", rutaRetro=" + rutaRetro + ", idRespuesta=" + idRespuesta + '}';
    }

}
