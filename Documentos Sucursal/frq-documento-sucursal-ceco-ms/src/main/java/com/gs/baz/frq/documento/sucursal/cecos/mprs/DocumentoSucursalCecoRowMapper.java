/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.mprs;

import com.gs.baz.frq.documento.sucursal.cecos.dto.Ceco;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalCecoRowMapper implements RowMapper<Ceco> {

    private Ceco ceco;

    @Override
    public Ceco mapRow(ResultSet rs, int rowNum) throws SQLException {
        ceco = new Ceco();
        ceco.setIdCeco(rs.getInt("FCID_CECO"));
        ceco.setDescripcion(rs.getString("FCNOMBRE"));
        return ceco;
    }
}
