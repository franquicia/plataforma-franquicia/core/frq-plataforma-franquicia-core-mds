/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del folio y documento", value = "BusquedaFolioUsuario")
public class BusquedaFolioAutorizacion extends Folio {

    @JsonProperty(value = "unidadesNegocio")
    @ApiModelProperty(notes = "unidadesNegocio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<UnidadNegocio> unidadesNegocio;

    @JsonProperty(value = "fechaSolicitud")
    @ApiModelProperty(notes = "Fecha de solicitud de la firma del folio", example = "01/03/2021")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fechaSolicitud;

    @JsonProperty(value = "visualizado")
    @ApiModelProperty(notes = "Estado de visualizado", example = "true")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean visualizado;

    @JsonProperty(value = "firma")
    @ApiModelProperty(notes = "Firma del folio y el usuario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Firma firma;

    public List<UnidadNegocio> getUnidadesNegocio() {
        return unidadesNegocio;
    }

    public void setUnidadesNegocio(List<UnidadNegocio> unidadesNegocio) {
        this.unidadesNegocio = unidadesNegocio;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Boolean getVisualizado() {
        return visualizado;
    }

    public void setVisualizado(Boolean visualizado) {
        this.visualizado = visualizado;
    }

    public Firma getFirma() {
        return firma;
    }

    public void setFirma(Firma firma) {
        this.firma = firma;
    }

    @Override
    public String toString() {
        return "BusquedaFolioUsuario{" + "unidadesNegocio=" + unidadesNegocio + ", fechaSolicitud=" + fechaSolicitud + ", visualizado=" + visualizado + ", firma=" + firma + '}';
    }

}
