/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Respuesta de aceptación del cliente", value = "RespuestaAceptaCliente")
public class RespuestaAceptaClienteDTO {

    @JsonProperty(value = "estatusAceptacion")
    @ApiModelProperty(notes = "Respuesta de salida", example = "1")
    private Integer estatusModificado;

    public RespuestaAceptaClienteDTO() {
        super();

    }

    public Integer getEstatusModificado() {
        return estatusModificado;
    }

    public void setEstatusModificado(Integer estatusModificado) {
        this.estatusModificado = estatusModificado;
    }

}
