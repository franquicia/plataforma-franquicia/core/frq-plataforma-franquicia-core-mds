/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.graficos.g001;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionG001DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicial")
    private String fechaInicial;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_final")
    private String fechaFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_gerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cobertura_total")
    private Integer coberturaTotal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cobertura_certificacion")
    private Integer coberturaCertificacion;

    @JsonProperty(value = "dias_restantes")
    private Integer diasRestantes;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango")
    private Integer rango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango_color")
    private String rangoColor;

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(BigDecimal idGerente) {
        this.idGerente = (idGerente == null ? null : idGerente.intValue());
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public Integer getCoberturaTotal() {
        return coberturaTotal;
    }

    public void setCoberturaTotal(BigDecimal coberturaTotal) {
        this.coberturaTotal = (coberturaTotal == null ? null : coberturaTotal.intValue());
    }

    public Integer getCoberturaCertificacion() {
        return coberturaCertificacion;
    }

    public void setCoberturaCertificacion(BigDecimal coberturaCertificacion) {
        this.coberturaCertificacion = (coberturaCertificacion == null ? null : coberturaCertificacion.intValue());
    }

    public Integer getDiasRestantes() {
        return diasRestantes;
    }

    public void setDiasRestantes(BigDecimal diasRestantes) {
        this.diasRestantes = (diasRestantes == null ? null : diasRestantes.intValue());
    }

    public Integer getRango() {
        return rango;
    }

    public void setRango(BigDecimal rango) {
        this.rango = (rango == null ? null : rango.intValue());
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    @Override
    public String toString() {
        return "DashboardCertificacionG001DTO{" + "fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", idGerente=" + idGerente + ", idNegocio=" + idNegocio + ", coberturaTotal=" + coberturaTotal + ", coberturaCertificacion=" + coberturaCertificacion + ", diasRestantes=" + diasRestantes + ", rango=" + rango + ", rangoColor=" + rangoColor + '}';
    }
}
