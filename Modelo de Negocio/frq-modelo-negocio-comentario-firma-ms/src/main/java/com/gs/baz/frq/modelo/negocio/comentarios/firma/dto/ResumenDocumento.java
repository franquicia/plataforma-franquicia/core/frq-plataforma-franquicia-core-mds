/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos resumen del documento", value = "ResumenDocumento")
public class ResumenDocumento {

    @JsonProperty(value = "documento")
    @ApiModelProperty(notes = "documento")
    private Documento documento;

    @JsonProperty(value = "etiquetas")
    @ApiModelProperty(notes = "etiquetas")
    private List<Etiqueta> etiquetas;

    @JsonProperty(value = "firmas")
    @ApiModelProperty(notes = "firmas")
    private List<Firma1> firmas;

    public ResumenDocumento() {
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public List<Etiqueta> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(List<Etiqueta> etiquetas) {
        this.etiquetas = etiquetas;
    }

    public List<Firma1> getFirmas() {
        return firmas;
    }

    public void setFirmas(List<Firma1> firmas) {
        this.firmas = firmas;
    }

    @Override
    public String toString() {
        return "ResumenDocumento{" + "documento=" + documento + ", etiquetas=" + etiquetas + ", firmas=" + firmas + '}';
    }

}
