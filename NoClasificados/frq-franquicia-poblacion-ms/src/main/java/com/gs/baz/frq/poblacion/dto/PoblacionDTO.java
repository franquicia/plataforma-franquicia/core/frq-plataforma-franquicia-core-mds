/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.poblacion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PoblacionDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_fin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejecucion")
    private Integer ejecucion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "registros_tabla")
    private Integer tabla;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "registro_insertados")
    private Integer registro;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protoocolo")
    private Integer idProtocolo;

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(BigDecimal ejecucion) {
        this.ejecucion = (ejecucion == null ? null : ejecucion.intValue());
    }

    public Integer getTabla() {
        return tabla;
    }

    public void setTabla(BigDecimal tabla) {
        this.tabla = (tabla == null ? null : tabla.intValue());
    }

    public Integer getRegistro() {
        return registro;
    }

    public void setRegistro(BigDecimal registro) {
        this.registro = (registro == null ? null : registro.intValue());
    }

    @Override
    public String toString() {
        return "PoblacionDTO{" + "fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", ejecucion=" + ejecucion + ", tabla=" + tabla + ", registro=" + registro + ", idProtocolo=" + idProtocolo + '}';
    }

}
