/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de ficha", value = "fichas")
public class Fichas {

    @JsonProperty(value = "fichas")
    @ApiModelProperty(notes = "fichas")
    private List<Ficha> fichas;

    public Fichas(List<Ficha> fichas) {
        this.fichas = fichas;
    }

    public List<Ficha> getFichas() {
        return fichas;
    }

    public void setFichas(List<Ficha> fichas) {
        this.fichas = fichas;
    }

    @Override
    public String toString() {
        return "Fichas{" + "fichas=" + fichas + '}';
    }

}
