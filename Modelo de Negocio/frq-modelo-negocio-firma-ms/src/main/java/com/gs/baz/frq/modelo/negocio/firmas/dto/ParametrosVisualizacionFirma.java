/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosVisualizacionFirma {

    @JsonProperty(value = "idFirma")
    private transient Integer idFirma;

    @JsonProperty(value = "idFolio")
    private transient Integer idFolio;

    @JsonProperty(value = "visualizado")
    @ApiModelProperty(notes = "Estado de visualización de la firma", example = "true", required = true)
    @NotNull
    private Boolean visualizado;

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Boolean getVisualizado() {
        return visualizado;
    }

    public void setVisualizado(Boolean visualizado) {
        this.visualizado = visualizado;
    }

    @Override
    public String toString() {
        return "ParametrosVisualizacionFirma{" + "idFirma=" + idFirma + ", idFolio=" + idFolio + ", visualizado=" + visualizado + '}';
    }

}
