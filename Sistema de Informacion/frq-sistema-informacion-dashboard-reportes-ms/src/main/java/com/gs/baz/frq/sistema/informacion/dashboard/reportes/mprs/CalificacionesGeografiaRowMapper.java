/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.CalificacionGeografia;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionesGeografiaRowMapper implements RowMapper<CalificacionGeografia> {

    @Override
    public CalificacionGeografia mapRow(ResultSet rs, int rowNum) throws SQLException {
        CalificacionGeografia calificacionGeografia = new CalificacionGeografia();
        calificacionGeografia.setCentroCostos(rs.getInt("ID_CECO"));
        calificacionGeografia.setDescripcion(rs.getString("DESCRIPCION_CECO"));
        calificacionGeografia.setCalificacion(rs.getInt("CALIFICACION"));
        calificacionGeografia.setPuntosVenta(rs.getInt("TOTAL_PDV"));
        return calificacionGeografia;
    }
}
