/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g001;

import com.gs.baz.frq.dashboard.dao.graficos.g001.DashboardDAOImpl001;
import com.gs.baz.frq.dashboard.dto.graficos.g001.CalificacionFinalDisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard001 {

    @Autowired
    private DashboardDAOImpl001 dashboardDAOImpl001;

    @RequestMapping(value = "secure/grafico/get/calificacion/total/disciplina/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CalificacionFinalDisciplinaDTO getCalificacionTotalDisciplina(@RequestBody CalificacionFinalDisciplinaDTO calificacionDisciplinaDTO) throws CustomException {
        return dashboardDAOImpl001.selectRowsCalificacionTotalDisciplina(calificacionDisciplinaDTO);
    }

}
