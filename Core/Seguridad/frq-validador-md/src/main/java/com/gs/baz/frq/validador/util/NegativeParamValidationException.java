/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

/**
 *
 * @author cescobarh
 */
public class NegativeParamValidationException extends Exception {

    private String name;
    private String value;

    public NegativeParamValidationException() {
        super();
    }

    public NegativeParamValidationException(String message) {
        super(message);
    }

    public NegativeParamValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NegativeParamValidationException(Throwable cause) {
        super(cause);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
