/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.model.bucket.client.services.dto.FormatoDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFormato;
import com.gs.baz.punto.contacto.dao.PuntoContactoDAOImpl;
import com.gs.baz.punto.contacto.dto.FranquiciaDTO;
import com.gs.baz.punto.contacto.dto.ModeloDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoCategoriaDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFormatoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFranquiciaModeloDTO;
import com.gs.baz.punto.contacto.dto.TipoFormatoDTO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

/**
 *
 * @author cescobarh
 */
public class ReadExcelUtil {

    public Resource readExcel(SubServiceFormato subServiceFormato, PuntoContactoDAOImpl puntoContactoDAOImpl, InputStream inputStream, boolean isXLSX, String noEmpleado) throws IOException {
        PuntoContactoDTO puntoContactoDTO;
        PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO = null;
        Workbook workbook;
        if (isXLSX) {
            workbook = new XSSFWorkbook(inputStream);

        } else {
            workbook = new HSSFWorkbook(inputStream);
        }
        CreationHelper factory = workbook.getCreationHelper();
        Sheet sheet = workbook.getSheetAt(0);
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        Row row;
        Cell cell;
        Cell outCell;
        String objectValue;
        List<PuntoContactoCategoriaDTO> categorias;
        for (int r = 0; rowCount > r; r++) {
            if (r > 0) {
                row = sheet.getRow(r);
                puntoContactoDTO = new PuntoContactoDTO();
                boolean isPuntoContactoRegistrado = true;
                for (int c = 0; row.getLastCellNum() > c; c++) {
                    cell = row.getCell(c);
                    if (cell == null) {
                        cell = row.createCell(c);
                        cell.setBlank();
                    }
                    objectValue = this.getCellStringValue(cell);
                    if (c == 0) {
                        try {
                            String ceco = this.getCellStringValue(row.getCell(1));
                            puntoContactoDTO = puntoContactoDAOImpl.selectRow(ceco);
                            if (puntoContactoDTO == null) {
                                throw new CustomException(ModelCodes.DATA_NOT_FOUND.detalle("idPuntoContacto null"));
                            }
                            puntoContactoFranquiciaModeloDTO = puntoContactoDTO.getPuntoContactoFranquiciaModeloDTO();
                            cell.setCellValue(puntoContactoDTO.getIdPuntoContacto());
                        } catch (CustomException ex) {
                            isPuntoContactoRegistrado = false;
                            Comment comment = cell.getCellComment();
                            ClientAnchor anchor = factory.createClientAnchor();
                            anchor.setCol1(cell.getColumnIndex());
                            anchor.setCol2(cell.getColumnIndex() + 8);
                            anchor.setRow1(row.getRowNum());
                            anchor.setRow2(row.getRowNum() + 8);
                            if (comment == null) {
                                Drawing drawing = sheet.createDrawingPatriarch();
                                comment = drawing.createCellComment(anchor);
                            }
                            RichTextString str = factory.createRichTextString(ex.toString());
                            comment.setString(str);
                            comment.setAuthor("Apache POI");
                            cell.setCellComment(comment);
                        }
                    }
                    if (isPuntoContactoRegistrado) {
                        if (c == 1) {
                            puntoContactoDTO.setCeco(objectValue);
                        }
                        if (c == 2) {

                        }
                        if (c == 3) {
                            puntoContactoFranquiciaModeloDTO.setFranquiciaDTO(new FranquiciaDTO(new BigDecimal(objectValue)));
                        }
                        if (c == 4) {
                            puntoContactoFranquiciaModeloDTO.setModeloDTO(new ModeloDTO(new BigDecimal(objectValue)));
                        }
                        if (c == 5) {
                            objectValue = objectValue.trim();
                            String[] idsCategorias = objectValue.split(",");
                            categorias = new ArrayList<>();
                            for (String idCategoria : idsCategorias) {
                                categorias.add(new PuntoContactoCategoriaDTO(new Integer(idCategoria)));
                            }
                            puntoContactoFranquiciaModeloDTO.setCategorias(categorias);
                        }
                        if (c == 6) {
                            objectValue = objectValue.trim();
                            String[] idsFormatos = objectValue.split(",");
                            List<TipoFormatoDTO> tiposFormato = puntoContactoFranquiciaModeloDTO.getTipoFormatos();
                            for (String idFormatoString : idsFormatos) {
                                Integer idFormato = new Integer(idFormatoString);
                                try {
                                    FormatoDTO formatoDTO = subServiceFormato.getFormato(idFormato);
                                    Integer idTipoFormato = formatoDTO.getIdTipoFormato();
                                    TipoFormatoDTO tipoFormatoDTO = tiposFormato.stream().filter(item -> item.getIdTipoFormato() == idTipoFormato).findFirst().orElse(null);
                                    if (tipoFormatoDTO != null) {
                                        tipoFormatoDTO.setPuntoContactoFormatoDTO(new PuntoContactoFormatoDTO(idFormato));
                                    }
                                } catch (CustomException ex) {
                                    Logger.getLogger(ReadExcelUtil.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                        puntoContactoFranquiciaModeloDTO.setIdEmpleado(new BigDecimal(noEmpleado));
                    }
                }
                if (isPuntoContactoRegistrado) {
                    outCell = row.createCell(7);
                    try {
                        puntoContactoDTO.setPuntoContactoFranquiciaModeloDTO(puntoContactoFranquiciaModeloDTO);
                        puntoContactoDTO = puntoContactoDAOImpl.cuRow(puntoContactoDTO);
                        if (puntoContactoFranquiciaModeloDTO.getInserted() != null && puntoContactoFranquiciaModeloDTO.getInserted()) {
                            outCell.setCellValue("INSERT OK");
                        } else if (puntoContactoFranquiciaModeloDTO.getUpdated() != null && puntoContactoFranquiciaModeloDTO.getUpdated()) {
                            outCell.setCellValue("UPDATE OK");
                        } else {
                            outCell.setCellValue("FAIL ON INSER OR UPDATE");
                        }
                    } catch (CustomException ex) {
                        outCell.setCellValue(ex.toString());
                    }
                }
            } else {
                row = sheet.getRow(r);
                outCell = row.createCell(7);
                outCell.setCellValue("SALIDA");
            }
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
        Resource resource = new ByteArrayResource(outputStream.toByteArray());
        return resource;
    }

    private String getCellStringValue(Cell cell) {
        if (cell.getCellType().equals(CellType.STRING)) {
            return cell.getRichStringCellValue().getString();
        } else if (cell.getCellType().equals(CellType.BLANK)) {
            return cell.getRichStringCellValue().getString();
        } else if (cell.getCellType().equals(CellType.NUMERIC)) {
            if (DateUtil.isCellDateFormatted(cell)) {
                return cell.getDateCellValue().toString();
            } else {
                return new Double(cell.getNumericCellValue()).intValue() + "";
            }
        } else if (cell.getCellType().equals(CellType.BOOLEAN)) {
            return cell.getBooleanCellValue() + "";
        } else if (cell.getCellType().equals(CellType.FORMULA)) {
            return cell.getCellFormula();
        } else {
            return null;
        }
    }
}
