/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosVista {

    @JsonProperty(value = "idVista")
    private transient Integer idVista;

    @JsonProperty(value = "idVistaPadre")
    @ApiModelProperty(notes = "Identificador de la vista padre", example = "1")
    @NotNull
    private Integer idVistaPadre;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del vista", example = "Plataforma de Franquicia")
    @NotEmpty
    private String nombre;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del vista", example = "Plataforma de Franquicia")
    @NotEmpty
    private String descripcion;

    @JsonProperty(value = "imagen")
    @ApiModelProperty(notes = "Imagen de la vista", example = "@/assets/img/")
    @NotEmpty
    private String imagen;

    @JsonProperty(value = "idRuta")
    @ApiModelProperty(notes = "Identificador de la ruta")
    @NotNull
    private Integer idRuta;

    public Integer getIdVista() {
        return idVista;
    }

    public void setIdVista(Integer idVista) {
        this.idVista = idVista;
    }

    public Integer getIdVistaPadre() {
        return idVistaPadre;
    }

    public void setIdVistaPadre(Integer idVistaPadre) {
        this.idVistaPadre = idVistaPadre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    @Override
    public String toString() {
        return "ParametrosVista{" + "idVista=" + idVista + ", idVistaPadre=" + idVistaPadre + ", nombre=" + nombre + ", descripcion=" + descripcion + ", imagen=" + imagen + ", idRuta=" + idRuta + '}';
    }

}
