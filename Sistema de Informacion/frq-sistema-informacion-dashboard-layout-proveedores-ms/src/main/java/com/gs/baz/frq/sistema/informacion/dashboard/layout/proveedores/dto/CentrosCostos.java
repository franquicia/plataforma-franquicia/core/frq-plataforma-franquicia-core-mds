/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de Centros de Costos", value = "CentrosCostos")
public class CentrosCostos {

    @JsonProperty(value = "centrosCostos")
    @ApiModelProperty(notes = "centrosCostos")
    private List<CentroCostos> centroCostos;

    public CentrosCostos(List<CentroCostos> centroCostos) {
        this.centroCostos = centroCostos;
    }

    public List<CentroCostos> getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(List<CentroCostos> centroCostos) {
        this.centroCostos = centroCostos;
    }

    @Override
    public String toString() {
        return "CentrosCostos{" + "centroCostos=" + centroCostos + '}';
    }

}
