package com.gs.baz.frq.negocio.version.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.negocio.version.dto.NegocioVersionDTO;

/**
 *
 * @author cescobarh
 */
public class NegociosCertificacionRowMapper implements RowMapper<NegocioVersionDTO> {

    private NegocioVersionDTO status;

    @Override
    public NegocioVersionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new NegocioVersionDTO();

        status.setIdNegocio(rs.getInt("FIID_NEGOCIO"));;
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        status.setAcronimo(rs.getString("FCACRONIMO"));
        status.setStatus(rs.getInt("FIACTIVO"));
        status.setFecha(rs.getString("FCPERIODO"));

        return status;
    }
}
