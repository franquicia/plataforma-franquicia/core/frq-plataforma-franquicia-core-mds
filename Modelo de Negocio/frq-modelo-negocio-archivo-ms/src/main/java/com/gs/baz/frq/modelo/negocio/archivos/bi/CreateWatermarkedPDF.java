package com.gs.baz.frq.modelo.negocio.archivos.bi;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;

@Component
public class CreateWatermarkedPDF {

    public byte[] addWatermark(Path path) {
        try {
            PdfReader reader = new PdfReader(path.toUri().toURL());
            ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
            PdfStamper stamper = new PdfStamper(reader, bOutput);

            // text watermark
            // properties
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;
            // loop over every page
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSizeWithRotation(i);
                Font font = new Font(Font.FontFamily.HELVETICA, pagesize.getHeight() / 10, Font.NORMAL, new GrayColor(0.5f));
                Phrase p = new Phrase("C O N F I D E N C I A L", font);
                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                // set transparency
                PdfGState state = new PdfGState();
                state.setFillOpacity(0.2f);
                over.setGState(state);
                ColumnText.showTextAligned(over, Element.ALIGN_CENTER, p, x + pagesize.getHeight() / 20, y, pagesize.getHeight() / 15);
                over.restoreState();
            }
            stamper.close();
            reader.close();
            return bOutput.toByteArray();
        } catch (IOException | DocumentException ex) {
            Logger.getLogger(CreateWatermarkedPDF.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
