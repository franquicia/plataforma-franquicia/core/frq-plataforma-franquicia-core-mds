package com.gs.baz.punto.contacto.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import com.gs.baz.punto.contacto.dao.PuntoContactoCategoriaDAOImpl;
import com.gs.baz.punto.contacto.dao.PuntoContactoDAOImpl;
import com.gs.baz.punto.contacto.dao.PuntoContactoDetalleDAOImpl;
import com.gs.baz.punto.contacto.dao.PuntoContactoFormatoDAOImpl;
import com.gs.baz.punto.contacto.dao.PuntoContactoFranquiciaModeloDAOImpl;
import com.gs.baz.punto.contacto.dao.TipoFormatoDAOImpl;
import com.gs.baz.punto.contacto.util.ReadExcelUtil;
import javax.servlet.MultipartConfigElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

/**
 *
 * @author cescobarh
 */
@Configuration
@Import(ConfigBucketServices.class)
@ComponentScan("com.gs.baz.punto.contacto")
public class ConfigPuntoContacto {

    private final Logger logger = LogManager.getLogger();

    public ConfigPuntoContacto() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        DataSize dataSize = DataSize.of(10, DataUnit.MEGABYTES);
        factory.setMaxFileSize(dataSize);
        factory.setMaxRequestSize(dataSize);
        return factory.createMultipartConfig();
    }

    @Bean(initMethod = "init")
    public PuntoContactoDAOImpl puntoContactoDAOImpl() {
        return new PuntoContactoDAOImpl();
    }

    @Bean(initMethod = "init")
    public PuntoContactoCategoriaDAOImpl puntoContactoCategoriaDAOImpl() {
        return new PuntoContactoCategoriaDAOImpl();
    }

    @Bean(initMethod = "init")
    public PuntoContactoDetalleDAOImpl puntoContactoDetalleDAOImpl() {
        return new PuntoContactoDetalleDAOImpl();
    }

    @Bean(initMethod = "init")
    public PuntoContactoFormatoDAOImpl puntoContactoFormatoDAOImpl() {
        return new PuntoContactoFormatoDAOImpl();
    }

    @Bean(initMethod = "init")
    public PuntoContactoFranquiciaModeloDAOImpl puntoContactoFranquiciaModeloDAOImpl() {
        return new PuntoContactoFranquiciaModeloDAOImpl();
    }

    @Bean(initMethod = "init")
    public TipoFormatoDAOImpl tipoFormatoDAOImpl() {
        return new TipoFormatoDAOImpl();
    }

    @Bean
    public ReadExcelUtil readExcelUtil() {
        return new ReadExcelUtil();
    }
}
