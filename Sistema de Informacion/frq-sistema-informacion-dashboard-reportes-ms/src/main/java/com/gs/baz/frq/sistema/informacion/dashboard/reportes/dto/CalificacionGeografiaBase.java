/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la calificacion de geografia base", value = "CalificacionGeografiaBase")
public class CalificacionGeografiaBase {

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Centro de Costos", example = "1")
    private Integer centroCostos;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la pregunta", example = "BAJIO")
    private String descripcion;

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Puntos de venta", example = "1")
    private Integer puntosVenta;

    public Integer getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(Integer centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    @Override
    public String toString() {
        return "CalificacionGeografiaBase{" + "centroCostos=" + centroCostos + ", descripcion=" + descripcion + ", puntosVenta=" + puntosVenta + '}';
    }

}
