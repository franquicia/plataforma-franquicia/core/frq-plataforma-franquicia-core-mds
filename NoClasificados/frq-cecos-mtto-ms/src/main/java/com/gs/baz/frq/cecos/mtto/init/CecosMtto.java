package com.gs.baz.frq.cecos.mtto.init;

import com.gs.baz.frq.cecos.mtto.dao.CecosMttoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.cecos.mtto")
public class CecosMtto {

    private final Logger logger = LogManager.getLogger();

    public CecosMtto() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqCecosMttoDAOImpl")
    public CecosMttoDAOImpl cecosMttoDAOImpl() {
        return new CecosMttoDAOImpl();
    }
}
