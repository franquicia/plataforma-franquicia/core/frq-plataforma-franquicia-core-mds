/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.mprs;

import com.gs.baz.frq.api.portal.cautivo.dto.ICUDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author MADA
 */
public class RespuestaICURowMapper implements RowMapper<ICUDTO> {

    @Override
    public ICUDTO mapRow(ResultSet rs, int i) throws SQLException {

        ICUDTO gobiernoDatosDTO = new ICUDTO();

        gobiernoDatosDTO.setSucursal(rs.getString("FCSUCURSAL") == null ? "" : rs.getString("FCSUCURSAL"));
        gobiernoDatosDTO.setMac(rs.getString("FCMAC") == null ? "" : rs.getString("FCMAC"));
        gobiernoDatosDTO.setIdClienteUnico(rs.getString("FCCLIENTE_ID") == null ? "" : rs.getString("FCCLIENTE_ID"));
        gobiernoDatosDTO.setInicioSesion(rs.getString("FDINICIO_SESION") == null ? "" : rs.getString("FDINICIO_SESION"));
        gobiernoDatosDTO.setUsername(rs.getString("FCUSERNAME") == null ? "" : rs.getString("FCUSERNAME"));

        return gobiernoDatosDTO;
    }

}
