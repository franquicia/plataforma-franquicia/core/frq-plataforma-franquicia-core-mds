/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UsuarioRowMapper implements RowMapper<Empleado> {

    private Empleado usuario;

    @Override
    public Empleado mapRow(ResultSet rs, int rowNum) throws SQLException {
        usuario = new Empleado();
        usuario.setNumeroEmpleado(rs.getInt("FIID_EMPLEADO"));
        usuario.setNombre(rs.getString("FCNOMBRE"));
        usuario.setCorreo(rs.getString("FCCORREO"));
        return usuario;
    }
}
