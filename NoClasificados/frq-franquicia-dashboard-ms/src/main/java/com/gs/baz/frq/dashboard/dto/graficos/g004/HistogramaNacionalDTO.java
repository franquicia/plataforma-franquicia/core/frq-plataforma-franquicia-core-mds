/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g004;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class HistogramaNacionalDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_histograma")
    private Integer idHistograma;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_final")
    private Integer calificacionFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "frecuencia")
    private Integer frecuencia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public Integer getIdHistograma() {
        return idHistograma;
    }

    public void setIdHistograma(BigDecimal idHistograma) {
        this.idHistograma = (idHistograma == null ? null : idHistograma.intValue());
    }

    public Integer getCalificacionFinal() {
        return calificacionFinal;
    }

    public void setCalificacionFinal(BigDecimal calificacionFinal) {
        this.calificacionFinal = (calificacionFinal == null ? null : calificacionFinal.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    public Integer getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(BigDecimal frecuencia) {
        this.frecuencia = (frecuencia == null ? null : frecuencia.intValue());
    }

    @Override
    public String toString() {
        return "HistogramaNacionalDTO{" + "idHistograma=" + idHistograma + ", calificacionFinal=" + calificacionFinal + ", frecuencia=" + frecuencia + ", filtros=" + filtros + '}';
    }

}
