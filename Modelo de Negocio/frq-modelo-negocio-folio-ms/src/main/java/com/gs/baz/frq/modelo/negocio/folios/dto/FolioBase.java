/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de folio", value = "FolioBase")
public class FolioBase {

    @JsonProperty(value = "estado")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Datos del estado del folio")
    protected EstadoFolio estado;

    @JsonProperty(value = "fechaCreacion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Fecha en la que se generó el documento", example = "01-03-2021")
    protected String fechaCreacion;

    @JsonProperty(value = "indicePaso")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Indice del paso de la creacion del documento", example = "1")
    protected Integer indicePaso;

    @JsonProperty(value = "fechaModificacion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Fecha en la que se modifico el folio", example = "01-03-2021")
    protected String fechaModificacion;

    @JsonProperty(value = "documento")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Datos del documento")
    protected Documento documento;

    @JsonProperty(value = "etiquetas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "etiquetas")
    protected List<Etiqueta> etiquetas;

    @JsonProperty(value = "rutas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "rutas")
    protected List<Ruta> rutas;

    @JsonProperty(value = "firmas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "firmas")
    protected Firmas firmas;

    @JsonProperty(value = "creador")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Datos del creador del documento")
    protected Empleado creador;

    public FolioBase() {
    }

    public FolioBase(FolioBase folioBase) {
    }

    public EstadoFolio getEstado() {
        return estado;
    }

    public void setEstado(EstadoFolio estado) {
        this.estado = estado;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getIndicePaso() {
        return indicePaso;
    }

    public void setIndicePaso(Integer indicePaso) {
        this.indicePaso = indicePaso;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public List<Etiqueta> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(List<Etiqueta> etiquetas) {
        this.etiquetas = etiquetas;
    }

    public List<Ruta> getRutas() {
        return rutas;
    }

    public void setRutas(List<Ruta> rutas) {
        this.rutas = rutas;
    }

    public Firmas getFirmas() {
        return firmas;
    }

    public void setFirmas(Firmas firmas) {
        this.firmas = firmas;
    }

    public Empleado getCreador() {
        return creador;
    }

    public void setCreador(Empleado creador) {
        this.creador = creador;
    }

    @Override
    public String toString() {
        return "FolioBase{" + "estado=" + estado + ", fechaCreacion=" + fechaCreacion + ", indicePaso=" + indicePaso + ", fechaModificacion=" + fechaModificacion + ", documento=" + documento + ", etiquetas=" + etiquetas + ", rutas=" + rutas + ", firmas=" + firmas + ", creador=" + creador + '}';
    }

}
