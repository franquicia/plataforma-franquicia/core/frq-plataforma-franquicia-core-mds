package com.gs.baz.frq.negocio.version.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.negocio.version.dto.NegocioVersionDTO;

/**
 *
 * @author cescobarh
 */
public class UsuariosCertificacionRowMapper implements RowMapper<NegocioVersionDTO> {

    private NegocioVersionDTO status;

    @Override
    public NegocioVersionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new NegocioVersionDTO();

        status.setIdUsuario(rs.getInt("FIID_USUARIO"));;
        status.setIdPuesto(rs.getInt("FIID_PUESTO"));
        status.setNombreUsuario(rs.getString("FCNOMBRE").split("-")[0]+"-"+rs.getString("FCNOMBRE").split("-")[1]);


        return status;
    }
}
