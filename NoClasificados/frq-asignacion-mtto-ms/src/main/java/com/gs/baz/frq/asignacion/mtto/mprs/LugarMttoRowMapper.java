package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.LugarMttoDTO;

/**
 *
 * @author cescobarh
 */
public class LugarMttoRowMapper implements RowMapper<LugarMttoDTO> {

    private LugarMttoDTO status;

    @Override
    public LugarMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new LugarMttoDTO();

        status.setidLugar(rs.getInt("FIID_LUGAR"));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        status.setStatus(rs.getInt("FISTATUS"));
        status.setIdTipoProyecto(rs.getInt("FIID_TIPOPROY"));
        status.setObservaciones(rs.getString("FCOBSERVACIONES"));
        status.setPeriodo(rs.getString("FCPERIODO"));

        return status;
    }
}
