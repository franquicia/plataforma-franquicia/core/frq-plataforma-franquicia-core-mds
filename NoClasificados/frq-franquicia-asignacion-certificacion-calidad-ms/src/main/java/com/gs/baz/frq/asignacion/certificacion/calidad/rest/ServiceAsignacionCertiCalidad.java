/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.certificacion.calidad.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.asignacion.certificacion.calidad.dao.AsignacionCertiCalidadDAOImpl;
import com.gs.baz.frq.asignacion.certificacion.calidad.dto.AsignacionCertiCalidadDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/asignacion/certificacion/calidad")

public class ServiceAsignacionCertiCalidad {

    @Autowired
    private AsignacionCertiCalidadDAOImpl asignacionCertiCalidadDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionCertiCalidadDTO postAsignacionCertiCalidadDAOImpl(@RequestBody AsignacionCertiCalidadDTO asignacionCertificacion) throws CustomException {
        if (asignacionCertificacion.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }

        return asignacionCertiCalidadDAOImpl.insertRow(asignacionCertificacion);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionCertiCalidadDTO putAsignacionCertiCalidadDAOImpl(@RequestBody AsignacionCertiCalidadDTO asignacionCertificacion) throws CustomException {
        if (asignacionCertificacion.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }
        return asignacionCertiCalidadDAOImpl.updateRow(asignacionCertificacion);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionCertiCalidadDTO> getAsignacionCertiCalidadDAOImpl() throws CustomException {
        return asignacionCertiCalidadDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionCertiCalidadDTO> getRowAsignacionCertiCalidadDAOImpl(
            @RequestParam(value = "idUsuario", required = false) Long idUsuario,
            @RequestParam(value = "idPerfil", required = false) Long idPerfil,
            @RequestParam(value = "idCeco", required = false) Long idCeco,
            @RequestParam(value = "idGerente", required = false) Long idGerente,
            @RequestParam(value = "activo", required = false) Long activo,
            @RequestParam(value = "inicioAsignacion", required = false) Long inicioAsignacion,
            @RequestParam(value = "finAsignacion", required = false) Long finAsignacion,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio) throws CustomException {
        return asignacionCertiCalidadDAOImpl.selectRow(idUsuario, idPerfil, idCeco, idGerente, activo, inicioAsignacion, finAsignacion, idNegocio);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionCertiCalidadDTO deleteAsignacionCertiCalidadDAOImpl(@RequestBody AsignacionCertiCalidadDTO asignacionCertificacion) throws CustomException {
        if (asignacionCertificacion.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }
        if (asignacionCertificacion.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }

        return asignacionCertiCalidadDAOImpl.deleteRow(asignacionCertificacion);
    }

    @RequestMapping(value = "secure/geo/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionCertiCalidadDTO postAsignacionCertiCalidadGeoDAOImpl(@RequestBody AsignacionCertiCalidadDTO asignacionGeoCertificacion) throws CustomException {
        if (asignacionGeoCertificacion.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }

        return asignacionCertiCalidadDAOImpl.insertRowGeo(asignacionGeoCertificacion);
    }

    @RequestMapping(value = "secure/geo/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionCertiCalidadDTO putAsignacionCertiCalidadGeoDAOImpl(@RequestBody AsignacionCertiCalidadDTO asignacionGeoCertificacion) throws CustomException {
        if (asignacionGeoCertificacion.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        return asignacionCertiCalidadDAOImpl.updateRowGeo(asignacionGeoCertificacion);
    }

    @RequestMapping(value = "secure/geo/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionCertiCalidadDTO> getAsignacionCertiCalidadGeoDAOImpl() throws CustomException {
        return asignacionCertiCalidadDAOImpl.selectRowsGeo();
    }

    @RequestMapping(value = "secure/geo/get/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionCertiCalidadDTO> getRowAsignacionCertiCalidadGeoDAOImpl(
            @RequestParam(value = "idCeco", required = false) Long idCeco) throws CustomException {
        return asignacionCertiCalidadDAOImpl.selectRowGeo(idCeco);
    }

    @RequestMapping(value = "secure/geo/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionCertiCalidadDTO deleteAsignacionCertiCalidadGeoDAOImpl(@RequestBody AsignacionCertiCalidadDTO asignacionGeoCertificacion) throws CustomException {

        if (asignacionGeoCertificacion.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }

        return asignacionCertiCalidadDAOImpl.deleteRowGeo(asignacionGeoCertificacion);
    }
}
