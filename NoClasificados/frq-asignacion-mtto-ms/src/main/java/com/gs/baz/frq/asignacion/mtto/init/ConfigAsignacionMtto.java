package com.gs.baz.frq.asignacion.mtto.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.baz.frq.asignacion.mtto.dao.AsignacionMttoDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.asignacion.mtto")
public class ConfigAsignacionMtto {

    private final Logger logger = LogManager.getLogger();

    public ConfigAsignacionMtto() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqAsignacionMttoDAOImpl")
    public AsignacionMttoDAOImpl asignacionMttoDAOImpl() {
        return new AsignacionMttoDAOImpl();
    }
}
