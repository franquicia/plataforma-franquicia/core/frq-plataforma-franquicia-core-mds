package com.gs.baz.protocolo.mprs;

import com.gs.baz.protocolo.dto.ProtocoloDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDisciplinaRowMapper implements RowMapper<ProtocoloDTO> {

    private ProtocoloDTO protocoloDTO;

    @Override
    public ProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        protocoloDTO = new ProtocoloDTO();
        protocoloDTO.setIdProtocoloDisciplina(((BigDecimal) rs.getObject("FIIPROTDIS")));
        protocoloDTO.setIdProtocolo(((BigDecimal) rs.getObject("FIIDPROT")));
        return protocoloDTO;
    }
}
