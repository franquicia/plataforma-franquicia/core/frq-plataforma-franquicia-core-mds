/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Arbol", value = "Arbol")
public class Arbol extends ArbolBase {

    @JsonProperty(value = "idArbol")
    @ApiModelProperty(notes = "Identificador del Arbol", example = "1", position = -1)
    private Integer idArbol;

    public Integer getIdArbol() {
        return idArbol;
    }

    public void setIdArbol(Integer idArbol) {
        this.idArbol = idArbol;
    }

    @Override
    public String toString() {
        return "Arbol{" + "idArbol=" + idArbol + '}';
    }

}
