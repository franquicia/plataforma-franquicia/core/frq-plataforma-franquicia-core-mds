/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author MADA
 */
public class RespuestaICUDTO {

    @JsonProperty(value = "Identificadores de Clientes")
    private List<ICUDTO> icu;

    public List<ICUDTO> getIcu() {
        return icu;
    }

    public void setIcu(List<ICUDTO> icu) {
        this.icu = icu;
    }

    @Override
    public String toString() {
        return "RespuestaICUDTO{" + "icu=" + icu + '}';
    }

}
