package com.gs.baz.frq.respuesta.visita.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.respuesta.visita.dao.RespuestaVisitaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.respuesta.visita")
public class ConfigRespuestaVisita {

    private final Logger logger = LogManager.getLogger();

    public ConfigRespuestaVisita() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "franquiciaRespuestaVisitaDAOImpl")
    public RespuestaVisitaDAOImpl respuestaVisitaDAOImpl() {
        return new RespuestaVisitaDAOImpl();
    }
}
