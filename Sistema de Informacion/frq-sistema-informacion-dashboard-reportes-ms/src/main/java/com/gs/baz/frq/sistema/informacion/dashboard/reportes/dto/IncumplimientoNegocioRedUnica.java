/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del Incumplimiento por negocio red unica", value = "IncumplimientoNegocioRedUnica")
public class IncumplimientoNegocioRedUnica {

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1")
    private Integer idNegocio;

    @JsonProperty(value = "idPregunta")
    @ApiModelProperty(notes = "Identificador de la pregunta", example = "1")
    private Integer idPregunta;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la pregunta", example = "2. ¿El mobiliario del búnker se encuentra en buen estado? (I)")
    private String descripcion;

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Puntos de venta incumplimiento", example = "1")
    private Integer puntosVenta;

    @JsonProperty(value = "porcentaje")
    @ApiModelProperty(notes = "Porcentaje", example = "1")
    private Integer porcentaje;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public String toString() {
        return "IncumplimientoNegocioRedUnica{" + "idNegocio=" + idNegocio + ", idPregunta=" + idPregunta + ", descripcion=" + descripcion + ", puntosVenta=" + puntosVenta + ", porcentaje=" + porcentaje + '}';
    }

}
