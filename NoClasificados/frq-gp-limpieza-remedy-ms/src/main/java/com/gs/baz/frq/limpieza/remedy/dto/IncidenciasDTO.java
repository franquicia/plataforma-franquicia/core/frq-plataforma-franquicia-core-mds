package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Detalle de la Incidencia", value = "Incidencias")
public class IncidenciasDTO {

    @JsonProperty(value = "idTipo")
    @ApiModelProperty(notes = "Clave del tipo de incidencia", example = "202", position = -1)
    private Integer idTipo;

    @JsonProperty(value = "servicio")
    @ApiModelProperty(notes = "Servicio que se requiere", example = "AIRE ACONDICIONADO")
    private String servicio;

    @JsonProperty(value = "ubicacionIncidencia")
    @ApiModelProperty(notes = "La ubicación donde se encuentra la incidencia", example = "AREA FINANCIERA")
    private String ubicacion;

    @JsonProperty(value = "incidencia")
    @ApiModelProperty(notes = "La incidencia que se está reportando", example = "EL EQUIPO HACE RUIDO")
    private String incidencia;

    @JsonProperty(value = "plantilla")
    @ApiModelProperty(notes = "La plantilla que le corresponde al incidente",
            example = "SUC-AIRE ACONDICIONADO-AREA FINANCIERA-EL EQUIPO HACE RUIDO")
    private String plantilla;

    @JsonProperty(value = "estatus")
    @ApiModelProperty(notes = "Estatus actual de la incidencia", example = "Activado")
    private String status;

    public IncidenciasDTO() {
        super();

    }

    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "IncidenciasDTO{" + "idTipo=" + idTipo + ", servicio=" + servicio
                + ", ubicacion=" + ubicacion + ", incidencia=" + incidencia
                + ", plantilla=" + plantilla + ", status=" + status + '}';
    }

}
