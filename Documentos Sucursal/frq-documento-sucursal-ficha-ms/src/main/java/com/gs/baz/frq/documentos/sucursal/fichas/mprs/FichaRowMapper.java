/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.mprs;

import com.gs.baz.frq.documentos.sucursal.fichas.dto.FichaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FichaRowMapper implements RowMapper<FichaBase> {

    private FichaBase fichaBase;

    @Override
    public FichaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        fichaBase = new FichaBase();
        fichaBase.setIdCeco(rs.getInt("FCID_CECO"));
        fichaBase.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        fichaBase.setIdFormato(rs.getInt("FIID_FORMATO"));
        fichaBase.setIdRegion(rs.getInt("FIID_REGION"));
        fichaBase.setRegion(rs.getString("FCREGION"));
        fichaBase.setIdZona(rs.getInt("FIID_ZONA"));
        fichaBase.setZona(rs.getString("FCZONA"));
        fichaBase.setIdTerritorio(rs.getInt("FIID_TERRITORIO"));
        fichaBase.setTerritorio(rs.getString("FCTERRITORIO"));
        fichaBase.setLatitud(rs.getString("FCLATITUD"));
        fichaBase.setLongitud(rs.getString("FCLONGITUD"));
        fichaBase.setDireccion(rs.getString("FCDIRECCION"));
        fichaBase.setIdFolio(rs.getInt("FIID_FOLIO"));
        return fichaBase;
    }
}
