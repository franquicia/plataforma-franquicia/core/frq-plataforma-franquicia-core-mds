package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.LogDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.LogRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LogDAOImpl extends DefaultDAO {

    DefaultJdbcCall jdbcObtieneErrores;
    private DefaultJdbcCall jdbcInsertaUsuario;
    private static final Logger LOGGER = LogManager.getLogger();

    public LogDAOImpl() {
        super();

    }

    public void init() {

        jdbcObtieneErrores = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMERRORES")
                .withProcedureName("SP_SEL_ERRORES")
                .returningResultSet("RCL_ERRORES", new LogRowMapper());

        jdbcInsertaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PA_LOGRESULTADOS")
                .withProcedureName("SP_REGISTRAERR");
    }

    @SuppressWarnings("all")
    public List<LogDTO> obtieneErrores(String fecha) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<LogDTO> listaErrores = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA", fecha);
        out = jdbcObtieneErrores.execute(in);

        LOGGER.info("Funcion ejecutada: {FRANQUICIA.PAADMERRORES.SP_SEL_ERRORES}");

        listaErrores = (List<LogDTO>) out.get("RCL_ERRORES");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            LOGGER.info("Algo ocurrió al obtener los Errores en la fecha: " + fecha);
        } else {
            return listaErrores;
        }

        return null;

    }

    @SuppressWarnings("all")
    public boolean inserta(int codigo, String mensaje, String origen) {

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NCODIGOERROR", codigo)
                .addValue("PA_VMENSAJEERR", mensaje)
                .addValue("PA_VORIGENERR", origen);

        out = jdbcInsertaUsuario.execute(in);

        LOGGER.info("Función ejecutada:{migestion.PA_LOGRESULTADOS}");

        return true;
    }
}
