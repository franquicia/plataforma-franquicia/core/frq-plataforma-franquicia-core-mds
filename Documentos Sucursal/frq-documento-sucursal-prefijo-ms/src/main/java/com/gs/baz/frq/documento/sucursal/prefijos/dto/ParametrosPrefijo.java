/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosPrefijo {

    @JsonProperty(value = "idNegocioPrefijo")
    private transient Integer idNegocioPrefijo;

    @JsonProperty(value = "prefijo", required = true)
    @ApiModelProperty(notes = "Descripcion nombre del prefijo", example = "23", required = true)
    private String prefijo;

    @JsonProperty(value = "idNegocio", required = true)
    @ApiModelProperty(notes = "Identificador del negocio", example = "1", required = true)
    private Integer idNegocio;

    public Integer getIdNegocioPrefijo() {
        return idNegocioPrefijo;
    }

    public void setIdNegocioPrefijo(Integer idNegocioPrefijo) {
        this.idNegocioPrefijo = idNegocioPrefijo;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }
}
