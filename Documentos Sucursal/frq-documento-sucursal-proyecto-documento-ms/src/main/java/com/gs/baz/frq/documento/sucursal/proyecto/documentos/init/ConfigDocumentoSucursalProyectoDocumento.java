package com.gs.baz.frq.documento.sucursal.proyecto.documentos.init;

import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dao.ProyectoDocumentoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.proyecto.documentos")
public class ConfigDocumentoSucursalProyectoDocumento {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalProyectoDocumento() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ProyectoDocumentoDAOImpl documentoSucursalProyectoDocumentoDAOImpl() {
        return new ProyectoDocumentoDAOImpl();
    }

}
