package com.gs.baz.punto.contacto.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.punto.contacto.dto.PuntoContactoDetalleDTO;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoDetalleDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINDETALLPC");
        jdbcSelect.withProcedureName("SP_SEL_DETPC");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoRowMapper().new PuntoContactoDetalleRowMapper());
    }

    public PuntoContactoDetalleDTO selectRow(String ceco) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCECO", null);
        mapSqlParameterSource.addValue("PA_FCCECO", ceco);
        mapSqlParameterSource.addValue("PA_FIPREFCC", null);
        mapSqlParameterSource.addValue("PA_FINUMECO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoDetalleDTO> data = (List<PuntoContactoDetalleDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

}
