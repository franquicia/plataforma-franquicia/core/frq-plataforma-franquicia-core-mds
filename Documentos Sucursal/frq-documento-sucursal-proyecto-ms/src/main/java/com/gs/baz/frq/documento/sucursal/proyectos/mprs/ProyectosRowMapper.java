/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyectos.mprs;

import com.gs.baz.frq.documento.sucursal.proyectos.dto.Proyecto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ProyectosRowMapper implements RowMapper<Proyecto> {

    private Proyecto proyecto;

    @Override
    public Proyecto mapRow(ResultSet rs, int rowNum) throws SQLException {
        proyecto = new Proyecto();
        proyecto.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        proyecto.setDescripcion(rs.getString("FCDESCRIPCION"));
        return proyecto;
    }
}
