/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.mprs;

import com.gs.baz.frq.modelo.negocio.etiqueta.dto.FolioEtiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.UnidadNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FoliosEtiquetaRowMapper implements RowMapper<FolioEtiqueta> {

    private FolioEtiqueta folioEtiqueta;

    @Override
    public FolioEtiqueta mapRow(ResultSet rs, int rowNum) throws SQLException {
        folioEtiqueta = new FolioEtiqueta();
        folioEtiqueta.setIdFolio(rs.getInt("FIIDFOLIO"));
        folioEtiqueta.setTituloDocumento(rs.getString("FCTITULO"));
        folioEtiqueta.setCreadorDocumento(rs.getString("CREADOR"));
        folioEtiqueta.setEstadoDocumento(rs.getString("ESTATUS"));
        List<UnidadNegocio> listaUnidadNegocio = new ArrayList<>();
        String[] arrayUnidades = rs.getString("UNIDADES_NEGOCIO").split(",");
        for (String unidades : arrayUnidades) {
            UnidadNegocio unidadNegocio = new UnidadNegocio();
            unidadNegocio.setDescripcion(unidades);
            listaUnidadNegocio.add(unidadNegocio);
        }
        folioEtiqueta.setUnidadesNegocio(listaUnidadNegocio);
        return folioEtiqueta;
    }
}
