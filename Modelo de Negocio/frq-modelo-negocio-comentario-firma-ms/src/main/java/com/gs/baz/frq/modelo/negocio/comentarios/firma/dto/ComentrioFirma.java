/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del comentario", value = "ComentarioFirma")
public class ComentrioFirma extends ComentrioFirmaBase {

    @JsonProperty(value = "idComentario")
    @ApiModelProperty(notes = "Identificador del comentario", example = "1", position = -1)
    private Integer idComentario;

    public Integer getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(Integer idComentario) {
        this.idComentario = idComentario;
    }

    @Override
    public String toString() {
        return "ComentrioFirma{" + "idComentario=" + idComentario + '}';
    }

}
