/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.geografia.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GeografiaMttoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Integer inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idGeografia")
    private Integer idGeografia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idNivel")
    private Integer idNivel;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idNivelPadre")
    private Integer idNivelPadre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuarioResponsable")
    private Integer idResponsable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "observaciones")
    private String observaciones;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "detalle")
    private String detalle;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status")
    private Integer status;

    public Integer getInserted() {
        return inserted;
    }

    public void setInserted(Integer inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Integer getIdGeografia() {
        return idGeografia;
    }

    public void setIdGeografia(Integer idGeografia) {
        this.idGeografia = idGeografia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public Integer getIdNivelPadre() {
        return idNivelPadre;
    }

    public void setIdNivelPadre(Integer idNivelPadre) {
        this.idNivelPadre = idNivelPadre;
    }

    public Integer getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(Integer idResponsable) {
        this.idResponsable = idResponsable;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GeografiaMttoDTO [inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + ", periodo="
                + periodo + ", idGeografia=" + idGeografia + ", descripcion=" + descripcion + ", idNivel=" + idNivel
                + ", idNivelPadre=" + idNivelPadre + ", idResponsable=" + idResponsable + ", observaciones="
                + observaciones + ", detalle=" + detalle + ", status=" + status + "]";
    }

}
