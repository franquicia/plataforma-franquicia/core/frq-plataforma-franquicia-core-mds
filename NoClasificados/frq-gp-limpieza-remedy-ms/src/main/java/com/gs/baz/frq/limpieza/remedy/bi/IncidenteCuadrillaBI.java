package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.IncidenteCuadrillaDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteCuadrillaDTO;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class IncidenteCuadrillaBI {

    @Autowired
    private IncidenteCuadrillaDAOImpl incidenteCuadrillaDAO;
    private static final Logger LOGGER = LogManager.getLogger();

    public IncidenteCuadrillaBI() {
        super();

    }

    public int inserta(IncidenteCuadrillaDTO bean) {
        int respuesta = 0;

        try {
            respuesta = incidenteCuadrillaDAO.inserta(bean);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible insertar ", e);

        }

        return respuesta;
    }

    public boolean elimina(String incidente) {
        boolean respuesta = false;

        try {
            respuesta = incidenteCuadrillaDAO.elimina(incidente);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible eliminar", e);

        }

        return respuesta;
    }

    public List<IncidenteCuadrillaDTO> obtieneCuadrillaTK(int idCuadrilla, int idProveedor, int zona) {
        List<IncidenteCuadrillaDTO> listafila = null;
        try {
            listafila = incidenteCuadrillaDAO.obtieneCuadrillaTK(idCuadrilla, idProveedor, zona);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener los datos", e);

        }

        return listafila;
    }

    public List<IncidenteCuadrillaDTO> obtieneDatos(int idCuadrilla) {
        List<IncidenteCuadrillaDTO> listafila = null;
        try {
            listafila = incidenteCuadrillaDAO.obtieneDatos(idCuadrilla);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener los datos", e);

        }

        return listafila;
    }

    public List<IncidenteCuadrillaDTO> obtieneInfo() {
        List<IncidenteCuadrillaDTO> listafila = null;
        try {
            listafila = incidenteCuadrillaDAO.obtieneInfo();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla Tk Cuadrilla", e);

        }

        return listafila;
    }

    public boolean actualiza(IncidenteCuadrillaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = incidenteCuadrillaDAO.actualiza(bean);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible actualizar", e);

        }

        return respuesta;
    }

}
