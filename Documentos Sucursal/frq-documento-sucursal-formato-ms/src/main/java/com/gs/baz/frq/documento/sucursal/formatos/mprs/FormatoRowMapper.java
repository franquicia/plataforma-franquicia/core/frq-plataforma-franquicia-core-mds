/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.formatos.mprs;

import com.gs.baz.frq.documento.sucursal.formatos.dto.FormatoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FormatoRowMapper implements RowMapper<FormatoBase> {

    private FormatoBase formatoBase;

    @Override
    public FormatoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        formatoBase = new FormatoBase();
        formatoBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        formatoBase.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        formatoBase.setNombreNegocio(rs.getString("NEGOCIO"));

        return formatoBase;
    }
}
