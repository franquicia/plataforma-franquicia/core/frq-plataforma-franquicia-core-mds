/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.AltaRango;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.ParametrosRango;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.Rango;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.RangoBase;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.mprs.RangoRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.mprs.RangosRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SemaforoRangoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcDelete;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMDASHSEMAF";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSRANGO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTRANGO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELRANGO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETRANGO");
        jdbcSelect.returningResultSet("PA_CDATOS", new RangoRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETRANGO");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new RangosRowMapper());
    }

    public RangoBase selectRow(Integer idSemaforo, Integer idRango) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIRANGO_ID", idRango);
        mapSqlParameterSource.addValue("PA_FIIDSEMAFORO", idSemaforo);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RangoBase> data = (List<RangoBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Rango> selectAllRows(Long idSemaforo) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIRANGO_ID", null);
        mapSqlParameterSource.addValue("PA_FIIDSEMAFORO", idSemaforo);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Rango>) out.get("PA_CDATOS");
    }

    public AltaRango insertRow(ParametrosRango entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDSEMAFORO", entityDTO.getIdSemaforo());
            mapSqlParameterSource.addValue("PA_FILIMITE_INF", entityDTO.getLimiteInferior());
            mapSqlParameterSource.addValue("PA_FILIMITE_SUP", entityDTO.getLimiteSuperior());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCIONRANGO", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCCOLORRANGO", entityDTO.getColor());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIRANGO_ID");
                return new AltaRango(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosRango entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO.getIdRango());
            mapSqlParameterSource.addValue("PA_FILIMITE_INF", entityDTO.getLimiteInferior());
            mapSqlParameterSource.addValue("PA_FILIMITE_SUP", entityDTO.getLimiteSuperior());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCIONRANGO", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCCOLORRANGO", entityDTO.getColor());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
