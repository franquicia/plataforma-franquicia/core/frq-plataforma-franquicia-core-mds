/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosEstadoFolio {

    @JsonProperty(value = "idEstadoFolio")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1", required = true)
    @NotNull
    private Integer idEstadoFolio;

    public ParametrosEstadoFolio() {
    }

    public ParametrosEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    public Integer getIdEstadoFolio() {
        return idEstadoFolio;
    }

    public void setIdEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    @Override
    public String toString() {
        return "ParametrosEstadoFolio{" + "idEstadoFolio=" + idEstadoFolio + '}';
    }

}
