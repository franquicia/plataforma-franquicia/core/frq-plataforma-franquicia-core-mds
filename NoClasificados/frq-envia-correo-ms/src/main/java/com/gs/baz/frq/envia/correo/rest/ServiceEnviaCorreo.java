/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.envia.correo.rest;

import com.gs.baz.frq.envia.correo.bi.EnviaCorreoBI;
import com.gs.baz.frq.envia.correo.dao.EnviaCorreoDAOImpl;
import com.gs.baz.frq.envia.correo.dto.DatosEntradaCorreoDTO;
import com.gs.baz.frq.envia.correo.dto.DatosEntradaCorreoSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.dto.ResponseReporteLayout;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/api-local/envia-correo/")
@Component("FRQServiceEnviaCorreo")

public class ServiceEnviaCorreo {

    @Autowired
    private EnviaCorreoDAOImpl enviaCorreoDAO;

    @Autowired
    private EnviaCorreoBI enviaCorreoBI;

    private final Logger logger = LogManager.getLogger();

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "Envío de correo", notes = "Servicio para envío de correo", nickname = "enviaCorreo")
    @RequestMapping(value = "/correo/envio/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean enviaCorreo(@ApiParam(name = "InformacionCentroCostos", value = "Objeto de información de Centro de Costos", required = true) @RequestBody DatosEntradaCorreoDTO datosEntradaCorreo) throws CustomException, DataNotFoundException {

        boolean response = true;

        response = enviaCorreoBI.enviaCorreo(datosEntradaCorreo);

        return response;

    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "Envío de correo semanal Layouts", notes = "Servicio para envío de correo semanal Layouts", nickname = "enviaCorreoSemanalLayouts")
    @RequestMapping(value = "/layouts/alta-semanal/correo/envio/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseReporteLayout enviaCorreoSemanalLayouts(@ApiParam(name = "InformacionCentroCostos", value = "Objeto de información de Centro de Costos", required = true) @RequestBody DatosEntradaCorreoSemanalLayoutsDTO datosEntradaCorreoSemanalLayouts) throws CustomException, DataNotFoundException {

        return enviaCorreoBI.enviaCorreoSemanalLayouts(datosEntradaCorreoSemanalLayouts);

    }

}
