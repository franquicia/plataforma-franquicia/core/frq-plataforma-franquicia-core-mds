/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.nivel.consulta.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.nivel.consulta.dao.NivelConsultaDAOImpl;
import com.gs.baz.nivel.consulta.dto.NivelConsultaDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/nivel/consulta")
public class ServiceNivelConsulta {

    @Autowired
    private NivelConsultaDAOImpl nivelConsultaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NivelConsultaDTO> getNivelConsultas() throws CustomException {
        return nivelConsultaDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_nivel_consulta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NivelConsultaDTO getNivelConsulta(@PathVariable("id_nivel_consulta") Long idNivelConsulta) throws CustomException {
        return nivelConsultaDAOImpl.selectRow(idNivelConsulta);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public NivelConsultaDTO postNivelConsulta(@RequestBody NivelConsultaDTO nivelConsulta) throws CustomException {
        if (nivelConsulta.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (nivelConsulta.getIdStatus() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_status"));
        }
        return nivelConsultaDAOImpl.insertRow(nivelConsulta);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public NivelConsultaDTO putNivelConsulta(@RequestBody NivelConsultaDTO nivelConsulta) throws CustomException {
        if (nivelConsulta.getIdNivelConsulta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_nivel_consulta"));
        }
        if (nivelConsulta.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (nivelConsulta.getIdStatus() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_status"));
        }
        return nivelConsultaDAOImpl.updateRow(nivelConsulta);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public NivelConsultaDTO deleteNivelConsulta(@RequestBody NivelConsultaDTO nivelConsulta) throws CustomException {
        if (nivelConsulta.getIdNivelConsulta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_nivel_consulta"));
        }
        return nivelConsultaDAOImpl.deleteRow(nivelConsulta);
    }
}
