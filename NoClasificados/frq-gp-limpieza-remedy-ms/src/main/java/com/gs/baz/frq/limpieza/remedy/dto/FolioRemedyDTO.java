package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Alta de Folio Remedy", value = "Incidente")
public class FolioRemedyDTO {

    @JsonProperty(value = "centroCostos", required = true)
    @ApiModelProperty(notes = "Centro de costos de la sucursal. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "480100", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String ceco;

    @JsonProperty(value = "numeroSucursal", required = true)
    @ApiModelProperty(notes = "Número de la sucursal.", example = "100", required = true)
    private Integer numeroSucursal;

    @JsonProperty(value = "nombreSucursal", required = true)
    @ApiModelProperty(notes = "Nombre de la sucursal", example = "BA MEGA DF LA LUNA", required = true)
    private String nombreSucursal;

    @JsonProperty(value = "telefonoSucursal", required = true)
    @ApiModelProperty(notes = "Teléfono de la sucursal. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "931352", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoSucursal;

    @JsonProperty(value = "correoElectronico", required = true)
    @ApiModelProperty(notes = "Correo del empleado. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "carlos@elektra.com.mx", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String correo;

    @JsonProperty(value = "numeroEmpleado", required = true)
    @ApiModelProperty(notes = "El número de empleado que levantó el folio."
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8` ", example = "202622", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String numeroEmpleado;

    @JsonProperty(value = "nombreEmpleado", required = true)
    @ApiModelProperty(notes = "Nombre del empleado. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "Carlos Antonio Escobar", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String nombre;

    @JsonProperty(value = "puestoEmpleado", required = true)
    @ApiModelProperty(notes = "Puesto del empleado", example = "Encargado de Limpieza", required = true)
    private String puesto;

    @JsonProperty(value = "comentario", required = true)
    @ApiModelProperty(notes = "Comentario o razones por las que se levanta el folio",
            example = "No hay suficiente material de limpieza", required = true)
    private String comentario;

    @JsonProperty(value = "puestoAlterno", required = true)
    @ApiModelProperty(notes = "Puesto del contacto alterno", example = "Auxiliar de limpieza", required = true)
    private String puestoAlterno;

    @JsonProperty(value = "telefonoUsuarioAlterno", required = true)
    @ApiModelProperty(notes = "Teléfono del contacto alterno. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "5524163986", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoUsuarioAlterno;

    @JsonProperty(value = "nombreUsuarioAlterno", required = true)
    @ApiModelProperty(notes = "Nombre del usuario alterno. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "Miguel Angel Delgado", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String nombreUsuarioAlterno;

    @JsonProperty(value = "servicio", required = true)
    @ApiModelProperty(notes = "Servicio para el incidente", example = "Insumos", required = true)
    private String servicio;

    @JsonProperty(value = "ubicacionFalla", required = true)
    @ApiModelProperty(notes = "Ubicación donde sucede la falla", example = "Dispensadores", required = true)
    private String ubicacion;

    @JsonProperty(value = "incidenciaDetalle", required = true)
    @ApiModelProperty(notes = "Especificación de la falla", example = "Aroma", required = true)
    private String incidencia;

    @JsonProperty(value = "nombreAdjunto", required = true)
    @ApiModelProperty(notes = "Nombre del archivo adjunto", example = "Evidencia.png", required = true)
    private String nombreAdjunto;

    @JsonProperty(value = "archivoAdjunto", required = true)
    @ApiModelProperty(notes = "Dato adjunto codificado en Base 64",
            example = "<Archivo generado en base64>", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String adjunto;

    public FolioRemedyDTO() {
        super();

    }

    public Integer getNumeroSucursal() {
        return numeroSucursal;
    }

    public void setNumeroSucursal(Integer numeroSucursal) {
        this.numeroSucursal = numeroSucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getTelefonoSucursal() {
        return telefonoSucursal;
    }

    public void setTelefonoSucursal(String telefonoSucursal) {
        this.telefonoSucursal = telefonoSucursal;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getPuestoAlterno() {
        return puestoAlterno;
    }

    public void setPuestoAlterno(String puestoAlterno) {
        this.puestoAlterno = puestoAlterno;
    }

    public String getTelefonoUsuarioAlterno() {
        return telefonoUsuarioAlterno;
    }

    public void setTelefonoUsuarioAlterno(String telefonoUsuarioAlterno) {
        this.telefonoUsuarioAlterno = telefonoUsuarioAlterno;
    }

    public String getNombreUsuarioAlterno() {
        return nombreUsuarioAlterno;
    }

    public void setNombreUsuarioAlterno(String nombreUsuarioAlterno) {
        this.nombreUsuarioAlterno = nombreUsuarioAlterno;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    public String getNombreAdjunto() {
        return nombreAdjunto;
    }

    public void setNombreAdjunto(String nombreAdjunto) {
        this.nombreAdjunto = nombreAdjunto;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(String numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    @Override
    public String toString() {
        return "FolioRemedyDTO{" + "ceco=" + ceco + ", numeroSucursal=" + numeroSucursal
                + ", nombreSucursal=" + nombreSucursal + ", telefonoSucursal=" + telefonoSucursal
                + ", correo=" + correo + ", numeroEmpleado=" + numeroEmpleado + ", nombre="
                + nombre + ", puesto=" + puesto + ", comentario=" + comentario + ", puestoAlterno="
                + puestoAlterno + ", telefonoUsuarioAlterno=" + telefonoUsuarioAlterno + ", nombreUsuarioAlterno="
                + nombreUsuarioAlterno + ", servicio=" + servicio + ", ubicacion=" + ubicacion + ", incidencia="
                + incidencia + ", nombreAdjunto=" + nombreAdjunto + ", adjunto=" + adjunto + '}';
    }

}
