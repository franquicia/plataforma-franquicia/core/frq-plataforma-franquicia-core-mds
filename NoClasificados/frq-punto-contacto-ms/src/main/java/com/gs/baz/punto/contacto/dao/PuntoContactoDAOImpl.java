package com.gs.baz.punto.contacto.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.punto.contacto.dto.PuntoContactoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFranquiciaModeloDTO;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;
    @Autowired
    private PuntoContactoDetalleDAOImpl puntoContactoDetalleDAOImpl;
    @Autowired
    private PuntoContactoFranquiciaModeloDAOImpl puntoContactoFranquiciaModeloDAOImpl;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPUNTOC");
        jdbcSelect.withProcedureName("SP_SEL_PC");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoRowMapper());
    }

    public PuntoContactoDTO selectRow(String ceco) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCECO", null);
        mapSqlParameterSource.addValue("PA_FCCECO", ceco);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoDTO> data = (List<PuntoContactoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            PuntoContactoDTO puntoContactoDTO = data.get(0);
            puntoContactoDTO.setPuntoContactoDetalleDTO(puntoContactoDetalleDAOImpl.selectRow(ceco));
            puntoContactoDTO.setPuntoContactoFranquiciaModeloDTO(puntoContactoFranquiciaModeloDAOImpl.selectRow(puntoContactoDTO.getIdPuntoContacto()));
            return puntoContactoDTO;
        } else {
            return null;
        }
    }

    public List<PuntoContactoDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCECO", null);
        mapSqlParameterSource.addValue("PA_FCCECO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoDTO> data = (List<PuntoContactoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Transactional(rollbackFor = {CustomException.class})
    public PuntoContactoDTO cuRow(PuntoContactoDTO entityDTO) throws CustomException {
        return this.cuPuntoContacto(entityDTO);
    }

    private PuntoContactoDTO cuPuntoContacto(PuntoContactoDTO entityDTO) throws CustomException {
        try {
            PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO = entityDTO.getPuntoContactoFranquiciaModeloDTO();
            puntoContactoFranquiciaModeloDTO.setIdPuntoContacto(new BigDecimal(entityDTO.getIdPuntoContacto()));
            if (puntoContactoFranquiciaModeloDTO.getIdPuntoContactoFranquiciaModelo() == null) {
                puntoContactoFranquiciaModeloDTO = puntoContactoFranquiciaModeloDAOImpl.insertRow(puntoContactoFranquiciaModeloDTO);
            } else {
                puntoContactoFranquiciaModeloDTO = puntoContactoFranquiciaModeloDAOImpl.updateRow(puntoContactoFranquiciaModeloDTO);
            }
            puntoContactoFranquiciaModeloDTO.setIdPuntoContacto(null);
            entityDTO.setPuntoContactoFranquiciaModeloDTO(puntoContactoFranquiciaModeloDTO);
        } catch (Exception ex) {
            logger.log(Level.DEBUG, ex);
            throw new CustomException(ex);
        }
        return entityDTO;
    }

}
