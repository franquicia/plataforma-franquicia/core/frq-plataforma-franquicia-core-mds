/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class FolioBase {

    @JsonProperty(value = "idEstadoFolio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificador del estado del documento", example = "1")
    private Integer idEstadoFolio;

    @JsonProperty(value = "fechaCreacion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Fecha en la que se generó el documento", example = "01-03-2021")
    private String fechaCreacion;

    @JsonProperty(value = "numeroEmpleadoCreador")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "numero de empleado del creador del documento", example = "331952")
    private Integer numeroEmpleadoCreador;

    @JsonProperty(value = "indicePaso")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Indice del paso de la creacion del documento", example = "1")
    private Integer indicePaso;

    @JsonProperty(value = "fechaModificacion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Fecha en la que se modifico el folio", example = "01-03-2021")
    private String fechaModificacion;

    public Integer getNumeroEmpleadoCreador() {
        return numeroEmpleadoCreador;
    }

    public void setNumeroEmpleadoCreador(Integer numeroEmpleadoCreador) {
        this.numeroEmpleadoCreador = numeroEmpleadoCreador;
    }

    public Integer getIdEstadoFolio() {
        return idEstadoFolio;
    }

    public void setIdEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getIndicePaso() {
        return indicePaso;
    }

    public void setIndicePaso(Integer indicePaso) {
        this.indicePaso = indicePaso;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

}
