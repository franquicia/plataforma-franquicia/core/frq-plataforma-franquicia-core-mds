/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modulo.flujo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class ModuloFlujoDTO {

    @JsonProperty(value = "id_flujo_modulo")
    private Integer idFlujoModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_flujo")
    private Integer idFlujo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_modulo")
    private Integer idModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private ModuloDTO moduloDTO;

    @JsonProperty(value = "id_parent")
    private Integer idParent;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "parent")
    private ModuloFlujoDTO parent;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "children")
    private List<ModuloFlujoDTO> children;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private Integer estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oculto_menu")
    private Boolean ocultoMenu;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "orden")
    private Integer orden;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public ModuloFlujoDTO() {
    }

    public Integer getIdFlujoModulo() {
        return idFlujoModulo;
    }

    public void setIdFlujoModulo(BigDecimal idFlujoModulo) {
        this.idFlujoModulo = (idFlujoModulo == null ? null : idFlujoModulo.intValue());
    }

    public Integer getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(BigDecimal idFlujo) {
        this.idFlujo = (idFlujo == null ? null : idFlujo.intValue());
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(BigDecimal idModulo) {
        this.idModulo = (idModulo == null ? null : idModulo.intValue());
    }

    public ModuloDTO getModuloDTO() {
        return moduloDTO;
    }

    public void setModuloDTO(ModuloDTO moduloDTO) {
        this.moduloDTO = moduloDTO;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(BigDecimal idParent) {
        this.idParent = (idParent == null ? null : idParent.intValue());
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(BigDecimal estatus) {
        this.estatus = (estatus == null ? null : estatus.intValue());
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(BigDecimal orden) {
        this.orden = (orden == null ? null : orden.intValue());
    }

    public Boolean getOcultoMenu() {
        return ocultoMenu;
    }

    public void setOcultoMenu(Boolean ocultoMenu) {
        this.ocultoMenu = ocultoMenu;
    }

    public ModuloFlujoDTO getParent() {
        return parent;
    }

    public void setParent(ModuloFlujoDTO parent) {
        this.parent = parent;
    }

    public List<ModuloFlujoDTO> getChildren() {
        return children;
    }

    public void setChildren(List<ModuloFlujoDTO> children) {
        this.children = children;
    }

    public void addChild(ModuloFlujoDTO child) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        if (!this.children.contains(child) && child != null) {
            this.children.add(child);
        }
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "ModuloFlujoDTO{" + "idFlujoModulo=" + idFlujoModulo + ", idFlujo=" + idFlujo + ", descripcion=" + descripcion + ", idModulo=" + idModulo + ", moduloDTO=" + moduloDTO + ", idParent=" + idParent + ", parent=" + parent + ", children=" + children + ", estatus=" + estatus + ", ocultoMenu=" + ocultoMenu + ", orden=" + orden + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
