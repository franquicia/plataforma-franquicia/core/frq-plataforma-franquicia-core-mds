/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.Negocio;

/**
 *
 * @author carlos
 */
public class NegociosRowMapper implements RowMapper<Negocio> {

    private Negocio negocio;

    @Override
    public Negocio mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocio = new Negocio();
        negocio.setIdNegocio(rs.getInt("FIIDPAIS"));
        negocio.setDescripcion(rs.getString("FCDESCRIPCION"));
        return negocio;
    }
}
