/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class Usuario {

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Identificador del Usuario", example = "189140", position = -1)
    private Integer numeroEmpleado;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del Empleado", example = "PEDRO PEREZ GOMEZ")
    private String nombre;

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Usuario{" + "numeroEmpleado=" + numeroEmpleado + ", nombre=" + nombre + '}';
    }

}
