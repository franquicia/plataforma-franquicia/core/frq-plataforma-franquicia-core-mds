/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class FileNotFoundException extends Exception{

    public FileNotFoundException() {
    }

    public FileNotFoundException(String string) {
        super(string);
    }

    public FileNotFoundException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public FileNotFoundException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
