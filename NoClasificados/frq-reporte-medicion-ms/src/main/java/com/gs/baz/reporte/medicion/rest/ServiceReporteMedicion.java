/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ProcessMonitor;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinaFranquicia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceJsonConverter;
import com.gs.baz.reporte.medicion.dao.NegocioProtocoloDAOImpl;
import com.gs.baz.reporte.medicion.dao.ProtocoloDisciplinaDAOImpl;
import com.gs.baz.reporte.medicion.dao.ReporteMedicionDAO;
import com.gs.baz.reporte.medicion.dao.VersionChecklistDAOImpl;
import com.gs.baz.reporte.medicion.dao.bi.ReporteIntermedia7SBI;
import com.gs.baz.reporte.medicion.dao.bi.ReporteIntermediaBI;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/reporte/medicion")
public class ServiceReporteMedicion {

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    ReporteMedicionDAO reporteMedicionDAO;

    @Autowired
    SubServiceJsonConverter subServiceJsonConverter;

    @Autowired
    ReporteIntermediaBI reporteIntermediaBI;

    @Autowired
    ReporteIntermedia7SBI reporteIntermedia7SBI;

    @Autowired
    ProtocoloDisciplinaDAOImpl protocoloDisciplinaDAOImpl;

    @Autowired
    VersionChecklistDAOImpl versionChecklistDAOImpl;

    @Autowired
    NegocioProtocoloDAOImpl negocioProtocoloDAOImpl;

    @Autowired
    SubServiceDisciplinaFranquicia subServiceDisciplinaFranquicia;

    private ProcessMonitor processMonitor;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    int index = 0;

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "/intermedia/secure/get", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity reporteMedicionIntermedia(@RequestHeader HttpHeaders headers, @RequestBody FiltersDTO filtersDTO) throws CustomException, JsonProcessingException {

        processMonitor = new ProcessMonitor("ServiceReporteMedicionIntermedia");
        processMonitor.startWatch("Consulta de Informacion Reporte Intermedia ");
        HttpEntity<Object> httpEntity = new HttpEntity<>("{}", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        subServiceDisciplinaFranquicia.init(basePath, httpEntity);
        ObjectNode reporteMedicion = reporteIntermediaBI.reporteMedicionIntermedia(subServiceDisciplinaFranquicia, filtersDTO);
        processMonitor.stopWatch();
        processMonitor.startWatch("Conversion Java a JSON");

        //Omite la primer columna de dashboard Id para no insertarla en el Excel
        index = 0;
        reporteMedicion.forEach((subnode) -> {
            //El primer registro es el nombre del archivo.
            if (index > 0) {
                subnode.forEach((row) -> {

                    if (row.isObject()) {
                        ((ObjectNode) row).remove("FIVISITA_ID");
                    }

                });

            }
            index++;
        });

        //--------------------------------------------------------------------
        try {
            httpEntity = new HttpEntity<>(new ObjectMapper().writeValueAsString(reporteMedicion), headers);
        } catch (JsonProcessingException ex) {
            logger.log(Level.ERROR, ex);
        }
        processMonitor.stopWatch();
        processMonitor.startWatch("Conversion JSON a Excel MicroService");
        subServiceJsonConverter.init(basePath, httpEntity);
        ResponseEntity<byte[]> file = subServiceJsonConverter.convertFromArray();
        Resource resource = new ByteArrayResource(file.getBody());
        processMonitor.stopWatch();
        processMonitor.resumeInfo();
        HttpHeaders outHeaders = new HttpHeaders();
        file.getHeaders().forEach((String header, List<String> value) -> {
            outHeaders.put(header, value);
        });
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS);
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS);
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN);
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(resource);
    }

    @RequestMapping(value = "/7s/intermedia/secure/get", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity reporteMedicionIntermedia7s(@RequestHeader HttpHeaders headers, @RequestBody FiltersDTO filtersDTO) throws CustomException, JsonProcessingException {

        processMonitor = new ProcessMonitor("ServiceReporteMedicionIntermedia 7S");
        processMonitor.startWatch("Consulta de Informacion");
        HttpEntity<Object> httpEntity = new HttpEntity<>("{}", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        subServiceDisciplinaFranquicia.init(basePath, httpEntity);
        ObjectNode reporteMedicion = reporteIntermedia7SBI.reporteMedicionIntermedia(subServiceDisciplinaFranquicia, filtersDTO);
        processMonitor.stopWatch();
        processMonitor.startWatch("Conversion Java a JSON");

        //Omite la primer columna de dashboard Id para no insertarla en el Excel
        index = 0;
        reporteMedicion.forEach((subnode) -> {
            //El primer registro es el nombre del archivo.
            if (index > 0) {
                subnode.forEach((row) -> {

                    if (row.isObject()) {
                        ((ObjectNode) row).remove("FIVISITA_ID");
                    }

                });

            }
            index++;
        });

        try {
            httpEntity = new HttpEntity<>(new ObjectMapper().writeValueAsString(reporteMedicion), headers);
        } catch (JsonProcessingException ex) {
            logger.log(Level.ERROR, ex);
        }
        processMonitor.stopWatch();
        processMonitor.startWatch("Conversion JSON a Excel MicroService");
        subServiceJsonConverter.init(basePath, httpEntity);
        HttpHeaders outHeaders = new HttpHeaders();
        ResponseEntity<byte[]> file = subServiceJsonConverter.convertFromArray();
        Resource resource = new ByteArrayResource(file.getBody());
        processMonitor.stopWatch();
        processMonitor.resumeInfo();
        file.getHeaders().forEach((String header, List<String> value) -> {
            outHeaders.put(header, value);
        });
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS);
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS);
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN);
        outHeaders.remove(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(resource);
    }

}
