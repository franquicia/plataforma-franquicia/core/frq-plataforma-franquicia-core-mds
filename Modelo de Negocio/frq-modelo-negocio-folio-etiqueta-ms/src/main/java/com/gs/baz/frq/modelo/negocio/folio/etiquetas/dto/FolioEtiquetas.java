/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de folio Etiquetas del documento", value = "folioEtiquetas")
public class FolioEtiquetas {

    @JsonProperty(value = "folioEtiquetas")
    @ApiModelProperty(notes = "folioEtiquetas")
    private List<FolioEtiquetaBase> folioEtiquetas;

    public FolioEtiquetas(List<FolioEtiquetaBase> folioEtiquetas) {
        this.folioEtiquetas = folioEtiquetas;
    }

    public List<FolioEtiquetaBase> getFolioEtiquetas() {
        return folioEtiquetas;
    }

    public void setFolioEtiquetas(List<FolioEtiquetaBase> folioEtiquetas) {
        this.folioEtiquetas = folioEtiquetas;
    }

    @Override
    public String toString() {
        return "FolioEtiquetas{" + "folioEtiquetas=" + folioEtiquetas + '}';
    }

}
