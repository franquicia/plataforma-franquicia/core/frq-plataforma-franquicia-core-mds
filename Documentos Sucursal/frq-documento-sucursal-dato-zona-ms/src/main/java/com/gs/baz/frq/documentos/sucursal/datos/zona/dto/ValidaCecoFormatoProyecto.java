/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Valida que exista la", value = "ValidaInformacion")
public class ValidaCecoFormatoProyecto {

    @JsonProperty(value = "idEstatusEdicion")
    @ApiModelProperty(notes = "Identificador del estatus de edicion", example = "1")
    private Integer idEstatusEdicion;

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Indica el valor del ceco", example = "1")
    private Integer idCeco;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Indica el valor del formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Indicador del id de documento ", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Indicador del id de Proyecto ", example = "1")
    private Integer idProyecto;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdEstatusEdicion() {
        return idEstatusEdicion;
    }

    public void setIdEstatusEdicion(Integer idEstatusEdicion) {
        this.idEstatusEdicion = idEstatusEdicion;
    }

}
