package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteCheckInnMttoDTO;

/**
 *
 * @author cescobarh
 */
public class ReporteCheckInnRowMapper implements RowMapper<ReporteCheckInnMttoDTO> {

    private ReporteCheckInnMttoDTO status;

    @Override
    public ReporteCheckInnMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteCheckInnMttoDTO();

        status.setIdUsuario(rs.getString("FCID_USUARIO"));
        status.setNombreUsuario(rs.getString("NOMBRE_USU"));
        status.setIdCeco(rs.getInt("CECO"));
        status.setNombreCeco(rs.getString("NOMBCECO"));
        status.setFecha(rs.getString("FECHA"));
        status.setSemana(rs.getString("SEMANA"));
        status.setDia(rs.getString("DIA"));
        status.setFechaInicio(rs.getString("FECHAINI_2DO"));
        status.setFechaFin(rs.getString("FECHASAL_2DO"));
        status.setHoraEntrada(rs.getString("HORA_ENTRADA2DO"));
        //salida segundo nivel
        status.setHoraSalida(rs.getString("HORA_SAL2DO"));
        //salida protocolo fecha max
        status.setHoraSalidaProtocolo(rs.getString("HORA_SAL3ER"));
        status.setHorasTrabajadas(rs.getString("HORAS_TRABAJADAS"));
        status.setNombreFase(rs.getString("FASE"));
        status.setIdFase(rs.getInt("IDFASE"));

        return status;
    }
}
