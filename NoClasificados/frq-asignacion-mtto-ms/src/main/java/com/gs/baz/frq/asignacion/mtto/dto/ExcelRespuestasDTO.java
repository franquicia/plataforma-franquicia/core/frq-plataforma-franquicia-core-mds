/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExcelRespuestasDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idSoft")
    private Integer idSoft;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "eco")
    private Integer numeroEconomico;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreSucursal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idFase")
    private Integer idFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numeroEmpleado")
    private Integer numeroEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idProyecto")
    private Integer idProyecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idHallazgo")
    private Integer idHallazgo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty Integer  idRespuesta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idPRegunta")
    private Integer idPRegunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pregunta")
    private String pregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "respuesta")
    private String respuesta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "observacion")
    private String observacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "bitacora")
    private Integer bitacora;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreChekclist")
    private String nombreChecklist;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idChecklist")
    private Integer idChecklist;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ruta")
    private String ruta;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fase")
    private String fase;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "proyecto")
    private String proyecto;
    
    



	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public Integer getIdSoft() {
		return idSoft;
	}

	public void setIdSoft(Integer idSoft) {
		this.idSoft = idSoft;
	}

	public Integer getNumeroEconomico() {
		return numeroEconomico;
	}

	public void setNumeroEconomico(Integer numeroEconomico) {
		this.numeroEconomico = numeroEconomico;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}

	public Integer getIdFase() {
		return idFase;
	}

	public void setIdFase(Integer idFase) {
		this.idFase = idFase;
	}

	public Integer getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(Integer numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public Integer getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(Integer idProyecto) {
		this.idProyecto = idProyecto;
	}

	public Integer getIdHallazgo() {
		return idHallazgo;
	}

	public void setIdHallazgo(Integer idHallazgo) {
		this.idHallazgo = idHallazgo;
	}

	public Integer getIdRespuesta() {
		return idRespuesta;
	}

	public void setIdRespuesta(Integer idRespuesta) {
		this.idRespuesta = idRespuesta;
	}

	public Integer getIdPRegunta() {
		return idPRegunta;
	}

	public void setIdPRegunta(Integer idPRegunta) {
		this.idPRegunta = idPRegunta;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getBitacora() {
		return bitacora;
	}

	public void setBitacora(Integer bitacora) {
		this.bitacora = bitacora;
	}

	public String getNombreChecklist() {
		return nombreChecklist;
	}

	public void setNombreChecklist(String nombreChecklist) {
		this.nombreChecklist = nombreChecklist;
	}

	public Integer getIdChecklist() {
		return idChecklist;
	}

	public void setIdChecklist(Integer idChecklist) {
		this.idChecklist = idChecklist;
	}



	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	@Override
	public String toString() {
		return "ExcelRespuestasDTO [idSoft=" + idSoft + ", numeroEconomico=" + numeroEconomico + ", nombreSucursal="
				+ nombreSucursal + ", idFase=" + idFase + ", idUsuario=" + numeroEmpleado + ", nombreUsuario="
				+ nombreUsuario + ", idProyecto=" + idProyecto + ", idHallazgo=" + idHallazgo + ", idRespuesta="
				+ idRespuesta + ", idPRegunta=" + idPRegunta + ", pregunta=" + pregunta + ", respuesta=" + respuesta
				+ ", observacion=" + observacion + ", bitacora=" + bitacora + ", nombreChecklist=" + nombreChecklist
				+ ", idChecklist=" + idChecklist + ", periodo=" + periodo + ", ruta=" + ruta + ", fase=" + fase
				+ ", proyecto=" + proyecto + "]";
	}

	

    
   

}
