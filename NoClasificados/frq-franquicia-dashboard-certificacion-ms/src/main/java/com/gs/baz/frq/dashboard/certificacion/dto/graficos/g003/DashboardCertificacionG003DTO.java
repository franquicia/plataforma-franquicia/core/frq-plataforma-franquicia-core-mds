/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.graficos.g003;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionG003DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicial")
    private String fechaInicial;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_final")
    private String fechaFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_gerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre")
    private String nombre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "total_asignados")
    private Integer totalAsignados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "total_certificados")
    private Integer totalCertificados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango_color")
    private String rangoColor;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango")
    private Integer rango;

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(BigDecimal idGerente) {
        this.idGerente = (idGerente == null ? null : idGerente.intValue());
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public Integer getTotalAsignados() {
        return totalAsignados;
    }

    public void setTotalAsignados(BigDecimal totalAsignados) {
        this.totalAsignados = (totalAsignados == null ? null : totalAsignados.intValue());
    }

    public Integer getTotalCertificados() {
        return totalCertificados;
    }

    public void setTotalCertificados(BigDecimal totalCertificados) {
        this.totalCertificados = (totalCertificados == null ? null : totalCertificados.intValue());
    }

    public Integer getRango() {
        return rango;
    }

    public void setRango(BigDecimal rango) {
        this.rango = (rango == null ? null : rango.intValue());
    }

    @Override
    public String toString() {
        return "DashboardCertificacionG002DTO{" + "fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", idGerente=" + idGerente + ", idNegocio=" + idNegocio + ", nombre=" + nombre + ", territorio=" + territorio + ", totalAsignados=" + totalAsignados + ", totalCertificados=" + totalCertificados + ", rangoColor=" + rangoColor + ", rango=" + rango + '}';
    }
}
