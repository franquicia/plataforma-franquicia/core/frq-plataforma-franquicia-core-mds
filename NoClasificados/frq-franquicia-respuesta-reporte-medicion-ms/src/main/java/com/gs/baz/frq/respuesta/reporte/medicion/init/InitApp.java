package com.gs.baz.frq.respuesta.reporte.medicion.init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 *
 * @author jlfr
 */
@SpringBootApplication
public class InitApp extends SpringBootServletInitializer {

    /**
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(InitApp.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(InitApp.class, args);
    }

}
