package com.gs.baz.frq.pregunta.visita.mprs;

import com.gs.baz.frq.pregunta.visita.dto.PreguntaVisitaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PreguntaVisitaRowMapper implements RowMapper<PreguntaVisitaDTO> {

    private PreguntaVisitaDTO preguntaVisitaDTO;

    @Override
    public PreguntaVisitaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        preguntaVisitaDTO = new PreguntaVisitaDTO();
        preguntaVisitaDTO.setIdBitacora(((BigDecimal) rs.getObject("FIID_BITACORA")));
        preguntaVisitaDTO.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        preguntaVisitaDTO.setFecha(rs.getString("FDFECHA"));
        preguntaVisitaDTO.setIdPregunta((BigDecimal) rs.getObject("FIID_PREGUNTA"));
        preguntaVisitaDTO.setPregunta(rs.getString("FCPREGUNTA"));
        preguntaVisitaDTO.setRespuesta(rs.getString("FCRESPUESTA"));
        preguntaVisitaDTO.setIdNegocioDisciplina((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        preguntaVisitaDTO.setPonderacion((BigDecimal) rs.getObject("FIPONDERACION"));
        preguntaVisitaDTO.setModulo(rs.getString("FCMODULO"));
        preguntaVisitaDTO.setIncumplimiento((BigDecimal) rs.getObject("FINCUMPLIMIENTO"));
        preguntaVisitaDTO.setIdChecklist((BigDecimal) rs.getObject("FIID_CHECKLIST"));
        preguntaVisitaDTO.setImperdonable((BigDecimal) rs.getObject("FIMPERDONABLE"));
        preguntaVisitaDTO.setReglaNueve((BigDecimal) rs.getObject("FIREGLANUEVE"));
        preguntaVisitaDTO.setIdDefecto((BigDecimal) rs.getObject("FIDEFECTO"));
        preguntaVisitaDTO.setOportunidad((BigDecimal) rs.getObject("FIOPORTUNIDAD"));
        return preguntaVisitaDTO;
    }
}
