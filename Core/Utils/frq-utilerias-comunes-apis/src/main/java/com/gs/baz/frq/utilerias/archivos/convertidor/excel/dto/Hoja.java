/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cescobarh
 */
public class Hoja {

    @JsonProperty(value = "descripcion", required = true)
    @NotBlank
    private String descripcion;

    @JsonProperty(value = "contenido", required = true)
    @NotNull
    @Valid
    private Contenido contenido;

    public Hoja() {
    }

    public Hoja(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Contenido getContenido() {
        return contenido;
    }

    public void setContenido(Contenido contenido) {
        this.contenido = contenido;
    }

    @Override
    public String toString() {
        return "Hoja{" + "descripcion=" + descripcion + ", contenido=" + contenido + '}';
    }

}
