package com.gs.baz.propiedades.dao;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.propiedades.dto.PropiedadesDTO;
import com.gs.baz.propiedades.mprs.PropiedadesRowMapper;
import com.gs.baz.propiedades.dao.util.GenericDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class PropiedadesDAOImpl extends DefaultDAO implements GenericDAO<PropiedadesDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINPROP");
        jdbcInsert.withProcedureName("SP_INS_PROP");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINPROP");
        jdbcUpdate.withProcedureName("SP_ACT_PROP");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINPROP");
        jdbcDelete.withProcedureName("SP_DEL_PROP");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPROP");
        jdbcSelect.withProcedureName("SP_SEL_PROP");
        jdbcSelect.returningResultSet("RCL_INFO", new PropiedadesRowMapper());
    }

    @Override
    public PropiedadesDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPROP", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PropiedadesDTO> data = (List<PropiedadesDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public PropiedadesDTO selectRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PropiedadesDTO> selectRows(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PropiedadesDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPROP", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PropiedadesDTO> data = (List<PropiedadesDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public PropiedadesDTO insertRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PropiedadesDTO insertRow(PropiedadesDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCCLAVE", entityDTO.getClave());
            mapSqlParameterSource.addValue("PA_FCVALOR", entityDTO.getValor());
            mapSqlParameterSource.addValue("PA_FIENCRIPTADO", entityDTO.getEncriptado());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdPropiedad((BigDecimal) out.get("PA_FIIDPROP"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Propiedades"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Propiedades"), ex);
        }
    }

    @Override
    public PropiedadesDTO updateRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PropiedadesDTO updateRow(PropiedadesDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROP", entityDTO.getIdPropiedad());
            mapSqlParameterSource.addValue("PA_FCCLAVE", entityDTO.getClave());
            mapSqlParameterSource.addValue("PA_FCVALOR", entityDTO.getValor());
            mapSqlParameterSource.addValue("PA_FIENCRIPTADO", entityDTO.getEncriptado());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Propiedades"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Propiedades"), ex);
        }
    }

    @Override
    public PropiedadesDTO deleteRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PropiedadesDTO deleteRow(PropiedadesDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROP", entityDTO.getIdPropiedad());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Propiedades"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Propiedades"), ex);
        }
    }

}
