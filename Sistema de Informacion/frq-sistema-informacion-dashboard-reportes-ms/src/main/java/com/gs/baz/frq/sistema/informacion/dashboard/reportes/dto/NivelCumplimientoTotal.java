/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del nivel cumplimiento total", value = "NivelCumplimientoTotal")
public class NivelCumplimientoTotal {

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Total de puntos de venta", example = "90")
    private Integer puntosVenta;

    @JsonProperty(value = "nivelesCumplimiento")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Niveles de cumplimiento")
    private List<NivelCumplimiento> nivelesCumplimiento;

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    public List<NivelCumplimiento> getNivelesCumplimiento() {
        return nivelesCumplimiento;
    }

    public void setNivelesCumplimiento(List<NivelCumplimiento> nivelesCumplimiento) {
        this.nivelesCumplimiento = nivelesCumplimiento;
    }

    @Override
    public String toString() {
        return "NivelCumplimientoTotal{" + "puntosVenta=" + puntosVenta + ", nivelesCumplimiento=" + nivelesCumplimiento + '}';
    }

}
