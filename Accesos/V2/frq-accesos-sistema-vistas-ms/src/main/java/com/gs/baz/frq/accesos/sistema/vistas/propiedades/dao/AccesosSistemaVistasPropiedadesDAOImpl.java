/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.AltaVistaPropiedad;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.ParametrosClaveVistaPropiedad;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.ParametrosVistaPropiedad;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.VistaPropiedad;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.VistaPropiedadBase;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.mprs.VistaPropiedadRowMapper;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.mprs.VistaPropiedadesRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class AccesosSistemaVistasPropiedadesDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final String schema = "MODFRANQ";

    public void init() {
        String catalogo = "PAADMDPROPVIST";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSDPROPVISTA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTDPROPVISTA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELDPROPVISTA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETDPROPVISTA");
        jdbcSelect.returningResultSet("PA_CDATOS", new VistaPropiedadRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETDPROPVISTA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new VistaPropiedadesRowMapper());
    }

    public VistaPropiedadBase selectRow(ParametrosClaveVistaPropiedad parametrosClaveVistaPropiedad) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDVISTA", parametrosClaveVistaPropiedad.getIdVista());
        mapSqlParameterSource.addValue("PA_FCNOMBRE", parametrosClaveVistaPropiedad.getNombre());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<VistaPropiedadBase> data = (List<VistaPropiedadBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<VistaPropiedad> selectAllRows(Integer idEntity) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDVISTA", idEntity);
        mapSqlParameterSource.addValue("PA_FCNOMBRE", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<VistaPropiedad>) out.get("PA_CDATOS");
    }

    public AltaVistaPropiedad insertRow(ParametrosVistaPropiedad entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDVISTA", entityDTO.getIdVista());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCVALOR", entityDTO.getValor());
            mapSqlParameterSource.addValue("PA_FCTIPO", entityDTO.getTipo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDPROPVISTA");
                return new AltaVistaPropiedad(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosVistaPropiedad entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource.addValue("PA_FIIDPROPVISTA", null);
            mapSqlParameterSource.addValue("PA_FIIDVISTA", entityDTO.getIdVista());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCVALOR", entityDTO.getValor());
            mapSqlParameterSource.addValue("PA_FCTIPO", entityDTO.getTipo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer idVista, Integer idPropiedad) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDVISTA", idVista);
            mapSqlParameterSource.addValue("PA_FIIDPROPVISTA", idPropiedad);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
