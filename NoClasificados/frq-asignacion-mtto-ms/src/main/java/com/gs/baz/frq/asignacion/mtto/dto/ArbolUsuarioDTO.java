/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ArbolUsuarioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCoordinador")
    private Integer idCoordinador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idJefeZona")
    private Integer idJefeZona;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idJefeTerritorio")
    private Integer idJefeTerritorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreEmpleadoZona")
    private String nombreEmpleadoZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreEmpleadoTerritorio")
    private String nombreEmpleadoTerritorio;

    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idZona")
    private Integer idZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idTerritorio")
    private Integer idTerritorio;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreTerritorio")
    private String nombreTerritorio;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreZona")
    private String nombreZona;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idGeografia")
    private Integer idGeografia;
    
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idSupervisor")
    private Integer idSupervisor;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreSupervisor")
    private String nombreSupervisor;

    
    
    
	public String getNombreSupervisor() {
		return nombreSupervisor;
	}

	public void setNombreSupervisor(String nombreSupervisor) {
		this.nombreSupervisor = nombreSupervisor;
	}

	public Integer getIdSupervisor() {
		return idSupervisor;
	}

	public void setIdSupervisor(Integer idSupervisor) {
		this.idSupervisor = idSupervisor;
	}


	public String getNombreTerritorio() {
		return nombreTerritorio;
	}

	public void setNombreTerritorio(String nombreTerritorio) {
		this.nombreTerritorio = nombreTerritorio;
	}

	public String getNombreZona() {
		return nombreZona;
	}

	public void setNombreZona(String nombreZona) {
		this.nombreZona = nombreZona;
	}

	public Integer getIdCoordinador() {
		return idCoordinador;
	}

	public void setIdCoordinador(Integer idCoordinador) {
		this.idCoordinador = idCoordinador;
	}

	public Integer getIdJefeZona() {
		return idJefeZona;
	}

	public void setIdJefeZona(Integer idJefeZona) {
		this.idJefeZona = idJefeZona;
	}

	public Integer getIdJefeTerritorio() {
		return idJefeTerritorio;
	}

	public void setIdJefeTerritorio(Integer idJefeTerritorio) {
		this.idJefeTerritorio = idJefeTerritorio;
	}

	public String getNombreEmpleadoZona() {
		return nombreEmpleadoZona;
	}

	public void setNombreEmpleadoZona(String nombreEmpleadoZona) {
		this.nombreEmpleadoZona = nombreEmpleadoZona;
	}

	public String getNombreEmpleadoTerritorio() {
		return nombreEmpleadoTerritorio;
	}

	public void setNombreEmpleadoTerritorio(String nombreEmpleadoTerritorio) {
		this.nombreEmpleadoTerritorio = nombreEmpleadoTerritorio;
	}

	public Integer getIdZona() {
		return idZona;
	}

	public void setIdZona(Integer idZona) {
		this.idZona = idZona;
	}

	public Integer getIdTerritorio() {
		return idTerritorio;
	}

	public void setIdTerritorio(Integer idTerritorio) {
		this.idTerritorio = idTerritorio;
	}

	public Integer getIdGeografia() {
		return idGeografia;
	}

	public void setIdGeografia(Integer idGeografia) {
		this.idGeografia = idGeografia;
	}

	@Override
	public String toString() {
		return "ArbolUsuarioDTO [idCoordinador=" + idCoordinador + ", idJefeZona=" + idJefeZona + ", idJefeTerritorio="
				+ idJefeTerritorio + ", nombreEmpleadoZona=" + nombreEmpleadoZona + ", nombreEmpleadoTerritorio="
				+ nombreEmpleadoTerritorio + ", idZona=" + idZona + ", idTerritorio=" + idTerritorio
				+ ", nombreTerritorio=" + nombreTerritorio + ", nombreZona=" + nombreZona + ", idGeografia="
				+ idGeografia + ", idSupervisor=" + idSupervisor + ", nombreSupervisor=" + nombreSupervisor + "]";
	}
    




  

}
