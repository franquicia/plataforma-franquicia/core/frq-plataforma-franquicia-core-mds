/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de estado de documento del modelo de negocio", value = "estado de documento")
public class EstadosDocumento {

    @JsonProperty(value = "estadoDocumento")
    @ApiModelProperty(notes = "estadoDocumento")
    private List<EstadoDocumento> estadoDocumento;

    public EstadosDocumento(List<EstadoDocumento> estadoDocumento) {
        this.estadoDocumento = estadoDocumento;
    }

    public List<EstadoDocumento> getEstadosDocumento() {
        return estadoDocumento;
    }

    public void setEstadosDocumento(List<EstadoDocumento> estadoDocumento) {
        this.estadoDocumento = estadoDocumento;
    }

    @Override
    public String toString() {
        return "EstadosDocumento{" + "estadoDocumento=" + estadoDocumento + '}';
    }

}
