/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g014;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g014.DashboardCertificacionG014DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author MADA
 */
public class DashboardCertificacionG014InvitadoRowMapper implements RowMapper<DashboardCertificacionG014DTO> {

    private DashboardCertificacionG014DTO dashboardCertificacionG014DTO;

    @Override
    public DashboardCertificacionG014DTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        dashboardCertificacionG014DTO = new DashboardCertificacionG014DTO();
        dashboardCertificacionG014DTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
        dashboardCertificacionG014DTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionG014DTO.setNombreCeco(rs.getString("NOMBRE_CECO"));
        dashboardCertificacionG014DTO.setFecha(rs.getString("FDFECHA"));
        dashboardCertificacionG014DTO.setIdUsuario((BigDecimal) rs.getObject("ID_USUARIO"));
        dashboardCertificacionG014DTO.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));

        dashboardCertificacionG014DTO.setCalificacion((BigDecimal) rs.getObject("CALIFICACION"));
        dashboardCertificacionG014DTO.setIdRango((BigDecimal) rs.getObject("RANGO_ID"));
        dashboardCertificacionG014DTO.setRangoColor(rs.getString("RANGOCOLOR"));

        return dashboardCertificacionG014DTO;
    }
}
