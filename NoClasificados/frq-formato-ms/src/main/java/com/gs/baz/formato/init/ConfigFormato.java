package com.gs.baz.formato.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.formato.dao.FormatoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.formato")
public class ConfigFormato {

    private final Logger logger = LogManager.getLogger();

    public ConfigFormato() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqFormatoDAOImpl")
    public FormatoDAOImpl formatoDAOImpl() {
        return new FormatoDAOImpl();
    }
}
