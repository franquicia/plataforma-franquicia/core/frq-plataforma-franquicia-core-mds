/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.rutas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos base de ruta", value = "RutaBase")
public class RutaBase {

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre de la ruta", example = "accesos_sistema_rutas_edicion")
    private String nombre;

    @JsonProperty(value = "rutaAbsoluta")
    @ApiModelProperty(notes = "Ruta absoluta declarada", example = "/accesos/sistema/rutas/edicion/:ruta")
    private String rutaAbsoluta;

    @JsonProperty(value = "borrado")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    transient private Boolean borrado;

    public RutaBase() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaAbsoluta() {
        return rutaAbsoluta;
    }

    public void setRutaAbsoluta(String rutaAbsoluta) {
        this.rutaAbsoluta = rutaAbsoluta;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    @Override
    public String toString() {
        return "RutaBase{" + "nombre=" + nombre + ", rutaAbsoluta=" + rutaAbsoluta + ", borrado=" + borrado + '}';
    }

}
