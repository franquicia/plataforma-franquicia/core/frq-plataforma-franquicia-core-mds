/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.formatos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Formato", value = "Formato")
public class Formato extends FormatoBase {

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Identificador del Formato", example = "1", position = -1)
    private Integer idFormato;

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    @Override
    public String toString() {
        return "Formato{" + "idFormato=" + idFormato + '}';
    }

}
