/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.modelos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosModelo {

    @JsonProperty(value = "idModelo")
    private transient Integer idModelo;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion nombre del pais", example = "México", required = true)
    private String descripcion;

    @JsonProperty(value = "acronimo", required = true)
    @ApiModelProperty(notes = "Aronimo del pais", example = "MEX", required = true)
    private String acronimo;

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Integer idModelo) {
        this.idModelo = idModelo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    @Override
    public String toString() {
        return "ParametrosModelo{" + "descripcion=" + descripcion + ", acronimo=" + acronimo + '}';
    }

}
