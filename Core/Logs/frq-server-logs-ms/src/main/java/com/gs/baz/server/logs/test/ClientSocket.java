/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

/**
 *
 * @author cescobarh
 */
public class ClientSocket {

    private static final String SEND_HELLO_MESSAGE_ENDPOINT = "/app/chat";
    private static final String SUBSCRIBE_GREETINGS_ENDPOINT = "/topic/chat";

    private static List<Transport> createTransportClient() {
        final List<Transport> transports = new ArrayList<>();
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
        final String url = "ws://10.51.218.10:8080/plataforma-franquicia-core/websocket/";

        final WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        final StompSession stompSession = stompClient.connect(url, new StompSessionHandlerAdapter() {
        }).get(10, TimeUnit.SECONDS);

        stompSession.subscribe(SUBSCRIBE_GREETINGS_ENDPOINT, new GreetingStompFrameHandler());
        stompSession.send(SEND_HELLO_MESSAGE_ENDPOINT, "holaaa desde java");

        new Scanner(System.in).nextLine();
    }

    private static class GreetingStompFrameHandler implements StompFrameHandler {

        @Override
        public Type getPayloadType(final StompHeaders stompHeaders) {
            return String.class;
        }

        @Override
        public void handleFrame(final StompHeaders stompHeaders, final Object o) {
            System.out.println(o);
        }
    }
}
