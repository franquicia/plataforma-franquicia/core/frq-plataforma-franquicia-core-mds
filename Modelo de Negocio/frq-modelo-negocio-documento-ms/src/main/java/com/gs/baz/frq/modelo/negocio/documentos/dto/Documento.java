/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Documento", value = "Documento")
public class Documento extends DocumentoBase {

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del Documento", example = "1", position = -1)
    private Integer idDocumento;

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public String toString() {
        return "Documento{" + "idDocumento=" + idDocumento + '}';
    }

}
