/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g012;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author B73601
 */
public class NegocioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "incumplimientos")
    private List<IncumplimientoDTO> incumplimientos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "incumplimientos")
    private List<IncumplimientoDTO> incumplimientosPPR;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "imperdonables")
    private List<ImperdonableDTO> imperdonables;

    public NegocioDTO(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public NegocioDTO() {
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public List<IncumplimientoDTO> getIncumplimientos() {
        return incumplimientos;
    }

    public void setIncumplimientos(List<IncumplimientoDTO> incumplimientos) {
        this.incumplimientos = incumplimientos;
    }

    public List<ImperdonableDTO> getImperdonables() {
        return imperdonables;
    }

    public void setImperdonables(List<ImperdonableDTO> imperdonables) {
        this.imperdonables = imperdonables;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", incumplimientos=" + incumplimientos + ", incumplimientosPPR=" + incumplimientosPPR + ", imperdonables=" + imperdonables + '}';
    }

}
