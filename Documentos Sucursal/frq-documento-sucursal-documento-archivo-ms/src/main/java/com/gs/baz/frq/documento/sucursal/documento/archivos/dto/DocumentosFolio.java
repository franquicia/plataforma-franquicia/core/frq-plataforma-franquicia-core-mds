/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author b191312
 */
public class DocumentosFolio {

    @JsonProperty(value = "documentoArchivos")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private List<DocumentoFolio> documentoArchivos;

    public DocumentosFolio(List<DocumentoFolio> documentoArchivos) {
        this.documentoArchivos = documentoArchivos;
    }

    public List<DocumentoFolio> getDocumentoArchivos() {
        return documentoArchivos;
    }

    public void setDocumentoArchivos(List<DocumentoFolio> documentoArchivos) {
        this.documentoArchivos = documentoArchivos;
    }

}
