/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.rutas.dto.AltaRuta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.ParametrosRuta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.Ruta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaBase;
import com.gs.baz.frq.modelo.negocio.rutas.mprs.RutaRowMapper;
import com.gs.baz.frq.modelo.negocio.rutas.mprs.RutasRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class RutaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectFolio;
    private DefaultJdbcCall jdbcSelectFolioRuta;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNRUTA";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNRUTA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNRUTA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNRUTA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETMNRUTA");
        jdbcSelect.returningResultSet("PA_CDATOS", new RutaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETMNRUTA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new RutasRowMapper());

        jdbcSelectFolio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFolio.withSchemaName(schema);
        jdbcSelectFolio.withCatalogName(catalogo);
        jdbcSelectFolio.withProcedureName("SPGETMNRUTAXFOLIO");
        jdbcSelectFolio.returningResultSet("PA_CDATOS", new RutasRowMapper());

        jdbcSelectFolioRuta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFolioRuta.withSchemaName(schema);
        jdbcSelectFolioRuta.withCatalogName(catalogo);
        jdbcSelectFolioRuta.withProcedureName("SPGETMNRUTAXFOLIO");
        jdbcSelectFolioRuta.returningResultSet("PA_CDATOS", new RutaRowMapper());

    }

    public RutaBase selectRow(Long idRuta) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDRUTA", idRuta);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RutaBase> data = (List<RutaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Ruta> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDRUTA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Ruta>) out.get("PA_CDATOS");
    }

    public List<Ruta> selectFolio(Long idFolio) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDRUTA", null);
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        Map<String, Object> out = jdbcSelectFolio.execute(mapSqlParameterSource);
        return (List<Ruta>) out.get("PA_CDATOS");
    }

    public RutaBase selectFolioRuta(Long idRuta, Long idFolio) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDRUTA", idRuta);
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RutaBase> data = (List<RutaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public AltaRuta insertRow(ParametrosRuta entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCCODIFICACION", entityDTO.getCodificacion());
            mapSqlParameterSource.addValue("PA_FIIDPAIS", entityDTO.getIdPais());
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            mapSqlParameterSource.addValue("PA_FIIDUNIDADNEGOCIO", entityDTO.getIdUnidadNegocio());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else if (success == -2) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_IDRUTA");
                return new AltaRuta(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosRuta entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO.getIdRuta());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCCODIFICACION", entityDTO.getCodificacion());
            mapSqlParameterSource.addValue("PA_FIIDPAIS", entityDTO.getIdPais());
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            mapSqlParameterSource.addValue("PA_FIIDUNIDADNEGOCIO", entityDTO.getIdUnidadNegocio());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            } else if (success == -2) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
