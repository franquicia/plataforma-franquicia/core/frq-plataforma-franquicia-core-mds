/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de Visitas pendientes", value = "VisitasPendientes")
public class VisitasPendientes {

    @JsonProperty(value = "visitasPendientes")
    @ApiModelProperty(notes = "visitasPendientes")
    private List<VisitaPendiente> visitaPendiente;

    public VisitasPendientes(List<VisitaPendiente> visitaPendiente) {
        this.visitaPendiente = visitaPendiente;
    }

    public List<VisitaPendiente> getVisitaPendiente() {
        return visitaPendiente;
    }

    public void setVisitaPendiente(List<VisitaPendiente> visitaPendiente) {
        this.visitaPendiente = visitaPendiente;
    }

    @Override
    public String toString() {
        return "VisitasPendientes{" + "visitaPendiente=" + visitaPendiente + '}';
    }

}
