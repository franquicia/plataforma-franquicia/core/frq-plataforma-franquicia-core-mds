package com.gs.baz.frq.dashboard.certificacion.dao.graficos.g014;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g014.DashboardCertificacionG014DTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g014.DashboardCertificacionG014InvitadoRowMapper;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g014.DashboardCertificacionG014RowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG014DAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectInvitado;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PADASHCCPVDET");
        jdbcSelect.withProcedureName("SPCERTPREVIAS");
        jdbcSelect.returningResultSet("PA_CCERTPREVIAS", new DashboardCertificacionG014RowMapper());

        jdbcSelectInvitado = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectInvitado.withSchemaName(schema);
        jdbcSelectInvitado.withCatalogName("PADASHCCPVDET");
        jdbcSelectInvitado.withProcedureName("SPCERTPREVIAS");
        jdbcSelectInvitado.returningResultSet("PA_CCERTPREVIAS", new DashboardCertificacionG014InvitadoRowMapper());

    }

    public List<DashboardCertificacionG014DTO> selectRowDashboardCertificacionG014(String entityDTO1, Long entityDTO2, String entityDTO3, String entityDTO4, Long entityDTO5, Long entityDTO6) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO2);
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO3);
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO4);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO5);

        if (entityDTO6 == null || entityDTO6.intValue() == 0) { //Invitado
            mapSqlParameterSource.addValue("PA_FIINVITADO", null);
            Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
            return (List<DashboardCertificacionG014DTO>) out.get("PA_CCERTPREVIAS");
        } else {
            mapSqlParameterSource.addValue("PA_FIINVITADO", entityDTO6);
            Map<String, Object> out = jdbcSelectInvitado.execute(mapSqlParameterSource);
            return (List<DashboardCertificacionG014DTO>) out.get("PA_CCERTPREVIAS");
        }

    }

}
