/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Resuesta de rechazo del cliente", value = "RespuestaRechazaCliente")
public class RespuestaRechazaClienteDTO {

    @JsonProperty(value = "estatusIncidente")
    @ApiModelProperty(notes = "Estatus de modificado", example = "1")
    private Integer estatus;

    public RespuestaRechazaClienteDTO() {
        super();

    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return "RespuestaRechazaClienteDTO{" + "estatus=" + estatus + '}';
    }

}
