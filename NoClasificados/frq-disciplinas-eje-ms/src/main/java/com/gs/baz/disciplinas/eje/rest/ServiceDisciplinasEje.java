/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.eje.rest;

import com.gs.baz.disciplinas.eje.dao.DisciplinasEjeDAOImpl;
import com.gs.baz.disciplinas.eje.dto.DisciplinaEjeDTO;
import com.gs.baz.disciplinas.eje.rest.dto.DataDisciplinasEjeDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.DisciplinaDTO;
import com.gs.baz.model.bucket.client.services.dto.EjecutaDTO;
import com.gs.baz.model.bucket.client.services.dto.EjeModeloDTO;
import com.gs.baz.model.bucket.client.services.dto.FrecuenciaMedicionDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import com.gs.baz.model.bucket.client.services.dto.StatusDTO;
import com.gs.baz.model.bucket.client.services.dto.TipoMedicionDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplina;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjecuta;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjesModelo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFrecuenciaMedicion;
import com.gs.baz.model.bucket.client.services.rest.SubServiceStatus;
import com.gs.baz.model.bucket.client.services.rest.SubServiceTipoMedicion;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/disciplinas/eje")
public class ServiceDisciplinasEje {

    @Autowired
    private DisciplinasEjeDAOImpl disciplinasEjeDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceDisciplina sbsDisciplina;

    @Autowired
    private SubServiceStatus sbsStatus;

    @Autowired
    private SubServiceTipoMedicion sbsTipoMedicion;

    @Autowired
    private SubServiceFrecuenciaMedicion sbsFrecuenciaMedicion;

    @Autowired
    private SubServiceEjecuta sbsEjecuta;

    @Autowired
    private SubServiceEjesModelo sbsEjesModelo;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/{id_modelo_eje}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DisciplinaEjeDTO> getDisciplinaModelo(@PathVariable("id_modelo_eje") Long id_modelo_eje) throws CustomException {
        return disciplinasEjeDAOImpl.selectRowsDisciplinasEje(id_modelo_eje);
    }

    @RequestMapping(value = "secure/get/data/{id_modelo_eje}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataDisciplinasEjeDTO getDisciplinasModelo(@RequestHeader HttpHeaders headers, @PathVariable("id_modelo_eje") Long id_modelo_eje) throws CustomException, IOException {
        final DataDisciplinasEjeDTO dataDisciplinaEjeDTO = new DataDisciplinasEjeDTO();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            sbsEjesModelo.init(basePath, httpEntity);
            List<EjeModeloDTO> ejesModelo = sbsEjesModelo.getEjesModelo(id_modelo_eje.intValue());
            if (ejesModelo != null) {
                List<DisciplinaEjeDTO> disciplinasEje = disciplinasEjeDAOImpl.selectRowsDisciplinasEje(id_modelo_eje);
                if (!disciplinasEje.isEmpty()) {
                    sbsDisciplina.init(basePath, httpEntity);
                    sbsStatus.init(basePath, httpEntity);
                    sbsTipoMedicion.init(basePath, httpEntity);
                    sbsFrecuenciaMedicion.init(basePath, httpEntity);
                    sbsEjecuta.init(basePath, httpEntity);

                    List<DisciplinaDTO> disciplinas = sbsDisciplina.getDisciplina();
                    List<StatusDTO> status = sbsStatus.getStatus();
                    List<TipoMedicionDTO> tipoMedicion = sbsTipoMedicion.getTipoMedicion();
                    List<FrecuenciaMedicionDTO> frecuenciaMedicion = sbsFrecuenciaMedicion.getFrecuenciaMedicion();
                    List<EjecutaDTO> ejecuta = sbsEjecuta.getEjecuta();

                    metadata.setDisciplinas(disciplinas);
                    metadata.setStatus(status);
                    metadata.setTipoMedicion(tipoMedicion);
                    metadata.setFrecuenciaMedicion(frecuenciaMedicion);
                    metadata.setEjecuta(ejecuta);

                    dataDisciplinaEjeDTO.setMetadata(metadata);
                    dataDisciplinaEjeDTO.setDisciplinaEjeDTO(disciplinasEje);
                } else {
                    throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA);
                }
            } else {
                throw new CustomException(ModelCodes.DATA_NOT_FOUND);
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }

        return dataDisciplinaEjeDTO;
    }

}
