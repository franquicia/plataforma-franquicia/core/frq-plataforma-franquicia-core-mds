/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g003;

import com.gs.baz.frq.dashboard.dao.graficos.g003.DashboardDAOImpl003;
import com.gs.baz.frq.dashboard.dto.graficos.g003.DefectoPorMillonDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard003 {

    @Autowired
    private DashboardDAOImpl003 dashboardDAOImpl003;

    @RequestMapping(value = "secure/grafico/get/defecto/por/millon/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DefectoPorMillonDTO getDefectoPorMillonNivel(@RequestBody DefectoPorMillonDTO defectoPorMillon) throws CustomException {
        return dashboardDAOImpl003.selectRowsDefectoMillonSigmaNivel(defectoPorMillon);
    }

}
