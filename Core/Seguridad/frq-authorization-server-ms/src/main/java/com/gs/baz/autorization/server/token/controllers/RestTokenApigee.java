/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.token.controllers;

import com.gs.baz.autorization.server.apigee.bi.ApigeeJWT;
import com.gs.baz.autorization.server.apigee.bi.ApigeeJWTException;
import com.gs.baz.autorization.server.apigee.bi.LoginToken;
import com.gs.baz.autorization.server.apigee.bi.TokenApi;
import com.gs.baz.autorization.server.apigee.bi.TokenApiValidate;
import io.swagger.annotations.Api;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Api(tags = "oauth", value = "oauth", description = "API de OAuth2")
@RestController
@RequestMapping("/api/oauth/token/v1")
public class RestTokenApigee {

    @Autowired
    @Qualifier("restTemplateToken")
    RestTemplate restTemplate;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    ApigeeJWT apigeeJWT;

    /**
     *
     * @param loginToken
     * @return
     * @throws java.lang.Exception
     */
    @RequestMapping(value = "/generate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity getTokenOptionAPI(@RequestBody LoginToken loginToken) throws Exception {
        Map<String, Object> claims = new HashMap<>();
        String token = apigeeJWT.generarJWT(claims, loginToken.getUsername());
        return new ResponseEntity(new TokenApi(token), HttpStatus.OK);
    }

    /**
     *
     * @param tokenApiValidate
     * @return
     * @throws ApigeeJWTException
     */
    @RequestMapping(value = "/validate/apigee", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity validateTokenApigee(@RequestBody TokenApiValidate tokenApiValidate) throws ApigeeJWTException {
        apigeeJWT.validateJWTApigeeUser(tokenApiValidate.getToken(), tokenApiValidate.getUsuario(), tokenApiValidate.getLlavePublica());
        return new ResponseEntity(tokenApiValidate, HttpStatus.OK);
    }

    /**
     *
     * @param tokenApiValidate
     * @return
     * @throws ApigeeJWTException
     */
    @RequestMapping(value = "/validate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity validateToken(@RequestBody TokenApiValidate tokenApiValidate) throws ApigeeJWTException {
        apigeeJWT.validateJWT(tokenApiValidate.getToken(), tokenApiValidate.getLlavePublica());
        return new ResponseEntity(tokenApiValidate, HttpStatus.OK);
    }

}
