/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.Etiqueta;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioResumen;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firma;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firmas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ResumenDocumentoRowMapper implements RowMapper<BusquedaFolioResumen> {

    private BusquedaFolioResumen resumenDocumento;

    @Override
    public BusquedaFolioResumen mapRow(ResultSet rs, int rowNum) throws SQLException {
        resumenDocumento = new BusquedaFolioResumen();
        Documento documento = new Documento();
        documento.setTitulo(rs.getString("FCTITULO"));
        documento.setVersion(rs.getString("FCVERSION"));
        documento.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        documento.setObjetivo(rs.getString("FCOBJETIVO"));
        documento.setVigencia(rs.getString("FDVIGENTE"));
        resumenDocumento.setDocumento(documento);
        Firmas firmas = new Firmas();
        List<Etiqueta> etiquetas = new ArrayList<>();
        try {
            String arregloEtiquetas[] = rs.getString("ETIQUETAS").split(",");
            for (String etiqueta : arregloEtiquetas) {
                etiquetas.add(new Etiqueta(etiqueta));
            }
        } catch (Exception ex) {
        }
        resumenDocumento.setEtiquetas(etiquetas);
        List<Firma> firmasTemp = new ArrayList<>();
        try {
            String arregloAutorizador[] = rs.getString("AUTORIZADORES").split(",");
            for (String autorizador : arregloAutorizador) {
                Firma firma = new Firma();
                firma.setEmpleado(new Empleado(autorizador));
                firma.setIdJerarquia(2);
                firmasTemp.add(firma);
            }
        } catch (Exception ex) {
        }
        try {
            String arreglorevisor[] = rs.getString("REVISORES").split(",");
            for (String revisor : arreglorevisor) {
                Firma firma = new Firma();
                firma.setEmpleado(new Empleado(revisor));
                firma.setIdJerarquia(1);
                firmasTemp.add(firma);
            }
        } catch (Exception ex) {
        }
        firmas.setTodas(firmasTemp);
        resumenDocumento.setFirmas(firmas);
        return resumenDocumento;
    }
}
