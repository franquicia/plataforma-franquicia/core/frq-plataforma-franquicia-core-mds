/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.mprs;

import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.UnidadNegocioBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UnidadNegocioRowMapper implements RowMapper<UnidadNegocioBase> {

    private UnidadNegocioBase paisBase;

    @Override
    public UnidadNegocioBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        paisBase = new UnidadNegocioBase();
        paisBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        paisBase.setAcronimo(rs.getString("FCACRONIMO"));
        return paisBase;
    }
}
