package com.gs.baz.frq.documento.sucursal.cecos.init;

import com.gs.baz.frq.documento.sucursal.cecos.dao.DocumentoSucursalCecoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.cecos")
public class ConfigDocumentoScursalCeco {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoScursalCeco() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalCecoDAOImpl documentoSucursalCecoDAOImpl() {
        return new DocumentoSucursalCecoDAOImpl();
    }

}
