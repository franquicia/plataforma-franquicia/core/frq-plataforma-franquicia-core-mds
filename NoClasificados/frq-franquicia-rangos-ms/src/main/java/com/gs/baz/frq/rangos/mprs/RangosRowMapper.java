package com.gs.baz.frq.rangos.mprs;

import com.gs.baz.frq.rangos.dto.RangosDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RangosRowMapper implements RowMapper<RangosDTO> {

    private RangosDTO status;

    @Override
    public RangosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new RangosDTO();
        status.setIdRango(((BigDecimal) rs.getObject("FIRANGO_ID")));
        status.setNumeroRango(((BigDecimal) rs.getObject("FINUMRANGO")));
        status.setLimiteInferior(((BigDecimal) rs.getObject("FILIMITE_INF")));
        status.setLimiteSuperior(((BigDecimal) rs.getObject("FILIMITE_SUP")));
        status.setValorRango(rs.getString("FCVALOR_RANGO"));
        status.setDetalle(rs.getString("FCDETALLE"));
        status.setRegistroActivo((BigDecimal) rs.getObject("FIREG_ACTIVO"));
        return status;
    }
}
