package com.gs.baz.access.log.mprs;

import com.gs.baz.access.log.dto.AccessLogDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class AccessLogRowMapper implements RowMapper<AccessLogDTO> {

    private AccessLogDTO accessLog;

    @Override
    public AccessLogDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        accessLog = new AccessLogDTO();
        accessLog.setIdAccessLog((BigDecimal) rs.getObject("FIID"));
        accessLog.setIdUsuario(((BigDecimal) rs.getObject("FIIDUSUARIO")));
        accessLog.setHttpSession(rs.getString("FCHTTPSESSION"));
        accessLog.setFechaInicio(rs.getString("FDFECHAINICIO"));
        accessLog.setFechaFin(rs.getString("FDFECHAFIN"));
        accessLog.setIpOrigin(rs.getString("FCIPORIGEN"));
        accessLog.setIpLocal(rs.getString("FCIPLOCAL"));
        accessLog.setIdDispositivo((BigDecimal) rs.getObject("FIIDDISPOSITIV"));
        accessLog.setIdNavegador((BigDecimal) rs.getObject("FIIDNAVEGADOR"));
        accessLog.setNombreNavegador(rs.getString("FCNOMNAVEGA"));
        accessLog.setTipoNavegador(rs.getString("FCTIPONAVEGA"));
        accessLog.setGrupoNavegador(rs.getString("FCGRUPONAVEGA"));
        accessLog.setManofacturaNavegador(rs.getString("FCMANONAVEGA"));
        accessLog.setMotorRenderizacionNavegador(rs.getString("FCMTRENDONAVEG"));
        accessLog.setVersionNavegador(rs.getString("FCVERSIONNAVEG"));
        accessLog.setIdSistemaOperativo((BigDecimal) rs.getObject("FIIDSISTEMA"));
        accessLog.setNombreSistemaOperativo(rs.getString("FCNOMBRESO"));
        accessLog.setTipoDispositivo(rs.getString("FCTIPODISP"));
        accessLog.setGrupoSistemaOperativo(rs.getString("FCGRUPOSO"));
        accessLog.setManofacturaSistemaOperativo(rs.getString("FCMANOSO"));
        accessLog.setIdTipoCierre((BigDecimal) rs.getObject("FIIDTIPOCIERRE"));
        return accessLog;
    }
}
