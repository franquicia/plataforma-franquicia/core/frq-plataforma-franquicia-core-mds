/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.mprs;

import com.gs.baz.frq.documento.sucursal.documentos.dto.DocumentoCeco;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author b191312
 */
public class DocumentoCecoRowMapper implements RowMapper<DocumentoCeco> {

    @Override
    public DocumentoCeco mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocumentoCeco documentoCeco = new DocumentoCeco();

        documentoCeco.setIdCeco(rs.getInt("FCID_CECO"));
        documentoCeco.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        documentoCeco.setIdFormato(rs.getInt("FIID_FORMATO"));
        documentoCeco.setNombreFormato(rs.getString("FORMATO"));
        documentoCeco.setIdFolio(rs.getInt("FIID_FOLIO"));
        documentoCeco.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        documentoCeco.setNombreProyecto(rs.getString("NOM_PROY"));
        documentoCeco.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        documentoCeco.setNombreDocumento(rs.getString("DOCUMENTO"));
        documentoCeco.setIdGeneraHistorico(rs.getInt("FIID_GENHISTO"));

        return documentoCeco;
    }

}
