package com.gs.baz.frq.respuesta.visita.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.respuesta.visita.dao.util.GenericDAO;
import com.gs.baz.frq.respuesta.visita.dto.RespuestaVisitaDTO;
import com.gs.baz.frq.respuesta.visita.mprs.DetalleRowMapper;
import com.gs.baz.frq.respuesta.visita.mprs.RespuestaVisitaRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class RespuestaVisitaDAOImpl extends DefaultDAO implements GenericDAO<RespuestaVisitaDTO> {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcgetDetalle;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMRESUMVISI");
        jdbcSelect.withProcedureName("SPGETRESUMVISITA");
        jdbcSelect.returningResultSet("PA_CDATOS", new RespuestaVisitaRowMapper());
        jdbcgetDetalle = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcgetDetalle.withSchemaName(schema);
        jdbcgetDetalle.withCatalogName("PAADMRESUMVISI");
        jdbcgetDetalle.withProcedureName("SPGETDETALLE");
        jdbcSelect.returningResultSet("PA_CDATOS", new DetalleRowMapper());

    }

    @Override
    public List<RespuestaVisitaDTO> selectRows(RespuestaVisitaDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIVISITA_ID", entityDTO.getIdVisita());
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO.getIdCecoBase());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<RespuestaVisitaDTO>) out.get("PA_CDATOS");
    }

    public List<RespuestaVisitaDTO> getDetalle(RespuestaVisitaDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        Map<String, Object> out = jdbcgetDetalle.execute(mapSqlParameterSource);
        return (List<RespuestaVisitaDTO>) out.get("PA_CDATOS");
    }

}
