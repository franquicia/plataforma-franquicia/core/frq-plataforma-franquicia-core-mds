/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del Estado del Documento", value = "EstadoDocumentoBase")
public class EstadoDocumentoBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del estado documento", example = "Cuando un documento se puede editar")
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del estatus del documento", example = "Por autorizar")
    private String nombre;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "EstadoDocumentoBase{" + "descripcion=" + descripcion + ", nombre=" + nombre + '}';
    }

}
