/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.file.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import java.nio.file.Path;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param fullPathFile
     * @return @throws CustomException
     */
    public dto readFileB64(String fullPathFile) throws CustomException;

    /**
     *
     * @param fullPathFile
     * @return @throws CustomException
     */
    public Path readFile(String fullPathFile) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto writeFile(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto writeDirectory(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public List<dto> readDirectory(dto entityDTO) throws CustomException;

}
