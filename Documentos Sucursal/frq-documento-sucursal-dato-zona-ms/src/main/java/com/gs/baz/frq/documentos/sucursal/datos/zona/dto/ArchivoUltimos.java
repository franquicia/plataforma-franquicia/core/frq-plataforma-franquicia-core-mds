/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Verifica las zonas que existen por Version", value = "ZonasPorVersion")
public class ArchivoUltimos {

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Indica el valor del ceco", example = "1")
    private Integer idCeco;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Indica el valor del formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Indicador del id de documento ", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Indicador del id de Proyecto ", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador delfolio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Nombre del Proyecto", example = "Nuevas")
    private String nombreProyecto;

    @JsonProperty(value = "nombreFormato")
    @ApiModelProperty(notes = "Nombre del Formato", example = "Retail")
    private String nombreFormato;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Nombre del Documento", example = "LAYOUT")
    private String nombreDocumento;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

}
