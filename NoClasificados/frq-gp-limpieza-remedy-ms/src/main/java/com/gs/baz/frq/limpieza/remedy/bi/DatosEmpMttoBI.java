package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.DatosEmpMttoDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.DatosEmpMttoDTO;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class DatosEmpMttoBI {

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private DatosEmpMttoDAOImpl datosEmpMttoDAO;

    public DatosEmpMttoBI() {
        super();

    }

    public List<DatosEmpMttoDTO> obtieneDatos(int idUsuario) {
        List<DatosEmpMttoDTO> listafila = null;
        try {
            listafila = datosEmpMttoDAO.obtieneDatos(idUsuario);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener los datos", e);

        }

        return listafila;
    }

    public List<DatosEmpMttoDTO> obtieneInfo() {
        List<DatosEmpMttoDTO> listafila = null;
        try {
            listafila = datosEmpMttoDAO.obtieneInfo();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla Usuarios", e);

        }

        return listafila;
    }

}
