package com.gs.baz.frq.modelo.negocio.usuarios.init;

import com.gs.baz.frq.modelo.negocio.usuarios.dao.ModeloNegocioUsuarioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.usuarios")
public class ConfigModeloNegocioUsuario {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioUsuario() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ModeloNegocioUsuarioDAOImpl modeloNegocioUsuarioDAOImpl() {
        return new ModeloNegocioUsuarioDAOImpl();
    }

}
