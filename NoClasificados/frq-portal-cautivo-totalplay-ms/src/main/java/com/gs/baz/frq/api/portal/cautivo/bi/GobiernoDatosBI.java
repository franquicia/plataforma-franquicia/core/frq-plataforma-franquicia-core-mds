/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.bi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.gs.baz.frq.api.portal.cautivo.dao.GobiernoDatosDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dao.IdentificadorDAOImpl;
import com.gs.baz.frq.api.portal.cautivo.dto.BancaDigitalDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.ConexionesDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.ICUDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.IdentificadorDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.PaginacionDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.PortalCautivoDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.RespuestaICUDTO;
import com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente.ECServicioIdentificador;
import com.gs.baz.frq.api.portal.cautivo.util.Parametros;
import com.gs.baz.frq.api.portal.cautivo.util.Util;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author MADA
 */
public class GobiernoDatosBI {

    public final Logger logger = LogManager.getLogger();

    @Autowired
    GobiernoDatosDAOImpl gobiernoDatosDAOImpl;

    public List<ICUDTO> obtieneICU(String fechaInicio, String fechaFin, String identificador) throws CustomException {
        List<ICUDTO> respuesta = null;

        try {
            respuesta = gobiernoDatosDAOImpl.obtieneICU(fechaInicio, fechaFin, identificador);

        } catch (Exception e) {
            logger.info("No fue posible obtener datos " + e);

        }
        return respuesta;
    }

    public List<PortalCautivoDTO> obtienePortalCautivo(String fechaInicio, String fechaFin, String mac) throws CustomException {
        List<PortalCautivoDTO> respuesta = null;

        try {
            respuesta = gobiernoDatosDAOImpl.obtienePortalCautivo(fechaInicio, fechaFin, mac);

        } catch (Exception e) {
            logger.info("No fue posible obtener datos " + e);

        }
        return respuesta;
    }

    public List<BancaDigitalDTO> obtieneBancaDigital(String fechaInicio, String fechaFin, String mac) throws CustomException {
        List<BancaDigitalDTO> respuesta = null;

        try {
            respuesta = gobiernoDatosDAOImpl.obtieneBancaDigital(fechaInicio, fechaFin, mac);

        } catch (Exception e) {
            logger.info("No fue posible obtener datos " + e);

        }
        return respuesta;
    }

}
