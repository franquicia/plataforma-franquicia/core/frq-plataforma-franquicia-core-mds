/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.mprs;

import com.gs.baz.frq.modelo.negocio.rutas.dto.Codificacion;
import com.gs.baz.frq.modelo.negocio.rutas.dto.Modelo;
import com.gs.baz.frq.modelo.negocio.rutas.dto.Pais;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaCompuesta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.UnidadNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class RutasCompuestaRowMapper implements RowMapper<RutaCompuesta> {

    private RutaCompuesta rutaCompuesta;

    @Override
    public RutaCompuesta mapRow(ResultSet rs, int rowNum) throws SQLException {
        rutaCompuesta = new RutaCompuesta();
        rutaCompuesta.setIdRuta(rs.getInt("FIIDRUTA"));
        Codificacion codificacion = new Codificacion();
        codificacion.setDescripcion(rs.getString("FCCODIFICACION"));
        rutaCompuesta.setCodificacion(codificacion);
        Pais pais = new Pais();
        pais.setDescripcion(rs.getString("FCDESCRIPCIONPAIS"));
        pais.setIdPais(rs.getInt("FIIDPAIS"));
        rutaCompuesta.setPais(pais);
        Modelo modelo = new Modelo();
        modelo.setIdModelo(rs.getInt("FIIDMODELO"));
        modelo.setDescripcion(rs.getString("FCDESCRIPCIONMODELO"));
        rutaCompuesta.setModelo(modelo);
        UnidadNegocio unidadNegocio = new UnidadNegocio();
        unidadNegocio.setIdUnidadNegocio(rs.getInt("FIIDUNIDADNEGOCIO"));
        unidadNegocio.setDescripcion(rs.getString("FCDESCRIPCIONUN"));
        rutaCompuesta.setUnidadNegocio(unidadNegocio);
        return rutaCompuesta;
    }
}
