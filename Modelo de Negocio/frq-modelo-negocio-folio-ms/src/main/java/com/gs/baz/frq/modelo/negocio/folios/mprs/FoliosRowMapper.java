/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.EstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.Folio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FoliosRowMapper implements RowMapper<Folio> {

    private Folio folio;

    @Override
    public Folio mapRow(ResultSet rs, int rowNum) throws SQLException {
        folio = new Folio();
        folio.setFechaCreacion(rs.getString("FDFECHACREACION"));
        folio.setCreador(new Empleado(rs.getInt("FIIDCREADOR")));
        folio.setEstado(new EstadoFolio(rs.getInt("FIIDEDODOCTO")));
        folio.setIndicePaso(rs.getInt("FIINDICEPASO"));
        folio.setFechaModificacion(rs.getString("FDFECHA_MOD"));
        return folio;
    }
}
