/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g015;

import com.gs.baz.frq.dashboard.dao.graficos.g015.DashboardDAOImpl015;
import com.gs.baz.frq.dashboard.dto.graficos.g015.CumplimientoCalificacionDisciplinaNegocioDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard015 {

    @Autowired
    private DashboardDAOImpl015 dashboardDAOImpl015;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/calificacion/disciplina/negocio/punto/contacto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CumplimientoCalificacionDisciplinaNegocioDTO> selectRowsCumplimientoCalificacionDisciplinaNegocioNivel(@RequestBody CumplimientoCalificacionDisciplinaNegocioDTO cumplimientoCalificacionDisciplinaNegocioDTO) throws CustomException {
        return dashboardDAOImpl015.selectRowsCumplimientoCalificacionDisciplinaNegocioNivel(cumplimientoCalificacionDisciplinaNegocioDTO);
    }
}
