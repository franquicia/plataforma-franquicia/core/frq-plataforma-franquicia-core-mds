package com.gs.baz.frq.respuesta.visita.mprs;

import com.gs.baz.frq.respuesta.visita.dto.RespuestaVisitaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RespuestaVisitaRowMapper implements RowMapper<RespuestaVisitaDTO> {

    private RespuestaVisitaDTO vistaPuntoContactoDTO;

    @Override
    public RespuestaVisitaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPuntoContactoDTO = new RespuestaVisitaDTO();
        vistaPuntoContactoDTO.setIdVisita(((BigDecimal) rs.getObject("FIVISITA_ID")));
        vistaPuntoContactoDTO.setIdCecoBase(((BigDecimal) rs.getObject("FICECOBASE_ID")));
        vistaPuntoContactoDTO.setPreCalificacion((BigDecimal) rs.getObject("FIPRECALIFICA"));
        vistaPuntoContactoDTO.setCalificacionLineal((BigDecimal) rs.getObject("FICALIF_LINEAL"));
        vistaPuntoContactoDTO.setCalificacionFinal((BigDecimal) rs.getObject("FICALIF_FINAL"));
        vistaPuntoContactoDTO.setConteoSi((BigDecimal) rs.getObject("FICONTEO_SI"));
        vistaPuntoContactoDTO.setConteoNo((BigDecimal) rs.getObject("FICONTEO_NO"));
        vistaPuntoContactoDTO.setConteoNa((BigDecimal) rs.getObject("FICONTEO_NA"));
        vistaPuntoContactoDTO.setConteoImperdonable((BigDecimal) rs.getObject("FICONTEO_IMP"));
        vistaPuntoContactoDTO.setIdProtocolo((BigDecimal) rs.getObject("FIID_PROTOCOLO"));
        vistaPuntoContactoDTO.setFecha(rs.getString("FDFECHA"));
        vistaPuntoContactoDTO.setIdUsuario((BigDecimal) rs.getObject("FIID_USUARIO"));
        vistaPuntoContactoDTO.setNombre((rs.getString("FCNOMBRE")));
        vistaPuntoContactoDTO.setIdPuesto((BigDecimal) rs.getObject("FIID_PUESTO"));
        vistaPuntoContactoDTO.setConteoIncumplimiento((BigDecimal) rs.getObject("FICONTEO_INC"));
        vistaPuntoContactoDTO.setIdNegocioDisciplina(((BigDecimal) rs.getObject("FINEGOCIODISCID")));
        vistaPuntoContactoDTO.setIdDefectos((BigDecimal) rs.getObject("FIDEFECTOS"));
        vistaPuntoContactoDTO.setOportunidades((BigDecimal) rs.getObject("FIOPORTUNIDADES"));
        vistaPuntoContactoDTO.setPuntoEsperado(((BigDecimal) rs.getObject("FIPUNTOSESP")));
        return vistaPuntoContactoDTO;
    }
}
