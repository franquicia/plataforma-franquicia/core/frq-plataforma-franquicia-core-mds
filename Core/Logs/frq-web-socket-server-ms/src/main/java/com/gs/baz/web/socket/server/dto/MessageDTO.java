/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.web.socket.server.dto;

import java.io.Serializable;

/**
 *
 * @author cescobarh
 */
public class MessageDTO implements Serializable {

    private String name;

    public MessageDTO() {
    }

    public MessageDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MessageDTO{" + "name=" + name + '}';
    }

}
