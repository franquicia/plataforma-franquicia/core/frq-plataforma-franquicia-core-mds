/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.paises.mprs;

import com.gs.baz.frq.modelo.negocio.paises.dto.PaisBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class PaisRowMapper implements RowMapper<PaisBase> {

    private PaisBase paisBase;

    @Override
    public PaisBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        paisBase = new PaisBase();
        paisBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        paisBase.setAcronimo(rs.getString("FCACRONIMO"));
        return paisBase;
    }
}
