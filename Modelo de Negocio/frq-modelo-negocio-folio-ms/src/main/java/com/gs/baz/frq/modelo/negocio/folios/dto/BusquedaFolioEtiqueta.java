/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del folio y documento", value = "BusquedaFolioEtiqueta")
public class BusquedaFolioEtiqueta extends Folio {

    @JsonProperty(value = "unidadesNegocio")
    @ApiModelProperty(notes = "unidadesNegocio")
    private List<UnidadNegocio> unidadesNegocio;

    @JsonProperty(value = "calificacion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Calificacion del documento", example = "1")
    private Integer calificacion;

    @JsonProperty(value = "vistas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Vistas del documento", example = "1")
    private Integer vistas;

    @JsonProperty(value = "fechaPublicacion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Fecha de publicacion", example = "01/01/2021")
    private String fechaPublicacion;

    @JsonProperty(value = "modeloNegocio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Modelo de negocio")
    private List<ModeloNegocio> modelosNegocio;

    public BusquedaFolioEtiqueta() {
    }

    public List<UnidadNegocio> getUnidadesNegocio() {
        return unidadesNegocio;
    }

    public void setUnidadesNegocio(List<UnidadNegocio> unidadesNegocio) {
        this.unidadesNegocio = unidadesNegocio;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getVistas() {
        return vistas;
    }

    public void setVistas(Integer vistas) {
        this.vistas = vistas;
    }

    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public List<ModeloNegocio> getModelosNegocio() {
        return modelosNegocio;
    }

    public void setModelosNegocio(List<ModeloNegocio> modelosNegocio) {
        this.modelosNegocio = modelosNegocio;
    }

    @Override
    public String toString() {
        return "BusquedaFolioEtiqueta{" + "unidadesNegocio=" + unidadesNegocio + ", calificacion=" + calificacion + ", vistas=" + vistas + ", fechaPublicacion=" + fechaPublicacion + ", modelosNegocio=" + modelosNegocio + '}';
    }

}
