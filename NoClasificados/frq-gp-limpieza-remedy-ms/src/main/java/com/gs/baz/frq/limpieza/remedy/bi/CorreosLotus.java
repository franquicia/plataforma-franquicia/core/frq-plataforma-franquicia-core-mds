/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.bi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author leodan1991
 */
public class CorreosLotus {

    private String httpsUrlPro = "http://10.50.183.58:80/apps/envionotificacionesgs.nsf/ObtieneInformacionEmpleado?WSDL";
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Integer TIMECONNECTION = 5000;
    private static final Integer DOS = 2;

    @SuppressWarnings("all")
    public String validaUsuarioLotusCorreos(String idUsuario) {
        if (idUsuario == null) {
            return "";
        }
        String correo = "";
        HttpURLConnection rc = null;
        BufferedReader read = null;
        OutputStream outStr = null;

        try {

            // crear la llamada al servidor
            LOGGER.info("CORREOS LOTUS");
            URL url = new URL(httpsUrlPro);
            rc = (HttpURLConnection) url.openConnection();

            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Accept-Charset", "UTF-8");
            rc.setRequestProperty("Content-Type", "text/html"); // application/soap+xml

            String reqStr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"
                    + "\" xmlns:urn=\"urn:webservice.gruposalinas.com\">"
                    + "<soapenv:Header/>" + "<soapenv:Body>" + "<urn:usuario>UsrR3fl3x1s</urn:usuario>" + "<urn:xml>"
                    + "&lt;EMPLEADOS&gt;&lt;EMPLEADO&gt;&lt;NUMERO&gt;" + idUsuario
                    + "&lt;/NUMERO&gt;&lt;EMPRESA&gt;EKT&lt;/EMPRESA&gt;&lt;/EMPLEADO&gt;&lt;/EMPLEADOS&gt;"
                    + "</urn:xml>" + "</soapenv:Body>" + "</soapenv:Envelope>";

            // several more definitions to request
            int len = reqStr.length();
            rc.setRequestProperty("Content-Length", Integer.toString(len));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setConnectTimeout(TIMECONNECTION);
            rc.connect();

            outStr = rc.getOutputStream();
            outStr.write(reqStr.getBytes(java.nio.charset.StandardCharsets.UTF_8));
            outStr.flush();

            read = null;
            read = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            String line;

            while ((line = read.readLine()) != null) {
                if (line.contains("MAIL")) {
                    String[] tags = line.split("MAIL");
                    correo = tags[1].replace("&lt;/", "").replace("&gt;", "");
                }

            }
        } catch (Exception e) {
            return "error2" + e.getMessage() + e.getStackTrace() + e.getCause() + e.hashCode() + e.toString()
                    + e.getLocalizedMessage();
        } finally {
            try {
                if (rc != null) {
                    rc.disconnect();
                }
            } catch (Exception e) {

                LOGGER.info(e.getMessage());
            }

            try {
                if (read != null) {
                    read.close();
                }
            } catch (Exception e) {

                LOGGER.info(e.getMessage());
            }
            try {
                if (outStr != null) {
                    outStr.close();
                }
            } catch (Exception e) {

                LOGGER.info(e.getMessage());
            }
        }
        return correo;
    }

    @SuppressWarnings("all")
    public String validaCorreoSuc(String idCeco) {
        String correoSuc = "";
        String eco = "";

        try {
            eco = "G" + idCeco.substring(DOS);
            correoSuc = eco + "@elektra.com.mx";
        } catch (Exception e) {
            LOGGER.info("AP al obtener el ceco");
            correoSuc = "";
        }

        return correoSuc;
    }

}
