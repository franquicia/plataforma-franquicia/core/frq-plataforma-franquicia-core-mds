/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.paises.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Pais", value = "Pais")
public class Pais extends PaisBase {

    @JsonProperty(value = "idPais")
    @ApiModelProperty(notes = "Identificador del Pais", example = "1", position = -1)
    private Integer idPais;

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    @Override
    public String toString() {
        return "Pais{" + "idPais=" + idPais + '}';
    }

}
