/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class CalificacionGeografia extends CalificacionGeografiaBase {

    @JsonProperty(value = "calificacion")
    @ApiModelProperty(notes = "Calificacion", example = "1")
    private Integer calificacion;

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "CalificacionGeografia{" + "calificacion=" + calificacion + '}';
    }

}
