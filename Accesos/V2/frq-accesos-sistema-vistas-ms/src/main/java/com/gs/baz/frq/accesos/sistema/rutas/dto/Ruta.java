/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de  la ruta", value = "Ruta")
public class Ruta extends RutaBase {

    @JsonProperty(value = "idRuta")
    private Integer idRuta;

    public Ruta() {
    }

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    @Override
    public String toString() {
        return "Ruta{" + "idRuta=" + idRuta + '}';
    }

}
