/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author fjaramillo
 */
public class ParametrosFolio {

    @JsonProperty(value = "numeroUsuario", required = true)
    @ApiModelProperty(notes = "Usuario que esta creando el folio", example = "191312", position = -3)
    private String numeroUsuario;

    @JsonProperty(value = "estatus", required = true)
    @ApiModelProperty(notes = "Estado del folio", example = "1", position = -2)
    private Integer estatus;

    @JsonProperty(value = "eliminadoLogico", required = true)
    @ApiModelProperty(notes = "Estado del folio", example = "1", position = -1)
    private Integer eliminadoLogico;

    public Integer getEliminadoLogico() {
        return eliminadoLogico;
    }

    public void setEliminadoLogico(Integer eliminadoLogico) {
        this.eliminadoLogico = eliminadoLogico;
    }

    public String getNumeroUsuario() {
        return numeroUsuario;
    }

    public void setNumeroUsuario(String numeroUsuario) {
        this.numeroUsuario = numeroUsuario;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

}
