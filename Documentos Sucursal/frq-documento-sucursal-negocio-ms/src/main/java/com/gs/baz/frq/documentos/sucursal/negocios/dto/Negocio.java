/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Negocio", value = "Negocio")
public class Negocio extends NegocioBase {

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del Negocio", example = "1", position = -1)
    private Integer idNegocio;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    @Override
    public String toString() {
        return "Negocio{" + "idNegocio=" + idNegocio + '}';
    }

}
