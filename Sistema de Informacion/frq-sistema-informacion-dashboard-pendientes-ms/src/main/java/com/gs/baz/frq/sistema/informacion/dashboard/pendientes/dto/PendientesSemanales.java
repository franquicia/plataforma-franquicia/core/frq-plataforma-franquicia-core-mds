/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de Visitas pendientes por semana", value = "VisitasPendientesSemanales")
public class PendientesSemanales {

    @JsonProperty(value = "visitasPendientesSemanales")
    @ApiModelProperty(notes = "visitasPendientesSemanales")
    private List<PendientesSemanal> visitasPendientesSemanales;

    public List<PendientesSemanal> getVisitasPendientesSemanales() {
        return visitasPendientesSemanales;
    }

    public void setVisitasPendientesSemanales(List<PendientesSemanal> visitasPendientesSemanales) {
        this.visitasPendientesSemanales = visitasPendientesSemanales;
    }

    @Override
    public String toString() {
        return "PendientesSemanales{" + "visitasPendientesSemanales=" + visitasPendientesSemanales + '}';
    }

}
