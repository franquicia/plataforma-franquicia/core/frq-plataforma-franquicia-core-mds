/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la unidad de negocio", value = "AltaUnidadNegocio")
public class AltaUnidadNegocio {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador de la unidad de negocio", example = "1")
    private Integer id;

    public AltaUnidadNegocio(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
