/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NegocioIncumplimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegociosIncumplimientoRowMapper implements RowMapper<NegocioIncumplimiento> {

    @Override
    public NegocioIncumplimiento mapRow(ResultSet rs, int rowNum) throws SQLException {
        NegocioIncumplimiento negocioIncumplimiento = new NegocioIncumplimiento();
        negocioIncumplimiento.setIdNegocio(rs.getInt("FINEGOCIODISCID"));
        return negocioIncumplimiento;
    }
}
