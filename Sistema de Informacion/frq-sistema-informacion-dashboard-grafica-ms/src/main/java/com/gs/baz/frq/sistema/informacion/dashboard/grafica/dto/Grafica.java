/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la Grafica", value = "Grafica")
public class Grafica extends GraficaBase {

    @JsonProperty(value = "idGrafica")
    @ApiModelProperty(notes = "Identificador de la Grafica", example = "1", position = -1)
    private Integer idGrafica;

    public Integer getIdGrafica() {
        return idGrafica;
    }

    public void setIdGrafica(Integer idGrafica) {
        this.idGrafica = idGrafica;
    }

    @Override
    public String toString() {
        return "Grafica{" + "idGrafica=" + idGrafica + '}';
    }

}
