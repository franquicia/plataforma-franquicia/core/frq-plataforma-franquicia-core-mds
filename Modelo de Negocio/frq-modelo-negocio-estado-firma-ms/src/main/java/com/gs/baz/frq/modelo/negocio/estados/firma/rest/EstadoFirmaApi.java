/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.estados.firma.dao.EstadoFirmaDAOImpl;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.AltaEstadoFirma;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.EstadoFirmaBase;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.EstadosFirma;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.ParametrosEstadoFirma;
import com.gs.baz.frq.modelo.negocio.estados.firma.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "catalogos-estado-firma", value = "catalogos-estado-firma", description = "Gestión del catalogo de estado de la firmas")
@RestController
@RequestMapping("/api-local/modelo-negocio/catalogos/estado-firma/v1")
public class EstadoFirmaApi {

    @Autowired
    private EstadoFirmaDAOImpl estadoFirmaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idEstadoFirma
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene estado firma", notes = "Obtiene un estado firma", nickname = "obtieneEstadoFirma")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idEstadoFirma}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstadoFirmaBase obtieneEstadoFirma(@ApiParam(name = "idEstadoFirma", value = "Identificador del estado firma", example = "1", required = true) @PathVariable("idEstadoFirma") Long idEstadoFirma) throws CustomException, DataNotFoundException {
        EstadoFirmaBase estadoFirmaBase = estadoFirmaDAOImpl.selectRow(idEstadoFirma);
        if (estadoFirmaBase == null) {
            throw new DataNotFoundException();
        }
        return estadoFirmaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene estado firma", notes = "Obtiene todos los estados de la firma", nickname = "obtieneEstadoFirma")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstadosFirma obtieneEstadoFirmas() throws CustomException, DataNotFoundException {
        EstadosFirma estadosFirma = new EstadosFirma(estadoFirmaDAOImpl.selectAllRows());
        if (estadosFirma.getEstadosFirma() != null && estadosFirma.getEstadosFirma().isEmpty()) {
            throw new DataNotFoundException();
        }
        return estadosFirma;
    }

    /**
     *
     * @param parametrosEstadoFirma
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear estadoFirma", notes = "Agrega un estado de la firma", nickname = "creaEstadoFirma")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaEstadoFirma creaEstadoFirma(@ApiParam(name = "ParametrosEstadoFirma", value = "Paramentros para el alta del estado de documento", required = true) @RequestBody ParametrosEstadoFirma parametrosEstadoFirma) throws DataNotInsertedException {
        return estadoFirmaDAOImpl.insertRow(parametrosEstadoFirma);
    }

    /**
     *
     * @param parametrosEstadoFirma
     * @param idEstadoFirma
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar estado firma", notes = "Actualiza un estado de la firma", nickname = "actualizaEstadoFirma")
    @RequestMapping(value = "/{idEstadoFirma}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaEstadoFirma(@ApiParam(name = "ParametrosEstadoFirma", value = "Paramentros para la actualización del estado de la firma", required = true) @RequestBody ParametrosEstadoFirma parametrosEstadoFirma, @ApiParam(name = "idEstadoFirma", value = "Identificador del estado de la firma", example = "1", required = true) @PathVariable("idEstadoFirma") Integer idEstadoFirma) throws DataNotUpdatedException {
        parametrosEstadoFirma.setIdEstadoFirma(idEstadoFirma);
        estadoFirmaDAOImpl.updateRow(parametrosEstadoFirma);
        return new SinResultado();
    }

    /**
     *
     * @param idEstadoFirma
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar estado de la firma", notes = "Elimina un item de los estados de la firma", nickname = "eliminaEstadoFirma")
    @RequestMapping(value = "/{idEstadoFirma}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaEstadoFirma(@ApiParam(name = "idEstadoFirma", value = "Identificador del estado de la firma", example = "1", required = true) @PathVariable("idEstadoFirma") Integer idEstadoFirma) throws DataNotDeletedException {
        estadoFirmaDAOImpl.deleteRow(idEstadoFirma);
        return new SinResultado();
    }

}
