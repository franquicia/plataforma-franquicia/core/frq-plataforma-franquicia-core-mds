/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de Firmas", value = "InsertaFirmas")
public class InsertaFirmas {


    @JsonProperty(value = "idJerarquia")
    @ApiModelProperty(notes = "Identificador de la jerarquia", example = "1", position = -1)
    private Integer idJerarquia;
    
    @JsonProperty(value = "estadoFirma")
    @ApiModelProperty(notes = "Estado de la firma", example = "1", position = -1)
    private Integer estadoFirma;
    
    @JsonProperty(value = "auxiliarUno")
    @ApiModelProperty(notes = "Nombre del auxiliar uno", example = "", position = -1)
    private String auxiliarUno;
    
    @JsonProperty(value = "auxiliarDos")
    @ApiModelProperty(notes = "Nombre del auxiliar dos", example = "1", position = -1)
    private String auxiliarDos;
    
    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private Integer idFolio;

    public Integer getIdJerarquia() {
        return idJerarquia;
    }

    public void setIdJerarquia(Integer idJerarquia) {
        this.idJerarquia = idJerarquia;
    }

    public Integer getEstadoFirma() {
        return estadoFirma;
    }

    public void setEstadoFirma(Integer estadoFirma) {
        this.estadoFirma = estadoFirma;
    }

    public String getAuxiliarUno() {
        return auxiliarUno;
    }

    public void setAuxiliarUno(String auxiliarUno) {
        this.auxiliarUno = auxiliarUno;
    }

    public String getAuxiliarDos() {
        return auxiliarDos;
    }

    public void setAuxiliarDos(String auxiliarDos) {
        this.auxiliarDos = auxiliarDos;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    @Override
    public String toString() {
        return "InsertaFirmas{" + "idJerarquia=" + idJerarquia + ", estadoFirma=" + estadoFirma + ", auxiliarUno=" + auxiliarUno + ", auxiliarDos=" + auxiliarDos + ", idFolio=" + idFolio + '}';
    }

}
