package com.gs.baz.indicador.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.indicador.dao.IndicadorDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.indicador")
public class ConfigIndicador {

    private final Logger logger = LogManager.getLogger();

    public ConfigIndicador() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public IndicadorDAOImpl indicadorDAOImpl() {
        return new IndicadorDAOImpl();
    }
}
