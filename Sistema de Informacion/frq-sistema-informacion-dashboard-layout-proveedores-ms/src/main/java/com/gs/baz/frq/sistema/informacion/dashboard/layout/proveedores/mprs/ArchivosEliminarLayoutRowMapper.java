/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.ArchivoEliminar;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ArchivosEliminarLayoutRowMapper implements RowMapper<ArchivoEliminar> {

    @Override
    public ArchivoEliminar mapRow(ResultSet rs, int rowNum) throws SQLException {
        ArchivoEliminar archivoEliminar = new ArchivoEliminar();
        archivoEliminar.setIdArchivo(rs.getInt("FIIDARCHIVO"));
        archivoEliminar.setIdFolio(rs.getInt("FIIDFOLIO"));
        archivoEliminar.setRuta(rs.getString("FCRUTA"));
        archivoEliminar.setTipo(rs.getInt("FITIPO"));
        archivoEliminar.setPeso(rs.getInt("FIPESO"));
        archivoEliminar.setIdEstado(rs.getInt("FIESTADO"));
        archivoEliminar.setAuxiliarUno(rs.getString("FCAUXILIAR1"));
        archivoEliminar.setAuxiliarDos(rs.getString("FCAUXILIAR2"));
       
        return archivoEliminar;
    }
}
