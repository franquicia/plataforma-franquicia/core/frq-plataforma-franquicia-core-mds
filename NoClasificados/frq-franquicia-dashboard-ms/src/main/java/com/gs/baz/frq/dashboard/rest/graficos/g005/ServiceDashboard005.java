/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g005;

import com.gs.baz.frq.dashboard.dao.graficos.g005.DashboardDAOImpl005;
import com.gs.baz.frq.dashboard.dto.graficos.g005.CumplimientoDisciplinaNegociosDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g005.CumplimientoDisciplinaNegocioDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard005 {

    @Autowired
    private DashboardDAOImpl005 dashboardDAOImpl005;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/disciplina/negocio/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CumplimientoDisciplinaNegociosDTO getCumplimientoNegocioDisciplinaNacional(@RequestBody CumplimientoDisciplinaNegocioDTO cumplimientoNacionalNegocio) throws CustomException {
        return dashboardDAOImpl005.getCumplimientoNegocioDisciplina(cumplimientoNacionalNegocio);
    }

}
