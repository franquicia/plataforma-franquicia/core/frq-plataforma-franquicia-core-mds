/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.rest.web.socket.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.server.logs.appender.ClientSocketLogs;
import com.gs.baz.server.logs.appender.NodeSession;
import java.util.List;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

/**
 *
 * @author cescobarh
 */
@Controller
public class ActionsController {

    private final ClientSocketLogs clientSocketLogs = new ClientSocketLogs();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @MessageMapping("/actions")
    @SendTo("/topic/actions")
    public List<Action> replyActions(List<Action> actions) throws Exception {
        if (actions != null) {
            actions.forEach((Action action) -> {
                if (action.getAction().equals(Action.Type.SELF_CONNECT)) {
                    clientSocketLogs.selfConnect();
                    if (ClientSocketLogs.session != null) {
                        ObjectNode objectNode = objectMapper.createObjectNode();
                        objectNode.put("node", clientSocketLogs.node());
                        objectNode.put("connected", ClientSocketLogs.session.isConnected());
                        if (ClientSocketLogs.session.isConnected()) {
                            objectNode.put("session_id", ClientSocketLogs.session.getSessionId());
                        }
                        action.setResult(objectNode);
                    } else {
                        action.setResult(false);
                    }
                }
                if (action.getAction().equals(Action.Type.NODES_CONNECT)) {
                    List<NodeSession> nodes = clientSocketLogs.nodesConnect();
                    ArrayNode arrayNode = objectMapper.createArrayNode();
                    nodes.forEach(node -> {
                        ObjectNode objectNode = objectMapper.createObjectNode();
                        objectNode.put("node", node.getNode());
                        if (node.getStompSession() != null) {
                            objectNode.put("connected", node.getStompSession().isConnected());
                            if (node.getStompSession().isConnected()) {
                                objectNode.put("session_id", node.getStompSession().getSessionId());
                            }
                        } else {
                            objectNode.put("connected", false);
                        }
                        arrayNode.add(objectNode);
                    });
                    action.setResult(arrayNode);
                }
            });
        }
        return actions;
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
