/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.model.bucket.client.services.util.DefaultRestTemplate;
import com.gs.baz.model.bucket.client.services.util.Relasionship;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author B73601
 */
public class SubServiceJsonConverter extends DefaultRestTemplate {

    public ResponseEntity<byte[]> convertFromTemplate() throws CustomException {
        try {
            responseBytes = restTemplate.exchange(Relasionship.JSON_CONVERT.url(basePath) + "/template/excel", HttpMethod.POST, httpEntity, byte[].class);
            if (responseBytes.getStatusCode().equals(HttpStatus.OK)) {
                return responseBytes;
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("JSON_CONVERTER response code error from service"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
    }

    public ResponseEntity<byte[]> convertFromArray() throws CustomException {
        try {
            responseBytes = restTemplate.exchange(Relasionship.JSON_CONVERT.url(basePath) + "/array/excel", HttpMethod.POST, httpEntity, byte[].class);
            if (responseBytes.getStatusCode().equals(HttpStatus.OK)) {
                return responseBytes;
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("JSON_CONVERTER response code error from service"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
    }
}
