/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos base de grafica", value = "GraficaBase")
public class GraficaBase {

    @JsonProperty(value = "clave")
    @ApiModelProperty(notes = "Clave de la grafica", example = "generico_odometro")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String clave;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la grafica", example = "Odometro")
    private String descripcion;

    @JsonProperty(value = "notas")
    @ApiModelProperty(notes = "Notas de la grafica", example = "La fuente de datos...")
    private String notas;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "GraficaBase{" + "clave=" + clave + ", descripcion=" + descripcion + ", notas=" + notas + '}';
    }

}
