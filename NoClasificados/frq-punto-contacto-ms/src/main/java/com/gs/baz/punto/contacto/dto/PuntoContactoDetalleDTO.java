/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoDetalleDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_punto_contacto")
    private Integer idPuntoContacto;

    @JsonProperty(value = "ceco")
    private Integer ceco;

    @JsonProperty(value = "referencia_ceco")
    private Integer referenciaCeco;

    @JsonProperty(value = "numero_economico")
    private Integer numeroEconomico;

    @JsonProperty(value = "id_estatus")
    private Integer idEstatus;

    @JsonProperty(value = "estatus")
    private String estatus;

    @JsonProperty(value = "id_territorio")
    private Integer idTerritorio;

    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonProperty(value = "id_zona")
    private Integer idZona;

    @JsonProperty(value = "zona")
    private String zona;

    @JsonProperty(value = "id_region")
    private Integer idRegion;

    @JsonProperty(value = "region")
    private String region;

    @JsonProperty(value = "numero_responsable")
    private String numeroResponsable;

    @JsonProperty(value = "responsable")
    private String responsable;

    @JsonProperty(value = "id_pais")
    private Integer idPais;

    @JsonProperty(value = "pais")
    private String pais;

    @JsonProperty(value = "id_estado")
    private Integer idEstado;

    @JsonProperty(value = "estado")
    private String estado;

    @JsonProperty(value = "municipio")
    private String municipio;

    @JsonProperty(value = "direccion")
    private String direccion;

    @JsonProperty(value = "fecha_carga")
    private String fechaCarga;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public PuntoContactoDetalleDTO() {
    }

    public Integer getIdPuntoContacto() {
        return idPuntoContacto;
    }

    public void setIdPuntoContacto(BigDecimal idPuntoContacto) {
        this.idPuntoContacto = (idPuntoContacto == null ? null : idPuntoContacto.intValue());
    }

    public Integer getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = new Integer(ceco);
    }

    public Integer getReferenciaCeco() {
        return referenciaCeco;
    }

    public void setReferenciaCeco(BigDecimal referenciaCeco) {
        this.referenciaCeco = (referenciaCeco == null ? null : referenciaCeco.intValue());
    }

    public Integer getNumeroEconomico() {
        return numeroEconomico;
    }

    public void setNumeroEconomico(BigDecimal numeroEconomico) {
        this.numeroEconomico = (numeroEconomico == null ? null : numeroEconomico.intValue());
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(BigDecimal idEstatus) {
        this.idEstatus = (idEstatus == null ? null : idEstatus.intValue());
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public Integer getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(BigDecimal idTerritorio) {
        this.idTerritorio = (idTerritorio == null ? null : idTerritorio.intValue());
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(BigDecimal idZona) {
        this.idZona = (idZona == null ? null : idZona.intValue());
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(BigDecimal idRegion) {
        this.idRegion = (idRegion == null ? null : idRegion.intValue());
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getNumeroResponsable() {
        return numeroResponsable;
    }

    public void setNumeroResponsable(String numeroResponsable) {
        this.numeroResponsable = numeroResponsable;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(BigDecimal idPais) {
        this.idPais = (idPais == null ? null : idPais.intValue());
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(BigDecimal idEstado) {
        this.idEstado = (idEstado == null ? null : idEstado.intValue());
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "PuntoContactoDetalleDTO{" + "idPuntoContacto=" + idPuntoContacto + ", ceco=" + ceco + ", referenciaCeco=" + referenciaCeco + ", numeroEconomico=" + numeroEconomico + ", idEstatus=" + idEstatus + ", estatus=" + estatus + ", idTerritorio=" + idTerritorio + ", territorio=" + territorio + ", idZona=" + idZona + ", zona=" + zona + ", idRegion=" + idRegion + ", region=" + region + ", numeroResponsable=" + numeroResponsable + ", responsable=" + responsable + ", idPais=" + idPais + ", pais=" + pais + ", idEstado=" + idEstado + ", estado=" + estado + ", municipio=" + municipio + ", direccion=" + direccion + ", fechaCarga=" + fechaCarga + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
