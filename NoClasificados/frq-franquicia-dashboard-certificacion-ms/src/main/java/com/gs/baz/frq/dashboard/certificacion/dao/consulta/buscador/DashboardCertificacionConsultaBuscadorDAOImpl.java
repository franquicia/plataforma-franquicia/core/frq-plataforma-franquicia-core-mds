package com.gs.baz.frq.dashboard.certificacion.dao.consulta.buscador;

import com.gs.baz.frq.dashboard.certificacion.dto.consulta.buscador.DashboardCertificacionConsultaBuscadorDTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.consulta.buscador.DashboardCertificacionGerenteConsultaBuscadorRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionConsultaBuscadorDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectGerente;
    private DefaultJdbcCall jdbcSelectCertificador;
    private DefaultJdbcCall jdbcSelectCeco;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectGerente = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGerente.withSchemaName(schema);
        jdbcSelectGerente.withCatalogName("PADASHCCBUSC");
        jdbcSelectGerente.withProcedureName("SPBUSXGERENTE");
        jdbcSelectGerente.returningResultSet("PA_CDATOS", new DashboardCertificacionGerenteConsultaBuscadorRowMapper());

        jdbcSelectCertificador = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCertificador.withSchemaName(schema);
        jdbcSelectCertificador.withCatalogName("PADASHCCBUSC");
        jdbcSelectCertificador.withProcedureName("SPBUSXCERTIF");
        jdbcSelectCertificador.returningResultSet("PA_CDATOS", new DashboardCertificacionGerenteConsultaBuscadorRowMapper().new DashboardCertificacionCertificadorConsultaBuscadorRowMapper());

        jdbcSelectCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCeco.withSchemaName(schema);
        jdbcSelectCeco.withCatalogName("PADASHCCBUSC");
        jdbcSelectCeco.withProcedureName("SPBUSXCECO");
        jdbcSelectCeco.returningResultSet("PA_CDATOS", new DashboardCertificacionGerenteConsultaBuscadorRowMapper().new DashboardCertificacionCecoConsultaBuscadorRowMapper());

    }

    public List<DashboardCertificacionConsultaBuscadorDTO> selectRowDashboardCertificacionGerenteConsultaBuscador(Long entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO);
        Map<String, Object> out = jdbcSelectGerente.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionConsultaBuscadorDTO>) out.get("PA_CDATOS");

    }

    public List<DashboardCertificacionConsultaBuscadorDTO> selectRowsDashboardCertificacionGerenteConsultaBuscador() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIGERENTEID", null);
        Map<String, Object> out = jdbcSelectGerente.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionConsultaBuscadorDTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionConsultaBuscadorDTO> selectRowDashboardCertificacionCertificadorConsultaBuscador(Long entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO);
        Map<String, Object> out = jdbcSelectCertificador.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionConsultaBuscadorDTO>) out.get("PA_CDATOS");

    }

    public List<DashboardCertificacionConsultaBuscadorDTO> selectRowsDashboardCertificacionCertificadorConsultaBuscador() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        Map<String, Object> out = jdbcSelectCertificador.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionConsultaBuscadorDTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionConsultaBuscadorDTO> selectRowDashboardCertificacionCecoConsultaBuscador(String entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO);
        Map<String, Object> out = jdbcSelectCeco.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionConsultaBuscadorDTO>) out.get("PA_CDATOS");

    }

    public List<DashboardCertificacionConsultaBuscadorDTO> selectRowsDashboardCertificacionCecoConsultaBuscador() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        Map<String, Object> out = jdbcSelectCeco.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionConsultaBuscadorDTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }
}
