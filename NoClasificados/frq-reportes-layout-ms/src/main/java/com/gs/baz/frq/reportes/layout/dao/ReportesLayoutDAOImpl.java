/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reportes.layout.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.reportes.layout.dto.ReporteExcel;
import com.gs.baz.frq.reportes.layout.dto.ConteoFechas;

import com.gs.baz.frq.reportes.layout.dto.ReporteCorreo;
import com.gs.baz.frq.reportes.layout.mprs.ConteoFechasRowMapper;
import com.gs.baz.frq.reportes.layout.mprs.ReporteExcelRowMapper;

import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class ReportesLayoutDAOImpl extends DefaultDAO {

    private static final String catalogoNegocioZonaFinal = "PA_SOPLAYOUT";

    private DefaultJdbcCall jdbcInsertNegocioZona;
    private DefaultJdbcCall jdbcUpdateNegocioZona;
    private DefaultJdbcCall jdbcSelectNegocioZona;
    private DefaultJdbcCall jdbcSelectReporteConteo;
    private DefaultJdbcCall jdbcSelectReporteCorreo;
    private DefaultJdbcCall jdbcDeleteNegocioZona;
    private DefaultJdbcCall jdbcSelectReporteExcelError;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogoNegocioZona = "PA_SOPLAYOUT";

        jdbcSelectReporteExcelError = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectReporteExcelError.withSchemaName(this.schema);
        jdbcSelectReporteExcelError.withCatalogName(catalogoNegocioZonaFinal);
        jdbcSelectReporteExcelError.withProcedureName("SP_SEL_PROYDETA");
        jdbcSelectReporteExcelError.returningResultSet("RCL_FORM", new ReporteExcelRowMapper());

        jdbcSelectReporteConteo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectReporteConteo.withSchemaName(schema);
        jdbcSelectReporteConteo.withCatalogName(catalogoNegocioZona);
        jdbcSelectReporteConteo.withProcedureName("SP_SEL_CONTEOP");
        jdbcSelectReporteConteo.returningResultSet("RCL_FORM", new ConteoFechasRowMapper());

        jdbcSelectReporteCorreo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectReporteCorreo.withSchemaName(schema);
        jdbcSelectReporteCorreo.withCatalogName(catalogoNegocioZona);
        jdbcSelectReporteCorreo.withProcedureName("SP_GET_NEGOZONA");
        jdbcSelectReporteCorreo.returningResultSet("RCL_FORM", new ReporteExcelRowMapper());

    }

    public List<ConteoFechas> selectConteoFechas(ConteoFechas entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());

        Map<String, Object> out = jdbcSelectReporteConteo.execute(mapSqlParameterSource);
        List<ConteoFechas> data = (List<ConteoFechas>) out.get("RCL_FORM");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteCorreo> selectAllRowsReporteCorreo(ReporteCorreo entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
        Map<String, Object> out = jdbcSelectReporteConteo.execute(mapSqlParameterSource);

        List<ReporteCorreo> data = (List<ReporteCorreo>) out.get("RCL_FORM");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }

    }

    public List<ReporteExcel> selectAllReporteExcelError(ReporteExcel entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

        Map<String, Object> out = jdbcSelectReporteExcelError.execute(mapSqlParameterSource);

        List<ReporteExcel> data = (List<ReporteExcel>) out.get("RCL_FORM");

        if (data.size() > 0) {

            return data;

        } else {

            return null;
        }

    }

    public List<ReporteExcel> selectAllReporteExcel(ReporteExcel entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

        Map<String, Object> out = generaInstanciaExcel().execute(mapSqlParameterSource);

        List<ReporteExcel> data = (List<ReporteExcel>) out.get("RCL_FORM");

        if (data.size() > 0) {

            return data;

        } else {

            return null;
        }

    }

    public DefaultJdbcCall generaInstanciaExcel() {

        DefaultJdbcCall jdbcSelectReporteExcel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectReporteExcel.withSchemaName(this.schema);
        jdbcSelectReporteExcel.withCatalogName(catalogoNegocioZonaFinal);
        jdbcSelectReporteExcel.withProcedureName("SP_SEL_PROYDETA");
        jdbcSelectReporteExcel.returningResultSet("RCL_FORM", new ReporteExcelRowMapper());

        return jdbcSelectReporteExcel;

    }

}
