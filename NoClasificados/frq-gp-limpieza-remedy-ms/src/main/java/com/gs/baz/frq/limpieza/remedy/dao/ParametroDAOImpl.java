package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.ParametroDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.ParametroRowMapper;
import com.gs.baz.frq.limpieza.remedy.util.UtilString;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ParametroDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcObtieneTodos;
    private DefaultJdbcCall jdbcObtieneParametro;
    private DefaultJdbcCall jdbcInsertaParametro;
    private DefaultJdbcCall jdbcActualizaParametro;
    private DefaultJdbcCall jdbcEliminaParametro;

    private static final Logger LOGGER = LogManager.getLogger();

    public ParametroDAOImpl() {
        super();

    }

    @SuppressWarnings("all")
    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_SEL_G_PARAM")
                .returningResultSet("RCL_PARAM", new ParametroRowMapper());

        jdbcObtieneParametro = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_PARAM", new ParametroRowMapper());

        jdbcInsertaParametro = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_INS_PARAM");

        jdbcActualizaParametro = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_ACT_PARAM");

        jdbcEliminaParametro = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPARAM")
                .withProcedureName("SP_DEL_PARAM");
    }

    @SuppressWarnings("all")
    public List<ParametroDTO> obtieneParametro() throws Exception {
        Map<String, Object> out = null;
        List<ParametroDTO> listaParametro = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        LOGGER.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_SEL_G_PARAM}");

        listaParametro = (List<ParametroDTO>) out.get("RCL_PARAM");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            LOGGER.info("Algo ocurrió al obtener los Parametros");
        } else {
            return listaParametro;
        }

        return listaParametro;

    }

    @SuppressWarnings("all")
    public List<ParametroDTO> obtieneParametro(String cve) throws Exception {
        Map<String, Object> out = null;
        List<ParametroDTO> listaParametro = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", cve);

        out = jdbcObtieneParametro.execute(in);

        LOGGER.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_SEL_PARAM}");

        listaParametro = (List<ParametroDTO>) out.get("RCL_PARAM");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            LOGGER.info("Algo ocurrió al obtener el Parametro con la cve (" + cve + ")");
        } else {
            return listaParametro;
        }

        return null;
    }

    @SuppressWarnings("all")
    public boolean insertaParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", bean.getCl())
                .addValue("PA_VAL_PARAM", bean.getValor())
                .addValue("PA_ACTIVO_PARAM", bean.getActivo());

        out = jdbcInsertaParametro.execute(in);

        LOGGER.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_INS_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            LOGGER.info("Algo ocurrió al insertar el Parametro");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("all")
    public boolean actualizaParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", bean.getCl())
                .addValue("PA_VAL_PARAM", bean.getValor())
                .addValue("PA_ACTIVO_PARAM", bean.getActivo());

        out = jdbcActualizaParametro.execute(in);

        LOGGER.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_ACT_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            String cve = UtilString.cleanParameter(bean.getCl())
                    .replace('\n', '_').replace('\r', '_');
            LOGGER.info("Algo ocurrió al actualizar el Parametro cve( " + cve + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("all")
    public boolean eliminaParametros(String cve) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", cve);

        out = jdbcEliminaParametro.execute(in);

        LOGGER.info("Funcion ejecutada: {FRANQUICIA.PAADMPARAM.SP_DEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            cve = UtilString.cleanParameter(cve)
                    .replace('\n', '_').replace('\r', '_');
            LOGGER.info("Algo ocurrió al borrar el Parametro cve(" + cve + ")");
        } else {
            return true;
        }

        return false;

    }
}
