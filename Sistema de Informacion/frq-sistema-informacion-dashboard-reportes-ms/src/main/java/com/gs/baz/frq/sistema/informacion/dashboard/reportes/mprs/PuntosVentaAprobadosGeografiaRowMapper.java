/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.PuntoVentaAprobadoGeografia;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PuntosVentaAprobadosGeografiaRowMapper implements RowMapper<PuntoVentaAprobadoGeografia> {

    @Override
    public PuntoVentaAprobadoGeografia mapRow(ResultSet rs, int rowNum) throws SQLException {
        PuntoVentaAprobadoGeografia puntoVentaAprobadoGeografia = new PuntoVentaAprobadoGeografia();
        puntoVentaAprobadoGeografia.setCentroCostos(rs.getInt("ID_CECO"));
        puntoVentaAprobadoGeografia.setDescripcion(rs.getString("DESCRIPCION_CECO"));
        puntoVentaAprobadoGeografia.setPorcentaje(rs.getInt("CALIFICACION"));
        puntoVentaAprobadoGeografia.setPuntosVenta(rs.getInt("TOTAL_PDV"));
        return puntoVentaAprobadoGeografia;
    }
}
