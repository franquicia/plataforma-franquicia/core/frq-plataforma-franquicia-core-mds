package com.gs.baz.frq.tipo.formato.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.tipo.formato.dao.util.GenericDAO;
import com.gs.baz.frq.tipo.formato.dto.TipoFormatoDTO;
import com.gs.baz.frq.tipo.formato.mprs.TipoFormatoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class TipoFormatoDAOImpl extends DefaultDAO implements GenericDAO<TipoFormatoDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectTipoFormatoFranquicia;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINTIPFORMATO");
        jdbcInsert.withProcedureName("SP_INS_TIPOFORM");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINTIPFORMATO");
        jdbcUpdate.withProcedureName("SP_ACT_TIPOFORM");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINTIPFORMATO");
        jdbcDelete.withProcedureName("SP_DEL_TIPOFORM");

        jdbcSelectTipoFormatoFranquicia = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTipoFormatoFranquicia.withSchemaName(schema);
        jdbcSelectTipoFormatoFranquicia.withCatalogName("PAADMINTIPFORMATO");
        jdbcSelectTipoFormatoFranquicia.withProcedureName("SP_SEL_TIPOFORM");
        jdbcSelectTipoFormatoFranquicia.returningResultSet("RCL_INFO", new TipoFormatoRowMapper());
    }

    @Override
    public TipoFormatoDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDTIFORM", entityID);
        Map<String, Object> out = jdbcSelectTipoFormatoFranquicia.execute(mapSqlParameterSource);
        List<TipoFormatoDTO> data = (List<TipoFormatoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<TipoFormatoDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDTIFORM", null);
        Map<String, Object> out = jdbcSelectTipoFormatoFranquicia.execute(mapSqlParameterSource);
        return (List<TipoFormatoDTO>) out.get("RCL_INFO");
    }

    @Override
    public TipoFormatoDTO insertRow(TipoFormatoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdTipoFormato(((BigDecimal) out.get("PA_FIIDTIFORM")));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Tipo Formato"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Tipo Formato"), ex);
        }
    }

    @Override
    public TipoFormatoDTO updateRow(TipoFormatoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDTIFORM", entityDTO.getIdTipoFormato());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Tipo Formato"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Tipo Formato"), ex);
        }
    }

    @Override
    public TipoFormatoDTO deleteRow(TipoFormatoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDTIFORM", entityDTO.getIdTipoFormato());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Tipo Formato"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Tipo Formato"), ex);
        }
    }

}
