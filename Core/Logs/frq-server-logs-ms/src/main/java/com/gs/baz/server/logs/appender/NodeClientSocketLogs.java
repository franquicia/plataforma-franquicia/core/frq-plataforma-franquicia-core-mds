/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.appender;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.server.logs.rest.web.socket.controller.Action;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

/**
 *
 * @author cescobarh
 */
public class NodeClientSocketLogs extends StompSessionHandlerAdapter {

    private final WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
    private List<Transport> transports;
    private final Logger logger = LogManager.getLogger();

    public void nodeConnect(NodeSession nodeSession) {
        String uri = "ws://" + nodeSession.getNode() + "/plataforma-franquicia-core/websocket/";
        if (nodeSession.getStompSession() == null) {
            transports = new ArrayList<>(1);
            transports.add(new WebSocketTransport(simpleWebSocketClient));
            SockJsClient sockJsClient = new SockJsClient(transports);
            WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
            stompClient.setMessageConverter(new MappingJackson2MessageConverter());
            try {
                nodeSession.setStompSession(stompClient.connect(uri, this).get());
            } catch (InterruptedException | ExecutionException ex) {
                System.out.println("Error");
            }
        }
    }

    private void showHeaders(StompHeaders headers) {
        logger.log(Level.DEBUG, "Headers");
        headers.entrySet().stream().map((e) -> {
            return e;
        }).map((e) -> {
            return e;
        }).forEach(System.out::println);
    }

    private void subscribeTopic(String topic, StompSession session) {
        logger.log(Level.DEBUG, "subscringTopic " + topic + " session " + session.getSessionId());
        session.subscribe(topic, this);
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("info", "welcome...!");
        session.send("/app/chat", objectNode);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ObjectNode.class;
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        logger.log(Level.DEBUG, "Connected with SessionId " + session.getSessionId());
        showHeaders(connectedHeaders);
        this.subscribeTopic("/topic/chat", session);
        List<Action> actions = new ArrayList<>();
        actions.add(new Action(Action.Type.SELF_CONNECT));
        session.send("/app/actions", actions);
    }

    @Override
    public void handleException(StompSession ss, StompCommand sc, StompHeaders sh, byte[] bytes, Throwable thrwbl) {
        logger.log(Level.ERROR, "handleException", thrwbl);
    }

    @Override
    public void handleTransportError(StompSession ss, Throwable thrwbl) {
        logger.log(Level.ERROR, "handleTransportError", thrwbl);
    }

    @Override
    public void handleFrame(StompHeaders sh, Object payload) {
        logger.log(Level.DEBUG, "payload " + payload + " from session " + ClientSocketLogs.session);
        if (ClientSocketLogs.session != null && ClientSocketLogs.session.isConnected()) {
            ClientSocketLogs.session.send("/app/chat", payload);
        }
    }
}
