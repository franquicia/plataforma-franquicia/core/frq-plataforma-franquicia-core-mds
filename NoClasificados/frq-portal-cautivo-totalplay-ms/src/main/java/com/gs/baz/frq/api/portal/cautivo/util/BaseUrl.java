/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.util;

/**
 *
 * @author MADA
 */
public enum BaseUrl {

    va2("http://200.52.135.170");

    private final String baseUrl;

    private BaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     *
     * @param path
     * @return
     */
    public String getBaseUrl() {
        return baseUrl;
    }

}
