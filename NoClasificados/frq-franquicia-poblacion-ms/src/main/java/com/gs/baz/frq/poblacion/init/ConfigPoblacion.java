package com.gs.baz.frq.poblacion.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.poblacion.dao.d001.PoblacionDAOImpl001;
import com.gs.baz.frq.poblacion.dao.d002.PoblacionDAOImpl002;
import com.gs.baz.frq.poblacion.dao.d003.PoblacionDAOImpl003;
import com.gs.baz.frq.poblacion.dao.d004.PoblacionDAOImpl004;
import com.gs.baz.frq.poblacion.dao.d005.PoblacionDAOImpl005;
import com.gs.baz.frq.poblacion.dao.d006.PoblacionDAOImpl006;
import com.gs.baz.frq.poblacion.dao.d007.PoblacionDAOImpl007;
import com.gs.baz.frq.poblacion.dao.d008.PoblacionDAOImpl008;
import com.gs.baz.frq.poblacion.dao.d009.PoblacionDAOImpl009;
import com.gs.baz.frq.poblacion.dao.d010.PoblacionDAOImpl010;
import com.gs.baz.frq.poblacion.dao.d011.PoblacionDAOImpl011;
import com.gs.baz.frq.poblacion.dao.d012.PoblacionDAOImpl012;
import com.gs.baz.frq.poblacion.dao.d013.PoblacionDAOImpl013;
import com.gs.baz.frq.poblacion.dao.d014.PoblacionDAOImpl014;
import com.gs.baz.frq.poblacion.dao.d015.PoblacionDAOImpl015;
import com.gs.baz.frq.poblacion.dao.d016.PoblacionDAOImpl016;
import com.gs.baz.frq.poblacion.dao.d017.PoblacionDAOImpl017;
import com.gs.baz.frq.poblacion.dao.d018.PoblacionDAOImpl018;
import com.gs.baz.frq.poblacion.dao.d019.PoblacionDAOImpl019;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.poblacion")
public class ConfigPoblacion {

    private final Logger logger = LogManager.getLogger();

    public ConfigPoblacion() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl")
    public PoblacionDAOImpl001 poblacionDAOImpl001() {
        return new PoblacionDAOImpl001();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl002")
    public PoblacionDAOImpl002 poblacionDAOImpl002() {
        return new PoblacionDAOImpl002();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl003")
    public PoblacionDAOImpl003 poblacionDAOImpl003() {
        return new PoblacionDAOImpl003();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl004")
    public PoblacionDAOImpl004 poblacionDAOImpl004() {
        return new PoblacionDAOImpl004();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl005")
    public PoblacionDAOImpl005 poblacionDAOImpl005() {
        return new PoblacionDAOImpl005();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl006")
    public PoblacionDAOImpl006 poblacionDAOImpl006() {
        return new PoblacionDAOImpl006();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl007")
    public PoblacionDAOImpl007 poblacionDAOImpl007() {
        return new PoblacionDAOImpl007();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl008")
    public PoblacionDAOImpl008 poblacionDAOImpl008() {
        return new PoblacionDAOImpl008();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl009")
    public PoblacionDAOImpl009 poblacionDAOImpl009() {
        return new PoblacionDAOImpl009();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl010")
    public PoblacionDAOImpl010 poblacionDAOImpl010() {
        return new PoblacionDAOImpl010();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl011")
    public PoblacionDAOImpl011 poblacionDAOImpl011() {
        return new PoblacionDAOImpl011();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl012")
    public PoblacionDAOImpl012 poblacionDAOImpl012() {
        return new PoblacionDAOImpl012();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl013")
    public PoblacionDAOImpl013 poblacionDAOImpl013() {
        return new PoblacionDAOImpl013();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl014")
    public PoblacionDAOImpl014 poblacionDAOImpl014() {
        return new PoblacionDAOImpl014();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl015")
    public PoblacionDAOImpl015 poblacionDAOImpl015() {
        return new PoblacionDAOImpl015();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl016")
    public PoblacionDAOImpl016 poblacionDAOImpl016() {
        return new PoblacionDAOImpl016();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl017")
    public PoblacionDAOImpl017 poblacionDAOImpl017() {
        return new PoblacionDAOImpl017();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl018")
    public PoblacionDAOImpl018 poblacionDAOImpl018() {
        return new PoblacionDAOImpl018();
    }

    @Bean(initMethod = "init", name = "frqPoblacionDAOImpl019")
    public PoblacionDAOImpl019 poblacionDAOImpl019() {
        return new PoblacionDAOImpl019();
    }

}
