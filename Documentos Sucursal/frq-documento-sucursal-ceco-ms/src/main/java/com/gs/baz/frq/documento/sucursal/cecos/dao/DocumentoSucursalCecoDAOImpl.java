/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.documento.sucursal.cecos.dto.Ceco;
import com.gs.baz.frq.documento.sucursal.cecos.dto.Geografia;
import com.gs.baz.frq.documento.sucursal.cecos.mprs.DocumentoSucursalCecoRowMapper;
import com.gs.baz.frq.documento.sucursal.cecos.mprs.DocumentoSucursalGeografiaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalCecoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectAllGeografia;
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PA_ADNEGO";
        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_NEGOCEC");
        jdbcSelectAll.returningResultSet("RCL_NEGO", new DocumentoSucursalCecoRowMapper());

        jdbcSelectAllGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAllGeografia.withSchemaName(schema);
        jdbcSelectAllGeografia.withCatalogName("PA_ADFICHA");
        jdbcSelectAllGeografia.withProcedureName("SP_SEL_CECOFI");
        jdbcSelectAllGeografia.returningResultSet("RCL_FICHA", new DocumentoSucursalGeografiaRowMapper());

    }

    public List<Ceco> selectAllRows(Integer idNegocio, String ceco) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDNEGO", idNegocio);
        mapSqlParameterSource.addValue("PA_CECO", ceco);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Ceco>) out.get("RCL_NEGO");
    }

    public List<Geografia> selectAllRowsGeografia(String ceco)
            throws CustomException, DataNotFoundException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", ceco);
        Map<String, Object> out = jdbcSelectAllGeografia.execute(mapSqlParameterSource);

        List<Geografia> data = (List<Geografia>) out.get("RCL_FICHA");

        if (data != null && !data.isEmpty()) {
            return data;
        } else {
            throw new DataNotFoundException();
        }

    }
}
