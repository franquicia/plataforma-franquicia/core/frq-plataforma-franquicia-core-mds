/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.rutas.dto.ModeloNegocio;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaCompuesta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutasCompuesta;
import com.gs.baz.frq.modelo.negocio.rutas.mprs.RutasCompuestaNivelRowMapper;
import com.gs.baz.frq.modelo.negocio.rutas.mprs.RutasCompuestaRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class RutaCompuestaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectRutasCompuesta;
    private DefaultJdbcCall jdbcSelectRutasCompuestaNivel;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAMNCONULTDOCTO";
        jdbcSelectRutasCompuesta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectRutasCompuesta.withSchemaName(schema);
        jdbcSelectRutasCompuesta.withCatalogName(catalogo);
        jdbcSelectRutasCompuesta.withProcedureName("SPRECUPERAFOLIORUTAS");
        jdbcSelectRutasCompuesta.returningResultSet("PA_CDATOS", new RutasCompuestaRowMapper());

        jdbcSelectRutasCompuestaNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectRutasCompuestaNivel.withSchemaName(schema);
        jdbcSelectRutasCompuestaNivel.withCatalogName(catalogo);
        jdbcSelectRutasCompuestaNivel.withProcedureName("SPRECUPERAFOLIORUTASNIVELES");
        jdbcSelectRutasCompuestaNivel.returningResultSet("PA_CDATOS", new RutasCompuestaNivelRowMapper());

    }

    public RutasCompuesta selectRutasCompuesta(Long idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", idFolio);
        Map<String, Object> out = jdbcSelectRutasCompuesta.execute(mapSqlParameterSource);
        List<RutaCompuesta> rutasCompuestas = (List<RutaCompuesta>) out.get("PA_CDATOS");
        for (RutaCompuesta rutaCompuesta : rutasCompuestas) {
            rutaCompuesta.setModelosNegocio(this.selectRutasCompuestaModelosNegocio(rutaCompuesta.getIdRuta()));
        }
        return new RutasCompuesta(rutasCompuestas);
    }

    private List<ModeloNegocio> selectRutasCompuestaModelosNegocio(Integer idRuta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_RUTA", idRuta);
        Map<String, Object> out = jdbcSelectRutasCompuestaNivel.execute(mapSqlParameterSource);
        return (List<ModeloNegocio>) out.get("PA_CDATOS");
    }

}
