/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.acceso.client.FolioClient;
import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Folio;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.AltaDocumentoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.AltaGrupoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.DocumentoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.GrupoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.ParametrosGrupoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.Usuario;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.ParametrosDocumentoUsuario;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.DocumentoUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.GrupoUsuarioRowMapper;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.UsuarioRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class UsuarioDAOImpl extends DefaultDAO {

    @Autowired
    private FolioClient folioClient;

    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectAllGrupos;
    private DefaultJdbcCall jdbcSelectAllGruposDisponibles;
    private DefaultJdbcCall jdbcInsertGrupo;
    private DefaultJdbcCall jdbcDeleteGrupo;
    private DefaultJdbcCall jdbcSelectAllDocumentos;
    private DefaultJdbcCall jdbcSelectAllDocumentosDisponibles;
    private DefaultJdbcCall jdbcInsertDocumento;
    private DefaultJdbcCall jdbcDeleteDocumento;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private final String schema = "MODFRANQ";

    public void init() {
        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAMNCONULTASEXT");
        jdbcSelectAll.withProcedureName("SPUSERCONSULTOR");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new UsuarioRowMapper());

        jdbcInsertGrupo = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertGrupo.withSchemaName(schema);
        jdbcInsertGrupo.withCatalogName("PAADMDMNGPOUSU");
        jdbcInsertGrupo.withProcedureName("SPINSDMNGPOUSUA");

        jdbcSelectAllGrupos = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllGrupos.withSchemaName(schema);
        jdbcSelectAllGrupos.withCatalogName("PAADMDMNGPOUSU");
        jdbcSelectAllGrupos.withProcedureName("SPGETDGPOXUSUA");
        jdbcSelectAllGrupos.returningResultSet("PA_CDATOS", new GrupoUsuarioRowMapper());

        jdbcSelectAllGruposDisponibles = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllGruposDisponibles.withSchemaName(schema);
        jdbcSelectAllGruposDisponibles.withCatalogName("PAADMDMNGPOUSU");
        jdbcSelectAllGruposDisponibles.withProcedureName("SPGETGRUPOSXUSUADISP");
        jdbcSelectAllGruposDisponibles.returningResultSet("PA_CDATOS", new GrupoUsuarioRowMapper());

        jdbcDeleteGrupo = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteGrupo.withSchemaName(schema);
        jdbcDeleteGrupo.withCatalogName("PAADMDMNGPOUSU");
        jdbcDeleteGrupo.withProcedureName("SPDELDMNGPOUSUA");

        jdbcSelectAllDocumentos = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDocumentos.withSchemaName(schema);
        jdbcSelectAllDocumentos.withCatalogName("PAADMDMNUSUDOC");
        jdbcSelectAllDocumentos.withProcedureName("SPGETDMNUSUDOCTO");
        jdbcSelectAllDocumentos.returningResultSet("PA_CDATOS", new DocumentoUsuarioRowMapper());

        jdbcSelectAllDocumentosDisponibles = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDocumentosDisponibles.withSchemaName(schema);
        jdbcSelectAllDocumentosDisponibles.withCatalogName("PAADMDMNUSUDOC");
        jdbcSelectAllDocumentosDisponibles.withProcedureName("SPGETDOCXUSUADIS");
        jdbcSelectAllDocumentosDisponibles.returningResultSet("PA_CDATOS", new DocumentoUsuarioRowMapper());

        jdbcInsertDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertDocumento.withSchemaName(schema);
        jdbcInsertDocumento.withCatalogName("PAADMDMNUSUDOC");
        jdbcInsertDocumento.withProcedureName("SPINSDMNUSUDOCTO");

        jdbcDeleteDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteDocumento.withSchemaName(schema);
        jdbcDeleteDocumento.withCatalogName("PAADMDMNUSUDOC");
        jdbcDeleteDocumento.withProcedureName("SPDELDMNUSUDOCTO");
    }

    public List<Usuario> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public AltaGrupoUsuario insertGrupoUsuario(ParametrosGrupoUsuario entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRUPO", entityDTO.getIdGrupo());
            mapSqlParameterSource.addValue("PA_FIIDUSUARIO", entityDTO.getNumeroEmpleado());
            Map<String, Object> out = jdbcInsertGrupo.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDGPOUSUA");
                return new AltaGrupoUsuario(dato.intValue());
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
    }

    public List<GrupoUsuario> selectAllGrupos(Integer numeroEmpleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllGrupos.execute(mapSqlParameterSource);
        return (List<GrupoUsuario>) out.get("PA_CDATOS");
    }

    public List<GrupoUsuario> selectAllGruposDisponibles(Integer numeroEmpleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUARIO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllGruposDisponibles.execute(mapSqlParameterSource);
        return (List<GrupoUsuario>) out.get("PA_CDATOS");
    }

    public void deleteGrupo(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGPOUSUA", entityDTO);
            Map<String, Object> out = jdbcDeleteGrupo.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<DocumentoUsuario> selectAllDocumentos(HttpHeaders headers, Integer numeroEmpleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUDOCTO", null);
        mapSqlParameterSource.addValue("PA_FINUMEROEMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllDocumentos.execute(mapSqlParameterSource);
        List<DocumentoUsuario> list = (List<DocumentoUsuario>) out.get("PA_CDATOS");
        for (DocumentoUsuario documento : list) {
            try {
                Folio folio = folioClient.getFolio(headers, documento.getFolio().getIdFolio());
                documento.setFolio(folio);
            } catch (Exception ex) {
                throw new CustomException(ex);
            }
        }
        return list;
    }

    public List<DocumentoUsuario> selectAllDocumentosDisponibles(HttpHeaders headers, Integer numeroEmpleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUDOCTO", null);
        mapSqlParameterSource.addValue("PA_FINUMEROEMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllDocumentosDisponibles.execute(mapSqlParameterSource);
        List<DocumentoUsuario> list = (List<DocumentoUsuario>) out.get("PA_CDATOS");
        for (DocumentoUsuario documento : list) {
            try {
                Folio folio = folioClient.getFolio(headers, documento.getFolio().getIdFolio());
                documento.setFolio(folio);
            } catch (Exception ex) {
                throw new CustomException(ex);
            }
        }
        return list;
    }

    public AltaDocumentoUsuario insertDocumentoUsuario(ParametrosDocumentoUsuario entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FINUMEROEMPLEADO", entityDTO.getNumeroEmpleado());
            mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", entityDTO.getIdDocumento());
            Map<String, Object> out = jdbcInsertDocumento.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDUSUDOCTO");
                return new AltaDocumentoUsuario(dato.intValue());
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
    }

    public void deleteDocumento(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDUSUDOCTO", entityDTO);
            Map<String, Object> out = jdbcDeleteDocumento.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
