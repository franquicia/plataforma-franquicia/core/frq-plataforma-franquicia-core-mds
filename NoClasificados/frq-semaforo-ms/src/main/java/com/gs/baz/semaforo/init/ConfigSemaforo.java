package com.gs.baz.semaforo.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.semaforo.dao.SemaforoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.semaforo")
public class ConfigSemaforo {

    private final Logger logger = LogManager.getLogger();

    public ConfigSemaforo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SemaforoDAOImpl semaforoDAOImpl() {
        return new SemaforoDAOImpl();
    }
}
