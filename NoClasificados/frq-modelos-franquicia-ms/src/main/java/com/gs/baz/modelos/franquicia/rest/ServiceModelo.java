/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.modelos.franquicia.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.UsuarioDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceUsuario;
import com.gs.baz.modelos.franquicia.dao.ModeloFranquiciaDAOImpl;
import com.gs.baz.modelos.franquicia.dto.ModeloFranquiciaDTO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/modelo")
public class ServiceModelo {

    @Autowired
    private ModeloFranquiciaDAOImpl modeloFranquiciaDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceUsuario subServiceUsuario;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/{id_modelo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModeloFranquiciaDTO getModelo(@RequestHeader HttpHeaders headers, @PathVariable("id_modelo") Long idModelo) throws CustomException {
        final ModeloFranquiciaDTO modeloFranquiciasDTO;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            modeloFranquiciasDTO = modeloFranquiciaDAOImpl.selectRow(idModelo);
            if (modeloFranquiciasDTO != null) {
                subServiceUsuario.init(basePath, httpEntity);
                this.setEmpleado(modeloFranquiciasDTO);
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return modeloFranquiciasDTO;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModeloFranquiciaDTO> getModelos(@RequestHeader HttpHeaders headers) throws CustomException {
        final List<ModeloFranquiciaDTO> modeloFranquiciasDTO;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            modeloFranquiciasDTO = modeloFranquiciaDAOImpl.selectRows();
            if (modeloFranquiciasDTO != null) {
                subServiceUsuario.init(basePath, httpEntity);
                for (ModeloFranquiciaDTO franquicia : modeloFranquiciasDTO) {
                    this.setEmpleado(franquicia);
                }
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return modeloFranquiciasDTO;
    }

    private void setEmpleado(ModeloFranquiciaDTO modeloFranquiciasDTO) throws CustomException {
        if (modeloFranquiciasDTO.getIdEmpleadoCrea() != null) {
            UsuarioDTO usuarioCrea = subServiceUsuario.getUsuario(modeloFranquiciasDTO.getIdEmpleadoCrea());
            modeloFranquiciasDTO.setEmpleadoCrea(usuarioCrea);
            modeloFranquiciasDTO.setIdEmpleadoCrea(null);
        }
        if (modeloFranquiciasDTO.getIdEmpleadoModifica() != null) {
            UsuarioDTO usuarioModifica = subServiceUsuario.getUsuario(modeloFranquiciasDTO.getIdEmpleadoModifica());
            modeloFranquiciasDTO.setEmpleadoModificacion(usuarioModifica);
            modeloFranquiciasDTO.setIdEmpleadoModifica(null);
        }
    }

    /**
     *
     * @param headers
     * @param value
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModeloFranquiciaDTO> getModeloByNombre(@RequestHeader HttpHeaders headers, @QueryParam("value") String value, Long idFranquicia) throws CustomException {
        return modeloFranquiciaDAOImpl.selectRowsByName(value, idFranquicia);
    }

}
