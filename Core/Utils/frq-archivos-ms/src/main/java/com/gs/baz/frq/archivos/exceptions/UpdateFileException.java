/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class UpdateFileException extends Exception{

    public UpdateFileException() {
    }

    public UpdateFileException(String string) {
        super(string);
    }

    public UpdateFileException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public UpdateFileException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
