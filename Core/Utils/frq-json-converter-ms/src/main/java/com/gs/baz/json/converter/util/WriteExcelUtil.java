/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.json.converter.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.json.converter.excel.dto.ExcelFileDTO;
import com.gs.baz.json.converter.excel.dto.MetadataColumnDTO;
import com.gs.baz.json.converter.excel.dto.MetadataDTO;
import com.gs.baz.json.converter.excel.dto.MetadataSheetDTO;
import com.gs.baz.json.converter.excel.dto.SheetDTO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StopWatch;

/**
 *
 * @author cescobarh
 */
public class WriteExcelUtil {

    private int indexColumn;
    private Sheet sheet;
    private Row row;
    private Cell cell;
    private SheetDTO sheetDTO;
    private ExcelFileDTO excelFileDTO;
    private JsonNode jsonNodeRows;
    private final Logger logger = LogManager.getLogger();
    private StopWatch stopWatch;

    private MetadataDTO prepareMetadata(ExcelFileDTO excelFileDTO) {
        MetadataDTO metadataDTO = excelFileDTO.getMetadata();
        if (metadataDTO == null) {
            metadataDTO = new MetadataDTO();
            List<SheetDTO> sheets = excelFileDTO.getSheets();
            if (sheets != null) {
                int indexSheet = 0;
                for (SheetDTO itemSheet : sheets) {
                    indexSheet++;
                    MetadataSheetDTO metadataSheetDTO;
                    if (itemSheet.getDescription() == null) {
                        metadataSheetDTO = new MetadataSheetDTO("Hoja" + indexSheet);
                    } else {
                        metadataSheetDTO = new MetadataSheetDTO(itemSheet.getDescription());
                    }
                    List<ObjectNode> rows = itemSheet.getRows();
                    if (rows != null && rows.size() > 0) {
                        ObjectNode firstRow = rows.get(0);
                        firstRow.fieldNames().forEachRemaining(column -> {
                            metadataSheetDTO.addColumn(new MetadataColumnDTO(column));
                        });
                    }
                    metadataDTO.addSheet(metadataSheetDTO);
                }
            }
        }
        return metadataDTO;
    }

    public Resource writeExcel(JsonNode jsonNode) throws CustomException {
        excelFileDTO = new ExcelFileDTO();
        jsonNode.fields().forEachRemaining(item -> {
            if (jsonNode.get(item.getKey()).isArray()) {
                sheetDTO = new SheetDTO(item.getKey());
                jsonNodeRows = item.getValue();
                if (jsonNodeRows != null && jsonNodeRows.size() > 0) {
                    jsonNodeRows.forEach(itemRow -> {
                        sheetDTO.addRow((ObjectNode) itemRow);
                    });
                }
                excelFileDTO.addSheet(sheetDTO);
            }
        });
        return this.processWriteExcel(excelFileDTO);
    }

    public Resource writeExcel(ExcelFileDTO excelFileDTO) throws CustomException {
        return this.processWriteExcel(excelFileDTO);
    }

    private Resource processWriteExcel(ExcelFileDTO excelFileDTO) throws CustomException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (Workbook workbook = new XSSFWorkbook()) {
            stopWatch = new StopWatch("WriteExcelUtil");
            if (stopWatch.isRunning()) {
                stopWatch.stop();
            }
            stopWatch.start("Preparando Hojas y Columnas Iniciando");
            MetadataDTO metadataDTO = this.prepareMetadata(excelFileDTO);
            List<MetadataSheetDTO> metadataSheets = metadataDTO.getSheets();
            if (metadataSheets == null) {
                List<MetadataSheetDTO> metadataSheetsDefault = new ArrayList<>();
                metadataSheetsDefault.add(new MetadataSheetDTO("Hoja 1"));
                metadataDTO.setSheets(metadataSheetsDefault);
                metadataSheets = metadataSheetsDefault;
            }
            metadataDTO.getSheets().forEach(itemSheet -> {
                sheet = workbook.createSheet(itemSheet.getDescription());
                List<MetadataColumnDTO> metadataColumns = itemSheet.getColumns();
                if (metadataColumns != null && metadataColumns.size() > 0) {
                    indexColumn = 0;
                    row = sheet.createRow(0);
                    metadataColumns.forEach(itemColumn -> {
                        cell = row.createCell(indexColumn);
                        cell.setCellValue(itemColumn.getDescription());
                        indexColumn++;
                    });
                }
            });
            stopWatch.stop();
            List<SheetDTO> sheets = excelFileDTO.getSheets();
            if (sheets != null && sheets.size() > 0) {
                int indexSheet = 0;
                for (SheetDTO itemSheet : sheets) {
                    stopWatch.start("Llenado de datos en Hoja " + indexSheet);
                    sheet = workbook.getSheetAt(indexSheet);
                    List<ObjectNode> rows = itemSheet.getRows();
                    if (rows != null && rows.size() > 0) {
                        int indexRow = 1;
                        for (ObjectNode itemRow : rows) {
                            row = sheet.createRow(indexRow);
                            indexColumn = 0;
                            itemRow.fieldNames().forEachRemaining(itemPropertie -> {
                                cell = row.createCell(indexColumn);
                                cell.setCellValue(itemRow.get(itemPropertie).asText());
                                indexColumn++;
                            });
                            indexRow++;
                        }
                    }
                    indexSheet++;
                    stopWatch.stop();
                }
            }
            workbook.write(outputStream);
            outputStream.close();
        } catch (IOException ex) {
            throw new CustomException(ex);
        }
        stopWatch.start("Conversion");
        Resource resource = new ByteArrayResource(outputStream.toByteArray());
        stopWatch.stop();
        printLogInfo();
        return resource;
    }

    private void printLogInfo() {
        StringBuilder infoTask = new StringBuilder();
        infoTask.append("\nStopWatch '").append(stopWatch.getId()).append("': running time = ").append(stopWatch.getTotalTimeSeconds()).append(" seconds \n");
        infoTask.append("---------------------------------\n");
        infoTask.append("  Seconds    |     Task Name     \n");
        infoTask.append("---------------------------------\n");
        for (StopWatch.TaskInfo itemTask : stopWatch.getTaskInfo()) {
            infoTask.append(itemTask.getTimeSeconds()).append("  |  ").append(itemTask.getTaskName()).append("\n");
        }
        infoTask.append("---------------------------------\n");
        logger.log(Level.INFO, infoTask);
    }
}
