/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.dto;

import com.gs.baz.frq.jobs.scheduler.util.CustomDateDeserializer;
import com.gs.baz.frq.jobs.scheduler.util.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author cescobarh
 */
public class Execution implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "last_execution_succcess")
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date lastExecutionSucccess;

    @JsonProperty(value = "last_execution_succcess_result")
    private String lastExecutionSucccessResult;

    @JsonProperty(value = "last_execution_error")
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date lastExecutionError;

    @JsonProperty(value = "last_execution_error_result")
    private String lastExecutionErrorResult;

    @JsonProperty(value = "healthy_status")
    private Integer healthyStatus;

    @JsonProperty(value = "last_duration")
    private String lastDuration;

    public Execution() {
    }

    public Execution(Date lastExecutionSucccess, String lastExecutionSucccessResult, Date lastExecutionError, String lastExecutionErrorResult) {
        this.lastExecutionSucccess = lastExecutionSucccess;
        this.lastExecutionSucccessResult = lastExecutionSucccessResult;
        this.lastExecutionError = lastExecutionError;
        this.lastExecutionErrorResult = lastExecutionErrorResult;
    }

    public Date getLastExecutionSucccess() {
        return lastExecutionSucccess;
    }

    public void setLastExecutionSucccess(Date lastExecutionSucccess) {
        this.lastExecutionSucccess = lastExecutionSucccess;
    }

    public String getLastExecutionSucccessResult() {
        return lastExecutionSucccessResult;
    }

    public void setLastExecutionSucccessResult(String lastExecutionSucccessResult) {
        this.lastExecutionSucccessResult = lastExecutionSucccessResult;
    }

    public Date getLastExecutionError() {
        return lastExecutionError;
    }

    public void setLastExecutionError(Date lastExecutionError) {
        this.lastExecutionError = lastExecutionError;
    }

    public String getLastExecutionErrorResult() {
        return lastExecutionErrorResult;
    }

    public void setLastExecutionErrorResult(String lastExecutionErrorResult) {
        this.lastExecutionErrorResult = lastExecutionErrorResult;
    }

    public Integer getHealthyStatus() {
        if (lastExecutionError == null) {
            return 1;
        }
        if (lastExecutionSucccess == null) {
            return 0;
        }
        if (lastExecutionSucccess.after(lastExecutionError)) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setHealthyStatus(Integer healthyStatus) {
        this.healthyStatus = this.getHealthyStatus();
    }

    public String getLastDuration() {
        return lastDuration;
    }

    public void setLastDuration(String lastDuration) {
        this.lastDuration = lastDuration;
    }

    @Override
    public String toString() {
        return "Execution{" + "lastExecutionSucccess=" + lastExecutionSucccess + ", lastExecutionSucccessResult=" + lastExecutionSucccessResult + ", lastExecutionError=" + lastExecutionError + ", lastExecutionErrorResult=" + lastExecutionErrorResult + ", healthyStatus=" + healthyStatus + ", lastDuration=" + lastDuration + '}';
    }

}
