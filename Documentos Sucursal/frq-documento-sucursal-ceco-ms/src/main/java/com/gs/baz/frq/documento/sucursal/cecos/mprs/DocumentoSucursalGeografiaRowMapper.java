/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.mprs;

import com.gs.baz.frq.documento.sucursal.cecos.dto.Ficha;
import com.gs.baz.frq.documento.sucursal.cecos.dto.Geografia;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalGeografiaRowMapper implements RowMapper<Geografia> {

    private Geografia ceco;

    @Override
    public Geografia mapRow(ResultSet rs, int rowNum) throws SQLException {
        ceco = new Geografia();
        ceco.setIdCeco(rs.getInt("FCID_CECO"));
        ceco.setIdRegion(rs.getInt("FCID_REGION"));
        ceco.setRegion(rs.getString("FCREGION"));
        ceco.setIdZona(rs.getInt("FCID_ZONA"));
        ceco.setZona(rs.getString("FCZONA"));
        ceco.setIdTerritorio(rs.getInt("FCID_TERRITORIO"));
        ceco.setTerritorio(rs.getString("FCTERRITORIO"));

        Ficha datosFicha = new Ficha();
        datosFicha.setId(rs.getInt("FIID_FICHA"));
        datosFicha.setIdFormato(rs.getInt("FIID_FORMATO"));
        datosFicha.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        datosFicha.setIdFolio(rs.getInt("FIID_FOLIO"));
        datosFicha.setLatitud(rs.getString("FCLATITUD"));
        datosFicha.setLongitud(rs.getString("FCLONGITUD"));
        datosFicha.setDireccion(rs.getString("FCDIRECCION"));

        ceco.setDatosFicha(datosFicha);

        return ceco;
    }
}
