/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.init;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.autorization.server.apigee.filters.ValidationTokenFilter;
import com.gs.baz.autorization.server.token.ObjectToUrlEncodedConverter;
import com.gs.baz.usuario.expose.EnableUsuario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.autorization.server")
@Import(value = {ConfigAuthorizationServerAdapter.class, ConfigSpringSecurityAdapter.class})
@EnableUsuario
public class ConfigAuthorizationServer {

    private final Logger logger = LogManager.getLogger();

    public ConfigAuthorizationServer() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    /**
     *
     * @return @throws IllegalArgumentException
     * @throws NamingException
     */
    /**
     *
     * @param mdfqrDS
     * @return
     */
    @Bean(name = "restTemplateToken")
    public RestTemplate restTemplateToken() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new ObjectToUrlEncodedConverter(new ObjectMapper()));
        return restTemplate;
    }

    @Bean
    public FilterRegistrationBean<ValidationTokenFilter> filterRegistrationBeanValidationTokenFilter() {
        FilterRegistrationBean<ValidationTokenFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new ValidationTokenFilter());
        registrationBean.addUrlPatterns("/api/*");
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE - 15);
        return registrationBean;
    }
}
