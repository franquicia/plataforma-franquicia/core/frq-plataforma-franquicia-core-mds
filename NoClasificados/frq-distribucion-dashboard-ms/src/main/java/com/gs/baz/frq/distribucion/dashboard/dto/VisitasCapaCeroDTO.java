package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Visitas de Capa Cero", value = "VisitasCapaCero")
public class VisitasCapaCeroDTO {

    @JsonProperty(value = "estatusVisitas")
    @ApiModelProperty(notes = "Indicador de la distribución de las visitas.", example = "1")
    private Integer estatus;

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return "VisitasCapaCeroDTO{" + "estatus=" + estatus + '}';
    }

}
