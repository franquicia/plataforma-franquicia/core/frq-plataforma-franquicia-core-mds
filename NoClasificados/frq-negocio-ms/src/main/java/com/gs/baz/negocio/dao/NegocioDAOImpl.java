package com.gs.baz.negocio.dao;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.categoria.dao.CategoriaDAOImpl;
import com.gs.baz.categoria.dto.CategoriaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.negocio.dao.util.GenericDAO;
import com.gs.baz.negocio.dto.NegocioDTO;
import com.gs.baz.negocio.mprs.NegocioFranquiciasRowMapper;
import com.gs.baz.negocio.mprs.NegocioRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cescobarh
 */
@Component
public class NegocioDAOImpl extends DefaultDAO implements GenericDAO<NegocioDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectIdFranquicia;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcSelectNegocioFranquicias;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;
    private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    @Autowired
    private CategoriaDAOImpl categoriaDAOImpl;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINNEGOCIO");
        jdbcInsert.withProcedureName("SP_INS_NEGOCIO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINNEGOCIO");
        jdbcUpdate.withProcedureName("SP_ACT_NEGOCIO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINNEGOCIO");
        jdbcSelect.withProcedureName("SP_SEL_NEGOCIOS");
        jdbcSelect.returningResultSet("RCL_INFO", new NegocioRowMapper());

        jdbcSelectIdFranquicia = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectIdFranquicia.withSchemaName(schema);
        jdbcSelectIdFranquicia.withCatalogName("PAADMINNEGOCIO");
        jdbcSelectIdFranquicia.withProcedureName("SP_SEL_NEGXFRAQ");
        jdbcSelectIdFranquicia.returningResultSet("RCL_INFO", new NegocioRowMapper());

        jdbcSelectNegocioFranquicias = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectNegocioFranquicias.withSchemaName(schema);
        jdbcSelectNegocioFranquicias.withCatalogName("PAADMINNEGOCIO");
        jdbcSelectNegocioFranquicias.withProcedureName("SP_SEL_NEGYFRAQ");
        jdbcSelectNegocioFranquicias.returningResultSet("RCL_INFO", new NegocioFranquiciasRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINNEGOCIO");
        jdbcDelete.withProcedureName("SP_DEL_NEGOCIO");

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMINNEGOCIO");
        jdbcSelectByName.withProcedureName("SP_NEGOCIO_NOMBRE");
        jdbcSelectByName.returningResultSet("RCL_INFO", new NegocioRowMapper());

    }

    @Override
    public NegocioDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNEGO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            data.get(0).setIdsFranquicias(this.selectRowsFranquicias(entityID.intValue()));
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<NegocioDTO> selectRowsIdFranquicia(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityID);
        Map<String, Object> out = jdbcSelectIdFranquicia.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public NegocioDTO selectRowLogo(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNEGO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<NegocioDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNEGO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            data.stream().forEach((negocioDTO) -> {
                negocioDTO.setIdsFranquicias(this.selectRowsFranquicias(negocioDTO.getIdNegocio()));
            });
            return data;
        } else {
            return null;
        }
    }

    private List<Integer> selectRowsFranquicias(Integer entityID) {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNEGO", entityID);
        Map<String, Object> out = jdbcSelectNegocioFranquicias.execute(mapSqlParameterSource);
        return (List<Integer>) out.get("RCL_INFO");
    }

    @Override
    @Transactional(rollbackFor = {CustomException.class})
    public NegocioDTO cudRow(NegocioDTO entityDTO) throws CustomException {
        try {
            if (entityDTO.getInserted() != null && entityDTO.getInserted() && entityDTO.getIdNegocio() == null) {
                //Se agrega negocio GEK
                List<NegocioDTO> existente = selectRowsByName("GEK");
                if (existente == null || existente.isEmpty()) {
                    NegocioDTO gek = new NegocioDTO();
                    gek.setDescripcion("GEK");
                    gek.setStatus(BigDecimal.ONE);
                    gek.setPathLogo("Sinlogo");
                    gek = this.insertRow(gek);
                }

                entityDTO = this.insertRow(entityDTO);
            }
            if (entityDTO.getUpdated() != null && entityDTO.getUpdated()) {
                entityDTO = this.updateRow(entityDTO);
            }
            if (entityDTO.getDeleted() != null && entityDTO.getDeleted()) {
                entityDTO = this.deleteRow(entityDTO);
            }
            if (entityDTO.getCategorias() != null) {
                List<CategoriaDTO> categoriasNegocio = objectMapper.convertValue(entityDTO.getCategorias(), objectMapper.getTypeFactory().constructCollectionType(List.class, CategoriaDTO.class));
                if (categoriasNegocio != null) {
                    for (CategoriaDTO categoriaDTO : categoriasNegocio) {
                        if (categoriaDTO.getInserted() != null && categoriaDTO.getInserted() && categoriaDTO.getIdCategoria() == null) {
                            categoriaDTO.setIdNegocio(new BigDecimal(entityDTO.getIdNegocio()));
                            categoriaDTO = categoriaDAOImpl.cudRow(categoriaDTO);
                        }
                        if (categoriaDTO.getUpdated() != null && categoriaDTO.getUpdated()) {
                            categoriaDTO = categoriaDAOImpl.updateRow(categoriaDTO);
                        }
                        if (categoriaDTO.getDeleted() != null && categoriaDTO.getDeleted()) {
                            categoriaDTO = categoriaDAOImpl.deleteRow(categoriaDTO);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.log(Level.DEBUG, ex);
            throw new CustomException(ex);
        }
        return entityDTO;
    }

    private NegocioDTO insertRow(NegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDSTATUS", entityDTO.getStatus());
            mapSqlParameterSource.addValue("PA_FCLOGO", entityDTO.getPathLogo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdNegocio((BigDecimal) out.get("PA_FIIDNEGO"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Negocio"));
            }

        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Negocio"), ex);
        }
    }

    private NegocioDTO updateRow(NegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDNEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDSTATUS", entityDTO.getStatus());
            mapSqlParameterSource.addValue("PA_FCLOGO", entityDTO.getPathLogo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Negocio"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Negocio"), ex);
        }
    }

    @Override
    public NegocioDTO deleteRow(NegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDNEGO", entityDTO.getIdNegocio());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Negocio success false"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Negocio"), ex);
        }
    }

    /**
     *
     * @param name
     * @return
     * @throws CustomException
     */
    @Override
    public List<NegocioDTO> selectRowsByName(String name) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCDESCRIPCION", name);
        Map<String, Object> out = jdbcSelectByName.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
