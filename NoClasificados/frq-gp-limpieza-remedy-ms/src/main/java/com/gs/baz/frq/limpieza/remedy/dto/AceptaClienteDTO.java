/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Aceptación del cliente", value = "AceptaCliente")
public class AceptaClienteDTO {

    @JsonProperty(value = "idIncidente", required = true)
    @ApiModelProperty(notes = "Clave del incidente", example = "13008219", position = -1, required = true)
    private Integer idIncidente;

    @JsonProperty(value = "calificacion", required = true)
    @ApiModelProperty(notes = "Calificación del cliente", example = "87", required = true)
    private Integer calificacion;

    @JsonProperty(value = "justificacion", required = true)
    @ApiModelProperty(notes = "Razones de la calificación", example = "No se cumplió con la solicitud", required = true)
    private String justificacion;

    public AceptaClienteDTO() {
        super();

    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    @Override
    public String toString() {
        return "AceptaClienteDTO{" + "idIncidente="
                + idIncidente + ", calificacion=" + calificacion
                + ", justificacion=" + justificacion + '}';
    }

}
