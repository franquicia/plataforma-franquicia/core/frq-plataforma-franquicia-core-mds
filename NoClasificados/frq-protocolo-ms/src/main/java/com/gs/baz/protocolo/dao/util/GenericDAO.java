/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.protocolo.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.protocolo.dto.ProtocoloDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public dto selectRow(Long entityID) throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<ProtocoloDTO> selectRows() throws CustomException;

    /**
     *
     * @param idDisciplinaNegocio
     * @return
     * @throws CustomException
     */
    public List<ProtocoloDTO> selectRowsProtocolosDisciplina(Long idDisciplinaNegocio) throws CustomException;

}
