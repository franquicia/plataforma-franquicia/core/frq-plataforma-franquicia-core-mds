/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.dto;

/**
 *
 * @author cescobarh
 */
public enum TiposElemento {
    
    directorio, archivo;

    public static TiposElemento getDirectorio() {
        return directorio;
    }

    public static TiposElemento getArchivo() {
        return archivo;
    }    
    
}
