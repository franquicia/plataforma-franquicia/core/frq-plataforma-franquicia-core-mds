/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.filtros;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class FiltroAnioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "anio")
    private Integer anio;

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(BigDecimal anio) {
        this.anio = (anio == null ? null : anio.intValue());
    }

    @Override
    public String toString() {
        return "AnioDTO{" + "anio=" + anio + '}';
    }

}
