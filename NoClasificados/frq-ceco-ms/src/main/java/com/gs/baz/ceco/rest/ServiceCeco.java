/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ceco.rest;

import com.gs.baz.ceco.dao.CecoDAOImpl;
import com.gs.baz.ceco.dto.CecoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/ceco")
public class ServiceCeco {

    @Autowired
    private CecoDAOImpl cecoDAOImpl;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @param ceco
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{ceco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CecoDTO getCeco(@PathVariable("ceco") Long ceco) throws CustomException {
        return cecoDAOImpl.selectRow(ceco);
    }

    /**
     *
     * @param value
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/ceco/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getCecosByCecoAll(@QueryParam("value") String value) throws CustomException {
        return cecoDAOImpl.selectRows(value, value);
    }

    /**
     *
     * @param value
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/ceco", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getCecosByCeco(@QueryParam("value") String value) throws CustomException {
        return cecoDAOImpl.selectRows(value, null);
    }

    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getCecosByNombre(@QueryParam("value") String value) throws CustomException {
        return cecoDAOImpl.selectRows(null, value);
    }

    @RequestMapping(value = "secure/get/region", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getRegion() throws CustomException {
        return cecoDAOImpl.selectRowsRegion();
    }

    @RequestMapping(value = "secure/get/zona", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getZona() throws CustomException {
        return cecoDAOImpl.selectRowsZona();
    }

    @RequestMapping(value = "secure/get/territorio", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getTerritorio() throws CustomException {
        return cecoDAOImpl.selectRowsTerritorio();
    }

    @RequestMapping(value = "secure/post/ceco/gen", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CecoDTO> getCecosByCecoGen(@RequestBody CecoDTO cecos) throws CustomException {
        return cecoDAOImpl.selectRowsGen(cecos);

    }

}
