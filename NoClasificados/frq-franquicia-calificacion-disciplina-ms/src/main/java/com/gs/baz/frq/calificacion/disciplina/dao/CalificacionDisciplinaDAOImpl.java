package com.gs.baz.frq.calificacion.disciplina.dao;

import com.gs.baz.frq.calificacion.disciplina.dao.util.GenericDAO;
import com.gs.baz.frq.calificacion.disciplina.dto.CalificacionDisciplinaDTO;
import com.gs.baz.frq.calificacion.disciplina.mprs.CalificacionDisciplinaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class CalificacionDisciplinaDAOImpl extends DefaultDAO implements GenericDAO<CalificacionDisciplinaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMCALIFDISC");
        jdbcInsert.withProcedureName("SPINSCALIFDISC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMCALIFDISC");
        jdbcUpdate.withProcedureName("SPACTCALIFDISC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMCALIFDISC");
        jdbcDelete.withProcedureName("SPDELCALIFDISC");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMCALIFDISC");
        jdbcSelect.withProcedureName("SPGETCALIFDISC");
        jdbcSelect.returningResultSet("PA_CDATOS", new CalificacionDisciplinaRowMapper());
    }

    @Override
    public List<CalificacionDisciplinaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", null);
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<CalificacionDisciplinaDTO>) out.get("PA_CDATOS");
    }

    @Override
    public List<CalificacionDisciplinaDTO> selectRowsCalificacionDisciplina(CalificacionDisciplinaDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<CalificacionDisciplinaDTO>) out.get("PA_CDATOS");

    }

    @Override
    public CalificacionDisciplinaDTO insertRow(CalificacionDisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FICALIF_DISC", entityDTO.getCalificacionDisciplina());
            mapSqlParameterSource.addValue("PA_FDFECHA", entityDTO.getFecha());
            mapSqlParameterSource.addValue("PA_FIPUNTOSOBT", entityDTO.getPuntosObtenidos());
            mapSqlParameterSource.addValue("PA_FIPUNTOSESP", entityDTO.getPuntosEsperados());
            mapSqlParameterSource.addValue("PA_FINUMDEFECTOS", entityDTO.getNumeroDefectos());
            mapSqlParameterSource.addValue("PA_FINUMOPORTUNID", entityDTO.getNumeroOportunidades());
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO.getIdRango());
            System.out.println("entityDTO " + entityDTO.toString());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of calificacion disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

    @Override
    public CalificacionDisciplinaDTO updateRow(CalificacionDisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FICALIF_DISC", entityDTO.getCalificacionDisciplina());
            mapSqlParameterSource.addValue("PA_FDFECHA", entityDTO.getFecha());
            mapSqlParameterSource.addValue("PA_FIPUNTOSOBT", entityDTO.getPuntosObtenidos());
            mapSqlParameterSource.addValue("PA_FIPUNTOSESP", entityDTO.getPuntosEsperados());
            mapSqlParameterSource.addValue("PA_FINUMDEFECTOS", entityDTO.getNumeroDefectos());
            mapSqlParameterSource.addValue("PA_FINUMOPORTUNID", entityDTO.getNumeroOportunidades());
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO.getIdRango());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of calificacion disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

    @Override
    public CalificacionDisciplinaDTO deleteRow(CalificacionDisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of calificacion disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

}
