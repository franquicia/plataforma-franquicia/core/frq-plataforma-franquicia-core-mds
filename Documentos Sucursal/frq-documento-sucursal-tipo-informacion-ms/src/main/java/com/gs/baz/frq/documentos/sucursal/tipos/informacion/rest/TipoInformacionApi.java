/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.tipos.informacion.rest;

import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dao.DocumentoSucursalTipoInformacionDAOImpl;
import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.AltaTipoInformacion;
import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.ParametrosTipoInformacion;
import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.SinResultado;
import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.TipoInformacionBase;
import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.TiposInformacion;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "tiposInformacion", value = "tiposInformacion", description = "Api para la gestión del catalogo de tipos de informacion")
@RestController
@RequestMapping("/api-local/documento-sucursal/tipo-informacion/v1")
public class TipoInformacionApi {

    @Autowired
    private DocumentoSucursalTipoInformacionDAOImpl tipoInformacionDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene tipoInformacion", notes = "Obtiene un tipoInformacion", nickname = "obtieneTipoInformacion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idTipoInformacion}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoInformacionBase obtieneTipoInformacion(@ApiParam(name = "idTipoInformacion", value = "Identificador del tipoInformacion", example = "1", required = true) @PathVariable("idTipoInformacion") Long idTipoInformacion) throws CustomException, DataNotFoundException {
        TipoInformacionBase tipoInformacionBase = tipoInformacionDAOImpl.selectRow(idTipoInformacion);
        if (tipoInformacionBase == null) {
            throw new DataNotFoundException();
        }
        return tipoInformacionBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene tiposInformacion", notes = "Obtiene todos los tiposInformacion", nickname = "obtieneTiposInformacion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TiposInformacion obtieneTiposInformacion() throws CustomException, DataNotFoundException {
        TiposInformacion tiposInformacion = new TiposInformacion(tipoInformacionDAOImpl.selectAllRows());
        if (tiposInformacion.getTiposInformacion() != null && tiposInformacion.getTiposInformacion().isEmpty()) {
            throw new DataNotFoundException();
        }
        return tiposInformacion;
    }

    /**
     *
     * @param parametrosTipoInformacion
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear tipoInformacion", notes = "Agrega un tipoInformacion", nickname = "creaTipoInformacion")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaTipoInformacion creaTipoInformacion(@ApiParam(name = "ParametrosTipoInformacion", value = "Paramentros para el alta del tipoInformacion", required = true) @RequestBody ParametrosTipoInformacion parametrosTipoInformacion) throws DataNotInsertedException {
        return tipoInformacionDAOImpl.insertRow(parametrosTipoInformacion);
    }

    /**
     *
     * @param parametrosTipoInformacion
     * @param idTipoInformacion
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar tipoInformacion", notes = "Actualiza un tipoInformacion", nickname = "actualizaTipoInformacion")
    @RequestMapping(value = "/{idTipoInformacion}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaTipoInformacion(@ApiParam(name = "ParametrosTipoInformacion", value = "Paramentros para la actualización del tipoInformacion", required = true) @RequestBody ParametrosTipoInformacion parametrosTipoInformacion, @ApiParam(name = "idTipoInformacion", value = "Identificador del tipoInformacion", example = "1", required = true) @PathVariable("idTipoInformacion") Integer idTipoInformacion) throws DataNotUpdatedException {
        parametrosTipoInformacion.setIdTipoInformacion(idTipoInformacion);
        tipoInformacionDAOImpl.updateRow(parametrosTipoInformacion);
        return new SinResultado();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar tipoInformacion", notes = "Elimina un item de los tiposInformacion", nickname = "eliminaTipoInformacion")
    @RequestMapping(value = "/{idTipoInformacion}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaTipoInformacion(@ApiParam(name = "idTipoInformacion", value = "Identificador del tipoInformacion", example = "1", required = true) @PathVariable("idTipoInformacion") Integer idTipoInformacion) throws DataNotDeletedException {
        tipoInformacionDAOImpl.deleteRow(idTipoInformacion);
        return new SinResultado();
    }

}
