/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import com.gs.baz.security.access.dao.SecurityAccessDAOImpl;
import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "seguridad", value = "seguridad", description = "API de seguridad para la generación de llaves asimétricas.")
@RestController
@RequestMapping("/api/seguridad/v1")
public class SecurityAccessApi {

    @Autowired
    private SecurityAccessDAOImpl generaLlaveDaoImp;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    @ApiOperation(value = "Acceso", notes = "Regresa el acceso consultado mediante el idAcceso", nickname = "getAcceso", hidden = true)
    @RequestMapping(value = "/aplicaciones/llaves/{idAcceso}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AccesoAsimetricoDTO getAcceso(@ApiParam(example = "1b634712-3258-4a8e-b8a8-f8e19f11d1e2", value = "Es el identificador del acceso", required = true) @PathVariable("idAcceso") String idAcceso) throws CustomException {
        return generaLlaveDaoImp.selectRow(idAcceso);
    }

    @ApiOperation(value = "GeneraAccesos", notes = "Genera llaves pública y privada asociada a un identificador de acceso.", nickname = "getGeneraAccesos")
    @RequestMapping(value = "/aplicaciones/llaves", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AccesoAsimetricoDTO getGeneraAccesos() throws CustomException {
        return generaLlaveDaoImp.insertRow();
    }

//    @ApiOperation(value = "BorrarAcceso", notes = "Borra acceso", nickname = "deleteAcceso", hidden = true)
//    @RequestMapping(value = "/aplicaciones/llaves/{idAcceso}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    private void deleteAcceso(@ApiParam(example = "1b634712-3258-4a8e-b8a8-f8e19f11d1e2", value = "Es el identificador del acceso", required = true) @PathVariable("idAcceso") String idAcceso) throws CustomException {
        generaLlaveDaoImp.deleteRow(idAcceso);
    }
}
