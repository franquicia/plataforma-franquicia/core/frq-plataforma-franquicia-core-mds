package com.gs.baz.frq.dashboard.mprs.graficos.g007;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DistribucionCumplimientoCecoNivelRowMapper implements RowMapper<DistribucionCumplimientoCecoNivelDTO> {

    private DistribucionCumplimientoCecoNivelDTO distribucionCumplimientoCecoNivelDTO;

    @Override
    public DistribucionCumplimientoCecoNivelDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        distribucionCumplimientoCecoNivelDTO = new DistribucionCumplimientoCecoNivelDTO();
        distribucionCumplimientoCecoNivelDTO.setDescripcion(rs.getString("DESCRIPCION"));
        distribucionCumplimientoCecoNivelDTO.setIdRango((BigDecimal) rs.getObject("FIRANGO_ID"));
        distribucionCumplimientoCecoNivelDTO.setPorcentaje((BigDecimal) rs.getObject("PORCENTAJE"));
        distribucionCumplimientoCecoNivelDTO.setPuntosContacto((BigDecimal) rs.getObject("PUNTOSCONTACTO"));
        distribucionCumplimientoCecoNivelDTO.setIdCeco(new Integer(rs.getString("CECO")));
        return distribucionCumplimientoCecoNivelDTO;
    }
}
