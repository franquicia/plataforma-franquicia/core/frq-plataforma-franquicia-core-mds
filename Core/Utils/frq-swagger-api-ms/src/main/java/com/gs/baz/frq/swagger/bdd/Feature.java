/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Feature {

    private String name;
    private String description;
    private List<ScenarioImp> scenarios;
    private List<ScenarioOutlineImp> scenariosOutline;

    public Feature() {
    }

    public Feature(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ScenarioImp> getScenarios() {
        return scenarios;
    }

    public ScenarioImp Scenario(String name) {
        ScenarioImp scenario = new ScenarioImp(name);
        if (scenarios == null) {
            this.scenarios = new ArrayList<>();
        }
        this.scenarios.add(scenario);
        return scenario;
    }

    public ScenarioImp Scenario(Scenario scenario) {
        ScenarioImp scenarioImp = new ScenarioImp(scenario);
        if (scenarios == null) {
            this.scenarios = new ArrayList<>();
        }
        this.scenarios.add(scenarioImp);
        return scenarioImp;
    }

    public ScenarioOutlineImp ScenarioOutline(String name) {
        ScenarioOutlineImp scenarioOutline = new ScenarioOutlineImp(name);
        if (scenariosOutline == null) {
            this.scenariosOutline = new ArrayList<>();
        }
        this.scenariosOutline.add(scenarioOutline);
        return scenarioOutline;
    }

    public ScenarioOutlineImp ScenarioOutline(Scenario scenarioOutline) {
        ScenarioOutlineImp scenarioOutlineImp = new ScenarioOutlineImp(scenarioOutline);
        if (scenariosOutline == null) {
            this.scenariosOutline = new ArrayList<>();
        }
        this.scenariosOutline.add(scenarioOutlineImp);
        return scenarioOutlineImp;
    }

    public String toStringFormat() {
        StringBuilder feature = new StringBuilder("Feature: " + name + "\n    ");
        feature.append(description).append("\n");
        if (scenarios != null) {
            scenarios.forEach(scenario -> {
                feature.append("        Scenario: ").append(scenario.getName()).append("\n");
                if (scenario.getSteps() != null) {
                    scenario.getSteps().forEach((step) -> {
                        feature.append("            ").append(step.name()).append(" ").append(step.getDescripction()).append("\n");
                        Parameters parameters = step.Parameters();
                        if (parameters != null) {
                            feature.append(parameters.toString()).append("\n");
                        }
                        if (step.getConnectors() != null) {
                            step.getConnectors().forEach((connector) -> {
                                feature.append("              ").append(connector.name()).append(" ").append(connector.getDescripction()).append("\n");
                                Parameters parametersConnector = connector.Parameters();
                                if (parametersConnector != null) {
                                    feature.append(parametersConnector.toString()).append("\n");
                                }
                            });
                        }
                    });
                }
            });
        }
        if (scenariosOutline != null) {
            scenariosOutline.forEach(scenarioOutline -> {
                feature.append("        Scenario Outline: ").append(scenarioOutline.getName()).append("\n");
                if (scenarioOutline.getSteps() != null) {
                    scenarioOutline.getSteps().forEach((step) -> {
                        feature.append("            ").append(step.name()).append(" ").append(step.getDescripction()).append("\n");
                        Parameters parameters = step.Parameters();
                        if (parameters != null) {
                            feature.append(parameters.toString()).append("\n");
                        }
                        if (step.getConnectors() != null) {
                            step.getConnectors().forEach((connector) -> {
                                feature.append("              ").append(connector.name()).append(" ").append(connector.getDescripction()).append("\n");
                                Parameters parametersConnector = connector.Parameters();
                                if (parametersConnector != null) {
                                    feature.append(parametersConnector.toString()).append("\n");
                                }
                            });
                        }
                    });
                }
                List<Examples> examplesList = scenarioOutline.getExamples();
                if (examplesList != null) {
                    int exampleIndex = 0;
                    for (Examples examples : examplesList) {
                        exampleIndex++;
                        feature.append("        ").append("Examples:").append("\n");
                        feature.append(examples.toString());
                        if (exampleIndex <= examplesList.size()) {
                            feature.append("\n");
                        }
                    }
                }
            });
        }
        return feature.toString();
    }

    @Override
    public String toString() {
        return "Feature{" + "name=" + name + ", description=" + description + ", scenarios=" + scenarios + ", scenariosOutline=" + scenariosOutline + '}';
    }

}
