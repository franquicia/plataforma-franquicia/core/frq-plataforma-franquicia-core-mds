/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao;

import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.FolioCargaParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.ValidaProveedor;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionProveedoresLayoutDAOImpl extends DefaultDAO {

   
    private DefaultJdbcCall jdbcValidaProveedor;
    private DefaultJdbcCall jdbcActualizaFolioCarga;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMLYPROVEED";
       
        jdbcValidaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcValidaProveedor.withSchemaName(schema);
        jdbcValidaProveedor.withCatalogName(catalogo);
        jdbcValidaProveedor.withProcedureName("SPVALPROVEEDOR");
        
        jdbcActualizaFolioCarga = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcActualizaFolioCarga.withSchemaName(schema);
        jdbcActualizaFolioCarga.withCatalogName(catalogo);
        jdbcActualizaFolioCarga.withProcedureName("SPACTLYFOLIOCARGA");
    }

    

    public boolean loginProveedor(ValidaProveedor entityDTO) throws DataNotFoundException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", entityDTO.getIdProveedor());
            mapSqlParameterSource.addValue("PA_FCCONTRASENA", entityDTO.getContrasena());
            Map<String, Object> out = jdbcValidaProveedor.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NVALIDO")).intValue();
            Boolean respuesta = false;
            if (success == 1) {
                respuesta = true;
                
            } else {
            respuesta = false;
                throw new DataNotFoundException();
            }
            return respuesta;
        } catch (Exception ex) {
            throw ex;
        }
        
    }
    
    
    public void actualizaFolioCarga(FolioCargaParametros entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getCentroCostos());
            mapSqlParameterSource.addValue("PA_FIESTADOCARGA", entityDTO.getIdEstado());
            mapSqlParameterSource.addValue("PA_FDFECHAFOLIO", entityDTO.getFechaFolio());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            Map<String, Object> out = jdbcActualizaFolioCarga.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

}
