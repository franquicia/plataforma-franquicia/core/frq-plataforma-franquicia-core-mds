package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.init;

import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dao.SemaforoRangoDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dao.SistemaInformacionDashboardSemaforoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.sistema.informacion.dashboard.semaforo")
public class ConfigSemaforo {

    private final Logger logger = LogManager.getLogger();

    public ConfigSemaforo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SistemaInformacionDashboardSemaforoDAOImpl sistemaInformacionDashboardSemaforoDAOImpl() {
        return new SistemaInformacionDashboardSemaforoDAOImpl();
    }

    @Bean(initMethod = "init")
    public SemaforoRangoDAOImpl semaforoRangoDAOImpl() {
        return new SemaforoRangoDAOImpl();
    }

}
