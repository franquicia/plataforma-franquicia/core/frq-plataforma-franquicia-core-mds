package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralDTO;

/**
 *
 * @author cescobarh
 */
public class ConteoGralRowMapper implements RowMapper<ReporteConteoGralDTO> {

    private ReporteConteoGralDTO status;

    @Override
    public ReporteConteoGralDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteConteoGralDTO();

        status.setConteoProgramadas(rs.getInt("CONTEO_PROGRAM"));
        status.setConteoRealizadas(rs.getInt("CONTEO_ECHAS"));
        //status.setNombreUsuario(rs.getString("NOMBRE_USU"));

        return status;
    }
}
