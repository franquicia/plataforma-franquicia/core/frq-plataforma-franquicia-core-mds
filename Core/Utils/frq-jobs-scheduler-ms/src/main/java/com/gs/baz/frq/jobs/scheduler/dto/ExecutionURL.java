/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author cescobarh
 */
public class ExecutionURL implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "url_execute")
    private Boolean urlExecute;

    @JsonProperty(value = "url_external")
    private Boolean urlExternal;

    @JsonProperty(value = "request_method")
    private String requestMethod;

    @JsonProperty(value = "uri")
    private String uri;

    @JsonProperty(value = "request_body")
    private String requestBody;

    public ExecutionURL() {
    }

    public ExecutionURL(Boolean urlExecute, Boolean urlExternal, String requestMethod, String uri, String requestBody) {
        this.urlExecute = urlExecute;
        this.urlExternal = urlExternal;
        this.requestMethod = requestMethod;
        this.uri = uri;
        this.requestBody = requestBody;
    }

    public Boolean getUrlExecute() {
        return urlExecute;
    }

    public void setUrlExecute(Boolean urlExecute) {
        this.urlExecute = urlExecute;
    }

    public Boolean getUrlExternal() {
        return urlExternal;
    }

    public void setUrlExternal(Boolean urlExternal) {
        this.urlExternal = urlExternal;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    @Override
    public String toString() {
        return "ExecutionURL{" + "urlExecute=" + urlExecute + ", urlExternal=" + urlExternal + ", requestMethod=" + requestMethod + ", uri=" + uri + ", requestBody=" + requestBody + '}';
    }

}
