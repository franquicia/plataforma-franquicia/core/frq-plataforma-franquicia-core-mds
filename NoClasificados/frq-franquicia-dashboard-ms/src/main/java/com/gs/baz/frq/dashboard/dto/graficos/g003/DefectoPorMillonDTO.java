/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g003;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DefectoPorMillonDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "defecto_millon")
    private Integer defectoPorMillon;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "sigma")
    private Integer sigma;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getDefectoPorMillon() {
        return defectoPorMillon;
    }

    public void setDefectoPorMillon(BigDecimal defectoPorMillon) {
        this.defectoPorMillon = (defectoPorMillon == null ? null : defectoPorMillon.intValue());
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(BigDecimal idRango) {
        this.idRango = (idRango == null ? null : idRango.intValue());
    }

    public Integer getSigma() {
        return sigma;
    }

    public void setSigma(BigDecimal sigma) {
        this.sigma = (sigma == null ? null : sigma.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "DefectoPorMillonDTO{" + "defectoPorMillon=" + defectoPorMillon + ", idDisciplina=" + idDisciplina + ", idRango=" + idRango + ", sigma=" + sigma + ", filtros=" + filtros + '}';
    }

}
