/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class DocumentoBase {

    @JsonProperty(value = "titulo")
    @ApiModelProperty(notes = "Titulo del documento", example = "Documento")
    private String titulo;

    @JsonProperty(value = "objetivo")
    @ApiModelProperty(notes = "Objetivo del documento", example = "Objetivo")
    private String objetivo;

    @JsonProperty(value = "version")
    @ApiModelProperty(notes = "version del documento", example = "Version")
    private String version;

    @JsonProperty(value = "controlCambios")
    @ApiModelProperty(notes = "control cambios", example = "ejemplo")
    private String controlCambios;

    @JsonProperty(value = "fechaInicio")
    @ApiModelProperty(notes = "fecha inicio de vigencia", example = "2021-01-30")
    private String fechaInicio;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getControlCambios() {
        return controlCambios;
    }

    public void setControlCambios(String controlCambios) {
        this.controlCambios = controlCambios;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

}
