package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProveedoresRowMapper implements RowMapper<ProveedoresDTO> {

    @Override
    public ProveedoresDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProveedoresDTO proveedores = new ProveedoresDTO();

        proveedores.setMenu(rs.getString("FCMENU"));
        proveedores.setIdProveedor(rs.getInt("FIIDPROVEEDOR"));
        proveedores.setNombreCorto(rs.getString("FCNOMBRECORTO"));
        proveedores.setRazonSocial(rs.getString("FCRAZONSOCIAL"));
        proveedores.setStatus(rs.getString("FCSTATUS"));

        return proveedores;
    }

}
