/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.servicios.servidor;

import com.gs.baz.frq.api.portal.cautivo.bi.AppBI;
import com.gs.baz.frq.api.portal.cautivo.bi.ConexionesBI;
import com.gs.baz.frq.api.portal.cautivo.bi.IdentificadorBI;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.concurrent.ExecutionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MADA
 */
@Api(tags = "portal-cautivo", value = "portal-cautivo", description = "API dedicada al consumo de información de Portal Cautivo de Total Play ubicado en sucursales.")
@RestController
@RequestMapping("/service/portal-cautivo/v1")
public class FRQServicioApp {

    @Autowired
    AppBI appBI;

    @Autowired
    ConexionesBI conexionesBI;

    @Autowired
    IdentificadorBI identificadorBI;

    private final Logger logger = LogManager.getLogger();

    @ApiOperation(value = "Obtiene los registros de las conexiones que son realizadas por la Aplicación mediante el portal cautivo.", notes = "Obtiene información de las conexiones por la Aplicación al Portal Cautivo.", nickname = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/conexiones/aplicacion-movil", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaLessResult200 getConexionesAPP(
            @ApiParam(name = "tipo", value = "Tipo de origen de la conexión", required = true)
            @RequestParam(value = "tipo", required = true) String tipo,
            //@ApiParam(name = "fecha", value = "Fecha Inicio en formato DD/MM/YYYY", required = true)
            //@RequestParam(value = "fecha", required = true) String fecha,
            @ApiParam(name = "fechaInicio", value = "Fecha Inicio en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaInicio", required = true) String fechaIni,
            @ApiParam(name = "fechaFin", value = "Fecha Fin en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @ApiParam(name = "numeroEconomico", value = "Numero Económico con prefijo del negocio (si hay más de uno se separa por comas)")
            @RequestParam(value = "numeroEconomico") String noEco
    ) throws CustomException, InterruptedException, ExecutionException {
        System.out.println("tipo " + tipo + " fechaInicio " + fechaIni + " fechaFin" + fechaFin);
        Boolean respuesta = appBI.insertarClientesEC(tipo, fechaIni, fechaFin, noEco);
        System.out.println("respuesta" + respuesta);
        if (!respuesta) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  DispositivosWifi"));
        }

        return new RespuestaLessResult200();

    }

    @ApiOperation(value = "Obtiene los registros de las conexiones que son realizadas por formulario fuera de la aplicación móvil mediante el portal cautivo.", notes = "Obtiene información de las conexiones sin ingreso a la Aplcicación Móvil sino por formulario al Portal Cautivo.", nickname = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/conexiones/formulario", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaLessResult200 getConexionesFormulario(
            @ApiParam(name = "tipo", value = "Tipo de origen de la conexión", required = true)
            @RequestParam(value = "tipo", required = true) String tipo,
            @ApiParam(name = "fechaInicio", value = "Fecha Inicio en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaInicio", required = true) String fechaIni,
            @ApiParam(name = "fechaFin", value = "Fecha Fin en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @ApiParam(name = "numeroEconomico", value = "Numero Económico con prefijo del negocio (si hay más de uno se separa por comas)")
            @RequestParam(value = "numeroEconomico") String noEco
    ) throws CustomException, InterruptedException, ExecutionException {
        System.out.println("tipo " + tipo + " fechaInicio " + fechaIni + " fechaFin " + fechaFin);
        Boolean respuesta = conexionesBI.insertarClientesEC(tipo, fechaIni, fechaFin, noEco);
        System.out.println("respuesta " + respuesta);
        if (!respuesta) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  DispositivosWifi"));
        }

        return new RespuestaLessResult200();

    }

    @ApiOperation(value = "Obtiene los registros de las conexiones que son realizadas por formulario fuera de la aplicación móvil mediante el portal cautivo.", notes = "Obtiene información de las conexiones sin ingreso a la Aplcicación Móvil sino por formulario al Portal Cautivo.", nickname = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/identificador-cliente-unico", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaLessResult200 getConexionesIdentificador(
            @ApiParam(name = "fechaInicio", value = "Fecha Inicio en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaInicio", required = true) String fechaIni,
            @ApiParam(name = "fechaFin", value = "Fecha Fin en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @ApiParam(name = "numeroEconomico", value = "Numero Económico con prefijo del negocio (si hay más de uno se separa por comas)")
            @RequestParam(value = "numeroEconomico") String noEco
    ) throws CustomException, InterruptedException, ExecutionException {
        System.out.println("fechaInicio " + fechaIni + " fechaFin " + fechaFin);
        Boolean respuesta = identificadorBI.insertarClientesEC(fechaIni, fechaFin, noEco);
        System.out.println("respuesta " + respuesta);
        if (!respuesta) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  DispositivosWifi"));
        }

        return new RespuestaLessResult200();

    }
}
