/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.bi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.validador.util.ForceStringDeserializer;
import com.gs.baz.frq.validador.util.JavaxValidationException;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 *
 * @author cescobarh
 */
public class RequestValidatorAdditionalWrapper extends HttpServletRequestWrapper {

    private final Logger logger = LogManager.getLogger();
    private String body;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    private ValidatorFactory factory = Validation.byDefaultProvider().configure().buildValidatorFactory();
    private Validator validator = factory.getValidator();
    private final PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();

    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws CustomException
     */
    public RequestValidatorAdditionalWrapper(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws CustomException {
        super(httpServletRequest);
        try {
            body = new BufferedReader(new InputStreamReader(httpServletRequest.getInputStream())).lines().parallel().collect(Collectors.joining("\n"));
        } catch (IOException ex) {
            throw new CustomException(ex);
        }
        this.applyValidations(httpServletRequest);
    }

    private JsonProperty getJsonProperty(Object object, String path) {
        try {
            PropertyDescriptor property = propertyUtilsBean.getPropertyDescriptor(object, path);
            Class tempClass = property.getReadMethod().getDeclaringClass();
            String propertyName = property.getName();
            Field field = tempClass.getDeclaredField(propertyName);
            for (Annotation ann : field.getDeclaredAnnotations()) {
                if (ann.annotationType() == JsonProperty.class) {
                    JsonProperty jsonProperty = (JsonProperty) ann;
                    return jsonProperty;
                }
            }
            return null;
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            return null;
        }
    }

    private List<String> getViolations(Object value) {
        List<String> errorMessages = new ArrayList<>();
        Set<ConstraintViolation<Object>> violations = validator.validate(value);
        violations.forEach(action -> {
            try {
                Path path = action.getPropertyPath();
                String pathString = path.toString();
                String[] pathPart = pathString.split("\\.");
                List<String> listPathFixed = new ArrayList<>();
                String splitJoin = "";
                String splitPathFixed = "";
                for (String item : pathPart) {
                    splitJoin += item;
                    JsonProperty jsonProperty = getJsonProperty(value, splitJoin);
                    if (jsonProperty != null) {
                        int startIndex = item.indexOf("[");
                        if (startIndex > -1) {
                            splitPathFixed += jsonProperty.value() + item.substring(startIndex, item.length());
                        } else {
                            splitPathFixed += jsonProperty.value();
                        }
                    } else {
                        splitPathFixed += item;
                    }
                    listPathFixed.add(splitPathFixed);
                    splitJoin += ".";
                    splitPathFixed += ".";
                }
                if (!listPathFixed.isEmpty()) {
                    errorMessages.add("El atributo '" + listPathFixed.get(listPathFixed.size() - 1) + "', " + action.getMessage());
                }
            } catch (SecurityException ex) {

            }
        });
        return errorMessages;
    }

    private void applyValidations(HttpServletRequest httpServletRequest) throws CustomException {
        if (requestMappingHandlerMapping == null) {
            ServletContext servletContext = httpServletRequest.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            if (webApplicationContext != null) {
                requestMappingHandlerMapping = webApplicationContext.getBean(RequestMappingHandlerMapping.class);
            }
        }
        if (requestMappingHandlerMapping != null) {
            try {
                HandlerExecutionChain handlerExecutionChain = requestMappingHandlerMapping.getHandler(httpServletRequest);
                if (handlerExecutionChain != null) {
                    HandlerMethod handlerMethod = (HandlerMethod) handlerExecutionChain.getHandler();
                    if (handlerMethod != null) {
                        Parameter[] methodParameters = handlerMethod.getMethod().getParameters();
                        for (Parameter itemMethodParameter : methodParameters) {
                            RequestBody requestBody = itemMethodParameter.getAnnotation(RequestBody.class);
                            if (requestBody != null) {
                                String mt = httpServletRequest.getContentType();
                                if (body != null && !body.isEmpty()) {
                                    if (mt != null && mt.contains(MediaType.APPLICATION_JSON_VALUE)) {
                                        SimpleModule module = new SimpleModule();
                                        module.addDeserializer(String.class, new ForceStringDeserializer());
                                        objectMapper.configure(DeserializationFeature.ACCEPT_FLOAT_AS_INT, false);
                                        objectMapper.registerModule(module);
                                        Object out = objectMapper.readValue(body, itemMethodParameter.getType());
                                        List<String> violations = getViolations(out);
                                        if (!violations.isEmpty()) {
                                            JavaxValidationException javaxValidationException = new JavaxValidationException();
                                            javaxValidationException.setViolations(violations);
                                            throw new CustomException(javaxValidationException);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                throw new CustomException(ex);
            }
        }
    }

    /**
     *
     * @return @throws IOException
     */
    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new ServletInputStreamRequestWrapper(body.getBytes());
    }

    /**
     *
     * @return @throws IOException
     */
    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }
}
