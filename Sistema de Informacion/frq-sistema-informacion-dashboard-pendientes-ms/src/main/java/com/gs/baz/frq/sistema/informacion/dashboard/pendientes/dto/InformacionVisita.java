/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Información general de Visita", value = "InformacionVisita")
public class InformacionVisita {

    @JsonProperty(value = "centroCostosBase")
    @ApiModelProperty(notes = "Número del ceco base", example = "", position = -1)
    private String centroCostosBase;

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del ceco", example = "", position = -2)
    private String centroCostos;
    
    @JsonProperty(value = "nombreCeco")
    @ApiModelProperty(notes = "Nombre del ceco", example = "", position = -3)
    private String nombreCeco;
    
    @JsonProperty(value = "fechaVisita")
    @ApiModelProperty(notes = "Fecha de la visita", example = "05/11/2021", position = -4)
    private String fechaVisita;
    
    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Número de empleado", example = "", position = -5)
    private Integer numeroEmpleado;
    
    @JsonProperty(value = "nombreEmpleado")
    @ApiModelProperty(notes = "Nombre del empleado", example = "", position = -6)
    private String nombreEmpleado;

    public String getCentroCostosBase() {
        return centroCostosBase;
    }

    public void setCentroCostosBase(String centroCostosBase) {
        this.centroCostosBase = centroCostosBase;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    @Override
    public String toString() {
        return "CalificacionCeco{" + "centroCostosBase=" + centroCostosBase + ", centroCostos=" + centroCostos + ", nombreCeco=" + nombreCeco + ", fechaVisita=" + fechaVisita + ", numeroEmpleado=" + numeroEmpleado + ", nombreEmpleado=" + nombreEmpleado + '}';
    }

}
