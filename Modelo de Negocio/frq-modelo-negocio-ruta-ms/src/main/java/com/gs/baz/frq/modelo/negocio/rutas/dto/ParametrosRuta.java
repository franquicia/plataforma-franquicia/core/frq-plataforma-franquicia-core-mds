/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosRuta {

    @JsonProperty(value = "idRuta")
    private transient Integer idRuta;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del Folio", example = "1")
    private transient Integer idFolio;

    @JsonProperty(value = "codificacion")
    @ApiModelProperty(notes = "Codificacion de la ruta basada al modelo de negocio", example = "FRA-CYC", required = true)
    private String codificacion;

    @JsonProperty(value = "idPais")
    @ApiModelProperty(notes = "Identificados del pais", example = "1", required = true)
    private Integer idPais;

    @JsonProperty(value = "idModelo")
    @ApiModelProperty(notes = "Identificados del modelo", example = "1", required = true)
    private Integer idModelo;

    @JsonProperty(value = "idUnidadNegocio")
    @ApiModelProperty(notes = "Identificados de la unidad de negocio", example = "1", required = true)
    private Integer idUnidadNegocio;

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(String codificacion) {
        this.codificacion = codificacion;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Integer idModelo) {
        this.idModelo = idModelo;
    }

    public Integer getIdUnidadNegocio() {
        return idUnidadNegocio;
    }

    public void setIdUnidadNegocio(Integer idUnidadNegocio) {
        this.idUnidadNegocio = idUnidadNegocio;
    }

}
