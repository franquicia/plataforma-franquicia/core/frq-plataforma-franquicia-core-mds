package com.gs.baz.frq.documento.sucursal.archivos.init;

import com.gs.baz.frq.documento.sucursal.archivos.dao.DocumentoSucursalArchivoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.archivos")
public class ConfigDocumentoSucursalArchivo {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalArchivo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalArchivoDAOImpl DocumentoSucursalArchivoDAOImpl() {
        return new DocumentoSucursalArchivoDAOImpl();
    }

}
