/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioDocumento;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FolioDocumentoRowMapper implements RowMapper<BusquedaFolioDocumento> {

    private BusquedaFolioDocumento busquedaFolioDocumento;

    @Override
    public BusquedaFolioDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
        busquedaFolioDocumento = new BusquedaFolioDocumento();
        Documento documento = new Documento();
        documento.setTitulo(rs.getString("FCTITULO"));
        documento.setObjetivo(rs.getString("FCOBJETIVO"));
        documento.setVersion(rs.getString("FCVERSION"));
        documento.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        busquedaFolioDocumento.setDocumento(documento);
        busquedaFolioDocumento.setCreador(new Empleado(rs.getInt("FIIDCREADOR"), rs.getString("FCNOMBRE")));
        busquedaFolioDocumento.setFechaCreacion(rs.getString("FDFECHACREACION"));
        return busquedaFolioDocumento;
    }
}
