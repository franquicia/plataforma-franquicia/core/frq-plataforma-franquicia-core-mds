/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class BancaDigitalDTO {

    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonProperty(value = "inicioSesion")
    private String inicioSesion;

    @JsonProperty(value = "tienda")
    private String sucursal;

    @JsonProperty(value = "mac")
    private String mac;

    @JsonProperty(value = "idPortal")
    private Integer idPortal;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getInicioSesion() {
        return inicioSesion;
    }

    public void setInicioSesion(String inicioSesion) {
        this.inicioSesion = inicioSesion;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Integer getIdPortal() {
        return idPortal;
    }

    public void setIdPortal(Integer idPortal) {
        this.idPortal = idPortal;
    }

    @Override
    public String toString() {
        return "BancaDigitalDTO{" + "nombreUsuario=" + nombreUsuario + ", inicioSesion=" + inicioSesion + ", sucursal=" + sucursal + ", mac=" + mac + ", idPortal=" + idPortal + '}';
    }

}
