/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CalificacionCeco;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CertificacionesGerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.DetalleVisita;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.DetalleVisitas;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.Gerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.GraficaGeneral;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.InformacionVisita;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.InformacionVisitas;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesGerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesSemanal;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PuntoVentaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PuntosVentaPendientes;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.RespuestaVisita;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.RespuestasVisitas;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendienteParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.CalificacionCecoRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.DetalleVisitaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.InfoVisitaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.PendientesGerenteRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.PendientesSemanalRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.PuntosVentaPendientesRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.RespuestasVisitaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.VisitasPendientesRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionVisitasPendientesDetalleDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCalificacionCeco;
    private DefaultJdbcCall jdbcSelectInfoVisita;
    private DefaultJdbcCall jdbcSelectDetalleVisita;
    private DefaultJdbcCall jdbcSelectRespuestasVisita;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PADASHBMONPENVIS";

        jdbcSelectCalificacionCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionCeco.withSchemaName(schema);
        jdbcSelectCalificacionCeco.withCatalogName(catalogo);
        jdbcSelectCalificacionCeco.withProcedureName("SPCALIFICECO");
        jdbcSelectCalificacionCeco.returningResultSet("PA_CDATOS", new CalificacionCecoRowMapper());

        jdbcSelectInfoVisita = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectInfoVisita.withSchemaName(schema);
        jdbcSelectInfoVisita.withCatalogName(catalogo);
        jdbcSelectInfoVisita.withProcedureName("SPINFVISITA");
        jdbcSelectInfoVisita.returningResultSet("PA_CDATOS", new InfoVisitaRowMapper());

        jdbcSelectDetalleVisita = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDetalleVisita.withSchemaName(schema);
        jdbcSelectDetalleVisita.withCatalogName(catalogo);
        jdbcSelectDetalleVisita.withProcedureName("SPDETALLEVISITA");
        jdbcSelectDetalleVisita.returningResultSet("PA_CDATOS", new DetalleVisitaRowMapper());
        
        jdbcSelectRespuestasVisita = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectRespuestasVisita.withSchemaName(schema);
        jdbcSelectRespuestasVisita.withCatalogName(catalogo);
        jdbcSelectRespuestasVisita.withProcedureName("SPRESPUESTASVIS");
        jdbcSelectRespuestasVisita.returningResultSet("PA_CDATOS", new RespuestasVisitaRowMapper());
        
    }
    public List<CalificacionCeco> selectCalificacionCeco(String idCeco, String fecha, Integer idProtocolo) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", idCeco);
        mapSqlParameterSource.addValue("PA_FECHA", fecha);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", idProtocolo);
        Map<String, Object> out = jdbcSelectCalificacionCeco.execute(mapSqlParameterSource);
        Integer ejecucion = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
        if(ejecucion==1){
            return (List<CalificacionCeco>) out.get("PA_CDATOS");
        }else{   
        return null;
        }
    }

    public List<InformacionVisita> selectInfoVisita(String idCeco, String fecha) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", idCeco);
        mapSqlParameterSource.addValue("PA_FECHA", fecha);
        Map<String, Object> out = jdbcSelectInfoVisita.execute(mapSqlParameterSource);
        Integer ejecucion = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
        if(ejecucion==1){
            return (List<InformacionVisita>) out.get("PA_CDATOS");
        }else{   
        return null;
        }
    }
    
    public List<DetalleVisita> selectDetalleVisita(String idCeco, String fecha, Integer idProtocolo) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", idCeco);
        mapSqlParameterSource.addValue("PA_FECHA", fecha);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", idProtocolo);
        Map<String, Object> out = jdbcSelectDetalleVisita.execute(mapSqlParameterSource);
        Integer ejecucion = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
        if(ejecucion==1){
            return (List<DetalleVisita>) out.get("PA_CDATOS");
        }else{   
        return null;
        }
    }
    
    public List<RespuestaVisita> selectRespuestasVisitas(String idCeco, String fecha, Integer idProtocolo) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", idCeco);
        mapSqlParameterSource.addValue("PA_FECHA", fecha);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", idProtocolo);
        Map<String, Object> out = jdbcSelectRespuestasVisita.execute(mapSqlParameterSource);
        Integer ejecucion = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
        if(ejecucion==1){
            return (List<RespuestaVisita>) out.get("PA_CDATOS");
        }else{   
        return null;
        }
    }
}
