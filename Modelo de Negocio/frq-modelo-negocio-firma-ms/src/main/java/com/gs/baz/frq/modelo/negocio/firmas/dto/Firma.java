/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Firma", value = "Firma")
public class Firma extends FirmaBase {

    @JsonProperty(value = "idFirma")
    @ApiModelProperty(notes = "Identificador del Firma", example = "1", position = -1)
    private Integer idFirma;

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    @Override
    public String toString() {
        return "Firma{" + "idFirma=" + idFirma + '}';
    }

}
