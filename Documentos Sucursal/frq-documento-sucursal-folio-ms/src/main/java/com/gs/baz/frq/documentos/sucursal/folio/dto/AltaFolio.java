/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author fjaramillo
 */
@ApiModel(description = "Datos del folio", value = "AltaFolio")
public class AltaFolio {

    @JsonProperty(value = "folio")
    @ApiModelProperty(notes = "Identificador del Folio", example = "1")
    private Integer id;

    public AltaFolio(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
