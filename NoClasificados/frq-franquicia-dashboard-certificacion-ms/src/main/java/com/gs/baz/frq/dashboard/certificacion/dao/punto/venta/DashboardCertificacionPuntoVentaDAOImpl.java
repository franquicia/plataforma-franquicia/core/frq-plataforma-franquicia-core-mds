package com.gs.baz.frq.dashboard.certificacion.dao.punto.venta;

import com.gs.baz.frq.dashboard.certificacion.dto.punto.venta.DashboardCertificacionPuntoVentaDTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.punto.venta.DashboardCertificacionPuntoVentaRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionPuntoVentaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectDetalle;
    private DefaultJdbcCall jdbcSelectRespCert;
    private DefaultJdbcCall jdbcSelectCalifCeco;
    private DefaultJdbcCall jdbcSelectEvidencia;
    private DefaultJdbcCall jdbcSelectInformeCert;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PADASHCCPVDET");
        jdbcSelect.withProcedureName("SPULTIMACERT");
        jdbcSelect.returningResultSet("PA_CULTIMACERT", new DashboardCertificacionPuntoVentaRowMapper());

        jdbcSelectDetalle = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDetalle.withSchemaName(schema);
        jdbcSelectDetalle.withCatalogName("PADASHCCPVDET");
        jdbcSelectDetalle.withProcedureName("SPDETALLECERT");
        jdbcSelectDetalle.returningResultSet("PA_CDETALLECERT", new DashboardCertificacionPuntoVentaRowMapper().new DashboardCertificacionPuntoVentaDetalleCertRowMapper());

        jdbcSelectRespCert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectRespCert.withSchemaName(schema);
        jdbcSelectRespCert.withCatalogName("PADASHCCPVDET");
        jdbcSelectRespCert.withProcedureName("SPRESPUESTASCERT");
        jdbcSelectRespCert.returningResultSet("PA_CRESPCERT", new DashboardCertificacionPuntoVentaRowMapper().new DashboardCertificacionPuntoVentaRespCertRowMapper());

        jdbcSelectCalifCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalifCeco.withSchemaName(schema);
        jdbcSelectCalifCeco.withCatalogName("PADASHCCPVDET");
        jdbcSelectCalifCeco.withProcedureName("SPCALIFICECO");
        jdbcSelectCalifCeco.returningResultSet("PA_CCALIFCECO", new DashboardCertificacionPuntoVentaRowMapper().new DashboardCertificacionPuntoVentaCalifCecoRowMapper());

        jdbcSelectEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectEvidencia.withSchemaName(schema);
        jdbcSelectEvidencia.withCatalogName("PADASHCCPVDET");
        jdbcSelectEvidencia.withProcedureName("SPEVIDENCIASCERT");
        jdbcSelectEvidencia.returningResultSet("PA_CEVIDENCIA", new DashboardCertificacionPuntoVentaRowMapper().new DashboardCertificacionPuntoVentaEvidenciaRowMapper());

        jdbcSelectInformeCert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectInformeCert.withSchemaName(schema);
        jdbcSelectInformeCert.withCatalogName("PADASHCCPVDET");
        jdbcSelectInformeCert.withProcedureName("SPINFORMECERT");
        jdbcSelectInformeCert.returningResultSet("PA_CINFORMECERT", new DashboardCertificacionPuntoVentaRowMapper().new DashboardCertificacionPuntoVentaInformeCertRowMapper());

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowDashboardCertificacionUltCerti(String entityDTO1, Long entityDTO2) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO2);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CULTIMACERT");

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowsDashboardCertificacionUltCerti() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CULTIMACERT");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowDashboardCertificacionDetalleCert(String entityDTO1, Long entityDTO2, String entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO2);
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO3);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", entityDTO4);
        Map<String, Object> out = jdbcSelectDetalle.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CDETALLECERT");

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowsDashboardCertificacionDetalleCert() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", null);
        mapSqlParameterSource.addValue("PA_FECHA", null);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", null);
        Map<String, Object> out = jdbcSelectDetalle.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CDETALLECERT");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowDashboardCertificacionRespCert(String entityDTO1, Long entityDTO2, String entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO2);
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO3);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", entityDTO4);
        Map<String, Object> out = jdbcSelectRespCert.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CRESPCERT");

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowsDashboardCertificacionRespCert() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", null);
        mapSqlParameterSource.addValue("PA_FECHA", null);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", null);
        Map<String, Object> out = jdbcSelectRespCert.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CRESPCERT");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowDashboardCertificacionCalifCeco(String entityDTO1, Long entityDTO2, String entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO2);
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO3);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", entityDTO4);
        Map<String, Object> out = jdbcSelectCalifCeco.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CCALIFCECO");

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowsDashboardCertificacionCalifCeco() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", null);
        mapSqlParameterSource.addValue("PA_FECHA", null);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", null);
        Map<String, Object> out = jdbcSelectCalifCeco.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CCALIFCECO");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowDashboardCertificacionEvidencia(String entityDTO1, Long entityDTO2, String entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO2);
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO3);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", entityDTO4);
        Map<String, Object> out = jdbcSelectEvidencia.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CEVIDENCIA");

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowsDashboardCertificacionEvidencia() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", null);
        mapSqlParameterSource.addValue("PA_FECHA", null);
        mapSqlParameterSource.addValue("PA_PROTOCOLO", null);
        Map<String, Object> out = jdbcSelectEvidencia.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CEVIDENCIA");
        } else {
            return null;
        }

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowDashboardCertificacionInformeCert(String entityDTO1, String entityDTO2, Long entityDTO3) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO1);
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO2);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO3);
        Map<String, Object> out = jdbcSelectInformeCert.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CINFORMECERT");

    }

    public List<DashboardCertificacionPuntoVentaDTO> selectRowsDashboardCertificacionInformeCert() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FECHA", null);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        Map<String, Object> out = jdbcSelectInformeCert.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionPuntoVentaDTO>) out.get("PA_CINFORMECERT");
        } else {
            return null;
        }

    }
}
