/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import com.gs.baz.security.access.dao.AtributoSensibleDAOImpl;
import com.gs.baz.security.access.dto.AtributoSensibleBase;
import com.gs.baz.security.access.dto.AtributoSensible;
import com.gs.baz.security.access.dto.AtributoSensibleFiltroDTO;
import com.gs.baz.security.access.dto.AtributoSensiblePostResponse;
import com.gs.baz.security.access.dto.DescifraRequestDTO;
import com.gs.baz.security.access.dto.DescifraResponseDTO;
import com.gs.baz.security.access.dto.CifraRequestDTO;
import com.gs.baz.security.access.dto.CifraResponseDTO;
import com.gs.baz.security.access.dto.SinResultado;
import com.gs.baz.security.access.dto.ListaAtributosSensibles;
import com.gs.baz.security.access.util.SecurityUtils;
import com.gs.baz.security.access.util.SecurityUtilsException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kramireza
 */
@Api(tags = "utilerias", value = "utilerias", description = "API de Utilerías que brinda recursos de uso común.")
@RestController
@RequestMapping("/api/utilerias/v1")
public class UtileriasApi {

    @Autowired
    private AtributoSensibleDAOImpl atributosSensiblesDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param cifraRequestDTO
     * @return
     * @throws SecurityUtilsException
     */
    @ApiOperation(value = "Cifra valor", notes = "Cifra valor", nickname = "postCifra", hidden = true)
    @RequestMapping(value = "/cifra", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CifraResponseDTO postCifra(@ApiParam(name = "CifraValor", value = "Cifra Texto Plano", required = true) @RequestBody CifraRequestDTO cifraRequestDTO) throws SecurityUtilsException {
        String encrypted = new SecurityUtils.Encrypter(cifraRequestDTO.getAccesoPublico()).encrypt(cifraRequestDTO.getValor());
        return new CifraResponseDTO(encrypted);
    }

    /**
     *
     * @param descifraRequestDTO
     * @return
     * @throws SecurityUtilsException
     */
    @ApiOperation(value = "Descifra valor", notes = "Descifra valor", nickname = "postDescifra", hidden = true)
    @RequestMapping(value = "/descifra", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public DescifraResponseDTO postDescifra(@ApiParam(name = "DescifraValor", value = "Descifra Texto Cifrado", required = true) @RequestBody DescifraRequestDTO descifraRequestDTO) throws SecurityUtilsException {
        String decrypted = new SecurityUtils.Decrypter(descifraRequestDTO.getAccesoPrivado()).decrypt(descifraRequestDTO.getValor());
        return new DescifraResponseDTO(decrypted);
    }

    /**
     *
     * @param idAtributo
     * @return
     * @throws CustomException
     */
    @ApiOperation(value = "Obtiene un atributo sensible", notes = "Regresa un atributo sensible", nickname = "getAtributoSensible")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/atributos-sensibles/{idAtributo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AtributoSensibleBase getAtributoSensible(@ApiParam(name = "idAtributo", value = "Id Atributo Sensible", example = "1", required = true) @PathVariable("idAtributo") Long idAtributo) throws CustomException, DataNotFoundException {
        AtributoSensibleBase atributoSensibleBase = atributosSensiblesDAOImpl.selectRow(idAtributo);
        if (atributoSensibleBase == null) {
            throw new DataNotFoundException();
        }
        return atributoSensibleBase;
    }

    /**
     *
     * @param atributoSensibleFiltroDTO
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Busca lista de atributos sensibles", notes = "Regresa todos los atributos sensibles", nickname = "getAtributosSensibles")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/atributos-sensibles/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ListaAtributosSensibles getAtributosSensibles(@ApiParam(name = "AtributoSensibleFiltros", value = "Atributo Sensible Filtros", required = true) @RequestBody AtributoSensibleFiltroDTO atributoSensibleFiltroDTO) throws CustomException, DataNotFoundException {
        ListaAtributosSensibles listaAtributosSensibles = new ListaAtributosSensibles(atributosSensiblesDAOImpl.selectList(atributoSensibleFiltroDTO));
        if (listaAtributosSensibles.getAtributosSensibles() != null && listaAtributosSensibles.getAtributosSensibles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return listaAtributosSensibles;
    }

    /**
     *
     * @param atributoSensibleBase
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Agrega un atributo nuevo", notes = "Inserta un atributo sensible", nickname = "postAtributoSensible")
    @RequestMapping(value = "/atributos-sensibles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AtributoSensiblePostResponse postAtributoSensible(@ApiParam(name = "AtributoSensible", value = "Atributo Sensible", required = true) @RequestBody AtributoSensibleBase atributoSensibleBase) throws DataNotInsertedException {
        return atributosSensiblesDAOImpl.insertRow(atributoSensibleBase);
    }

    /**
     *
     * @param atributoSensibleBase
     * @param idAtributo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Modifica atributo sensible", notes = "Actualiza un atributo sensible", nickname = "putAtributoSensible")
    @RequestMapping(value = "/atributos-sensibles/{idAtributo}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado putAtributoSensible(@ApiParam(name = "AtributoSensible", value = "Atributo Sensible", required = true) @RequestBody AtributoSensibleBase atributoSensibleBase, @ApiParam(name = "idAtributo", value = "Id Atributo Sensible", example = "1", required = true) @PathVariable("idAtributo") Integer idAtributo) throws DataNotUpdatedException {
        AtributoSensible atributoSensible = new AtributoSensible();
        atributoSensible.setIdAtributo(idAtributo);
        atributoSensible.setApiOrigen(atributoSensibleBase.getApiOrigen().trim());
        atributoSensible.setNombre(atributoSensibleBase.getNombre().trim());
        atributoSensible.setNombreRecurso(atributoSensibleBase.getNombreRecurso().trim());
        atributoSensible.setTipo(atributoSensibleBase.getTipo());
        atributosSensiblesDAOImpl.updateRow(atributoSensible);
        return new SinResultado();
    }

    /**
     *
     * @param idAtributo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Borra atributo sensible", notes = "Borra un atributo sensible", nickname = "deleteAtributoSensible")
    @RequestMapping(value = "/atributos-sensibles/{idAtributo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado deleteAtributoSensible(@ApiParam(name = "idAtributo", value = "Id Atributo Sensible", example = "1", required = true) @PathVariable("idAtributo") Integer idAtributo) throws DataNotDeletedException {
        atributosSensiblesDAOImpl.deleteRow(idAtributo);
        return new SinResultado();
    }

}
