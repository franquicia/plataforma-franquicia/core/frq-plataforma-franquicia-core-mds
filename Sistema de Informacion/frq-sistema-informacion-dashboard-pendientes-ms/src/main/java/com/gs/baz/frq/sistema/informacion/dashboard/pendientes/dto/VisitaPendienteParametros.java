/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos para consulta Visita pendiente", value = "ConsultaVisitaPendiente")
public class VisitaPendienteParametros {

    @JsonProperty(value = "idBitacora")
    @ApiModelProperty(notes = "Identificador de la bitácora", example = "1", position = -1)
    private Integer idBitacora;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Número de empleado del usuario", example = "202622", position = -2)
    private Integer idUsuario;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costos.", example = "920100", position = -3)
    private Integer idCeco;
    
    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Integer idBitacora) {
        this.idBitacora = idBitacora;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    @Override
    public String toString() {
        return "VisitaPendienteParametros{" + "idBitacora=" + idBitacora + ", idUsuario=" + idUsuario + ", idCeco=" + idCeco + '}';
    }

}
