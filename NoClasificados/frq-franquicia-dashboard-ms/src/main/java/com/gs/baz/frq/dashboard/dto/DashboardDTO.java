/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class DashboardDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_dashboard")
    private Integer idDashboard;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_configuracion_disciplina")
    private Integer idConfiguracionDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProotocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_territorio")
    private String idTerritorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_zona")
    private String idZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_region")
    private String idRegion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_disciplina")
    private Integer idNegocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "precalificacion")
    private Integer precalificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_lineal")
    private Integer calificacionLineal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_imperdonable")
    private Integer numeroImperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_imcumplimiento")
    private Integer numeroIncumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_termino")
    private String fechaTermino;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_obtenidos")
    private Integer puntosObtenidos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_esperados")
    private Integer puntosEsperados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_efectos")
    private Integer idEfectos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oportunidades")
    private Integer oportunidades;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_negocio")
    private String idCecoNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_usuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_puesto")
    private Integer idPuesto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "anio")
    private Integer anio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "resultado")
    private Integer resultado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public DashboardDTO() {
    }

    public Integer getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(BigDecimal idDashboard) {
        this.idDashboard = (idDashboard == null ? null : idDashboard.intValue());
    }

    public Integer getIdConfiguracionDisciplina() {
        return idConfiguracionDisciplina;
    }

    public void setIdConfiguracionDisciplina(BigDecimal idConfiguracionDisciplina) {
        this.idConfiguracionDisciplina = (idConfiguracionDisciplina == null ? null : idConfiguracionDisciplina.intValue());
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getIdProotocolo() {
        return idProotocolo;
    }

    public void setIdProotocolo(BigDecimal idProotocolo) {
        this.idProotocolo = (idProotocolo == null ? null : idProotocolo.intValue());
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(String idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getIdZona() {
        return idZona;
    }

    public void setIdZona(String idZona) {
        this.idZona = idZona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getIdNegocioDisciplina() {
        return idNegocioDisciplina;
    }

    public void setIdNegocioDisciplina(BigDecimal idNegocioDisciplina) {
        this.idNegocioDisciplina = (idNegocioDisciplina == null ? null : idNegocioDisciplina.intValue());
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(BigDecimal calificacion) {
        this.calificacion = (calificacion == null ? null : calificacion.intValue());
    }

    public Integer getPrecalificacion() {
        return precalificacion;
    }

    public void setPrecalificacion(BigDecimal precalificacion) {
        this.precalificacion = (precalificacion == null ? null : precalificacion.intValue());
    }

    public Integer getCalificacionLineal() {
        return calificacionLineal;
    }

    public void setCalificacionLineal(BigDecimal calificacionLineal) {
        this.calificacionLineal = (calificacionLineal == null ? null : calificacionLineal.intValue());
    }

    public Integer getNumeroImperdonable() {
        return numeroImperdonable;
    }

    public void setNumeroImperdonable(BigDecimal numeroImperdonable) {
        this.numeroImperdonable = (numeroImperdonable == null ? null : numeroImperdonable.intValue());
    }

    public Integer getNumeroIncumplimiento() {
        return numeroIncumplimiento;
    }

    public void setNumeroIncumplimiento(BigDecimal numeroIncumplimiento) {
        this.numeroIncumplimiento = (numeroIncumplimiento == null ? null : numeroIncumplimiento.intValue());
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public Integer getPuntosObtenidos() {
        return puntosObtenidos;
    }

    public void setPuntosObtenidos(BigDecimal puntosObtenidos) {
        this.puntosObtenidos = (puntosObtenidos == null ? null : puntosObtenidos.intValue());
    }

    public Integer getPuntosEsperados() {
        return puntosEsperados;
    }

    public void setPuntosEsperados(BigDecimal puntosEsperados) {
        this.puntosEsperados = (puntosEsperados == null ? null : puntosEsperados.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public Integer getIdEfectos() {
        return idEfectos;
    }

    public void setIdEfectos(BigDecimal idEfectos) {
        this.idEfectos = (idEfectos == null ? null : idEfectos.intValue());
    }

    public Integer getOportunidades() {
        return oportunidades;
    }

    public void setOportunidades(BigDecimal oportunidades) {
        this.oportunidades = (oportunidades == null ? null : oportunidades.intValue());
    }

    public String getIdCecoNegocio() {
        return idCecoNegocio;
    }

    public void setIdCecoNegocio(String idCecoNegocio) {
        this.idCecoNegocio = idCecoNegocio;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Integer getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(BigDecimal idPuesto) {
        this.idPuesto = (idPuesto == null ? null : idPuesto.intValue());
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(BigDecimal anio) {
        this.anio = (anio == null ? null : anio.intValue());
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(BigDecimal resultado) {
        this.resultado = (resultado == null ? null : resultado.intValue());
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "DashboardDTO{" + "idDashboard=" + idDashboard + ", idConfiguracionDisciplina=" + idConfiguracionDisciplina + ", idDisciplina=" + idDisciplina + ", idProotocolo=" + idProotocolo + ", idCeco=" + idCeco + ", descripcion=" + descripcion + ", periodo=" + periodo + ", idTerritorio=" + idTerritorio + ", territorio=" + territorio + ", idZona=" + idZona + ", zona=" + zona + ", idRegion=" + idRegion + ", region=" + region + ", idNegocioDisciplina=" + idNegocioDisciplina + ", calificacion=" + calificacion + ", numeroImperdonable=" + numeroImperdonable + ", numeroIncumplimiento=" + numeroIncumplimiento + ", fechaTermino=" + fechaTermino + ", puntosObtenidos=" + puntosObtenidos + ", puntosEsperados=" + puntosEsperados + ", idEfectos=" + idEfectos + ", oportunidades=" + oportunidades + ", idCecoNegocio=" + idCecoNegocio + ", idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", idPuesto=" + idPuesto + ", anio=" + anio + ", resultado=" + resultado + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
