/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.rutas.rest;

import com.gs.baz.frq.accesos.sistema.rutas.dao.AccesosSistemaRutasDAOImpl;
import com.gs.baz.frq.accesos.sistema.rutas.dto.AltaRuta;
import com.gs.baz.frq.accesos.sistema.rutas.dto.ParametrosRuta;
import com.gs.baz.frq.accesos.sistema.rutas.dto.RutaBase;
import com.gs.baz.frq.accesos.sistema.rutas.dto.Rutas;
import com.gs.baz.frq.accesos.sistema.rutas.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "accesos", value = "accesos", description = "Api para la gestión de accesos")
@RestController
@RequestMapping("/api-local/accesos/sistema/rutas/v1")
public class AccesosSistemaRutasApi {

    @Autowired
    private AccesosSistemaRutasDAOImpl accesosSistemaRutasDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idRuta
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ruta", notes = "Obtiene una ruta", nickname = "obtieneRuta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idRuta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RutaBase obtieneRuta(@ApiParam(name = "idRuta", value = "Identificador de la ruta", example = "1", required = true) @PathVariable("idRuta") Long idRuta) throws DataNotFoundException {
        RutaBase rutaBase = accesosSistemaRutasDAOImpl.selectRow(idRuta);
        if (rutaBase == null) {
            throw new DataNotFoundException();
        }
        return rutaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutas", notes = "Obtiene todas las rutas", nickname = "obtieneRutas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rutas obtieneRutas() throws CustomException, DataNotFoundException {
        Rutas rutas = new Rutas(accesosSistemaRutasDAOImpl.selectRows());
        if (rutas.getRutas() != null && rutas.getRutas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutas;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutas", notes = "Obtiene todas las rutas", nickname = "obtieneRutas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/borradas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rutas obtieneRutasBorradas() throws CustomException, DataNotFoundException {
        Rutas rutas = new Rutas(accesosSistemaRutasDAOImpl.selectRowsAll());
        if (rutas.getRutas() != null && rutas.getRutas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutas;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutas", notes = "Obtiene todas las rutas", nickname = "obtieneRutas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disponibles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rutas obtieneRutasDisponibles() throws CustomException, DataNotFoundException {
        Rutas rutas = new Rutas(accesosSistemaRutasDAOImpl.selectRowsAllDisponibles());
        if (rutas.getRutas() != null && rutas.getRutas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutas;
    }

    /**
     *
     * @param idRuta
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutas", notes = "Obtiene todas las rutas", nickname = "obtieneRutas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/disponibles/{idRuta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rutas obtieneRutasDisponibles(@ApiParam(name = "idRuta", value = "Identificador de la ruta", example = "1", required = true) @PathVariable("idRuta") Long idRuta) throws CustomException, DataNotFoundException {
        Rutas rutas = new Rutas(accesosSistemaRutasDAOImpl.selectRowsAllDisponibles(idRuta));
        if (rutas.getRutas() != null && rutas.getRutas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutas;
    }

    /**
     *
     * @param parametrosRuta
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear ruta", notes = "Agrega una ruta", nickname = "creaRuta")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaRuta creaRuta(@ApiParam(name = "ParametrosRuta", value = "Paramentros para el alta de la ruta", required = true) @RequestBody ParametrosRuta parametrosRuta) throws DataNotInsertedException {
        return accesosSistemaRutasDAOImpl.insertRow(parametrosRuta);
    }

    /**
     *
     * @param parametrosRuta
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear rutas", notes = "Agrega rutas", nickname = "creaRutas")
    @RequestMapping(value = "/", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public List<AltaRuta> creaRutas(@ApiParam(name = "ParametrosRuta", value = "Paramentros para el alta de la ruta", required = true) @RequestBody List<ParametrosRuta> parametrosRuta) throws DataNotInsertedException {
        return accesosSistemaRutasDAOImpl.insertRow(parametrosRuta);
    }

    /**
     *
     * @param parametrosRuta
     * @param idRuta
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar ruta", notes = "Actualiza una ruta", nickname = "actualizaRuta")
    @RequestMapping(value = "/{idRuta}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaRuta(@ApiParam(name = "ParametrosRuta", value = "Paramentros para la actualización de la ruta", required = true) @RequestBody ParametrosRuta parametrosRuta, @ApiParam(name = "idRuta", value = "Identificador de la ruta", example = "1", required = true) @PathVariable("idRuta") Integer idRuta) throws DataNotUpdatedException {
        parametrosRuta.setIdRuta(idRuta);
        accesosSistemaRutasDAOImpl.updateRow(parametrosRuta);
        return new SinResultado();
    }

    /**
     *
     * @param idRuta
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar ruta", notes = "Elimina un item de las rutas", nickname = "eliminaRuta")
    @RequestMapping(value = "/{idRuta}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaRuta(@ApiParam(name = "idRuta", value = "Identificador de la ruta", example = "1", required = true) @PathVariable("idRuta") Integer idRuta) throws DataNotDeletedException {
        accesosSistemaRutasDAOImpl.deleteRow(idRuta);
        return new SinResultado();
    }

}
