package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Detalle de Proveedores", value = "Proveedores")
public class ProveedoresDTO {

    @JsonProperty(value = "idProveedor")
    @ApiModelProperty(notes = "Clave del proveedor", example = "80800815", position = -1)
    private Integer idProveedor;

    @JsonProperty(value = "menuProveedor")
    @ApiModelProperty(notes = "Indetificador del proveedor conformado por el número de proveedor y "
            + "el nombre del proveedor, se utiliza para el llenado de un menú desplegable",
            example = "80800815 Lava Tap")
    private String menu;

    @JsonProperty(value = "nombreCorto")
    @ApiModelProperty(notes = "Nombre corto del proveedor", example = "Lava Tap")
    private String nombreCorto;

    @JsonProperty(value = "razonSocial")
    @ApiModelProperty(notes = "Razón social del proveedor", example = "Lava Tap SA de CV")
    private String razonSocial;

    @JsonProperty(value = "estatusProveedor")
    @ApiModelProperty(notes = "Estatus del proveedor", example = "Activo")
    private String status;

    public ProveedoresDTO() {
        super();

    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ProveedoresDTO{" + "idProveedor=" + idProveedor + ", menu="
                + menu + ", nombreCorto=" + nombreCorto + ", razonSocial=" + razonSocial
                + ", status=" + status + '}';
    }

}
