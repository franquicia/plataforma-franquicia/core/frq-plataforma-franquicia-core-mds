package com.gs.baz.pf.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.data.sources.config.GenericConfig;
import com.gs.baz.version.dto.VersionDTO;
import com.gs.baz.version.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Import(GenericConfig.class)
@Configuration
@ComponentScan("com.gs.baz.mf")
public class ConfigApp {

    private final Logger logger = LogManager.getLogger();

    public ConfigApp() {
        logger.info("Loading Base " + getClass().getName() + "...!");
        VersionDTO versionDTO = new Version().getBuildProperties();
        try {
            logger.info(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(versionDTO));
        } catch (JsonProcessingException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

}
