/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta de acceso a Proveedores", value = "ValidaProveedorRespuesta")
public class ValidaProveedorRespuesta {

    @JsonProperty(value = "accesoExitoso")
    @ApiModelProperty(notes = "Respuesta de la validación de acceso.", example = "true", position = -1)
    private Boolean accesoExitoso;

    public Boolean getAccesoExitoso() {
        return accesoExitoso;
    }

    public void setAccesoExitoso(Boolean accesoExitoso) {
        this.accesoExitoso = accesoExitoso;
    }

    @Override
    public String toString() {
        return "ValidaProveedorRespuesta{" + "accesoExitoso=" + accesoExitoso + '}';
    }

}
