package com.gs.baz.frq.modelo.negocio.comentarios.firma.init;

import com.gs.baz.frq.modelo.negocio.comentarios.firma.bi.CorreoFirmaBI;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dao.ComentarioFirmaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.comentarios.firma")
public class ConfigComentarioFirma {

    private final Logger logger = LogManager.getLogger();

    public ConfigComentarioFirma() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ComentarioFirmaDAOImpl comentarioFirmaDAOImpl() {
        return new ComentarioFirmaDAOImpl();
    }

    @Bean()
    public CorreoFirmaBI correoFirmaBI() {
        return new CorreoFirmaBI();
    }
}
