/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class RespuestaVisitaDTO {

    @JsonProperty(value = "id_dashboard")
    private Integer idDashboard;

    @JsonProperty(value = "pregunta")
    private String pregunta;

    @JsonProperty(value = "respuesta")
    private String respuesta;

    public Integer getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(BigDecimal idDashboard) {
        this.idDashboard = (idDashboard == null ? null : idDashboard.intValue());
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public String toString() {
        return "RespuestaVisitaDTO{" + "idDashboard=" + idDashboard + ", pregunta=" + pregunta + ", respuesta=" + respuesta + '}';
    }

}
