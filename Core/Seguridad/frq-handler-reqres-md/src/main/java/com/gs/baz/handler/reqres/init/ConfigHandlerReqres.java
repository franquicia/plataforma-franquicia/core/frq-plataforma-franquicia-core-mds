package com.gs.baz.handler.reqres.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.handler.reqres")
public class ConfigHandlerReqres {

    private final Logger logger = LogManager.getLogger();

    public ConfigHandlerReqres() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

}
