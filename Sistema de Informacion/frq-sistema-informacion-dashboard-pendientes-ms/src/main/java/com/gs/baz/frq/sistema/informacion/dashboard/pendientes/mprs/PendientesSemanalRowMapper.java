/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesSemanal;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PendientesSemanalRowMapper implements RowMapper<PendientesSemanal> {

    @Override
    public PendientesSemanal mapRow(ResultSet rs, int rowNum) throws SQLException {
        PendientesSemanal pendientesSemanal = new PendientesSemanal();
        pendientesSemanal.setSemana(rs.getInt("FISEMANA"));
        pendientesSemanal.setAnio(rs.getInt("FIANIO"));
        pendientesSemanal.setTotalCertificaciones(rs.getInt("TOTAL_CERTIFICACIONES"));
        return pendientesSemanal;
    }
}
