package com.gs.baz.reporte.medicion.dto.mprs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class VisitasRowMapper implements RowMapper<ObjectNode> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private ObjectNode objectNode;

    @Override
    public ObjectNode mapRow(ResultSet rs, int rowNum) throws SQLException {
        objectNode = objectMapper.createObjectNode();
        ResultSetMetaData resultSetMetaData = rs.getMetaData();
        int columns = rs.getMetaData().getColumnCount();
        for (int column = 1; column < columns; column++) {
            Object value = rs.getObject(column);
            String name = resultSetMetaData.getColumnName(column);
            if (value instanceof BigDecimal) {
                objectNode.put(name, (BigDecimal) value);
            }
            if (value instanceof String) {
                objectNode.put(name, (String) value);
            }
            if (value instanceof Short) {
                objectNode.put(name, (Short) value);
            }
            if (value instanceof Integer) {
                objectNode.put(name, (Integer) value);
            }
            if (value instanceof Long) {
                objectNode.put(name, (Long) value);
            }
            if (value instanceof Float) {
                objectNode.put(name, (Float) value);
            }
            if (value instanceof Double) {
                objectNode.put(name, (Double) value);
            }
            if (value instanceof BigInteger) {
                objectNode.put(name, (BigInteger) value);
            }
            if (value instanceof Boolean) {
                objectNode.put(name, (Boolean) value);
            }
        }
        return objectNode;
    }
}
