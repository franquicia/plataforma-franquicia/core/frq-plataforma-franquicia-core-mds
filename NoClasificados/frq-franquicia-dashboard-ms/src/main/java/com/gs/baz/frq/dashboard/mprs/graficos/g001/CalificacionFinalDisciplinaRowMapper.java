package com.gs.baz.frq.dashboard.mprs.graficos.g001;

import com.gs.baz.frq.dashboard.dto.graficos.g001.CalificacionFinalDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionFinalDisciplinaRowMapper implements RowMapper<CalificacionFinalDisciplinaDTO> {

    private CalificacionFinalDisciplinaDTO disciplinaDTO;

    @Override
    public CalificacionFinalDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        disciplinaDTO = new CalificacionFinalDisciplinaDTO();
        disciplinaDTO.setIdDisciplina((BigDecimal) rs.getObject("FIDISCIPLINAID"));
        disciplinaDTO.setCalificacionFinal((BigDecimal) rs.getObject("RESULTADO"));
        return disciplinaDTO;
    }
}
