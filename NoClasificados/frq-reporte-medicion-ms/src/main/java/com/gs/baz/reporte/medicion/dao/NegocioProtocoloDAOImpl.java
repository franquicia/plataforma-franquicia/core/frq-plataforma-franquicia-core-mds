package com.gs.baz.reporte.medicion.dao;

import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import com.gs.baz.reporte.medicion.dto.NegocioDTO;
import com.gs.baz.reporte.medicion.dto.mprs.NegocioRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class NegocioProtocoloDAOImpl extends DefaultDAO {

    private final Logger logger = LogManager.getLogger();
    private String schema;
    private DefaultJdbcCall jdbcSelectNegocioProtocolo;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectNegocioProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegocioProtocolo.withSchemaName(schema);
        jdbcSelectNegocioProtocolo.withCatalogName("PAREPORTEMEDEXT");
        jdbcSelectNegocioProtocolo.withProcedureName("SPGETNEGXPROT");
        jdbcSelectNegocioProtocolo.returningResultSet("PA_NEGOCIOS", new NegocioRowMapper());
    }

    public List<NegocioDTO> selectNegocioProtocolo(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA_INI", filtersDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHA_FIN", filtersDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());

        Map<String, Object> out = jdbcSelectNegocioProtocolo.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<NegocioDTO>) out.get("PA_NEGOCIOS");
        } else {
            return null;
        }

    }

}
