/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modulo.flujo.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.modulo.flujo.bi.ModuloFlujoBI;
import com.gs.baz.frq.modulo.flujo.dao.ModuloFlujoDAOImpl;
import com.gs.baz.frq.modulo.flujo.dto.ModuloDTO;
import com.gs.baz.frq.modulo.flujo.dto.ModuloFlujoDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModulo;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/modulo/flujo")
public class ServiceModuloFlujo {

    @Autowired
    private ModuloFlujoDAOImpl moduloFlujoDAOImpl;

    @Autowired
    private SubServiceModulo subServiceModulo;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    ModuloFlujoBI moduloFlujoBI;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloFlujoDTO> getModuloFlujo() throws CustomException {
        return moduloFlujoDAOImpl.selectRows();
    }

    /**
     *
     * @param idFlujoModulo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_flujo_modulo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloFlujoDTO getModulosFlujo(@PathVariable("id_flujo_modulo") Integer idFlujoModulo) throws CustomException {
        return moduloFlujoDAOImpl.selectRow(idFlujoModulo);
    }

    /**
     *
     * @param idFlujo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/flujo/{id_flujo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloFlujoDTO> getModulosFlujoByFlujo(@PathVariable("id_flujo") Integer idFlujo) throws CustomException {
        return moduloFlujoDAOImpl.selectRowsByFlujo(idFlujo);
    }

    /**
     *
     * @param flujoModuloDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/flujo/parent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloFlujoDTO> getModulosFlujoParent(@RequestBody ModuloFlujoDTO flujoModuloDTO) throws CustomException {
        return moduloFlujoDAOImpl.selectRowsByFlujoParent(flujoModuloDTO.getIdFlujo(), flujoModuloDTO.getIdParent());
    }

    /**
     *
     * @param idFlujo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/flujo/tree/{id_flujo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloFlujoDTO> getModulosFlujoTreeHierarchical(@RequestHeader HttpHeaders headers, @PathVariable("id_flujo") Integer idFlujo) throws CustomException {
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        subServiceModulo.init(basePath, httpEntity);
        List<ModuloFlujoDTO> rootsModulos = moduloFlujoDAOImpl.selectRowsByFlujoParent(idFlujo, null);
        List<ModuloFlujoDTO> finalModulos = new ArrayList<>();
        for (ModuloFlujoDTO moduloFlujoDTO : rootsModulos) {
            if (moduloFlujoDTO.getIdFlujoModulo().equals(moduloFlujoDTO.getIdParent())) {
                moduloFlujoDTO.setIdParent(null);
            }
            List<ModuloFlujoDTO> temp = moduloFlujoDAOImpl.selectRowsTreeHierarchicalDesc(moduloFlujoDTO.getIdFlujoModulo());
            finalModulos.addAll(temp);
        }
        for (ModuloFlujoDTO moduloFlujo : finalModulos) {
            moduloFlujo.setModuloDTO((ModuloDTO) subServiceModulo.getModulo(ModuloDTO.class, moduloFlujo.getIdModulo()));
        }
        return moduloFlujoBI.toTree(finalModulos);
    }

    /**
     *
     * @param idFlujoModulo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/flujo/tree/asc/{id_flujo_modulo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloFlujoDTO> getModulosFlujoTreeHierarchicalAsc(@PathVariable("id_flujo_modulo") Integer idFlujoModulo) throws CustomException {
        return moduloFlujoDAOImpl.selectRowsTreeHierarchicalAsc(idFlujoModulo);
    }

    /**
     *
     * @param idFlujoModulo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/flujo/tree/desc/{id_flujo_modulo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModuloFlujoDTO> getModulosFlujoTreeHierarchicalDesc(@PathVariable("id_flujo_modulo") Integer idFlujoModulo) throws CustomException {
        return moduloFlujoDAOImpl.selectRowsTreeHierarchicalDesc(idFlujoModulo);
    }

    /**
     *
     * @param flujoModuloDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloFlujoDTO postModuloFlujo(@RequestBody ModuloFlujoDTO flujoModuloDTO) throws CustomException {
        if (flujoModuloDTO.getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo"));
        }
        if (flujoModuloDTO.getIdModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modulo"));
        }
        if (flujoModuloDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return moduloFlujoDAOImpl.insertRow(flujoModuloDTO);
    }

    /**
     *
     * @param flujoModuloDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloFlujoDTO putModuloFlujo(@RequestBody ModuloFlujoDTO flujoModuloDTO) throws CustomException {
        if (flujoModuloDTO.getIdFlujoModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo_modulo"));
        }
        if (flujoModuloDTO.getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo"));
        }
        if (flujoModuloDTO.getIdModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modulo"));
        }
        if (flujoModuloDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (flujoModuloDTO.getOrden() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("orden"));
        }
        return moduloFlujoDAOImpl.updateRow(flujoModuloDTO);
    }

    /**
     *
     * @param flujoModuloDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModuloFlujoDTO deleteModuloFlujo(@RequestBody ModuloFlujoDTO flujoModuloDTO) throws CustomException {
        if (flujoModuloDTO.getIdFlujoModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo_modulo"));
        }
        return moduloFlujoDAOImpl.deleteRow(flujoModuloDTO);
    }

}
