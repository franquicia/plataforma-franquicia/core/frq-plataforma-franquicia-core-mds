/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del punto de venta aprovado por geografia", value = "PuntoVentaAprobadoGeografia")
public class PuntoVentaAprobadoGeografia extends CalificacionGeografiaBase {

    @JsonProperty(value = "porcentaje")
    @ApiModelProperty(notes = "Porcentaje", example = "1")
    private Integer porcentaje;

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public String toString() {
        return "PuntoVentaAprobadoGeografia{" + "porcentaje=" + porcentaje + '}';
    }

}
