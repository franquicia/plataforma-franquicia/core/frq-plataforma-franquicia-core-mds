/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Certificaciones por gerente", value = "CertificacionesGerente")
public class CertificacionesGerente {

    @JsonProperty(value = "numeroEmpleadoGerente")
    @ApiModelProperty(notes = "Número de empleado del Gerente", example = "84539", position = -1)
    private Integer numeroEmpleadoGerente;

    @JsonProperty(value = "nombreGerente")
    @ApiModelProperty(notes = "Nombre del Gerente", example = "JOSE ALBERTO AGUILAR MORALES", position = -2)
    private String nombreGerente;
    
    @JsonProperty(value = "territorio")
    @ApiModelProperty(notes = "Territorio al que pertenece", example = "GOLFO", position = -3)
    private String territorio;
    
    @JsonProperty(value = "totalAsignados")
    @ApiModelProperty(notes = "Total de puntos de venta asignados", example = "49", position = -4)
    private Integer totalAsignados;
    
    @JsonProperty(value = "totalPendientes")
    @ApiModelProperty(notes = "Total de puntos de venta pendientes", example = "49", position = -5)
    private Integer totalPendientes;

    public Integer getNumeroEmpleadoGerente() {
        return numeroEmpleadoGerente;
    }

    public void setNumeroEmpleadoGerente(Integer numeroEmpleadoGerente) {
        this.numeroEmpleadoGerente = numeroEmpleadoGerente;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public Integer getTotalAsignados() {
        return totalAsignados;
    }

    public void setTotalAsignados(Integer totalAsignados) {
        this.totalAsignados = totalAsignados;
    }

    public Integer getTotalPendientes() {
        return totalPendientes;
    }

    public void setTotalPendientes(Integer totalPendientes) {
        this.totalPendientes = totalPendientes;
    }

    @Override
    public String toString() {
        return "CertificacionesGerente{" + "numeroEmpleadoGerente=" + numeroEmpleadoGerente + ", nombreGerente=" + nombreGerente + ", territorio=" + territorio + ", totalAsignados=" + totalAsignados + ", totalPendientes=" + totalPendientes + '}';
    }

}
