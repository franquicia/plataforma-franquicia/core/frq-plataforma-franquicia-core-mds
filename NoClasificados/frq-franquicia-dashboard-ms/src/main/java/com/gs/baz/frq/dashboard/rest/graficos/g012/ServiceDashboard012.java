/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g012;

import com.gs.baz.frq.dashboard.dao.graficos.g012.DashboardDAOImpl012;
import com.gs.baz.frq.dashboard.dto.graficos.g012.IncumplimientosImperdonablesDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard012 {

    @Autowired
    private DashboardDAOImpl012 dashboardDAOImpl012;

    @RequestMapping(value = "secure/grafico/get/incumplimiento/imperdonable/negocios/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public IncumplimientosImperdonablesDTO getIncumplimientosImperdonablesDTO(@RequestBody IncumplimientosImperdonablesDTO incumplimientosImperdonablesDTO) throws CustomException {
        return dashboardDAOImpl012.selectIncumplimientosImperdonables(incumplimientosImperdonablesDTO);
    }
}
