package com.gs.baz.frq.asignacion.mprs;

import com.gs.baz.frq.asignacion.dto.AsignacionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class AsignacionCalendarioRowMapper implements RowMapper<AsignacionDTO> {

    private AsignacionDTO status;

    @Override
    public AsignacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionDTO();
        status.setCeco(rs.getString("NOMB_CECO"));
        status.setIdCeco(rs.getString("FCID_CECO"));
        status.setIdFase(rs.getInt("FIID_FASE"));
        status.setOrdenFase(rs.getInt("FIID_ORDENFASE"));
        status.setNombreFase(rs.getString("NOMBRE_FASE"));
        status.setProyecto((BigDecimal) rs.getObject("FIID_PROYECTO"));
        status.setNombreProyecto(rs.getString("NOMBRE_PROYECTO"));
        status.setNegocio(rs.getString("NEGOCIO"));
        status.setIdNegocio(rs.getInt("IDNEGOCIO"));
        //status.setTipoProyecto(rs.getInt("FIIDTIPOPROY"));
        // status.setFaseActual(rs.getInt("FIID_FASEACT"));
        //status.setStatus(rs.getInt("FCSTATUS"));
        status.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
        //status.setResponsableAsigno(rs.getInt("RESPONSABLE_ASIGNO"));
        status.setIdUsuario(rs.getInt("FIID_USUARIO"));
        status.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
        //status.setObsAsignacion(rs.getString("OBS_ASIGNA"));
        status.setFechaTermino(rs.getString("PERIODO"));
        status.setSemana(rs.getInt("SEMANA"));

        return status;
    }
}
