/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dao;

/**
 *
 * @author jfernandor
 */
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.usuario.dto.FlujoPerfilUsuarioDTO;
import com.gs.baz.usuario.dto.PerfilUsuarioDTO;
import com.gs.baz.usuario.mprs.PerfilUsuarioRowMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class PerfilUsuarioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectFinalPerfilesUsuario;
    private DefaultJdbcCall jdbcSelectAvailable;
    private DefaultJdbcCall jdbcSelectAssociated;
    private DefaultJdbcCall jdbcSelectFixed;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    @Autowired
    private FlujoPerfilUsuarioDAOImpl flujoPerfilUsuarioDAOImpl;

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAPERF_USU");
        jdbcInsert.withProcedureName("SPINSAPERF_USUA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAPERF_USU");
        jdbcSelect.withProcedureName("SPGETAPERF_USUA");
        jdbcSelect.returningResultSet("PA_CDATOS", new PerfilUsuarioRowMapper());

        jdbcSelectFinalPerfilesUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFinalPerfilesUsuario.withSchemaName(schema);
        jdbcSelectFinalPerfilesUsuario.withCatalogName("PAADMAPERF_USU");
        jdbcSelectFinalPerfilesUsuario.withProcedureName("SPGETPERFILUSU");
        jdbcSelectFinalPerfilesUsuario.returningResultSet("PA_CDATOS", new PerfilUsuarioRowMapper());

        jdbcSelectAvailable = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAvailable.withSchemaName(schema);
        jdbcSelectAvailable.withCatalogName("PAADMAPERF_USU");
        jdbcSelectAvailable.withProcedureName("SPGETPERFUSUDIS");
        jdbcSelectAvailable.returningResultSet("PA_CDATOS", new PerfilUsuarioRowMapper());

        jdbcSelectAssociated = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAssociated.withSchemaName(schema);
        jdbcSelectAssociated.withCatalogName("PAADMAPERF_USU");
        jdbcSelectAssociated.withProcedureName("SPGETPERUSUASIG");
        jdbcSelectAssociated.returningResultSet("PA_CDATOS", new PerfilUsuarioRowMapper());

        jdbcSelectFixed = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFixed.withSchemaName(schema);
        jdbcSelectFixed.withCatalogName("PAADMAPERF_USU");
        jdbcSelectFixed.withProcedureName("SPGETPERFIJOS");
        jdbcSelectFixed.returningResultSet("PA_CDATOS", new PerfilUsuarioRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAPERF_USU");
        jdbcUpdate.withProcedureName("SPACTAPERF_USUA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAPERF_USU");
        jdbcDelete.withProcedureName("SPDELAPERF_USUA");
    }

    public FlujoPerfilUsuarioDAOImpl getFlujoPerfilUsuarioDAOImpl() {
        return flujoPerfilUsuarioDAOImpl;
    }

    public PerfilUsuarioDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPERFIL_USR", entityID);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PerfilUsuarioDTO> data = (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<PerfilUsuarioDTO> selectRows() throws CustomException {
        List<PerfilUsuarioDTO> lista;
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPERFIL_USR", null);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        lista = (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public List<PerfilUsuarioDTO> selectRowsByEmpleado(Integer empleadoID) throws CustomException {
        List<PerfilUsuarioDTO> lista;
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPERFIL_USR", null);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", empleadoID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        lista = (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public List<PerfilUsuarioDTO> selectRowsFinalByEmpleado(Integer empleadoID) throws CustomException {
        List<PerfilUsuarioDTO> lista;
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", empleadoID);
        Map<String, Object> out = jdbcSelectFinalPerfilesUsuario.execute(mapSqlParameterSource);
        lista = (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public List<PerfilUsuarioDTO> selectRowsDeepByEmpleado(Integer empleadoID) throws CustomException {
        List<PerfilUsuarioDTO> perfilUsuarioDTOs = this.selectRowsFinalByEmpleado(empleadoID);
        for (PerfilUsuarioDTO perfilUsuarioDTO : perfilUsuarioDTOs) {
            List<FlujoPerfilUsuarioDTO> flujoUsuarioDTOs = flujoPerfilUsuarioDAOImpl.selectRowsDeepFinalByUsuarioPerfil(empleadoID, perfilUsuarioDTO.getPerfilDTO().getIdPerfil());
            perfilUsuarioDTO.setFlujoUsuarioDTOs(flujoUsuarioDTOs);
        }
        return perfilUsuarioDTOs;
    }

    public List<PerfilUsuarioDTO> selectPerfilesByUsuarioAvailable(Integer usuarioID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        Map<String, Object> out = jdbcSelectAvailable.execute(mapSqlParameterSource);
        return (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<PerfilUsuarioDTO> selectPerfilesByUsuarioAssociated(Integer usuarioID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        Map<String, Object> out = jdbcSelectAssociated.execute(mapSqlParameterSource);
        return (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<PerfilUsuarioDTO> selectPerfilesByUsuarioFixed(Integer usuarioID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        Map<String, Object> out = jdbcSelectFixed.execute(mapSqlParameterSource);
        return (List<PerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

//    @Transactional(rollbackFor = {CustomException.class})
    public List<PerfilUsuarioDTO> pushPerfiles(Integer empleadoID, List<PerfilUsuarioDTO> newPerfilUsuarioDTOs) throws CustomException {
        List<Integer> oldKeys = new ArrayList<>();
        List<Integer> newKeys = new ArrayList<>();
        List<PerfilUsuarioDTO> current = this.selectRowsByEmpleado(empleadoID);
        current.forEach(item -> {
            if (item.getPersonalizado() == null) {
                oldKeys.add(item.getPerfilDTO().getIdPerfil());
            } else {
                if (item.getPersonalizado() == 0) {
                    oldKeys.add(item.getPerfilDTO().getIdPerfil());
                }
            }
        });
        newPerfilUsuarioDTOs.forEach(item -> {
            newKeys.add(item.getPerfilDTO().getIdPerfil());
        });
        List<Integer> toDeletes = oldKeys.stream().filter(aObject -> !newKeys.contains(aObject)).collect(Collectors.toList());
        List<Integer> toInserts = newKeys.stream().filter(aObject -> !oldKeys.contains(aObject)).collect(Collectors.toList());
        if (current.isEmpty()) {
            for (PerfilUsuarioDTO itemPerfil : newPerfilUsuarioDTOs) {
                itemPerfil.setIdEmpleado(new BigDecimal(empleadoID));
                this.insertRow(itemPerfil);
            }
        } else {
            for (Integer idPerfil : toDeletes) {
                PerfilUsuarioDTO perfilUsuarioDTO = current.stream().filter(item -> item.getPerfilDTO().getIdPerfil().equals(idPerfil)).findFirst().orElse(null);
                this.deleteRow(perfilUsuarioDTO);
            }
            for (Integer idPerfil : toInserts) {
                PerfilUsuarioDTO perfilUsuarioDTO = newPerfilUsuarioDTOs.stream().filter(item -> item.getPerfilDTO().getIdPerfil().equals(idPerfil)).findFirst().orElse(null);
                perfilUsuarioDTO.setPersonalizado(new BigDecimal(0));
                perfilUsuarioDTO.setIdEmpleado(new BigDecimal(empleadoID));
                this.insertRow(perfilUsuarioDTO);
            }
        }
        return this.selectRowsByEmpleado(empleadoID);
    }

    public PerfilUsuarioDTO insertRow(PerfilUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPERFIL_USR", entityDTO.getIdPerfilUsuario());
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdEmpleado());
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getPerfilDTO().getIdPerfil());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FIPERSONALIZADO", entityDTO.getPersonalizado());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Perfil Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Perfil Usuarioo"), ex);
        }
    }

    public PerfilUsuarioDTO updateRow(PerfilUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPERFIL_USR", entityDTO.getIdPerfilUsuario());
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdEmpleado());
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getPerfilDTO().getIdPerfil());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Perfil Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Perfil Usuario"), ex);
        }
    }

    public PerfilUsuarioDTO deleteRow(PerfilUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPERFIL_USR", entityDTO.getIdPerfilUsuario());
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Perfil Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Perfil Usuario"), ex);
        }
    }

}
