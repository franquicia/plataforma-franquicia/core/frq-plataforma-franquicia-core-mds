/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folio.etiquetas.mprs;

import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.FolioEtiquetaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FolioEtiquetaRowMapper implements RowMapper<FolioEtiquetaBase> {

    private FolioEtiquetaBase folioEtiquetaBase;

    @Override
    public FolioEtiquetaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        folioEtiquetaBase = new FolioEtiquetaBase();
        folioEtiquetaBase.setIdEtiqueta(rs.getInt("FIIDETIQUETA"));
        return folioEtiquetaBase;
    }
}
