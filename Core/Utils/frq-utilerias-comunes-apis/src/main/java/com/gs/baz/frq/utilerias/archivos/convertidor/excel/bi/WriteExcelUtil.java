/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.excel.bi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto.Cabezera;
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto.ParametrosPlantillaExcel;
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto.Contenido;
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto.Hoja;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StopWatch;

/**
 *
 * @author cescobarh
 */
public class WriteExcelUtil {

    private int indexColumn;
    private Sheet sheet;
    private Row row;
    private Cell cell;
    private JsonNode jsonNodeRows;
    private final Logger logger = LogManager.getLogger();
    private StopWatch stopWatch;

    private ParametrosPlantillaExcel preparePlantillaExcel(ParametrosPlantillaExcel plantillaExcel) {
        if (plantillaExcel.getHojas() != null && plantillaExcel.getHojas().size() > 0) {
            List<Hoja> hojas = plantillaExcel.getHojas();
            int indexSheet = 0;
            for (Hoja itemSheet : hojas) {
                indexSheet++;
                if (itemSheet.getDescripcion() == null) {
                    itemSheet.setDescripcion("Hoja " + indexSheet);
                }
                Contenido contenido = itemSheet.getContenido();
                if (contenido.getCabezeras() == null) {
                    List<ObjectNode> registros = contenido.getRegistros();
                    if (registros.size() > 0) {
                        List<Cabezera> cabezeras = new ArrayList<>();
                        ObjectNode firstRow = registros.get(0);
                        indexColumn = 0;
                        firstRow.fieldNames().forEachRemaining(column -> {
                            Cabezera cabezera = new Cabezera();
                            cabezera.setDescripcion(column);
                            cabezera.setValor(column);
                            cabezera.setIndex(indexColumn);
                            cabezeras.add(cabezera);
                            indexColumn++;
                        });
                        contenido.setCabezeras(cabezeras);
                    }
                } else if (contenido.getCabezeras().isEmpty()) {
                    List<ObjectNode> registros = contenido.getRegistros();
                    if (registros.size() > 0) {
                        List<Cabezera> cabezeras = new ArrayList<>();
                        ObjectNode firstRow = registros.get(0);
                        indexColumn = 0;
                        firstRow.fieldNames().forEachRemaining(column -> {
                            Cabezera cabezera = new Cabezera();
                            cabezera.setDescripcion(column);
                            cabezera.setValor(column);
                            cabezera.setIndex(indexColumn);
                            cabezeras.add(cabezera);
                            indexColumn++;
                        });
                        contenido.setCabezeras(cabezeras);
                    }
                } else {
                    indexColumn = 0;
                    contenido.getCabezeras().forEach(item -> {
                        item.setIndex(indexColumn);
                        indexColumn++;
                    });
                }
            }
        }
        return plantillaExcel;
    }

    public Resource writeExcel(JsonNode jsonNode) throws Exception {
        ParametrosPlantillaExcel plantillaExcel = new ParametrosPlantillaExcel();
        jsonNode.fields().forEachRemaining(item -> {
            if (jsonNode.get(item.getKey()).isArray()) {
                Hoja hoja = new Hoja(item.getKey());
                Contenido contenido = new Contenido();
                jsonNodeRows = item.getValue();
                if (jsonNodeRows != null && jsonNodeRows.size() > 0) {
                    ObjectNode cabezera = (ObjectNode) jsonNodeRows.get(0);
                    indexColumn = 0;
                    cabezera.fieldNames().forEachRemaining(itemProperty -> {
                        Cabezera columnaEncabezado = new Cabezera(itemProperty);
                        columnaEncabezado.setValor(itemProperty);
                        columnaEncabezado.setIndex(indexColumn);
                        contenido.addCabezera(columnaEncabezado);
                        indexColumn++;
                    });
                    jsonNodeRows.forEach(itemRow -> {
                        contenido.addRegistro((ObjectNode) itemRow);
                    });
                }
                hoja.setContenido(contenido);
                plantillaExcel.addHoja(hoja);
            }
        });
        return this.processWriteExcel(plantillaExcel);
    }

    public Resource writeExcel(ParametrosPlantillaExcel plantillaExcel) throws Exception {
        return this.processWriteExcel(plantillaExcel);
    }

    private Resource processWriteExcel(ParametrosPlantillaExcel plantillaExcel) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            stopWatch = new StopWatch("WriteExcelUtil");
            if (stopWatch.isRunning()) {
                stopWatch.stop();
            }
            stopWatch.start("Preparando Hojas y Columnas Iniciando");
            plantillaExcel = this.preparePlantillaExcel(plantillaExcel);
            List<Hoja> hojas = plantillaExcel.getHojas();
            if (hojas != null && hojas.size() > 0) {
                hojas.forEach(itemSheet -> {
                    sheet = workbook.createSheet(itemSheet.getDescripcion());
                    List<Cabezera> cabezeras = itemSheet.getContenido().getCabezeras();
                    if (cabezeras != null && cabezeras.size() > 0) {
                        indexColumn = 0;
                        row = sheet.createRow(0);
                        cabezeras.forEach(itemColumn -> {
                            cell = row.createCell(indexColumn);
                            cell.setCellValue(itemColumn.getDescripcion());
                            indexColumn++;
                        });
                    }
                });
                stopWatch.stop();
                int indexSheet = 0;
                for (Hoja itemSheet : hojas) {
                    stopWatch.start("Llenado de datos en Hoja " + indexSheet);
                    sheet = workbook.getSheetAt(indexSheet);
                    List<ObjectNode> registros = itemSheet.getContenido().getRegistros();
                    if (registros != null && registros.size() > 0) {
                        int indexRow = 1;
                        Contenido contenido = itemSheet.getContenido();
                        for (ObjectNode itemRow : registros) {
                            row = sheet.createRow(indexRow);
                            itemRow.fieldNames().forEachRemaining(itemProperty -> {
                                Integer indexColumn = contenido.getIndexCabezeraByValue(itemProperty);
                                if (indexColumn != null) {
                                    cell = row.createCell(indexColumn);
                                    cell.setCellValue(itemRow.get(itemProperty).asText());
                                }
                            });
                            indexRow++;
                        }
                    }
                    indexSheet++;
                    stopWatch.stop();
                }
                workbook.write(outputStream);
                outputStream.close();
            }
        } catch (IOException ex) {
            throw new Exception(ex);
        }
        if (stopWatch.isRunning()) {
            stopWatch.stop();
        }
        stopWatch.start("Conversion");
        Resource resource = new ByteArrayResource(outputStream.toByteArray());
        stopWatch.stop();
        printLogInfo();
        return resource;
    }

    private void printLogInfo() {
        StringBuilder infoTask = new StringBuilder();
        infoTask.append("\nStopWatch '").append(stopWatch.getId()).append("': running time = ").append(stopWatch.getTotalTimeSeconds()).append(" seconds \n");
        infoTask.append("---------------------------------\n");
        infoTask.append("  Seconds    |     Task Name     \n");
        infoTask.append("---------------------------------\n");
        for (StopWatch.TaskInfo itemTask : stopWatch.getTaskInfo()) {
            infoTask.append(itemTask.getTimeSeconds()).append("  |  ").append(itemTask.getTaskName()).append("\n");
        }
        infoTask.append("---------------------------------\n");
        logger.log(Level.INFO, infoTask);
    }
}
