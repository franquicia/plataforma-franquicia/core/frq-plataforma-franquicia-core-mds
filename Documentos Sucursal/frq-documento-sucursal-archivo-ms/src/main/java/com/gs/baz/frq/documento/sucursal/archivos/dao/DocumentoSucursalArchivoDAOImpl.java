/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.dao;

import com.gs.baz.frq.documento.sucursal.archivos.dto.AltaArchivoVirtual;
import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoCeco;
import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoVersionVirtual;
import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoVirtual;
import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoVirtualBase;
import com.gs.baz.frq.documento.sucursal.archivos.dto.ParametrosArchivoVirtual;
import com.gs.baz.frq.documento.sucursal.archivos.mprs.ArchivoCecoRowMapper;
import com.gs.baz.frq.documento.sucursal.archivos.mprs.ArchivoRowMapper;
import com.gs.baz.frq.documento.sucursal.archivos.mprs.ArchivosRowMapper;
import com.gs.baz.frq.documento.sucursal.archivos.mprs.ArchivosVersionRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalArchivoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectByFolioDoc;
    private DefaultJdbcCall jdbcSelectVersiones;

    private DefaultJdbcCall jdbcSelectArchivoCeco;

    private DefaultJdbcCall jdbcDelete;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PA_ARCHIVODOC";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_ARCH2");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_ARCHFRON");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_ARCH");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SP_SEL_ARCH");
        jdbcSelect.returningResultSet("RCL_ARCH", new ArchivoRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_ARCH");
        jdbcSelectAll.returningResultSet("RCL_ARCH", new ArchivosRowMapper());

        String consultaEspecificas = "PA_SOPDOCS";

        jdbcSelectArchivoCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectArchivoCeco.withSchemaName(schema);
        jdbcSelectArchivoCeco.withCatalogName(consultaEspecificas);
        jdbcSelectArchivoCeco.withProcedureName("SP_SEL_ARCHIVO");
        jdbcSelectArchivoCeco.returningResultSet("RCL_FICHA", new ArchivoCecoRowMapper());

        jdbcSelectByFolioDoc = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByFolioDoc.withSchemaName(schema);
        jdbcSelectByFolioDoc.withCatalogName(consultaEspecificas);
        jdbcSelectByFolioDoc.withProcedureName("SP_SEL_ARCHDOC");
        jdbcSelectByFolioDoc.returningResultSet("RCL_FICHA", new ArchivosRowMapper());

        jdbcSelectVersiones = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectVersiones.withSchemaName(schema);
        jdbcSelectVersiones.withCatalogName("PA_SOPDOCS");
        jdbcSelectVersiones.withProcedureName("SP_SEL_VERS");
        jdbcSelectVersiones.returningResultSet("RCL_FICHA", new ArchivosVersionRowMapper());

    }

    public List<ArchivoVirtual> selectRow(Long idDocumento, Long idFolio) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_IDDOC", idDocumento);

        Map<String, Object> out = jdbcSelectByFolioDoc.execute(mapSqlParameterSource);

        List<ArchivoVirtual> data = (List<ArchivoVirtual>) out.get("RCL_FICHA");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ArchivoVersionVirtual> selectVersions(Long idArchivo) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDARCHIVO", idArchivo);

        Map<String, Object> out = jdbcSelectVersiones.execute(mapSqlParameterSource);

        List<ArchivoVersionVirtual> data = (List<ArchivoVersionVirtual>) out.get("RCL_FICHA");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public ArchivoVirtualBase selectRow(Long idArchivo) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDARCH", idArchivo);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ArchivoVirtualBase> data = (List<ArchivoVirtualBase>) out.get("RCL_ARCH");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<ArchivoVirtual> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDARCH", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<ArchivoVirtual>) out.get("RCL_ARCH");
    }

    public AltaArchivoVirtual insertRow(ParametrosArchivoVirtual entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            //Validar si viene padre o no, Se arega version del archivo
            mapSqlParameterSource.addValue("PA_ARCHPAD", entityDTO.getIdArchivoPadre());
            mapSqlParameterSource.addValue("PA_RUTA", entityDTO.getUid());
            mapSqlParameterSource.addValue("PA_DESC", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_NOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_EXTENSION", entityDTO.getExtension());
            mapSqlParameterSource.addValue("PA_TIPO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_INDICE", entityDTO.getIndice());
            mapSqlParameterSource.addValue("PA_PESO", entityDTO.getPeso());
            mapSqlParameterSource.addValue("PA_CONTENT", entityDTO.getTipoContenido());
            mapSqlParameterSource.addValue("PA_FOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_IDDOC", null);
            mapSqlParameterSource.addValue("PA_IDUSU", entityDTO.getUsuario());
            mapSqlParameterSource.addValue("PA_NEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_VERSION", entityDTO.getIdVersion() == null ? 1 : entityDTO.getIdVersion());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -2) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_IDARCH");
                return new AltaArchivoVirtual(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosArchivoVirtual entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDARCH", entityDTO.getIdArchivo());
            mapSqlParameterSource.addValue("PA_ARCHPAD", entityDTO.getIdArchivoPadre());
            mapSqlParameterSource.addValue("PA_RUTA", entityDTO.getUid());
            mapSqlParameterSource.addValue("PA_DESC", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_NOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_EXTENSION", entityDTO.getExtension());
            mapSqlParameterSource.addValue("PA_TIPO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_INDICE", entityDTO.getIndice());
            mapSqlParameterSource.addValue("PA_PESO", entityDTO.getPeso());
            mapSqlParameterSource.addValue("PA_CONTENT", entityDTO.getTipoContenido());
            mapSqlParameterSource.addValue("PA_FOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_ELIMINADO", 0);
            mapSqlParameterSource.addValue("PA_IDUSU", entityDTO.getUsuario());
            mapSqlParameterSource.addValue("PA_IDUSUMODI", entityDTO.getUsuario());
            mapSqlParameterSource.addValue("PA_IDDOC", null);
            mapSqlParameterSource.addValue("PA_NEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_IDVERSION", entityDTO.getIdVersion() == null ? 1 : entityDTO.getIdVersion());

            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDARCH", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<ArchivoCeco> selectArchivo(String ceco) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", ceco);
        Map<String, Object> out = jdbcSelectArchivoCeco.execute(mapSqlParameterSource);
        List<ArchivoCeco> data = (List<ArchivoCeco>) out.get("RCL_FICHA");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }

    }

}
