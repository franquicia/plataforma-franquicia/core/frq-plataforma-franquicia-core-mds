/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.Datozona;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DatosZonaRowMapper implements RowMapper<Datozona> {

    private Datozona datozona;

    @Override
    public Datozona mapRow(ResultSet rs, int rowNum) throws SQLException {
        datozona = new Datozona();
        datozona.setIdDatoZona(rs.getInt("FIIDDATOSZONA"));
        datozona.setIdCatalogoZona(rs.getInt("FIIDLYZONAS"));
        datozona.setIdFolio(rs.getInt("FIID_FOLIO"));
        datozona.setObservaciones(rs.getString("FCOBSERVACIONES"));
        datozona.setValor(rs.getString("FCVALOR"));

        return datozona;
    }
}
