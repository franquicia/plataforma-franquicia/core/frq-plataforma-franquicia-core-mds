package com.gs.baz.pf.init;

import com.gs.baz.access.log.expose.EnableAccessLog;
import com.gs.baz.actuator.expose.EnableActuator;
import com.gs.baz.autorization.server.expose.EnableAuthorizationServer;
import com.gs.baz.categoria.expose.EnableCategoria;
import com.gs.baz.ceco.expose.EnableCeco;
import com.gs.baz.colorimetria.expose.EnableColorimetria;
import com.gs.baz.disciplina.expose.EnableDisciplina;
import com.gs.baz.disciplinas.eje.expose.EnableDisciplinasEje;
import com.gs.baz.disciplinas.negocios.expose.EnableDisciplinasNegocios;
import com.gs.baz.eje.expose.EnableEje;
import com.gs.baz.ejecuta.expose.EnableEjecuta;
import com.gs.baz.ejes.modelo.expose.EnableEjesModelo;
import com.gs.baz.estatus.usuario.expose.EnableEstatusUsuario;
import com.gs.baz.file.expose.EnableFile;
import com.gs.baz.formato.expose.EnableFormato;
import com.gs.baz.franquicia.expose.EnableFranquicia;
import com.gs.baz.frecuencia.medicion.expose.EnableFrecuenciaMedicion;
import com.gs.baz.frq.accesos.sistema.perfiles.expose.EnableAccesosSistemaPerfiles;
import com.gs.baz.frq.accesos.sistema.rutas.expose.EnableAccesosSistemaRutas;
import com.gs.baz.frq.accesos.sistema.usuarios.expose.EnableAccesosSitemaUsuarios;
import com.gs.baz.frq.accesos.sistema.vistas.expose.EnableAccesosSistemaVistas;
import com.gs.baz.frq.api.portal.cautivo.expose.EnableAPIPortalCautivo;
import com.gs.baz.frq.archivos.expose.EnableArchivos;
import com.gs.baz.frq.asignacion.certificacion.calidad.expose.EnableAsignacionCertiCalidad;
import com.gs.baz.frq.asignacion.expose.EnableAsignacion;
import com.gs.baz.frq.asignacion.mtto.expose.EnableAsignacionMtto;
import com.gs.baz.frq.asignacion.sup.expose.EnableAsignacionSup;
import com.gs.baz.frq.calificacion.disciplina.expose.EnableCalificacionDisciplina;
import com.gs.baz.frq.ceco.base.expose.EnableCecoBase;
import com.gs.baz.frq.cecos.mtto.expose.EnableCecosMtto;
import com.gs.baz.frq.configuracion.disciplina.expose.EnableConfiguracionDisciplina;
import com.gs.baz.frq.cumplimiento.dashboard.expose.EnableCumplimientoDashboard;
import com.gs.baz.frq.dashboard.certificacion.expose.EnableDashboardCertificacion;
import com.gs.baz.frq.dashboard.expose.EnableDashboard;
import com.gs.baz.frq.depuracion.expose.EnableDepuracion;
import com.gs.baz.frq.distribucion.dashboard.expose.EnableDistDashboard;
import com.gs.baz.frq.documento.sucursal.archivos.expose.EnableDocumentoSucursalArchivo;
import com.gs.baz.frq.documento.sucursal.cecos.expose.EnableDocumentoSucursalCeco;
import com.gs.baz.frq.documento.sucursal.documento.archivos.expose.EnableDocumentoSucursalDocumentoArchivo;
import com.gs.baz.frq.documento.sucursal.documentos.expose.EnableDocumentoSucursalDocumento;
import com.gs.baz.frq.documento.sucursal.formatos.expose.EnableDocumentoSucursalFormato;
import com.gs.baz.frq.documento.sucursal.prefijos.expose.EnablePrefijo;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.expose.EnableDocumentoSucursalProyectoDocumento;
import com.gs.baz.frq.documento.sucursal.proyectos.expose.EnableDocumentoSucursalProyecto;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.expose.EnableDocumentoSucursalCatalogoZonas;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.expose.EnableDocumentoSucursalCecoProyectoFolio;
import com.gs.baz.frq.documentos.sucursal.datos.zona.expose.EnableDocumentoSucursalDatosZona;
import com.gs.baz.frq.documentos.sucursal.fichas.expose.EnableDocumentoSucursalFicha;
import com.gs.baz.frq.documentos.sucursal.folio.expose.EnableDocumentoSucursalFolio;
import com.gs.baz.frq.documentos.sucursal.negocio.zona.expose.EnableDocumentoSucursalNegocioZonas;
import com.gs.baz.frq.documentos.sucursal.negocios.expose.EnableDocumentoSucursalNegocio;
import com.gs.baz.frq.documentos.sucursal.tipos.informacion.expose.EnableDocumentoSucursalTipoInformacion;
import com.gs.baz.frq.envia.correo.expose.EnableEnvioCorreo;
import com.gs.baz.frq.excel.expose.EnableExcel;
import com.gs.baz.frq.flujo.expose.EnableFlujo;
import com.gs.baz.frq.flujo.perfil.expose.EnableFlujoPerfil;
import com.gs.baz.frq.geografia.mtto.expose.EnableGeografiaMtto;
import com.gs.baz.frq.imperdonable.dashboard.expose.EnableImperdonableDashboard;
import com.gs.baz.frq.modelo.negocio.acceso.expose.EnableModeloNegocioAcceso;
import com.gs.baz.frq.modelo.negocio.arboles.expose.EnableModeloNegocioArbol;
import com.gs.baz.frq.modelo.negocio.archivos.expose.EnableModeloNegocioArchivo;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.expose.EnableModeloNegocioComentarioFirma;
import com.gs.baz.frq.modelo.negocio.documentos.expose.EnableModeloNegocioDocumento;
import com.gs.baz.frq.modelo.negocio.estados.documento.expose.EnableModeloNegocioEstadoDocumento;
import com.gs.baz.frq.modelo.negocio.estados.firma.expose.EnableModeloNegocioEstadoFirma;
import com.gs.baz.frq.modelo.negocio.etiqueta.expose.EnableModeloNegocioEtiqueta;
import com.gs.baz.frq.modelo.negocio.firmas.expose.EnableModeloNegocioFirma;
import com.gs.baz.frq.modelo.negocio.folio.etiquetas.expose.EnableModeloNegocioFolioEtiqueta;
import com.gs.baz.frq.modelo.negocio.folios.expose.EnableModeloNegocioFolio;
import com.gs.baz.frq.modelo.negocio.modelos.expose.EnableModeloNegocioModelo;
import com.gs.baz.frq.modelo.negocio.niveles.expose.EnableModeloNegocioNivel;
import com.gs.baz.frq.modelo.negocio.paises.expose.EnableModeloNegocioPais;
import com.gs.baz.frq.modelo.negocio.rutas.expose.EnableModeloNegocioRuta;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.expose.EnableModeloNegocioUnidadNegocio;
import com.gs.baz.frq.modelo.negocio.usuarios.expose.EnableModeloNegocioUsuario;
import com.gs.baz.frq.modulo.expose.EnableModulo;
import com.gs.baz.frq.modulo.flujo.expose.EnableModuloFlujo;
import com.gs.baz.frq.negocio.version.expose.EnableNegocioVersion;
import com.gs.baz.frq.poblacion.expose.EnablePoblacion;
import com.gs.baz.frq.pregunta.visita.expose.EnablePreguntaVisita;
import com.gs.baz.frq.punto.contacto.protocolo.expose.EnablePuntoContactoProtocolo;
import com.gs.baz.frq.rangos.expose.EnableRangos;
import com.gs.baz.frq.regla7s.expose.EnableRegla7S;
import com.gs.baz.frq.reglas7s.preguntas.expose.EnableReglas7sPreguntas;
import com.gs.baz.frq.relacion.protocolo.checklist.expose.EnableRelacionProtocoloChecklist;
import com.gs.baz.frq.reportes.layout.expose.EnableReportesLayout;
import com.gs.baz.frq.respuesta.pregunta.visita.expose.EnableRespuestaPreguntaVisita;
import com.gs.baz.frq.respuesta.reporte.medicion.expose.EnableRespuestaReporteMedicion;
import com.gs.baz.frq.respuesta.visita.expose.EnableRespuestaVisita;
import com.gs.baz.frq.seccion.cumplimiento.expose.EnableSeccionCumplimiento;
import com.gs.baz.frq.secciones.protocolo.expose.EnableSeccionesProtocolo;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.expose.EnableSistemaInformacionDashboardGrafica;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.expose.EnableSistemaInformacionDashboardReportes;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.expose.EnableSistemaInformacionDashboardSemaforo;
import com.gs.baz.frq.swagger.expose.EnableSwaggerAPI;
import com.gs.baz.frq.tipo.formato.expose.EnableTipoFormato;
import com.gs.baz.frq.ubicacion.sucursal.expose.EnableUbicacionSucursal;
import com.gs.baz.frq.utilerias.expose.EnableUtilerias;
import com.gs.baz.frq.validador.expose.EnableValidador;
import com.gs.baz.frq.visita.punto.contacto.expose.EnableVisitaPuntoContacto;
import com.gs.baz.handler.reqres.expose.EnableHandlerReqres;
import com.gs.baz.indicador.expose.EnableIndicador;
import com.gs.baz.json.converter.expose.EnableJsonConverter;
import com.gs.baz.modelos.franquicia.expose.EnableModelosFranquicia;
import com.gs.baz.negocio.expose.EnableNegocio;
import com.gs.baz.nivel.consulta.expose.EnableNivelConsulta;
import com.gs.baz.perfil.expose.EnablePerfil;
import com.gs.baz.propiedades.expose.EnablePropiedades;
import com.gs.baz.protocolo.expose.EnableProtocolo;
import com.gs.baz.punto.contacto.expose.EnablePuntoContacto;
import com.gs.baz.reporte.medicion.expose.EnableReporteMedicion;
import com.gs.baz.resource.server.expose.EnableResourceServer;
import com.gs.baz.security.access.expose.EnableSecurityAccess;
import com.gs.baz.semaforo.expose.EnableSemaforo;
import com.gs.baz.server.logs.expose.EnableServerLogs;
import com.gs.baz.status.expose.EnableStatus;
import com.gs.baz.tipo.medicion.expose.EnableTipoMedicion;
import com.gs.baz.track.time.expose.EnableTrackTime;
import com.gs.baz.usuario.expose.EnableUsuario;
import com.gs.baz.version.expose.EnableVersion;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@SpringBootApplication
@Import({ConfigApp.class})
@EnableArchivos
@EnableHandlerReqres
@EnableUsuario
@EnableValidador
@EnableUtilerias
@EnableTrackTime
@EnableAccessLog
@EnableServerLogs
@EnableSecurityAccess
@EnableResourceServer
@EnableAuthorizationServer
@EnableSwaggerAPI
@EnableActuator
@EnableSistemaInformacionDashboardReportes
@EnableSistemaInformacionDashboardGrafica
@EnableSistemaInformacionDashboardSemaforo
@EnableAccesosSistemaPerfiles
@EnableAccesosSitemaUsuarios
@EnableAccesosSistemaRutas
@EnableAccesosSistemaVistas
@EnableModeloNegocioAcceso
@EnableModeloNegocioEtiqueta
@EnableModeloNegocioEstadoDocumento
@EnableModeloNegocioModelo
@EnableModeloNegocioNivel
@EnableModeloNegocioPais
@EnableModeloNegocioComentarioFirma
@EnableModeloNegocioArbol
@EnableModeloNegocioArchivo
@EnableModeloNegocioEstadoFirma
@EnableModeloNegocioFirma
@EnableModeloNegocioFolioEtiqueta
@EnableModeloNegocioFolio
@EnableModeloNegocioRuta
@EnableModeloNegocioUnidadNegocio
@EnableModeloNegocioDocumento
@EnableModeloNegocioUsuario
//@EnableLimpiezaRemedy
//@EnableJobsScheduler
@EnableAsignacion
@EnableEstatusUsuario
@EnableModuloFlujo
@EnableFlujoPerfil
@EnableFlujo
@EnableModulo
@EnableVersion
@EnableNegocio
@EnableEje
@EnableTipoMedicion
@EnableColorimetria
@EnableCeco
@EnableFrecuenciaMedicion
@EnableStatus
@EnableEjecuta
@EnablePerfil
@EnableDisciplinasEje
@EnableDisciplina
@EnableEjesModelo
@EnableCategoria
@EnableIndicador
@EnableSemaforo
@EnableModelosFranquicia
@EnableProtocolo
@EnableFile
@EnableFranquicia
@EnableDisciplinasNegocios
@EnablePropiedades
@EnableFormato
@EnablePuntoContacto
@EnableNivelConsulta
@EnableCalificacionDisciplina
@EnableCecoBase
@EnableConfiguracionDisciplina
@EnableDashboard
@EnableDashboardCertificacion
@EnableDepuracion
@com.gs.baz.frq.disciplina.expose.EnableDisciplina
@EnableExcel
@com.gs.baz.frq.frecuencia.medicion.expose.EnableFrecuenciaMedicion
@com.gs.baz.frq.negocio.expose.EnableNegocio
@EnablePoblacion
@EnablePreguntaVisita
@EnableRangos
@EnableRegla7S
@EnableReglas7sPreguntas
@EnableRespuestaPreguntaVisita
@EnableRespuestaVisita
@EnableSeccionesProtocolo
@EnableVisitaPuntoContacto
@EnableCumplimientoDashboard
@EnableImperdonableDashboard
@EnableSeccionCumplimiento
@EnablePuntoContactoProtocolo
@EnableTipoFormato
@EnableRespuestaReporteMedicion
@EnableRelacionProtocoloChecklist
@EnableJsonConverter
@EnableReporteMedicion
@EnableAsignacionCertiCalidad
@EnableAsignacionSup
@EnableNegocioVersion
@EnableAsignacionMtto
@EnableGeografiaMtto
@EnableCecosMtto
@EnableDocumentoSucursalCeco
@EnablePrefijo
@EnableDocumentoSucursalFormato
@EnableDocumentoSucursalFolio
@EnableDocumentoSucursalFicha
@EnableDocumentoSucursalArchivo
@EnableDocumentoSucursalProyecto
@EnableDocumentoSucursalCecoProyectoFolio
@EnableDocumentoSucursalNegocio
@EnableDocumentoSucursalDocumento
@EnableDocumentoSucursalProyectoDocumento
@EnableDocumentoSucursalDocumentoArchivo
@EnableUbicacionSucursal
@EnableAPIPortalCautivo
@EnableDistDashboard
@EnableDocumentoSucursalCatalogoZonas
@EnableDocumentoSucursalTipoInformacion
@EnableDocumentoSucursalDatosZona
@EnableDocumentoSucursalNegocioZonas
@EnableReportesLayout
@EnableEnvioCorreo
@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class})
public class BaseInitApp extends SpringBootServletInitializer {

    /**
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BaseInitApp.class);
    }

    public static void main(String[] args) {

    }

}
