/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del Incumplimiento por negocio red unica", value = "IncumplimientoNegocioRedUnica")
public class IncumplimientoSeccionResponsableRedUnica {

    @JsonProperty(value = "responsable")
    @ApiModelProperty(notes = "Responsable", example = "Red Única")
    private String responsable;

    @JsonProperty(value = "idSeccion")
    @ApiModelProperty(notes = "Identificador de la seccion", example = "1")
    private Integer idSeccion;

    @JsonProperty(value = "idPregunta")
    @ApiModelProperty(notes = "Identificador de la pregunta", example = "1")
    private Integer idPregunta;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la pregunta", example = "1. ¿Se cuenta con folio de reporte para falla en el pedestal digital? (RU)")
    private String descripcion;

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Puntos de venta incumplimiento", example = "1")
    private Integer puntosVenta;

    @JsonProperty(value = "porcentaje")
    @ApiModelProperty(notes = "Porcentaje", example = "1")
    private Integer porcentaje;

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Integer getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public String toString() {
        return "IncumplimientoSeccionResponsableRedUnica{" + "responsable=" + responsable + ", idSeccion=" + idSeccion + ", idPregunta=" + idPregunta + ", descripcion=" + descripcion + ", puntosVenta=" + puntosVenta + ", porcentaje=" + porcentaje + '}';
    }

}
