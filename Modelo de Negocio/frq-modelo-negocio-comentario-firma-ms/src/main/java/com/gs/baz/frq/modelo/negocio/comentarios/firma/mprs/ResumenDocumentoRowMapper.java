/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs;

import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.Documento;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.Etiqueta;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.Firma1;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ResumenDocumento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ResumenDocumentoRowMapper implements RowMapper<ResumenDocumento> {

    private ResumenDocumento resumenDocumento;

    @Override
    public ResumenDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
        resumenDocumento = new ResumenDocumento();
        Documento documento = new Documento();
        documento.setTitulo(rs.getString("FCTITULO"));
        documento.setVersion(rs.getString("FCVERSION"));
        documento.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        documento.setObjetivo(rs.getString("FCOBJETIVO"));
        documento.setVigencia(rs.getString("FDVIGENTE"));
        resumenDocumento.setDocumento(documento);

        List<Etiqueta> etiquetas = new ArrayList<>();
        try {
            String arregloEtiquetas[] = rs.getString("ETIQUETAS").split(",");
            for (String etiqueta : arregloEtiquetas) {
                etiquetas.add(new Etiqueta(etiqueta));
            }
        } catch (Exception ex) {
        }
        resumenDocumento.setEtiquetas(etiquetas);

        List<Firma1> firmas = new ArrayList<>();
        try {
            String arregloAutorizador[] = rs.getString("AUTORIZADORES").split(",");
            for (String autorizador : arregloAutorizador) {
                Firma1 firma = new Firma1();
                firma.setEmpleado(new Empleado(autorizador));
                firma.setIdJerarquia(2);
                firmas.add(firma);
            }
        } catch (Exception ex) {
        }
        try {
            String arreglorevisor[] = rs.getString("REVISORES").split(",");
            for (String revisor : arreglorevisor) {
                Firma1 firma = new Firma1();
                firma.setEmpleado(new Empleado(revisor));
                firma.setIdJerarquia(1);
                firmas.add(firma);
            }
        } catch (Exception ex) {
        }
        resumenDocumento.setFirmas(firmas);
        return resumenDocumento;
    }
}
