/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocios.rest;

import com.gs.baz.frq.documentos.sucursal.negocios.dao.DocumentoSucursalNegocioDAOImpl;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.AltaNegocio;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.NegocioBase;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.Negocios;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.ParametrosNegocio;
import com.gs.baz.frq.documentos.sucursal.negocios.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "negocios", value = "negocios", description = "Api para la gestión del catalogo de negocios")
@RestController
@RequestMapping("/api-local/documento-sucursal/negocio/v1")
public class NegocioApi {

    @Autowired
    private DocumentoSucursalNegocioDAOImpl documentoSucursalNegocioDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idNegocio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene negocio", notes = "Obtiene un negocio", nickname = "obtieneNegocio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioBase obtieneNegocio(@ApiParam(name = "idNegocio", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idNegocio") Long idNegocio) throws CustomException, DataNotFoundException {
        NegocioBase negocioBase = documentoSucursalNegocioDAOImpl.selectRow(idNegocio);
        if (negocioBase == null) {
            throw new DataNotFoundException();
        }
        return negocioBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene negocios", notes = "Obtiene todos los negocios", nickname = "obtieneNegocios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Negocios obtieneNegocios() throws CustomException, DataNotFoundException {
        Negocios negocios = new Negocios(documentoSucursalNegocioDAOImpl.selectAllRows());
        if (negocios.getNegocios() != null && negocios.getNegocios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return negocios;
    }

    /**
     *
     * @param parametrosNegocio
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear negocio", notes = "Agrega un negocio", nickname = "creaNegocio")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaNegocio creaNegocio(@ApiParam(name = "ParametrosNegocio", value = "Paramentros para el alta del negocio", required = true) @RequestBody ParametrosNegocio parametrosNegocio) throws DataNotInsertedException {
        return documentoSucursalNegocioDAOImpl.insertRow(parametrosNegocio);
    }

    /**
     *
     * @param parametrosNegocio
     * @param idNegocio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar negocio", notes = "Actualiza un negocio", nickname = "actualizaNegocio")
    @RequestMapping(value = "/{idNegocio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaNegocio(@ApiParam(name = "ParametrosNegocio", value = "Paramentros para la actualización del negocio", required = true) @RequestBody ParametrosNegocio parametrosNegocio, @ApiParam(name = "idNegocio", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idNegocio") Integer idNegocio) throws DataNotUpdatedException {
        parametrosNegocio.setIdNegocio(idNegocio);
        documentoSucursalNegocioDAOImpl.updateRow(parametrosNegocio);
        return new SinResultado();
    }

    /**
     *
     * @param idNegocio
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar negocio", notes = "Elimina un item de los negocios", nickname = "eliminaNegocio")
    @RequestMapping(value = "/{idNegocio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaNegocio(@ApiParam(name = "idNegocio", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idNegocio") Integer idNegocio) throws DataNotDeletedException {
        documentoSucursalNegocioDAOImpl.deleteRow(idNegocio);
        return new SinResultado();
    }

}
