package com.gs.baz.frq.sistema.informacion.dashboard.reportes.init;


import com.gs.baz.frq.data.sources.config.GenericConfig;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dao.SistemaInformacionDashboardReportesDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Import(GenericConfig.class)
@Configuration
@ComponentScan("com.gs.baz.frq.sistema.informacion.dashboard.reportes")
public class ConfigReporte {

    private final Logger logger = LogManager.getLogger();

    public ConfigReporte() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SistemaInformacionDashboardReportesDAOImpl sistemaInformacionDashboardReportesDAOImpl() {
        return new SistemaInformacionDashboardReportesDAOImpl();
    }

}
