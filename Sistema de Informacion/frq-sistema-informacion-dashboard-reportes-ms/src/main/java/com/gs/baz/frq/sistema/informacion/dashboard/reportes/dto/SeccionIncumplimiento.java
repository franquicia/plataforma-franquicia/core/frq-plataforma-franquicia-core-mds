/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la seccion del incumplimiento", value = "SeccionIncumplimiento")
public class SeccionIncumplimiento {

    @JsonProperty(value = "idSeccion")
    @ApiModelProperty(notes = "Identificador de la seccion", example = "1")
    private Integer idSeccion;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la seccion", example = "III. Patio Bancario")
    private String descripcion;

    public Integer getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "SeccionIncumplimiento{" + "idSeccion=" + idSeccion + ", descripcion=" + descripcion + '}';
    }

}
