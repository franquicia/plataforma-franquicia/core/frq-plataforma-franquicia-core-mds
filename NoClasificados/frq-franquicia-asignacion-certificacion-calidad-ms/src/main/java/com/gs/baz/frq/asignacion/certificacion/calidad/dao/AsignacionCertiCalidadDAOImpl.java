package com.gs.baz.frq.asignacion.certificacion.calidad.dao;

import com.gs.baz.frq.asignacion.certificacion.calidad.dto.AsignacionCertiCalidadDTO;
import com.gs.baz.frq.asignacion.certificacion.calidad.mprs.AsignacionCertiCalidadRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class AsignacionCertiCalidadDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertAsig;
    private DefaultJdbcCall jdbcUpdateAsig;
    private DefaultJdbcCall jdbcSelectAsig;
    private DefaultJdbcCall jdbcDeleteAsig;
    private DefaultJdbcCall jdbcInsertAsigGeo;
    private DefaultJdbcCall jdbcUpdateAsigGeo;
    private DefaultJdbcCall jdbcSelectAsigGeo;
    private DefaultJdbcCall jdbcDeleteAsigGeo;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertAsig = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertAsig.withSchemaName(schema);
        jdbcInsertAsig.withCatalogName("PADMASIGNACION");
        jdbcInsertAsig.withProcedureName("SPINSASIGNACION");

        jdbcUpdateAsig = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateAsig.withSchemaName(schema);
        jdbcUpdateAsig.withCatalogName("PADMASIGNACION");
        jdbcUpdateAsig.withProcedureName("SPACTASIGNACION");

        jdbcSelectAsig = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAsig.withSchemaName(schema);
        jdbcSelectAsig.withCatalogName("PADMASIGNACION");
        jdbcSelectAsig.withProcedureName("SPGETASIGNACION");
        jdbcSelectAsig.returningResultSet("PA_CONSULTA", new AsignacionCertiCalidadRowMapper());

        jdbcDeleteAsig = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteAsig.withSchemaName(schema);
        jdbcDeleteAsig.withCatalogName("PADMASIGNACION");
        jdbcDeleteAsig.withProcedureName("SPDELASIGNACION");

        jdbcInsertAsigGeo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertAsigGeo.withSchemaName(schema);
        jdbcInsertAsigGeo.withCatalogName("PAADM_ASIGNGEO");
        jdbcInsertAsigGeo.withProcedureName("SPINS_ASIGNGEO");

        jdbcUpdateAsigGeo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateAsigGeo.withSchemaName(schema);
        jdbcUpdateAsigGeo.withCatalogName("PAADM_ASIGNGEO");
        jdbcUpdateAsigGeo.withProcedureName("SPACT_ASIGNGEO");

        jdbcSelectAsigGeo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAsigGeo.withSchemaName(schema);
        jdbcSelectAsigGeo.withCatalogName("PAADM_ASIGNGEO");
        jdbcSelectAsigGeo.withProcedureName("SPGET_ASIGNGEO");
        jdbcSelectAsigGeo.returningResultSet("PA_CDATOS", new AsignacionCertiCalidadRowMapper().new AsignacionCertiCalidadGeoRowMapper());

        jdbcDeleteAsigGeo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteAsigGeo.withSchemaName(schema);
        jdbcDeleteAsigGeo.withCatalogName("PAADM_ASIGNGEO");
        jdbcDeleteAsigGeo.withProcedureName("SPDEL_ASIGNGEO");
    }

    public AsignacionCertiCalidadDTO insertRow(AsignacionCertiCalidadDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FIPERFIL_ID", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO.getIdGerente());
            mapSqlParameterSource.addValue("PA_FIACTIVO", entityDTO.getActivo());
            mapSqlParameterSource.addValue("PA_FDINICIO_ASIGN", entityDTO.getInicioAsignacion());
            mapSqlParameterSource.addValue("PA_FDFIN_ASIGN", entityDTO.getFinAsignacion());
            mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO.getIdNegocio());
            Map<String, Object> out = jdbcInsertAsig.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_RESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of Asignacion"), ex);
        }
    }

    public AsignacionCertiCalidadDTO updateRow(AsignacionCertiCalidadDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FIPERFIL_ID", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO.getIdGerente());
            mapSqlParameterSource.addValue("PA_FIACTIVO", entityDTO.getActivo());
            mapSqlParameterSource.addValue("PA_FDINICIO_ASIGN", entityDTO.getInicioAsignacion());
            mapSqlParameterSource.addValue("PA_FDFIN_ASIGN", entityDTO.getFinAsignacion());
            mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO.getIdNegocio());
            Map<String, Object> out = jdbcUpdateAsig.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_RESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Asignacion"), ex);
        }
    }

    public List<AsignacionCertiCalidadDTO> selectRows() throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        mapSqlParameterSource.addValue("PA_FIPERFIL_ID", null);
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", null);
        mapSqlParameterSource.addValue("PA_FIACTIVO", null);
        mapSqlParameterSource.addValue("PA_FDINICIO_ASIGN", null);
        mapSqlParameterSource.addValue("PA_FDFIN_ASIGN", null);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", null);
        Map<String, Object> out = jdbcSelectAsig.execute(mapSqlParameterSource);
        return (List<AsignacionCertiCalidadDTO>) out.get("PA_CONSULTA");
    }

    public List<AsignacionCertiCalidadDTO> selectRow(Long entityID1, Long entityID2, Long entityID3, Long entityID4, Long entityID5, Long entityID6, Long entityID7, Long entityID8) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityID1);
        mapSqlParameterSource.addValue("PA_FIPERFIL_ID", entityID2);
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityID3);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", entityID4);
        mapSqlParameterSource.addValue("PA_FIACTIVO", entityID5);
        mapSqlParameterSource.addValue("PA_FDINICIO_ASIGN", entityID6);
        mapSqlParameterSource.addValue("PA_FDFIN_ASIGN", entityID7);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityID8);
        Map<String, Object> out = jdbcSelectAsig.execute(mapSqlParameterSource);
        return (List<AsignacionCertiCalidadDTO>) out.get("PA_CONSULTA");

    }

    public AsignacionCertiCalidadDTO deleteRow(AsignacionCertiCalidadDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            Map<String, Object> out = jdbcDeleteAsig.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_RESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete row of Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception Error to delete row of Asignacion"), ex);
        }
    }

    public AsignacionCertiCalidadDTO insertRowGeo(AsignacionCertiCalidadDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCTERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_FCZONA", entityDTO.getZona());
            Map<String, Object> out = jdbcInsertAsigGeo.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of Asignacion Geo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of Asignacion Geo"), ex);
        }
    }

    public AsignacionCertiCalidadDTO updateRowGeo(AsignacionCertiCalidadDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCTERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_FCZONA", entityDTO.getZona());
            mapSqlParameterSource.addValue("PA_FIACTIVO", entityDTO.getActivo());

            Map<String, Object> out = jdbcUpdateAsigGeo.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Asignacion Geo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Asignacion Geo"), ex);
        }
    }

    public List<AsignacionCertiCalidadDTO> selectRowsGeo() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);

        Map<String, Object> out = jdbcSelectAsigGeo.execute(mapSqlParameterSource);
        return (List<AsignacionCertiCalidadDTO>) out.get("PA_CDATOS");
    }

    public List<AsignacionCertiCalidadDTO> selectRowGeo(Long entityID) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityID);
        Map<String, Object> out = jdbcSelectAsigGeo.execute(mapSqlParameterSource);
        return (List<AsignacionCertiCalidadDTO>) out.get("PA_CDATOS");

    }

    public AsignacionCertiCalidadDTO deleteRowGeo(AsignacionCertiCalidadDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            Map<String, Object> out = jdbcDeleteAsigGeo.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete row of Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception Error to delete row of Asignacion"), ex);
        }
    }

}
