/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos calificacion promedio", value = "CalificacionPromedio")
public class CalificacionPromedio {

    @JsonProperty(value = "totalPuntosVenta")
    @ApiModelProperty(notes = "Total de puntos de venta", example = "100")
    private Integer totalPuntosVenta;

    @JsonProperty(value = "calificacion")
    @ApiModelProperty(notes = "Calificación promedio", example = "80")
    private Integer calificacion;

    public Integer getTotalPuntosVenta() {
        return totalPuntosVenta;
    }

    public void setTotalPuntosVenta(Integer totalPuntosVenta) {
        this.totalPuntosVenta = totalPuntosVenta;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "CalificacionPromedio{" + "totalPuntosVenta=" + totalPuntosVenta + ", calificacion=" + calificacion + '}';
    }

}
