/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Folio;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class DocumentoUsuario {

    @JsonProperty(value = "idDocumentoUsuario")
    @ApiModelProperty(notes = "Identificador del documento por usuario", example = "1", position = -1)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idDocumentoUsuario;

    @JsonProperty(value = "folio")
    @ApiModelProperty(notes = "Datos del documento", example = "1")
    private Folio folio;

    public DocumentoUsuario() {
    }

    public Integer getIdDocumentoUsuario() {
        return idDocumentoUsuario;
    }

    public void setIdDocumentoUsuario(Integer idDocumentoUsuario) {
        this.idDocumentoUsuario = idDocumentoUsuario;
    }

    public Folio getFolio() {
        return folio;
    }

    public void setFolio(Folio folio) {
        this.folio = folio;
    }

    @Override
    public String toString() {
        return "DocumentoUsuario{" + "idDocumentoUsuario=" + idDocumentoUsuario + ", folio=" + folio + '}';
    }

}
