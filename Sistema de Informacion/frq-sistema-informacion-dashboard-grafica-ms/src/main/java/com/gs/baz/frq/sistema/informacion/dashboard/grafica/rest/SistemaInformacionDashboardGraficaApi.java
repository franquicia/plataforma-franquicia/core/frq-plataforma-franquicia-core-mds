/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dao.SistemaInformacionDashboardGraficaDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.AltaGrafica;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaBase;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.Graficas;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.ParametrosGrafica;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "graficas", value = "graficas", description = "Api para la gestión del catalogo de graficas")
@RestController
@RequestMapping("/api-local/sistema-informacion/tablero/graficas/v1")
public class SistemaInformacionDashboardGraficaApi {

    @Autowired
    private SistemaInformacionDashboardGraficaDAOImpl sistemaInformacionDashboardGraficaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idGrafica
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrafica}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GraficaBase obtieneGrafica(@ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica) throws CustomException, DataNotFoundException {
        GraficaBase graficaBase = sistemaInformacionDashboardGraficaDAOImpl.selectRow(idGrafica);
        if (graficaBase == null) {
            throw new DataNotFoundException();
        }
        return graficaBase;
    }

    /**
     *
     * @param valor
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica por clave", notes = "Obtiene un grafica por clave", nickname = "obtieneGraficaClave")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/clave/{valor}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GraficaBase obtieneGraficaClave(@ApiParam(name = "valor", value = "Clave de la grafica", example = "odmetro", required = true) @PathVariable("valor") String valor) throws CustomException, DataNotFoundException {
        GraficaBase graficaBase = sistemaInformacionDashboardGraficaDAOImpl.selectRow(valor);
        if (graficaBase == null) {
            throw new DataNotFoundException();
        }
        return graficaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Graficas obtieneGraficas() throws CustomException, DataNotFoundException {
        Graficas graficas = new Graficas(sistemaInformacionDashboardGraficaDAOImpl.selectAllRows());
        if (graficas.getGraficas() != null && graficas.getGraficas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return graficas;
    }

    /**
     *
     * @param parametrosGrafica
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaGrafica creaGrafica(@ApiParam(name = "ParametrosGrafica", value = "Paramentros para el alta de la grafica", required = true) @RequestBody ParametrosGrafica parametrosGrafica) throws DataNotInsertedException {
        return sistemaInformacionDashboardGraficaDAOImpl.insertRow(parametrosGrafica);
    }

    /**
     *
     * @param parametrosGrafica
     * @param idGrafica
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar grafica", notes = "Actualiza un grafica", nickname = "actualizaGrafica")
    @RequestMapping(value = "/{idGrafica}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaGrafica(@ApiParam(name = "ParametrosGrafica", value = "Paramentros para la actualización de la grafica", required = true) @RequestBody ParametrosGrafica parametrosGrafica, @ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica) throws DataNotUpdatedException {
        parametrosGrafica.setIdGrafica(idGrafica);
        sistemaInformacionDashboardGraficaDAOImpl.updateRow(parametrosGrafica);
        return new SinResultado();
    }

    /**
     *
     * @param idGrafica
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grafica", notes = "Elimina un item de los graficas", nickname = "eliminaGrafica")
    @RequestMapping(value = "/{idGrafica}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaGrafica(@ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica) throws DataNotDeletedException {
        sistemaInformacionDashboardGraficaDAOImpl.deleteRow(idGrafica);
        return new SinResultado();
    }

}
