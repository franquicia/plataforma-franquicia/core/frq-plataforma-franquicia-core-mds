/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Detalle de Punto de venta pendiente por gerente", value = "PuntoVentaPendiente")
public class PuntoVentaPendiente {

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costos", example = "230233", position = -1)
    private String centroCostos;

    @JsonProperty(value = "nombreCentroCostos")
    @ApiModelProperty(notes = "Nombre del centro de costos", example = "GCC EL SECO", position = -2)
    private String nombreCentroCostos;
    
    @JsonProperty(value = "numeroEmpleadoCertificador")
    @ApiModelProperty(notes = "Número de empleado del certificador", example = "949154", position = -3)
    private Integer numeroEmpleadoCertificador;
    
    @JsonProperty(value = "nombreGerente")
    @ApiModelProperty(notes = "Nombre del gerente", example = "EDHER GUILLERMO PEREZ CASTILLO", position = -4)
    private String nombreGerente;
    
    @JsonProperty(value = "fechaCertificacion")
    @ApiModelProperty(notes = "Fecha de la certificación", example = "05/11/2021", position = -5)
    private String fecha;
    
    @JsonProperty(value = "grupoClave")
    @ApiModelProperty(notes = "Clave del grupo al que pertenece.", example = "324", position = -6)
    private Integer ordenGrupo;
    
    @JsonProperty(value = "totalPendientes")
    @ApiModelProperty(notes = "Total de pendientes", example = "4", position = -7)
    private Integer totalPendientes;

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getNombreCentroCostos() {
        return nombreCentroCostos;
    }

    public void setNombreCentroCostos(String nombreCentroCostos) {
        this.nombreCentroCostos = nombreCentroCostos;
    }

    public Integer getNumeroEmpleadoCertificador() {
        return numeroEmpleadoCertificador;
    }

    public void setNumeroEmpleadoCertificador(Integer numeroEmpleadoCertificador) {
        this.numeroEmpleadoCertificador = numeroEmpleadoCertificador;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getOrdenGrupo() {
        return ordenGrupo;
    }

    public void setOrdenGrupo(Integer ordenGrupo) {
        this.ordenGrupo = ordenGrupo;
    }

    public Integer getTotalPendientes() {
        return totalPendientes;
    }

    public void setTotalPendientes(Integer totalPendientes) {
        this.totalPendientes = totalPendientes;
    }

    @Override
    public String toString() {
        return "PuntosVentaPendientes{" + "centroCostos=" + centroCostos + ", nombreCentroCostos=" + nombreCentroCostos + ", numeroEmpleadoCertificador=" + numeroEmpleadoCertificador + ", nombreGerente=" + nombreGerente + ", fecha=" + fecha + ", ordenGrupo=" + ordenGrupo + ", totalPendientes=" + totalPendientes + '}';
    }

}
