package com.gs.baz.modelos.franquicia.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.modelos.franquicia.dao.util.GenericDAODisciplinasNegocios;
import com.gs.baz.modelos.franquicia.dto.DisciplinaNegocioDTO;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class DisciplinasNegociosModeloDAOImpl extends DefaultDAO implements GenericDAODisciplinasNegocios<DisciplinaNegocioDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINDISCNEG");
        jdbcInsert.withProcedureName("SP_INS_DISCNEG");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINDISCNEG");
        jdbcUpdate.withProcedureName("SP_ACT_DISCNEG");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINDISCNEG");
        jdbcDelete.withProcedureName("SP_DEL_DISCNEG");
    }

    @Override
    public DisciplinaNegocioDTO insertRow(DisciplinaNegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDEJEDIS", entityDTO.getIdDisciplinaEje());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FIIDNEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdDisciplinaNegocio((BigDecimal) out.get("PA_FIIDDISNEG"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Disciplina Negocio"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Disciplina Negocio"), ex);
        }
    }

    @Override
    public DisciplinaNegocioDTO updateRow(DisciplinaNegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDEJEDIS", entityDTO.getIdDisciplinaEje());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Disciplina Negocio"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Disciplina Negocio"), ex);
        }
    }
}
