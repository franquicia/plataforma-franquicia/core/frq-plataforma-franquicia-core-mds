/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class ReadDirectoryException extends Exception{

    public ReadDirectoryException() {
    }

    public ReadDirectoryException(String string) {
        super(string);
    }

    public ReadDirectoryException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public ReadDirectoryException(Throwable thrwbl) {
        super(thrwbl);
    }
}
