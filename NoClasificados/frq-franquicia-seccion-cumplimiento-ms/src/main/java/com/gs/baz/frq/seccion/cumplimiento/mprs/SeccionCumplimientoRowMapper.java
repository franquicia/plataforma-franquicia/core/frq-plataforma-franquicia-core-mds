package com.gs.baz.frq.seccion.cumplimiento.mprs;

import com.gs.baz.frq.seccion.cumplimiento.dto.SeccionCumplimientoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SeccionCumplimientoRowMapper implements RowMapper<SeccionCumplimientoDTO> {

    private SeccionCumplimientoDTO seccionCumplimiento;

    @Override
    public SeccionCumplimientoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        seccionCumplimiento = new SeccionCumplimientoDTO();
        seccionCumplimiento.setIdCeco((rs.getString("FCID_CECO")));
        seccionCumplimiento.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        seccionCumplimiento.setModulo(rs.getString("FCMODULO"));
        seccionCumplimiento.setPonderacion((BigDecimal) rs.getObject("FIPONDERACION"));
        seccionCumplimiento.setFechaVisista(rs.getString("FECHA_VISITA"));
        seccionCumplimiento.setPonderacionEsperado((BigDecimal) rs.getObject("FIPONDESPE"));
        seccionCumplimiento.setIdNegocioDisc(((BigDecimal) rs.getObject("FINEGOCIODISCID")));
        seccionCumplimiento.setModUsuario(rs.getString("FCUSUARIO_MOD"));
        seccionCumplimiento.setModFecha(rs.getString("FDFECHA_MOD"));

        return seccionCumplimiento;
    }
}
