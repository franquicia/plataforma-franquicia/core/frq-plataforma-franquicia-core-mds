/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos Alta Centro de costos", value = "AltaCentroCostos")
public class CentroCostos {

    @JsonProperty(value = "idProveedor")
    @ApiModelProperty(notes = "Identificador del proveedor", example = "1", position = -1)
    private Integer idProveedor;

    @JsonProperty(value = "numeroEmpleadoCertificador")
    @ApiModelProperty(notes = "Número de empleado del certificador", example = "202622", position = -2)
    private Integer numeroEmpleadoCertificador;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costos.", example = "920100", position = -3)
    private Integer idCeco;
    
    @JsonProperty(value = "auxiliarUno")
    @ApiModelProperty(notes = "Nombre del auxiliar uno.", example = "", position = -4)
    private String auxiliarUno;
    
    @JsonProperty(value = "auxiliarDos")
    @ApiModelProperty(notes = "Nombre del auxiliar dos.", example = "", position = -5)
    private String auxiliarDos;

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Integer getNumeroEmpleadoCertificador() {
        return numeroEmpleadoCertificador;
    }

    public void setNumeroEmpleadoCertificador(Integer numeroEmpleadoCertificador) {
        this.numeroEmpleadoCertificador = numeroEmpleadoCertificador;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getAuxiliarUno() {
        return auxiliarUno;
    }

    public void setAuxiliarUno(String auxiliarUno) {
        this.auxiliarUno = auxiliarUno;
    }

    public String getAuxiliarDos() {
        return auxiliarDos;
    }

    public void setAuxiliarDos(String auxiliarDos) {
        this.auxiliarDos = auxiliarDos;
    }

    @Override
    public String toString() {
        return "CentroCostos{" + "idProveedor=" + idProveedor + ", numeroEmpleadoCertificador=" + numeroEmpleadoCertificador + ", idCeco=" + idCeco + ", auxiliarUno=" + auxiliarUno + ", auxiliarDos=" + auxiliarDos + '}';
    }

}
