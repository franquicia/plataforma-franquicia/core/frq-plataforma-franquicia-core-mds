/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

/**
 *
 * @author cescobarh
 */
public class ClientSocket2 {

    static public class CustomStompSessionHandler extends StompSessionHandlerAdapter {

        private final String userId;

        public CustomStompSessionHandler(String userId) {
            this.userId = userId;
        }

        private void showHeaders(StompHeaders headers) {
            headers.entrySet().stream().map((e) -> {
                System.out.print("  " + e.getKey() + ": ");
                return e;
            }).map((e) -> {
                boolean first = true;
                for (String v : e.getValue()) {
                    if (!first) {
                        System.out.print(", ");
                    }
                    System.out.print(v);
                    first = false;
                }
                return e;
            }).forEach(System.out::println);
        }

        private void subscribeTopic(String topic, StompSession session) {
            session.subscribe(topic, new StompFrameHandler() {
                @Override
                public Type getPayloadType(StompHeaders headers) {
                    return String.class;
                }

                @Override
                public void handleFrame(StompHeaders headers, Object payload) {
                    System.out.println("holaa->" + payload.toString());
                }
            });
        }

        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            System.out.println("Connected! Headers:");
            showHeaders(connectedHeaders);
            subscribeTopic("/topic/chat", session);
            //session.send("/app/chat", new MessageReply(userId + "-> hello from spring"));
        }
    }

    public static void main(String args[]) throws Exception {
        WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(simpleWebSocketClient));

        SockJsClient sockJsClient = new SockJsClient(transports);
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        String url = "ws://10.53.33.83:82/plataforma-franquicia-core/websocket/";
        String userId = "spring-" + ThreadLocalRandom.current().nextInt(1, 99);

        StompSessionHandler sessionHandler = new CustomStompSessionHandler(userId);
        StompSession session = stompClient.connect(url, sessionHandler).get();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        for (;;) {
            System.out.print(userId + " >> ");
            System.out.flush();
            String line = in.readLine();
            if (line == null) {
                break;
            }
            if (line.length() == 0) {
                continue;
            }
            //session.send("/app/chat", new MessageReply(userId + "-> " + line));
        }
    }
}
