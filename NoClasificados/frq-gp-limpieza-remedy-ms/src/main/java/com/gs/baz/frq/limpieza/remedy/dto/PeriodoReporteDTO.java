package com.gs.baz.frq.limpieza.remedy.dto;

public class PeriodoReporteDTO {

    private Integer idPeriodoReporte;
    private Integer idPeriodo;
    private Integer idReporte;
    private String horario;

    public PeriodoReporteDTO() {
        super();
    }

    public Integer getIdPeriodoReporte() {
        return idPeriodoReporte;
    }

    public void setIdPeriodoReporte(Integer idPeriodoReporte) {
        this.idPeriodoReporte = idPeriodoReporte;
    }

    public Integer getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(Integer idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public Integer getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

}
