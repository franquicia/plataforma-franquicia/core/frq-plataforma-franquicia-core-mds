/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.client.bi;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Component
public class ArchivosVirtualesBI {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpServletRequest httpServletRequest;

    public void duplicaArchivos(HttpHeaders httpHeaders, Integer idFolio, Integer idFolioNuevo) throws Exception {
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folio-virtuales-archivos/v1/" + idFolio + "/virtuales/copia-archivos/" + idFolioNuevo;
        //ParametrosEstadoFolio parametrosEstadoFolio = new ParametrosEstadoFolio(idEstadoFolio);
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(httpHeaders);
            restTemplate.exchange(basePath, HttpMethod.GET, httpEntityUsuario, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        }
    }
}
