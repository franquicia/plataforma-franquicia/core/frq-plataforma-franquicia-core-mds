/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.appender;

import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.frq.model.commons.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

/**
 *
 * @author cescobarh
 */
public class ClientSocketLogs extends StompSessionHandlerAdapter {

    private final WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
    private List<Transport> transports;
    public static StompSession session;
    private final Logger logger = LogManager.getLogger();
    public static final List<NodeSession> nodes = new ArrayList<>();

    public ClientSocketLogs() {
        if (Environment.CURRENT.value().equals(Environment.DEVELOPING)) {
            StompSession nodeSession = null;
            nodes.add(new NodeSession("10.53.33.83:82", nodeSession));
        } else {
            String[] servers = {"10.53.33.74", "10.53.33.75", "10.53.33.76", "10.53.33.77"};
            for (String node : servers) {
                if (!Util.getIPLocal().equals(node)) {
                    StompSession nodeSession = null;
                    nodes.add(new NodeSession(node + ":8082", nodeSession));
                }
            }
        }
    }

    public String node() {
        return Util.getIPLocal();
    }

    public void selfConnect() {
        String uri;
        if (Environment.CURRENT.value().equals(Environment.PRODUCTION)) {
            uri = "ws://" + this.node() + ":8082/plataforma-franquicia-core/websocket/";
        } else {
            uri = "ws://" + this.node() + ":8080/plataforma-franquicia-core/websocket/";
        }
        if (ClientSocketLogs.session == null) {
            transports = new ArrayList<>(1);
            transports.add(new WebSocketTransport(simpleWebSocketClient));
            SockJsClient sockJsClient = new SockJsClient(transports);
            WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
            stompClient.setMessageConverter(new MappingJackson2MessageConverter());
            try {
                ClientSocketLogs.session = stompClient.connect(uri, this).get();
            } catch (InterruptedException | ExecutionException ex) {
                System.out.println("Error");
            }
        }
    }

    public List<NodeSession> nodesConnect() {
        nodes.forEach(nodeSession -> {
            NodeClientSocketLogs nodeClientSocketLogs = new NodeClientSocketLogs();
            nodeClientSocketLogs.nodeConnect(nodeSession);
        });
        return nodes;
    }

    private void showHeaders(StompHeaders headers) {
        logger.log(Level.DEBUG, "Headers");
        headers.entrySet().stream().map((e) -> {
            return e;
        }).map((e) -> {
            return e;
        }).forEach(System.out::println);
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        logger.log(Level.DEBUG, "Connected with SessionId " + session.getSessionId());
        showHeaders(connectedHeaders);
    }

    @Override
    public void handleException(StompSession ss, StompCommand sc, StompHeaders sh, byte[] bytes, Throwable thrwbl) {
    }

    @Override
    public void handleTransportError(StompSession ss, Throwable thrwbl) {
    }

    @Override
    public void handleFrame(StompHeaders sh, Object o) {
    }
}
