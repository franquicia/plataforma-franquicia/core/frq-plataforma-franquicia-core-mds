/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del Centro de costos asociados al Proveedor", value = "CentroCostosProveedor")
public class CentroCostosProveedor {

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1", position = -1)
    private Integer idNegocio;

    @JsonProperty(value = "nombreNegocio")
    @ApiModelProperty(notes = "Nombre del negocio", example = "EKT", position = -2)
    private String nombreNegocio;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costos.", example = "920100", position = -3)
    private Integer idCeco;
    
    @JsonProperty(value = "nombreCentroCostos")
    @ApiModelProperty(notes = "Nombre del centro de costos.", example = "MEGA DF LA LUNA", position = -4)
    private String nombreCentroCostos;
    
    @JsonProperty(value = "idProveedor")
    @ApiModelProperty(notes = "Identificador del proveedor.", example = "1", position = -5)
    private Integer idProveedor;

    @JsonProperty(value = "formato")
    @ApiModelProperty(notes = "Formato de la sucursal.", example = "RETAIL", position = -6)
    private String formato;
    
    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Número del folio asociado a la sucursal.", example = "1", position = -7)
    private Integer idFolio;
    
    @JsonProperty(value = "idEstado")
    @ApiModelProperty(notes = "Identificador del estado.", example = "2", position = -8)
    private Integer idEstado;
    
    @JsonProperty(value = "nombreEstado")
    @ApiModelProperty(notes = "Nombre del estado o bandeja al que pertenece el centro de costos", example = "PENDIENTE DE VALIDACION VISUAL", position = -9)
    private String nombreEstado;
    
    @JsonProperty(value = "fechaFolio")
    @ApiModelProperty(notes = "Fecha de alta del folio.", example = "21/12/2021", position = -10)
    private String fechaFolio;
    
    @JsonProperty(value = "fechaActualizacion")
    @ApiModelProperty(notes = "Fecha de actualización del folio.", example = "22/12/2021", position = -11)
    private String fechaActualizacion;
    
    @JsonProperty(value = "idTerritorio")
    @ApiModelProperty(notes = "Identificador del territorio.", example = "2", position = -12)
    private String idTerritorio;
    
    @JsonProperty(value = "territorio")
    @ApiModelProperty(notes = "Nombre del territorio.", example = "GOLFO", position = -13)
    private String territorio;
    
    @JsonProperty(value = "comentario")
    @ApiModelProperty(notes = "Comentario de la validación.", example = "", position = -14)
    private String comentario;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCentroCostos() {
        return nombreCentroCostos;
    }

    public void setNombreCentroCostos(String nombreCentroCostos) {
        this.nombreCentroCostos = nombreCentroCostos;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getFechaFolio() {
        return fechaFolio;
    }

    public void setFechaFolio(String fechaFolio) {
        this.fechaFolio = fechaFolio;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(String idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public String toString() {
        return "CentroCostosProveedor{" + "idNegocio=" + idNegocio + ", nombreNegocio=" + nombreNegocio + ", idCeco=" + idCeco + ", nombreCentroCostos=" + nombreCentroCostos + ", idProveedor=" + idProveedor + ", formato=" + formato + ", idFolio=" + idFolio + ", idEstado=" + idEstado + ", nombreEstado=" + nombreEstado + ", fechaFolio=" + fechaFolio + ", fechaActualizacion=" + fechaActualizacion + ", idTerritorio=" + idTerritorio + ", territorio=" + territorio + ", comentario=" + comentario + '}';
    }

}
