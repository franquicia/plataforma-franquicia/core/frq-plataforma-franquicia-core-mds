package com.gs.baz.modelos.franquicia.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import com.gs.baz.modelos.franquicia.dao.DisciplinasEjeDAOImpl;
import com.gs.baz.modelos.franquicia.dao.DisciplinasNegociosModeloDAOImpl;
import com.gs.baz.modelos.franquicia.dao.EjesModeloDAOImpl;
import com.gs.baz.modelos.franquicia.dao.ModeloFranquiciaDAOImpl;
import com.gs.baz.modelos.franquicia.dao.ProtocoloDisciplinaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Configuration
@Import(ConfigBucketServices.class)
@ComponentScan("com.gs.baz.modelos.franquicia")
public class ConfigModelosFranquicia {

    private final Logger logger = LogManager.getLogger();

    public ConfigModelosFranquicia() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ModeloFranquiciaDAOImpl modeloFranquiciaDAOImpl() {
        return new ModeloFranquiciaDAOImpl();
    }

    @Bean(name = "disciplinasEjeModeloFranquiciaDAOImpl", initMethod = "init")
    public DisciplinasEjeDAOImpl disciplinasEjeDAOImpl() {
        return new DisciplinasEjeDAOImpl();
    }

    @Bean(name = "ejesModeloFranquiciaDAOImpl", initMethod = "init")
    public EjesModeloDAOImpl ejesModeloDAOImpl() {
        return new EjesModeloDAOImpl();
    }

    @Bean(name = "disciplinasNegociosModeloFranquiciaDAOImpl", initMethod = "init")
    public DisciplinasNegociosModeloDAOImpl disciplinasNegociosModeloDAOImpl() {
        return new DisciplinasNegociosModeloDAOImpl();
    }

    @Bean(name = "protocoloDisciplinaDAOImpl", initMethod = "init")
    public ProtocoloDisciplinaDAOImpl protocoloDisciplinaDAOImpl() {
        return new ProtocoloDisciplinaDAOImpl();
    }

}
