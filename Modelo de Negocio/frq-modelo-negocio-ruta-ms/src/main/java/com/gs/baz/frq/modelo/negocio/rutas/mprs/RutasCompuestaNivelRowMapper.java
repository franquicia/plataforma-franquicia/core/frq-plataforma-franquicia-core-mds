/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.mprs;

import com.gs.baz.frq.modelo.negocio.rutas.dto.ModeloNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class RutasCompuestaNivelRowMapper implements RowMapper<ModeloNegocio> {

    private ModeloNegocio modeloNegocio;

    @Override
    public ModeloNegocio mapRow(ResultSet rs, int rowNum) throws SQLException {
        modeloNegocio = new ModeloNegocio();
        modeloNegocio.setIdArbol(rs.getInt("FIIDMODENEGOC"));
        modeloNegocio.setDescripcion(rs.getString("FCNOMBREUNEG"));
        return modeloNegocio;
    }
}
