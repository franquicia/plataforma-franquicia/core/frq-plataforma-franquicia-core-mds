/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.mprs;

import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.DocumentoFolio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoFolioRowMapper implements RowMapper<DocumentoFolio> {

    private DocumentoFolio documentoFolio;

    @Override
    public DocumentoFolio mapRow(ResultSet rs, int rowNum) throws SQLException {

        documentoFolio = new DocumentoFolio();

        documentoFolio.setFolio(rs.getInt("FIID_FOLIO"));
        documentoFolio.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        documentoFolio.setIdArchivo(rs.getInt("FIID_ARCHIVO"));

        return documentoFolio;
    }
}
