/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.filters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorValidationClient;
import com.gs.baz.frq.validador.bi.RequestValidatorAdditionalWrapper;
import com.gs.baz.frq.validador.util.JavaxValidationException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE - 12)
public class RequestValidatiorAdditionalFilter extends OncePerRequestFilter {

    private final Logger loggerValidator = LogManager.getLogger();

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            httpServletRequest = new RequestValidatorAdditionalWrapper(httpServletRequest, httpServletResponse);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (CustomException ex) {
            ResponseApiCodes apiCode;
            ResponseMessage responseMessage;
            RespuestaAPI respuestaErrorClient;
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04001;
            responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> erroresDetalles = new ArrayList<>();
            Throwable cause = ex.getCause().getCause();
            if (cause == null) {
                cause = ex.getCause();
            }
            if (cause instanceof JavaxValidationException) {
                JavaxValidationException javaxValidationException = (JavaxValidationException) cause;
                javaxValidationException.getViolations().stream().forEach((itemMessage) -> {
                    erroresDetalles.add(new ErrorDetalle(itemMessage));
                });
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof JsonParseException) {
                erroresDetalles.add(new ErrorDetalle("El cuerpo de la petición no tiene un formato correcto"));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof UnrecognizedPropertyException) {
                UnrecognizedPropertyException unrecognizedPropertyException = (UnrecognizedPropertyException) cause;
                erroresDetalles.add(new ErrorDetalle("El atributo '" + unrecognizedPropertyException.getPropertyName() + "' no es reconocido por esta petición"));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof InvalidFormatException) {
                InvalidFormatException invalidFormatException = (InvalidFormatException) cause;
                Object[] enumConstants = invalidFormatException.getTargetType().getEnumConstants();
                if (enumConstants != null && enumConstants.length > 0) {
                    List<String> valuesEnums = new ArrayList<>();
                    for (Object value : enumConstants) {
                        valuesEnums.add(value.toString());
                    }
                    String attributeName = null;
                    try {
                        JsonProperty jsonProperty = invalidFormatException.getTargetType().getEnclosingClass().getDeclaredField(invalidFormatException.getPath().get(0).getFieldName()).getAnnotation(JsonProperty.class);
                        attributeName = jsonProperty.value();
                    } catch (NoSuchFieldException | SecurityException ex1) {
                        java.util.logging.Logger.getLogger(RequestValidatiorFilter.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    erroresDetalles.add(new ErrorDetalle("El valor '" + invalidFormatException.getValue() + "' del atributo '" + attributeName + "' no es permitido, verifica que el valor concuerde con alguno de estos valores " + valuesEnums.toString()));
                } else {
                    erroresDetalles.add(new ErrorDetalle("El formato de alguno de los valores es incorrecto, favor de validar"));
                }
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof MismatchedInputException) {
                MismatchedInputException mismatchedInputException = (MismatchedInputException) cause;
                String attributeName = null;
                String originalType = mismatchedInputException.getTargetType().getName().replace("java.lang.", "");
                try {
                    JsonProperty jsonProperty = mismatchedInputException.getPath().get(0).getFrom().getClass().getDeclaredField(mismatchedInputException.getPath().get(0).getFieldName()).getAnnotation(JsonProperty.class);
                    attributeName = jsonProperty.value();
                } catch (NoSuchFieldException | SecurityException ex1) {
                    java.util.logging.Logger.getLogger(RequestValidatiorFilter.class.getName()).log(Level.SEVERE, null, ex1);
                }
                erroresDetalles.add(new ErrorDetalle("El parámetro '" + attributeName + "' debe de ser tipo " + originalType));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else {
                respuestaErrorClient = null;
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
            if (respuestaErrorClient != null) {
                loggerValidator.error(ex.getMessage(), ex);
                httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                httpServletResponse.setStatus(responseMessage.getCode());
                OutputStream out = httpServletResponse.getOutputStream();
                ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(out, respuestaErrorClient);
                out.flush();
            }

        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest httpServletRequest) throws ServletException {
        String uri = httpServletRequest.getRequestURI();
        return uri.contains("aplicaciones/llaves") || uri.contains("oauth/token");
    }

}
