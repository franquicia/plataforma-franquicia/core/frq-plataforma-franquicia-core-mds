/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta Distribución de Respuestas, Imperdonables e Incumplimiento", value = "RespuestaImperdonableIncumplimiento")
public class RespuestasImperdonableIncumplimientoDTO {

    @JsonProperty(value = "ejecucion")
    @ApiModelProperty(notes = "Indicador de la ejecución.", example = "1")
    private Integer ejecucion;

    @JsonProperty(value = "respuestaEjecucion")
    @ApiModelProperty(notes = "Respuesta de la ejecución.", example = "1")
    private String respuestaEjecucion;

    @JsonProperty(value = "envioCorreo")
    @ApiModelProperty(notes = "Indicador del envío de notificación por correo electrónico.", example = "true")
    private Boolean envioCorreo;

    public Integer getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(Integer ejecucion) {
        this.ejecucion = ejecucion;
    }

    public String getRespuestaEjecucion() {
        return respuestaEjecucion;
    }

    public void setRespuestaEjecucion(String respuestaEjecucion) {
        this.respuestaEjecucion = respuestaEjecucion;
    }

    public Boolean getEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(Boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    @Override
    public String toString() {
        return "RespuestasImperdonableIncumplimientoDTO{" + "ejecucion=" + ejecucion + ", respuestaEjecucion=" + respuestaEjecucion + ", envioCorreo=" + envioCorreo + '}';
    }

}
