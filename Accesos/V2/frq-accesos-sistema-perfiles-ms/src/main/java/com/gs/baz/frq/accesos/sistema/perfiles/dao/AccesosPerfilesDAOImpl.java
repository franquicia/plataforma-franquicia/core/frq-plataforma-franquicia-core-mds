package com.gs.baz.frq.accesos.sistema.perfiles.dao;

import com.gs.baz.frq.accesos.sistema.perfiles.dto.AltaPerfil;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.AltaPerfilUsuario;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.AltaPerfilVistas;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.ParametroVista;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.ParametrosPerfil;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.ParametrosPerfilVistas;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.Perfil;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.PerfilBase;
import com.gs.baz.frq.accesos.sistema.perfiles.mprs.PerfilRowMapper;
import com.gs.baz.frq.accesos.sistema.perfiles.mprs.PerfilesRowMapper;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class AccesosPerfilesDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcInsertPerfilUsuario;
    private DefaultJdbcCall jdbcDeletePerfilUsuario;
    private DefaultJdbcCall jdbcDeletePerfilVista;
    private DefaultJdbcCall jdbcInsertPerfilVistas;
    private DefaultJdbcCall jdbcDeletePerfilVistas;
    private DefaultJdbcCall jdbcSelectAllPerfilesUsuario;
    private DefaultJdbcCall jdbcSelectAllPerfilesDisponiblesUsuario;
    private DefaultJdbcCall jdbcSelectAllPerfilesFijosUsuario;
    private DefaultJdbcCall jdbcSelectAllPerfilesFinalesUsuario;
    private DefaultJdbcCall jdbcSelectAllPerfilesVista;
    private final String schema = "MODFRANQ";

    public void init() {
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAPERFIL");
        jdbcInsert.withProcedureName("SPINSAPERFIL");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAPERFIL");
        jdbcUpdate.withProcedureName("SPACTAPERFIL");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAPERFIL");
        jdbcDelete.withProcedureName("SPDELAPERFIL");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAPERFIL");
        jdbcSelect.withProcedureName("SPGETAPERFIL");
        jdbcSelect.returningResultSet("PA_CDATOS", new PerfilRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAADMAPERFIL");
        jdbcSelectAll.withProcedureName("SPGETAPERFIL");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new PerfilesRowMapper());

        jdbcInsertPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertPerfilUsuario.withSchemaName(schema);
        jdbcInsertPerfilUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcInsertPerfilUsuario.withProcedureName("SPINSAPERF_USUA");

        jdbcDeletePerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeletePerfilUsuario.withSchemaName(schema);
        jdbcDeletePerfilUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcDeletePerfilUsuario.withProcedureName("SPDELAPERF_USUA");

        jdbcInsertPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertPerfilUsuario.withSchemaName(schema);
        jdbcInsertPerfilUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcInsertPerfilUsuario.withProcedureName("SPINSAPERF_USUA");

        jdbcDeletePerfilVista = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeletePerfilVista.withSchemaName(schema);
        jdbcDeletePerfilVista.withCatalogName("PAADMDPERVISTA");
        jdbcDeletePerfilVista.withProcedureName("SPDELDPERVISTA");

        jdbcInsertPerfilVistas = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertPerfilVistas.withSchemaName(schema);
        jdbcInsertPerfilVistas.withCatalogName("PAADMDPERVISTA");
        jdbcInsertPerfilVistas.withProcedureName("SPINSDPERVISTA");

        jdbcDeletePerfilVistas = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeletePerfilVistas.withSchemaName(schema);
        jdbcDeletePerfilVistas.withCatalogName("PAADMDPERVISTA");
        jdbcDeletePerfilVistas.withProcedureName("SPDELVISTASPER");

        jdbcSelectAllPerfilesUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllPerfilesUsuario.withSchemaName(schema);
        jdbcSelectAllPerfilesUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcSelectAllPerfilesUsuario.withProcedureName("SPGETPERFXUSUA");
        jdbcSelectAllPerfilesUsuario.returningResultSet("PA_CDATOS", new PerfilesRowMapper());

        jdbcSelectAllPerfilesFijosUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllPerfilesFijosUsuario.withSchemaName(schema);
        jdbcSelectAllPerfilesFijosUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcSelectAllPerfilesFijosUsuario.withProcedureName("SPGETPERFFIJOSXUSUA");
        jdbcSelectAllPerfilesFijosUsuario.returningResultSet("PA_CDATOS", new PerfilesRowMapper());

        jdbcSelectAllPerfilesDisponiblesUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllPerfilesDisponiblesUsuario.withSchemaName(schema);
        jdbcSelectAllPerfilesDisponiblesUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcSelectAllPerfilesDisponiblesUsuario.withProcedureName("SPGETPERFDISPXUSUA");
        jdbcSelectAllPerfilesDisponiblesUsuario.returningResultSet("PA_CDATOS", new PerfilesRowMapper());

        jdbcSelectAllPerfilesFinalesUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllPerfilesFinalesUsuario.withSchemaName(schema);
        jdbcSelectAllPerfilesFinalesUsuario.withCatalogName("PAADMAUSUARIO");
        jdbcSelectAllPerfilesFinalesUsuario.withProcedureName("SPGETPERFFINALXUSUA");
        jdbcSelectAllPerfilesFinalesUsuario.returningResultSet("PA_CDATOS", new PerfilesRowMapper());

        jdbcSelectAllPerfilesVista = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllPerfilesVista.withSchemaName(schema);
        jdbcSelectAllPerfilesVista.withCatalogName("PAADMAPERFIL");
        jdbcSelectAllPerfilesVista.withProcedureName("SPGETDPERVISTA");
        jdbcSelectAllPerfilesVista.returningResultSet("PA_CDATOS", new PerfilesRowMapper());
    }

    public PerfilBase selectRow(Long entityID) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PerfilBase> data = (List<PerfilBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Perfil> selectAllRows() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Perfil>) out.get("PA_CDATOS");
    }

    public AltaPerfil insertRow(ParametrosPerfil entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
            mapSqlParameterSource.addValue("PA_FIESCRITURA", 1);
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaPerfil(dato.intValue());
            } else {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosPerfil entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIESTATUS", 1);
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
            mapSqlParameterSource.addValue("PA_FIESCRITURA", 1);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotUpdatedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public AltaPerfilUsuario insertRowPerfilEmpleado(Integer idPerfil, Integer numeroEmpleado) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", idPerfil);
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
            Map<String, Object> out = jdbcInsertPerfilUsuario.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaPerfilUsuario(dato.intValue());
            } else {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRowPerfilEmpleado(Integer idPerfil, Integer numeroEmpleado) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", idPerfil);
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
            Map<String, Object> out = jdbcDeletePerfilUsuario.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRowPerfilVista(Integer idPerfil, Integer idVista) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPERFIL", idPerfil);
            mapSqlParameterSource.addValue("PA_FIIDVISTA", idVista);
            Map<String, Object> out = jdbcDeletePerfilVista.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public AltaPerfilVistas insertRowPerfilVistas(ParametrosPerfilVistas parametrosPerfilVistas) throws DataNotInsertedException {
        try {
            this.deleteRowPerfilVistas(parametrosPerfilVistas.getIdPerfil());
        } catch (DataNotDeletedException ex) {
            throw new DataNotInsertedException(ex);
        }
        for (ParametroVista parametroVista : parametrosPerfilVistas.getVistas()) {
            this.insertRowPerfilVista(parametrosPerfilVistas.getIdPerfil(), parametroVista);
        }
        return new AltaPerfilVistas(parametrosPerfilVistas.getIdPerfil());
    }

    private AltaPerfilVistas insertRowPerfilVista(Integer idPerfil, ParametroVista parametroVista) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPERFIL", idPerfil);
            mapSqlParameterSource.addValue("PA_FIIDVISTA", parametroVista.getIdVista());
            Map<String, Object> out = jdbcInsertPerfilVistas.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaPerfilVistas(dato.intValue());
            } else {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void deleteRowPerfilVistas(Integer idPerfil) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPERFIL", idPerfil);
            Map<String, Object> out = jdbcDeletePerfilVistas.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List<Perfil> selectAllRowsPerfilesUsuario(Integer numeroEmpleado) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllPerfilesUsuario.execute(mapSqlParameterSource);
        return (List<Perfil>) out.get("PA_CDATOS");
    }

    public List<Perfil> selectAllRowsPerfilesFijosUsuario(Integer numeroEmpleado) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllPerfilesFijosUsuario.execute(mapSqlParameterSource);
        return (List<Perfil>) out.get("PA_CDATOS");
    }

    public List<Perfil> selectAllRowsPerfilesDisponiblesUsuario(Integer numeroEmpleado) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllPerfilesDisponiblesUsuario.execute(mapSqlParameterSource);
        return (List<Perfil>) out.get("PA_CDATOS");
    }

    public List<Perfil> selectAllRowsPerfilesFinalesUsuario(Integer numeroEmpleado) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
        Map<String, Object> out = jdbcSelectAllPerfilesFinalesUsuario.execute(mapSqlParameterSource);
        return (List<Perfil>) out.get("PA_CDATOS");
    }

    public List<Perfil> selectAllRowsPerfilesVista(Integer idVista) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDVISTA", idVista);
        Map<String, Object> out = jdbcSelectAllPerfilesVista.execute(mapSqlParameterSource);
        return (List<Perfil>) out.get("PA_CDATOS");
    }

}
