package com.gs.baz.franquicia.dao;

import com.gs.baz.franquicia.dao.util.GenericDAO;
import com.gs.baz.franquicia.dto.CategoriaDTO;
import com.gs.baz.franquicia.dto.FranquiciaDTO;
import com.gs.baz.franquicia.dto.FranquiciaNegCatDTO;
import com.gs.baz.franquicia.dto.NegocioFranquiciaDTO;
import com.gs.baz.franquicia.mprs.CategoriaRowMapper;
import com.gs.baz.franquicia.mprs.FranquiciaNegCatRowMapper;
import com.gs.baz.franquicia.mprs.FranquiciaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cescobarh
 */
@Component
public class FranquiciaDAOImpl extends DefaultDAO implements GenericDAO<FranquiciaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcSelectByNameCatego;
    private DefaultJdbcCall jdbcSelectFranNegCat;
    private DefaultJdbcCall jdbcInsertFranNegCat;
    private DefaultJdbcCall jdbcDeleteFranNegCat;

    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINFRANQ");
        jdbcSelect.withProcedureName("SP_SEL_FRQ");
        jdbcSelect.returningResultSet("RCL_INFO", new FranquiciaRowMapper());

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINFRANQ");
        jdbcInsert.withProcedureName("SP_INS_FRQ");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINFRANQ");
        jdbcUpdate.withProcedureName("SP_ACT_FRQ");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINFRANQ");
        jdbcDelete.withProcedureName("SP_DEL_FRQ");

        jdbcSelectFranNegCat = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFranNegCat.withSchemaName(schema);
        jdbcSelectFranNegCat.withCatalogName("PAADMINFRQNEG");
        jdbcSelectFranNegCat.withProcedureName("SP_SEL_FRQNEG");
        jdbcSelectFranNegCat.returningResultSet("RCL_INFO", new FranquiciaNegCatRowMapper());

        jdbcInsertFranNegCat = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertFranNegCat.withSchemaName(schema);
        jdbcInsertFranNegCat.withCatalogName("PAADMINFRQNEG");
        jdbcInsertFranNegCat.withProcedureName("SP_INS_FRQNEG");

        jdbcDeleteFranNegCat = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteFranNegCat.withSchemaName(schema);
        jdbcDeleteFranNegCat.withCatalogName("PAADMINFRQNEG");
        jdbcDeleteFranNegCat.withProcedureName("SP_DEL_FRQNEG");

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMINFRANQ");
        jdbcSelectByName.withProcedureName("SP_FRANQUICIA_NOMBRE");
        jdbcSelectByName.returningResultSet("RCL_INFO", new FranquiciaRowMapper());

        jdbcSelectByNameCatego = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByNameCatego.withSchemaName(schema);
        jdbcSelectByNameCatego.withCatalogName("PAADMINCATEGORIA");
        jdbcSelectByNameCatego.withProcedureName("SP_CATEGO_NOMBRE");
        jdbcSelectByNameCatego.returningResultSet("RCL_INFO", new CategoriaRowMapper());

    }

    @Override
    public FranquiciaDTO selectSimpleRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FranquiciaDTO> data = (List<FranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public FranquiciaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FranquiciaDTO> data = (List<FranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            FranquiciaDTO entityDTO = selectRowFrqNegCat(data.get(0));
            return entityDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional(rollbackFor = {CustomException.class})
    public List<FranquiciaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FranquiciaDTO> data = (List<FranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    private FranquiciaDTO selectRowFrqNegCat(FranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getIdFranquicia());
            Map<String, Object> out = jdbcSelectFranNegCat.execute(mapSqlParameterSource);
            List<FranquiciaNegCatDTO> data = (List<FranquiciaNegCatDTO>) out.get("RCL_INFO");
            if (data.size() > 0) {
                List<NegocioFranquiciaDTO> negocios = new ArrayList();
                NegocioFranquiciaDTO negocio;
                List<Integer> idsNegocios = new ArrayList();
                for (FranquiciaNegCatDTO negocioCat : data) {
                    if (!idsNegocios.contains(negocioCat.getIdNegocio())) {
                        negocio = new NegocioFranquiciaDTO();
                        negocio.setIdNegocio(BigDecimal.valueOf(negocioCat.getIdNegocio()));
                        negocios.add(negocio);
                        idsNegocios.add(negocioCat.getIdNegocio());
                    }
                    for (NegocioFranquiciaDTO negocioFranquiciaDTO : negocios) {
                        if (negocioFranquiciaDTO.getIdNegocio().equals(negocioCat.getIdNegocio())) {
                            if (negocioFranquiciaDTO.getIdsCategorias() == null) {
                                negocioFranquiciaDTO.setIdsCategorias(new ArrayList());
                            }
                            negocioFranquiciaDTO.getIdsCategorias().add(negocioCat.getIdCategoria());
                        }
                    }
                }
                entityDTO.setNegociosFranquicia(negocios);
            } else {
                entityDTO.setNegociosFranquicia(new ArrayList());
            }
            return entityDTO;
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ATTRIBUTE_DATA_NOT_FOUND.detalle("Error to select row of Franquicia Negocio"), ex);
        }
    }

    @Override
    @Transactional(rollbackFor = {CustomException.class})
    public FranquiciaDTO insertRow(FranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDSTATUS", entityDTO.getIdStatus());
            mapSqlParameterSource.addValue("PA_FIIDEMCREA", entityDTO.getIdEmpleadoCrea());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdFranquicia((BigDecimal) out.get("PA_FIIDFRANQ"));
                insertRowNegocioCatPorFrq(entityDTO);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of  Franquicia success false"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of  Franquicia"), ex);
        }
    }

    public FranquiciaDTO insertRowNegocioCatPorFrq(FranquiciaDTO entityDTO) throws CustomException {
        try {
            for (NegocioFranquiciaDTO negocio : entityDTO.getNegociosFranquicia()) {
                for (Integer idCategoria : negocio.getIdsCategorias()) {
                    boolean result = insertRowFrqNegCat(entityDTO.getIdFranquicia().longValue(), negocio.getIdNegocio().longValue(), idCategoria.longValue());
                    if (!result) {
                        throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of FrqNeg"));
                    }
                }
            }

            //buscar la categoria gek y negocio gek y traer el id
            CategoriaDTO categoriaGek = this.selectRowByNameCategoria("CATEGORIA GEK");
            if (categoriaGek != null) {
                //insertarlo
                boolean result = insertRowFrqNegCat(entityDTO.getIdFranquicia().longValue(), categoriaGek.getIdNegocio().longValue(), categoriaGek.getIdCategoria().longValue());
                if (!result) {
                    throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of FrqNeg NegGek"));
                }
            }

            return entityDTO;
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to update row of Franquicia"), ex);
        }
    }

    public Boolean insertRowFrqNegCat(Long franquiciaID, Long negocioID, Long categoriaID) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", franquiciaID);
            mapSqlParameterSource.addValue("PA_FIIDNEGO", negocioID);
            mapSqlParameterSource.addValue("PA_FIIDCATEG", categoriaID);
            Map<String, Object> out = jdbcInsertFranNegCat.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            return success;
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of FrqNeg"), ex);
        }
    }

    @Override
    @Transactional(rollbackFor = {CustomException.class})
    public FranquiciaDTO updateRow(FranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getIdFranquicia());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDSTATUS", entityDTO.getIdStatus());
            mapSqlParameterSource.addValue("PA_FIIDEMMOD", entityDTO.getIdEmpleadoModifica());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                boolean result = deleteRowFrqNegCat(entityDTO);
                if (!result) {
                    throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to delete row of Franquicia Neg success false"));
                }
                entityDTO = insertRowNegocioCatPorFrq(entityDTO);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Franquicia success false"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Franquicia"), ex);
        }
    }

    private Boolean deleteRowFrqNegCat(FranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getIdFranquicia());
            Map<String, Object> out = jdbcDeleteFranNegCat.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            return success;
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to delete row of Franquicia Negocio"), ex);
        }
    }

    @Override
    public FranquiciaDTO deleteRow(FranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getIdFranquicia());
            mapSqlParameterSource.addValue("PA_FIIDSTATUS", entityDTO.getIdStatus());
            mapSqlParameterSource.addValue("PA_FIIDEMMOD", entityDTO.getIdEmpleadoModifica());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Franquicia success false"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Franquicia"), ex);
        }
    }

    @Override
    public List<FranquiciaDTO> selectRowsByName(String name) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCDESCRIPCION", name);
        Map<String, Object> out = jdbcSelectByName.execute(mapSqlParameterSource);
        List<FranquiciaDTO> data = (List<FranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public CategoriaDTO selectRowByNameCategoria(String name) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCDESCRIPCION", name);
        Map<String, Object> out = jdbcSelectByNameCatego.execute(mapSqlParameterSource);
        List<CategoriaDTO> data = (List<CategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

}
