/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g008;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class CumplimientoSeccionNivelDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "row_number")
    private Integer rowNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "punto_obtenido")
    private Integer puntoObtenido;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "valor_seccion")
    private Integer valorSeccion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    public String getModulo() {
        return modulo;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getPuntoObtenido() {
        return puntoObtenido;
    }

    public void setPuntoObtenido(BigDecimal puntoObtenido) {
        this.puntoObtenido = (puntoObtenido == null ? null : puntoObtenido.intValue());
    }

    public Integer getValorSeccion() {
        return valorSeccion;
    }

    public void setValorSeccion(BigDecimal valorSeccion) {
        this.valorSeccion = (valorSeccion == null ? null : valorSeccion.intValue());
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(BigDecimal calificacion) {
        this.calificacion = (calificacion == null ? null : calificacion.intValue());
    }

    @Override
    public String toString() {
        return "CumplimientoSeccionNivelDTO{" + "rowNumber=" + rowNumber + ", modulo=" + modulo + ", puntoObtenido=" + puntoObtenido + ", valorSeccion=" + valorSeccion + ", calificacion=" + calificacion + '}';
    }

}
