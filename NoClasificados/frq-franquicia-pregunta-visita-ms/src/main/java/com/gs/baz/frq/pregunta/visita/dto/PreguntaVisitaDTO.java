/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.pregunta.visita.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PreguntaVisitaDTO {

    @JsonProperty(value = "id_bitacora")
    private Integer idBitacora;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_pregunta")
    private Integer idPregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pregunta")
    private String pregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "respuesta")
    private String respuesta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_diciplina")
    private Integer idNegocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pondetacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "incumplimiento")
    private Integer incumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_checklist")
    private Integer idChecklist;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "imperdonable")
    private Integer imperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "reglaNueve")
    private Integer reglaNueve;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_defecto")
    private Integer idDefecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oportunidad")
    private Integer oportunidad;

    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(BigDecimal idBitacora) {
        this.idBitacora = (idBitacora == null ? null : idBitacora.intValue());
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(BigDecimal idPregunta) {
        this.idPregunta = (idPregunta == null ? null : idPregunta.intValue());
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getIdNegocioDisciplina() {
        return idNegocioDisciplina;
    }

    public void setIdNegocioDisciplina(BigDecimal idNegocioDisciplina) {
        this.idNegocioDisciplina = (idNegocioDisciplina == null ? null : idNegocioDisciplina.intValue());
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getIncumplimiento() {
        return incumplimiento;
    }

    public void setIncumplimiento(BigDecimal incumplimiento) {
        this.incumplimiento = (incumplimiento == null ? null : incumplimiento.intValue());
    }

    public Integer getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(BigDecimal idChecklist) {
        this.idChecklist = (idChecklist == null ? null : idChecklist.intValue());
    }

    public Integer getImperdonable() {
        return imperdonable;
    }

    public void setImperdonable(BigDecimal imperdonable) {
        this.imperdonable = (imperdonable == null ? null : imperdonable.intValue());
    }

    public Integer getReglaNueve() {
        return reglaNueve;
    }

    public void setReglaNueve(BigDecimal reglaNueve) {
        this.reglaNueve = (reglaNueve == null ? null : reglaNueve.intValue());
    }

    public Integer getIdDefecto() {
        return idDefecto;
    }

    public void setIdDefecto(BigDecimal idDefecto) {
        this.idDefecto = (idDefecto == null ? null : idDefecto.intValue());
    }

    public Integer getOportunidad() {
        return oportunidad;
    }

    public void setOportunidad(BigDecimal oportunidad) {
        this.oportunidad = (oportunidad == null ? null : oportunidad.intValue());
    }

    @Override
    public String toString() {
        return "PreguntaVisitaDTO{" + "idBitacora=" + idBitacora + ", idProtocolo=" + idProtocolo + ", fecha=" + fecha + ", idPregunta=" + idPregunta + ", pregunta=" + pregunta + ", respuesta=" + respuesta + ", idNegocioDisciplina=" + idNegocioDisciplina + ", ponderacion=" + ponderacion + ", modulo=" + modulo + ", incumplimiento=" + incumplimiento + ", idChecklist=" + idChecklist + ", imperdonable=" + imperdonable + ", reglaNueve=" + reglaNueve + ", idDefecto=" + idDefecto + ", oportunidad=" + oportunidad + '}';
    }

}
