/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de geografias del geografia", value = "geografias")
public class Geografias {

    @JsonProperty(value = "geografias")
    @ApiModelProperty(notes = "geografias")
    private List<Geografia> geografias;

    public Geografias(List<Geografia> geografias) {
        this.geografias = geografias;
    }

    public List<Geografia> getGeografias() {
        return geografias;
    }

    public void setGeografias(List<Geografia> geografias) {
        this.geografias = geografias;
    }
}
