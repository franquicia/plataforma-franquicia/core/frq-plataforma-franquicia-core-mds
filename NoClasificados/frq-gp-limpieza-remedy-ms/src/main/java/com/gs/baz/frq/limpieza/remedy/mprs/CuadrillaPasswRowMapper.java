package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.CuadrillaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CuadrillaPasswRowMapper implements RowMapper<CuadrillaDTO> {

    @Override
    public CuadrillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CuadrillaDTO cuad = new CuadrillaDTO();

        cuad.setIdCuadrilla(rs.getInt("FIID_CUADRILLA"));
        cuad.setIdProveedor(rs.getInt("FIID_PROVEEDOR"));
        cuad.setZona(rs.getInt("FIID_ZONA"));
        cuad.setLiderCuad(rs.getString("FCLIDER_CUAD"));
        cuad.setPs5(rs.getString("FCPASSW"));
        cuad.setCorreo(rs.getString("FCCORREO"));
        cuad.setSegundo(rs.getString("FCSEG_CARGO"));

        return cuad;
    }

}
