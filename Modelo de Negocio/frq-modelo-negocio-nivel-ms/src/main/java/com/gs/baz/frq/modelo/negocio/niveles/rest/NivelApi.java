/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.niveles.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.niveles.dao.NivelDAOImpl;
import com.gs.baz.frq.modelo.negocio.niveles.dto.AltaNivel;
import com.gs.baz.frq.modelo.negocio.niveles.dto.NivelBase;
import com.gs.baz.frq.modelo.negocio.niveles.dto.Niveles;
import com.gs.baz.frq.modelo.negocio.niveles.dto.ParametrosNivel;
import com.gs.baz.frq.modelo.negocio.niveles.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "catalogos-nivel", value = "catalogos-nivel", description = "Gestión del catalogo de niveles")
@RestController
@RequestMapping("/api-local/modelo-negocio/catalogos/nivel/v1")
public class NivelApi {

    @Autowired
    private NivelDAOImpl nivelDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idNivel
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene nivel", notes = "Obtiene un nivel", nickname = "obtieneNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idNivel}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NivelBase obtieneModelo(@ApiParam(name = "idNivel", value = "Identificador del nivel", example = "1", required = true) @PathVariable("idNivel") Long idNivel) throws CustomException, DataNotFoundException {
        NivelBase nivelBase = nivelDAOImpl.selectRow(idNivel);
        if (nivelBase == null) {
            throw new DataNotFoundException();
        }
        return nivelBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene nivels", notes = "Obtiene todos los nivels", nickname = "obtieneNiveles")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Niveles obtieneModelos() throws CustomException, DataNotFoundException {
        Niveles niveles = new Niveles(nivelDAOImpl.selectAllRows());
        if (niveles.getNiveles() != null && niveles.getNiveles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return niveles;
    }

    /**
     *
     * @param parametrosNivel
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear nivel", notes = "Agrega un nivel", nickname = "creaNivel")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaNivel creaNivel(@ApiParam(name = "ParametrosNivel", value = "Paramentros para el alta del nivel", required = true) @RequestBody ParametrosNivel parametrosNivel) throws DataNotInsertedException {
        return nivelDAOImpl.insertRow(parametrosNivel);
    }

    /**
     *
     * @param parametrosNivel
     * @param idNivel
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar nivel", notes = "Actualiza un nivel", nickname = "actualizaNivel")
    @RequestMapping(value = "/{idNivel}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaNivel(@ApiParam(name = "ParametrosNivel", value = "Paramentros para la actualización del nivel", required = true) @RequestBody ParametrosNivel parametrosNivel, @ApiParam(name = "idNivel", value = "Identificador del nivel", example = "1", required = true) @PathVariable("idNivel") Integer idNivel) throws DataNotUpdatedException {
        parametrosNivel.setIdNivel(idNivel);
        nivelDAOImpl.updateRow(parametrosNivel);
        return new SinResultado();
    }

    /**
     *
     * @param idNivel
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar nivel", notes = "Elimina un item de los nivels", nickname = "eliminaNivel")
    @RequestMapping(value = "/{idNivel}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaNivel(@ApiParam(name = "idNivel", value = "Identificador del nivel", example = "1", required = true) @PathVariable("idNivel") Integer idNivel) throws DataNotDeletedException {
        nivelDAOImpl.deleteRow(idNivel);
        return new SinResultado();
    }

}
