/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.modelo.negocio.firmas.dto.AltaFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.Firma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.FirmaBase;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosEstadoFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosVisualizacionFirma;
import com.gs.baz.frq.modelo.negocio.firmas.mprs.FirmaRowMapper;
import com.gs.baz.frq.modelo.negocio.firmas.mprs.FirmasRowMapper;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class FirmaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcUpdateVisualizacion;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNFIRMAS";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNFIRMAS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNFIRMAS");

        jdbcUpdateVisualizacion = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdateVisualizacion.withSchemaName(schema);
        jdbcUpdateVisualizacion.withCatalogName(catalogo);
        jdbcUpdateVisualizacion.withProcedureName("SPACTVISUALIZADO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNFIRMAS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETFIRMASXFOL");
        jdbcSelect.returningResultSet("PA_CDATOS", new FirmaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETFIRMASXFOL");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new FirmasRowMapper());

    }

    public FirmaBase selectRow(Long idFolio, Long idFirma) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDFIRMA", idFirma);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FirmaBase> data = (List<FirmaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Firma> selectAllRows(Long idFolio) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDFIRMA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Firma>) out.get("PA_CDATOS");
    }

    public AltaFirma insertRow(ParametrosFirma entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO.getIdFirma() != null ? entityDTO.getIdFirma() : 0);
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FINUMEROEMPLEADO", entityDTO.getNumeroEmpleado());
            mapSqlParameterSource.addValue("PA_FIIDJERARQUIA", entityDTO.getIdJerarquia());
            mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", 1);
            mapSqlParameterSource.addValue("PA_FIELIMINADO", 0);
            mapSqlParameterSource.addValue("PA_FIINDICE", entityDTO.getOrdenFirma());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDFIRMA");
                return new AltaFirma(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosFirma entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO.getIdFirma());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FINUMEROEMPLEADO", entityDTO.getNumeroEmpleado());
            mapSqlParameterSource.addValue("PA_FIIDJERARQUIA", entityDTO.getIdJerarquia());
            mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", null);
            mapSqlParameterSource.addValue("PA_FIELIMINADO", 0);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Map<String, BigDecimal> updateEstadoRow(ParametrosEstadoFirma entityDTO) throws DataNotUpdatedException, CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO.getIdFirma());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FIINDICE", null);
            mapSqlParameterSource.addValue("PA_FINUMEROEMPLEADO", null);
            mapSqlParameterSource.addValue("PA_FIIDJERARQUIA", null);
            mapSqlParameterSource.addValue("PA_FIIDESTADOFIRMA", entityDTO.getIdEstadoFirma());
            mapSqlParameterSource.addValue("PA_FIELIMINADO", 0);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            BigDecimal idEstadoFolioOut = ((BigDecimal) out.get("PA_EDOFOLIO"));
            BigDecimal empleadoSiguiente = ((out.get("PA_EMPLEADOSIG") != null) ? (BigDecimal) out.get("PA_EMPLEADOSIG") : null);
            if (idEstadoFolioOut != null) {
                Map<String, BigDecimal> mapaRetorno = new HashMap<>();
                mapaRetorno.put("ESTADO_FOLIO", idEstadoFolioOut);
                mapaRetorno.put("EMPLEADO_SIGUIENTE", empleadoSiguiente);
                return mapaRetorno;
            }
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (DataNotUpdatedException | NumberFormatException ex) {
            throw ex;
        }

        return null;
    }

    public void updateVisualizacionRow(ParametrosVisualizacionFirma entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO.getIdFirma());
            mapSqlParameterSource.addValue("PA_FIVISUALIZADO", entityDTO.getVisualizado());
            Map<String, Object> out = jdbcUpdateVisualizacion.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
