/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.modelos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del modelo", value = "Modelo")
public class Modelo extends ModeloBase {

    @JsonProperty(value = "idModelo")
    @ApiModelProperty(notes = "Identificador del modelo", example = "1", position = -1)
    private Integer idModelo;

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Integer idModelo) {
        this.idModelo = idModelo;
    }

    @Override
    public String toString() {
        return "Modelo{" + "idModelo=" + idModelo + '}';
    }

}
