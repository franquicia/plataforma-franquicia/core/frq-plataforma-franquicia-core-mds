/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.model.bucket.client.services.dto.FranquiciaDTO;
import com.gs.baz.model.bucket.client.services.util.DefaultRestTemplate;
import com.gs.baz.model.bucket.client.services.util.Relasionship;
import java.io.IOException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

/**
 *
 * @author B73601
 */
public class SubServiceFranquicia extends DefaultRestTemplate {

    public FranquiciaDTO getFranquicia(Integer idFranquicia) throws CustomException {
        try {
            responseString = restTemplate.exchange(Relasionship.FRANQUICIA.url(basePath) + "/" + idFranquicia, HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    if (responseStringBody.get("data") != null) {
                        return objectMapper.readValue(responseStringBody.get("data").toString(), FranquiciaDTO.class);
                    } else {
                        return null;
                    }
                } else {
                    throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("Franquicia response code error"));
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("Franquicia  response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

    public String getFranquiciaById(Integer idFranquicia) throws CustomException {
        try {
            responseString = restTemplate.exchange(Relasionship.FRANQUICIADATA.url(basePath) + "/" + idFranquicia, HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    if (responseStringBody.get("data") != null) {
                        // return objectMapper.readValue(responseStringBody.get("data").toString(), FranquiciaGeneralDTO.class);
                        return responseStringBody.get("data").get("franquicia").toString();
                    } else {
                        return null;
                    }
                } else {
                    throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("Franquicia Data response code error"));
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("Franquicia Data response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

}
