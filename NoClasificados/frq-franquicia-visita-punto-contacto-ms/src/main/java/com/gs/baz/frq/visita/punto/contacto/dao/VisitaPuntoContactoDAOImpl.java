package com.gs.baz.frq.visita.punto.contacto.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.visita.punto.contacto.dao.util.GenericDAO;
import com.gs.baz.frq.visita.punto.contacto.dto.VisitaPuntoContactoDTO;
import com.gs.baz.frq.visita.punto.contacto.mprs.DetalleRowMapper;
import com.gs.baz.frq.visita.punto.contacto.mprs.VisitaPuntoContactoRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class VisitaPuntoContactoDAOImpl extends DefaultDAO implements GenericDAO<VisitaPuntoContactoDTO> {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbGetDetalle;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMVISITAS");
        jdbcSelect.withProcedureName("SPGETVISITAS");
        jdbcSelect.returningResultSet("PA_CDATOS", new VisitaPuntoContactoRowMapper());

        jdbGetDetalle = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbGetDetalle.withSchemaName(schema);
        jdbGetDetalle.withCatalogName("PAADMVISITAS");
        jdbGetDetalle.withProcedureName("SPGETDETALLE");
        jdbGetDetalle.returningResultSet("PA_CDATOS", new DetalleRowMapper());
    }

    @Override
    public List<VisitaPuntoContactoDTO> selectRows(VisitaPuntoContactoDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_BITACORA", entityDTO.getIdBitacora());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<VisitaPuntoContactoDTO>) out.get("PA_CDATOS");
    }

    public List<VisitaPuntoContactoDTO> getDetalle(VisitaPuntoContactoDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        Map<String, Object> out = jdbGetDetalle.execute(mapSqlParameterSource);
        return (List<VisitaPuntoContactoDTO>) out.get("PA_CDATOS");
    }
}
