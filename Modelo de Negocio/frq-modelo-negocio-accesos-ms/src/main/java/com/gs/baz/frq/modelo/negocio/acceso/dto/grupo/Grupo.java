/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.grupo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del grupo", value = "Grupo")
public class Grupo extends GrupoBase {

    @JsonProperty(value = "idGrupo")
    @ApiModelProperty(notes = "Identificador del grupo", example = "1", position = -1)
    private Integer idGrupo;

    public Grupo() {
    }

    public Grupo(Integer idGrupo, String descripcion) {
        this.idGrupo = idGrupo;
        this.descripcion = descripcion;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public String toString() {
        return "Grupo{" + "idGrupo=" + idGrupo + '}';
    }

}
