/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.servicios.servidor;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MADA
 */
@Api(tags = "identificador-unico-cliente", value = "identificador-unico-cliente", description = "API dedicada al consumo de información a través del hostpot ubicado en tiendas.")
@RestController
@RequestMapping("portal-cautivo/v1")
public class FRQServicioIdentificador {
    /*
    @Autowired
    IdentificadorBI localizacionesBI;

    private final Logger logger = LogManager.getLogger();

    @ApiOperation(value = "Descarga las localizaciones de los clientes conectados a los AP", notes = "Descarga las localizaciones de los clientes conectados a los AP", nickname = "identificador-unico-cliente")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class)
        ,
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/identificador-unico-cliente", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void postLocalizacionesActivas(
            @ApiParam(name = "idPropietario", value = "Identificador de Extreme Cloud", required = true)
            @PathVariable("idPropietario") Long idPropietario,
            @ApiParam(name = "macsDispositivos", value = "Macs de los dispositivos", required = true)
            @RequestParam(name = "macsDispositivos", required = true) String macsDispositivos
    ) throws CustomException, InterruptedException, ExecutionException {

        String[] macsAP = {
            "C413E2D47A80,C413E29F0FC0,BCF3104EEB00,C413E2A01E00",
            "C413E29F0E80,D854A2D544C0,348584050980,3485840A8280",
            "3485840A87C0,3485840A6F40,3485840543C0,BCF3104EAD40",
            "BCF310504F40,C413E27E37C0,BCF31049B840,C8675E2C2700",
            "BCF3104EE780,C8675E1D5880,C8675E6D5B80,F4EAB59C7A80",
            "F4EAB59C6E80,F4EAB59C6E40,C8675E6B7BC0,3485840AA7C0",
            "D854A2D5A400,C413E29EC400,C413E29F0340,C8675E15FC00",
            "C8675E2BFE00,C413E29EBAC0,BCF310697FC0,C413E29E9E00",
            "C8675E6D3840,C8675E6BDD40,C413E2C4E600,C413E29F7F80",
            "C413E29EBA40,C8675E6D2CC0,F4EAB5509D40,C413E27DA600",
            "C8675E2C0AC0,F4EAB5512940,D854A22A0880,C8675E2C0B00",
            "C8675E2C11C0,C413E29EB3C0,D854A22A1D00,C8675E6D6CC0",
            "C8675E1D5800,C413E29F7980,C413E2C4E700,C8675E6D5F40",
            "C413E29F7800,C413E29EB040,C8675E2C1C00,F4EAB5506E00",
            "C413E2D56480,D854A2D72E80,C8675E6D2EC0,D854A22A1EC0",
            "C413E29ED280,C413E29F8200,C413E29E9B00,C413E29F9400",
            "D854A2D54240,C413E27E0280,F4EAB59C7140,C8675E6BE580",
            "C413E2A015C0,C413E29F7180,D854A2D58900,C413E29F9880",
            "D854A2D5E7C0,C8675E6BCDC0,C8675E6D35C0,C8675E6F1780"};

        int i = 0;

        while (true) {

            //for (int i = 0; i < macsAP.length; i++) {
            //logger.debug("Inicio Consumo de Servicio Localizacion " + i + " = " + macsAP[i]);
            Boolean error = localizacionesBI.insertarLocalizacionesV1(idPropietario, macsAP[i]);
            //logger.debug("Fin Consumo de Servicio Localizacion " + i + " = " + macsAP[i]);
            if (error == false) {
                Thread.sleep(Util.tiempo / 2);
                
            } else {
                Thread.sleep(Util.tiempo / 2);
                logger.debug("Error al consumo del servicio MACS = " + macsAP[i]);
            }

            i++;

            //}
            if (i == macsAP.length) {
                i = 0;
                logger.debug("Reinicio de consumo de servicios");
            }

        }

        /*Boolean respuesta = localizacionesBI.insertarLocalizacionesV1(idPropietario, macsDispositivos);

        if (!respuesta) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  LocalizacionesWifi"));
        }

        return new RespuestaLessResult200();*/
 /*
    }

    @ApiOperation(value = "Descarga las localizaciones v3 de los clientes conectados a los AP", notes = "Descarga las localizaciones de los clientes conectados a los AP", nickname = "location-clients")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class)
        ,
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/localizaciones-nuevas/{idPropietario}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void postLocalizacionesV3(
            @ApiParam(name = "idPropietario", value = "Identificador de Extreme Cloud", required = true)
            @PathVariable("idPropietario") Long idPropietario,
            @ApiParam(name = "macsDispositivos", value = "Macs de los dispositivos", required = true)
            @RequestParam(name = "macsDispositivos", required = true) String macsDispositivos
    ) throws CustomException, InterruptedException, ExecutionException {

        String[] macsAP = {"C413E2D47A80", "C413E29F0FC0", "BCF3104EEB00", "C413E2A01E00", "C413E29F0E80", "D854A2D544C0", "3.48584E+11", "3485840A8280", "3485840A87C0", "3485840A6F40", "3485840543C0", "BCF3104EAD40", "BCF310504F40", "C413E27E37C0", "BCF31049B840", "C8675E2C2700", "BCF3104EE780", "C8675E1D5880", "C8675E6D5B80", "F4EAB59C7A80", "F4EAB59C6E80", "F4EAB59C6E40", "C8675E6B7BC0", "3485840AA7C0", "D854A2D5A400", "C413E29EC400", "C413E29F0340", "C8675E15FC00", "C8675E2BFE00", "C413E29EBAC0", "BCF310697FC0", "C413E29E9E00", "C8675E6D3840", "C8675E6BDD40", "C413E2C4E600", "C413E29F7F80", "C413E29EBA40", "C8675E6D2CC0", "F4EAB5509D40", "C413E27DA600", "C8675E2C0AC0", "F4EAB5512940", "D854A22A0880", "C8675E2C0B00", "C8675E2C11C0", "C413E29EB3C0", "D854A22A1D00", "C8675E6D6CC0", "C8675E1D5800", "C413E29F7980", "C413E2C4E700", "C8675E6D5F40", "C413E29F7800", "C413E29EB040", "C8675E2C1C00", "F4EAB5506E00", "C413E2D56480", "D854A2D72E80", "C8675E6D2EC0", "D854A22A1EC0", "C413E29ED280", "C413E29F8200", "C413E29E9B00", "C413E29F9400", "D854A2D54240", "C413E27E0280", "F4EAB59C7140", "C8675E6BE580", "C413E2A015C0", "C413E29F7180", "D854A2D58900", "C413E29F9880", "D854A2D5E7C0", "C8675E6BCDC0", "C8675E6D35C0", "C8675E6F1780"};

        while (true) {

            for (int i = 0; i < macsAP.length; i++) {
                logger.debug("Inicio Consumo de Servicio Localizacion " + i + " = " + macsAP[i]);
                localizacionesBI.insertarLocalizacionesV3(idPropietario, macsAP[i]);
                logger.debug("Fin Consumo de Servicio Localizacion " + i + " = " + macsAP[i]);
                Thread.sleep(1000);
            }

        }

        /*Boolean respuesta = localizacionesBI.insertarLocalizacionesV3(idPropietario, macsDispositivos);

        if (!respuesta) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  LocalizacionesWifi"));
        }

        return new RespuestaLessResult200();

    }
     */
}
