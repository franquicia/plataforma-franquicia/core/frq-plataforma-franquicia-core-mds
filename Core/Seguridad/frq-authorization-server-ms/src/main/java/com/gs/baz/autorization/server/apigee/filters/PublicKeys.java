/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.filters;

import com.gs.baz.frq.model.commons.Environment;

/**
 *
 * @author cescobarh
 */
public enum PublicKeys {

    PUBLIC_KEY_UTILERIAS("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3bPE7zjnPFzdgzhRCKv+/goNfiADk6bevg8Qsu9g5YntP2j/e5SVJBo+c/FSahhtW0fNm2Z3X7y35sjcTyORYa/RRd2tvCIFW8KVJ8eAVa6GFQcSZt2n4ozMDpy1QQcT5x5kyJ0QSwrKlJSzXO9fin2furIM/EelZaR0lxqgXAXmcOer189K5EtpD086XKbllk9U0eHRIQRPzOU2P+jurj7X7myDAOcxPlU43fhqw0zuNoGHiAHO55WA0p6G0WiDWe/SDwZpJk7pIw+Ojn0JG3RlNEXE2tGhmyNaXANQmTq03pWESRdzguySxZG8yuB8pELvQlzAHYytEHaxsr2IfQIDAQAB"),
    PUBLIC_KEY_SEGURIDAD("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2zcZ6Qp2qNwAxR4mmi04Jw0u/u7qLbSGFa0rVtQgtyxNoTpLwrPCCmi6/PDZdFRWLzh5fqz3l7SVjEnljauvFSMxavioRc7q3nGhdiuB4NVzXTegCs2Po3gBI5x17f61UL++v5/F5BbFt9y29gktZDifPV2C7U/ukTq9cRTZZUcqEulRa5aqzldMJRNyiyGEUBOPmxtnELmiIVXcwUSJTte7rVE9FcfoJ3Tobw5wb5WBx4di6XYoi+p0mrSGvTZWGWisfKagTHNFcJip5hMpyRHnk62DEjYAX7S9iRxKHEw4a/j0zDRO0B5ot7l1Wh9LbGo0x6j5/bbpQIAxuzO/pwIDAQAB"),
    PUBLIC_KEY_GESTION_INCIDENTES_LIMPIEZA("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqMQmuDytBFq8aLTbKhgIGQDmzKpY/eBHOafuTWtlPi9txdOVT1PTNVzXZCRxEGUn1Fs/MJ49qzBurxQbO+TpKzqsLV4pdqF1hxBTNxvQdbR3pb+p2H1pLrK1f2s5hmmnlj/48qeThkArGcHtxiMRuqZ5xey8FhCrvmuXgdkE6/bC/SlslfUWCVpcsdVh9unpcBB7k8j4WvrM1wv30iMjQiUp5x5zpKXwD1T8+iyMLCu1P7TcHvniqAXiB64BlSTfihiFdGK1LIYu5CawWrQp2FFkH26xp+6aVtJfEnWIu77VMx9eAUws4c/QUuUX/TrWd8Dq/eent1RSWc9CLhfBVwIDAQAB"),
    PUBLIC_KEY_GESTION_INCIDENTES_CATALOGOS("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAynsBcfx4NCG0+OjCDdou9KqtJ+BxVpI3j2ymGn8oZVJASoiNaJX0AaHUyE6y90MgKVQyQsuaZ/2u2l09ZrwANboYQw5f5PfMQfbhO33ylQmEfQh1rzs+4FoyVSwanzXGHLDK1COp+DxXZx9PzvX5VpqkV3zAWBrix/xMDJfLCHwvOQ9oztR35vVeYbCaDuESLOSc/7/CkjDxQCjp8AxmiEYKfD6oPIoCJUL5Ya7hctjTgCHVZkZi9ZlnAWkkH5j4JCNcW4+YwD5xQx4N8aFhW8ZzKIL0afhA09O0guRf9JgR7rWM5d2kji9JupjWcL0B0Pc3rI+GZ0BB4Mxl/m4FJQIDAQAB");

    private final String value;

    PublicKeys(String value) {
        String testValue = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyGpkUJ6m2WLm4WMHtpszcd3TxjLOJJDRVnv4GA7lROcz3xCe94IjCEzofVkafgqMYwCKKtjFHJZzGvV6Pe39jacBp4a5YZRv5/3ZgqnEo5sllNTbOblLCnSiiXzYx+slKYgcPr4zGFSROQekSdEjDCRmfiuyKKroutsJCNYXJiT8RrWl/jmyyT4Fn6kSnT6QP4fvQ7jhI55T+B/c5hTeYphpwe5dX1mYJr2swjy0bSZYQwSGXUk/W7jsTpFQjM8eT7W+hQJHZKVOFTntnEC7UdRUNxGN6Q2kre7I7lOVREXj9/bH+sboJk26Vu7G1pv3QHL7C/p9/SBjO8uWX+TbjQIDAQAB";
        if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
            this.value = value;
        } else if (Environment.CURRENT.valueReal().equals(Environment.QUALITY)) {
            this.value = testValue;
        } else {
            this.value = testValue;
        }
    }

    public String value() {
        return value;
    }

}
