package com.gs.baz.frq.ubicacion.sucursal.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.gs.baz.frq.ubicacion.sucursal.dao.UbicacionCecoDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.ubicacion.sucursal")
public class ConfigUbicacionSucursal {

    private final Logger logger = LogManager.getLogger();

    public ConfigUbicacionSucursal() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqUbicacionCecoDAOImpl")
    public UbicacionCecoDAOImpl ubicacionCecoDAOImpl() {
        return new UbicacionCecoDAOImpl();
    }
}
