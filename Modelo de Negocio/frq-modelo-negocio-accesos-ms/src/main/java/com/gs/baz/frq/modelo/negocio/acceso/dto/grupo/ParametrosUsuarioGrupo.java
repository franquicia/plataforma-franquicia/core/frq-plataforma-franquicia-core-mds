/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.grupo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosUsuarioGrupo {

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Numero de empleado", example = "202622")
    private Integer numeroEmpleado;

    private transient Integer idGrupo;

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public String toString() {
        return "ParametrosUsuarioGrupo{" + "numeroEmpleado=" + numeroEmpleado + ", idGrupo=" + idGrupo + '}';
    }

}
