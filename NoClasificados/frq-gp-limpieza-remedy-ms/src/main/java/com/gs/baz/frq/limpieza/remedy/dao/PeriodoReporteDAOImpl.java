package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.PeriodoReporteDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.PeriodoReporteRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PeriodoReporteDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertaPeriodoReporte;
    private DefaultJdbcCall jdbcActualizaPeriodoReporte;
    private DefaultJdbcCall jdbcEliminaPeriodoReporte;
    private DefaultJdbcCall jdbcBuscaPeriodoReporte;

    private static final Logger LOGGER = LogManager.getLogger();

    public PeriodoReporteDAOImpl() {
        super();

    }

    @SuppressWarnings("all")
    public void init() {

        jdbcBuscaPeriodoReporte = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODOREP")
                .withProcedureName("SP_SEL_PERIODOREP")
                .returningResultSet("RCL_PERIODO_REP", new PeriodoReporteRowMapper());

        jdbcInsertaPeriodoReporte = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODOREP")
                .withProcedureName("SP_INS_PERIODOREP");

        jdbcActualizaPeriodoReporte = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODOREP")
                .withProcedureName("SP_ACT_PERIODOREP");

        jdbcEliminaPeriodoReporte = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPERIODOREP")
                .withProcedureName("SP_DEL_PERIODOREP");

    }

    @SuppressWarnings("all")
    public List<PeriodoReporteDTO> obtienePeriodoReporte(String idsPerRep, String idPeriodo, String idReporte)
            throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;
        List<PeriodoReporteDTO> listaPeriodoReporte;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPER_REP", idsPerRep)
                .addValue("PA_IDPERIODO", idPeriodo)
                .addValue("PA_IDREPORTE", idReporte);

        out = jdbcBuscaPeriodoReporte.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPERIODOREP.SP_SEL_PERIODOREP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaPeriodoReporte = (List<PeriodoReporteDTO>) out.get("RCL_PERIODO_REP");

        if (respuesta == 0) {
            LOGGER.info("Algo paso al consular el reporte periodo ");
        }

        return listaPeriodoReporte;
    }

    @SuppressWarnings("all")
    public boolean insertaPeriodoReporte(int idPeriodo, int idReporte, String horario, int idPerRep, int commit)
            throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPERIODO", idPeriodo)
                .addValue("PA_IDREPORTE", idReporte)
                .addValue("PA_HORARIO", horario)
                .addValue("PA_IDPER_REP", idPerRep)
                .addValue("PA_COMMIT", commit);

        out = jdbcInsertaPeriodoReporte.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPERIODOREP.SP_INS_PERIODOREP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            LOGGER.info("Algo paso al insertar el reporte periodo");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("all")
    public boolean actualizaPeriodoReporte(int idPeriodo, int idReporte, String horario, int idPerRep, int commit)
            throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPERIODO", idPeriodo)
                .addValue("PA_IDREPORTE", idReporte)
                .addValue("PA_HORARIO", horario)
                .addValue("PA_IDPER_REP", idPerRep)
                .addValue("PA_COMMIT", commit);

        out = jdbcActualizaPeriodoReporte.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPERIODOREP.SP_ACT_PERIODOREP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            LOGGER.info("Algo paso al actualizar el reporte periodo ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("all")
    public boolean eliminaPeriodoReporte(String idPerRep) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPER_REP", idPerRep);

        out = jdbcEliminaPeriodoReporte.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPERIODOREP.SP_DEL_PERIODOREP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            LOGGER.info("Algo paso al elimnar el reporte periodo ");
        } else {
            return true;
        }

        return false;
    }

}
