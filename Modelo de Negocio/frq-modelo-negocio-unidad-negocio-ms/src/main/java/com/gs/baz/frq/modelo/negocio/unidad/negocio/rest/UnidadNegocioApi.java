/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.dao.UnidadNegocioDAOImpl;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.AltaUnidadNegocio;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.ParametrosUnidadNegocio;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.SinResultado;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.UnidadNegocioBase;
import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.UnidadesNegocio;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "unidades-negocios", value = "unidades-negocios", description = "Gestión del catalogo de unidades negocio")
@RestController
@RequestMapping("/api-local/unidades-negocios/v1")
public class UnidadNegocioApi {

    @Autowired
    private UnidadNegocioDAOImpl unidadNegocioDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idUnidadNegocio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene unidadNegocio", notes = "Obtiene un unidadNegocio", nickname = "obtieneUnidadNegocio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idUnidadNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UnidadNegocioBase obtieneUnidadNegocio(@ApiParam(name = "idUnidadNegocio", value = "Identificador del unidadNegocio", example = "1", required = true) @PathVariable("idUnidadNegocio") Long idUnidadNegocio) throws CustomException, DataNotFoundException {
        UnidadNegocioBase unidadNegocioBase = unidadNegocioDAOImpl.selectRow(idUnidadNegocio);
        if (unidadNegocioBase == null) {
            throw new DataNotFoundException();
        }
        return unidadNegocioBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene unidadesNegocio", notes = "Obtiene todos los unidadesNegocio", nickname = "obtieneUnidadesNegocio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UnidadesNegocio obtieneUnidadesNegocio() throws CustomException, DataNotFoundException {
        UnidadesNegocio unidadesNegocio = new UnidadesNegocio(unidadNegocioDAOImpl.selectAllRows());
        if (unidadesNegocio.getUnidadesNegocio() != null && unidadesNegocio.getUnidadesNegocio().isEmpty()) {
            throw new DataNotFoundException();
        }
        return unidadesNegocio;
    }

    /**
     *
     * @param parametrosUnidadNegocio
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear unidadNegocio", notes = "Agrega un unidadNegocio", nickname = "creaUnidadNegocio")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaUnidadNegocio creaUnidadNegocio(@ApiParam(name = "ParametrosUnidadNegocio", value = "Paramentros para el alta del unidadNegocio", required = true) @RequestBody ParametrosUnidadNegocio parametrosUnidadNegocio) throws DataNotInsertedException {
        return unidadNegocioDAOImpl.insertRow(parametrosUnidadNegocio);
    }

    /**
     *
     * @param parametrosUnidadNegocio
     * @param idUnidadNegocio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar unidadNegocio", notes = "Actualiza un unidadNegocio", nickname = "actualizaUnidadNegocio")
    @RequestMapping(value = "/{idUnidadNegocio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaUnidadNegocio(@ApiParam(name = "ParametrosUnidadNegocio", value = "Paramentros para la actualización del unidadNegocio", required = true) @RequestBody ParametrosUnidadNegocio parametrosUnidadNegocio, @ApiParam(name = "idUnidadNegocio", value = "Identificador del unidadNegocio", example = "1", required = true) @PathVariable("idUnidadNegocio") Integer idUnidadNegocio) throws DataNotUpdatedException {
        parametrosUnidadNegocio.setIdUnidadNegocio(idUnidadNegocio);
        unidadNegocioDAOImpl.updateRow(parametrosUnidadNegocio);
        return new SinResultado();
    }

    /**
     *
     * @param idUnidadNegocio
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar unidadNegocio", notes = "Elimina un item de los unidadesNegocio", nickname = "eliminaUnidadNegocio")
    @RequestMapping(value = "/{idUnidadNegocio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaUnidadNegocio(@ApiParam(name = "idUnidadNegocio", value = "Identificador del unidadNegocio", example = "1", required = true) @PathVariable("idUnidadNegocio") Integer idUnidadNegocio) throws DataNotDeletedException {
        unidadNegocioDAOImpl.deleteRow(idUnidadNegocio);
        return new SinResultado();
    }

}
