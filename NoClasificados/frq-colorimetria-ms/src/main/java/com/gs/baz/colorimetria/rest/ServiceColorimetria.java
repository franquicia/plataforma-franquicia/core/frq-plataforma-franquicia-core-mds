/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.colorimetria.rest;

import com.gs.baz.colorimetria.dao.ColorimetriaDAOImpl;
import com.gs.baz.colorimetria.dto.ColorimetriaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import java.util.Base64;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/colorimetria")
public class ServiceColorimetria {

    @Autowired
    private ColorimetriaDAOImpl colorimetriaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ColorimetriaDTO> getColorimetrias() throws CustomException {
        return colorimetriaDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_colorimetria}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ColorimetriaDTO getColorimetria(@PathVariable("id_colorimetria") Long id_colorimetria) throws CustomException {
        return colorimetriaDAOImpl.selectRow(id_colorimetria);
    }

    /**
     *
     * @param id_colorimetria
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/resource/img/{id_colorimetria}", method = RequestMethod.GET)
    public ResponseEntity getLogo(@PathVariable("id_colorimetria") Long id_colorimetria) throws CustomException {
        ContentInfoUtil util = new ContentInfoUtil();
        HttpHeaders headers = new HttpHeaders();
        try {
            ColorimetriaDTO colorimetriaDTO = colorimetriaDAOImpl.selectRowIcono(id_colorimetria);
            if (colorimetriaDTO != null) {
                byte[] image = Base64.getDecoder().decode(colorimetriaDTO.getIcono());
                ContentInfo info = util.findMatch(image);
                headers.set("Pragma", "no-cache");
                headers.set("Cache-Control", "no-cache");
                headers.set("Content-Type", info.getMimeType());
                Resource resource = new ByteArrayResource(image);
                return ResponseEntity.status(HttpStatus.OK).headers(headers).body(resource);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            logger.error(ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ColorimetriaDTO postColorimetria(@RequestBody ColorimetriaDTO colorimetriaDTO) throws CustomException {
        if (colorimetriaDTO.getTemaWeb() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("tema_web"));
        }
        if (colorimetriaDTO.getColorPrim() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("color_primario"));
        }
        if (colorimetriaDTO.getColorSegund() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("color_segundario"));
        }
        if (colorimetriaDTO.getColorTerc() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("color_terciario"));
        }
        if (colorimetriaDTO.getIcono() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("img_icono"));
        }
        return colorimetriaDAOImpl.insertRow(colorimetriaDTO);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ColorimetriaDTO putColorimetria(@RequestBody ColorimetriaDTO colorimetriaDTO) throws CustomException {
        if (colorimetriaDTO.getIdColorimetria() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_colorimetria"));
        }
        if (colorimetriaDTO.getTemaWeb() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("tema_web"));
        }
        if (colorimetriaDTO.getColorPrim() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("color_primario"));
        }
        if (colorimetriaDTO.getColorSegund() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("color_segundario"));
        }
        if (colorimetriaDTO.getColorTerc() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("color_terciario"));
        }
        if (colorimetriaDTO.getIcono() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("img_icono"));
        }
        return colorimetriaDAOImpl.updateRow(colorimetriaDTO);
    }

}
