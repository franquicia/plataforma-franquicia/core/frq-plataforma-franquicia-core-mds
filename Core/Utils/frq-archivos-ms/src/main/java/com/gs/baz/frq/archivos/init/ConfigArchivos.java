package com.gs.baz.frq.archivos.init;

import com.gs.baz.frq.archivos.bi.ArchivosApiBI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.archivos")
public class ConfigArchivos {

    private final Logger logger = LogManager.getLogger();

    public ConfigArchivos() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ArchivosApiBI archivosApiBI() {
        return new ArchivosApiBI();
    }
}
