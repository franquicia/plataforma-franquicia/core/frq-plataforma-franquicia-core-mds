/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.visita.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class VisitaPuntoContactoDTO {

    @JsonProperty(value = "id_bitacora")
    private Integer idBitacora;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_disciplina")
    private Integer idNegocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_check_usuario")
    private Integer idCheckUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_check_lista")
    private Integer idCheckList;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_base")
    private Integer idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo")
    private Integer conteo;

    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(BigDecimal conteo) {
        this.conteo = (conteo == null ? null : conteo.intValue());
    }

    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(BigDecimal idBitacora) {
        this.idBitacora = (idBitacora == null ? null : idBitacora.intValue());
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public Integer getIdNegocioDisciplina() {
        return idNegocioDisciplina;
    }

    public void setIdNegocioDisciplina(BigDecimal idNegocioDisciplina) {
        this.idNegocioDisciplina = (idNegocioDisciplina == null ? null : idNegocioDisciplina.intValue());
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdCheckUsuario() {
        return idCheckUsuario;
    }

    public void setIdCheckUsuario(BigDecimal idCheckUsuario) {
        this.idCheckUsuario = (idCheckUsuario == null ? null : idCheckUsuario.intValue());
    }

    public Integer getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(BigDecimal idCheckList) {
        this.idCheckList = (idCheckList == null ? null : idCheckList.intValue());
    }

    public Integer getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(BigDecimal idCecoBase) {
        this.idCecoBase = (idCecoBase == null ? null : idCecoBase.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    @Override
    public String toString() {
        return "VisitaPuntoContactoDTO{" + "idBitacora=" + idBitacora + ", idProtocolo=" + idProtocolo + ", idNegocioDisciplina=" + idNegocioDisciplina + ", idCeco=" + idCeco + ", idCheckUsuario=" + idCheckUsuario + ", idCheckList=" + idCheckList + ", idCecoBase=" + idCecoBase + ", fecha=" + fecha + ", idUsuario=" + idUsuario + ", conteo=" + conteo + '}';
    }

}
