/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos Generales Certificadores y Gerentes", value = "GraficaGeneral")
public class PendientesGerente {

    @JsonProperty(value = "certificadores")
    @ApiModelProperty(notes = "Total de certificadores", example = "99", position = -1)
    private Integer certificadores;

    @JsonProperty(value = "certificacionesPendientes")
    @ApiModelProperty(notes = "Total de certificaciones pendientes por gerente", example = "99", position = -2)
    private Integer certificacionesPendientes;

    public Integer getCertificadores() {
        return certificadores;
    }

    public void setCertificadores(Integer certificadores) {
        this.certificadores = certificadores;
    }

    public Integer getCertificacionesPendientes() {
        return certificacionesPendientes;
    }

    public void setCertificacionesPendientes(Integer certificacionesPendientes) {
        this.certificacionesPendientes = certificacionesPendientes;
    }

    @Override
    public String toString() {
        return "PendientesGerente{" + "certificadores=" + certificadores + ", certificacionesPendientes=" + certificacionesPendientes + '}';
    }

    
}
