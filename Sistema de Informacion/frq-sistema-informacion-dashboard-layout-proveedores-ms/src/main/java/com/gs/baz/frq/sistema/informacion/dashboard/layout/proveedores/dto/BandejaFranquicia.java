/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la bandeja de responsables de Franquicia", value = "BandejaFranquicia")
public class BandejaFranquicia {

    @JsonProperty(value = "idArchivo")
    @ApiModelProperty(notes = "Identificador del archivo", example = "1", position = -1)
    private Integer idArchivo;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "EKT", position = -2)
    private Integer idFolio;
    
    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Ruta.", example = "", position = -3)
    private String ruta;
    
    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = ".", example = "", position = -4)
    private Integer tipo;
    
    @JsonProperty(value = "idEstado")
    @ApiModelProperty(notes = "Identificador del estado.", example = "1", position = -5)
    private Integer idEstado;

    @JsonProperty(value = "fecha")
    @ApiModelProperty(notes = ".", example = "", position = -6)
    private String fecha;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = ".", example = "", position = -7)
    private String centroCostos;

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    @Override
    public String toString() {
        return "BandejaFranquicia{" + "idArchivo=" + idArchivo + ", idFolio=" + idFolio + ", ruta=" + ruta + ", tipo=" + tipo + ", idEstado=" + idEstado + ", fecha=" + fecha + ", centroCostos=" + centroCostos + '}';
    }
    
}
