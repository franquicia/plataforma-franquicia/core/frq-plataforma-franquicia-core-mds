package com.gs.baz.frq.documento.sucursal.documento.archivos.init;

import com.gs.baz.frq.documento.sucursal.documento.archivos.dao.DocumentoArchivoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.documento.archivos")
public class ConfigDocumentoSucursalDocumentoArchivos {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalDocumentoArchivos() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoArchivoDAOImpl documentoArchivoDAOImpl() {
        return new DocumentoArchivoDAOImpl();
    }

}
