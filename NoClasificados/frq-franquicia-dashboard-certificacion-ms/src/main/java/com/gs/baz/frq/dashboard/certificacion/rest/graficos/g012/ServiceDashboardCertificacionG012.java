/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g012;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g012.DashboardCertificacionG012DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g012.DashboardCertificacionG012DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g012")
public class ServiceDashboardCertificacionG012 {

    @Autowired
    private DashboardCertificacionG012DAOImpl dashboardCertificacionG012DAOImpl;

    @RequestMapping(value = "secure/grafico/get/pendientes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG012DTO> getDatosPuntosPendientesG012() throws CustomException {

        return dashboardCertificacionG012DAOImpl.selectRowsDashboardCertificacionG012();
    }

    @RequestMapping(value = "secure/grafico/get/pendientes/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG012DTO> getDatoPuntosPendientesG012(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @RequestParam(value = "idGerente", required = true) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario) throws CustomException {
        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFin == null || fechaFin.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFin"));
        }
        if (idGerente == null || idGerente.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("idGerente"));
        }

        return dashboardCertificacionG012DAOImpl.selectRowDashboardCertificacionG012(fechaInicial, fechaFin, idGerente, idNegocio, idUsuario);
    }
}
