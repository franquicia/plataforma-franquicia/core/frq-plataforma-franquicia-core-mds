/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.client.bi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firma;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firmas;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Component
public class FirmasBI {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private UsuariosBI usuariosBI;

    public Firmas getFirmasFolio(HttpHeaders httpHeaders, Integer idFolio) throws Exception {
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folio-firmas/v1/" + idFolio + "/firmas";
        try {
            final HttpEntity<Object> httpEntity = new HttpEntity<>(httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.GET, httpEntity, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);
            com.gs.baz.frq.modelo.negocio.folios.client.dto.Firmas firmas = objectMapper.readValue(objectNode.get("resultado").toString(), com.gs.baz.frq.modelo.negocio.folios.client.dto.Firmas.class);
            Firmas firmas1 = new Firmas();
            ArrayList<Firma> listFirmas = new ArrayList<>();
            firmas.getFirmas().forEach(item -> {
                Firma firma = new Firma();
                try {
                    Empleado empleado = usuariosBI.getUsuario(httpHeaders, item.getNumeroEmpleado());
                    firma.setEmpleado(empleado);
                } catch (Exception ex) {
                    Logger.getLogger(FirmasBI.class.getName()).log(Level.SEVERE, null, ex);
                }
                firma.setIdFirma(item.getIdFirma());
                firma.setIdJerarquia(item.getIdJerarquia());
                listFirmas.add(firma);
            });
            firmas1.setTodas(listFirmas);
            return firmas1;
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        }
    }
}
