/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyecto.documentos.rest;

import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dao.ProyectoDocumentoDAOImpl;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.AltaProyectoDocumento;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.ParametrosProyectoDocumento;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.ProyectoDocumentos;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "proyectoDocumentos", value = "proyectoDocumentos", description = "Api para la gestión del catalogo de proyectoDocumentos")
@RestController
@RequestMapping("/api-local/documento-sucursal/proyectoDocumento/v1")
public class ProyectoDocumentoApi {

    @Autowired
    private ProyectoDocumentoDAOImpl proyectoDocumentoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idProyecto
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene proyectoDocumento", notes = "Obtiene un proyectoDocumento", nickname = "obtieneProyectoDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idProyecto}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProyectoDocumentos obtieneProyectoDocumento(@ApiParam(name = "idProyecto", value = "Identificador del proyecto", example = "1", required = true) @PathVariable("idProyecto") Long idProyecto) throws CustomException, DataNotFoundException {
        ProyectoDocumentos proyectos = new ProyectoDocumentos(proyectoDocumentoDAOImpl.selectAllRows(idProyecto));

        if (proyectos == null) {
            throw new DataNotFoundException();
        }
        return proyectos;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene proyectoDocumentos", notes = "Obtiene todos los proyectoDocumentos", nickname = "obtieneProyectoDocumentos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProyectoDocumentos obtieneProyectoDocumentos() throws CustomException, DataNotFoundException {
        ProyectoDocumentos proyectoDocumentos = new ProyectoDocumentos(proyectoDocumentoDAOImpl.selectAllRows());
        if (proyectoDocumentos.getProyectoDocumentos() != null && proyectoDocumentos.getProyectoDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return proyectoDocumentos;
    }

    /**
     *
     * @param parametrosProyectoDocumento
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear proyectoDocumento", notes = "Agrega un proyectoDocumento", nickname = "creaProyectoDocumento")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaProyectoDocumento creaProyectoDocumento(@ApiParam(name = "ParametrosProyectoDocumento", value = "Paramentros para el alta del proyectoDocumento", required = true) @RequestBody ParametrosProyectoDocumento parametrosProyectoDocumento) throws DataNotInsertedException {
        return proyectoDocumentoDAOImpl.insertRow(parametrosProyectoDocumento);
    }

    /**
     *
     * @param parametrosProyectoDocumento
     * @param idProyectoDocumento
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar proyectoDocumento", notes = "Actualiza un proyectoDocumento", nickname = "actualizaProyectoDocumento")
    @RequestMapping(value = "/", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaProyectoDocumento(@RequestBody ParametrosProyectoDocumento parametrosProyectoDocumento, @ApiParam(name = "idProyectoDocumento", value = "Identificador del proyectoDocumento", example = "1", required = true) @PathVariable("idProyectoDocumento") Integer idProyectoDocumento) throws DataNotUpdatedException {
        proyectoDocumentoDAOImpl.updateRow(parametrosProyectoDocumento);
        return new SinResultado();
    }

    /**
     *
     * @param idProyectoDocumento
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar proyectoDocumento", notes = "Elimina un item de los proyectoDocumentos", nickname = "eliminaProyectoDocumento")
    @RequestMapping(value = "/{idProyecto}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaProyectoDocumento(@ApiParam(name = "idProyecto", value = "Identificador del proyecto", example = "1", required = true) @PathVariable("idProyecto") Integer idProyecto) throws DataNotDeletedException {
        proyectoDocumentoDAOImpl.deleteRow(idProyecto);
        return new SinResultado();
    }

}
