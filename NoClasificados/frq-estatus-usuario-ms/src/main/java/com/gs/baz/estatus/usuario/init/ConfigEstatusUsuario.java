package com.gs.baz.estatus.usuario.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.estatus.usuario.dao.EstatusUsuarioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.estatus.usuario")
public class ConfigEstatusUsuario {

    private final Logger logger = LogManager.getLogger();

    public ConfigEstatusUsuario() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public EstatusUsuarioDAOImpl estatusUsuarioDAOImpl() {
        return new EstatusUsuarioDAOImpl();
    }
}
