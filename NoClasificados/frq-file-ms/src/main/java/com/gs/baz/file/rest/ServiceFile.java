/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.file.rest;

import com.gs.baz.file.dao.FileDAOImpl;
import com.gs.baz.file.dto.FileGenericDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.model.bucket.client.services.rest.SubServiceArchivo;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MADA
 */
@RestController
@RequestMapping("service/file")
public class ServiceFile {

    private final FileDAOImpl fileDAOImpl = new FileDAOImpl();

    private final Logger logger = LogManager.getLogger();

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceArchivo subServiceArchivo;

    @Autowired
    private ModelMapper modelMapper;

    /**
     *
     * @param headers
     * @param fullPath
     * @param accessToken
     * @param pForceDeveloping
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/read/b64/{full_path}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FileGenericDTO readFileGeneric(@RequestHeader HttpHeaders headers, @PathVariable("full_path") String fullPath, @RequestParam(required = false, name = "access_token") String accessToken, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        String fullPathFile = new String(Base64.getDecoder().decode(fullPath));
        fullPathFile = fullPathFile.replace("\"", "");
        fullPathFile = fullPathFile.replace("'", "");
        if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING)) {
            if (pForceDeveloping == null) {
                String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                basePath = this.basePathQA(basePath);
                if (accessToken != null) {
                    headers.set("Authorization", "Bearer " + accessToken);
                }
                final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
                subServiceArchivo.init(basePath, httpEntity);
                return modelMapper.map(subServiceArchivo.getFileGeneric(fullPath, false), FileGenericDTO.class);
            }
        }
        return fileDAOImpl.readFileB64(fullPathFile);
    }

    /**
     *
     * @param headers
     * @param fullPath
     * @param accessToken
     * @param pForceDeveloping
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/read/{full_path}", method = RequestMethod.GET)
    public ResponseEntity readFile(@RequestHeader HttpHeaders headers, @PathVariable("full_path") String fullPath, @RequestParam(required = false, name = "access_token") String accessToken, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        try {
            String fullPathFile = new String(Base64.getDecoder().decode(fullPath));
            fullPathFile = fullPathFile.replace("\"", "");
            fullPathFile = fullPathFile.replace("'", "");
            int beginIndex = fullPathFile.indexOf("__") + 2;
            int endIndex = fullPathFile.length();
            String nameFile = fullPathFile.substring(beginIndex, endIndex).toLowerCase();
            if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING)) {
                if (pForceDeveloping == null) {
                    String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                    basePath = this.basePathQA(basePath);
                    if (accessToken != null) {
                        headers.set("Authorization", "Bearer " + accessToken);
                    }
                    final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
                    subServiceArchivo.init(basePath, httpEntity);
                    ResponseEntity<byte[]> file = subServiceArchivo.getFileBytes(fullPath, false);
                    Resource resource = new ByteArrayResource(file.getBody());
                    return ResponseEntity.status(HttpStatus.OK).header("Content-Disposition", file.getHeaders().getContentDisposition().toString()).body(resource);
                }
            }
            Path pathFile = fileDAOImpl.readFile(fullPathFile);
            URLConnection urlConnection = pathFile.toUri().toURL().openConnection();
            if (beginIndex == 1) {
                nameFile = pathFile.getFileName().toString();
            }
            MediaType mediaType = MediaType.asMediaType(MimeType.valueOf(urlConnection.getContentType()));
            Resource resource = new ByteArrayResource(StreamUtils.copyToByteArray(urlConnection.getInputStream()));
            headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=" + nameFile);
            return ResponseEntity.status(HttpStatus.OK).headers(headers).contentType(mediaType).body(resource);
        } catch (CustomException | IOException ex) {
            logger.log(Level.TRACE, ex);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     *
     * @param headers
     * @param fileGenericDTO
     * @param pForceDeveloping
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/write", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FileGenericDTO writeFileGeneric(@RequestHeader HttpHeaders headers, @RequestBody FileGenericDTO fileGenericDTO, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        if (fileGenericDTO.getPath() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("path"));
        }
        if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING)) {
            if (pForceDeveloping == null) {
                String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                basePath = this.basePathQA(basePath);
                final HttpEntity<Object> httpEntity = new HttpEntity<>(fileGenericDTO, headers);
                subServiceArchivo.init(basePath, httpEntity);
                return modelMapper.map(subServiceArchivo.postFileGeneric(false), FileGenericDTO.class);
            }
        }
        return fileDAOImpl.writeFile(fileGenericDTO);
    }

    /**
     *
     * @param headers
     * @param fileGenericDTO
     * @param pForceDeveloping
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/write/directory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FileGenericDTO writeDirectoryGeneric(@RequestHeader HttpHeaders headers, @RequestBody FileGenericDTO fileGenericDTO, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        if (fileGenericDTO.getPath() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("path"));
        }
        if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING)) {
            if (pForceDeveloping == null) {
                String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                basePath = this.basePathQA(basePath);
                final HttpEntity<Object> httpEntity = new HttpEntity<>(fileGenericDTO, headers);
                subServiceArchivo.init(basePath, httpEntity);
                return modelMapper.map(subServiceArchivo.postDirectoryGeneric(false), FileGenericDTO.class);
            }
        }
        return fileDAOImpl.writeDirectory(fileGenericDTO);
    }

    /**
     *
     * @param headers
     * @param fileGenericDTO
     * @param pForceDeveloping
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/read/directory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FileGenericDTO> readDirectoryGeneric(@RequestHeader HttpHeaders headers, @RequestBody FileGenericDTO fileGenericDTO, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        if (fileGenericDTO.getPath() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("path"));
        }
        if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING)) {
            if (pForceDeveloping == null) {
                String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                basePath = this.basePathQA(basePath);
                final HttpEntity<Object> httpEntity = new HttpEntity<>(fileGenericDTO, headers);
                subServiceArchivo.init(basePath, httpEntity);
                return Arrays.asList(modelMapper.map(subServiceArchivo.getDirectoryGeneric(false), FileGenericDTO[].class));
            }
        }
        return fileDAOImpl.readDirectory(fileGenericDTO);
    }

    private String basePathQA(String basePath) {
        StringBuilder newPath = new StringBuilder(basePath);
        int fromIndex = basePath.indexOf("//") + 2;
        int toIndex = basePath.indexOf(":", 5);
        newPath.delete(fromIndex, toIndex);
        newPath.insert(fromIndex, "10.89.85.160");
        newPath = new StringBuilder(newPath);
        fromIndex = newPath.indexOf(":", 5) + 1;
        toIndex = newPath.length();
        newPath.delete(fromIndex, toIndex);
        newPath.insert(fromIndex, "8080");
        return newPath.toString();
    }

}
