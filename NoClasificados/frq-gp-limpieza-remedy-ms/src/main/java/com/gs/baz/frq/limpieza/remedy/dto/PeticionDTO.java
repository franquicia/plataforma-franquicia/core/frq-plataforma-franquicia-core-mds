package com.gs.baz.frq.limpieza.remedy.dto;

public class PeticionDTO {

    private String origenPeticion;
    private String fechaPeticion;
    private Integer conteoPeticion;

    public PeticionDTO() {
        super();

    }

    public String getOrigenPeticion() {
        return origenPeticion;
    }

    public void setOrigenPeticion(String origenPeticion) {
        this.origenPeticion = origenPeticion;
    }

    public String getFechaPeticion() {
        return fechaPeticion;
    }

    public void setFechaPeticion(String fechaPeticion) {
        this.fechaPeticion = fechaPeticion;
    }

    public Integer getConteoPeticion() {
        return conteoPeticion;
    }

    public void setConteoPeticion(Integer conteoPeticion) {
        this.conteoPeticion = conteoPeticion;
    }

}
