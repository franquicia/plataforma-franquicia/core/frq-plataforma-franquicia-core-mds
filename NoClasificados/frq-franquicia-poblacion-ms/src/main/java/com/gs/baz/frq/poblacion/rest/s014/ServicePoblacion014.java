/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.poblacion.rest.s014;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.poblacion.dao.d014.PoblacionDAOImpl014;
import com.gs.baz.frq.poblacion.dto.PoblacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/poblacion/")
@Component("ServicePoblacion014")
public class ServicePoblacion014 {

    @Autowired
    private PoblacionDAOImpl014 poblacionDAOImpl014;

    @RequestMapping(value = "secure/ceco/merge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PoblacionDTO poblacionDTO(@RequestBody PoblacionDTO poblacionDTO) throws CustomException {
        return poblacionDAOImpl014.executeStore(poblacionDTO);
    }

}
