/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g013;

import com.gs.baz.frq.dashboard.dao.graficos.g013.DashboardDAOImpl013;
import com.gs.baz.frq.dashboard.dto.graficos.g013.CumplimientoPuntajeNacionalDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard013 {

    @Autowired
    private DashboardDAOImpl013 dashboardDAOImpl013;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/puntaje/nacional", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CumplimientoPuntajeNacionalDTO> getCumplimientoPuntajeNacional(@RequestBody CumplimientoPuntajeNacionalDTO cumplimientoPuntajeNacioal) throws CustomException {
        return dashboardDAOImpl013.selectRowsCumplimientoPuntajeNacional(cumplimientoPuntajeNacioal);
    }
}
