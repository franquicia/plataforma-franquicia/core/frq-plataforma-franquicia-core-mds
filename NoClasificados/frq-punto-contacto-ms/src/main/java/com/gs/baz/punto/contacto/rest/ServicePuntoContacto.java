/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.UserToken;
import com.gs.baz.frq.model.commons.UserTokenDTO;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.CargaPuntoContactoByCecoDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceCargaPuntoContactoByCeco;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFormato;
import com.gs.baz.punto.contacto.dao.PuntoContactoDAOImpl;
import com.gs.baz.punto.contacto.dto.PuntoContactoDTO;
import com.gs.baz.punto.contacto.dto.PuntoContactoFranquiciaModeloDTO;
import com.gs.baz.punto.contacto.util.ReadExcelUtil;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/punto/contacto")
public class ServicePuntoContacto {

    @Autowired
    private PuntoContactoDAOImpl puntoContactoDAOImpl;

    @Autowired
    private ReadExcelUtil readExcelUtil;

    @Autowired
    private SubServiceFormato subServiceFormato;

    @Autowired
    private SubServiceCargaPuntoContactoByCeco subServiceCargaPuntoContactoByCeco;

    @Autowired
    HttpServletRequest httpServletRequest;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PuntoContactoDTO> getPuntosContactos() throws CustomException {
        return puntoContactoDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{ceco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PuntoContactoDTO getPuntoContacto(@PathVariable("ceco") String ceco) throws CustomException {
        return puntoContactoDAOImpl.selectRow(ceco);
    }

    @RequestMapping(value = "secure/cu", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public PuntoContactoDTO cuPuntoContacto(@RequestBody PuntoContactoDTO puntoContactoDTO) throws CustomException {
        if (puntoContactoDTO.getIdPuntoContacto() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_punto_contacto"));
        }
        puntoContactoDTO = puntoContactoDAOImpl.cuRow(puntoContactoDTO);
        PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO = puntoContactoDTO.getPuntoContactoFranquiciaModeloDTO();
        if (puntoContactoFranquiciaModeloDTO.getInserted() != null && puntoContactoFranquiciaModeloDTO.getInserted() || puntoContactoFranquiciaModeloDTO.getUpdated() != null && puntoContactoFranquiciaModeloDTO.getUpdated()) {
            HttpHeaders headers = new HttpHeaders();
            final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
            String basePath;
            if (Environment.CURRENT.value().equals(Environment.DEVELOPING)) {
                basePath = httpServletRequest.getScheme() + "://10.53.33.83";
            } else {
                basePath = httpServletRequest.getScheme() + "://10.53.33.83";
            }
            subServiceCargaPuntoContactoByCeco.init(basePath, httpEntity);
            try {
                CargaPuntoContactoByCecoDTO cargaPuntoContactoByCecoDTO = subServiceCargaPuntoContactoByCeco.cargaPuntoContactoByCecoDTO(puntoContactoDTO.getCeco());
                puntoContactoDTO.setUsuariosModificados(cargaPuntoContactoByCecoDTO.getUsuariosModificados());
                puntoContactoDTO.setResultadoOk(cargaPuntoContactoByCecoDTO.getResultadoOk());
            } catch (Exception ex) {
                logger.log(Level.WARN, ex);
            }
        }
        return puntoContactoDTO;
    }

    @RequestMapping(value = "secure/carga/post/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity cargaFile(HttpServletRequest request, @RequestBody @RequestParam("file") MultipartFile file) throws CustomException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", request.getHeader("Authorization"));
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        String contentType = file.getContentType();
        MediaType mediaType = MediaType.asMediaType(MimeType.valueOf(contentType));
        HttpHeaders outHeaders = new HttpHeaders();
        outHeaders.set("Content-Disposition", "inline;filename=" + new Date().getTime() + "_" + file.getOriginalFilename());
        UserTokenDTO userTokenDTO = new UserToken(request).getUserToken();

        Resource fileOut = null;
        try {
            Boolean isXLSX = null;
            if (contentType != null) {
                if (contentType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                    isXLSX = true;
                } else if (contentType.equals("application/vnd.ms-excel")) {
                    isXLSX = false;
                }
            }
            if (userTokenDTO == null) {
                throw new CustomException(ModelCodes.TOKEN_MISSING_IN_REQUEST.detalle("Token requerido en esta solicitud"));
            }
            if (isXLSX == null) {
                throw new CustomException(ModelCodes.ERROR_TO_READ_FILE.detalle("Solo formato xls y xlsx permitidos"));
            }
            subServiceFormato.init(basePath, httpEntity);
            fileOut = readExcelUtil.readExcel(subServiceFormato, puntoContactoDAOImpl, file.getInputStream(), isXLSX, userTokenDTO.getNombreUsuario());
        } catch (IOException ex) {
            logger.log(Level.ERROR, ex);
        }
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).contentType(mediaType).body(fileOut);
    }
}
