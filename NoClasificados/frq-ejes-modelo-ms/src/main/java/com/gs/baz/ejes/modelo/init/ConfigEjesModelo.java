package com.gs.baz.ejes.modelo.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.ejes.modelo.dao.EjesModeloDAOImpl;
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Configuration
@Import(ConfigBucketServices.class)
@ComponentScan("com.gs.baz.ejes.modelo")
public class ConfigEjesModelo {

    private final Logger logger = LogManager.getLogger();

    public ConfigEjesModelo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public EjesModeloDAOImpl ejesModeloDAOImpl() {
        return new EjesModeloDAOImpl();
    }
}
