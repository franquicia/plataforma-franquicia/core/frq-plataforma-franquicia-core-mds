package com.gs.baz.server.logs.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.server.logs.bi.ServerLogsBI;
import com.gs.baz.server.logs.filter.EndIsnpectRequestFilter;
import com.gs.baz.server.logs.filter.StartInspectRequestFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.hateoas.client.LinkDiscoverers;
import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.plugin.core.SimplePluginRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.server.logs")
@EnableWebSocketMessageBroker
public class ConfigServerLogs implements WebSocketMessageBrokerConfigurer {

    private final Logger logger = LogManager.getLogger();

    public ConfigServerLogs() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public FilterRegistrationBean<StartInspectRequestFilter> resourcesFilterStartInspectRequest() {
        FilterRegistrationBean<StartInspectRequestFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new StartInspectRequestFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<EndIsnpectRequestFilter> resourcesFilterEndInspectRequest() {
        FilterRegistrationBean<EndIsnpectRequestFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new EndIsnpectRequestFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
        return registrationBean;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic", "/queue/", "/logs", "/actions");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket").setHandshakeHandler(defaultHandshakeHandler()).setAllowedOrigins("*").withSockJS();
    }

    @Bean
    public DefaultHandshakeHandler defaultHandshakeHandler() {
        return new DefaultHandshakeHandler() {
            public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map attributes) throws Exception {
                if (request instanceof ServletServerHttpRequest) {
                    ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
                    HttpSession session = servletRequest.getServletRequest().getSession();
                    attributes.put("JSESSIONID", session.getId());
                }
                return true;
            }
        };
    }

    @Bean
    public LinkDiscoverers discoverers() {
        List<LinkDiscoverer> plugins = new ArrayList<>();
        plugins.add(new CollectionJsonLinkDiscoverer());
        return new LinkDiscoverers(SimplePluginRegistry.create(plugins));
    }

    @Bean(initMethod = "init")
    public ServerLogsBI serverLogsDAOImpl() {
        return new ServerLogsBI();
    }
}
