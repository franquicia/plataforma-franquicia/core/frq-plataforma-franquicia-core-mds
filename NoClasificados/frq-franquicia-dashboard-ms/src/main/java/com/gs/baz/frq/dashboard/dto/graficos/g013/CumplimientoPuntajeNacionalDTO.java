/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g013;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class CumplimientoPuntajeNacionalDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcionCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "totales")
    private Integer totales;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cumple")
    private Integer cumple;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "porcentaje")
    private Integer porcentaje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescripcionCeco() {
        return descripcionCeco;
    }

    public void setDescripcionCeco(String descripcionCeco) {
        this.descripcionCeco = descripcionCeco;
    }

    public Integer getTotales() {
        return totales;
    }

    public void setTotales(BigDecimal totales) {
        this.totales = (totales == null ? null : totales.intValue());
    }

    public Integer getCumple() {
        return cumple;
    }

    public void setCumple(BigDecimal cumple) {
        this.cumple = (cumple == null ? null : cumple.intValue());
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(BigDecimal idRango) {
        this.idRango = (idRango == null ? null : idRango.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = (porcentaje == null ? null : porcentaje.intValue());
    }

    @Override
    public String toString() {
        return "CumplimientoPuntajeNacionalDTO{" + "idCeco=" + idCeco + ", descripcionCeco=" + descripcionCeco + ", totales=" + totales + ", cumple=" + cumple + ", idRango=" + idRango + ", porcentaje=" + porcentaje + ", filtros=" + filtros + '}';
    }

}
