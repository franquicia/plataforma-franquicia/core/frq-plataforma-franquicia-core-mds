/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.CalificacionPromedio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionesPromedioRowMapper implements RowMapper<CalificacionPromedio> {

    @Override
    public CalificacionPromedio mapRow(ResultSet rs, int rowNum) throws SQLException {
        CalificacionPromedio calificacionPromedio = new CalificacionPromedio();
        calificacionPromedio.setTotalPuntosVenta(rs.getInt("TOTAL_PDV"));
        calificacionPromedio.setCalificacion(rs.getInt("PROMEDIO"));
        return calificacionPromedio;
    }
}
