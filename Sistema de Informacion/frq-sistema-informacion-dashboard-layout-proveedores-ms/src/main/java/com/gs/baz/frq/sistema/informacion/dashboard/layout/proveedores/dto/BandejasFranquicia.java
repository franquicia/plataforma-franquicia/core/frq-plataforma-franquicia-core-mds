/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de bandejas de Franquicia", value = "BandejasFranquicia")
public class BandejasFranquicia {

    @JsonProperty(value = "bandejasFranquicia")
    @ApiModelProperty(notes = "bandejasFranquicia")
    private List<BandejaFranquicia> bandejasFranquicia;

    public BandejasFranquicia(List<BandejaFranquicia> bandejasFranquicia) {
        this.bandejasFranquicia = bandejasFranquicia;
    }

    public List<BandejaFranquicia> getBandejasFranquicia() {
        return bandejasFranquicia;
    }

    public void setBandejasFranquicia(List<BandejaFranquicia> bandejasFranquicia) {
        this.bandejasFranquicia = bandejasFranquicia;
    }

    @Override
    public String toString() {
        return "BandejasFranquicia{" + "bandejasFranquicia=" + bandejasFranquicia + '}';
    }

}
