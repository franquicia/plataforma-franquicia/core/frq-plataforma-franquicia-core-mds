/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g010;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g010.DashboardCertificacionG010DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g010.DashboardCertificacionG010DTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g010")
public class ServiceDashboardCertificacionG010 {

    @Autowired
    private DashboardCertificacionG010DAOImpl dashboardCertificacionG010DAOImpl;

    @RequestMapping(value = "secure/grafico/get/puntos/venta", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG010DTO> getDatosPuntosG010() throws CustomException {
        return dashboardCertificacionG010DAOImpl.selectRowsDashboardCertificacionG010();
    }

    @RequestMapping(value = "secure/grafico/get/puntos/venta/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG010DTO> getDatoPuntosG010(
            @RequestParam(value = "idUsuario", required = false) Long idUsuario) throws CustomException {
        return dashboardCertificacionG010DAOImpl.selectRowDashboardCertificacionG010(idUsuario);
    }

}
