/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.base64.rest;

import com.gs.baz.frq.utilerias.archivos.convertidor.base64.dto.SalidaArchivoBase64;
import java.util.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/api-local/utilerias/archivos")
public class ArchivosBase64Api {

    @RequestMapping(value = "/convertir/a/base64", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity convertirArchivoBase64(@RequestParam("archivo") final MultipartFile inputFile) throws Exception {
        SalidaArchivoBase64 salidaArchivoBase64 = new SalidaArchivoBase64();
        String fileBase64 = Base64.getEncoder().encodeToString(inputFile.getBytes());
        salidaArchivoBase64.setTipoContenido(inputFile.getContentType());
        salidaArchivoBase64.setNombre(inputFile.getOriginalFilename());
        salidaArchivoBase64.setArchivo(fileBase64);
        return ResponseEntity.status(HttpStatus.OK).body(salidaArchivoBase64);
    }
}
