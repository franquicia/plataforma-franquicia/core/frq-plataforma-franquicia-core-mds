/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de graficas", value = "Graficas")
public class Graficas {

    @JsonProperty(value = "graficas")
    @ApiModelProperty(notes = "graficas")
    private List<Grafica> graficas;

    public Graficas(List<Grafica> graficas) {
        this.graficas = graficas;
    }

    public List<Grafica> getGraficas() {
        return graficas;
    }

    public void setGraficas(List<Grafica> graficas) {
        this.graficas = graficas;
    }

    @Override
    public String toString() {
        return "Graficas{" + "graficas=" + graficas + '}';
    }

}
