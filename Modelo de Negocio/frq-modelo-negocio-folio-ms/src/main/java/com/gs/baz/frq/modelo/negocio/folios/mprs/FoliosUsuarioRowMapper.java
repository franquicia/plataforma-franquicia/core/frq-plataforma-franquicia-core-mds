/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioAutorizacion;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.EstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firma;
import com.gs.baz.frq.modelo.negocio.folios.dto.UnidadNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FoliosUsuarioRowMapper implements RowMapper<BusquedaFolioAutorizacion> {

    private BusquedaFolioAutorizacion folioBusqueda;

    @Override
    public BusquedaFolioAutorizacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        folioBusqueda = new BusquedaFolioAutorizacion();
        folioBusqueda.setIdFolio(rs.getInt("FIIDFOLIO"));
        folioBusqueda.setDocumento(new Documento(rs.getString("FCTITULO")));
        folioBusqueda.setCreador(new Empleado(rs.getString("CREADOR")));
        folioBusqueda.setFechaCreacion(rs.getString("FDFECHACREACION"));
        folioBusqueda.setFechaSolicitud(rs.getString("FDFECHASOLICITUD"));
        folioBusqueda.setVisualizado(rs.getBoolean("FIVISUALIZADO"));
        List<UnidadNegocio> listaUnidadNegocio = new ArrayList<>();
        String[] arrayUnidades = rs.getString("UNIDADES_NEGOCIO").split(",");
        for (String unidades : arrayUnidades) {
            UnidadNegocio unidadNegocio = new UnidadNegocio();
            unidadNegocio.setDescripcion(unidades);
            listaUnidadNegocio.add(unidadNegocio);
        }
        folioBusqueda.setUnidadesNegocio(listaUnidadNegocio);
        folioBusqueda.setEstado(new EstadoFolio(rs.getInt("FIIDEDODOCTO"), rs.getString("FCNOMBRE")));
        folioBusqueda.setFirma(new Firma(rs.getInt("FIIDFIRMA")));
        return folioBusqueda;
    }
}
