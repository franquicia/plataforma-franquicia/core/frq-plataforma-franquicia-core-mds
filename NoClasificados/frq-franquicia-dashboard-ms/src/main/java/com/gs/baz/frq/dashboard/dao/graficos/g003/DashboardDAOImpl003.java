package com.gs.baz.frq.dashboard.dao.graficos.g003;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g003.DefectoPorMillonDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g003.DefectoPorMillonRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl003 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectDefectoPorMillon;
    private DefaultJdbcCall jdbcSelectDefectoPorMillonArbol;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectDefectoPorMillon = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDefectoPorMillon.withSchemaName(schema);
        jdbcSelectDefectoPorMillon.withCatalogName("PACONSDASHBOARD");
        jdbcSelectDefectoPorMillon.withProcedureName("SPDEFXMSIGMA");
        jdbcSelectDefectoPorMillon.returningResultSet("RCL_INFO", new DefectoPorMillonRowMapper());

        jdbcSelectDefectoPorMillonArbol = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDefectoPorMillonArbol.withSchemaName(schema);
        jdbcSelectDefectoPorMillonArbol.withCatalogName("PAADMINDPSGAR");
        jdbcSelectDefectoPorMillonArbol.withProcedureName("SPDEFYSIGARB");
        jdbcSelectDefectoPorMillonArbol.returningResultSet("RCL_INFO", new DefectoPorMillonRowMapper());
    }

    public DefectoPorMillonDTO selectRowsDefectoPorMillonNacional(FiltrosDTO filtros) throws CustomException {

        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_PERIODO", filtros.getNumeroPeriodo());
        mapSqlParameterSource.addValue("PA_ANIO", filtros.getAnio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectDefectoPorMillon.execute(mapSqlParameterSource);
        List<DefectoPorMillonDTO> data = (List<DefectoPorMillonDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public DefectoPorMillonDTO selectRowsDefectoPorMillonNivel(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectDefectoPorMillonArbol.execute(mapSqlParameterSource);
        List<DefectoPorMillonDTO> data = (List<DefectoPorMillonDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public DefectoPorMillonDTO selectRowsDefectoMillonSigmaNivel(DefectoPorMillonDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        if (filtros.getNivelGeografico().equals(4)) {
            return this.selectRowsDefectoPorMillonNacional(filtros);
        } else {
            return this.selectRowsDefectoPorMillonNivel(filtros);
        }
    }

}
