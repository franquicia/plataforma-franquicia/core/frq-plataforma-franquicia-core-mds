package com.gs.baz.ejes.modelo.mprs;

import com.gs.baz.ejes.modelo.dto.EjeModeloDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class EjesModeloRowMapper implements RowMapper<EjeModeloDTO> {

    private EjeModeloDTO ejesModeloDTO;

    @Override
    public EjeModeloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ejesModeloDTO = new EjeModeloDTO();
        ejesModeloDTO.setIdModEje((BigDecimal) rs.getObject("FIIDMODEJE"));
        ejesModeloDTO.setIdModelo((BigDecimal) rs.getObject("FIIDMODELO"));
        ejesModeloDTO.setIdEje(((BigDecimal) rs.getObject("FIIDEJE")));
        ejesModeloDTO.setPonderacion(((BigDecimal) rs.getObject("FIPONDE")));
        return ejesModeloDTO;
    }
}
