/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de semaforos", value = "Semaforos")
public class Semaforos {

    @JsonProperty(value = "semaforos")
    @ApiModelProperty(notes = "semaforos")
    private List<Semaforo> semaforos;

    public Semaforos(List<Semaforo> semaforos) {
        this.semaforos = semaforos;
    }

    public List<Semaforo> getSemaforos() {
        return semaforos;
    }

    public void setSemaforos(List<Semaforo> semaforos) {
        this.semaforos = semaforos;
    }

    @Override
    public String toString() {
        return "Semaforos{" + "semaforos=" + semaforos + '}';
    }

}
