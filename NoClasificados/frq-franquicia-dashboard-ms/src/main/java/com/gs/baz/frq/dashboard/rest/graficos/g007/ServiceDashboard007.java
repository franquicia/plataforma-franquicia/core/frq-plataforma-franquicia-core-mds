/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g007;

import com.gs.baz.frq.dashboard.dao.graficos.g007.DashboardDAOImpl007;
import com.gs.baz.frq.dashboard.dto.graficos.g007.DistribucionCumplimientoNivelDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g007.DistribucionCumplimientoCecoNivelDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard007 {

    @Autowired
    private DashboardDAOImpl007 dashboardDAOImpl007;

    @RequestMapping(value = "secure/grafico/get/distribucion/cumplimiento/ceco/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DistribucionCumplimientoNivelDTO getDistribucionCumplimientoNivel(@RequestBody DistribucionCumplimientoCecoNivelDTO distribucionCumplimientoCecoNivelDTO) throws CustomException {
        return dashboardDAOImpl007.getDistribucionCumplimientoNivel(distribucionCumplimientoCecoNivelDTO);
    }
}
