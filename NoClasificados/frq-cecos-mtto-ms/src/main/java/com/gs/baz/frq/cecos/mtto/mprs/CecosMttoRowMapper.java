package com.gs.baz.frq.cecos.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.cecos.mtto.dto.CecosMttoDTO;

/**
 *
 * @author cescobarh
 */
public class CecosMttoRowMapper implements RowMapper<CecosMttoDTO> {

    private CecosMttoDTO status;

    @Override
    public CecosMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new CecosMttoDTO();

        status.setIdCeco(rs.getInt("FCID_CECO"));;
        status.setNombreCeco(rs.getString("NOMBRECECO"));
        status.setIdCecoSuperior(rs.getInt("FCID_CECOSUP"));
        status.setObservaciones(rs.getString("FCOBSERVACIONES"));
        status.setNegocio(rs.getString("FCNEGOCIO"));
        status.setPeriodo(rs.getString("FCPERIODO"));

        return status;
    }
}
