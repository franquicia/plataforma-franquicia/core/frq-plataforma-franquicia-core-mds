package com.gs.baz.frq.ubicacion.sucursal.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.ubicacion.sucursal.dto.UbicacionCecoDTO;

/**
 *
 * @author cescobarh
 */
public class UbicacionSucursalRowMapper implements RowMapper<UbicacionCecoDTO> {

    private UbicacionCecoDTO status;

    @Override
    public UbicacionCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new UbicacionCecoDTO();

        status.setCentroCostos(rs.getInt("FCID_CECO"));
        status.setNombreCeco(rs.getString("FCCALLE"));
        status.setCiudad(rs.getString("FCCIUDAD"));
        status.setCodigoPostal(rs.getString("FCCP"));
        status.setNumeroEconomico(rs.getInt("FIIDNU_SUCURSAL"));
        status.setNombreSucursal(rs.getString("CECO"));
        status.setCanal(rs.getInt("FIID_CANAL"));
        status.setPais(rs.getInt("FIID_PAIS"));
        status.setLongitud(rs.getDouble("FCLONGITUD"));
        status.setLatitud(rs.getDouble("FCLATITUD"));

        return status;
    }
}
