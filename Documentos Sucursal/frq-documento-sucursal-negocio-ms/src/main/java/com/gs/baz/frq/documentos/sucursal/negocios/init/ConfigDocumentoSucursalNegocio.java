package com.gs.baz.frq.documentos.sucursal.negocios.init;

import com.gs.baz.frq.documentos.sucursal.negocios.dao.DocumentoSucursalNegocioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documentos.sucursal.negocios")
public class ConfigDocumentoSucursalNegocio {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalNegocio() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "DocumentoSucursalNegocioDAOImplTest")
    public DocumentoSucursalNegocioDAOImpl documentoSucursalNegocioDAOImpl() {
        return new DocumentoSucursalNegocioDAOImpl();
    }

}
