/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.rutas.dto.AltaRutaNivel;
import com.gs.baz.frq.modelo.negocio.rutas.dto.ParametrosRutaNivel;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaNivel;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaNivelBase;
import com.gs.baz.frq.modelo.negocio.rutas.mprs.RutaNivelRowMapper;
import com.gs.baz.frq.modelo.negocio.rutas.mprs.RutasNivelRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class RutaNivelDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNRUTANIV";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNRUTANIVEL");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNRUTANIVEL");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNRUTANIVEL");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETRUTNIVXFOL");
        jdbcSelect.returningResultSet("PA_CDATOS", new RutaNivelRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETRUTNIVXFOL");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new RutasNivelRowMapper());

    }

    public RutaNivelBase selectRow(Long idFolio, Long idRuta, Long idModeloNegocio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDRUTA", idRuta);
        mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", idModeloNegocio);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<RutaNivelBase> data = (List<RutaNivelBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<RutaNivel> selectAllRows(Long idFolio, Long idRuta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDRUTA", idRuta);
        mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<RutaNivel>) out.get("PA_CDATOS");
    }

    public AltaRutaNivel insertRow(ParametrosRutaNivel entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO.getIdRuta());
            mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", entityDTO.getIdModeloNegocio());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaRutaNivel(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosRutaNivel entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO.getIdRuta());
            mapSqlParameterSource.addValue("PA_FIIDMODENEGOC", entityDTO.getIdModeloNegocio());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDRUTA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
