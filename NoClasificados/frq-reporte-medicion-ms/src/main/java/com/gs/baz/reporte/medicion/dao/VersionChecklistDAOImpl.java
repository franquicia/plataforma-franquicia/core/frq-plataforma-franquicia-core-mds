package com.gs.baz.reporte.medicion.dao;

import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import com.gs.baz.reporte.medicion.dto.VersionChecklistDTO;
import com.gs.baz.reporte.medicion.dto.mprs.VersionChecklistRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class VersionChecklistDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAREPORTEMEDEXT");
        jdbcSelect.withProcedureName("SPGETCHECKXPROT");
        jdbcSelect.returningResultSet("PA_CHECKLIST", new VersionChecklistRowMapper());
    }

    public List<VersionChecklistDTO> selectVersionChecklist(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA_INI", filtersDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHA_FIN", filtersDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_ID_DISIP", filtersDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<VersionChecklistDTO>) out.get("PA_CHECKLIST");

    }

}
