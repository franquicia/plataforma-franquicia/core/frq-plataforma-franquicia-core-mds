/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.mprs;

import com.gs.baz.frq.modelo.negocio.estados.firma.dto.EstadoFirmaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EstadoFirmaRowMapper implements RowMapper<EstadoFirmaBase> {

    private EstadoFirmaBase estadoFirmaBase;

    @Override
    public EstadoFirmaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        estadoFirmaBase = new EstadoFirmaBase();
        estadoFirmaBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        estadoFirmaBase.setNombre(rs.getString("FCNOMBRE"));
        return estadoFirmaBase;
    }
}
