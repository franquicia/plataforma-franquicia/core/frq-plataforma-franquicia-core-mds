package com.gs.baz.frq.ubicacion.sucursal.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.ubicacion.sucursal.dto.CecoDatosEntradaDTO;
import com.gs.baz.frq.ubicacion.sucursal.dto.UbicacionCecoDTO;
import com.gs.baz.frq.ubicacion.sucursal.mprs.UbicacionSucursalRowMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class UbicacionCecoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcObtieneCoordenadasCeco;

    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcObtieneCoordenadasCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneCoordenadasCeco.withSchemaName(schema);
        jdbcObtieneCoordenadasCeco.withCatalogName("PASUCUBICA");
        jdbcObtieneCoordenadasCeco.withProcedureName("SP_SEL_SUCNVO");
        jdbcObtieneCoordenadasCeco.returningResultSet("RCL_SUC", new UbicacionSucursalRowMapper());

    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneUbicacionSucursalesCeco(CecoDatosEntradaDTO bean) throws CustomException {

        Map<String, Object> response = new HashMap<String, Object>();
        List<UbicacionCecoDTO> listaUbicaciones = new ArrayList<UbicacionCecoDTO>();
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", bean.getCentroCostos().toString());

        out = jdbcObtieneCoordenadasCeco.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICIA. PASUCUBICA.SP_SEL_SUCNVO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            listaUbicaciones = (List<UbicacionCecoDTO>) out.get("RCL_SUC");
            response.put("listaUbicaciones", listaUbicaciones);
        }

        return response;
    }

}
