/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos para consulta Centro de Costos", value = "ConsultaCentroCostos")
public class CentroCostosParametros {

    @JsonProperty(value = "idProveedor")
    @ApiModelProperty(notes = "Identificador del proveedor externo", example = "1", position = -1)
    private Integer idProveedor;

    @JsonProperty(value = "numeroEmpleadoCertificador")
    @ApiModelProperty(notes = "Número de empleado del certificador", example = "202622", position = -2)
    private Integer idUsuario;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costos.", example = "920100", position = -3)
    private Integer idCeco;

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    @Override
    public String toString() {
        return "CentroCostosParametros{" + "idProveedor=" + idProveedor + ", idUsuario=" + idUsuario + ", idCeco=" + idCeco + '}';
    }
    
}
