/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.constants;

/**
 *
 * @author cescobarh
 */
public class ResponseApiCodes {

    public static ResponseApiCodes RESPONSE_MESSAGE_00000 = new ResponseApiCodes().code(100000).description("Error no identificado").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04001 = new ResponseApiCodes().code(104001).description("El token no tiene un formato correcto.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04011 = new ResponseApiCodes().code(104011).description("El token expiro.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04031 = new ResponseApiCodes().code(104031).description("La solicitud requiere autenticación.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04040 = new ResponseApiCodes().code(104040).description("Información no encontrada con los parámetros solicitados.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04041 = new ResponseApiCodes().code(104041).description("El token no pudo ser verificado.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04051 = new ResponseApiCodes().code(104051).description("El generador del token no pudo ser verificado.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04061 = new ResponseApiCodes().code(104061).description("Error de validación.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_05005 = new ResponseApiCodes().code(105005).description("Error al desencriptar datos.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04005 = new ResponseApiCodes().code(104005).description("ID de Acceso no encontrado.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04006 = new ResponseApiCodes().code(104006).description("Error los datos a actualizar ya existen en otro registro y estos datos deben ser únicos.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04007 = new ResponseApiCodes().code(104007).description("Error los datos a insertar ya existen en otro registro y estos datos deben ser únicos.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04008 = new ResponseApiCodes().code(104008).description("Error el registro que intenta actualizar no exite.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04009 = new ResponseApiCodes().code(104009).description("Error el registro que intenta borrar no exite.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04010 = new ResponseApiCodes().code(104010).description("El tamaño del arreglo de hallazgos es mayor a 10 elementos.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_05001 = new ResponseApiCodes().code(105001).description("Error el registro no pudo ser insertado.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_05002 = new ResponseApiCodes().code(105002).description("Error el registro no pudo ser actualizado.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_05003 = new ResponseApiCodes().code(105003).description("Error el registro no pudo ser borrado.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_04015 = new ResponseApiCodes().code(104015).description("ID de Acceso requerido.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_05015 = new ResponseApiCodes().code(105015).description("Error al encriptar datos.").build();
    public static ResponseApiCodes RESPONSE_MESSAGE_05025 = new ResponseApiCodes().code(105025).description("Error al descriptar datos.").build();

    private Integer code;
    private String description;

    public ResponseApiCodes() {
    }

    public ResponseApiCodes(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    private ResponseApiCodes code(Integer code) {
        this.code = code;
        return this;
    }

    public int code() {
        return code;
    }

    public String description() {
        return description;
    }

    private ResponseApiCodes description(String description) {
        this.description = description;
        return this;
    }

    private ResponseApiCodes build() {
        return new ResponseApiCodes(code, description);
    }

}
