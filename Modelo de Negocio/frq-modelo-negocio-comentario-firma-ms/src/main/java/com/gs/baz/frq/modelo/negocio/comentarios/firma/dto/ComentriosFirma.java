/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de comentariosFirma del documento", value = "comentariosFirma")
public class ComentriosFirma {

    @JsonProperty(value = "comentariosFirma")
    @ApiModelProperty(notes = "comentariosFirma")
    private List<ComentrioFirma> comentariosFirma;

    public ComentriosFirma(List<ComentrioFirma> comentariosFirma) {
        this.comentariosFirma = comentariosFirma;
    }

    public List<ComentrioFirma> getComentariosFirma() {
        return comentariosFirma;
    }

    public void setComentariosFirma(List<ComentrioFirma> comentariosFirma) {
        this.comentariosFirma = comentariosFirma;
    }

    @Override
    public String toString() {
        return "ComentariosFirma{" + "comentariosFirma=" + comentariosFirma + '}';
    }

}
