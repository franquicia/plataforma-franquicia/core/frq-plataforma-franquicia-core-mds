/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author MADA
 */
public class RespuestaPortalCautivoDTO {

    @JsonProperty(value = "Lista registros Portal Cautivo")
    private List<PortalCautivoDTO> portalCautivo;

    public List<PortalCautivoDTO> getPortalCautivo() {
        return portalCautivo;
    }

    public void setPortalCautivo(List<PortalCautivoDTO> portalCautivo) {
        this.portalCautivo = portalCautivo;
    }

    @Override
    public String toString() {
        return "RespuestaPortalCautivoDTO{" + "portalCautivo=" + portalCautivo + '}';
    }

}
