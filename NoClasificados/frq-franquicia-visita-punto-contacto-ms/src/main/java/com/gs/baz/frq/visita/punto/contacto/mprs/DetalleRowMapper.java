/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.visita.punto.contacto.mprs;

import com.gs.baz.frq.visita.punto.contacto.dto.VisitaPuntoContactoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author luisgomez
 */
public class DetalleRowMapper implements RowMapper<VisitaPuntoContactoDTO> {

    private VisitaPuntoContactoDTO vistaPuntoContactoDTO;

    @Override
    public VisitaPuntoContactoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPuntoContactoDTO = new VisitaPuntoContactoDTO();
        vistaPuntoContactoDTO.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        vistaPuntoContactoDTO.setIdNegocioDisciplina((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        vistaPuntoContactoDTO.setConteo((BigDecimal) rs.getObject("FICONTEO"));
        return vistaPuntoContactoDTO;
    }
}
