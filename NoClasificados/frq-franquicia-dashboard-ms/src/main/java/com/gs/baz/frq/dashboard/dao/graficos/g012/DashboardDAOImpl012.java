package com.gs.baz.frq.dashboard.dao.graficos.g012;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g012.ImperdonableDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g012.IncumplimientoDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g012.IncumplimientosImperdonablesDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g012.NegocioDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g012.NegociosRowMapper;
import com.gs.baz.frq.dashboard.mprs.graficos.g012.TopImperdonableRowMapper;
import com.gs.baz.frq.dashboard.mprs.graficos.g012.TopIncumplimientoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl012 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectNegocios;
    private DefaultJdbcCall jdbcSelectTopIncumplimiento;
    private DefaultJdbcCall jdbcSelectTopImperdonable;
    private DefaultJdbcCall jdbcSelectTopIncumplimientoNivel;
    private DefaultJdbcCall jdbcSelectTopIncumplimientoNivelPPR;
    private DefaultJdbcCall jdbcSelectTopImperdonableNivel;
    private DefaultJdbcCall jdbcSelectTopIncumplimientoPuntoContacto;
    private DefaultJdbcCall jdbcSelectTopImperdonablePuntoContacto;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectNegocios = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegocios.withSchemaName(schema);
        jdbcSelectNegocios.withCatalogName("PAREPODASHBCUMP");
        jdbcSelectNegocios.withProcedureName("SPGET_NEGOCIODISCID");
        jdbcSelectNegocios.returningResultSet("RCL_INFO", new NegociosRowMapper());

        jdbcSelectTopIncumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopIncumplimiento.withSchemaName(schema);
        jdbcSelectTopIncumplimiento.withCatalogName("PAREPODASHBINC");
        jdbcSelectTopIncumplimiento.withProcedureName("SPGET_TOPINCUMP");
        jdbcSelectTopIncumplimiento.returningResultSet("RCL_INFO", new TopIncumplimientoRowMapper());

        jdbcSelectTopImperdonable = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopImperdonable.withSchemaName(schema);
        jdbcSelectTopImperdonable.withCatalogName("PAREPODASHBIMPE");
        jdbcSelectTopImperdonable.withProcedureName("SPGET_TOPIMPERD");
        jdbcSelectTopImperdonable.returningResultSet("RCL_INFO", new TopImperdonableRowMapper());

        jdbcSelectTopIncumplimientoNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopIncumplimientoNivel.withSchemaName(schema);
        jdbcSelectTopIncumplimientoNivel.withCatalogName("PAREPODASHBINC2");
        jdbcSelectTopIncumplimientoNivel.withProcedureName("SPGET_TOPINCUMP_GEO");
        jdbcSelectTopIncumplimientoNivel.returningResultSet("RCL_INFO", new TopIncumplimientoRowMapper());

        jdbcSelectTopIncumplimientoNivelPPR = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopIncumplimientoNivelPPR.withSchemaName(schema);
        jdbcSelectTopIncumplimientoNivelPPR.withCatalogName("PAREPODASHBINC3");
        jdbcSelectTopIncumplimientoNivelPPR.withProcedureName("SPGET_TOPINCUMP_GEO");
        jdbcSelectTopIncumplimientoNivelPPR.returningResultSet("RCL_INFO", new TopIncumplimientoRowMapper());

        jdbcSelectTopImperdonableNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopImperdonableNivel.withSchemaName(schema);
        jdbcSelectTopImperdonableNivel.withCatalogName("PAREPODASHBIMPE");
        jdbcSelectTopImperdonableNivel.withProcedureName("SPGET_TOPIMPERD_GEO");
        jdbcSelectTopImperdonableNivel.returningResultSet("RCL_INFO", new TopImperdonableRowMapper());

        jdbcSelectTopIncumplimientoPuntoContacto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopIncumplimientoPuntoContacto.withSchemaName(schema);
        jdbcSelectTopIncumplimientoPuntoContacto.withCatalogName("PAREPODASHBINC2");
        jdbcSelectTopIncumplimientoPuntoContacto.withProcedureName("SPGET_TOPINCUMP_CONT");
        jdbcSelectTopIncumplimientoPuntoContacto.returningResultSet("RCL_INFO", new TopIncumplimientoRowMapper());

        jdbcSelectTopImperdonablePuntoContacto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTopImperdonablePuntoContacto.withSchemaName(schema);
        jdbcSelectTopImperdonablePuntoContacto.withCatalogName("PAREPODASHBIMPE");
        jdbcSelectTopImperdonablePuntoContacto.withProcedureName("SPGET_TOPIMPERD_CONT");
        jdbcSelectTopImperdonablePuntoContacto.returningResultSet("RCL_INFO", new TopImperdonableRowMapper());

    }

    private List<NegocioDTO> selectRowsNegocios(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectNegocios.execute(mapSqlParameterSource);
        return (List<NegocioDTO>) out.get("RCL_INFO");
    }

    private List<IncumplimientoDTO> selectRowsIncumplimientos(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectTopIncumplimiento.execute(mapSqlParameterSource);
        return (List<IncumplimientoDTO>) out.get("RCL_INFO");
    }

    private List<ImperdonableDTO> selectRowsImperdonables(FiltrosDTO filtros) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectTopImperdonable.execute(mapSqlParameterSource);
        return (List<ImperdonableDTO>) out.get("RCL_INFO");
    }

    private List<IncumplimientoDTO> selectRowsIncumplimientosNivel(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECOID", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectTopIncumplimientoNivel.execute(mapSqlParameterSource);
        return (List<IncumplimientoDTO>) out.get("RCL_INFO");
    }

    private List<IncumplimientoDTO> selectRowsIncumplimientosNivelPPR(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECOID", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectTopIncumplimientoNivelPPR.execute(mapSqlParameterSource);
        return (List<IncumplimientoDTO>) out.get("RCL_INFO");
    }

    private List<ImperdonableDTO> selectRowsImperdonablesNivel(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECOID", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectTopImperdonableNivel.execute(mapSqlParameterSource);
        return (List<ImperdonableDTO>) out.get("RCL_INFO");
    }

    private List<IncumplimientoDTO> selectRowsIncumplimientosPuntoContacto(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECOID", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectTopIncumplimientoPuntoContacto.execute(mapSqlParameterSource);
        return (List<IncumplimientoDTO>) out.get("RCL_INFO");
    }

    private List<ImperdonableDTO> selectRowsImperdonablesPuntoContacto(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECOID", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico()); //ese parametro no se si va o no, dependera del sp productivo.....
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());

        Map<String, Object> out = jdbcSelectTopImperdonablePuntoContacto.execute(mapSqlParameterSource);
        return (List<ImperdonableDTO>) out.get("RCL_INFO");
    }

    public IncumplimientosImperdonablesDTO selectIncumplimientosImperdonables(IncumplimientosImperdonablesDTO entityDTO) throws CustomException {
        IncumplimientosImperdonablesDTO incumplimientoImperdonablesDTO = new IncumplimientosImperdonablesDTO();
        FiltrosDTO filtros = entityDTO.getFiltros();
        List<NegocioDTO> negocios = this.selectRowsNegocios(entityDTO.getFiltros());
        for (NegocioDTO itemNegocio : negocios) {
            filtros.setIdNegocio(itemNegocio.getIdNegocio());
            if (filtros.getNivelGeografico().equals(4)) {
                itemNegocio.setImperdonables(this.selectRowsImperdonables(filtros));
                itemNegocio.setIncumplimientos(this.selectRowsIncumplimientos(filtros));
            } else if (filtros.getNivelGeografico() >= 1 && filtros.getNivelGeografico() <= 3) {
                itemNegocio.setImperdonables(this.selectRowsImperdonablesNivel(filtros));
                itemNegocio.setIncumplimientos(this.selectRowsIncumplimientosNivel(filtros));
                if (itemNegocio.getIdNegocio().equals(4)) {
                    itemNegocio.setIncumplimientos(this.selectRowsIncumplimientosNivelPPR(filtros));
                }
            } else {
                itemNegocio.setImperdonables(this.selectRowsImperdonablesPuntoContacto(filtros));
                itemNegocio.setIncumplimientos(this.selectRowsIncumplimientosPuntoContacto(filtros));
            }
        }
        incumplimientoImperdonablesDTO.setNegocios(negocios);
        return incumplimientoImperdonablesDTO;
    }

}
