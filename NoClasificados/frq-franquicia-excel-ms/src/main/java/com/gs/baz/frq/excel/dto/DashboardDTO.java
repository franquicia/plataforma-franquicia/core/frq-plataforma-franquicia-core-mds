/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class DashboardDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_dashboard")
    private Integer id_dashboard;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_configuracion_disciplina")
    private Integer id_configuracion_disciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer id_disciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer id_protocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String id_ceco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_territorio")
    private String id_territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_zona")
    private String id_zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_region")
    private String id_region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_disciplina")
    private Integer id_negocio_disciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_imperdonable")
    private Integer numero_imperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_imcumplimiento")
    private Integer numero_imcumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_termino")
    private String fecha_termino;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_obtenidos")
    private Integer puntos_obtenidos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_esperados")
    private Integer puntos_esperados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_efectos")
    private Integer id_efectos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oportunidades")
    private Integer oportunidades;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_negocio")
    private String id_ceco_negocio;

    public Integer getId_dashboard() {
        return id_dashboard;
    }

    public void setId_dashboard(Integer id_dashboard) {
        this.id_dashboard = id_dashboard;
    }

    public Integer getId_configuracion_disciplina() {
        return id_configuracion_disciplina;
    }

    public void setId_configuracion_disciplina(Integer id_configuracion_disciplina) {
        this.id_configuracion_disciplina = id_configuracion_disciplina;
    }

    public Integer getId_disciplina() {
        return id_disciplina;
    }

    public void setId_disciplina(Integer id_disciplina) {
        this.id_disciplina = id_disciplina;
    }

    public Integer getId_protocolo() {
        return id_protocolo;
    }

    public void setId_protocolo(Integer id_protocolo) {
        this.id_protocolo = id_protocolo;
    }

    public String getId_ceco() {
        return id_ceco;
    }

    public void setId_ceco(String id_ceco) {
        this.id_ceco = id_ceco;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getId_territorio() {
        return id_territorio;
    }

    public void setId_territorio(String id_territorio) {
        this.id_territorio = id_territorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getId_zona() {
        return id_zona;
    }

    public void setId_zona(String id_zona) {
        this.id_zona = id_zona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getId_region() {
        return id_region;
    }

    public void setId_region(String id_region) {
        this.id_region = id_region;
    }

    public Integer getId_negocio_disciplina() {
        return id_negocio_disciplina;
    }

    public void setId_negocio_disciplina(Integer id_negocio_disciplina) {
        this.id_negocio_disciplina = id_negocio_disciplina;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getNumero_imperdonable() {
        return numero_imperdonable;
    }

    public void setNumero_imperdonable(Integer numero_imperdonable) {
        this.numero_imperdonable = numero_imperdonable;
    }

    public Integer getNumero_imcumplimiento() {
        return numero_imcumplimiento;
    }

    public void setNumero_imcumplimiento(Integer numero_imcumplimiento) {
        this.numero_imcumplimiento = numero_imcumplimiento;
    }

    public String getFecha_termino() {
        return fecha_termino;
    }

    public void setFecha_termino(String fecha_termino) {
        this.fecha_termino = fecha_termino;
    }

    public Integer getPuntos_obtenidos() {
        return puntos_obtenidos;
    }

    public void setPuntos_obtenidos(Integer puntos_obtenidos) {
        this.puntos_obtenidos = puntos_obtenidos;
    }

    public Integer getPuntos_esperados() {
        return puntos_esperados;
    }

    public void setPuntos_esperados(Integer puntos_esperados) {
        this.puntos_esperados = puntos_esperados;
    }

    public Integer getId_efectos() {
        return id_efectos;
    }

    public void setId_efectos(Integer id_efectos) {
        this.id_efectos = id_efectos;
    }

    public Integer getOportunidades() {
        return oportunidades;
    }

    public void setOportunidades(Integer oportunidades) {
        this.oportunidades = oportunidades;
    }

    public String getId_ceco_negocio() {
        return id_ceco_negocio;
    }

    public void setId_ceco_negocio(String id_ceco_negocio) {
        this.id_ceco_negocio = id_ceco_negocio;
    }

    @Override
    public String toString() {
        return "DashboardDTO{" + "id_dashboard=" + id_dashboard + ", id_configuracion_disciplina=" + id_configuracion_disciplina + ", id_disciplina=" + id_disciplina + ", id_protocolo=" + id_protocolo + ", id_ceco=" + id_ceco + ", descripcion=" + descripcion + ", id_territorio=" + id_territorio + ", territorio=" + territorio + ", id_zona=" + id_zona + ", zona=" + zona + ", region=" + region + ", id_region=" + id_region + ", id_negocio_disciplina=" + id_negocio_disciplina + ", calificacion=" + calificacion + ", numero_imperdonable=" + numero_imperdonable + ", numero_imcumplimiento=" + numero_imcumplimiento + ", fecha_termino=" + fecha_termino + ", puntos_obtenidos=" + puntos_obtenidos + ", puntos_esperados=" + puntos_esperados + ", id_efectos=" + id_efectos + ", oportunidades=" + oportunidades + ", id_ceco_negocio=" + id_ceco_negocio + '}';
    }

}
