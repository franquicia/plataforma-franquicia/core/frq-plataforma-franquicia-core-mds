/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos entrada", value = "DatosEntrada")
public class CargaVisitasRequestDTO {

    @JsonProperty(value = "centroCostosBase")
    @ApiModelProperty(notes = "Centro de costos base de la sucursal. `Cifre el valor de éste campo con la llave pública (accesoPublico) con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "480100")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String ceco;

    @JsonProperty(value = "fecha")
    @ApiModelProperty(notes = "Fecha de consulta", example = "02/02/2021")
    private String fecha;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "CargaVisitasRequestDTO{" + "ceco=" + ceco + ", fecha=" + fecha + '}';
    }

}
