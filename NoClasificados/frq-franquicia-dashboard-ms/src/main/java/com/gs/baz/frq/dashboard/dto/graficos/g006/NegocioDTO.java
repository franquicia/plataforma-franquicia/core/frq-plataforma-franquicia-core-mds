/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g006;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class NegocioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "totales")
    private Integer totales;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cumple")
    private Integer cumple;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "porcentaje")
    private Integer porcentaje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntaje")
    private Integer puntaje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public NegocioDTO() {
    }

    public NegocioDTO(Integer puntaje) {
        this.puntaje = puntaje;
    }

    public Integer getTotales() {
        return totales;
    }

    public void setTotales(BigDecimal totales) {
        this.totales = (totales == null ? null : totales.intValue());
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public Integer getCumple() {
        return cumple;
    }

    public void setCumple(BigDecimal cumple) {
        this.cumple = (cumple == null ? null : cumple.intValue());
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = (porcentaje == null ? null : porcentaje.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    public Integer getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Integer puntaje) {
        this.puntaje = puntaje;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", totales=" + totales + ", cumple=" + cumple + ", porcentaje=" + porcentaje + ", puntaje=" + puntaje + ", filtros=" + filtros + '}';
    }

}
