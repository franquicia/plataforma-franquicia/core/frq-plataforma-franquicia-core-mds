package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g005;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g005.DashboardCertificacionG005DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG005RowMapper implements RowMapper<DashboardCertificacionG005DTO> {

    private DashboardCertificacionG005DTO dashboardCertificacionG005DTO;

    @Override
    public DashboardCertificacionG005DTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        dashboardCertificacionG005DTO = new DashboardCertificacionG005DTO();
        dashboardCertificacionG005DTO.setSemana((BigDecimal) rs.getObject("SEMANA"));
        dashboardCertificacionG005DTO.setTotalCertificaciones((BigDecimal) rs.getObject("TOTAL_CERTIFICACIONES"));
        dashboardCertificacionG005DTO.setRango((BigDecimal) rs.getObject("RANGO"));
        dashboardCertificacionG005DTO.setRangoColor(rs.getString("RANGOCOLOR"));

        return dashboardCertificacionG005DTO;
    }
}
