/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.PorcentajePuntoVentaAprobado;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PorcentajePuntosVentaAprobadosRowMapper implements RowMapper<PorcentajePuntoVentaAprobado> {

    @Override
    public PorcentajePuntoVentaAprobado mapRow(ResultSet rs, int rowNum) throws SQLException {
        PorcentajePuntoVentaAprobado nivelesGeograficos = new PorcentajePuntoVentaAprobado();
        nivelesGeograficos.setAprobados(rs.getInt("TOTAL_PDVAPROBADOS"));
        nivelesGeograficos.setPorcentaje(rs.getInt("PORCENTAJE"));
        return nivelesGeograficos;
    }
}
