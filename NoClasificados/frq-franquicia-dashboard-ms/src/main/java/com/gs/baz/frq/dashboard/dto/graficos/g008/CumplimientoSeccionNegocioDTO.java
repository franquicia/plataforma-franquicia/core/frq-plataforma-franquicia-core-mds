/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g008;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author B73601
 */
public class CumplimientoSeccionNegocioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulos")
    List<ModuloDTO> modulos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificaciones")
    List<CalificacionDTO> calificaciones;

    public List<ModuloDTO> getModulos() {
        return modulos;
    }

    public void setModulos(List<ModuloDTO> modulos) {
        this.modulos = modulos;
    }

    public List<CalificacionDTO> getCalificaciones() {
        return calificaciones;
    }

    public void setCalificaciones(List<CalificacionDTO> calificaciones) {
        this.calificaciones = calificaciones;
    }

    @Override
    public String toString() {
        return "CumplimientoSeccionNegocioDTO{" + "modulos=" + modulos + ", calificaciones=" + calificaciones + '}';
    }

}
