/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.BandejaProveedor;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class BandejasProveedoresLayoutRowMapper implements RowMapper<BandejaProveedor> {

    @Override
    public BandejaProveedor mapRow(ResultSet rs, int rowNum) throws SQLException {
        BandejaProveedor bandejaProveedor = new BandejaProveedor();
        bandejaProveedor.setIdNegocio(rs.getInt("IDNEGOCIO"));
        bandejaProveedor.setNombreNegocio(rs.getString("NEGOCIO"));
        bandejaProveedor.setIdCeco(rs.getInt("FIID_CECO"));
        bandejaProveedor.setNombreCentroCostos(rs.getString("CECO"));
        bandejaProveedor.setIdProveedor(rs.getInt("FIIDPROVEEDOR"));
        bandejaProveedor.setFormato(rs.getString("FORMATO"));
        bandejaProveedor.setIdFolio(rs.getInt("FIIDFOLIO"));
        bandejaProveedor.setNombreEstado(rs.getString("ESTADOFOLIO"));
        bandejaProveedor.setIdEstado(rs.getInt("FIESTADOCARGA"));
        bandejaProveedor.setFechaFolio(rs.getString("FDFECHAFOLIO"));
        bandejaProveedor.setFechaActualizacion(rs.getString("FECHA_ACTUALIZACION"));
        bandejaProveedor.setIdTerritorio(rs.getString("IDTERRITORIO"));
        bandejaProveedor.setTerritorio(rs.getString("TERRITORIO"));
        bandejaProveedor.setComentario(rs.getString("COMENTARIO"));
        return bandejaProveedor;
    }
}
