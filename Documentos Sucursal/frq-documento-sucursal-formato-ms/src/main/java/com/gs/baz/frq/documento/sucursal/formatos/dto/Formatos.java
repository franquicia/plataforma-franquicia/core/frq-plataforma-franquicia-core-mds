/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.formatos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de formato", value = "formatos")
public class Formatos {

    @JsonProperty(value = "formatos")
    @ApiModelProperty(notes = "formatos")
    private List<Formato> formatos;

    public Formatos(List<Formato> formatos) {
        this.formatos = formatos;
    }

    public List<Formato> getFormatos() {
        return formatos;
    }

    public void setFormatos(List<Formato> formatos) {
        this.formatos = formatos;
    }

    @Override
    public String toString() {
        return "Formatos{" + "formatos=" + formatos + '}';
    }

}
