/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.accesos.sistema.rutas.dto.Ruta;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de la vista", value = "VistaBase")
public class VistaBase {

    @JsonProperty(value = "idVistaPadre")
    @ApiModelProperty(notes = "Identificador de la vista padre", example = "1")
    private Integer idVistaPadre;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del vista", example = "Plataforma de Franquicia")
    private String nombre;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del vista", example = "Plataforma de Franquicia")
    private String descripcion;

    @JsonProperty(value = "imagen")
    @ApiModelProperty(notes = "Imagen de la vista", example = "@/assets/img/")
    private String imagen;

    @JsonProperty(value = "origen")
    @ApiModelProperty(notes = "Origen de la vista", example = "perfil")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String origen;

    @JsonProperty(value = "idRuta")
    @ApiModelProperty(notes = "Identificador de la ruta", example = "1")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idRuta;

    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Datos de la ruta")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Ruta ruta;

    @JsonProperty(value = "propiedades")
    @ApiModelProperty(notes = "Propiedades de la vista")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> propiedades;

    public Integer getIdVistaPadre() {
        return idVistaPadre;
    }

    public void setIdVistaPadre(Integer idVistaPadre) {
        this.idVistaPadre = idVistaPadre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public Map<String, Object> getPropiedades() {
        return propiedades;
    }

    public void setPropiedades(Map<String, Object> propiedades) {
        this.propiedades = propiedades;
    }

    @Override
    public String toString() {
        return "VistaBase{" + "idVistaPadre=" + idVistaPadre + ", nombre=" + nombre + ", descripcion=" + descripcion + ", imagen=" + imagen + ", origen=" + origen + ", idRuta=" + idRuta + ", ruta=" + ruta + ", propiedades=" + propiedades + '}';
    }

}
