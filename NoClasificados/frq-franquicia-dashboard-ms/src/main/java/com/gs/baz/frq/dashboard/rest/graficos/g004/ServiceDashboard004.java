/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g004;

import com.gs.baz.frq.dashboard.dao.graficos.g004.DashboardDAOImpl004;
import com.gs.baz.frq.dashboard.dto.graficos.g004.HistogramaNacionalDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard004 {

    @Autowired
    private DashboardDAOImpl004 dashboardDAOImpl004;

    @RequestMapping(value = "secure/grafico/get/histograma/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HistogramaNacionalDTO> getHistogramaNacional(@RequestBody HistogramaNacionalDTO histogramaNacional) throws CustomException {
        return dashboardDAOImpl004.selectRowsHistogramaGeneral(histogramaNacional);
    }

}
