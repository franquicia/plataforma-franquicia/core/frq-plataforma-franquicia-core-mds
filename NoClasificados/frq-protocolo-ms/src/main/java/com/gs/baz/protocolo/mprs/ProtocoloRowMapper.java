package com.gs.baz.protocolo.mprs;

import com.gs.baz.protocolo.dto.ProtocoloDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ProtocoloRowMapper implements RowMapper<ProtocoloDTO> {

    private ProtocoloDTO protocolo;

    @Override
    public ProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        protocolo = new ProtocoloDTO();
        protocolo.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        protocolo.setDescripcion(rs.getString("FCDESCRIPCION"));
        protocolo.setObservacion(rs.getString("FCOBSERVACION"));
        protocolo.setStatus(((BigDecimal) rs.getObject("FCSTATUS")));
        return protocolo;
    }
}
