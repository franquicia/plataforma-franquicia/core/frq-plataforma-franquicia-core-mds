/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioFechas;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.EstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firmas;
import com.gs.baz.frq.modelo.negocio.folios.dto.Firma;
import com.gs.baz.frq.modelo.negocio.folios.dto.UnidadNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class EstadoFolioRowMapper implements RowMapper<BusquedaFolioFechas> {

    private BusquedaFolioFechas estadoFolio;

    @Override
    public BusquedaFolioFechas mapRow(ResultSet rs, int rowNum) throws SQLException {
        estadoFolio = new BusquedaFolioFechas();
        Firmas firmas = new Firmas();
        estadoFolio.setDocumento(new Documento(rs.getString("FCTITULO")));
        estadoFolio.setIdFolio(rs.getInt("FIIDFOLIO"));
        estadoFolio.setCreador(new Empleado(rs.getString("CREADOR")));
        estadoFolio.setEstado(new EstadoFolio(rs.getInt("ID_ESTATUS"), rs.getString("ESTATUS")));
        firmas.setUltima(new Firma(new Empleado(rs.getString("ULTIMAFIRMA"))));
        List<UnidadNegocio> unidadesNegocio = new ArrayList<>();
        try {
            String arregloUnidadNegocio[] = rs.getString("UNIDADES_NEGOCIO").split(",");
            for (String unidadNegocio : arregloUnidadNegocio) {
                UnidadNegocio objetounidadNegocio = new UnidadNegocio();
                objetounidadNegocio.setDescripcion(unidadNegocio);
                unidadesNegocio.add(objetounidadNegocio);
            }
        } catch (Exception ex) {
        }
        estadoFolio.setUnidadesNegocio(unidadesNegocio);
        List<Firma> pendientes = new ArrayList<>();
        try {
            String arreglorevisor[] = rs.getString("FIRMAS_PENDIENTES").split(",");
            for (String revisor : arreglorevisor) {
                pendientes.add(new Firma(new Empleado(revisor)));
            }
        } catch (Exception ex) {
        }
        firmas.setPendientes(pendientes);
        estadoFolio.setFirmas(firmas);
        return estadoFolio;
    }
}
