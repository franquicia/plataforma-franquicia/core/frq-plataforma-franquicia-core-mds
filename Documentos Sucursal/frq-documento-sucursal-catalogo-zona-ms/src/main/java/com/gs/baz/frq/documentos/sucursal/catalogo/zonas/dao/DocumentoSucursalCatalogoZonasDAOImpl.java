/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dao;

import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.AltaCatalogoZona;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.ParametrosCatalogoZona;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.CatalogoZonaBase;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.CatalogoZonas;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.mprs.CatalogoZonaRowMapper;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.mprs.CatalogoZonasRowMapper;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalCatalogoZonasDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcDelete;

    /*
    private DefaultJdbcCall jdbcInsertNegocioZona;
    private DefaultJdbcCall jdbcUpdateNegocioZona;
    private DefaultJdbcCall jdbcSelectNegocioZona;
    private DefaultJdbcCall jdbcSelectAllNegocioZona;
    private DefaultJdbcCall jdbcDeleteNegocioZona;
     */
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PADMLAYOUTZONA";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_ZONA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_ZONA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_ZONA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SP_GET_ZONA");
        jdbcSelect.returningResultSet("PA_CDATOS", new CatalogoZonaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_GET_ZONA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new CatalogoZonasRowMapper());

        /*
        String catalogoNegocioZona = "PADMLAYOUTZONA";
        jdbcInsertNegocioZona = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertNegocioZona.withSchemaName(schema);
        jdbcInsertNegocioZona.withCatalogName(catalogoNegocioZona);
        jdbcInsertNegocioZona.withProcedureName("SP_INS_NEGOZONA");

        jdbcUpdateNegocioZona = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateNegocioZona.withSchemaName(schema);
        jdbcUpdateNegocioZona.withCatalogName(catalogoNegocioZona);
        jdbcUpdateNegocioZona.withProcedureName("SP_ACT_NEGOZONA");

        jdbcDeleteNegocioZona = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteNegocioZona.withSchemaName(schema);
        jdbcDeleteNegocioZona.withCatalogName(catalogoNegocioZona);
        jdbcDeleteNegocioZona.withProcedureName("SP_DEL_NEGOZONA");

        jdbcSelectNegocioZona = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegocioZona.withSchemaName(schema);
        jdbcSelectNegocioZona.withCatalogName(catalogoNegocioZona);
        jdbcSelectNegocioZona.withProcedureName("SP_GET_NEGOZONA");
        jdbcSelectNegocioZona.returningResultSet("PA_CDATOS", new NegocioZonaRowMapper());

        jdbcSelectAllNegocioZona = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAllNegocioZona.withSchemaName(schema);
        jdbcSelectAllNegocioZona.withCatalogName(catalogoNegocioZona);
        jdbcSelectAllNegocioZona.withProcedureName("SP_GET_NEGOZONA");
        jdbcSelectAllNegocioZona.returningResultSet("PA_CDATOS", new NegocioZonaRowMapper());
         */
    }

    public CatalogoZonaBase selectRow(Long idCampoSensible) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDZONA", idCampoSensible);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<CatalogoZonaBase> data = (List<CatalogoZonaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<CatalogoZonas> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDZONA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<CatalogoZonas>) out.get("PA_CDATOS");
    }

    public AltaCatalogoZona insertRow(ParametrosCatalogoZona entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_TIPOINFOR", entityDTO.getIdTipoInformacion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);

            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_IDZONA");
                return new AltaCatalogoZona(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosCatalogoZona entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_TIPOINFOR", entityDTO.getIdTipoInformacion());
            mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdCatalogoZona());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    /*
    ///---NEGOCIO ZONA
    
    public NegocioZonaBase selectRowNegocioZona(ParametrosNegocioZona entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdZona());
        mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
        
        Map<String, Object> out = jdbcSelectNegocioZona.execute(mapSqlParameterSource);
        List<NegocioZonaBase> data = (List<NegocioZonaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<NegocioZona> selectAllRowsNegocioZona() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDZONA", null);
        mapSqlParameterSource.addValue("PA_IDNEGO", null);
        Map<String, Object> out = jdbcSelectAllNegocioZona.execute(mapSqlParameterSource);
        return (List<NegocioZona>) out.get("PA_CDATOS");
    }
    
    
    public AltaNegocioZona insertNegocioZona(ParametrosNegocioZona entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            Map<String, Object> out = jdbcInsertNegocioZona.execute(mapSqlParameterSource);
            
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaNegocioZona(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateNegocioZona(ParametrosNegocioZona entityDTO) throws Exception {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcInsertNegocioZona.execute(mapSqlParameterSource);
            
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public void deleteRowNegocioZona(ParametrosNegocioZona entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            Map<String, Object> out = jdbcDeleteNegocioZona.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

     */
}
