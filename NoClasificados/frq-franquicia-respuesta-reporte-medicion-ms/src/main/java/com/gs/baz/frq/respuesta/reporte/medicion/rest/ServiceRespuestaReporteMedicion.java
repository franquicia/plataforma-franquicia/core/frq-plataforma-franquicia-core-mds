/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.respuesta.reporte.medicion.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.respuesta.reporte.medicion.dao.RespuestaReporteMedicionDAOImpl;
import com.gs.baz.frq.respuesta.reporte.medicion.dto.RespuestaReporteMedicionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlfr
 */
@RestController
@RequestMapping("service/franquicia/respuesta/medicion")
public class ServiceRespuestaReporteMedicion {

    @Autowired
    private RespuestaReporteMedicionDAOImpl respuestaMedicionDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RespuestaReporteMedicionDTO> getRespuestasReporteMedicion() throws CustomException {
        return respuestaMedicionDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RespuestaReporteMedicionDTO> getRespuestaReporteMedicion(@RequestParam(value = "idDashboard", required = false) Long idDashboard, @RequestParam(value = "idPregunta", required = false) Long idPregunta) throws CustomException {
        return respuestaMedicionDAOImpl.selectRow(idDashboard, idPregunta);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaReporteMedicionDTO post(@RequestBody RespuestaReporteMedicionDTO respuestaReporteMedicion) throws CustomException {
        if (respuestaReporteMedicion.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (respuestaReporteMedicion.getIdPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta"));
        }
        if (respuestaReporteMedicion.getRespuesta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("respuesta"));
        }
        if (respuestaReporteMedicion.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (respuestaReporteMedicion.getIdConfigDis() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_config_dis"));
        }
        if (respuestaReporteMedicion.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        if (respuestaReporteMedicion.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (respuestaReporteMedicion.getNumSerie() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("num_serie"));
        }

        return respuestaMedicionDAOImpl.insertRow(respuestaReporteMedicion);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaReporteMedicionDTO putRespuestasReporteMedicion(@RequestBody RespuestaReporteMedicionDTO respuestaReporteMedicion) throws CustomException {
        if (respuestaReporteMedicion.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (respuestaReporteMedicion.getIdPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta"));
        }
        if (respuestaReporteMedicion.getRespuesta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("respuesta"));
        }
        if (respuestaReporteMedicion.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (respuestaReporteMedicion.getIdConfigDis() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_config_dis"));
        }
        if (respuestaReporteMedicion.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion_esperado"));
        }
        if (respuestaReporteMedicion.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (respuestaReporteMedicion.getNumSerie() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("num_serie"));
        }
        return respuestaMedicionDAOImpl.updateRow(respuestaReporteMedicion);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaReporteMedicionDTO deleteRespuestasReporteMedicion(@RequestBody RespuestaReporteMedicionDTO respuestasReporteMedicion) throws CustomException {
        if (respuestasReporteMedicion.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (respuestasReporteMedicion.getIdPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta"));
        }

        return respuestaMedicionDAOImpl.deleteRow(respuestasReporteMedicion);
    }
}
