/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class RequiredDataException extends Exception {

    private List<String> fields;

    public RequiredDataException(String message) {
        super(message);
    }

    public RequiredDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequiredDataException(Throwable cause) {
        super(cause);
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

}
