/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto;

/**
 *
 * @author kramireza
 */
public class Entrada {

    String url;
    String cadenaJson;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCadenaJson() {
        return cadenaJson;
    }

    public void setCadenaJson(String cadenaJson) {
        this.cadenaJson = cadenaJson;
    }

    @Override
    public String toString() {
        return "Entrada:" + "url=" + url + ", cadenaJson=" + cadenaJson + ';';
    }

}
