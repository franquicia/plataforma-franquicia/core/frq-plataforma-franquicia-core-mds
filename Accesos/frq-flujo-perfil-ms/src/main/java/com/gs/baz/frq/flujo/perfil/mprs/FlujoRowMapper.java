package com.gs.baz.frq.flujo.perfil.mprs;

import com.gs.baz.frq.flujo.perfil.dto.FlujoDTO;
import com.gs.baz.frq.flujo.perfil.dto.FlujoPerfilDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FlujoRowMapper implements RowMapper<FlujoPerfilDTO> {

    private FlujoPerfilDTO flujoPerfilDTO;
    private FlujoDTO flujoDTO;

    @Override
    public FlujoPerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        flujoPerfilDTO = new FlujoPerfilDTO();
        flujoDTO = new FlujoDTO();
        flujoPerfilDTO.setIdFlujoPerfil((BigDecimal) rs.getObject("FIIDFLUJO_PERFIL"));
        flujoDTO.setIdFlujo((BigDecimal) rs.getObject("FIID_FLUJO"));
        flujoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        flujoDTO.setTipoApp(rs.getString("FCTIPOAPP"));
        flujoPerfilDTO.setFlujoDTO(flujoDTO);
        flujoPerfilDTO.setIdPerfil((BigDecimal) rs.getObject("FIID_PERFIL"));
        return flujoPerfilDTO;
    }
}
