/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class OverwriteFileException extends Exception{

    public OverwriteFileException() {
    }

    public OverwriteFileException(String string) {
        super(string);
    }

    public OverwriteFileException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public OverwriteFileException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
