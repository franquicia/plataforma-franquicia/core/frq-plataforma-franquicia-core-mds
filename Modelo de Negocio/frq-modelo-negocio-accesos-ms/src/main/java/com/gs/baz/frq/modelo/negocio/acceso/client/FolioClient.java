/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Folio;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Component
public class FolioClient {

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private RestTemplate restTemplate;

    public Folio getFolio(HttpHeaders httpHeaders, Integer idFolio) throws Exception {
        Folio folio;
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folios/v1/" + idFolio + "/resumen";
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.GET, httpEntityUsuario, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);
            folio = objectMapper.readValue(objectNode.get("resultado").toString(), Folio.class);
            folio.setIdFolio(idFolio);
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        } catch (IOException ex) {
            throw new Exception(ex);
        }
        return folio;
    }
}
