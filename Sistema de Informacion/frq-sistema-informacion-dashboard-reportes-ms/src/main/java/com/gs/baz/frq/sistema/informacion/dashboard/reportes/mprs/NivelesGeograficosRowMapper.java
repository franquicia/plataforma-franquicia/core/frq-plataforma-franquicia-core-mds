/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NivelGeografico;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NivelesGeograficosRowMapper implements RowMapper<NivelGeografico> {

    @Override
    public NivelGeografico mapRow(ResultSet rs, int rowNum) throws SQLException {
        NivelGeografico nivelesGeograficos = new NivelGeografico();
        nivelesGeograficos.setCentroCostos((rs.getObject("ID_CECO") != null ? new Integer(rs.getString("ID_CECO")) : null));
        nivelesGeograficos.setDescripcion(rs.getString("DESCRIPCION"));
        nivelesGeograficos.setIndice(rs.getInt("INDICE"));
        return nivelesGeograficos;
    }
}
