/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de tipo de Informacion", value = "tiposInformacion")
public class TiposInformacion {

    @JsonProperty(value = "tiposInformacion")
    @ApiModelProperty(notes = "tipos Informacion")
    private List<TipoInformacion> tiposInformacion;

    public TiposInformacion(List<TipoInformacion> tiposInformacion) {
        this.tiposInformacion = tiposInformacion;
    }

    public List<TipoInformacion> getTiposInformacion() {
        return tiposInformacion;
    }

    public void setTiposInformacion(List<TipoInformacion> tiposInformacion) {
        this.tiposInformacion = tiposInformacion;
    }

}
