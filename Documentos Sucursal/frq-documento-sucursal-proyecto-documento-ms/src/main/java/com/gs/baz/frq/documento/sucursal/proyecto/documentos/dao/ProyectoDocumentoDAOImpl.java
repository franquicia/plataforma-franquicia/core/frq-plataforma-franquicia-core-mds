/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyecto.documentos.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.AltaProyectoDocumento;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.ParametrosProyectoDocumento;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.ProyectoDocumento;
import com.gs.baz.frq.documento.sucursal.proyecto.documentos.mprs.ProyectoDocumentosRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class ProyectoDocumentoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PAPROYECTOS";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_PROYDOC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_PROYDOC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_PROYDOC");

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_SEL_PROYDOC");
        jdbcSelectAll.returningResultSet("RCL_PROY", new ProyectoDocumentosRowMapper());

    }

    public List<ProyectoDocumento> selectAllRows(Long idProyecto) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDPROYD", idProyecto);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<ProyectoDocumento>) out.get("RCL_PROY");
    }

    public List<ProyectoDocumento> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDPROYD", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<ProyectoDocumento>) out.get("RCL_PROY");
    }

    public AltaProyectoDocumento insertRow(ParametrosProyectoDocumento entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDPROY", entityDTO.getIdProyecto());
            mapSqlParameterSource.addValue("PA_IDDOC", entityDTO.getIdDocumento());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_IDPROYD");
                return new AltaProyectoDocumento(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosProyectoDocumento entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDPROY", entityDTO.getIdProyecto());
            mapSqlParameterSource.addValue("PA_IDDOC", entityDTO.getIdDocumento());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDPROY", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
