/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.FirmaCorreo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FirmasRowMapper implements RowMapper<FirmaCorreo> {

    private FirmaCorreo firma;

    @Override
    public FirmaCorreo mapRow(ResultSet rs, int rowNum) throws SQLException {
        firma = new FirmaCorreo();
        firma.setIdFirma(rs.getInt("FIIDFIRMA"));
        firma.setIdFolio(rs.getInt("FIIDFOLIO"));
        firma.setNumeroEmpleado(rs.getInt("FINUMEROEMPLEADO"));
        firma.setIdJerarquia(rs.getInt("FIIDJERARQUIA"));
        firma.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        firma.setEliminado(rs.getInt("FIELIMINADO"));
        firma.setCorreo(rs.getString("FCCORREO"));
        firma.setIndice(rs.getInt("FIINDICE"));
        return firma;
    }
}
