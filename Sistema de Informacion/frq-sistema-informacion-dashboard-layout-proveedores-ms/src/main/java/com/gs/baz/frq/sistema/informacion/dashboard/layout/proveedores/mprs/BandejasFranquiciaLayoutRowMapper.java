/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.BandejaFranquicia;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class BandejasFranquiciaLayoutRowMapper implements RowMapper<BandejaFranquicia> {

    @Override
    public BandejaFranquicia mapRow(ResultSet rs, int rowNum) throws SQLException {
        BandejaFranquicia bandejaFranquicia = new BandejaFranquicia();
        bandejaFranquicia.setIdArchivo(rs.getInt("FIIDARCHIVO"));
        bandejaFranquicia.setIdFolio(rs.getInt("FIIDFOLIO"));
        bandejaFranquicia.setRuta(rs.getString("FCRUTA"));
        bandejaFranquicia.setTipo(rs.getInt("FITIPO"));
        bandejaFranquicia.setIdEstado(rs.getInt("FIESTADO"));
        bandejaFranquicia.setFecha(rs.getString("FDFECHA"));
        bandejaFranquicia.setCentroCostos(rs.getString("FCID_CECO"));
       
        return bandejaFranquicia;
    }
}
