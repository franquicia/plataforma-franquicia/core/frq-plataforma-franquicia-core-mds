package com.gs.baz.frq.respuesta.reporte.medicion.mprs;

import com.gs.baz.frq.respuesta.reporte.medicion.dto.RespuestaReporteMedicionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author jlfr
 */
public class RespuestaReporteMedicionRowMapper implements RowMapper<RespuestaReporteMedicionDTO> {

    private RespuestaReporteMedicionDTO respuestaReporteMedicion;

    @Override
    public RespuestaReporteMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        respuestaReporteMedicion = new RespuestaReporteMedicionDTO();
        respuestaReporteMedicion.setIdDashboard(((BigDecimal) rs.getObject("FIDASHBOARD_ID")));
        respuestaReporteMedicion.setIdPregunta(((BigDecimal) rs.getObject("FIID_PREGUNTA")));
        respuestaReporteMedicion.setRespuesta(rs.getString("FCRESPUESTA"));
        respuestaReporteMedicion.setIdProtocolo((BigDecimal) rs.getObject("FIID_PROTOCOLO"));
        respuestaReporteMedicion.setIdConfigDis(((BigDecimal) rs.getObject("FICONFIGDIS_ID")));
        respuestaReporteMedicion.setPonderacion(((BigDecimal) rs.getObject("FIPONDERACION")));
        respuestaReporteMedicion.setModulo(rs.getString("FCMODULO"));
        respuestaReporteMedicion.setNumSerie(((BigDecimal) rs.getObject("FINUMSERIE")));
        respuestaReporteMedicion.setUsuarioMod((rs.getString("FCUSUARIO_MOD")));
        respuestaReporteMedicion.setFechaMod(rs.getString("FDFECHA_MOD"));
        respuestaReporteMedicion.setImperdonable(((BigDecimal) rs.getObject("PA_FIMPERDONABLE")));

        return respuestaReporteMedicion;
    }
}
