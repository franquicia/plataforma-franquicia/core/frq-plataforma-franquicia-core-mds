/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.indicador.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.indicador.dao.IndicadorDAOImpl;
import com.gs.baz.indicador.dto.IndicadorDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/indicador")
public class ServiceIndicador {

    @Autowired
    private IndicadorDAOImpl indicadorDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<IndicadorDTO> getIndicador() throws CustomException {
        return indicadorDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_indicador}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public IndicadorDTO getIndicador(@PathVariable("id_indicador") Long id_indicador) throws CustomException {
        return indicadorDAOImpl.selectRow(id_indicador);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public IndicadorDTO postIndicador(@RequestBody IndicadorDTO indicador) throws CustomException {
        if (indicador.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return indicadorDAOImpl.insertRow(indicador);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public IndicadorDTO putIndicador(@RequestBody IndicadorDTO indicador) throws CustomException {
        if (indicador.getIdIndicador() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_indicador"));
        }
        if (indicador.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return indicadorDAOImpl.updateRow(indicador);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public IndicadorDTO deleteIndicador(@RequestBody IndicadorDTO indicador) throws CustomException {
        if (indicador.getIdIndicador() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_indicador"));
        }
        return indicadorDAOImpl.deleteRow(indicador);
    }
}
