package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoTerritorioDTO;

/**
 *
 * @author cescobarh
 */
public class ConteoGralTerritorioRowMapper implements RowMapper<ReporteConteoTerritorioDTO> {

    private ReporteConteoTerritorioDTO status;

    @Override
    public ReporteConteoTerritorioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteConteoTerritorioDTO();

        status.setConteoRealizadas(rs.getInt("CONTEO_PROGRAMADAS"));
        status.setTerritorio(rs.getString("FCTERRITORIO"));
        status.setIdTerritorio(rs.getInt("FCID_TERRITORIO"));
        status.setConteoProgramadas(rs.getInt("CONTEO_ECHAS"));

        return status;
    }
}
