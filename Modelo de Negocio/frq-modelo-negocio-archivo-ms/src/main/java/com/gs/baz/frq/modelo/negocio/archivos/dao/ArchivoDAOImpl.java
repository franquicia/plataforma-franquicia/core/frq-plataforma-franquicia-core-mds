/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.archivos.dto.AltaArchivoVirtual;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoVirtual;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoVirtualBase;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ParametrosArchivoVirtual;
import com.gs.baz.frq.modelo.negocio.archivos.mprs.ArchivoRowMapper;
import com.gs.baz.frq.modelo.negocio.archivos.mprs.ArchivosRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class ArchivoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcDelete;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNARCHIVO";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNARCHIVOS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNARCHIVOS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNARCHIVOS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETARCHXFOLIO");
        jdbcSelect.returningResultSet("PA_CDATOS", new ArchivoRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETARCHXFOLIO");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new ArchivosRowMapper());

    }

    public ArchivoVirtualBase selectRow(Long idFolio, Long idArchivo) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDARCHIVO", idArchivo);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ArchivoVirtualBase> data = (List<ArchivoVirtualBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<ArchivoVirtual> selectAllRows(Long idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDARCHIVO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<ArchivoVirtual>) out.get("PA_CDATOS");
    }

    public AltaArchivoVirtual insertRow(ParametrosArchivoVirtual entityDTO) throws DataNotInsertedException {
        logger.debug(System.getProperty("file.encoding"));
        System.setProperty("file.encoding", "UTF-8");
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCRUTA", entityDTO.getUid());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCEXTENSION", entityDTO.getExtension());
            mapSqlParameterSource.addValue("PA_FITIPO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_FIINDICE", entityDTO.getIndice());
            mapSqlParameterSource.addValue("PA_FISIZE", entityDTO.getPeso());
            mapSqlParameterSource.addValue("PA_FCCONTENTTYPE", entityDTO.getTipoContenido());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDARCHIVO");
                return new AltaArchivoVirtual(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosArchivoVirtual entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDARCHIVO", entityDTO.getIdArchivo());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCRUTA", entityDTO.getUid());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCEXTENSION", entityDTO.getExtension());
            mapSqlParameterSource.addValue("PA_FITIPO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_FIINDICE", entityDTO.getIndice());
            mapSqlParameterSource.addValue("PA_FISIZE", entityDTO.getPeso());
            mapSqlParameterSource.addValue("PA_FCCONTENTTYPE", entityDTO.getTipoContenido());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDARCHIVO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
