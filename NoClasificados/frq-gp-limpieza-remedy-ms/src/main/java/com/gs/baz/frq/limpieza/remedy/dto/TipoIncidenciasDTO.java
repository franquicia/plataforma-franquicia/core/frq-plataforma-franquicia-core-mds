/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author kramireza
 */
@ApiModel(description = "Tipo de Incidencias", value = "TipoIncidencias")
public class TipoIncidenciasDTO {

    @JsonProperty(value = "tipoIncidencias")
    @ApiModelProperty(notes = "Tipos de incidencias")
    private List<IncidenciasDTO> tipoIncidencias;

    public List<IncidenciasDTO> getTipoIncidencias() {
        return tipoIncidencias;
    }

    public void setTipoIncidencias(List<IncidenciasDTO> tipoIncidencias) {
        this.tipoIncidencias = tipoIncidencias;
    }

    @Override
    public String toString() {
        return "TipoIncidenciasDTO{" + "tipoIncidencias=" + tipoIncidencias + '}';
    }

}
