package com.gs.baz.frq.configuracion.disciplina.mprs;

import com.gs.baz.frq.configuracion.disciplina.dto.ConfiguracionDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ConfiguracionDisciplinaRowMapper implements RowMapper<ConfiguracionDisciplinaDTO> {

    private ConfiguracionDisciplinaDTO status;

    @Override
    public ConfiguracionDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ConfiguracionDisciplinaDTO();
        status.setIdConfiguracion(((BigDecimal) rs.getObject("FICONFIGDIS_ID")));
        status.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        status.setIdDisciplina(((BigDecimal) rs.getObject("FIDISCIPLINA_ID")));
        status.setIdFrecuencia(((BigDecimal) rs.getObject("FIFRECUENCIA_ID")));
        status.setProtocolo((rs.getString("FCPROTOCOLO")));
        status.setIdNegocioDisciplina(((BigDecimal) rs.getObject("FINEGOCIODISCID")));
        return status;
    }
}
