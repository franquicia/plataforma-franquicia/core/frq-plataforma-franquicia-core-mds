/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.rest;

import com.gs.baz.frq.limpieza.remedy.bi.AprobacionesRemedyLimpiezaHilos;
import com.gs.baz.frq.limpieza.remedy.bi.CorreosLotus;
import com.gs.baz.frq.limpieza.remedy.bi.IncidenteLimpiezaBI;
import com.gs.baz.frq.limpieza.remedy.bi.ProveedoresBI;
import com.gs.baz.frq.limpieza.remedy.bi.ServiciosRemedyLimpiezaBI;
import com.gs.baz.frq.limpieza.remedy.bi.ServiciosRemedyLimpiezaDosBI;
import com.gs.baz.frq.limpieza.remedy.dto.AceptaClienteDTO;
import com.gs.baz.frq.limpieza.remedy.dto.AsignaProveedorDTO;
import com.gs.baz.frq.limpieza.remedy.dto.AsignaProveedorResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.BandejaIncidentesRequestDTO;
import com.gs.baz.frq.limpieza.remedy.dto.BandejaIncidentesResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.CerrarSinOtDTO;
import com.gs.baz.frq.limpieza.remedy.dto.DetalleAprobacionesResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.DetalleIncidenteResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.FolioRemedyDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteLimpiezaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import com.gs.baz.frq.limpieza.remedy.dto.RechazaClienteDTO;
import com.gs.baz.frq.limpieza.remedy.dto.RespuestaAceptaClienteDTO;
import com.gs.baz.frq.limpieza.remedy.dto.RespuestaAprobacionesDTO;
import com.gs.baz.frq.limpieza.remedy.dto.RespuestaCerrarSinOtDTO;
import com.gs.baz.frq.limpieza.remedy.dto.RespuestaNuevoIncidenteDTO;
import com.gs.baz.frq.limpieza.remedy.dto.RespuestaRechazaClienteDTO;
import com.gs.baz.frq.limpieza.remedy.dto.SucursalRequestDTO;
import com.gs.baz.frq.limpieza.remedy.dto.TrackingDetalleDTO;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Aprobacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfInformacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfTracking;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Informacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Tracking;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "gestion-incidentes-limpieza",
        value = "gestion-incidentes-limpieza",
        description = "API de gestión de incidentes para el capital "
        + "humano e insumos y herramientas de limpieza en sucursal.")
@RestController
@RequestMapping("/api/gestion-incidentes/limpieza/v1")
public class LimpiezaRemedyAPI {

    @Autowired
    private ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI;

    @Autowired
    private IncidenteLimpiezaBI incidenciaLimpiezaBI;

    @Autowired
    private ProveedoresBI proveedoresBI;

    @Autowired
    private ServiciosRemedyLimpiezaDosBI serviciosRemedyLimpiezaDosBI;

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Integer CERO = 0, UNO = 1, DOS = 2, CINCO = 5, SEIS = 6;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    //anteriormente /altaIncidenteLimpiezaPost
    @ApiOperation(value = "Incidente de Limpieza",
            notes = "Da de alta un incidente de limpieza, "
            + "incluye los datos de la falla;"
            + " así como quién y en qué sucursal se levanta el incidente",
            nickname = "postIncidenteLimpieza")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "x-idAcceso",
                value = "Identificador del Acceso",
                example = "987cdd90-fd7f-499c-b09d-f9ffa2599696",
                required = true, allowEmptyValue = false,
                paramType = "header", dataTypeClass = String.class)
    })
    @RequestMapping(value = "/incidencias", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public RespuestaNuevoIncidenteDTO altaIncidenteLimpiezaPost(@ApiParam(name = "Incidente",
            value = "Alta de Folio Incidente",
            required = true) @RequestBody FolioRemedyDTO folioRemedy) {
        LOGGER.log(Level.INFO, "OBJETO ENTRADA: ", folioRemedy);
        int noSucursal = 0;
        String cecoS = folioRemedy.getCeco() + "";
        if (cecoS.length() == SEIS) {
            noSucursal = Integer.parseInt(cecoS.substring(DOS, cecoS.length()));
        }
        if (cecoS.length() == CINCO) {
            noSucursal = Integer.parseInt(cecoS.substring(UNO, cecoS.length()));
        }
        CorreosLotus correosLotus = new CorreosLotus();
        String correo = "";
        if (cecoS.length() == CINCO) {
            cecoS = "0" + cecoS;
        }
        correo = correosLotus.validaCorreoSuc(cecoS);
        folioRemedy.setCorreo(correo);
        folioRemedy.setNumeroSucursal(noSucursal);
        int idFolio = serviciosRemedyLimpiezaBI.levantaFolio(folioRemedy);
        LOGGER.log(Level.INFO, "OBJETO GENERADO: ", folioRemedy);
        RespuestaNuevoIncidenteDTO folioGenerado = new RespuestaNuevoIncidenteDTO();
        if (idFolio == CERO) {
            folioGenerado.setIdIncidente(null);
        } else {
            folioGenerado.setIdIncidente(idFolio);
        }
        return folioGenerado;
    }

    //@RequestMapping(value = "/getAceptaClienteLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Aceptación del Cliente de Limpieza",
            notes = "Realiza la aceptación de un Cliente para la resolución "
            + "que se le ha dado al incidente de Limpieza reportado.",
            nickname = "putAceptaClienteLimpieza")
    @RequestMapping(value = "/clientes/estatus-aceptado",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaAceptaClienteDTO getAceptaClienteLimpieza(
            @ApiParam(name = "AceptaCliente", value = "Aceptación del cliente", required = true)
            @RequestBody AceptaClienteDTO aceptaClienteDTO) {
        RespuestaAceptaClienteDTO aceptaCliente = new RespuestaAceptaClienteDTO();
        try {
            boolean actIncidente = false;
            ArrayOfInformacion auxIncidente
                    = serviciosRemedyLimpiezaDosBI.ConsultaDetalleFolio(aceptaClienteDTO.getIdIncidente());
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("cierre técnico")) {
                            resp = CERO;
                        } else {
                            resp = UNO;
                        }
                    }
                }
            }
            if (resp == UNO) {
                resp = DOS;
            } else {
                actIncidente = serviciosRemedyLimpiezaBI.aceptaCliente(
                        aceptaClienteDTO.getIdIncidente(),
                        aceptaClienteDTO.getCalificacion(),
                        aceptaClienteDTO.getJustificacion());
            }
            if (resp != DOS) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }
            aceptaCliente.setEstatusModificado(resp);
        } catch (Exception e) {
            LOGGER.info(e);
            aceptaCliente = null;

        }
        return aceptaCliente;
    }

    //@RequestMapping(value = "/postRechazaClienteLimpieza", method = RequestMethod.POST)
    @ApiOperation(value = "Rechazo del Cliente de Limpieza",
            notes = "Realiza el rechazo de un Cliente para la resolución que se le ha dado al incidente de Limpieza reportado.",
            nickname = "putRechazaClienteLimpieza")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "x-idAcceso",
                value = "Identificador del Acceso",
                example = "987cdd90-fd7f-499c-b09d-f9ffa2599696",
                required = true, allowEmptyValue = false,
                paramType = "header",
                dataTypeClass = String.class)
    })
    @RequestMapping(value = "/clientes/estatus-rechazado", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaRechazaClienteDTO postRechazaClienteLimpieza(
            @ApiParam(name = "RechazaCliente", value = "Detalle del Rechazo por el cliente", required = true)
            @RequestBody RechazaClienteDTO rechazaCliente) {
        boolean actIncidente = false;
        RespuestaRechazaClienteDTO response = null;
        LOGGER.log(Level.INFO, "DATOS ENTRADA: ", rechazaCliente);
        ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaDosBI.ConsultaDetalleFolio(rechazaCliente.getIdIncidente());
        int resp = 0;
        if (auxIncidente != null) {
            Informacion[] arregloInc = auxIncidente.getInformacion();
            if (arregloInc != null) {
                for (Informacion aux : arregloInc) {
                    if (aux.getEstado().trim().toLowerCase().equals("en recepción")
                            && aux.getMotivoEstado().trim().toLowerCase().equals("cierre técnico")) {
                        resp = 0;
                    } else {
                        resp = 1;
                    }
                }
            }
        }
        if (resp == UNO) {
            resp = DOS;
        } else {
            actIncidente = serviciosRemedyLimpiezaBI.rechazaCliente(
                    rechazaCliente.getIdIncidente(), rechazaCliente.getJustificacion(),
                    rechazaCliente.getNombreAdjunto(), rechazaCliente.getAdjunto(),
                    rechazaCliente.getSolicitante(), rechazaCliente.getAprobador());
        }
        if (resp != DOS) {
            if (actIncidente) {
                resp = 1;
            } else {
                resp = 0;
            }
        }
        response = new RespuestaRechazaClienteDTO();
        response.setEstatus(resp);
        return response;
    }

    //@RequestMapping(value = "/cerrarSinOTLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Cerrar Incidente Sin Orden de Trabajo",
            notes = "Realiza el cierre (fin de la atención) de un Ticket sin Orden de Trabajo y"
            + " regresa el estatus de cierre.", nickname = "putCerrarSinOT")
    @RequestMapping(value = "/incidencias/estatus-cerrado",
            method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaCerrarSinOtDTO cerrarSinOT(
            @ApiParam(name = "CerrarSinOt", value = "Cierre del Ticket", required = true)
            @RequestBody CerrarSinOtDTO cerrarSinOtDTO) {
        RespuestaCerrarSinOtDTO response = null;
        try {
            LOGGER.log(Level.INFO, "params ", cerrarSinOtDTO.getIdIncidente()
                    + ", " + cerrarSinOtDTO.getResolucion() + ", " + cerrarSinOtDTO.getMotivoEstado());
            ArrayOfInformacion auxIncidente = serviciosRemedyLimpiezaDosBI.ConsultaDetalleFolio(cerrarSinOtDTO.getIdIncidente());
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if ((aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("por asignar"))
                                || (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && aux.getMotivoEstado().trim().toLowerCase().equals("rechazado proveedor"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            boolean actIncidente = false;
            if (resp == UNO) {
                resp = DOS;
            } else {
                actIncidente = serviciosRemedyLimpiezaDosBI.cierreSinOT(
                        cerrarSinOtDTO.getIdIncidente(), cerrarSinOtDTO.getResolucion(), cerrarSinOtDTO.getMotivoEstado());
            }
            if (resp != DOS) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }
            response = new RespuestaCerrarSinOtDTO();
            response.setEstatus(resp);
        } catch (Exception e) {
            response = null;
        }
        return response;
    }

    //@RequestMapping(value = "/getConsultarAprobacionesLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Consultar Aprobaciones del Incidente",
            notes = "Obtiene el detalle de las aprobaciones (aceptación del usuario a"
            + " la resolución o atención prestada al incidente) de un incidente dado.", nickname = "getConsultarAprobaciones")
    @RequestMapping(value = "/incidentes/{idIncidente}/estatus/aprobaciones",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    public RespuestaAprobacionesDTO getConsultarAprobaciones(
            @ApiParam(example = "13007939", value = "Es el identificador del incidente", required = true)
            @PathVariable("idIncidente") Integer idIncidente,
            @ApiParam(example = "6", value = "Opción de la aprobación a Consultar", required = true)
            @RequestParam("opcion") Integer opcion) throws DataNotFoundException {
        RespuestaAprobacionesDTO response = null;
        try {
            Aprobacion aprobacion = serviciosRemedyLimpiezaDosBI.consultarAprobaciones(idIncidente, opcion);
            response = new RespuestaAprobacionesDTO();
            if (aprobacion != null) {
                if (aprobacion.getA1_Base() == null) {
                    String arch1 = null;
                    response.setArchivo1Base64(arch1);
                } else {
                    InputStream in = aprobacion.getA1_Base().getInputStream();
                    byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
                    String encoded = new String(Base64.encodeBase64(byteArray), StandardCharsets.UTF_8);
                    response.setArchivo1Base64(encoded);
                }
                if (aprobacion.getA2_Base() == null) {
                    String arch1 = null;
                    response.setArchivo2Base64(arch1);
                } else {
                    InputStream in = aprobacion.getA2_Base().getInputStream();
                    byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
                    String encoded = new String(Base64.encodeBase64(byteArray), StandardCharsets.UTF_8);
                    response.setArchivo2Base64(encoded);
                }
                if (aprobacion.getJustificacion() == null) {
                    String arch1 = null;
                    response.setJustificacionAprobacion(arch1);
                } else {
                    response.setJustificacionAprobacion(aprobacion.getJustificacion());
                }
                if (aprobacion.getEstadoAprobacion() == null) {
                    String arch1 = null;
                    response.setEstadoAprobacion(arch1);
                } else {
                    response.setEstadoAprobacion(aprobacion.getEstadoAprobacion());
                }
                Date date = aprobacion.getFechaModificacion().getTime();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date1 = format1.format(date);
                response.setFecha(date1);
            } else {
                throw new DataNotFoundException();
            }
            if (response.getArchivo1Base64() == null
                    && response.getArchivo2Base64() == null
                    && response.getEstadoAprobacion() == null
                    && response.getJustificacionAprobacion() == null) {
                throw new DataNotFoundException();
            }
            return response;
        } catch (Exception e) {
            throw new DataNotFoundException();
        }
    }

    //@RequestMapping(value = "/getIncidentesLimpiezaSucursal", method = RequestMethod.GET)
    @ApiOperation(value = "Incidentes de Limpieza por Sucursal",
            notes = "Regresa los Incidentes de Limpieza con toda su información detallada "
            + "pertenecientes a una sucursal dada.", nickname = "getIncidentesLimpiezaSucursal")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "x-idAcceso", value = "Identificador del Acceso",
                example = "987cdd90-fd7f-499c-b09d-f9ffa2599696", required = true,
                allowEmptyValue = false, paramType = "header", dataTypeClass = String.class)
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/incidencias/busquedas",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ArrayList<IncidenteLimpiezaDTO> getIncidentesLimpiezaSucursal(
            @ApiParam(name = "Sucursal", value = "Es el centro de costos de la sucurusal", required = true)
            @RequestBody SucursalRequestDTO sucursalDTO) throws DataNotFoundException {
        LOGGER.log(Level.INFO, "GET INCIDENTES ", sucursalDTO.getCentroCostos());
        ArrayList<IncidenteLimpiezaDTO> arrIncidentesAbiertos = null;
        try {
            int numSucursal = 0;
            String cecoS = sucursalDTO.getCentroCostos() + "";
            if (cecoS.length() == SEIS) {
                numSucursal = Integer.parseInt(cecoS.substring(DOS, cecoS.length()));
            }
            if (cecoS.length() == CINCO) {
                numSucursal = Integer.parseInt(cecoS.substring(UNO, cecoS.length()));
            }
            arrIncidentesAbiertos = serviciosRemedyLimpiezaBI.ConsultaFoliosSucursal(numSucursal, "0");
            if (arrIncidentesAbiertos == null || arrIncidentesAbiertos.isEmpty()) {
                throw new DataNotFoundException();
            }
            LOGGER.log(Level.INFO, "incidentes-> ", arrIncidentesAbiertos);
            return arrIncidentesAbiertos;
        } catch (Exception e) {
            throw new DataNotFoundException();
        }

    }

    //@RequestMapping(value = "/getBandejaTicketsLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Bandeja de Incidentes de Limpieza",
            notes = "Regresa la bandeja con los folios de los Incidentes de Limpieza asociados "
            + "al Usuario y su información correspondiente.", nickname = "getBandejaTicketsLimpieza")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "x-idAcceso", value = "Identificador del Acceso",
                example = "987cdd90-fd7f-499c-b09d-f9ffa2599696", required = true, allowEmptyValue = false,
                paramType = "header", dataTypeClass = String.class)
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/bandejas/busquedas-incidentes",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BandejaIncidentesResponseDTO getBandejaTicketsLimpieza(
            @ApiParam(name = "BandejaEntrada", value = "Datos de entrada para consultar "
                    + "la bandeja por perfil del usuario", required = true)
            @RequestBody(required = true) BandejaIncidentesRequestDTO bandejaEntrada) throws DataNotFoundException {
        LOGGER.log(Level.INFO, "ENTRADA ", bandejaEntrada.getNumEmpleado() + " "
                + bandejaEntrada.getTipoUsuario() + " " + bandejaEntrada.getIdProveedor());
        BandejaIncidentesResponseDTO res = null;
        if (bandejaEntrada.getIdProveedor() == null) {
            res = incidenciaLimpiezaBI.getBandejaTickets(bandejaEntrada.getNumEmpleado(), bandejaEntrada.getTipoUsuario());
        } else {
            //Implementa buscar el idProveedor
            List<ProveedoresDTO> proveedoresArray = proveedoresBI.obtieneDatos(bandejaEntrada.getIdProveedor());
            ProveedoresDTO proveedorDTO = null;
            if (proveedoresArray != null && proveedoresArray.size() > CERO) {
                proveedorDTO = proveedoresArray.get(0);
                res = incidenciaLimpiezaBI.getBandejaTickets(proveedorDTO.getNombreCorto(), bandejaEntrada.getTipoUsuario());
            }
        }
        if (res == null) {
            throw new DataNotFoundException();
        }
        return res;
    }

    //@RequestMapping(value = "/getDetalleIncidenteClickLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Detalle de Incidentes al Click de Limpieza",
            notes = "Regresa la información detallada completa de un folio de Incidente de Limpieza.",
            nickname = "getDetalleIncidenteClickLimpieza")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "x-idAcceso", value = "Identificador del Acceso",
                example = "987cdd90-fd7f-499c-b09d-f9ffa2599696", required = true,
                allowEmptyValue = false, paramType = "header", dataTypeClass = String.class)
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/incidentes/{idIncidente}/detalles",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IncidenteLimpiezaDTO getDetalleIncidenteClickLimpieza(
            @ApiParam(example = "13008100", value = "Es el identificador del folio del incidente", required = true)
            @PathVariable("idIncidente") Integer idIncidente) throws DataNotFoundException {
        LOGGER.log(Level.INFO, "FOLIO DE ENTRADA ->", idIncidente);
        IncidenteLimpiezaDTO respuesta = null;
        try {
            ArrayOfInformacion arrIncidentes = serviciosRemedyLimpiezaDosBI.ConsultaDetalleFolio(idIncidente);
            if (arrIncidentes.getInformacion() != null && arrIncidentes.getInformacion().length > 0) {
                AprobacionesRemedyLimpiezaHilos hiloAprobacion = new AprobacionesRemedyLimpiezaHilos(idIncidente, 1);
                hiloAprobacion.start();
                while (hiloAprobacion.isAlive()) {
                }
                hiloAprobacion.getRespuesta();
                respuesta = incidenciaLimpiezaBI.arrOrdenado(arrIncidentes.getInformacion()[0]);
            }
            if (respuesta == null) {
                throw new DataNotFoundException();
            }
            LOGGER.log(Level.INFO, "RESPUESTA ->", respuesta);
            return respuesta;
        } catch (Exception e) {
            throw new DataNotFoundException();
        }

    }

    //@RequestMapping(value = "/getDetalleIncidenteLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Detalle de Incidentes de Limpieza",
            notes = "Regresa el Detalle de seguimiento de un incidente de Limpieza.",
            nickname = "getDetalleIncidenteLimpieza")
    @RequestMapping(value = "/incidentes/{idIncidente}/seguimiento",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    public DetalleIncidenteResponseDTO getDetalleIncidenteLimpieza(
            @ApiParam(example = "13008031", value = "Es el identificador del folio del incidente", required = true)
            @PathVariable("idIncidente") Integer idIncidente) throws DataNotFoundException {
        DetalleIncidenteResponseDTO res = new DetalleIncidenteResponseDTO();
        try {
            ArrayOfTracking arrTraking = null;
            String imagen = "";

            imagen = serviciosRemedyLimpiezaBI.ConsultaAdjunto(idIncidente);

            arrTraking = serviciosRemedyLimpiezaBI.ConsultaBitacora(idIncidente);
            ArrayList<TrackingDetalleDTO> incidenciasJsonArray = new ArrayList<>();
            if (arrTraking != null) {
                Tracking[] traking = arrTraking.getTracking();
                if (traking != null && traking.length > 0) {
                    for (Tracking aux : traking) {
                        String detalle = aux.getDetalle();
                        if (detalle.contains("||")) {
                            String arrAux[] = detalle.split("\\|\\|");
                            String estado = detalle.split("\\|\\|")[1];
                            String motivoEstado = detalle.split("\\|\\|")[3];
                            Date date = aux.getFechaEnvio().getTime();
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String date1 = format1.format(date);
                            TrackingDetalleDTO detalleJsonObj = new TrackingDetalleDTO();
                            detalleJsonObj.setEstado(estado);
                            detalleJsonObj.setMotivoEstado(motivoEstado);
                            detalleJsonObj.setFecha(date1);
                            incidenciasJsonArray.add(detalleJsonObj);
                        }
                    }

                    res.setImagen(imagen);
                    res.setTracking((io.vavr.collection.List<TrackingDetalleDTO>) incidenciasJsonArray);
                } else {
                    throw new DataNotFoundException();
                }
            } else {
                throw new DataNotFoundException();

            }
            return res;
        } catch (NumberFormatException e) {
            throw new DataNotFoundException();
        }

    }

    //@RequestMapping(value = "/getInfoCierreLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Informacion del Cierre del Incidente de Limpieza",
            notes = "Regresa la información con el detalle del cierre (fin de la atención del incidente) "
            + "realizado al Ticket de Limpieza dado.", nickname = "getInformacionCierreLimpieza")
    @RequestMapping(value = "/incidentes/{idIncidente}/estatus",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    public DetalleAprobacionesResponseDTO getInformacionCierreLimpieza(
            @ApiParam(example = "13007939", value = "Es el identificador del folio del incidente", required = true)
            @PathVariable("idIncidente") Integer idIncidente,
            @ApiParam(example = "1", value = "Es la opción de la aprobación a consultar", required = true)
            @RequestParam("opcion") Integer opcion) throws DataNotFoundException {
        DetalleAprobacionesResponseDTO respuesta = null;
        try {
            respuesta = serviciosRemedyLimpiezaDosBI.getInfoAprobaciones(idIncidente, opcion);
            if (respuesta == null) {
                throw new DataNotFoundException();
            }
            Boolean notFound = false;
            notFound |= (respuesta.getArchivoCierreUno() != null && respuesta.getArchivoCierreUno().isEmpty());
            if (Boolean.TRUE.equals(notFound)) {
                throw new DataNotFoundException();
            }
            return respuesta;
        } catch (Exception e) {
            throw new DataNotFoundException();

        }

    }

    //@RequestMapping(value = "/asignaProveedorLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Asignar Proveedor a Incidente",
            notes = "Realiza la asignación de un proveedor para atender un folio (incidente) "
            + "de limpieza devolviendo el estatus en que se encuentra el incidente.", nickname = "asignaProveedor")
    @RequestMapping(value = "proveedores/asignaciones",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public AsignaProveedorResponseDTO asignaProveedor(
            @ApiParam(name = "AsignaProveedor", value = "Asigna a un Proveedor", required = true)
            @RequestBody AsignaProveedorDTO asignaProveedorDTO) {
        AsignaProveedorResponseDTO res = null;
        try {
            ArrayOfInformacion auxIncidente
                    = serviciosRemedyLimpiezaDosBI.ConsultaDetalleFolio(asignaProveedorDTO.getIdIncidente());
            int resp = 0;
            if (auxIncidente != null) {
                Informacion[] arregloInc = auxIncidente.getInformacion();
                if (arregloInc != null) {
                    for (Informacion aux : arregloInc) {
                        if (aux.getEstado().trim().toLowerCase().equals("recibido por atender")
                                && (aux.getMotivoEstado().trim().toLowerCase().equals("por asignar")
                                || aux.getMotivoEstado().trim().toLowerCase().equals("rechazado proveedor"))) {
                            resp = 0;
                        } else {
                            resp = 1;
                        }
                    }
                }
            }
            boolean actIncidente = false;
            if (resp == 1) {
                resp = DOS;
            } else {
                actIncidente = serviciosRemedyLimpiezaDosBI.asignarProveedor(asignaProveedorDTO.getIdIncidente(),
                        asignaProveedorDTO.getProveedor());
            }
            if (resp != DOS) {
                if (actIncidente) {
                    resp = 1;
                } else {
                    resp = 0;
                }
            }
            res = new AsignaProveedorResponseDTO();
            res.setEstatus(resp);
        } catch (Exception e) {
            res = null;
            LOGGER.info(e);
        }
        return res;
    }

}
