/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.calificacion.disciplina.dao.util;

import com.gs.baz.frq.calificacion.disciplina.dto.CalificacionDisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @return @throws CustomException
     */
    public List<CalificacionDisciplinaDTO> selectRows() throws CustomException;

    /**
     *
     * @param idCeco
     * @param idProtocolo
     * @param modulo
     * @return
     * @throws CustomException
     */
    public List<CalificacionDisciplinaDTO> selectRowsCalificacionDisciplina(CalificacionDisciplinaDTO entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
