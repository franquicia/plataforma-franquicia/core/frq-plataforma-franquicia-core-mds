/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosDocumentoArchivo {

    @JsonProperty(value = "idArchivo")
    private Integer idProyectoDocumento;

    @JsonProperty(value = "idProyecto", required = true)
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1", required = true)
    private Integer idProyecto;

    @JsonProperty(value = "idDocumento", required = true)
    @ApiModelProperty(notes = "Identificador del documento", example = "1", required = true)
    private Integer idDocumento;

    public Integer getIdProyectoDocumento() {
        return idProyectoDocumento;
    }

    public void setIdProyectoDocumento(Integer idProyectoDocumento) {
        this.idProyectoDocumento = idProyectoDocumento;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }
}
