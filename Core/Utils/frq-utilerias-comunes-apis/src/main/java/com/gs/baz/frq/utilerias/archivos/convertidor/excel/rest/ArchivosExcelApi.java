/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.excel.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto.ParametrosPlantillaExcel;
import com.gs.baz.frq.utilerias.archivos.convertidor.excel.bi.WriteExcelUtil;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/api-local/utilerias/archivos")
public class ArchivosExcelApi {

    @Autowired
    WriteExcelUtil writeExcelUtil;
    private final Logger logger = LogManager.getLogger();

    private static final String APPLICATION_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private static final String APPLICATION_CSV = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private static final String APPLICATION_EXCEL_OLD = "application/vnd.ms-excel";

    @RequestMapping(value = "/convertir/json/excel/plantilla", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertirJsonExcelPlantilla(@RequestBody ParametrosPlantillaExcel parametrosPlantillaExcel) throws Exception {
        HttpHeaders outHeaders = new HttpHeaders();
        String nameFile = new Date().getTime() + ".xlsx";
        if (parametrosPlantillaExcel.getNombre() == null) {
            parametrosPlantillaExcel.setNombre(nameFile);
        } else {
            parametrosPlantillaExcel.setNombre(parametrosPlantillaExcel.getNombre() + ".xlsx");
        }
        outHeaders.set("Content-Disposition", "inline;filename=\"" + parametrosPlantillaExcel.getNombre() + "\"");
        outHeaders.set("file-name", parametrosPlantillaExcel.getNombre());
        Resource resource = writeExcelUtil.writeExcel(parametrosPlantillaExcel);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(resource);
    }

    @RequestMapping(value = "/convertir/json/excel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertirJsonExcelArreglo(@RequestBody JsonNode jsonNode) throws Exception {
        HttpHeaders outHeaders = new HttpHeaders();
        String nameFile = new Date().getTime() + ".xlsx";
        if (jsonNode == null) {
            throw new Exception("Object is null");
        }
        if (!jsonNode.isObject()) {
            throw new Exception("Object is required");
        }
        outHeaders.set("Content-Disposition", "inline;filename=\"" + nameFile + "\"");
        outHeaders.set("file-name", nameFile);
        Resource resource = writeExcelUtil.writeExcel(jsonNode);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(resource);
    }

    @RequestMapping(value = "/convertir/json/excel/arreglo/archivo", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertirJsonExcelArregloArchivo(@RequestPart("file") MultipartFile file) throws Exception {
        JsonNode jn = new ObjectMapper().readValue(file.getInputStream(), JsonNode.class);
        return this.convertirJsonExcelArreglo(jn);
    }
}
