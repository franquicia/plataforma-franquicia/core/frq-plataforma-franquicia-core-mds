/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos disciplina", value = "Disciplina")
public class Disciplina {

    @JsonProperty(value = "idDisciplina")
    @ApiModelProperty(notes = "Identificador de la disciplina", example = "18")
    private Integer idDisciplina;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la disciplia", example = "BAZ Cajeros Automáticos - Exterior")
    private String descripcion;

    @JsonProperty(value = "calificacion")
    @ApiModelProperty(notes = "Calificación de la disciplina", example = "100")
    private Integer calificacion;

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "Disciplina{" + "idDisciplina=" + idDisciplina + ", descripcion=" + descripcion + ", calificacion=" + calificacion + '}';
    }

}
