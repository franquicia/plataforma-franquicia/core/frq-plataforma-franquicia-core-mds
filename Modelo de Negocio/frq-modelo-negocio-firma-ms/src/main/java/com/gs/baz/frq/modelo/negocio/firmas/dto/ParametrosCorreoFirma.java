/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosCorreoFirma {

    @JsonProperty(value = "idFirma")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private transient Integer idFirma;

    @JsonProperty(value = "idFolio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private transient Integer idFolio;

    @JsonProperty(value = "idEstadoFolio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1", required = true)
    private Integer idEstadoFolio;

    public Integer getIdEstadoFolio() {
        return idEstadoFolio;
    }

    public void setIdEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1", required = true)
    private Integer numeroEmpleado;

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

}
