/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Verifica las zonas que existen por documentos", value = "ZonasPorDocumento")
public class ZonasPorDocumento {

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Indica el valor del ceco", example = "1")
    private Integer idCeco;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Indica el valor del formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Indicador del id de documento ", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Indicador del id de Proyecto ", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "idDatoZona")
    @ApiModelProperty(notes = "Identificador del dato de la zona", example = "1")
    private Integer idDatoZona;

    @JsonProperty(value = "idZona")
    @ApiModelProperty(notes = "Identificador  de la zona", example = "1")
    private Integer idZona;

    @JsonProperty(value = "valorMetros")
    @ApiModelProperty(notes = "Indica el valor de los metros de la zona", example = "100")
    private String valorMetros;

    @JsonProperty(value = "observaciones")
    @ApiModelProperty(notes = "Indica las observaciones de la zona", example = "EKT")
    private String observaciones;

    @JsonProperty(value = "idVersion")
    @ApiModelProperty(notes = "Identificador de la version de la zona", example = "1")
    private Integer idVersion;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Indica la descripcion de la zona", example = "Patio Bancario")
    private String descripcion;

    @JsonProperty(value = "idTipoInformacion")
    @ApiModelProperty(notes = "Identificador de idTipoInformacion segun el negocio", example = "1")
    private Integer idTipoInformacion;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Indicador del nombre de Documento ", example = "LAYOUT")
    private String nombreDocumento;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Indicador del nombre de Proyecto ", example = "Reconstrucciones")
    private String nombreProyecto;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdDatoZona() {
        return idDatoZona;
    }

    public void setIdDatoZona(Integer idDatoZona) {
        this.idDatoZona = idDatoZona;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public String getValorMetros() {
        return valorMetros;
    }

    public void setValorMetros(String valorMetros) {
        this.valorMetros = valorMetros;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdTipoInformacion() {
        return idTipoInformacion;
    }

    public void setIdTipoInformacion(Integer idTipoInformacion) {
        this.idTipoInformacion = idTipoInformacion;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

}
