/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ModeloNegocio {

    @JsonProperty(value = "idArbol")
    @ApiModelProperty(notes = "identificador del nivel", example = "1")
    private Integer idArbol;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del nivel", example = "Estrategia")
    private String descripcion;

    public Integer getIdArbol() {
        return idArbol;
    }

    public void setIdArbol(Integer idArbol) {
        this.idArbol = idArbol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ModeloNegocio{" + "idArbol=" + idArbol + ", descripcion=" + descripcion + '}';
    }

}
