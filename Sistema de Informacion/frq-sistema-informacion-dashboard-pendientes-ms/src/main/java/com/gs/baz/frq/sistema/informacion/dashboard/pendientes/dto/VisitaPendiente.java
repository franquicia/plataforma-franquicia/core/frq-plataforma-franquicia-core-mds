/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos Visita pendiente", value = "VisitaPendiente")
public class VisitaPendiente {

    @JsonProperty(value = "idBitacora")
    @ApiModelProperty(notes = "Identificador de la bitácora", example = "1", position = -1)
    private Integer idBitacora;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Número de empleado del usuario", example = "202622", position = -2)
    private Integer idUsuario;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costos.", example = "920100", position = -3)
    private String idCeco;
    
    @JsonProperty(value = "fechaVisita")
    @ApiModelProperty(notes = "Fecha de la Visita.", example = "08/11/2021", position = -4)
    private String fecha;
    
    @JsonProperty(value = "grupoClave")
    @ApiModelProperty(notes = "Clave del grupo al que pertenece.", example = "321", position = -5)
    private Integer ordenGrupo;
    
    @JsonProperty(value = "estatus")
    @ApiModelProperty(notes = "Estatus del envío.", example = "1", position = -6)
    private Integer estatus;

    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Integer idBitacora) {
        this.idBitacora = idBitacora;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getOrdenGrupo() {
        return ordenGrupo;
    }

    public void setOrdenGrupo(Integer ordenGrupo) {
        this.ordenGrupo = ordenGrupo;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return "VisitaPendiente{" + "idBitacora=" + idBitacora + ", idUsuario=" + idUsuario + ", idCeco=" + idCeco + ", fecha=" + fecha + ", ordenGrupo=" + ordenGrupo + ", estatus=" + estatus + '}';
    }

}
