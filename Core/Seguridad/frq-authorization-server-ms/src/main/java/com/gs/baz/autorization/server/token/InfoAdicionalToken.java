package com.gs.baz.autorization.server.token;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.usuario.dao.UsuarioDAOImpl;
import com.gs.baz.usuario.dto.UsuarioDTO;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    private UsuarioDAOImpl usuarioDAOImpl;

    private UsuarioDTO empleado;

    private Map<String, Object> infoAdicional;

    /**
     *
     * @param accessToken
     * @param authentication
     * @return
     */
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        try {
            empleado = usuarioDAOImpl.selectRow(new Integer(authentication.getName()));
            infoAdicional = new HashMap<>();
            infoAdicional.put("empleado", empleado.getIdUsuario());
            infoAdicional.put("nombre", empleado.getNombreUsuario());
            infoAdicional.put("ceco", empleado.getIdCeco());
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(infoAdicional);
        } catch (CustomException ex) {
            logger.error(ex);
        }
        return accessToken;
    }

}
