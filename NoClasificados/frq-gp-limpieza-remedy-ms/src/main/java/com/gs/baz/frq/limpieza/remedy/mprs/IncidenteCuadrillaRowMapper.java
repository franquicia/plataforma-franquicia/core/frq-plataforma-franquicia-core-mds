package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.IncidenteCuadrillaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class IncidenteCuadrillaRowMapper implements RowMapper<IncidenteCuadrillaDTO> {

    @Override
    public IncidenteCuadrillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        IncidenteCuadrillaDTO tk = new IncidenteCuadrillaDTO();

        tk.setIdIncidenteCuadrilla(rs.getInt("FIID_TKCUAD"));
        tk.setIdCuadrilla(rs.getInt("FIID_CUADRILLA"));
        tk.setIdProveedor(rs.getInt("FIID_PROVEEDOR"));
        tk.setIncidente(rs.getString("FIID_TICKET"));
        tk.setZona(rs.getInt("FIID_ZONA"));
        tk.setLiderCuadrilla(rs.getString("FCLIDER_CUAD"));
        tk.setStatus(rs.getInt("FCSTATUS"));

        return tk;
    }

}
