package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.Formato;

public class CecoFormatoRowMapper implements RowMapper<Formato> {

    @Override
    public Formato mapRow(ResultSet rs, int rowNum) throws SQLException {

        Formato formato = new Formato();

        formato.setIdFormato(rs.getInt("FIID_FORMATO"));
        formato.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        formato.setNombreFormato(rs.getString("FCDESCRIPCION"));
        formato.setNombreNegocio(rs.getString("NEGOCIO"));

        return formato;
    }

}
