/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosEstadoFirma {

    @JsonProperty(value = "idFirma")
    private transient Integer idFirma;

    @JsonProperty(value = "idFolio")
    private transient Integer idFolio;

    @JsonProperty(value = "idEstadoFolio")
    private transient Integer idEstadoFolio;

    public Integer getIdEstadoFolio() {
        return idEstadoFolio;
    }

    public void setIdEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1", required = true)
    @NotNull
    private Integer idEstadoFirma;

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    @Override
    public String toString() {
        return "ParametrosEstadoFirma{" + "idFirma=" + idFirma + ", idFolio=" + idFolio + ", idEstadoFirma=" + idEstadoFirma + '}';
    }

}
