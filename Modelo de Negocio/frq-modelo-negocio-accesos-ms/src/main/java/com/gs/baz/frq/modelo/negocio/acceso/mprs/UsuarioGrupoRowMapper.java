/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.mprs;

import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.UsuarioGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UsuarioGrupoRowMapper implements RowMapper<UsuarioGrupo> {

    private UsuarioGrupo usuarioGrupo;

    @Override
    public UsuarioGrupo mapRow(ResultSet rs, int rowNum) throws SQLException {
        usuarioGrupo = new UsuarioGrupo();
        usuarioGrupo.setIdGrupoUsuario(rs.getInt("FIIDGPOUSUA"));
        usuarioGrupo.setUsuario(new Usuario(rs.getInt("FIIDUSUARIO"), rs.getString("FCNOMBRE")));
        return usuarioGrupo;
    }
}
