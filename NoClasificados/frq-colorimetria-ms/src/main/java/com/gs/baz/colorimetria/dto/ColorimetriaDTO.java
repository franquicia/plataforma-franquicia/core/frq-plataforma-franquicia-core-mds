/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.colorimetria.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class ColorimetriaDTO {

    @JsonProperty(value = "id_colorimetria")
    private Integer idColorimetria;

    @JsonProperty(value = "tema_web")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String temaWeb;

    @JsonProperty(value = "color_primario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String colorPrim;

    @JsonProperty(value = "color_segundario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String colorSegund;

    @JsonProperty(value = "color_terciario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String colorTerc;

    @JsonProperty(value = "img_icono")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String icono;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public ColorimetriaDTO() {
    }

    public Integer getIdColorimetria() {
        return idColorimetria;
    }

    public void setIdColorimetria(BigDecimal idColorimetria) {
        this.idColorimetria = (idColorimetria == null ? null : idColorimetria.intValue());
    }

    public String getTemaWeb() {
        return temaWeb;
    }

    public void setTemaWeb(String temaWeb) {
        this.temaWeb = temaWeb;
    }

    public String getColorPrim() {
        return colorPrim;
    }

    public void setColorPrim(String colorPrim) {
        this.colorPrim = colorPrim;
    }

    public String getColorSegund() {
        return colorSegund;
    }

    public void setColorSegund(String colorSegund) {
        this.colorSegund = colorSegund;
    }

    public String getColorTerc() {
        return colorTerc;
    }

    public void setColorTerc(String colorTerc) {
        this.colorTerc = colorTerc;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "ColorimetriaDTO{" + "idColorimetria=" + idColorimetria + ", temaWeb=" + temaWeb + ", colorPrim=" + colorPrim + ", colorSegund=" + colorSegund + ", colorTerc=" + colorTerc + ", icono=" + icono + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
