package com.gs.baz.frq.accesos.sistema.usuarios.init;

import com.gs.baz.frq.accesos.sistema.usuarios.dao.AccesosUsuariosDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.accesos.sistema.usuarios")
public class ConfigAccesosUsuarios {

    private final Logger logger = LogManager.getLogger();

    public ConfigAccesosUsuarios() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public AccesosUsuariosDAOImpl accesosUsuariosDAOImpl() {
        return new AccesosUsuariosDAOImpl();
    }

}
