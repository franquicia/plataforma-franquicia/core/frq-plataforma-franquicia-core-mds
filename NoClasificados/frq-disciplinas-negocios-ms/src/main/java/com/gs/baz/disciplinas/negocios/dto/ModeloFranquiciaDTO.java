/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class ModeloFranquiciaDTO {

    @JsonProperty(value = "id_modelo")
    private Integer idModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_franquicia")
    private Integer idFranquicia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_modelo")
    private String nombreModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_creacion")
    private Date fechaCreacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_empleado_crea")
    private Integer idEmpleadoCrea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_modificacion")
    private Date fechaModificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_empleado_modifica")
    private Integer idEmpleadoModifica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_status")
    private Integer idStatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "empleado_crea")
    private Object empleadoCrea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "empleado_modifica")
    private Object empleadoModificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejes_modelo")
    private List<EjeModeloDTO> ejesModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocios_modelo")
    private List<NegocioDTO> negociosModelo;

    public ModeloFranquiciaDTO() {
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(BigDecimal idModelo) {
        this.idModelo = (idModelo == null ? null : idModelo.intValue());
    }

    public Integer getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(BigDecimal idFranquicia) {
        this.idFranquicia = (idFranquicia == null ? null : idFranquicia.intValue());
    }

    public String getNombreModelo() {
        return nombreModelo;
    }

    public void setNombreModelo(String nombreModelo) {
        this.nombreModelo = nombreModelo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    public Integer getIdEmpleadoCrea() {
        return idEmpleadoCrea;
    }

    public void setIdEmpleadoCrea(BigDecimal idEmpleadoCrea) {
        this.idEmpleadoCrea = (idEmpleadoCrea == null ? null : idEmpleadoCrea.intValue());
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getIdEmpleadoModifica() {
        return idEmpleadoModifica;
    }

    public void setIdEmpleadoModifica(BigDecimal idEmpleadoModifica) {
        this.idEmpleadoModifica = (idEmpleadoModifica == null ? null : idEmpleadoModifica.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public List<EjeModeloDTO> getEjesModelo() {
        return ejesModelo;
    }

    public void setEjesModelo(List<EjeModeloDTO> ejesModelo) {
        this.ejesModelo = ejesModelo;
    }

    public Object getEmpleadoCrea() {
        return empleadoCrea;
    }

    public void setEmpleadoCrea(Object empleadoCrea) {
        this.empleadoCrea = empleadoCrea;
    }

    public Object getEmpleadoModificacion() {
        return empleadoModificacion;
    }

    public void setEmpleadoModificacion(Object empleadoModificacion) {
        this.empleadoModificacion = empleadoModificacion;
    }

    public List<NegocioDTO> getNegociosModelo() {
        return negociosModelo;
    }

    public void setNegociosModelo(List<NegocioDTO> negociosModelo) {
        this.negociosModelo = negociosModelo;
    }

    @Override
    public String toString() {
        return "ModeloFranquiciaDTO{" + "idModelo=" + idModelo + ", idFranquicia=" + idFranquicia + ", nombreModelo=" + nombreModelo + ", fechaCreacion=" + fechaCreacion + ", idEmpleadoCrea=" + idEmpleadoCrea + ", fechaModificacion=" + fechaModificacion + ", idEmpleadoModifica=" + idEmpleadoModifica + ", idStatus=" + idStatus + ", empleadoCrea=" + empleadoCrea + ", empleadoModificacion=" + empleadoModificacion + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", ejesModelo=" + ejesModelo + ", negociosModelo=" + negociosModelo + '}';
    }

}
