/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la calificacion de geografia de red unica", value = "CalificacionGeografiaRedUnica")
public class CalificacionGeografiaRedUnica {

    @JsonProperty(value = "meta")
    @ApiModelProperty(notes = "Meta", example = "90")
    private Integer meta;

    @JsonProperty(value = "calificacionesGeografia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Calificaciones por geografia de red unica")
    private List<CalificacionGeografia> calificacionesGeografia;

    public Integer getMeta() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta = meta;
    }

    public List<CalificacionGeografia> getCalificacionesGeografia() {
        return calificacionesGeografia;
    }

    public void setCalificacionesGeografia(List<CalificacionGeografia> calificacionesGeografia) {
        this.calificacionesGeografia = calificacionesGeografia;
    }

    @Override
    public String toString() {
        return "CalificacionGeografiaRedUnica{" + "meta=" + meta + ", calificacionesGeografia=" + calificacionesGeografia + '}';
    }

}
