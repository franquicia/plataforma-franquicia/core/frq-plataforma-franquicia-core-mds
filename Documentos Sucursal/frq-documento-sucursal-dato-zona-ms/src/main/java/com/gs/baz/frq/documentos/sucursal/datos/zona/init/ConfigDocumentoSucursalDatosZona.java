package com.gs.baz.frq.documentos.sucursal.datos.zona.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dao.DocumentoSucursalDatosZonaDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documentos.sucursal.datos.zona")
public class ConfigDocumentoSucursalDatosZona {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalDatosZona() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalDatosZonaDAOImpl documentoSucursalDatosZonaDAOImpl() {
        return new DocumentoSucursalDatosZonaDAOImpl();
    }

}
