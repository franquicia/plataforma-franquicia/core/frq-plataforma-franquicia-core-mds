/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.rest;

import com.gs.baz.frq.jobs.scheduler.dto.Message;
import com.gs.baz.frq.jobs.scheduler.dto.ScheduleJob;
import com.gs.baz.frq.jobs.scheduler.quartz.service.ScheduleJobService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.SchedulerException;
import org.quartz.SchedulerMetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/jobs/scheduler")
public class ServiceJobsScheduler {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    private ScheduleJobService scheduleJobService;

    @RequestMapping(value = "secure/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object metaData() throws SchedulerException {
        SchedulerMetaData metaData = scheduleJobService.getMetaData();
        return metaData;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getAllJobs() throws SchedulerException {
        List<ScheduleJob> jobList = scheduleJobService.getAllJobList();
        return jobList;
    }

    @RequestMapping(value = "secure/get/running", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getRunningJobs() throws SchedulerException {
        List<ScheduleJob> jobList = scheduleJobService.getRunningJobList();
        return jobList;
    }

    @RequestMapping(value = "secure/put/pause", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object pauseJob(@RequestBody List<ScheduleJob> jobs) {
        logger.info("params, job = {}", jobs);
        Message message = Message.failure();
        try {
            scheduleJobService.pauseJob(jobs);
            message = Message.success();
        } catch (Exception e) {
            message.setMsg(e.getMessage());
            logger.error("pauseJob ex:", e);
        }
        return message;
    }

    @RequestMapping(value = "secure/put/resume", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object resumeJob(@RequestBody List<ScheduleJob> jobs) {
        logger.info("params, job = {}", jobs);
        Message message = Message.failure();
        try {
            scheduleJobService.resumeJob(jobs);
            message = Message.success();
        } catch (Exception e) {
            message.setMsg(e.getMessage());
            logger.error("resumeJob ex:", e);
        }
        return message;
    }

    @RequestMapping(value = "secure/put/run", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object runJob(@RequestBody List<ScheduleJob> jobs) {
        logger.info("params, job = {}", jobs);
        Message message = Message.failure();
        try {
            scheduleJobService.runJobOnce(jobs);
            message = Message.success();
        } catch (Exception e) {
            message.setMsg(e.getMessage());
            logger.error("runJob ex:", e);
        }
        return message;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object postJob(@RequestBody ScheduleJob job) {
        logger.info("params, job = {}", job);
        Message message = Message.failure();
        try {
            return scheduleJobService.addJob(job);
        } catch (Exception e) {
            message.setMsg(e.getMessage());
            logger.error("postJob ex:", e);
        }
        return message;
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object updateJob(@RequestBody ScheduleJob job) {
        logger.info("params, job = {}", job);
        Message message = Message.failure();
        try {
            return scheduleJobService.updateJob(job);
        } catch (Exception e) {
            message.setMsg(e.getMessage());
            logger.error("updateJob ex:", e);
        }
        return message;
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object deleteJob(@RequestBody List<ScheduleJob> jobs) {
        logger.info("params, job = {}", jobs);
        Message message = Message.failure();
        try {
            scheduleJobService.deleteJob(jobs);
            message = Message.success();
        } catch (Exception e) {
            message.setMsg(e.getMessage());
            logger.error("deleteJob ex:", e);
        }
        return message;
    }

}
