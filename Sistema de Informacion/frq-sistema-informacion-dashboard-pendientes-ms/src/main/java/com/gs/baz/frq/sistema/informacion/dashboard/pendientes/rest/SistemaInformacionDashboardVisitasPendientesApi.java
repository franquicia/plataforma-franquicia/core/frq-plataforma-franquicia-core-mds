/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao.SistemaInformacionVisitasPendientesDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao.SistemaInformacionVisitasPendientesDetalleDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CalificacionesCeco;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.DetalleVisitas;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.InformacionVisitas;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.RespuestasVisitas;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.SinResultado;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendienteParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitasPendientes;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "visitas pendientes", value = "visitas pendientes", description = "Api para la gestión de visitas pendientes")
@RestController
@RequestMapping("/api-local/sistema-informacion/visitas/pendientes/v1")
public class SistemaInformacionDashboardVisitasPendientesApi {

    @Autowired
    private SistemaInformacionVisitasPendientesDAOImpl sistemaInformacionVisitasPendientesDAOImpl;

    @Autowired
    private SistemaInformacionVisitasPendientesDetalleDAOImpl sistemaInformacionVisitasPendientesDetalleDAOImpl;
    
//estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idBitacora
     * @param visitaPendienteParametros
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grafica", notes = "Obtiene un grafica", nickname = "obtieneGrafica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/bitacoras/{idBitacora}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public VisitaPendiente obtieneGrafica(@ApiParam(name = "idGrafica", value = "Identificador de la bitácora", example = "44679279", required = true) @PathVariable("idBitacora") Integer idBitacora,
@ApiParam(name = "VisitaPendienteParametros", value = "Parámetros para la consulta de una visita pendiente", required = true) @RequestBody VisitaPendienteParametros visitaPendienteParametros ) throws CustomException, DataNotFoundException {
        visitaPendienteParametros.setIdBitacora(idBitacora);
        VisitaPendiente visitaPendiente = sistemaInformacionVisitasPendientesDAOImpl.selectRow(visitaPendienteParametros);
        if (visitaPendiente == null) {
            throw new DataNotFoundException();
        }
        return visitaPendiente;
    }

    

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public VisitasPendientes obtieneGraficas() throws CustomException, DataNotFoundException {
        VisitasPendientes visitasPendientes = new VisitasPendientes(sistemaInformacionVisitasPendientesDAOImpl.selectAllRows());
        if (visitasPendientes.getVisitaPendiente()!= null && visitasPendientes.getVisitaPendiente().isEmpty()) {
            throw new DataNotFoundException();
        }
        return visitasPendientes;
    }

    /**
     *
     * @param visitaPendiente
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grafica", notes = "Agrega un grafica", nickname = "creaGrafica")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public SinResultado creaGrafica(@ApiParam(name = "ParametrosGrafica", value = "Paramentros para el alta de la grafica", required = true) @RequestBody VisitaPendiente visitaPendiente) throws DataNotInsertedException {
         sistemaInformacionVisitasPendientesDAOImpl.insertRow(visitaPendiente);
        return new SinResultado();
    }

    /**
     *
     * @param visitaPendiente
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar grafica", notes = "Actualiza un grafica", nickname = "actualizaGrafica")
    @RequestMapping(value = "/", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaGrafica(@ApiParam(name = "ParametrosGrafica", value = "Paramentros para la actualización de la grafica", required = true) @RequestBody VisitaPendiente visitaPendiente) throws DataNotUpdatedException {
      //  visitaPendienteParametros.setIdGrafica(idGrafica);
        sistemaInformacionVisitasPendientesDAOImpl.updateRow(visitaPendiente);
        return new SinResultado();
    }

    /**
     *
     * 
     * @param idBitacora
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grafica", notes = "Elimina un item de los graficas", nickname = "eliminaGrafica")
    @RequestMapping(value = "/bitacoras/{idBitacora}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaGrafica(@ApiParam(name = "idGrafica", value = "Identificador de la bitácora", example = "44679279", required = true) @PathVariable("idBitacora") Integer idBitacora)  throws DataNotDeletedException {
        sistemaInformacionVisitasPendientesDAOImpl.deleteRow(idBitacora);
        return new SinResultado();
    }

    
    
    /**
     *
     * @param centroCostos
     * @param fecha
     * @param idProtocolo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/centro-costos/{centroCostos}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CalificacionesCeco obtieneCalificacionesCeco(@ApiParam(name = "centroCostos", value = "Centro de costos.", example = "922884") @PathVariable("centroCostos") String centroCostos,
            @ApiParam(name = "idProtocolo", value = "Clave del protocolo", example = "1") @RequestParam("idProtocolo") String idProtocolo,
            @ApiParam(name = "fecha", value = "Fecha", example = "14/11/2021") @RequestParam("fecha") String fecha) throws CustomException, DataNotFoundException {
       Integer protocolo;
      
        if(idProtocolo == null || idProtocolo.isEmpty()){
            protocolo = null;
            
       } else {
           protocolo = Integer.parseInt(idProtocolo);
           
        }
        CalificacionesCeco calificacionesCeco = new CalificacionesCeco(sistemaInformacionVisitasPendientesDetalleDAOImpl.selectCalificacionCeco(centroCostos, fecha, protocolo));
        if (calificacionesCeco.getCalificacionCeco()!= null && calificacionesCeco.getCalificacionCeco().isEmpty()) {
            throw new DataNotFoundException();
        }
        return calificacionesCeco;
    }
    
        /**
     *
     * @param centroCostos
     * @param fecha
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/centro-costos/{centroCostos}/informacion-general", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public InformacionVisitas obtieneInfoVisita(@ApiParam(name = "centroCostos", value = "Centro de costos.", example = "922884") @PathVariable("centroCostos") String centroCostos,         
            @ApiParam(name = "fecha", value = "Fecha", example = "14/11/2021") @RequestParam("fecha") String fecha) throws CustomException, DataNotFoundException {
       
        InformacionVisitas informacionVisitas = new InformacionVisitas(sistemaInformacionVisitasPendientesDetalleDAOImpl.selectInfoVisita(centroCostos, fecha));
        if (informacionVisitas.getInformacionVisita()!= null && informacionVisitas.getInformacionVisita().isEmpty()) {
            throw new DataNotFoundException();
        }
        return informacionVisitas;
    }
    
    
    /**
     *
     * @param centroCostos
     * @param fecha
     * @param idProtocolo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/centro-costos/{centroCostos}/detalles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DetalleVisitas obtieneDetalleVisita(@ApiParam(name = "centroCostos", value = "Centro de costos.", example = "922884") @PathVariable("centroCostos") String centroCostos,
            @ApiParam(name = "idProtocolo", value = "Clave del protocolo", example = "1") @RequestParam("idProtocolo") String idProtocolo,
            @ApiParam(name = "fecha", value = "Fecha", example = "14/11/2021") @RequestParam("fecha") String fecha) throws CustomException, DataNotFoundException {
       Integer protocolo;
       
        if(idProtocolo == null || idProtocolo.isEmpty()){
            protocolo = null;
       } else {
           protocolo = Integer.parseInt(idProtocolo);
        }
        DetalleVisitas detalleVisitas = new DetalleVisitas(sistemaInformacionVisitasPendientesDetalleDAOImpl.selectDetalleVisita(centroCostos, fecha, protocolo));
        if (detalleVisitas.getDetalleVisita()!= null && detalleVisitas.getDetalleVisita().isEmpty()) {
            throw new DataNotFoundException();
        }
        return detalleVisitas;
    }
    
    
       /**
     *
     * @param centroCostos
     * @param fecha
     * @param idProtocolo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene graficas", notes = "Obtiene todos los graficas", nickname = "obtieneGraficas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/centro-costos/{centroCostos}/respuestas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestasVisitas obtieneRespuestasVisita(@ApiParam(name = "centroCostos", value = "Centro de costos.", example = "922884") @PathVariable("centroCostos") String centroCostos,
            @ApiParam(name = "idProtocolo", value = "Clave del protocolo", example = "1") @RequestParam("idProtocolo") String idProtocolo,
            @ApiParam(name = "fecha", value = "Fecha", example = "14/11/2021") @RequestParam("fecha") String fecha) throws CustomException, DataNotFoundException {
       Integer protocolo;
        if(idProtocolo == null || idProtocolo.isEmpty()){
            protocolo = null;
       } else {
           protocolo = Integer.parseInt(idProtocolo);
        }
        RespuestasVisitas respuestasVisitas = new RespuestasVisitas(sistemaInformacionVisitasPendientesDetalleDAOImpl.selectRespuestasVisitas(centroCostos, fecha, protocolo));
        if (respuestasVisitas.getRespuestaVisita()!= null && respuestasVisitas.getRespuestaVisita().isEmpty()) {
            throw new DataNotFoundException();
        }
        return respuestasVisitas;
    }
}
