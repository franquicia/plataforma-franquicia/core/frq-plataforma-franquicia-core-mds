/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.pdf.bi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.jodconverter.core.DocumentConverter;
import org.jodconverter.core.document.DefaultDocumentFormatRegistry;
import org.jodconverter.core.document.DocumentFormat;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.office.OfficeManager;
import org.jodconverter.local.LocalConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class Convertidor {

    @Autowired
    @Qualifier("localOfficeManager")
    private OfficeManager officeManager;

    private static final Logger LOGGER = LoggerFactory.getLogger(Convertidor.class);

    private static final String FILTER_DATA = "FilterData";
    private static final String FILTER_DATA_PREFIX_PARAM = "fd";
    private static final String LOAD_PROPERTIES_PREFIX_PARAM = "l";
    private static final String LOAD_FILTER_DATA_PREFIX_PARAM = LOAD_PROPERTIES_PREFIX_PARAM + FILTER_DATA_PREFIX_PARAM;
    private static final String STORE_PROPERTIES_PREFIX_PARAM = "s";
    private static final String STORE_FILTER_DATA_PREFIX_PARAM = STORE_PROPERTIES_PREFIX_PARAM + FILTER_DATA_PREFIX_PARAM;

    private void addFilterDataProperty(final String paramName, final Map.Entry<String, String> param, final Map<String, Object> properties) {
        final String name = param.getKey().substring(paramName.length());
        final String value = param.getValue();
        if ("true".equalsIgnoreCase(value)) {
            properties.put(name, Boolean.TRUE);
        } else if ("false".equalsIgnoreCase(value)) {
            properties.put(name, Boolean.FALSE);
        } else {
            try {
                final int ival = Integer.parseInt(value);
                properties.put(name, ival);
            } catch (NumberFormatException nfe) {
                properties.put(name, value);
            }
        }
    }

    private void decodeParameters(final Map<String, String> parameters, final Map<String, Object> loadProperties, final Map<String, Object> storeProperties) {
        if (parameters == null || parameters.isEmpty()) {
            return;
        }
        final Map<String, Object> loadFilterDataProperties = new HashMap<>();
        final Map<String, Object> storeFilterDataProperties = new HashMap<>();
        parameters.entrySet().stream().forEach((param) -> {
            final String key = param.getKey().toLowerCase(Locale.ROOT);
            if (key.startsWith(LOAD_FILTER_DATA_PREFIX_PARAM)) {
                addFilterDataProperty(LOAD_FILTER_DATA_PREFIX_PARAM, param, loadFilterDataProperties);
            } else if (key.startsWith(LOAD_PROPERTIES_PREFIX_PARAM)) {
                addFilterDataProperty(LOAD_PROPERTIES_PREFIX_PARAM, param, loadProperties);
            } else if (key.startsWith(STORE_FILTER_DATA_PREFIX_PARAM)) {
                addFilterDataProperty(STORE_FILTER_DATA_PREFIX_PARAM, param, storeFilterDataProperties);
            } else if (key.startsWith(STORE_PROPERTIES_PREFIX_PARAM)) {
                addFilterDataProperty(STORE_PROPERTIES_PREFIX_PARAM, param, storeProperties);
            }
        });
        if (!loadFilterDataProperties.isEmpty()) {
            loadProperties.put(FILTER_DATA, loadFilterDataProperties);
        }
        if (!storeFilterDataProperties.isEmpty()) {
            storeProperties.put(FILTER_DATA, storeFilterDataProperties);
        }
    }

    public byte[] convertirdorPDF(InputStream inputStream, final String outputFormat, final Map<String, String> parameters) throws OfficeException, IOException {
        LOGGER.debug("convertUsingPathVariable > Converting file to {}", outputFormat);
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            final DocumentFormat targetFormat = DefaultDocumentFormatRegistry.getFormatByExtension(outputFormat);
            final Map<String, Object> loadProperties = new HashMap<>(LocalConverter.DEFAULT_LOAD_PROPERTIES);
            final Map<String, Object> storeProperties = new HashMap<>();
            this.decodeParameters(parameters, loadProperties, storeProperties);
            final DocumentConverter converter = LocalConverter.builder().officeManager(officeManager).loadProperties(loadProperties).storeProperties(storeProperties).build();
            converter.convert(inputStream).to(baos).as(targetFormat).execute();
            return baos.toByteArray();
        } catch (OfficeException | IOException ex) {
            throw ex;
        }
    }

}
