package com.gs.baz.punto.contacto.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.punto.contacto.dto.PuntoContactoCategoriaDTO;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoCategoriaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINPCCATE");
        jdbcInsert.withProcedureName("SP_INS_PCCATE");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINPCCATE");
        jdbcUpdate.withProcedureName("SP_ACT_PCCATE");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINPCCATE");
        jdbcDelete.withProcedureName("SP_DEL_PCCATE");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPCCATE");
        jdbcSelect.withProcedureName("SP_SEL_PCCATE");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoRowMapper().new PuntoContactoCategoriaRowMapper());
    }

    public PuntoContactoCategoriaDTO selectRow(Long idEntity) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPCCATE", idEntity);
        mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoCategoriaDTO> data = (List<PuntoContactoCategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<PuntoContactoCategoriaDTO> selectRows(Integer idPuntoContactoFranquiciaModelo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPCCATE", null);
        mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", idPuntoContactoFranquiciaModelo);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoCategoriaDTO> data = (List<PuntoContactoCategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return new ArrayList<>();
        }
    }

    public List<PuntoContactoCategoriaDTO> cdRows(Integer idPuntoContactoFranquiciaModelo, List<PuntoContactoCategoriaDTO> entitiesDTO) throws CustomException {
        List<PuntoContactoCategoriaDTO> puntosContactoCategoriasCurrent = this.selectRows(idPuntoContactoFranquiciaModelo);

        List<Integer> oldKeys = new ArrayList<>();
        puntosContactoCategoriasCurrent.forEach(item -> {
            oldKeys.add(item.getIdCategoria());
        });

        List<Integer> newKeys = new ArrayList<>();

        entitiesDTO.forEach(item -> {
            newKeys.add(item.getIdCategoria());
        });

        List<Integer> toDeletes = oldKeys.stream().filter(aObject
                -> !newKeys.contains(aObject)).collect(Collectors.toList());

        List<Integer> toInserts = newKeys.stream().filter(aObject
                -> !oldKeys.contains(aObject)).collect(Collectors.toList());

        for (Integer idCategoria : toDeletes) {
            PuntoContactoCategoriaDTO puntoContactoCategoriaDTO = puntosContactoCategoriasCurrent.stream().filter(item -> item.getIdCategoria().equals(idCategoria)).findFirst().orElse(null);
            this.deleteRow(puntoContactoCategoriaDTO);
        }

        for (Integer idCategoria : toInserts) {
            PuntoContactoCategoriaDTO puntoContactoCategoriaDTO = entitiesDTO.stream().filter(item -> item.getIdCategoria().equals(idCategoria)).findFirst().orElse(null);
            this.insertRow(puntoContactoCategoriaDTO);
        }
        return entitiesDTO;
    }

    private PuntoContactoCategoriaDTO insertRow(PuntoContactoCategoriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", entityDTO.getIdPuntoContactoFranquiciaModelo());
            mapSqlParameterSource.addValue("PA_FIIDCATEG", entityDTO.getIdCategoria());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdCategoriaPuntoContacto((BigDecimal) out.get("PA_FIIDPCCATE"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  PuntoContactoCategoria "));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  PuntoContactoCategoria "), ex);
        }
    }

    private PuntoContactoCategoriaDTO deleteRow(PuntoContactoCategoriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPCCATE", entityDTO.getIdCategoriaPuntoContacto());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error  not success to delete row of PuntoContactoCategoria"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error  to delete row of PuntoContactoCategoria"), ex);
        }
    }

}
