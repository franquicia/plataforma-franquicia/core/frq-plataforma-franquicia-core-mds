/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del catalogo Zona", value = "CatalogoZona")
public class CatalogoZonaBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del catalogo Zona", example = "Patio Bancario")
    private String descripcion;

    @JsonProperty(value = "estatus")
    @ApiModelProperty(notes = "Descripcion del estatus", example = "1")
    private Integer estatus;

    @JsonProperty(value = "idTipoInformacion")
    @ApiModelProperty(notes = "Descripcion del tipo de informacion si es banco (1) o elektra (2)", example = "1")
    private Integer idTipoInformacion;

    @JsonProperty(value = "periodo")
    @ApiModelProperty(notes = "Fecha que se dio de alta la zona", example = "2021-08-15")
    private String periodo;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Integer getIdTipoInformacion() {
        return idTipoInformacion;
    }

    public void setIdTipoInformacion(Integer idTipoInformacion) {
        this.idTipoInformacion = idTipoInformacion;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

}
