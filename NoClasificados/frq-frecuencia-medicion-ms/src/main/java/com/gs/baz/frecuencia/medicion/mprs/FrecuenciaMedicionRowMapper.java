package com.gs.baz.frecuencia.medicion.mprs;

import com.gs.baz.frecuencia.medicion.dto.FrecuenciaMedicionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FrecuenciaMedicionRowMapper implements RowMapper<FrecuenciaMedicionDTO> {

    private FrecuenciaMedicionDTO puesto;

    @Override
    public FrecuenciaMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        puesto = new FrecuenciaMedicionDTO();
        puesto.setIdFrecuenciaMedicion(((BigDecimal) rs.getObject("FIIDFRECMED")));
        puesto.setDescripcion(rs.getString("FCDESCRIPCION"));
        return puesto;
    }
}
