package com.gs.baz.frq.excel.mprs;

import com.gs.baz.frq.excel.dto.Pregunta7sDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class Pregunta7sRowMapper implements RowMapper<Pregunta7sDTO> {

    private Pregunta7sDTO status;

    @Override
    public Pregunta7sDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new Pregunta7sDTO();
        status.setIdPregunta((BigDecimal) rs.getObject("FIPREGUNTA_BASE"));
        status.setNumeroSerie((BigDecimal) rs.getObject("FINUMSERIE"));
        status.setCritica((BigDecimal) rs.getObject("FICRITICA"));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        status.setRegal_nueve((BigDecimal) rs.getObject("FIREGLANUEVE"));
        return status;
    }
}
