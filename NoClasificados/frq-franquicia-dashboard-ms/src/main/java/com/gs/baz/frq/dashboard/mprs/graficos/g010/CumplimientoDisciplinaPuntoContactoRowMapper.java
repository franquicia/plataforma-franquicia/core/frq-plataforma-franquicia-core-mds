package com.gs.baz.frq.dashboard.mprs.graficos.g010;

import com.gs.baz.frq.dashboard.dto.graficos.g010.CumplimientoDisciplinaPuntoContactoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoDisciplinaPuntoContactoRowMapper implements RowMapper<CumplimientoDisciplinaPuntoContactoDTO> {

    private CumplimientoDisciplinaPuntoContactoDTO cumplimientoDisciplinaPuntoContactoDTO;

    @Override
    public CumplimientoDisciplinaPuntoContactoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoDisciplinaPuntoContactoDTO = new CumplimientoDisciplinaPuntoContactoDTO();
        cumplimientoDisciplinaPuntoContactoDTO.setIdCeco(rs.getString("FCID_CECO"));
        cumplimientoDisciplinaPuntoContactoDTO.setCalificacion((BigDecimal) rs.getObject("FICALIFICACION"));
        cumplimientoDisciplinaPuntoContactoDTO.setIdNegocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        return cumplimientoDisciplinaPuntoContactoDTO;
    }
}
