package com.gs.baz.autorization.server.local.login;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.usuario.dao.UsuarioDAOImpl;
import com.gs.baz.usuario.dto.PerfilUsuarioDTO;
import com.gs.baz.usuario.dto.UsuarioDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UsuarioDAOImpl usuarioDAOImpl;

    /**
     *
     * @param idUsuario
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String idUsuario) throws UsernameNotFoundException {
        try {
            UsuarioDTO usuario = usuarioDAOImpl.selectRow(new Integer(idUsuario));
            if (usuario == null) {
                logger.error("Error en el login, no existe el usuario '" + idUsuario + "' en el sistema");
                throw new UsernameNotFoundException("Error en el login, no existe el usuario '" + idUsuario + "' en el sistema");
            }
            List<GrantedAuthority> authorities = new ArrayList<>();
            try {
                List<PerfilUsuarioDTO> perfilesPerfilUsuarioDTOs = usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectRowsByEmpleado(new Integer(idUsuario));
                usuario.setPerfilUsuarioDTOs(perfilesPerfilUsuarioDTOs);
                authorities = usuario.getPerfilUsuarioDTOs().stream().map((PerfilUsuarioDTO perfilUsuarioDTO) -> new SimpleGrantedAuthority(perfilUsuarioDTO.getPerfilDTO().getDescripcion())).peek(authority -> {
                    logger.info("Role: " + authority.getAuthority());
                }).collect(Collectors.toList());
            } catch (CustomException ex) {
                logger.warn(ex.getMessage(), ex);
            }
            boolean enabled = (usuario.getEstatusEmpleado() != null ? usuario.getEstatusEmpleado().equals(1) : false);
            logger.info("Usuario autenticado: " + idUsuario);
            return new User(usuario.getIdUsuario().toString(), passwordEncoder.encode(usuario.getIdUsuario().toString()), enabled, true, true, true, authorities);
        } catch (CustomException ex) {
            throw new UsernameNotFoundException("Error to login user " + idUsuario + " ", ex);
        }
    }

}
