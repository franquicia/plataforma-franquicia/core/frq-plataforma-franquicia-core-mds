package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.init;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao.SistemaInformacionVisitasPendientesDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao.SistemaInformacionVisitasPendientesDetalleDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao.SistemaInformacionVisitasPendientesGraficasDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.sistema.informacion.dashboard.pendientes")
public class ConfigDashboardPendientes {

    private final Logger logger = LogManager.getLogger();

    public ConfigDashboardPendientes() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SistemaInformacionVisitasPendientesDAOImpl sistemaInformacionVisitasPendientesDAOImpl() {
        return new SistemaInformacionVisitasPendientesDAOImpl();
    }

     @Bean(initMethod = "init")
    public SistemaInformacionVisitasPendientesGraficasDAOImpl sistemaInformacionVisitasPendientesGraficasDAOImpl() {
        return new SistemaInformacionVisitasPendientesGraficasDAOImpl();
    }
    
     @Bean(initMethod = "init")
    public SistemaInformacionVisitasPendientesDetalleDAOImpl sistemaInformacionVisitasPendientesDetalleDAOImpl() {
        return new SistemaInformacionVisitasPendientesDetalleDAOImpl();
    }
}
