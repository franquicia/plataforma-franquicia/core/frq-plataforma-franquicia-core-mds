package com.gs.baz.frq.asignacion.sup.init;

import com.gs.baz.frq.asignacion.sup.dao.AsignacionSupDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.asignacion.sup")
public class ConfigAsignacionSup {

    private final Logger logger = LogManager.getLogger();

    public ConfigAsignacionSup() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqAsignacionSupDAOImpl")
    public AsignacionSupDAOImpl asignacionSupDAOImpl() {
        return new AsignacionSupDAOImpl();
    }
}
