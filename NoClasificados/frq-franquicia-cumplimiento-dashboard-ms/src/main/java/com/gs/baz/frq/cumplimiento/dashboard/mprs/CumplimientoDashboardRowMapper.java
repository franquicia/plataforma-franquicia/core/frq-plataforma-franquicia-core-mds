package com.gs.baz.frq.cumplimiento.dashboard.mprs;

import com.gs.baz.frq.cumplimiento.dashboard.dto.CumplimientoDashboardDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoDashboardRowMapper implements RowMapper<CumplimientoDashboardDTO> {

    private CumplimientoDashboardDTO status;

    @Override
    public CumplimientoDashboardDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new CumplimientoDashboardDTO();
        status.setIdCeco(rs.getString("FCID_CECO"));
        status.setIdChecklist(((BigDecimal) rs.getObject("FIID_CHECKLIST")));
        status.setIdDashboard(((BigDecimal) rs.getObject("FIDASHBOARD_ID")));
        status.setIdPregunta(((BigDecimal) rs.getObject("FIID_PREGUNTA")));
        status.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        status.setModUsuario(rs.getString("FCUSUARIO_MOD"));
        status.setModFecha(rs.getString("FDFECHA_MOD"));
        return status;
    }
}
