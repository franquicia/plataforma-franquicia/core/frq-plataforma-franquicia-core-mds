package com.gs.baz.franquicia.mprs;

import com.gs.baz.franquicia.dto.FranquiciaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FranquiciaRowMapper implements RowMapper<FranquiciaDTO> {

    private FranquiciaDTO franquicia;

    @Override
    public FranquiciaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        franquicia = new FranquiciaDTO();
        franquicia.setIdFranquicia((BigDecimal) rs.getObject("FIIDFRANQ"));
        franquicia.setDescripcion(rs.getString("FCDESCRIPCION"));
        franquicia.setIdStatus((BigDecimal) rs.getObject("FIIDSTATUS"));
        franquicia.setFechaCreacion(rs.getDate("FDFCHACREACION"));
        franquicia.setIdEmpleadoCrea((BigDecimal) rs.getObject("FIIDEMCREA"));
        franquicia.setFechaModificacion(rs.getDate("FDFMODIFIC"));
        franquicia.setIdEmpleadoModifica((BigDecimal) rs.getObject("FIIDEMMOD"));
        return franquicia;
    }
}
