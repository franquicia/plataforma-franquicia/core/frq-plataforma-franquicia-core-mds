/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.ApiModel;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta sin resultado", value = "SinResultado")
public class SinResultado extends RespuestaLessResult200 {

}
