/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.perfiles.rest;

import com.gs.baz.frq.accesos.sistema.perfiles.dao.AccesosPerfilesDAOImpl;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.AltaPerfil;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.AltaPerfilUsuario;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.AltaPerfilVistas;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.ParametrosPerfil;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.ParametrosPerfilVistas;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.PerfilBase;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.Perfiles;
import com.gs.baz.frq.accesos.sistema.perfiles.dto.SinResultado;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "accesos", value = "accesos", description = "Api para la gestión de accesos")
@RestController
@RequestMapping("/api-local/accesos/sistema/perfiles/v1")
public class AccesosPerfilesApi {

    @Autowired
    private AccesosPerfilesDAOImpl accesosPerfilesDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idPerfil
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfil", notes = "Obtiene un perfil", nickname = "obtienePerfil")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idPerfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilBase obtienePerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Long idPerfil) throws DataNotFoundException {
        PerfilBase perfilBase = accesosPerfilesDAOImpl.selectRow(idPerfil);
        if (perfilBase == null) {
            throw new DataNotFoundException();
        }
        return perfilBase;
    }

    /**
     *
     * @return * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfiles", notes = "Obtiene todos los perfiles", nickname = "obtienePerfiles")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Perfiles obtienePerfiles() throws DataNotFoundException {
        Perfiles perfiles = new Perfiles(accesosPerfilesDAOImpl.selectAllRows());
        if (perfiles.getPerfiles() != null && perfiles.getPerfiles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return perfiles;
    }

    /**
     *
     * @param numeroEmpleado
     * @return * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfiles", notes = "Obtiene todos los perfiles", nickname = "obtienePerfilesUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Perfiles obtienePerfilesUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotFoundException {
        Perfiles perfiles = new Perfiles(accesosPerfilesDAOImpl.selectAllRowsPerfilesUsuario(numeroEmpleado));
        if (perfiles.getPerfiles() != null && perfiles.getPerfiles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return perfiles;
    }

    /**
     *
     * @param numeroEmpleado
     * @return * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfiles", notes = "Obtiene todos los perfiles", nickname = "obtienePerfilesFijosUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}/fijos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Perfiles obtienePerfilesFijosUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotFoundException {
        Perfiles perfiles = new Perfiles(accesosPerfilesDAOImpl.selectAllRowsPerfilesFijosUsuario(numeroEmpleado));
        if (perfiles.getPerfiles() != null && perfiles.getPerfiles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return perfiles;
    }

    /**
     *
     * @param numeroEmpleado
     * @return * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfiles", notes = "Obtiene todos los perfiles", nickname = "obtienePerfilesDisponiblesUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}/disponibles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Perfiles obtienePerfilesDisponiblesUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotFoundException {
        Perfiles perfiles = new Perfiles(accesosPerfilesDAOImpl.selectAllRowsPerfilesDisponiblesUsuario(numeroEmpleado));
        if (perfiles.getPerfiles() != null && perfiles.getPerfiles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return perfiles;
    }

    /**
     *
     * @param numeroEmpleado
     * @return * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfiles", notes = "Obtiene todos los perfiles", nickname = "obtienePerfilesFinalesUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}/finales", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Perfiles obtienePerfilesFinalesUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotFoundException {
        Perfiles perfiles = new Perfiles(accesosPerfilesDAOImpl.selectAllRowsPerfilesFinalesUsuario(numeroEmpleado));
        if (perfiles.getPerfiles() != null && perfiles.getPerfiles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return perfiles;
    }

    /**
     *
     * @param idVista
     * @return * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene perfiles", notes = "Obtiene todos los perfiles", nickname = "obtienePerfilesVista")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/vistas/{idVista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Perfiles obtienePerfilesVista(@ApiParam(name = "idVista", value = "Identificador del la vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotFoundException {
        Perfiles perfiles = new Perfiles(accesosPerfilesDAOImpl.selectAllRowsPerfilesVista(idVista));
        if (perfiles.getPerfiles() != null && perfiles.getPerfiles().isEmpty()) {
            throw new DataNotFoundException();
        }
        return perfiles;
    }

    /**
     *
     * @param parametrosPerfil
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear perfil", notes = "Agrega un perfil", nickname = "creaPerfil")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaPerfil creaPerfil(@ApiParam(name = "ParametrosPerfil", value = "Paramentros para el alta del perfil", required = true) @RequestBody ParametrosPerfil parametrosPerfil) throws DataNotInsertedException {
        return accesosPerfilesDAOImpl.insertRow(parametrosPerfil);
    }

    /**
     *
     * @param parametrosPerfil
     * @param idPerfil
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar perfil", notes = "Actualiza un perfil", nickname = "actualizaPerfil")
    @RequestMapping(value = "/{idPerfil}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaPerfil(@ApiParam(name = "ParametrosPerfil", value = "Paramentros para la actualización del perfil", required = true) @RequestBody ParametrosPerfil parametrosPerfil, @ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil) throws DataNotUpdatedException {
        parametrosPerfil.setIdPerfil(idPerfil);
        accesosPerfilesDAOImpl.updateRow(parametrosPerfil);
        return new SinResultado();
    }

    /**
     *
     * @param idPerfil
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar perfil", notes = "Elimina un item de los perfiles", nickname = "eliminaPerfil")
    @RequestMapping(value = "/{idPerfil}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil) throws DataNotDeletedException {
        accesosPerfilesDAOImpl.deleteRow(idPerfil);
        return new SinResultado();
    }

    /**
     *
     * @param idPerfil
     * @param numeroEmpleado
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear perfil usuario", notes = "Agrega un perfil al usuario", nickname = "creaUsuarioPerfil")
    @RequestMapping(value = "/{idPerfil}/usuarios/{numeroEmpleado}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaPerfilUsuario creaUsuarioPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil, @ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotInsertedException {
        return accesosPerfilesDAOImpl.insertRowPerfilEmpleado(idPerfil, numeroEmpleado);
    }

    /**
     *
     * @param idPerfil
     * @param numeroEmpleado
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar perfil usuario", notes = "Elimina un perfil al usuario", nickname = "eliminaUsuarioPerfil")
    @RequestMapping(value = "/{idPerfil}/usuarios/{numeroEmpleado}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaUsuarioPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil, @ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotDeletedException {
        accesosPerfilesDAOImpl.deleteRowPerfilEmpleado(idPerfil, numeroEmpleado);
        return new SinResultado();
    }

    /**
     *
     * @param idPerfil
     * @param parametrosPerfilVistas
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear vistas perfil", notes = "Agrega vistas al perfil", nickname = "creaVistasPerfil")
    @RequestMapping(value = "/{idPerfil}/vistas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaPerfilVistas creaVistasPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil, @ApiParam(name = "ParametrosPerfilVistas", value = "Paramentros para el alta de las vistas", required = true) @RequestBody ParametrosPerfilVistas parametrosPerfilVistas) throws DataNotInsertedException {
        parametrosPerfilVistas.setIdPerfil(idPerfil);
        return accesosPerfilesDAOImpl.insertRowPerfilVistas(parametrosPerfilVistas);
    }

    /**
     *
     * @param idPerfil
     * @param idVista
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Borra vista perfil", notes = "Borra vista al perfil", nickname = "borraVistaPerfil")
    @RequestMapping(value = "/{idPerfil}/vistas/{idVista}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado borraVistaUsuario(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil, @ApiParam(name = "idVista", value = "Identificador del la vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotDeletedException {
        accesosPerfilesDAOImpl.deleteRowPerfilVista(idPerfil, idVista);
        return new SinResultado();
    }

}
