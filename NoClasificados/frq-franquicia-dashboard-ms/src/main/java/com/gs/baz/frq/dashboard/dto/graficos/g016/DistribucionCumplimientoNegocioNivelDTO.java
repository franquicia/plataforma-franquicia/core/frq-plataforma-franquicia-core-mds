/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g016;

import com.gs.baz.frq.dashboard.dto.graficos.g007.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author B73601
 */
public class DistribucionCumplimientoNegocioNivelDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cecos")
    private List<CecoDTO> cecos;

    public List<CecoDTO> getCecos() {
        return cecos;
    }

    public void setCecos(List<CecoDTO> cecos) {
        this.cecos = cecos;
    }

    @Override
    public String toString() {
        return "DistribucionCumplimientoDTO{" + "cecos=" + cecos + '}';
    }

}
