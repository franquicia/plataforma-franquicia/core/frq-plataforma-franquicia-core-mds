package com.gs.baz.frq.asignacion.sup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ArchivoAsignacionesDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "archivoAsignaciones")
    private String archivoAsignaciones;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "eco")
    private Integer numeroEconomico;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreSucursal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "acronimo")
    private String tipoVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private Integer numeroEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreCertificador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numeroGerente")
    private Integer numeroGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreGerente")
    private String nombreGerente;

    public String getArchivoAsignaciones() {
        return archivoAsignaciones;
    }

    public void setArchivoAsignaciones(String archivoAsignaciones) {
        this.archivoAsignaciones = archivoAsignaciones;
    }

    public Integer getNumeroEconomico() {
        return numeroEconomico;
    }

    public void setNumeroEconomico(Integer numeroEconomico) {
        this.numeroEconomico = numeroEconomico;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getTipoVisita() {
        return tipoVisita;
    }

    public void setTipoVisita(String tipoVisita) {
        this.tipoVisita = tipoVisita;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombreCertificador() {
        return nombreCertificador;
    }

    public void setNombreCertificador(String nombreCertificador) {
        this.nombreCertificador = nombreCertificador;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getNumeroGerente() {
        return numeroGerente;
    }

    public void setNumeroGerente(Integer numeroGerente) {
        this.numeroGerente = numeroGerente;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    @Override
    public String toString() {
        return "ArchivoAsignacionesDTO{" + "archivoAsignaciones=" + archivoAsignaciones + ", numeroEconomico=" + numeroEconomico + ", nombreSucursal=" + nombreSucursal + ", tipoVisita=" + tipoVisita + ", numeroEmpleado=" + numeroEmpleado + ", nombreCertificador=" + nombreCertificador + ", zona=" + zona + ", territorio=" + territorio + ", numeroGerente=" + numeroGerente + ", nombreGerente=" + nombreGerente + '}';
    }

}
