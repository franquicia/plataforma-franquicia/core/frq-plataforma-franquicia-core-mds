package com.gs.baz.frq.modelo.negocio.documentos.init;

import com.gs.baz.frq.modelo.negocio.documentos.dao.DocumentoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.documentos")
public class ConfigDocumento {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumento() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoDAOImpl documentoDAOImpl() {
        return new DocumentoDAOImpl();
    }

}
