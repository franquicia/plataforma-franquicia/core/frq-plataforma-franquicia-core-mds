/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.bi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

/**
 *
 * @author cescobarh
 */
public class ServletInputStreamRequestWrapper extends ServletInputStream {

    private final ByteArrayInputStream buffer;

    public ServletInputStreamRequestWrapper(byte[] contents) {
        this.buffer = new ByteArrayInputStream(contents);
    }

    @Override
    public boolean isFinished() {
        return buffer.available() == 0;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void setReadListener(ReadListener readListener) {
    }

    @Override
    public int read() throws IOException {
        return buffer.read();
    }

}
