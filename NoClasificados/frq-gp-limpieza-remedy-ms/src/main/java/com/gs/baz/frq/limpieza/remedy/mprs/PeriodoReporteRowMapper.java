package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.PeriodoReporteDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PeriodoReporteRowMapper implements RowMapper<PeriodoReporteDTO> {

    public PeriodoReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PeriodoReporteDTO peridoReporteDTO = new PeriodoReporteDTO();

        peridoReporteDTO.setIdPeriodoReporte(rs.getInt("FIID_PER_REP"));
        peridoReporteDTO.setIdReporte(rs.getInt("FIID_REPORTE"));
        peridoReporteDTO.setIdPeriodo(rs.getInt("FIID_PERIODO"));
        peridoReporteDTO.setHorario(rs.getString("FCHORARIO"));

        return peridoReporteDTO;

    }
}
