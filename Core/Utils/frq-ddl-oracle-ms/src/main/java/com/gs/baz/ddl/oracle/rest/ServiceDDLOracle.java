/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ddl.oracle.rest;

import com.gs.baz.ddl.oracle.process.UtilZip;
import java.io.IOException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/ddl/oracle")
public class ServiceDDLOracle {

    @Autowired
    UtilZip utilZip;

    @Autowired
    @Qualifier("frqDS")
    private DataSource frqDS;

    @Autowired
    @Qualifier("gtnDS")
    private DataSource gtnDS;

    @Autowired
    @Qualifier("mdfqrDS")
    private DataSource mdfqrDS;

    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity test() {
        System.out.println("holaa");
        return ResponseEntity.status(HttpStatus.OK).body("hola");
    }

    @RequestMapping(value = "/packages/definition", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity packagesDefinition() {
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/packages/body", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity packagesBody() {
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/package/definition", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity packageDefinition() {
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/package/body", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity packageBody() {
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/package", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity packageAll() {
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/franquicia/packages", method = RequestMethod.GET, produces = "application/zip")
    @ResponseBody
    public ResponseEntity packagesFranquicia() throws IOException {
        String nameFile = "franquicia";
        byte[] fileZip = utilZip.packages(frqDS, nameFile, null);
        nameFile += ".zip";
        HttpHeaders outHeaders = new HttpHeaders();
        outHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + nameFile);
        outHeaders.set("file-name", nameFile);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(fileZip);
    }

    @RequestMapping(value = "/gestion/packages", method = RequestMethod.GET, produces = "application/zip")
    @ResponseBody
    public ResponseEntity packagesGestion() throws IOException {
        String nameFile = "gestion";
        byte[] fileZip = utilZip.packages(gtnDS, nameFile, null);
        nameFile += ".zip";
        HttpHeaders outHeaders = new HttpHeaders();
        outHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + nameFile);
        outHeaders.set("file-name", nameFile);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(fileZip);
    }

    @RequestMapping(value = "/modfranq/packages", method = RequestMethod.GET, produces = "application/zip")
    @ResponseBody
    public ResponseEntity packagesModelo() throws IOException {
        String nameFile = "modfranq";
        byte[] fileZip = utilZip.packages(mdfqrDS, nameFile, null);
        nameFile += ".zip";
        HttpHeaders outHeaders = new HttpHeaders();
        outHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + nameFile);
        outHeaders.set("file-name", nameFile);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(fileZip);
    }

}
