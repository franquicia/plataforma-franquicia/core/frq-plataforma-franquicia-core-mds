package com.gs.baz.reporte.medicion.dto.mprs;

import com.gs.baz.reporte.medicion.dto.ProtocoloDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDisciplinaRowMapper implements RowMapper<ProtocoloDisciplinaDTO> {

    private ProtocoloDisciplinaDTO protocoloDisciplinaDTO;

    @Override
    public ProtocoloDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        protocoloDisciplinaDTO = new ProtocoloDisciplinaDTO();
        protocoloDisciplinaDTO.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        protocoloDisciplinaDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        return protocoloDisciplinaDTO;
    }
}
