/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.bi;

/**
 *
 * @author cescobarh
 */
public class LoginToken {

    private String username;

    public LoginToken() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "LoginToken{" + "username=" + username + '}';
    }

}
