/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param idModelo
     * @param idEjeModelo
     * @param idNegocio
     * @return
     * @throws CustomException
     */
    public List<dto> selectRow(Long idModelo, Long idEjeModelo, Long idNegocio) throws CustomException;

}
