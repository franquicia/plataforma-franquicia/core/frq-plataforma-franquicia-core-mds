package com.gs.baz.frq.dashboard.certificacion.mprs.reportes;

import com.gs.baz.frq.dashboard.certificacion.dto.reportes.DashboardCertificacionReporteCoberturaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionReporteCoberturaRowMapper implements RowMapper<DashboardCertificacionReporteCoberturaDTO> {

    private DashboardCertificacionReporteCoberturaDTO dashboardCertificacionReporteCoberturaDTO;

    @Override
    public DashboardCertificacionReporteCoberturaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionReporteCoberturaDTO = new DashboardCertificacionReporteCoberturaDTO();
        dashboardCertificacionReporteCoberturaDTO.setFechaRealizacion(rs.getString("FECHA QUE SE REALIZO"));
        dashboardCertificacionReporteCoberturaDTO.setNombre(rs.getString("QUIEN LO REALIZO").toUpperCase());
        dashboardCertificacionReporteCoberturaDTO.setIdUsuario((BigDecimal) rs.getObject("NUMERO DE USUARIO"));
        dashboardCertificacionReporteCoberturaDTO.setPuesto(rs.getString("PUESTO"));
        dashboardCertificacionReporteCoberturaDTO.setPais(rs.getString("PAÍS"));
        dashboardCertificacionReporteCoberturaDTO.setTerritorio(rs.getString("TERRITORIO"));
        dashboardCertificacionReporteCoberturaDTO.setZona(rs.getString("ZONA"));
        dashboardCertificacionReporteCoberturaDTO.setRegional(rs.getString("REGIONAL"));
        dashboardCertificacionReporteCoberturaDTO.setSucursal(rs.getString("SUCURSAL"));
        dashboardCertificacionReporteCoberturaDTO.setNombreSucursal(rs.getString("NOMBRE SUCURSAL"));
        dashboardCertificacionReporteCoberturaDTO.setCanal(rs.getString("CANAL"));
        return dashboardCertificacionReporteCoberturaDTO;
    }
}
