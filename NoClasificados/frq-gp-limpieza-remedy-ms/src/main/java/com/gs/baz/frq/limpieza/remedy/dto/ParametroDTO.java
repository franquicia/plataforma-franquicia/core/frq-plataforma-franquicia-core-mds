package com.gs.baz.frq.limpieza.remedy.dto;

public class ParametroDTO {

    private String cl;
    private String valor;
    private Integer activo;

    public ParametroDTO() {
        super();

    }

    public String getCl() {
        return cl;
    }

    public void setCl(String cl) {
        this.cl = cl;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

}
