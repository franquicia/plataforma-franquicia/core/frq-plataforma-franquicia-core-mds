package com.gs.baz.frq.modulo.mprs;

import com.gs.baz.frq.modulo.dto.ModuloDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ModuloRowMapper implements RowMapper<ModuloDTO> {

    private ModuloDTO moduloFlujoDTO;

    @Override
    public ModuloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        moduloFlujoDTO = new ModuloDTO();
        moduloFlujoDTO.setIdModulo(((BigDecimal) rs.getObject("FIID_MODULO")));
        moduloFlujoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        moduloFlujoDTO.setModulo(rs.getString("FCMODULO"));
        moduloFlujoDTO.setEsWeb((BigDecimal) rs.getObject("FIESWEB"));
        return moduloFlujoDTO;
    }
}
