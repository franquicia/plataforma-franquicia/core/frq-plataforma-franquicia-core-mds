/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.file.dao;

import com.gs.baz.file.dao.util.GenericDAO;
import com.gs.baz.file.dto.FileGenericDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.PathsResources;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;

/**
 *
 * @author MADA
 */
@Component
public class FileDAOImpl implements GenericDAO<FileGenericDTO> {

    @Override
    public FileGenericDTO readFileB64(String fullPathFile) throws CustomException {
        FileGenericDTO fileGenericDTO = new FileGenericDTO();
        try {
            Path path = Paths.get(fullPathFile);
            if (Files.exists(path)) {
                byte[] file = Files.readAllBytes(path);
                String fileBase64 = new String(Base64.getEncoder().encode(file), "UTF-8");
                int beginIndex = fullPathFile.indexOf("__") + 2;
                int endIndex = fullPathFile.length();
                String nameFile = fullPathFile.substring(beginIndex, endIndex).toLowerCase();
                if (beginIndex == 1) {
                    nameFile = path.getFileName().toString();
                }
                String contentType = Paths.get(fullPathFile).toUri().toURL().openConnection().getContentType();
                fileGenericDTO.setContentB64(fileBase64);
                fileGenericDTO.setNameFile(nameFile);
                fileGenericDTO.setContentType(contentType);
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_READ_FILE.detalle("Error to select " + fullPathFile + " of Files"));
            }
        } catch (IOException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_READ_FILE.detalle("Exception to select " + fileGenericDTO.getPath() + " of Files"), ex);
        }
        return fileGenericDTO;
    }

    @Override
    public Path readFile(String fullPathFile) throws CustomException {
        Path path = Paths.get(fullPathFile);
        if (Files.exists(path)) {
            return path;
        } else {
            throw new CustomException(ModelCodes.ERROR_TO_READ_FILE.detalle("Error to select " + fullPathFile + " of Files"));
        }
    }

    @Override
    public FileGenericDTO writeFile(FileGenericDTO entityDTO) throws CustomException {
        String rootPath = File.listRoots()[0].getAbsolutePath();
        String absolutePath = rootPath + entityDTO.getPath();
        Path path = Paths.get(absolutePath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                throw new CustomException(ex);
            }
        }
        byte[] fileDecoded = Base64.getDecoder().decode(entityDTO.getContentB64());
        String fullPath = absolutePath + entityDTO.getNameFile();
        try (FileOutputStream fileOutput = new FileOutputStream(fullPath)) {
            try (BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput)) {
                bufferOutput.write(fileDecoded);
            }
            entityDTO.setWrited(true);
        } catch (IOException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("Error to insert " + entityDTO.getPath() + " on Files"), ex);
        }
        return entityDTO;
    }

    @Override
    public FileGenericDTO writeDirectory(FileGenericDTO fileGenericDTO) throws CustomException {
        String rootPath = PathsResources.DEFAULT.getRootPath();
        String absolutePath = rootPath + fileGenericDTO.getPath();
        Path path = Paths.get(absolutePath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
                fileGenericDTO.setWrited(true);
            } catch (IOException ex) {
                throw new CustomException(ex);
            }
        }
        return fileGenericDTO;
    }

    @Override
    public List<FileGenericDTO> readDirectory(FileGenericDTO fileGenericDTO) throws CustomException {
        List<FileGenericDTO> files = new ArrayList<>();
        String rootPath = PathsResources.DEFAULT.getRootPath();
        String absolutePath = rootPath + fileGenericDTO.getPath();
        Integer level = fileGenericDTO.getLevel() != null ? fileGenericDTO.getLevel() : Integer.MAX_VALUE;
        try (Stream<Path> paths = Files.walk(Paths.get(absolutePath), level)) {
            paths.forEach(item -> {
                boolean isFile = item.toFile().isFile();
                FileGenericDTO fileGeneric = new FileGenericDTO();
                fileGeneric.setPath(item.normalize().toString());
                fileGeneric.setNameFile(item.getFileName().toString());
                fileGeneric.setIsFile(isFile);
                fileGeneric.setParent(item.getParent().normalize().toString());
                if (isFile) {
                    String keyPath = Base64.getEncoder().encodeToString(fileGeneric.getPath().getBytes());
                    fileGeneric.setKeyPath("/service/file/secure/read/" + keyPath);
                    fileGeneric.setKeyB64Path("/service/file/secure/read/b64/" + keyPath);
                }
                files.add(fileGeneric);
            });
        } catch (IOException ex) {
            throw new CustomException(ex);
        }
        return files;
    }

}
