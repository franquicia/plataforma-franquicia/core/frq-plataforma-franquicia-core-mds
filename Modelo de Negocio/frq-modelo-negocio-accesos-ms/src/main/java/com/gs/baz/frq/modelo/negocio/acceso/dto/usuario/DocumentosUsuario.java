/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de documentos del usuario", value = "DocumentosUsuario")
public class DocumentosUsuario {

    @JsonProperty(value = "documentos")
    @ApiModelProperty(notes = "documentos")
    private List<DocumentoUsuario> documentoUsuarios;

    public DocumentosUsuario(List<DocumentoUsuario> documentoUsuarios) {
        this.documentoUsuarios = documentoUsuarios;
    }

    public List<DocumentoUsuario> getDocumentos() {
        return documentoUsuarios;
    }

    public void setDocumentos(List<DocumentoUsuario> documentoUsuarios) {
        this.documentoUsuarios = documentoUsuarios;
    }

    @Override
    public String toString() {
        return "DocumentosUsuario{" + "documentoUsuarios=" + documentoUsuarios + '}';
    }

}
