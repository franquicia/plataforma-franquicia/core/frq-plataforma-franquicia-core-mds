/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.appender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.JsonLayout;

/**
 *
 * @author karlo
 */
@Plugin(name = "WebSocket", category = "Core", elementType = "appender", printObject = true)
public final class WebSocketAppender extends AbstractAppender implements ApplicationListener {

    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private ObjectNode objectNode;

    protected WebSocketAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
        new ApplicationThrowEvent(this).waintingForLoadComplete();
    }

    @Override
    public void append(LogEvent event) {
        if (ClientSocketLogs.session != null && ClientSocketLogs.session.isConnected()) {
            readLock.lock();
            try {
                final byte[] bytes = getLayout().toByteArray(event);
                String log = new String(bytes);
                objectNode = objectMapper.readValue(log, ObjectNode.class);
                if (!objectNode.toString().contains("/websocket") && !objectNode.toString().contains("com.gs.baz.track.time")) {
                    if (ClientSocketLogs.session != null && ClientSocketLogs.session.isConnected()) {
                        ClientSocketLogs.session.send("/app/logs", objectNode);
                    }
                }
            } catch (JsonProcessingException ex) {
                System.err.println(ex);
            } finally {
                readLock.unlock();
            }
        }
    }

    /**
     *
     * @param name
     * @param jsonLayout
     * @param filter
     * @return
     */
    @PluginFactory
    public static WebSocketAppender createAppender(@PluginAttribute("Name") String name, @PluginElement("Layout") Layout<? extends Serializable> jsonLayout, @PluginElement("Filters") final Filter filter) {
        if (name == null) {
            System.err.println("No name provided for WebSocketAppender");
            return null;
        }
        if (jsonLayout == null) {
            jsonLayout = JsonLayout.newBuilder().setComplete(true).setLocationInfo(true).build();
        }
        return new WebSocketAppender(name, filter, jsonLayout, false);
    }

    @Override
    public void onLoadedEvent() {
        // ClientSocketLogs.CustomStompSessionHandler.connect();
    }

}
