/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.pregunta.visita.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.pregunta.visita.dto.PreguntaVisitaDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @return @throws CustomException
     */
    public List<PreguntaVisitaDTO> selectRows(dto entityDTO) throws CustomException;

}
