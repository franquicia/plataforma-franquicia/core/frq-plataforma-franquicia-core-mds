/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Ruta", value = "Ruta")
public class Ruta extends RutaBase {

    @JsonProperty(value = "idRuta")
    @ApiModelProperty(notes = "Identificador del Ruta", example = "1", position = -1)
    private Integer idRuta;

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    @Override
    public String toString() {
        return "Ruta{" + "idRuta=" + idRuta + '}';
    }

}
