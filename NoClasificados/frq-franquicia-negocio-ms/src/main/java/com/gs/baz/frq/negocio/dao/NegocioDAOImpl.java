package com.gs.baz.frq.negocio.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.negocio.dao.util.GenericDAO;
import com.gs.baz.frq.negocio.dto.NegocioDTO;
import com.gs.baz.frq.negocio.mprs.NegocioRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class NegocioDAOImpl extends DefaultDAO implements GenericDAO<NegocioDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectNegocioFranquicia;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMNEGOCIODI");
        jdbcInsert.withProcedureName("SPINSNEGOCIODISC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMNEGOCIODI");
        jdbcUpdate.withProcedureName("SPACTNEGOCIODISC");

        jdbcSelectNegocioFranquicia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegocioFranquicia.withSchemaName(schema);
        jdbcSelectNegocioFranquicia.withCatalogName("PAADMNEGOCIODI");
        jdbcSelectNegocioFranquicia.withProcedureName("SPGETNEGOCIODISC");
        jdbcSelectNegocioFranquicia.returningResultSet("PA_CDATOS", new NegocioRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMNEGOCIODI");
        jdbcDelete.withProcedureName("SPDELNEGOCIODISC");
    }

    @Override
    public NegocioDTO insertRow(NegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioFranquicia());
            mapSqlParameterSource.addValue("PA_FCNEGOCIODISC", entityDTO.getNegocioDisciplinaFranquicia());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  NegocioFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  NegocioFranquicia"), ex);
        }
    }

    @Override
    public NegocioDTO updateRow(NegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioFranquicia());
            mapSqlParameterSource.addValue("PA_FCNEGOCIODISC", entityDTO.getNegocioDisciplinaFranquicia());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of NegocioFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of NegocioFranquicia"), ex);
        }
    }

    @Override
    public List<NegocioDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", null);
        Map<String, Object> out = jdbcSelectNegocioFranquicia.execute(mapSqlParameterSource);
        return (List<NegocioDTO>) out.get("PA_CDATOS");
    }

    @Override
    public NegocioDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityID);
        Map<String, Object> out = jdbcSelectNegocioFranquicia.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public NegocioDTO deleteRow(NegocioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioFranquicia());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of NegocioFranquicia"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of NegocioFranquicia"), ex);
        }
    }

}
