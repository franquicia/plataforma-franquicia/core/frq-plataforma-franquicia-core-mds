package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestElimina", description = "Objeto que contiene los datos a elminar en la tabla")
public class RequestElimina {

    @JsonProperty(value = "idProyecto", required = true)
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1", position = -3)
    private Integer idProyecto;

    @JsonProperty(value = "centroCostos", required = true)
    @ApiModelProperty(notes = "Centro de costos", example = "1", position = -2)
    private String centroCostos;

    @JsonProperty(value = "folio", required = true)
    @ApiModelProperty(notes = "Numero de folio asignado", example = "1", position = -1)
    private Integer folio;

    @JsonProperty(value = "idFormato", required = true)
    @ApiModelProperty(notes = "Numero de formato asignado", example = "1", position = -1)
    private Integer idFormato;

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

}
