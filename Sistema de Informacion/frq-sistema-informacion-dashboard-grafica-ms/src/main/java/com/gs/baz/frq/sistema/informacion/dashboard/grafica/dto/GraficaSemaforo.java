/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import io.swagger.annotations.ApiModel;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la grafica semaforo", value = "GraficaSemaforo")
public class GraficaSemaforo extends GraficaSemaforoBase {

}
