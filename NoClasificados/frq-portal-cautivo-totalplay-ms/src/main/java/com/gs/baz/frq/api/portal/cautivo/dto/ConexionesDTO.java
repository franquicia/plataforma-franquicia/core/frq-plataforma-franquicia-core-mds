/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class ConexionesDTO {

    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "correo")
    private String correo;

    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonProperty(value = "mac")
    private String mac;

    @JsonProperty(value = "tienda")
    private String tienda;

    // @JsonProperty(value = "codigoPostal")
    // private String codigoPostal;
    @JsonProperty(value = "edadCuestionario")
    private Integer edadCuestionario;

    @JsonProperty(value = "tipoCorreo")
    private Integer tipoCorreo;

    @JsonProperty(value = "fbNombre")
    private String fbNombre;

    @JsonProperty(value = "fbApellidoPaterno")
    private String fbApellido;

    @JsonProperty(value = "fbApellidoMaterno")
    private String apellidoMaterno;

    @JsonProperty(value = "fbEdad")
    private Integer fbEdad;

    @JsonProperty(value = "fbGenero")
    private String fbGenero;

    @JsonProperty(value = "fbFotoPerfil")
    private String fbFotoPerfil;

    @JsonProperty(value = "faseSesion")
    private String faseSesion;

    @JsonProperty(value = "ip")
    private String ip;

    @JsonProperty(value = "idUsuario")
    private String idUsuario;

    @JsonProperty(value = "enviar")
    private String enviar;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    public Integer getEdadCuestionario() {
        return edadCuestionario;
    }

    public void setEdadCuestionario(Integer edadCuestionario) {
        this.edadCuestionario = edadCuestionario;
    }

    public Integer getTipoCorreo() {
        return tipoCorreo;
    }

    public void setTipoCorreo(Integer tipoCorreo) {
        this.tipoCorreo = tipoCorreo;
    }

    public String getFbNombre() {
        return fbNombre;
    }

    public void setFbNombre(String fbNombre) {
        this.fbNombre = fbNombre;
    }

    public String getFbApellido() {
        return fbApellido;
    }

    public void setFbApellido(String fbApellido) {
        this.fbApellido = fbApellido;
    }

    public Integer getFbEdad() {
        return fbEdad;
    }

    public void setFbEdad(Integer fbEdad) {
        this.fbEdad = fbEdad;
    }

    public String getFbGenero() {
        return fbGenero;
    }

    public void setFbGenero(String fbGenero) {
        this.fbGenero = fbGenero;
    }

    public String getFbFotoPerfil() {
        return fbFotoPerfil;
    }

    public void setFbFotoPerfil(String fbFotoPerfil) {
        this.fbFotoPerfil = fbFotoPerfil;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getFaseSesion() {
        return faseSesion;
    }

    public void setFaseSesion(String faseSesion) {
        this.faseSesion = faseSesion;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEnviar() {
        return enviar;
    }

    public void setEnviar(String enviar) {
        this.enviar = enviar;
    }

}
