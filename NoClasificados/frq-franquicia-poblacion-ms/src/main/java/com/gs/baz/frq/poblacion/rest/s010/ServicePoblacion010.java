/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.poblacion.rest.s010;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.poblacion.dao.d010.PoblacionDAOImpl010;
import com.gs.baz.frq.poblacion.dto.PoblacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/poblacion/")
@Component("ServicePoblacion010")
public class ServicePoblacion010 {

    @Autowired
    private PoblacionDAOImpl010 poblacionDAOImpl;

    @RequestMapping(value = "secure/incump/dash/merge/incumplimientos/dash", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PoblacionDTO poblacionDTO(@RequestBody PoblacionDTO poblacionDTO) throws CustomException {
        return poblacionDAOImpl.executeStore(poblacionDTO);
    }

}
