/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

/**
 *
 * @author cescobarh
 */
public class SensibleDataBase64Exception extends Base64Exception {

    public SensibleDataBase64Exception(String message) {
        super(message);
    }

    public SensibleDataBase64Exception(String message, Throwable cause) {
        super(message, cause);
    }

}
