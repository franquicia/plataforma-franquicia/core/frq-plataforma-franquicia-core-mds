/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.firmas.client.bi.FoliosBI;
import com.gs.baz.frq.modelo.negocio.firmas.dao.FirmaDAOImpl;
import com.gs.baz.frq.modelo.negocio.firmas.dto.AltaFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.FirmaBase;
import com.gs.baz.frq.modelo.negocio.firmas.dto.Firmas;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosEstadoFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosVisualizacionFirma;
import com.gs.baz.frq.modelo.negocio.firmas.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.math.BigDecimal;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "firmas", value = "firmas", description = "Gestión de firmas")
@RestController
@RequestMapping("/api-local/modelo-negocio/folio-firmas/v1")
public class FirmaApi {

    @Autowired
    private FirmaDAOImpl firmaDAOImpl;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private FoliosBI firmaBI;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @param idFirma
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene firma", notes = "Obtiene un firma", nickname = "obtieneFirma")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FirmaBase obtieneFirma(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Long idFirma) throws CustomException, DataNotFoundException {
        FirmaBase firmaBase = firmaDAOImpl.selectRow(idFolio, idFirma);
        if (firmaBase == null) {
            throw new DataNotFoundException();
        }
        return firmaBase;
    }

    /**
     *
     * @param idFolio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene firmas", notes = "Obtiene todos los firmas", nickname = "obtieneFirmas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/firmas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Firmas obtieneFirmas(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        Firmas firmas = new Firmas(firmaDAOImpl.selectAllRows(idFolio));
        if (firmas.getFirmas() != null && firmas.getFirmas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return firmas;
    }

    /**
     *
     * @param idFolio
     * @param parametrosFirma
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear firma", notes = "Agrega un firma", nickname = "creaFirma")
    @RequestMapping(value = "/{idFolio}/firmas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFirma creaFirma(
            @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "ParametrosFirma", value = "Paramentros para el alta del firma", required = true) @RequestBody ParametrosFirma parametrosFirma) throws DataNotInsertedException {
        parametrosFirma.setIdFolio(idFolio);
        return firmaDAOImpl.insertRow(parametrosFirma);
    }

    /**
     *
     * @param idFolio
     * @param parametrosFirma
     * @param idFirma
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar firma", notes = "Actualiza un firma", nickname = "actualizaFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFirma(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "ParametrosFirma", value = "Paramentros para la actualización del firma", required = true) @RequestBody ParametrosFirma parametrosFirma, @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Integer idFirma) throws DataNotUpdatedException {
        parametrosFirma.setIdFirma(idFirma);
        parametrosFirma.setIdFolio(idFolio);
        firmaDAOImpl.updateRow(parametrosFirma);
        return new SinResultado();
    }

    /**
     *
     * @param headers
     * @param idFolio
     * @param parametrosEstadoFirma
     * @param idFirma
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar firma", notes = "Actualiza el estado firma", nickname = "actualizaEstadoFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/estados", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaEstadoFirma(@RequestHeader HttpHeaders headers,
            @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "ParametrosFirma", value = "Paramentros para la actualización del firma", required = true) @RequestBody ParametrosEstadoFirma parametrosEstadoFirma,
            @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Integer idFirma
    ) throws DataNotUpdatedException, Exception {
        parametrosEstadoFirma.setIdFirma(idFirma);
        parametrosEstadoFirma.setIdFolio(idFolio);

        //Integer idEstadoFolio = firmaDAOImpl.updateEstadoRow(parametrosEstadoFirma); 
        Map<String, BigDecimal> resultado = firmaDAOImpl.updateEstadoRow(parametrosEstadoFirma);  // Indice del siguiente firmador

        if (resultado != null) {
            firmaBI.putEstadoCorreo(httpServletRequest, headers, idFolio, resultado);
        } else {

        }

        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @param parametrosVisualizacionFirma
     * @param idFirma
     * @return
     * @throws DataNotUpdputEstadoFolioatedException
     */
    @ApiOperation(value = "Actualiza vizualizacion firma", notes = "Actualiza vizualizacion de una firma", nickname = "actualizaVisualizacionFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/visualizacion", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaVisualizacionFirma(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "ParametrosFirma", value = "Paramentros para la actualización de la visualizacion de firma", required = true) @RequestBody ParametrosVisualizacionFirma parametrosVisualizacionFirma, @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Integer idFirma) throws DataNotUpdatedException {
        parametrosVisualizacionFirma.setIdFirma(idFirma);
        firmaDAOImpl.updateVisualizacionRow(parametrosVisualizacionFirma);
        return new SinResultado();
    }

    /**
     *
     * @param idFirma
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar firma", notes = "Elimina un item de las firmas", nickname = "eliminaFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaFirma(@ApiParam(name = "idFirma", value = "Identificador de la firma", example = "1", required = true) @PathVariable("idFirma") Integer idFirma) throws DataNotDeletedException {
        firmaDAOImpl.deleteRow(idFirma);
        return new SinResultado();
    }

}
