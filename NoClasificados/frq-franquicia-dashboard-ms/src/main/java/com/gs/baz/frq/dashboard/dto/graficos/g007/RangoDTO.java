/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g007;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class RangoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntos_contacto")
    private Integer puntosContacto;

    public RangoDTO() {
    }

    public RangoDTO(Integer idRango, Integer calificacion, Integer puntosContacto) {
        this.idRango = idRango;
        this.calificacion = calificacion;
        this.puntosContacto = puntosContacto;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getPuntosContacto() {
        return puntosContacto;
    }

    public void setPuntosContacto(Integer puntosContacto) {
        this.puntosContacto = puntosContacto;
    }

    @Override
    public String toString() {
        return "RangoDTO{" + "idRango=" + idRango + ", calificacion=" + calificacion + ", puntosContacto=" + puntosContacto + '}';
    }

}
