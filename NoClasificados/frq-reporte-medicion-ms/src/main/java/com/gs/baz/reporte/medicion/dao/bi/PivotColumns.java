/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dao.bi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class PivotColumns {

    private final ArrayList<Object> columns;

    public PivotColumns(List<Object> objs, int... pRows) {
        columns = new ArrayList<>();
        for (int i = 0; i < pRows.length; i++) {
            columns.add(objs.get(pRows[i]));
        }
    }

    public void addObject(Object obj) {
        columns.add(obj);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((columns == null) ? 0 : columns.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PivotColumns other = (PivotColumns) obj;
        if (columns == null) {
            if (other.columns != null) {
                return false;
            }
        } else if (!columns.equals(other.columns)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String s = "";
        s = columns.stream().map((obj) -> obj + ",").reduce(s, String::concat);
        return s.substring(0, s.lastIndexOf(','));
    }
}
