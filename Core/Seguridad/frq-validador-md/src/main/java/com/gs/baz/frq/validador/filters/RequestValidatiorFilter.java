/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorValidationClient;
import com.gs.baz.frq.validador.bi.RequestValidatorWrapper;
import com.gs.baz.frq.validador.util.Base64Exception;
import com.gs.baz.frq.validador.util.NegativeParamValidationException;
import com.gs.baz.frq.validador.util.NegativePathParamValidationException;
import com.gs.baz.frq.validador.util.ParamValidationException;
import com.gs.baz.frq.validador.util.PathParamValidationException;
import com.gs.baz.frq.validador.util.RequiredDataException;
import com.gs.baz.frq.validador.util.SensibleDataBase64Exception;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE - 11)
public class RequestValidatiorFilter extends OncePerRequestFilter {

    private final Logger loggerValidator = LogManager.getLogger();

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            httpServletRequest = new RequestValidatorWrapper(httpServletRequest, httpServletResponse);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (CustomException ex) {
            loggerValidator.error(ex.getMessage(), ex);
            ResponseApiCodes apiCode;
            ResponseMessage responseMessage;
            RespuestaAPI respuestaErrorClient;
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04001;
            responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> erroresDetalles = new ArrayList<>();
            Throwable cause = ex.getCause().getCause();
            if (cause instanceof RequiredDataException) {
                RequiredDataException requiredDataException = (RequiredDataException) cause;
                requiredDataException.getFields().stream().forEach((itemField) -> {
                    erroresDetalles.add(new ErrorDetalle("El atributo '" + itemField + "' es requerido"));
                });
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof SensibleDataBase64Exception) {
                SensibleDataBase64Exception sensibleDataBase64Exception = (SensibleDataBase64Exception) cause;
                sensibleDataBase64Exception.getFields().stream().forEach((itemField) -> {
                    erroresDetalles.add(new ErrorDetalle("El atributo '" + itemField + "' debe estar en formato base64 y cifrado con la llave privada (accesoPublico) con cifrado RSA/ECB/PKCS1Padding encoding UTF-8"));
                });
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof Base64Exception) {
                Base64Exception base64Exception = (Base64Exception) cause;
                base64Exception.getFields().stream().forEach((itemField) -> {
                    erroresDetalles.add(new ErrorDetalle("El atributo '" + itemField + "' debe estar en formato base64"));
                });
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof NegativeParamValidationException) {
                NegativeParamValidationException negativeParamValidationException = (NegativeParamValidationException) cause;
                erroresDetalles.add(new ErrorDetalle("El parámetro '" + negativeParamValidationException.getName() + "' tiene un valor " + negativeParamValidationException.getValue() + " negativo"));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof NegativePathParamValidationException) {
                NegativePathParamValidationException negativePathParamValidationException = (NegativePathParamValidationException) cause;
                erroresDetalles.add(new ErrorDetalle("El parámetro de url '" + negativePathParamValidationException.getName() + "' tiene un valor " + negativePathParamValidationException.getValue() + " negativo"));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof PathParamValidationException) {
                PathParamValidationException pathParamValidationException = (PathParamValidationException) cause;
                erroresDetalles.add(new ErrorDetalle("El parámetro de url '" + pathParamValidationException.getName() + "' con valor " + pathParamValidationException.getValue() + " debe ser de tipo " + pathParamValidationException.getOriginalType()));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else if (cause instanceof ParamValidationException) {
                ParamValidationException paramValidationException = (ParamValidationException) cause;
                erroresDetalles.add(new ErrorDetalle("El parámetro '" + paramValidationException.getName() + "' con valor " + paramValidationException.getValue() + " debe ser de tipo " + paramValidationException.getOriginalType()));
                respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            } else {
                respuestaErrorClient = null;
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
            if (respuestaErrorClient != null) {
                httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                httpServletResponse.setStatus(responseMessage.getCode());
                OutputStream out = httpServletResponse.getOutputStream();
                ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(out, respuestaErrorClient);
                out.flush();
            }
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest httpServletRequest) throws ServletException {
        String uri = httpServletRequest.getRequestURI();
        return uri.contains("aplicaciones/llaves") || uri.contains("oauth/token");
    }

}
