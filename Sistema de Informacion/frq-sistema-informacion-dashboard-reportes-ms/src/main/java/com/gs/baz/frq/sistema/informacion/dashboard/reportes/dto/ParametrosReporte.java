/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class ParametrosReporte {

    @JsonProperty(value = "idDisciplina")
    @ApiModelProperty(notes = "Identificador de la disciplina", example = "1")
    private transient Integer idDisciplina;

    @JsonProperty(value = "fechaInicio")
    @ApiModelProperty(notes = "Fecha inicio", example = "15/12/2021", required = true)
    private String fechaInicio;

    @JsonProperty(value = "fechaFin")
    @ApiModelProperty(notes = "Fecha fin", example = "28/12/2021", required = true)
    private String fechaFin;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "-1")
    private Integer idNegocio;

    @JsonProperty(value = "nivelCentroCostos")
    @ApiModelProperty(notes = "Nivel geografico del Centro de Costos", example = "-1")
    private Integer nivelCentroCostos;

    @JsonProperty(value = "idSemaforo")
    @ApiModelProperty(notes = "Identificador del semaforo de la disciplina", example = "1")
    private Integer idSemaforo;
    
    @JsonProperty(value = "responsable")
    @ApiModelProperty(notes = "Area responsable", example = "Despachos")
    private String responsable;
    
    @JsonProperty(value = "modulo")
    @ApiModelProperty(notes = "Modulo asociado", example = "11020")
    private String modulo;
    
    @JsonProperty(value = "negocioPregunta")
    @ApiModelProperty(notes = "Negocio asociado a la pregunta", example = "4")
    private String negocioPregunta;

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getNivelCentroCostos() {
        return nivelCentroCostos;
    }

    public void setNivelCentroCostos(Integer nivelCentroCostos) {
        this.nivelCentroCostos = nivelCentroCostos;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public Integer getIdSemaforo() {
        return idSemaforo;
    }

    public void setIdSemaforo(Integer idSemaforo) {
        this.idSemaforo = idSemaforo;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getNegocioPregunta() {
        return negocioPregunta;
    }

    public void setNegocioPregunta(String negocioPregunta) {
        this.negocioPregunta = negocioPregunta;
    }

    @Override
    public String toString() {
        return "ParametrosReporte{" + "idDisciplina=" + idDisciplina + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", idNegocio=" + idNegocio + ", nivelCentroCostos=" + nivelCentroCostos + ", idSemaforo=" + idSemaforo + ", responsable=" + responsable + ", modulo=" + modulo + ", negocioPregunta=" + negocioPregunta + '}';
    }

}
