package com.gs.baz.frq.dashboard.certificacion.dao.graficos.g011;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g011.DashboardCertificacionG011DTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g011.DashboardCertificacionG011RowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG011DAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PADASHCCCERT");
        jdbcSelect.withProcedureName("SPASISTENCIACORE");
        jdbcSelect.returningResultSet("PA_CDATOS", new DashboardCertificacionG011RowMapper());

    }

    public List<DashboardCertificacionG011DTO> selectRowDashboardCertificacionG011(String entityDTO1, String entityDTO2, Long entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO1);
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO2);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO3);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO4);

        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionG011DTO>) out.get("PA_CDATOS");
        } else {

            logger.info("Algo paso con la asistencia" + out.get("PA_CDATOS"));

            return null;
        }

    }

    public List<DashboardCertificacionG011DTO> selectRowsDashboardCertificacionG011() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", null);
        mapSqlParameterSource.addValue("PA_FECHAFIN", null);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionG011DTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }

}
