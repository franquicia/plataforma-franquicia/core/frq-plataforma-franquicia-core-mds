package com.gs.baz.frq.envia.correo.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.envia.correo.dto.ConteoNegocioSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.dto.DatosEntradaCorreoSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.dto.ObtieneConteoNegocioSemanalLayoutsDTO;
import com.gs.baz.frq.envia.correo.mprs.ConteoNegocioSemanalLayoutsRowMapper;
import com.gs.baz.frq.envia.correo.mprs.ObtieneConteoNegocioSemanalLayoutsRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EnviaCorreoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcObtieneConteoNegocioSemanalLayouts;
    private DefaultJdbcCall jdbcObtieneInformacionGeneralSemanalLayouts;

    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcObtieneConteoNegocioSemanalLayouts = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoNegocioSemanalLayouts.withSchemaName(schema);
        jdbcObtieneConteoNegocioSemanalLayouts.withCatalogName("PA_SOPLAYOUT");
        jdbcObtieneConteoNegocioSemanalLayouts.withProcedureName("SP_SEL_CONTEOP");
        jdbcObtieneConteoNegocioSemanalLayouts.returningResultSet("RCL_FORM", new ConteoNegocioSemanalLayoutsRowMapper());

        jdbcObtieneInformacionGeneralSemanalLayouts = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneInformacionGeneralSemanalLayouts.withSchemaName(schema);
        jdbcObtieneInformacionGeneralSemanalLayouts.withCatalogName("PA_SOPLAYOUT");
        jdbcObtieneInformacionGeneralSemanalLayouts.withProcedureName("SP_SEL_PROYGEN");
        jdbcObtieneInformacionGeneralSemanalLayouts.returningResultSet("RCL_FORM", new ObtieneConteoNegocioSemanalLayoutsRowMapper());

    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> ObtieneConteoNegocioSemanalLayouts(DatosEntradaCorreoSemanalLayoutsDTO bean) throws CustomException {

        Map<String, Object> response = new HashMap<String, Object>();
        List<ConteoNegocioSemanalLayoutsDTO> listaConteos = new ArrayList<ConteoNegocioSemanalLayoutsDTO>();
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHAINI", bean.getFechaInicialReporte().toString())
                .addValue("PA_FECHAFIN", bean.getFechaFinalReporte().toString())
                .addValue("PA_NEGOCIO", null);

        out = jdbcObtieneConteoNegocioSemanalLayouts.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICIA.PA_SOPLAYOUT.SP_SEL_CONTEOP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 1) {
            listaConteos = (List<ConteoNegocioSemanalLayoutsDTO>) out.get("RCL_FORM");
            response.put("listaConteos", listaConteos);
        }

        return response;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> ObtieneInformacionGeneralSemanalLayouts(DatosEntradaCorreoSemanalLayoutsDTO bean) throws CustomException {

        Map<String, Object> response = new HashMap<String, Object>();
        List<ObtieneConteoNegocioSemanalLayoutsDTO> listaConsulta = new ArrayList<ObtieneConteoNegocioSemanalLayoutsDTO>();
        Map<String, Object> out = null;
        int ejecucucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHAINI", bean.getFechaInicialReporte().toString())
                .addValue("PA_FECHAFIN", bean.getFechaFinalReporte().toString())
                .addValue("PA_NEGOCIO", null);

        out = jdbcObtieneInformacionGeneralSemanalLayouts.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICIA.PA_SOPLAYOUT.SP_SEL_PROYGEN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucucion = resultado.intValue();

        if (ejecucucion == 1) {
            listaConsulta = (List<ObtieneConteoNegocioSemanalLayoutsDTO>) out.get("RCL_FORM");
            response.put("listaConsulta", listaConsulta);
        }

        return response;
    }

}
