/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de arboles del modelo de negocio", value = "arboles")
public class Arboles {

    @JsonProperty(value = "arboles")
    @ApiModelProperty(notes = "arboles")
    private List<Arbol> arboles;

    public Arboles(List<Arbol> arboles) {
        this.arboles = arboles;
    }

    public List<Arbol> getArboles() {
        return arboles;
    }

    public void setArboles(List<Arbol> arboles) {
        this.arboles = arboles;
    }

    @Override
    public String toString() {
        return "Arboles{" + "arboles=" + arboles + '}';
    }

}
