package com.gs.baz.frq.envia.correo.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.baz.frq.envia.correo.bi.EnviaCorreoBI;
import com.gs.baz.frq.envia.correo.dao.EnviaCorreoDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.envia.correo")
public class ConfigEnvioCorreo {

    private final Logger logger = LogManager.getLogger();

    public ConfigEnvioCorreo() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqEnviaCorreoDAOImpl")
    public EnviaCorreoDAOImpl enviaCorreoDAOImpl() {
        return new EnviaCorreoDAOImpl();
    }

    @Bean(initMethod = "init", name = "frqEnviaCorreoBI")
    public EnviaCorreoBI enviaCorreoBI() {
        return new EnviaCorreoBI();
    }
}
