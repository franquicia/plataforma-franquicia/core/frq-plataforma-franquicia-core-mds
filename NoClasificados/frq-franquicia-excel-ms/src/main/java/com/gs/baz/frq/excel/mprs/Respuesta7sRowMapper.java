package com.gs.baz.frq.excel.mprs;

import com.gs.baz.frq.excel.dto.Respuesta7sDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class Respuesta7sRowMapper implements RowMapper<Respuesta7sDTO> {

    private Respuesta7sDTO status;

    @Override
    public Respuesta7sDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new Respuesta7sDTO();
        status.setIdVisita((BigDecimal) rs.getObject("FIVISITA_ID"));
        status.setIdPregunta((BigDecimal) rs.getObject("FIPREGUNTA_BASE"));
        status.setRespuesta(rs.getString("FCRESPUESTA"));

        return status;
    }
}
