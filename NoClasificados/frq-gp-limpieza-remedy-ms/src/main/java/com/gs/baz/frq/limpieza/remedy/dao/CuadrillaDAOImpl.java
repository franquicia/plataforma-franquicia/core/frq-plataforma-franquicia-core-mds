package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.CuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.CuadrillaPasswRowMapper;
import com.gs.baz.frq.limpieza.remedy.mprs.CuadrillaRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CuadrillaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertaCuadrilla;
    private DefaultJdbcCall jdbcEliminaCuadrilla;
    private DefaultJdbcCall jdbcBuscaCuadrilla;
    private DefaultJdbcCall jdbcBuscaInfoCuadrilla;
    private DefaultJdbcCall jdbcActualizaCuadrilla;
    private DefaultJdbcCall jdbcCargaPass;
    private DefaultJdbcCall jdbcobtienePass;
    private DefaultJdbcCall jdbcEliminaCuadrillaProveedor;

    private List<CuadrillaDTO> listaDetU;

    private static final Logger LOGGER = LogManager.getLogger();
    private static String schemaBD = "GESTION";
    private static String catalogName = "PADMCUADRILLA";
    private static String cursorSalida = "PA_EJECUCION";
    private static final String CURSORGENERAL = "RCL_CUADRI";

    public CuadrillaDAOImpl() {
        super();

    }

    public void init() {

        jdbcInsertaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_INS_CUAD");

        jdbcEliminaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_DEL_CUAD");

        jdbcBuscaInfoCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet(CURSORGENERAL, new CuadrillaRowMapper());

        jdbcBuscaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet(CURSORGENERAL, new CuadrillaRowMapper());

        jdbcActualizaCuadrilla = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_ACT_CUAD");

        jdbcCargaPass = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_CARGAPAS");

        jdbcobtienePass = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_SEL_PASS")
                .returningResultSet(CURSORGENERAL, new CuadrillaPasswRowMapper());

        jdbcEliminaCuadrillaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName(schemaBD)
                .withCatalogName(catalogName)
                .withProcedureName("SP_DEL_CUADPROV");

    }

    @SuppressWarnings("all")
    public int inserta(CuadrillaDTO bean) {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDERCUAD", bean.getLiderCuad())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_SEGUNDO", bean.getSegundo())
                .addValue("PA_PASS", bean.getPs5());

        out = jdbcInsertaCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_INS_CUAD}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        BigDecimal idbloc = (BigDecimal) out.get("PA_IDCUADRI");
        idNota = idbloc.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al insertar el evento");
        }

        return idNota;
    }

    public boolean elimina(String idCuadrilla) {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", idCuadrilla);

        out = jdbcEliminaCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_DEL_CUAD}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("all")
    public List<CuadrillaDTO> obtieneDatos(int idCuadrilla) {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROVEED", idCuadrilla);

        out = jdbcBuscaCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        listaDetU = (List<CuadrillaDTO>) out.get("RCL_CUADRI");

        if (respuesta != 1) {
            LOGGER.info("Algo paso al consular la cuadrilla ");
        }

        return listaDetU;
    }

    @SuppressWarnings("all")
    public List<CuadrillaDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<CuadrillaDTO> lista = null;
        int error = 0;

        out = jdbcBuscaInfoCuadrilla.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_SEL_DETALLE}");
        listaDetU = (List<CuadrillaDTO>) out.get("RCL_CUADRI");
        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        error = resultado.intValue();
        if (error != 1) {
            LOGGER.info("Algo paso al obtener las Cuadrilla");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @SuppressWarnings("all")
    public boolean actualiza(CuadrillaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCUADRI", bean.getIdCuadrilla())
                .addValue("PA_PROVEED", bean.getIdProveedor())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_LIDERCUAD", bean.getLiderCuad())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_SEGUNDO", bean.getSegundo())
                .addValue("PA_PASS", bean.getPs5());

        out = jdbcActualizaCuadrilla.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_ACT_FILAS}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            LOGGER.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("all")
    public boolean cargapass() throws Exception {
        int respuesta = 0;
        Map<String, Object> out = null;

        out = jdbcCargaPass.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_CARGAPAS}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("all")
    public List<CuadrillaDTO> obtienepass(int idCuadrilla, int idProveedor, int zona) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRILLA", idCuadrilla)
                .addValue("PA_PROVEED", idProveedor)
                .addValue("PA_ZONA", zona);

        out = jdbcobtienePass.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PADMCUADRILLA.SP_SEL_PASS}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        listaDetU = (List<CuadrillaDTO>) out.get("RCL_CUADRI");

        if (respuesta != 1) {
            LOGGER.info("Algo paso al consular la cuadrilla ");
        }

        return listaDetU;
    }

    @SuppressWarnings("all")
    public boolean eliminaCuadrillaProveedor(int cuadrilla, int proveedor, int zona) throws Exception {

        Map< String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CUADRILLA", cuadrilla)
                .addValue("PA_PROVEEDOR", proveedor)
                .addValue("PA_ZONA", zona);

        out = jdbcEliminaCuadrillaProveedor.execute(in);

        LOGGER.info("Funcion Ejecutada: {GESTION.PADMCUADRILLA.SP_DEL_CUADPROV}");

        BigDecimal resultado = (BigDecimal) out.get(cursorSalida);
        respuesta = resultado.intValue();

        boolean retorno = false;

        if (respuesta == 1) {
            retorno = true;
        } else {
            retorno = false;
            LOGGER.info("Algo paso al eliminar la cuadrilla del proveedor ");
        }

        return retorno;
    }

}
