/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.configuracion.disciplina.rest;

import com.gs.baz.frq.configuracion.disciplina.dao.ConfiguracionDisciplinaDAOImpl;
import com.gs.baz.frq.configuracion.disciplina.dto.ConfiguracionDisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/configuracion/disciplina")
@Component("FRQServiceConfiguracionDisciplina")
public class ServiceConfiguracionDisciplina {

    @Autowired
    @Qualifier("frqConfiguracionDisciplinaDAOImpl")
    private ConfiguracionDisciplinaDAOImpl configuracionDisciplinaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ConfiguracionDisciplinaDTO postConfiguracionDisciplina(@RequestBody ConfiguracionDisciplinaDTO configuracionDisciplina) throws CustomException {
        if (configuracionDisciplina.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (configuracionDisciplina.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (configuracionDisciplina.getIdFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia"));
        }
        if (configuracionDisciplina.getProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("protocolo"));
        }
        if (configuracionDisciplina.getRegistroActivo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("registro_activo"));
        }
        if (configuracionDisciplina.getIdNegocioDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio"));
        }
        return configuracionDisciplinaDAOImpl.insertRow(configuracionDisciplina);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ConfiguracionDisciplinaDTO putConfiguracionDisciplina(@RequestBody ConfiguracionDisciplinaDTO configuracionDisciplina) throws CustomException {
        if (configuracionDisciplina.getIdConfiguracion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_configuracion"));
        }
        if (configuracionDisciplina.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (configuracionDisciplina.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (configuracionDisciplina.getIdFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia"));
        }
        if (configuracionDisciplina.getProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("protocolo"));
        }
        if (configuracionDisciplina.getRegistroActivo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("registro_activo"));
        }
        if (configuracionDisciplina.getIdNegocioDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio"));
        }
        return configuracionDisciplinaDAOImpl.updateRow(configuracionDisciplina);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ConfiguracionDisciplinaDTO> getConfiguracionDisciplina(@RequestBody ConfiguracionDisciplinaDTO configuracionDisciplina) throws CustomException {
        return configuracionDisciplinaDAOImpl.selectRows(configuracionDisciplina);
    }

    @RequestMapping(value = "secure/getParam", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ConfiguracionDisciplinaDTO> getConfiguracionDisciplinaByParameter(
            @RequestParam(value = "idDisciplina", required = false) Long idDisciplina,
            @RequestParam(value = "idProtocolo", required = false) Long idProtocolo,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio) throws CustomException {

        ConfiguracionDisciplinaDTO configuracionDisciplina = new ConfiguracionDisciplinaDTO();
        configuracionDisciplina.setIdDisciplina(BigDecimal.valueOf(idDisciplina));
        configuracionDisciplina.setIdProtocolo(BigDecimal.valueOf(idProtocolo));
        configuracionDisciplina.setIdNegocioDisciplina(BigDecimal.valueOf(idNegocio));
        return configuracionDisciplinaDAOImpl.selectRows(configuracionDisciplina);
    }

    @RequestMapping(value = "secure/getPost", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ConfiguracionDisciplinaDTO> getConfiguracionDisciplinaPost(@RequestBody ConfiguracionDisciplinaDTO configuracionDisciplina) throws CustomException {
        return configuracionDisciplinaDAOImpl.selectRows(configuracionDisciplina);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ConfiguracionDisciplinaDTO deleteConfiguracionDisciplina(@RequestBody ConfiguracionDisciplinaDTO configuracionDisciplina) throws CustomException {
        if (configuracionDisciplina.getIdConfiguracion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_configuracion"));
        }
        if (configuracionDisciplina.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (configuracionDisciplina.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (configuracionDisciplina.getIdFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia"));
        }
        if (configuracionDisciplina.getIdNegocioDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio"));
        }
        return configuracionDisciplinaDAOImpl.deleteRow(configuracionDisciplina);
    }
}
