package com.gs.baz.frq.frecuencia.medicion.mprs;

import com.gs.baz.frq.frecuencia.medicion.dto.FrecuenciaMedicionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FrecuenciaMedicionRowMapper implements RowMapper<FrecuenciaMedicionDTO> {

    private FrecuenciaMedicionDTO status;

    @Override
    public FrecuenciaMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new FrecuenciaMedicionDTO();
        status.setIdFrecuencia(((BigDecimal) rs.getObject("FIFRECUENCIA_ID")));
        status.setFrecuencia(rs.getString("FCFRECUENCIA"));
        return status;
    }
}
