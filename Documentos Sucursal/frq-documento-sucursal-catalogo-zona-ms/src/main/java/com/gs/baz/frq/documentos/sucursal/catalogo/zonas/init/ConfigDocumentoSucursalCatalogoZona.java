package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dao.DocumentoSucursalCatalogoZonasDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documentos.sucursal.catalogo.zonas")
public class ConfigDocumentoSucursalCatalogoZona {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalCatalogoZona() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalCatalogoZonasDAOImpl documentoSucursalCatalogoZonasDAOImpl() {
        return new DocumentoSucursalCatalogoZonasDAOImpl();
    }

}
