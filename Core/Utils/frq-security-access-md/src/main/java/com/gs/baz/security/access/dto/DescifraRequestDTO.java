/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Estructura de entrada para descifrar algun valor", value = "DescifraSolicitud")
public class DescifraRequestDTO {

    @JsonProperty(value = "accesoPrivado", required = true)
    @ApiModelProperty(notes = "Llave asimétrica privada", example = "<Cadena con la llave privada>", required = true)
    private String accesoPrivado;

    @JsonProperty(value = "valor", required = true)
    @ApiModelProperty(notes = "Valor cifrado", example = "<Cadena cifrada en Base64>", required = true)
    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getAccesoPrivado() {
        return accesoPrivado;
    }

    public void setAccesoPrivado(String accesoPrivado) {
        this.accesoPrivado = accesoPrivado;
    }

}
