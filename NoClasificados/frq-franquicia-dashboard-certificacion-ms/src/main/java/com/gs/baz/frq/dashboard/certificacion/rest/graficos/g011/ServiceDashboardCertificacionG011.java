/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g011;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g011.DashboardCertificacionG011DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g011.DashboardCertificacionG011DTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g011")
public class ServiceDashboardCertificacionG011 {

    @Autowired
    private DashboardCertificacionG011DAOImpl dashboardCertificacionG011DAOImpl;

    @RequestMapping(value = "secure/grafico/get/puntos/asistencia", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG011DTO> getDatosPuntosG011() throws CustomException {
        return dashboardCertificacionG011DAOImpl.selectRowsDashboardCertificacionG011();
    }

    @RequestMapping(value = "secure/grafico/get/puntos/asistencia/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG011DTO> getDatoPuntosG011(
            @RequestParam(value = "fechaInicial", required = false) String fechaInicial,
            @RequestParam(value = "fechaFinal", required = false) String fechaFinal,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio) throws CustomException {
        return dashboardCertificacionG011DAOImpl.selectRowDashboardCertificacionG011(fechaInicial, fechaFinal, idUsuario, idNegocio);
    }

}
