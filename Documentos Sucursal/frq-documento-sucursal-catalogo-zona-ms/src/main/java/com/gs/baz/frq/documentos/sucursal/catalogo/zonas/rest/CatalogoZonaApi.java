/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.rest;

import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dao.DocumentoSucursalCatalogoZonasDAOImpl;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.AltaCatalogoZona;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.CatalogoZonaBase;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.CatalogoZonas;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.ParametrosCatalogoZona;
import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "catalogoZona", value = "catalogoZona", description = "Api para la gestión del catalogo zonas de Layout")
@RestController
@RequestMapping("/api-local/documento-sucursal/catalogo-zona/v1")
public class CatalogoZonaApi {

    @Autowired
    private DocumentoSucursalCatalogoZonasDAOImpl documentoSucursalCatalogoZonasDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene CatalogoZona", notes = "Obtiene una Zona", nickname = "obtieneCatalogoZona")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idZona}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CatalogoZonaBase obtieneTipoInformacion(@ApiParam(name = "idZona", value = "Identificador de la zona", example = "1", required = true) @PathVariable("idZona") Long idZona) throws CustomException, DataNotFoundException {
        CatalogoZonaBase catalogoZonaBase = documentoSucursalCatalogoZonasDAOImpl.selectRow(idZona);
        if (catalogoZonaBase == null) {
            throw new DataNotFoundException();
        }
        return catalogoZonaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene catalogos zona", notes = "Obtiene todas las zonas", nickname = "obtieneCatalogoZonas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CatalogoZonas obtieneCatalogoZonas() throws CustomException, DataNotFoundException {
        CatalogoZonas catalogoZonas = new CatalogoZonas(documentoSucursalCatalogoZonasDAOImpl.selectAllRows());
        if (catalogoZonas.getTiposInformacion() != null && catalogoZonas.getTiposInformacion().isEmpty()) {
            throw new DataNotFoundException();
        }
        return catalogoZonas;
    }

    /**
     *
     * @param parametrosCatalogoZona
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear catalogoZona", notes = "Agrega un tipoInformacion", nickname = "creaZona")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaCatalogoZona creaZona(@ApiParam(name = "ParametrosCatalogoZona", value = "Paramentros para el alta del catalogo zona", required = true) @RequestBody ParametrosCatalogoZona parametrosCatalogoZona) throws DataNotInsertedException {
        return documentoSucursalCatalogoZonasDAOImpl.insertRow(parametrosCatalogoZona);
    }

    /**
     *
     * @param parametrosCatalogoZona
     * @param idTipoInformacion
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar zona", notes = "Actualiza una zona", nickname = "actualizaNegocioZona")
    @RequestMapping(value = "/{idCatalogoZona}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaCatalogoZona(@ApiParam(name = "ParametrosTipoInformacion", value = "Paramentros para la actualización del zona", required = true) @RequestBody ParametrosCatalogoZona parametrosCatalogoZona, @ApiParam(name = "idCatalogoZona", value = "Identificador del catalogo zona", example = "1", required = true) @PathVariable("idCatalogoZona") Integer idCatalogoZona) throws DataNotUpdatedException {
        parametrosCatalogoZona.setIdCatalogoZona(idCatalogoZona);

        documentoSucursalCatalogoZonasDAOImpl.updateRow(parametrosCatalogoZona);
        return new SinResultado();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar zona", notes = "Elimina una zona", nickname = "eliminaZona")
    @RequestMapping(value = "/{idCatalogoZona}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaTipoInformacion(@ApiParam(name = "idCatalogoZona", value = "Identificador del id catalogo zona", example = "1", required = true) @PathVariable("idCatalogoZona") Integer idCatalogoZona) throws DataNotDeletedException {
        documentoSucursalCatalogoZonasDAOImpl.deleteRow(idCatalogoZona);
        return new SinResultado();
    }

}
