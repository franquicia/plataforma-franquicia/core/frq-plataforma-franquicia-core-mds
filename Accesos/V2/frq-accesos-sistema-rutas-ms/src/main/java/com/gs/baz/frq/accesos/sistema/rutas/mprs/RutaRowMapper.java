package com.gs.baz.frq.accesos.sistema.rutas.mprs;

import com.gs.baz.frq.accesos.sistema.rutas.dto.RutaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RutaRowMapper implements RowMapper<RutaBase> {

    @Override
    public RutaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        RutaBase ruta = new RutaBase();
        ruta.setNombre(rs.getString("FCDESCRIPCION"));
        ruta.setRutaAbsoluta(rs.getString("FCRUTA"));
        return ruta;
    }

}
