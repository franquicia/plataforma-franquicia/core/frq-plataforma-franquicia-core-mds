/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de vistas", value = "vistas")
public class Vistas {

    @JsonProperty(value = "vistas")
    @ApiModelProperty(notes = "vistas")
    private List<Vista> vistas;

    public Vistas(List<Vista> vistas) {
        this.vistas = vistas;
    }

    public List<Vista> getVistas() {
        return vistas;
    }

    public void setVistas(List<Vista> vistas) {
        this.vistas = vistas;
    }

    @Override
    public String toString() {
        return "Vistas{" + "vistas=" + vistas + '}';
    }

}
