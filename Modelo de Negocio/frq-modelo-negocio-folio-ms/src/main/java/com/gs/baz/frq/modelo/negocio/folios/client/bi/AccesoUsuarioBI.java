/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.client.bi;

import com.gs.baz.frq.modelo.negocio.folios.client.dto.ParametrosDocumentoUsuario;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Component
public class AccesoUsuarioBI {

    @Autowired
    private RestTemplate restTemplate;

    public void postDocumentoUsuario(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer numeroEmpleado, Integer idDocumento) throws Exception {
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/accesos/usuarios/v1/" + numeroEmpleado + "/documentos";
        ParametrosDocumentoUsuario parametrosDocumentoUsuario = new ParametrosDocumentoUsuario(idDocumento);
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(parametrosDocumentoUsuario, httpHeaders);
            restTemplate.exchange(basePath, HttpMethod.POST, httpEntityUsuario, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        }
    }
}
