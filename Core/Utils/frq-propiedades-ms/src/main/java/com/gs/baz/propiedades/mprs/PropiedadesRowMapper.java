package com.gs.baz.propiedades.mprs;

import com.gs.baz.propiedades.dto.PropiedadesDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PropiedadesRowMapper implements RowMapper<PropiedadesDTO> {

    private PropiedadesDTO propiedades;

    @Override
    public PropiedadesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        propiedades = new PropiedadesDTO();
        propiedades.setIdPropiedad(((BigDecimal) rs.getObject("FIIDPROP")));
        propiedades.setClave(rs.getString("FCCLAVE"));
        propiedades.setValor(rs.getString("FCVALOR"));
        propiedades.setEncriptado(((BigDecimal) rs.getObject("FIENCRIPTADO")));
        return propiedades;
    }
}
