/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.Grupo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del grupo usuario", value = "GrupoUsuario")
public class GrupoUsuario {

    @JsonProperty(value = "idGrupoUsuario")
    @ApiModelProperty(notes = "Identificador del grupo", example = "1", position = -1)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idGrupoUsuario;

    @JsonProperty(value = "grupo")
    @ApiModelProperty(notes = "Datos del grupo", example = "1")
    private Grupo grupo;

    public Integer getIdGrupoUsuario() {
        return idGrupoUsuario;
    }

    public void setIdGrupoUsuario(Integer idGrupoUsuario) {
        this.idGrupoUsuario = idGrupoUsuario;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @Override
    public String toString() {
        return "GrupoUsuario{" + "idGrupoUsuario=" + idGrupoUsuario + ", grupo=" + grupo + '}';
    }

}
