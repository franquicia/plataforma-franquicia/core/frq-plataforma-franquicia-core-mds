/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.DatoZonaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DatoZonaRowMapper implements RowMapper<DatoZonaBase> {

    private DatoZonaBase datoZonaBase;

    @Override
    public DatoZonaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        datoZonaBase = new DatoZonaBase();
        datoZonaBase.setIdCatalogoZona(rs.getInt("FIIDLYZONAS"));
        datoZonaBase.setIdFolio(rs.getInt("FIID_FOLIO"));
        datoZonaBase.setObservaciones(rs.getString("FCOBSERVACIONES"));
        datoZonaBase.setValor(rs.getString("FCVALOR"));

        return datoZonaBase;
    }
}
