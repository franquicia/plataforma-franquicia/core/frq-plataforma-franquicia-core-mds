/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dao.SistemaInformacionDashboardSemaforoDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.AltaSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.ParametrosSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.SemaforoBase;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.Semaforos;
import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "semaforos", value = "semaforos", description = "Api para la gestión del catalogo de semaforos")
@RestController
@RequestMapping("/api-local/sistema-informacion/tablero/semaforos/v1")
public class SistemaInformacionDashboardSemaforoApi {

    @Autowired
    private SistemaInformacionDashboardSemaforoDAOImpl sistemaInformacionDashboardSemaforoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idSemaforo
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene semaforo", notes = "Obtiene un semaforo", nickname = "obtieneSemaforo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idSemaforo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SemaforoBase obtieneSemaforo(@ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo) throws CustomException, DataNotFoundException {
        SemaforoBase semaforoBase = sistemaInformacionDashboardSemaforoDAOImpl.selectRow(idSemaforo);
        if (semaforoBase == null) {
            throw new DataNotFoundException();
        }
        return semaforoBase;
    }

    /**
     *
     * @param clave
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene semaforo desde la clave", notes = "Obtiene un semaforo desde la clave", nickname = "obtieneSemaforoClave")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/clave/{valor}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SemaforoBase obtieneSemaforoClave(@ApiParam(name = "valor", value = "Clave del semaforo", example = "1", required = true) @PathVariable("valor") String clave) throws CustomException, DataNotFoundException {
        SemaforoBase semaforoBase = sistemaInformacionDashboardSemaforoDAOImpl.selectRow(clave);
        if (semaforoBase == null) {
            throw new DataNotFoundException();
        }
        return semaforoBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene semaforos", notes = "Obtiene todos los semaforos", nickname = "obtieneSemaforos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Semaforos obtieneSemaforos() throws CustomException, DataNotFoundException {
        Semaforos semaforos = new Semaforos(sistemaInformacionDashboardSemaforoDAOImpl.selectAllRows());
        if (semaforos.getSemaforos() != null && semaforos.getSemaforos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return semaforos;
    }

    /**
     *
     * @param parametrosSemaforo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear semaforo", notes = "Agrega un semaforo", nickname = "creaSemaforo")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaSemaforo creaSemaforo(@ApiParam(name = "ParametrosSemaforo", value = "Paramentros para el alta del semaforo", required = true) @RequestBody ParametrosSemaforo parametrosSemaforo) throws DataNotInsertedException {
        return sistemaInformacionDashboardSemaforoDAOImpl.insertRow(parametrosSemaforo);
    }

    /**
     *
     * @param parametrosSemaforo
     * @param idSemaforo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar semaforo", notes = "Actualiza un semaforo", nickname = "actualizaSemaforo")
    @RequestMapping(value = "/{idSemaforo}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaSemaforo(@ApiParam(name = "ParametrosSemaforo", value = "Paramentros para la actualización del semaforo", required = true) @RequestBody ParametrosSemaforo parametrosSemaforo, @ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo) throws DataNotUpdatedException {
        parametrosSemaforo.setIdSemaforo(idSemaforo);
        sistemaInformacionDashboardSemaforoDAOImpl.updateRow(parametrosSemaforo);
        return new SinResultado();
    }

    /**
     *
     * @param idSemaforo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar semaforo", notes = "Elimina un item de los semaforos", nickname = "eliminaSemaforo")
    @RequestMapping(value = "/{idSemaforo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaSemaforo(@ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo) throws DataNotDeletedException {
        sistemaInformacionDashboardSemaforoDAOImpl.deleteRow(idSemaforo);
        return new SinResultado();
    }

}
