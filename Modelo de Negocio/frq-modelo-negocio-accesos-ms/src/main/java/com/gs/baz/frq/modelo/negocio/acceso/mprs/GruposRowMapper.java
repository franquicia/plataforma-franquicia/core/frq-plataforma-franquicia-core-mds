/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.mprs;

import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.Grupo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class GruposRowMapper implements RowMapper<Grupo> {

    private Grupo documento;

    @Override
    public Grupo mapRow(ResultSet rs, int rowNum) throws SQLException {
        documento = new Grupo();
        documento.setIdGrupo(rs.getInt("FIIDGRUPO"));
        documento.setDescripcion(rs.getString("FCDESCRIPCION"));
        documento.setTotalDocumentos(rs.getInt("TOTAL_DOCTOS"));
        documento.setTotalUsuarios(rs.getInt("TOTAL_USUARIO"));
        return documento;
    }
}
