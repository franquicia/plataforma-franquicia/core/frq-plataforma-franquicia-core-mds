/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g004;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g004.DashboardCertificacionG004DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g004.DashboardCertificacionG004DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g004")
public class ServiceDashboardCertificacionG004 {

    @Autowired
    private DashboardCertificacionG004DAOImpl dashboardCertificacionG004DAOImpl;

    @RequestMapping(value = "secure/grafico/get/odometro/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DashboardCertificacionG004DTO getDatoOdometroG004(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @RequestParam(value = "idGerente", required = false) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio) throws CustomException {
        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFin == null || fechaFin.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFin"));
        }

        return dashboardCertificacionG004DAOImpl.selectRowDashboardCertificacionG004(fechaInicial, fechaFin, idGerente, idNegocio);

    }
}
