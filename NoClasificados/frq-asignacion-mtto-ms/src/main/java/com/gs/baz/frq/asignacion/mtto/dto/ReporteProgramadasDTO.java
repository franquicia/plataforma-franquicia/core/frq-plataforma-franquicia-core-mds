/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReporteProgramadasDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idProgramacion")
    private Integer idProgramacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private String estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaProgramada")
    private String fechaProgramada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaRealizada")
    private String fechaRealizada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCecoSuperior")
    private Integer idCecoSuperior;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "reprogramada")
    private String reprogramada;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "comentarioReprogramacion")
    private String comentarioReprogramacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idZoma")
    private Integer idZona;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idTerritorio")
    private Integer idTerritorio;
    
    
    
    public Integer getIdZona() {
		return idZona;
	}

	public void setIdZona(Integer idZona) {
		this.idZona = idZona;
	}

	public Integer getIdTerritorio() {
		return idTerritorio;
	}

	public void setIdTerritorio(Integer idTerritorio) {
		this.idTerritorio = idTerritorio;
	}

	public String getReprogramada() {
		return reprogramada;
	}

	public void setReprogramada(String reprogramada) {
		this.reprogramada = reprogramada;
	}

	public String getComentarioReprogramacion() {
		return comentarioReprogramacion;
	}

	public void setComentarioReprogramacion(String comentarioReprogramacion) {
		this.comentarioReprogramacion = comentarioReprogramacion;
	}

	public Integer getIdProgramacion() {
        return idProgramacion;
    }

    public void setIdProgramacion(Integer idProgramacion) {
        this.idProgramacion = idProgramacion;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getFechaProgramada() {
        return fechaProgramada;
    }

    public void setFechaProgramada(String fechaProgramada) {
        this.fechaProgramada = fechaProgramada;
    }

    public String getFechaRealizada() {
        return fechaRealizada;
    }

    public void setFechaRealizada(String fechaRealizada) {
        this.fechaRealizada = fechaRealizada;
    }

    public Integer getIdCecoSuperior() {
        return idCecoSuperior;
    }

    public void setIdCecoSuperior(Integer idCecoSuperior) {
        this.idCecoSuperior = idCecoSuperior;
    }

    @Override
	public String toString() {
		return "ReporteProgramadasDTO [idProgramacion=" + idProgramacion + ", idCeco=" + idCeco + ", nombreCeco="
				+ nombreCeco + ", idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", estatus=" + estatus
				+ ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", zona=" + zona + ", territorio="
				+ territorio + ", fechaProgramada=" + fechaProgramada + ", fechaRealizada=" + fechaRealizada
				+ ", idCecoSuperior=" + idCecoSuperior + ", reprogramada=" + reprogramada
				+ ", comentarioReprogramacion=" + comentarioReprogramacion + ", idZona=" + idZona + ", idTerritorio="
				+ idTerritorio + "]";
	}

}
