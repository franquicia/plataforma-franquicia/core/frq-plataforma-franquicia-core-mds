/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class EstadoFolio {

    @JsonProperty(value = "idEstadoFolio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1")
    private Integer idEstadoFolio;

    @JsonProperty(value = "descripcion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Descripción del estado", example = "Por documentar")
    private String descripcion;

    public EstadoFolio() {
    }

    public EstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    public EstadoFolio(String descripcion) {
        this.descripcion = descripcion;
    }

    public EstadoFolio(Integer idEstadoFolio, String descripcion) {
        this.idEstadoFolio = idEstadoFolio;
        this.descripcion = descripcion;
    }

    public Integer getIdEstadoFolio() {
        return idEstadoFolio;
    }

    public void setIdEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "EstadoFolio{" + "idEstadoFolio=" + idEstadoFolio + ", descripcion=" + descripcion + '}';
    }

}
