package com.gs.baz.frq.modelo.negocio.firmas.init;

import com.gs.baz.frq.modelo.negocio.firmas.dao.FirmaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.firmas")
public class ConfigFirma {

    private final Logger logger = LogManager.getLogger();

    public ConfigFirma() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean(initMethod = "init")
    public FirmaDAOImpl firmaDAOImpl() {
        return new FirmaDAOImpl();
    }
}
