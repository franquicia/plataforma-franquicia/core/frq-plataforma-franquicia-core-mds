/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de Certificaciones pendientes por gerente", value = "VisitasPendientesGerentea")
public class CertificacionesGerentes {

    @JsonProperty(value = "certificacionesGerente")
    @ApiModelProperty(notes = "certificacionesGerente")
    private List<CertificacionesGerente> certificacionesGerente;

    public List<CertificacionesGerente> getCertificacionesGerente() {
        return certificacionesGerente;
    }

    public void setCertificacionesGerente(List<CertificacionesGerente> certificacionesGerente) {
        this.certificacionesGerente = certificacionesGerente;
    }

    @Override
    public String toString() {
        return "CertificacionesGerentes{" + "certificacionesGerente=" + certificacionesGerente + '}';
    }

}
