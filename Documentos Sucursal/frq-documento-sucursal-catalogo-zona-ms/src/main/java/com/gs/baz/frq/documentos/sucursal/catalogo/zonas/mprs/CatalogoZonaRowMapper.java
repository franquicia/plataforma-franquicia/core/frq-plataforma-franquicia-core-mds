/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto.CatalogoZonaBase;

/**
 *
 * @author carlos
 */
public class CatalogoZonaRowMapper implements RowMapper<CatalogoZonaBase> {

    private CatalogoZonaBase catalogoZonaBase;

    @Override
    public CatalogoZonaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        catalogoZonaBase = new CatalogoZonaBase();
        catalogoZonaBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        catalogoZonaBase.setIdTipoInformacion(rs.getInt("FIIDTIPOINFOR"));
        catalogoZonaBase.setEstatus(rs.getInt("FIESTADO"));
        catalogoZonaBase.setPeriodo(rs.getString("FCPERIODO"));
        return catalogoZonaBase;
    }
}
