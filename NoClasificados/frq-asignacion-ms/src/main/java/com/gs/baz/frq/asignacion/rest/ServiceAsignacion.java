/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.rest;

import com.gs.baz.frq.asignacion.dao.AsignacionDAOImpl;
import com.gs.baz.frq.asignacion.dto.AsignacionDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/asignacion")
@Component("FRQServiceAsignacion")
public class ServiceAsignacion {

    @Autowired
    private AsignacionDAOImpl asignacionDAO;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/ceco/{ceco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionDTO> getProyectosCeco(@PathVariable("ceco") String ceco) throws CustomException {
        if (ceco == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("proyectos"));
        }
        return (List<AsignacionDTO>) asignacionDAO.selectRowDTO(ceco);

    }

    @RequestMapping(value = "secure/post/fases", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionDTO> getFasesProyecto(@RequestBody AsignacionDTO fases) throws CustomException {

        if (fases.getCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fases"));
        }
        return (List<AsignacionDTO>) asignacionDAO.selectRows(fases);
    }

    @RequestMapping(value = "secure/get/asignacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionDTO> getAsignaciones(@RequestBody AsignacionDTO asignaciones) throws CustomException {

        if (asignaciones.getCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignaciones"));
        }
        return (List<AsignacionDTO>) asignacionDAO.selectRows1(asignaciones);
    }

    /*
//esta mal 
    @RequestMapping(value = "secure/get/{id_tipo_formato}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionDTO getTipoFormato(@PathVariable("id_tipo_formato") Long idTipoFormato) throws CustomException {
        return asignacionDAO.selectRow(idTipoFormato);
    }
     */
    @RequestMapping(value = "secure/post/altaAsignacion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaAsignacion(@RequestBody AsignacionDTO asignacion) throws CustomException {

        if (asignacion.getCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignacion"));
        }
        return asignacionDAO.insertRow(asignacion);
    }
//hacerlo asi 

    @RequestMapping(value = "secure/put/desbloqueaFase", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionDTO putDesbloqueaFase(@RequestBody AsignacionDTO desbloqueaFase) throws CustomException {
        if (desbloqueaFase.getCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ceco"));
        }
        if (desbloqueaFase.getProyecto() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("proyecto"));
        }
        return asignacionDAO.updateRow(desbloqueaFase);
    }

    @RequestMapping(value = "secure/put/desactivaAsignaciones", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionDTO deleteDesactivaAsignaciones(@RequestBody AsignacionDTO desactiva) throws CustomException {
        if (desactiva.getCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ceco"));
        }
        if (desactiva.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("usuario"));
        }
        return asignacionDAO.updateRows(desactiva);
    }

    @RequestMapping(value = "secure/delete/elimina/asignaciones", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaAsignacionesRows(@RequestBody AsignacionDTO elimina) throws CustomException {
        if (elimina.getCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ceco"));
        }
        if (elimina.getProyecto() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("proyecto"));
        }
        return asignacionDAO.eliminaRows(elimina);
    }

    @RequestMapping(value = "secure/post/obtiene/informacionCalendario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getDatosCalendario(@RequestBody AsignacionDTO asignaciones) throws CustomException {

        if (asignaciones.getFecha() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignaciones"));
        }
        return asignacionDAO.selectCalendarioRows(asignaciones);
    }

}
