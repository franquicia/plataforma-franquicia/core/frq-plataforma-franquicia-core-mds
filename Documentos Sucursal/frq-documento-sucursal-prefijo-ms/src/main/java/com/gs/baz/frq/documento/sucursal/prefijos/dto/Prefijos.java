/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de prefijo", value = "prefijos")
public class Prefijos {

    @JsonProperty(value = "prefijos")
    @ApiModelProperty(notes = "prefijos")
    private List<Prefijo> prefijos;

    public Prefijos(List<Prefijo> prefijos) {
        this.prefijos = prefijos;
    }

    public List<Prefijo> getPrefijos() {
        return prefijos;
    }

    public void setPrefijos(List<Prefijo> prefijos) {
        this.prefijos = prefijos;
    }
}
