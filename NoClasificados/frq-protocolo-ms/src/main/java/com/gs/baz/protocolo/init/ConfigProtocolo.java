package com.gs.baz.protocolo.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.protocolo.dao.ProtocoloDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.protocolo")
public class ConfigProtocolo {

    private final Logger logger = LogManager.getLogger();

    public ConfigProtocolo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ProtocoloDAOImpl protocoloDAOImpl() {
        return new ProtocoloDAOImpl();
    }
}
