/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaginacionDTO {

    @JsonProperty(value = "offset")
    private Integer offset;

    @JsonProperty(value = "countInPage")
    private Integer countInPage;

    @JsonProperty(value = "totalCount")
    private Integer totalCount;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getCountInPage() {
        return countInPage;
    }

    public void setCountInPage(Integer countInPage) {
        this.countInPage = countInPage;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

}
