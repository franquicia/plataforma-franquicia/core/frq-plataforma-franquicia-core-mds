/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.pf.init;

import com.gs.baz.frq.model.commons.Environment;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.lookup.StrLookup;

/**
 *
 * @author cescobarh
 */
@Plugin(name = "profile", category = StrLookup.CATEGORY)
public class profileLookup implements StrLookup {

    private final static Properties props = new Properties();

    public profileLookup() {
        String rootLogsPath;
        if (Environment.CURRENT.valueReal().equals(Environment.LOCAL)) {
            try {
                rootLogsPath = System.getProperty("user.home") + "/logs";
                Path path = Paths.get(rootLogsPath);
                if (!Files.exists(path)) {
                    Files.createDirectories(path);
                }
            } catch (IOException ex) {
                rootLogsPath = System.getProperty("user.home");
            }
            props.put("rootLogsPath", rootLogsPath);
        }
    }

    @Override
    public String lookup(String key) {
        return props.getProperty(key);
    }

    @Override
    public String lookup(LogEvent event, String key) {
        return props.getProperty(key);
    }

}
