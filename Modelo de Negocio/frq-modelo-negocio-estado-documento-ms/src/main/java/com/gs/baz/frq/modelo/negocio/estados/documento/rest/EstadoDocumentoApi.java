/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.estados.documento.dao.EstadoDocumentoDAOImpl;
import com.gs.baz.frq.modelo.negocio.estados.documento.dto.AltaEstadoDocumento;
import com.gs.baz.frq.modelo.negocio.estados.documento.dto.EstadoDocumentoBase;
import com.gs.baz.frq.modelo.negocio.estados.documento.dto.EstadosDocumento;
import com.gs.baz.frq.modelo.negocio.estados.documento.dto.ParametrosEstadoDocumento;
import com.gs.baz.frq.modelo.negocio.estados.documento.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "catalogos-estado-documento", value = "catalogos-estado-documento", description = "Gestión del catalogo de estado del documentos")
@RestController
@RequestMapping("/api-local/modelo-negocio/catalogos/estado-documento/v1")
public class EstadoDocumentoApi {

    @Autowired
    private EstadoDocumentoDAOImpl estadoDocumentoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idEstadoDocumento
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene estado documento", notes = "Obtiene un estado documento", nickname = "obtieneEstadoDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idEstadoDocumento}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstadoDocumentoBase obtieneEstadoDocumento(@ApiParam(name = "idEstadoDocumento", value = "Identificador del estado documento", example = "1", required = true) @PathVariable("idEstadoDocumento") Long idEstadoDocumento) throws CustomException, DataNotFoundException {
        EstadoDocumentoBase estadoDocumentoBase = estadoDocumentoDAOImpl.selectRow(idEstadoDocumento);
        if (estadoDocumentoBase == null) {
            throw new DataNotFoundException();
        }
        return estadoDocumentoBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene estado Documentos", notes = "Obtiene todos los estados del documento", nickname = "obtieneEstadosDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EstadosDocumento obtieneEstadoDocumentos() throws CustomException, DataNotFoundException {
        EstadosDocumento estadosDocumento = new EstadosDocumento(estadoDocumentoDAOImpl.selectAllRows());
        if (estadosDocumento.getEstadosDocumento() != null && estadosDocumento.getEstadosDocumento().isEmpty()) {
            throw new DataNotFoundException();
        }
        return estadosDocumento;
    }

    /**
     *
     * @param parametrosEstadoDocumento
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear estadoDocumento", notes = "Agrega un estado de documento", nickname = "creaEstadoDocumento")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaEstadoDocumento creaEstadoDocumento(@ApiParam(name = "ParametrosEstadoDocumento", value = "Paramentros para el alta del estado de documento", required = true) @RequestBody ParametrosEstadoDocumento parametrosEstadoDocumento) throws DataNotInsertedException {
        return estadoDocumentoDAOImpl.insertRow(parametrosEstadoDocumento);
    }

    /**
     *
     * @param parametrosEstadoDocumento
     * @param idEstadoDocumento
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar estadoDocumento", notes = "Actualiza un estado de documento", nickname = "actualizaEstadoDocumento")
    @RequestMapping(value = "/{idEstadoDocumento}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaEstadoDocumento(@ApiParam(name = "ParametrosEstadoDocumento", value = "Paramentros para la actualización del estado del documento", required = true) @RequestBody ParametrosEstadoDocumento parametrosEstadoDocumento, @ApiParam(name = "idEstadoDocumento", value = "Identificador del estado del documento", example = "1", required = true) @PathVariable("idEstadoDocumento") Integer idEstadoDocumento) throws DataNotUpdatedException {
        parametrosEstadoDocumento.setIdEstadoDocumento(idEstadoDocumento);
        estadoDocumentoDAOImpl.updateRow(parametrosEstadoDocumento);
        return new SinResultado();
    }

    /**
     *
     * @param idEstadoDocumento
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar estado del documento", notes = "Elimina un item de los estados del documentos", nickname = "eliminaEstadoDocumento")
    @RequestMapping(value = "/{idEstadoDocumento}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaEstadoDocumento(@ApiParam(name = "idEstadoDocumento", value = "Identificador del estado del documento", example = "1", required = true) @PathVariable("idEstadoDocumento") Integer idEstadoDocumento) throws DataNotDeletedException {
        estadoDocumentoDAOImpl.deleteRow(idEstadoDocumento);
        return new SinResultado();
    }

}
