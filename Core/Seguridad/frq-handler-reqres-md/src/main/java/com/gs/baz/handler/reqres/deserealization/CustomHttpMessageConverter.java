/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.handler.reqres.deserealization;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.gs.baz.handler.reqres.ex.AttributeMissingException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonInputMessage;
import org.springframework.lang.Nullable;

/**
 *
 * @author cescobarh
 */
public class CustomHttpMessageConverter extends MappingJackson2HttpMessageConverter {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Override
    public Object read(Type type, @Nullable Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        JavaType javaType = getJavaType(type, contextClass);
        return this.customReadJavaType(javaType, inputMessage);
    }

    private Object customReadJavaType(JavaType javaType, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        if (inputMessage instanceof MappingJacksonInputMessage) {
            Class<?> deserializationView = ((MappingJacksonInputMessage) inputMessage).getDeserializationView();
            if (deserializationView != null) {
                return objectMapper.readerWithView(deserializationView).forType(javaType).readValue(inputMessage.getBody());
            }
        }
        String nameApi = (String) httpServletRequest.getRequestURI();
        if (nameApi.contains("/api/")) {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
            Object out = objectMapper.readValue(inputMessage.getBody(), javaType);
            Field[] fields = out.getClass().getDeclaredFields();
            Method[] methods = out.getClass().getDeclaredMethods();
            List<String> requiredFields = new ArrayList<>();
            if (fields != null) {
                for (Field field : fields) {
                    JsonProperty jsonProperty = field.getAnnotation(JsonProperty.class);
                    if (jsonProperty != null) {
                        if (jsonProperty.required()) {
                            requiredFields.add(jsonProperty.value());
                        }
                    }
                }
            }
            if (methods != null) {
                for (Method method : methods) {
                    JsonProperty jsonProperty = method.getAnnotation(JsonProperty.class);
                    if (jsonProperty != null) {
                        if (jsonProperty.required()) {
                            requiredFields.add(jsonProperty.value());
                        }
                    }
                }
            }
            if (fields != null) {
                List<String> fieldsMissing = new ArrayList<>();
                for (Field field : fields) {
                    try {
                        String nameField = field.getName();
                        field.setAccessible(true);
                        Object value = field.get(out);
                        if (requiredFields.contains(nameField) && value == null) {
                            fieldsMissing.add(nameField);
                        }
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(CustomHttpMessageConverter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (fieldsMissing.size() > 0) {
                    throw new HttpMessageNotReadableException("Attributes missing exception", new AttributeMissingException("Attributes missing or values are required", fieldsMissing), inputMessage);
                }
            }
            return out;
        } else {
            return objectMapper.readValue(inputMessage.getBody(), javaType);
        }
    }

}
