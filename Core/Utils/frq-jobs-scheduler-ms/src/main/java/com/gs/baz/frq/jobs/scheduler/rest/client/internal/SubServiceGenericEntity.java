/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.jobs.scheduler.rest.client.internal;

import com.fasterxml.jackson.databind.JsonNode;
import com.gs.baz.frq.jobs.scheduler.rest.client.util.DefaultRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class SubServiceGenericEntity extends DefaultRestTemplate {

    public String getEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    if (responseStringBody.get("data") != null) {
                        return responseStringBody.get("data").toString();
                    } else {
                        throw new Exception("Entidad response body data attribute is null");
                    }
                } else {
                    throw new Exception("Entidad response code error");
                }
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public String updateEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.PUT, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    if (responseStringBody.get("data") != null) {
                        return responseStringBody.get("data").toString();
                    } else {
                        throw new Exception("Entidad response body data attribute is null");
                    }
                } else {
                    throw new Exception("Entidad response code error");
                }
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public String addEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.POST, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    if (responseStringBody.get("data") != null) {
                        return responseStringBody.get("data").toString();
                    } else {
                        throw new Exception("Entidad response body data attribute is null");
                    }
                } else {
                    return responseStringBody.get("error").toString();
                }
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public String deleteEntity() throws Exception {
        try {
            responseString = restTemplate.exchange(basePath, HttpMethod.DELETE, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    if (responseStringBody.get("data") != null) {
                        return responseStringBody.get("data").toString();
                    } else {
                        throw new Exception("Entidad response body data attribute is null");
                    }
                } else {
                    throw new Exception("Entidad response code error");
                }
            } else {
                throw new Exception("Entidad response code error from service");
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

}
