package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestInserta", description = "Objeto que contiene los datos a insertar en la tabla")
public class RequestActualiza {

    @JsonProperty(value = "idProyecto", required = true)
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1", position = -3)
    private Integer idProyecto;

    @JsonProperty(value = "centroCostos", required = true)
    @ApiModelProperty(notes = "Centro de costos", example = "1", position = -2)
    private String centroCostos;

    @JsonProperty(value = "folio", required = true)
    @ApiModelProperty(notes = "Numero de folio asignado", example = "1", position = -1)
    private Integer folio;

    @JsonProperty(value = "idFormato", required = true)
    @ApiModelProperty(notes = "Numero de Formato asignado", example = "1", position = -1)
    private Integer idFormato;

    @JsonProperty(value = "idDocumento", required = true)
    @ApiModelProperty(notes = "Numero de documento asignado", example = "1", position = -1)
    private Integer idDocumento;

    @JsonProperty(value = "idEstatus", required = true)
    @ApiModelProperty(notes = "Estado de la relacion", example = "1", position = -1)
    private Integer idEstatus;

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(Integer idEstatus) {
        this.idEstatus = idEstatus;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

}
