package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestConsulta", description = "Objeto que contiene los campos por los cual se realizara la consulta")
public class RequestConsulta {

    @JsonProperty(value = "idProyecto", required = false)
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1", position = -3)
    private String idProyecto;

    @JsonProperty(value = "centroCostos", required = false)
    @ApiModelProperty(notes = "Centro de costos", example = "1", position = -2)
    private String centroCostos;

    @JsonProperty(value = "folio", required = false)
    @ApiModelProperty(notes = "Numero de folio asignado", example = "1", position = -1)
    private String folio;

    @JsonProperty(value = "idFormato", required = false)
    @ApiModelProperty(notes = "Numero de Formato asignado", example = "1", position = -1)
    private String idFormato;

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(String idFormato) {
        this.idFormato = idFormato;
    }

}
