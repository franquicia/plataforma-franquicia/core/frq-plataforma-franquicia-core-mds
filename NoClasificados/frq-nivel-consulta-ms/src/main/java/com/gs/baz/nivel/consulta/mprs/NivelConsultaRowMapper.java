package com.gs.baz.nivel.consulta.mprs;

import com.gs.baz.nivel.consulta.dto.NivelConsultaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NivelConsultaRowMapper implements RowMapper<NivelConsultaDTO> {

    private NivelConsultaDTO nivelConsulta;

    @Override
    public NivelConsultaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        nivelConsulta = new NivelConsultaDTO();
        nivelConsulta.setIdNivelConsulta(((BigDecimal) rs.getObject("FIIDNIVELC")));
        nivelConsulta.setDescripcion(rs.getString("FCDESCRIPCION"));
        nivelConsulta.setIdStatus((BigDecimal) rs.getObject("FIIDESTATUS"));
        return nivelConsulta;
    }
}
