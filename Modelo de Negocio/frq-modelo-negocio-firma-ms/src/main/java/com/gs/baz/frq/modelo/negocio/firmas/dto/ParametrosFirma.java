/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosFirma {

    @JsonProperty(value = "idFirma")
    private transient Integer idFirma;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del Folio", example = "1")
    private transient Integer idFolio;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Numero de Empleado de la firma", example = "331952", required = true)
    @NotNull
    private Integer numeroEmpleado;

    @JsonProperty(value = "idJerarquia")
    @ApiModelProperty(notes = "Identificados de la jerarquia de la firma", example = "1", required = true)
    @NotNull
    private Integer idJerarquia;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "Orden de la firma", example = "1", required = true)
    @NotNull
    private Integer ordenFirma;

    public Integer getOrdenFirma() {
        return ordenFirma;
    }

    public void setOrdenFirma(Integer ordenFirma) {
        this.ordenFirma = ordenFirma;
    }

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdJerarquia() {
        return idJerarquia;
    }

    public void setIdJerarquia(Integer idJerarquia) {
        this.idJerarquia = idJerarquia;
    }

    @Override
    public String toString() {
        return "ParametrosFirma{" + "idFirma=" + idFirma + ", idFolio=" + idFolio + ", numeroEmpleado=" + numeroEmpleado + ", idJerarquia=" + idJerarquia + '}';
    }

}
