package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g010;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g010.DashboardCertificacionG010DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG010RowMapper implements RowMapper<DashboardCertificacionG010DTO> {

    private DashboardCertificacionG010DTO dashboardCertificacionG010DTO;

    @Override
    public DashboardCertificacionG010DTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionG010DTO = new DashboardCertificacionG010DTO();
        dashboardCertificacionG010DTO.setIdUsuario((BigDecimal) rs.getObject("NUMERO_EMPLEADO"));
        dashboardCertificacionG010DTO.setNombre(rs.getString("NOMBRE"));
        dashboardCertificacionG010DTO.setZona(rs.getString("ZONA"));
        return dashboardCertificacionG010DTO;
    }
}
