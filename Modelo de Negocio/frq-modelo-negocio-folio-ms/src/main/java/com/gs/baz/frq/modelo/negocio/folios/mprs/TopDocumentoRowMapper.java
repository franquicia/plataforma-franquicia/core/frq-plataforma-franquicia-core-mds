/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioTop;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.ModeloNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class TopDocumentoRowMapper implements RowMapper<BusquedaFolioTop> {

    private BusquedaFolioTop busquedaFolioTop;

    @Override
    public BusquedaFolioTop mapRow(ResultSet rs, int rowNum) throws SQLException {
        busquedaFolioTop = new BusquedaFolioTop();
        Documento documento = new Documento();
        documento.setTitulo(rs.getString("FCTITULO"));
        busquedaFolioTop.setDocumento(documento);
        busquedaFolioTop.setIdFolio(rs.getInt("FIIDFOLIO"));
        busquedaFolioTop.setCalificacion(rs.getInt("FICALIFICACION"));
        busquedaFolioTop.setFechaPublicacion(rs.getString("FDFECHAPUBLICADO"));
        busquedaFolioTop.setVistas(rs.getInt("FINUMVISUALIZACION"));
        String color = rs.getString("COLOR");
        String[] modelosNegocioSplit = color.split("\\|");
        List<ModeloNegocio> modelosNegocio = new ArrayList<>();
        for (String modeloNegocioSplit : modelosNegocioSplit) {
            modeloNegocioSplit = modeloNegocioSplit.replace("[", "");
            modeloNegocioSplit = modeloNegocioSplit.replace("]", "");
            String[] atributos = modeloNegocioSplit.split(",");
            Integer idArbol = new Integer(atributos[0]);
            String valueColor = atributos[1];
            modelosNegocio.add(new ModeloNegocio(idArbol, null, valueColor));
        }
        busquedaFolioTop.setModelosNegocio(modelosNegocio);
        return busquedaFolioTop;
    }
}
