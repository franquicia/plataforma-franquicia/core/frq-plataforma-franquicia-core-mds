/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.secciones.protocolo.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.secciones.protocolo.dao.SeccionesProtocoloDAOImpl;
import com.gs.baz.frq.secciones.protocolo.dto.SeccionesProtocoloDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/secciones/protocolo")
@Component("FRQServiceSeccionProtocolo")
public class ServiceSeccionesProtocolo {

    @Autowired
    private SeccionesProtocoloDAOImpl seccionDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeccionesProtocoloDTO postSeccion(@RequestBody SeccionesProtocoloDTO seccionProtocolo) throws CustomException {
        if (seccionProtocolo.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (seccionProtocolo.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (seccionProtocolo.getNumeroPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_pregunta"));
        }
        if (seccionProtocolo.getPorcentaje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("porcentaje"));
        }
        return seccionDAOImpl.insertRow(seccionProtocolo);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeccionesProtocoloDTO putSeccion(@RequestBody SeccionesProtocoloDTO seccionProtocolo) throws CustomException {
        if (seccionProtocolo.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (seccionProtocolo.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (seccionProtocolo.getNumeroPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_pregunta"));
        }
        if (seccionProtocolo.getPorcentaje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("porcentaje"));
        }
        if (seccionProtocolo.getVersion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("version"));
        }
        if (seccionProtocolo.getFechaVersion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fecha_version"));
        }
        return seccionDAOImpl.updateRow(seccionProtocolo);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SeccionesProtocoloDTO> getSeccion() throws CustomException {
        return seccionDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_protocolo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SeccionesProtocoloDTO> getSeccion(@PathVariable("id_protocolo") Long idProtoclo) throws CustomException {
        return seccionDAOImpl.selectRows(idProtoclo);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeccionesProtocoloDTO deleteSeccion(@RequestBody SeccionesProtocoloDTO seccionProtocolo) throws CustomException {
        if (seccionProtocolo.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (seccionProtocolo.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        return seccionDAOImpl.deleteRow(seccionProtocolo);
    }
}
