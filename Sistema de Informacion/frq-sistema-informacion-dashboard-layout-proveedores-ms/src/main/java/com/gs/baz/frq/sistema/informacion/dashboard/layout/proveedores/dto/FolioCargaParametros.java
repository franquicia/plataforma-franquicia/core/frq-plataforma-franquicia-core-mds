/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de los parámetros para la actualización del Folio de carga", value = "ActualizaFolioCarga")
public class FolioCargaParametros {


    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private Integer idFolio;
    
    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Identificador del centro de costos.", example = "920100", position = -2)
    private String centroCostos;
    
    @JsonProperty(value = "idEstado")
    @ApiModelProperty(notes = "Identificador del estado de carga", example = "3", position = -4)
    private Integer idEstado;
    
    @JsonProperty(value = "fechaFolio")
    @ApiModelProperty(notes = "Fecha del folio.", example = "02/12/2021", position = -5)
    private String fechaFolio;

    @JsonProperty(value = "auxiliarUno")
    @ApiModelProperty(notes = "", example = "", position = -7)
    private String auxiliarUno;
     
    @JsonProperty(value = "auxiliarDos")
    @ApiModelProperty(notes = "", example = "", position = -7)
    private String auxiliarDos;    

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getFechaFolio() {
        return fechaFolio;
    }

    public void setFechaFolio(String fechaFolio) {
        this.fechaFolio = fechaFolio;
    }

    public String getAuxiliarUno() {
        return auxiliarUno;
    }

    public void setAuxiliarUno(String auxiliarUno) {
        this.auxiliarUno = auxiliarUno;
    }

    public String getAuxiliarDos() {
        return auxiliarDos;
    }

    public void setAuxiliarDos(String auxiliarDos) {
        this.auxiliarDos = auxiliarDos;
    }

    @Override
    public String toString() {
        return "FolioCargaParametros{" + "idFolio=" + idFolio + ", centroCostos=" + centroCostos + ", idEstado=" + idEstado + ", fechaFolio=" + fechaFolio + ", auxiliarUno=" + auxiliarUno + ", auxiliarDos=" + auxiliarDos + '}';
    }

}
