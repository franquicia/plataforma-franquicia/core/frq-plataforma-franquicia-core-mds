package com.gs.baz.security.access.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.security.access.dao.AtributoSensibleDAOImpl;
import com.gs.baz.security.access.dao.SecurityAccessDAOImpl;
import com.gs.baz.security.access.filters.RequestSecurityFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.security.access")
public class ConfigSecurityAccess {

    private final Logger logger = LogManager.getLogger();

    public ConfigSecurityAccess() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SecurityAccessDAOImpl securityAccessDAOImpl() {
        return new SecurityAccessDAOImpl();
    }

    @Bean(initMethod = "init")
    public AtributoSensibleDAOImpl atributoSensibleDAOImpl() {
        return new AtributoSensibleDAOImpl();
    }

    @Bean
    public FilterRegistrationBean<RequestSecurityFilter> filterRegistrationBeanRequestSecurityFilter() {
        FilterRegistrationBean<RequestSecurityFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new RequestSecurityFilter());
        registrationBean.addUrlPatterns("/api/*", "/api-local/*");
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE - 10);
        return registrationBean;
    }
}
