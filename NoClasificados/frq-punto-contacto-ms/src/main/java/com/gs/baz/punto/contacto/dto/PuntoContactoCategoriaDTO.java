/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoCategoriaDTO {

    @JsonProperty(value = "id_categoria_punto_contacto")
    private Integer idCategoriaPuntoContacto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_punto_contacto_franquicia_modelo")
    private Integer idPuntoContactoFranquiciaModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_categoria")
    private Integer idCategoria;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public PuntoContactoCategoriaDTO() {
    }

    public PuntoContactoCategoriaDTO(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdCategoriaPuntoContacto() {
        return idCategoriaPuntoContacto;
    }

    public void setIdCategoriaPuntoContacto(BigDecimal idCategoriaPuntoContacto) {
        this.idCategoriaPuntoContacto = (idCategoriaPuntoContacto == null ? null : idCategoriaPuntoContacto.intValue());
    }

    public Integer getIdPuntoContactoFranquiciaModelo() {
        return idPuntoContactoFranquiciaModelo;
    }

    public void setIdPuntoContactoFranquiciaModelo(BigDecimal idPuntoContactoFranquiciaModelo) {
        this.idPuntoContactoFranquiciaModelo = (idPuntoContactoFranquiciaModelo == null ? null : idPuntoContactoFranquiciaModelo.intValue());
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(BigDecimal idCategoria) {
        this.idCategoria = (idCategoria == null ? null : idCategoria.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "PuntoContactoCategoriaDTO{" + "idCategoriaPuntoContacto=" + idCategoriaPuntoContacto + ", idPuntoContactoFranquiciaModelo=" + idPuntoContactoFranquiciaModelo + ", idCategoria=" + idCategoria + ", fecha=" + fecha + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
