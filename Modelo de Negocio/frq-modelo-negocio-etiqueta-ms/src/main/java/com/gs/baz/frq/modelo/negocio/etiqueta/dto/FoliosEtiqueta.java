/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de folios por etiquetas del modelo de negocio", value = "FoliosEtiqueta")
public class FoliosEtiqueta {

    @JsonProperty(value = "foliosEtiqueta")
    @ApiModelProperty(notes = "folioEtiqueta")
    private List<FolioEtiqueta> foliosEtiqueta;

    public FoliosEtiqueta(List<FolioEtiqueta> foliosEtiqueta) {
        this.foliosEtiqueta = foliosEtiqueta;
    }

    public List<FolioEtiqueta> getFolioEtiqueta() {
        return foliosEtiqueta;
    }

    public void setFolioEtiqueta(List<FolioEtiqueta> foliosEtiqueta) {
        this.foliosEtiqueta = foliosEtiqueta;
    }
}
