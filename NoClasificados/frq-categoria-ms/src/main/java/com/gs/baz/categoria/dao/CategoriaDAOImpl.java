package com.gs.baz.categoria.dao;

import com.gs.baz.categoria.dao.util.GenericDAO;
import com.gs.baz.categoria.dto.CategoriaDTO;
import com.gs.baz.categoria.dto.NegocioDTO;
import com.gs.baz.categoria.mprs.CategoriaRowMapper;
import com.gs.baz.categoria.mprs.NegocioRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cescobarh
 */
public class CategoriaDAOImpl extends DefaultDAO implements GenericDAO<CategoriaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelectNegocio;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcSelectByNameNeg;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {
        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINCATEGORIA");
        jdbcInsert.withProcedureName("SP_INS_CATEGO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINCATEGORIA");
        jdbcUpdate.withProcedureName("SP_ACT_CATEGO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINCATEGORIA");
        jdbcDelete.withProcedureName("SP_DEL_CATEGO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINCATEGORIA");
        jdbcSelect.withProcedureName("SP_SEL_CATEGO");
        jdbcSelect.returningResultSet("RCL_INFO", new CategoriaRowMapper());

        jdbcSelectNegocio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectNegocio.withSchemaName(schema);
        jdbcSelectNegocio.withCatalogName("PAADMINCATEGORIA");
        jdbcSelectNegocio.withProcedureName("SP_SEL_CATEGO");
        jdbcSelectNegocio.returningResultSet("RCL_INFO", new CategoriaRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMINCATEGORIA");
        jdbcSelectByName.withProcedureName("SP_CATEGO_NOMBRE");
        jdbcSelectByName.returningResultSet("RCL_INFO", new CategoriaRowMapper());

        jdbcSelectByNameNeg = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByNameNeg.withSchemaName(schema);
        jdbcSelectByNameNeg.withCatalogName("PAADMINNEGOCIO");
        jdbcSelectByNameNeg.withProcedureName("SP_NEGOCIO_NOMBRE");
        jdbcSelectByNameNeg.returningResultSet("RCL_INFO", new NegocioRowMapper());
    }

    @Override
    public CategoriaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCATEG", entityID);
        mapSqlParameterSource.addValue("PA_FIIDNEG", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<CategoriaDTO> data = (List<CategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<CategoriaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCATEG", null);
        mapSqlParameterSource.addValue("PA_FIIDNEG", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<CategoriaDTO> data = (List<CategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public List<CategoriaDTO> selectRowsPorNegocio(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDNEG", entityID);
        mapSqlParameterSource.addValue("PA_FIIDCATEG", null);
        Map<String, Object> out = jdbcSelectNegocio.execute(mapSqlParameterSource);
        List<CategoriaDTO> data = (List<CategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Transactional(rollbackFor = {CustomException.class})
    public CategoriaDTO cudRow(CategoriaDTO entityDTO) throws CustomException {
        try {
            //se busca si existe la categoria con nombre categoria geek
            List<CategoriaDTO> categoriaGek = this.selectRowByName("CATEGORIA GEK");
            if (categoriaGek == null || categoriaGek.isEmpty()) {
                //se busca el negocio de gek

                List<NegocioDTO> existente = this.selectRowsByNameNeg("GEK");
                if (existente != null && !existente.isEmpty()) {
                    //se inserta la categoria de geek
                    CategoriaDTO catGek = new CategoriaDTO();
                    catGek.setDescripcion("CATEGORIA GEK");
                    catGek.setIdStatus(BigDecimal.ONE);
                    catGek.setIdNegocio(BigDecimal.valueOf(existente.get(0).getIdNegocio()));
                    catGek = this.insertRow(catGek);
                }
            }
            entityDTO = this.insertRow(entityDTO);

        } catch (Exception ex) {
            logger.log(Level.DEBUG, ex);
            throw new CustomException(ex);
        }
        return entityDTO;
    }

    @Override
    public CategoriaDTO insertRow(CategoriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDESTATUS", entityDTO.getIdStatus());
            mapSqlParameterSource.addValue("PA_FIIDNEG", entityDTO.getIdNegocio());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdCategoria((BigDecimal) out.get("PA_FIIDCATEG"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  categoria"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  categoria"), ex);
        }
    }

    @Override
    public CategoriaDTO updateRow(CategoriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCATEG", entityDTO.getIdCategoria());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIIDESTATUS", entityDTO.getIdStatus());
            mapSqlParameterSource.addValue("PA_FIIDNEG", entityDTO.getIdNegocio());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of categoria"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of categoria"), ex);
        }
    }

    @Override
    public CategoriaDTO deleteRow(CategoriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCATEG", entityDTO.getIdCategoria());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete rows of categoria"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception Error to delete rows of categoria"), ex);
        }
    }

    @Override
    public Boolean deleteRow(Long entityID) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCATEG", entityID);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                return success;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete rows of categoria"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception Error to delete rows of categoria"), ex);
        }
    }

    public List<CategoriaDTO> selectRowByName(String name) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCDESCRIPCION", name);
        Map<String, Object> out = jdbcSelectByName.execute(mapSqlParameterSource);
        List<CategoriaDTO> data = (List<CategoriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<NegocioDTO> selectRowsByNameNeg(String name) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCDESCRIPCION", name);
        Map<String, Object> out = jdbcSelectByNameNeg.execute(mapSqlParameterSource);
        List<NegocioDTO> data = (List<NegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
