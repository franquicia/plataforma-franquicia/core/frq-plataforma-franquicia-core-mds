/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocio.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del catalogo de Zona", value = "CatalogoZonaBase")
public class NegocioZona extends NegocioZonaBase {

    @JsonProperty(value = "idCatalogoZona")
    @ApiModelProperty(notes = "Identificador de catalogo zona", example = "1", position = -2)
    private Integer idCatalogoZona;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1", position = -1)
    private Integer idNegocio;

    @JsonProperty(value = "estatus")
    @ApiModelProperty(notes = "Descripcion del estatus", example = "1")
    private Integer estatus;

    public Integer getIdTipoInformacion() {
        return idCatalogoZona;
    }

    public void setIdTipoInformacion(Integer idCatalogoZona) {
        this.idCatalogoZona = idCatalogoZona;
    }

    public Integer getIdCatalogoZona() {
        return idCatalogoZona;
    }

    public void setIdCatalogoZona(Integer idCatalogoZona) {
        this.idCatalogoZona = idCatalogoZona;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

}
