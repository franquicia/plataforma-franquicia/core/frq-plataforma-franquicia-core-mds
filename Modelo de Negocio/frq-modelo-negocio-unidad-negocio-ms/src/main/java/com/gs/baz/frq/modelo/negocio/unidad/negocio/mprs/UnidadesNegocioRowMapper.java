/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.mprs;

import com.gs.baz.frq.modelo.negocio.unidad.negocio.dto.UnidadNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UnidadesNegocioRowMapper implements RowMapper<UnidadNegocio> {

    private UnidadNegocio pais;

    @Override
    public UnidadNegocio mapRow(ResultSet rs, int rowNum) throws SQLException {
        pais = new UnidadNegocio();
        pais.setIdUnidadNegocio(rs.getInt("FIIDUNIDADNEGOCIO"));
        pais.setDescripcion(rs.getString("FCDESCRIPCION"));
        pais.setAcronimo(rs.getString("FCACRONIMO"));
        return pais;
    }
}
