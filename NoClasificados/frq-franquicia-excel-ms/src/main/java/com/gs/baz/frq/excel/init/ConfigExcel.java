package com.gs.baz.frq.excel.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.excel.dao.ExcelDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.excel")
public class ConfigExcel {

    private final Logger logger = LogManager.getLogger();

    public ConfigExcel() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqExcelDAOImpl")
    public ExcelDAOImpl excelDAOImpl() {
        return new ExcelDAOImpl();
    }
}
