/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.AltaComentarioFirma;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ComentrioFirma;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ComentrioFirmaBase;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.FirmaBase;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.FolioBase;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ParametrosComentrioFirma;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ResumenDocumento;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs.ComentarioFirmaRowMapper;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs.ComentariosFirmaRowMapper;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs.FirmaRowMapper;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs.FolioRowMapper;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs.ModeloNegocioUsuariosRowMapper;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs.ResumenDocumentoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class ComentarioFirmaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectFirma;
    private DefaultJdbcCall jdbcSelectFolio;
    private DefaultJdbcCall jdbcSelectEmpleado;
    private DefaultJdbcCall jdbcSelectResumenDocumento;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNCOMFIRM";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNCOMFIRMAS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNCOMFIRMAS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNCOMFIRMAS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETMNCOMFIRMAS");
        jdbcSelect.returningResultSet("PA_CDATOS", new ComentarioFirmaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETMNCOMFIRMAS");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new ComentariosFirmaRowMapper());

        jdbcSelectFirma = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFirma.withSchemaName(schema);
        jdbcSelectFirma.withCatalogName("PAADMMNFIRMAS");
        jdbcSelectFirma.withProcedureName("SPGETFIRMASXFOL");
        jdbcSelectFirma.returningResultSet("PA_CDATOS", new FirmaRowMapper());

        jdbcSelectFolio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFolio.withSchemaName(schema);
        jdbcSelectFolio.withCatalogName("PAADMMNFOLIO");
        jdbcSelectFolio.withProcedureName("SPGETMNFOLIO");
        jdbcSelectFolio.returningResultSet("PA_CDATOS", new FolioRowMapper());

        jdbcSelectEmpleado = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectEmpleado.withSchemaName(schema);
        jdbcSelectEmpleado.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectEmpleado.withProcedureName("SPGETUSUARIOS");
        jdbcSelectEmpleado.returningResultSet("PA_CDATOS", new ModeloNegocioUsuariosRowMapper());

        jdbcSelectResumenDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectResumenDocumento.withSchemaName(schema);
        jdbcSelectResumenDocumento.withCatalogName("PAADMMNDOCUMEN");
        jdbcSelectResumenDocumento.withProcedureName("SPRECUPERADOCTO");
        jdbcSelectResumenDocumento.returningResultSet("PA_CDATOS", new ResumenDocumentoRowMapper());
    }

    public ComentrioFirmaBase selectRow(Long idComentario) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCOMENTARIO", idComentario);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ComentrioFirmaBase> data = (List<ComentrioFirmaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<ComentrioFirma> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCOMENTARIO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<ComentrioFirma>) out.get("PA_CDATOS");
    }

    public AltaComentarioFirma insertRow(ParametrosComentrioFirma entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO.getIdFirma());
            mapSqlParameterSource.addValue("PA_FCCOMENTARIO", entityDTO.getComentario());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESFIIDCOMENTARIO");
                return new AltaComentarioFirma(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosComentrioFirma entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCOMENTARIO", entityDTO.getIdComentario());
            mapSqlParameterSource.addValue("PA_FIIDFIRMA", entityDTO.getIdFirma());
            mapSqlParameterSource.addValue("PA_FCCOMENTARIO", entityDTO.getComentario());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCOMENTARIO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public FirmaBase selectFirmaRow(Long idFolio, Long idFirma) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        mapSqlParameterSource.addValue("PA_FIIDFIRMA", idFirma);
        Map<String, Object> out = jdbcSelectFirma.execute(mapSqlParameterSource);
        List<FirmaBase> data = (List<FirmaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public FolioBase selectFolioRow(Long idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        Map<String, Object> out = jdbcSelectFolio.execute(mapSqlParameterSource);
        List<FolioBase> data = (List<FolioBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public Empleado selectEmpleadoRows(String usuario) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_USUARIO", usuario);
        Map<String, Object> out = jdbcSelectEmpleado.execute(mapSqlParameterSource);
        List<Empleado> data = (List<Empleado>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public ResumenDocumento selectResumenDocumento(Long idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FOLIO", idFolio);
        Map<String, Object> out = jdbcSelectResumenDocumento.execute(mapSqlParameterSource);
        List<ResumenDocumento> data = (List<ResumenDocumento>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }
}
