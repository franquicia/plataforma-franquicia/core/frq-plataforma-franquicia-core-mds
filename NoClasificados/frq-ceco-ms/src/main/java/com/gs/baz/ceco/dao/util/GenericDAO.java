/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ceco.dao.util;

import com.gs.baz.ceco.dto.CecoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param entityID
     * @return
     * @throws CustomException
     */
    public dto selectRow(Long entityID) throws CustomException;

    /**
     *
     * @param ceco
     * @param nombre
     * @return
     * @throws CustomException
     */
    public List<dto> selectRows(String ceco, String nombre) throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<dto> selectRowsRegion() throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<dto> selectRowsZona() throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<dto> selectRowsTerritorio() throws CustomException;

    List<CecoDTO> selectRowsGen(CecoDTO cecos) throws CustomException;

}
