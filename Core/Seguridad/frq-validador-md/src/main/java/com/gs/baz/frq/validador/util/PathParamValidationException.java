/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

/**
 *
 * @author cescobarh
 */
public class PathParamValidationException extends Exception {

    private String name;
    private String originalType;
    private String value;

    public PathParamValidationException() {
        super();
    }

    public PathParamValidationException(String message) {
        super(message);
    }

    public PathParamValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PathParamValidationException(Throwable cause) {
        super(cause);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalType() {
        return originalType;
    }

    public void setOriginalType(String originalType) {
        this.originalType = originalType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
