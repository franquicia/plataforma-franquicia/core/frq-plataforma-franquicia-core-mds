package com.gs.baz.frq.dashboard.mprs.graficos.g013;

import com.gs.baz.frq.dashboard.dto.graficos.g013.CumplimientoPuntajeNacionalDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoPuntajeNacionalRowMapper implements RowMapper<CumplimientoPuntajeNacionalDTO> {

    private CumplimientoPuntajeNacionalDTO cumplimientoPuntajeNacionalDTO;

    @Override
    public CumplimientoPuntajeNacionalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoPuntajeNacionalDTO = new CumplimientoPuntajeNacionalDTO();
        cumplimientoPuntajeNacionalDTO.setIdCeco(rs.getString("FCID_TERRITORIO"));
        cumplimientoPuntajeNacionalDTO.setDescripcionCeco(rs.getString("FCTERRITORIO"));
        cumplimientoPuntajeNacionalDTO.setTotales((BigDecimal) rs.getObject("PCTOTALES"));
        cumplimientoPuntajeNacionalDTO.setCumple((BigDecimal) rs.getObject("CUMPLE"));
        cumplimientoPuntajeNacionalDTO.setPorcentaje((BigDecimal) rs.getObject("CUMPLEPORCENTAJE"));
        cumplimientoPuntajeNacionalDTO.setIdRango((BigDecimal) rs.getObject("FIRANGO_ID"));
        return cumplimientoPuntajeNacionalDTO;
    }
}
