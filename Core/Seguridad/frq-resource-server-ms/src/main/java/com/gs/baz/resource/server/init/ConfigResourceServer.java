package com.gs.baz.resource.server.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.resource.server.oauth.RestAccessDeniedHandler;
import com.gs.baz.resource.server.oauth.RestAuthenticationEntryPoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.resource.server")
@RefreshScope
@EnableResourceServer
public class ConfigResourceServer extends ResourceServerConfigurerAdapter {

    private final Logger logger = LogManager.getLogger();

    @Value("${config.security.oauth.jwt.key}")
    private String jwtKey;

    @Autowired
    RestAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    RestAccessDeniedHandler restAccessDeniedHandler;

    public ConfigResourceServer() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(tokenStore());
        resources.authenticationEntryPoint(authenticationEntryPoint).accessDeniedHandler(restAccessDeniedHandler);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/service/security/oauth/**").permitAll();
        http.authorizeRequests().antMatchers("/api-local/security/oauth/**").permitAll();
        http.authorizeRequests().requestMatchers(req -> req.getRequestURI().contains("/secure/")).authenticated();
        http.authorizeRequests().requestMatchers(req -> {
            if (req.getRequestURI().contains("/api-local/")) {
                switch (Environment.CURRENT.valueReal()) {
                    case PRODUCTION:
                        return true;
                    case QUALITY:
                        return true;
                    default:
                        return false;
                }
            } else {
                return false;
            }
        }).authenticated();
    }

    @Bean
    public JwtTokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
        tokenConverter.setSigningKey(jwtKey);
        return tokenConverter;
    }
}
