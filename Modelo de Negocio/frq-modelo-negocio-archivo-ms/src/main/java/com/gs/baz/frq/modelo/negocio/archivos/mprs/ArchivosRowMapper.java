/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.mprs;

import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoVirtual;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ArchivosRowMapper implements RowMapper<ArchivoVirtual> {

    private ArchivoVirtual archivo;

    @Override
    public ArchivoVirtual mapRow(ResultSet rs, int rowNum) throws SQLException {
        archivo = new ArchivoVirtual();
        archivo.setIdArchivo(rs.getInt("FIIDARCHIVO"));
        archivo.setIdFolio(rs.getInt("FIIDFOLIO"));
        archivo.setUid(rs.getLong("FCRUTA"));
        archivo.setDescripcion(rs.getString("FCDESCRIPCION"));
        archivo.setNombre(rs.getString("FCNOMBRE"));
        archivo.setExtension(rs.getString("FCEXTENSION"));
        archivo.setTipo(rs.getInt("FITIPO"));
        archivo.setIndice(rs.getInt("FIINDICE"));
        archivo.setPeso(rs.getInt("FISIZE"));
        archivo.setTipoContenido(rs.getString("FCCONTENTTYPE"));
        return archivo;
    }
}
