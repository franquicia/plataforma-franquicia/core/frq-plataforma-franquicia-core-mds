/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.acceso.client.FolioClient;
import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Folio;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.AltaDocumentoGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.AltaGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.AltaUsuarioGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.DocumentoGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.Grupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.GrupoBase;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.UsuarioGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.ParametrosDocumentoGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.ParametrosGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.ParametrosUsuarioGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.DocumentoGrupoRowMapper;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.GrupoRowMapper;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.GruposRowMapper;
import com.gs.baz.frq.modelo.negocio.acceso.mprs.UsuarioGrupoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class GrupoDAOImpl extends DefaultDAO {

    @Autowired
    private FolioClient folioClient;

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;

    private DefaultJdbcCall jdbcSelectAllUsuarios;
    private DefaultJdbcCall jdbcSelectAllUsuariosDisponibles;
    private DefaultJdbcCall jdbcInsertUsuario;
    private DefaultJdbcCall jdbcDeleteUsuario;
    private DefaultJdbcCall jdbcSelectAllDocumentos;
    private DefaultJdbcCall jdbcSelectAllDocumentosDisponibles;
    private DefaultJdbcCall jdbcInsertDocumento;
    private DefaultJdbcCall jdbcDeleteDocumento;

    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private final String schema = "MODFRANQ";

    public void init() {
        String catalogo = "PAADMDMNGRUPOS";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSDMNGRUPOS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTDMNGRUPOS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELDMNGRUPOS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETDMNGRUPOS");
        jdbcSelect.returningResultSet("PA_CDATOS", new GrupoRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETDMNGRUPOS");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new GruposRowMapper());

        jdbcSelectAllUsuarios = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllUsuarios.withSchemaName(schema);
        jdbcSelectAllUsuarios.withCatalogName("PAADMDMNGPOUSU");
        jdbcSelectAllUsuarios.withProcedureName("SPGETUSUAXGPO");
        jdbcSelectAllUsuarios.returningResultSet("PA_CDATOS", new UsuarioGrupoRowMapper());

        jdbcSelectAllUsuariosDisponibles = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllUsuariosDisponibles.withSchemaName(schema);
        jdbcSelectAllUsuariosDisponibles.withCatalogName("PAADMDMNGPOUSU");
        jdbcSelectAllUsuariosDisponibles.withProcedureName("SPGETUSUANOTIENENGPO");
        jdbcSelectAllUsuariosDisponibles.returningResultSet("PA_CDATOS", new UsuarioGrupoRowMapper());

        jdbcInsertUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertUsuario.withSchemaName(schema);
        jdbcInsertUsuario.withCatalogName("PAADMDMNGPOUSU");
        jdbcInsertUsuario.withProcedureName("SPINSDMNGPOUSUA");

        jdbcDeleteUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteUsuario.withSchemaName(schema);
        jdbcDeleteUsuario.withCatalogName("PAADMDMNGPOUSU");
        jdbcDeleteUsuario.withProcedureName("SPDELDMNGPOUSUA");

        jdbcSelectAllDocumentos = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDocumentos.withSchemaName(schema);
        jdbcSelectAllDocumentos.withCatalogName("PAADMDMNGPODOC");
        jdbcSelectAllDocumentos.withProcedureName("SPGETDMNGPODOCTO");
        jdbcSelectAllDocumentos.returningResultSet("PA_CDATOS", new DocumentoGrupoRowMapper());

        jdbcSelectAllDocumentosDisponibles = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllDocumentosDisponibles.withSchemaName(schema);
        jdbcSelectAllDocumentosDisponibles.withCatalogName("PAADMDMNGPODOC");
        jdbcSelectAllDocumentosDisponibles.withProcedureName("SPGETDOCTONOTIENEGPO");
        jdbcSelectAllDocumentosDisponibles.returningResultSet("PA_CDATOS", new DocumentoGrupoRowMapper());

        jdbcInsertDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertDocumento.withSchemaName(schema);
        jdbcInsertDocumento.withCatalogName("PAADMDMNGPODOC");
        jdbcInsertDocumento.withProcedureName("SPINSDMNGPODOCTO");

        jdbcDeleteDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteDocumento.withSchemaName(schema);
        jdbcDeleteDocumento.withCatalogName("PAADMDMNGPODOC");
        jdbcDeleteDocumento.withProcedureName("SPDELDMNGPODOCTO");
    }

    public GrupoBase selectRow(Long idGrupo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRUPO", idGrupo);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<GrupoBase> data = (List<GrupoBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Grupo> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRUPO", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Grupo>) out.get("PA_CDATOS");
    }

    public AltaGrupo insertRow(ParametrosGrupo entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDGRUPO");
                return new AltaGrupo(dato.intValue());
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
    }

    public void updateRow(ParametrosGrupo entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRUPO", entityDTO.getIdGrupo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw new DataNotUpdatedException(ex);
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRUPO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<UsuarioGrupo> selectAllUsuarios(Integer idGrupo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRUPO", idGrupo);
        Map<String, Object> out = jdbcSelectAllUsuarios.execute(mapSqlParameterSource);
        return (List<UsuarioGrupo>) out.get("PA_CDATOS");
    }

    public List<UsuarioGrupo> selectAllUsuariosDisponibles(Integer idGrupo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRUPO", idGrupo);
        Map<String, Object> out = jdbcSelectAllUsuariosDisponibles.execute(mapSqlParameterSource);
        return (List<UsuarioGrupo>) out.get("PA_CDATOS");
    }

    public AltaUsuarioGrupo insertGrupoUsuario(ParametrosUsuarioGrupo entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRUPO", entityDTO.getIdGrupo());
            mapSqlParameterSource.addValue("PA_FIIDUSUARIO", entityDTO.getNumeroEmpleado());
            Map<String, Object> out = jdbcInsertUsuario.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDGPOUSUA");
                return new AltaUsuarioGrupo(dato.intValue());
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
    }

    public void deleteUsuario(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGPOUSUA", entityDTO);
            Map<String, Object> out = jdbcDeleteUsuario.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<DocumentoGrupo> selectAllDocumentos(HttpHeaders headers, Integer idGrupo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRUPO", idGrupo);
        Map<String, Object> out = jdbcSelectAllDocumentos.execute(mapSqlParameterSource);
        List<DocumentoGrupo> list = (List<DocumentoGrupo>) out.get("PA_CDATOS");
        for (DocumentoGrupo documento : list) {
            try {
                Folio folio = folioClient.getFolio(headers, documento.getFolio().getIdFolio());
                documento.setFolio(folio);
            } catch (Exception ex) {
                throw new CustomException(ex);
            }
        }
        return list;
    }

    public List<DocumentoGrupo> selectAllDocumentosDisponibles(HttpHeaders headers, Integer idGrupo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRUPO", idGrupo);
        Map<String, Object> out = jdbcSelectAllDocumentosDisponibles.execute(mapSqlParameterSource);
        List<DocumentoGrupo> list = (List<DocumentoGrupo>) out.get("PA_CDATOS");
        for (DocumentoGrupo documento : list) {
            try {
                Folio folio = folioClient.getFolio(headers, documento.getFolio().getIdFolio());
                documento.setFolio(folio);
            } catch (Exception ex) {
                throw new CustomException(ex);
            }
        }
        return list;
    }

    public AltaDocumentoGrupo insertDocumentoGrupo(ParametrosDocumentoGrupo entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRUPO", entityDTO.getIdGrupo());
            mapSqlParameterSource.addValue("PA_FIIDDOCUMENTO", entityDTO.getIdDocumento());
            Map<String, Object> out = jdbcInsertDocumento.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDGPODOCTO");
                return new AltaDocumentoGrupo(dato.intValue());
            }
        } catch (Exception ex) {
            throw new DataNotInsertedException(ex);
        }
    }

    public void deleteDocumento(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGPODOCTO", entityDTO);
            Map<String, Object> out = jdbcDeleteDocumento.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
