/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g012;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class IncumplimientoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo")
    private Integer conteo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pregunta")
    private String pregunta;

    public IncumplimientoDTO() {
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(BigDecimal conteo) {
        this.conteo = (conteo == null ? null : conteo.intValue());
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public String toString() {
        return "TopIncumplimientoDTO{" + "idNegocio=" + idNegocio + ", conteo=" + conteo + ", pregunta=" + pregunta + '}';
    }

}
