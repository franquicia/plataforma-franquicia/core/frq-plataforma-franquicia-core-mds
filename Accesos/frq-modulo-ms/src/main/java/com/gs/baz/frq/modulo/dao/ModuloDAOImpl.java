/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modulo.dao;

/**
 *
 * @author jfernandor
 */
import com.gs.baz.frq.modulo.dto.ModuloDTO;
import com.gs.baz.frq.modulo.mprs.ModuloRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class ModuloDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAMOD_ACCE");
        jdbcInsert.withProcedureName("SPINSAMOD_ACCESO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAMOD_ACCE");
        jdbcSelect.withProcedureName("SPGETAMOD_ACCESO");
        jdbcSelect.returningResultSet("PA_CDATOS", new ModuloRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAADMAMOD_ACCE");
        jdbcSelectAll.withProcedureName("SPGETAMOD_ACCESO_ALL");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new ModuloRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAMOD_ACCE");
        jdbcUpdate.withProcedureName("SPACTAMOD_ACCESO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAMOD_ACCE");
        jdbcDelete.withProcedureName("SPDELAMOD_ACCESO");
    }

    public ModuloDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_MODULO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ModuloDTO> data = (List<ModuloDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<ModuloDTO> selectRows() throws CustomException {
        List<ModuloDTO> lista;
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_MODULO", null);
        Map<String, Object> out = jdbcSelect.execute(in);
        lista = (List<ModuloDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public List<ModuloDTO> selectRowsAll() throws CustomException {
        List<ModuloDTO> lista;
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_MODULO", null);
        Map<String, Object> out = jdbcSelectAll.execute(in);
        lista = (List<ModuloDTO>) out.get("PA_CDATOS");
        return lista;
    }

    @Transactional(rollbackFor = {CustomException.class})
    public List<ModuloDTO> insertRow(List<ModuloDTO> entitiesDTO) throws CustomException {
        try {
            for (ModuloDTO entityDTO : entitiesDTO) {
                entityDTO = this.insertRow(entityDTO);
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Modulo Flujo"), ex);
        }
        return entitiesDTO;
    }

    public ModuloDTO insertRow(ModuloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_MODULO", entityDTO.getIdModulo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FIESWEB", entityDTO.getEsWeb());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdModulo((BigDecimal) out.get("PA_NRESEJECUCION"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Modulo Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Modulo Flujo"), ex);
        }
    }

    public ModuloDTO updateRow(ModuloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_MODULO", entityDTO.getIdModulo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FIESWEB", entityDTO.getEsWeb());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Modulo Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Modulo Flujo"), ex);
        }
    }

    public ModuloDTO deleteRow(ModuloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_MODULO", entityDTO.getIdModulo());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Modulo Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Modulo Flujo"), ex);
        }
    }

}
