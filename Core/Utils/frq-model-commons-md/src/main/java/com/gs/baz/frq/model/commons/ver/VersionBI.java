/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons.ver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author cescobarh
 */
public class VersionBI {

    private final List<String> nodes = new ArrayList<>();
    private String url_service_version = "http://@host:8080/version/version/info";
    private final ObjectMapper objectMapper = new ObjectMapper();
    final private transient static Logger logger = LogManager.getLogger();

    public VersionBI() {
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public VersionDTO getVersion() {
        Version version = new Version();
        return version.getVersion();
    }

    public String infoNodes() {
        String jsonOut = null;
        String response;
        ObjectNode nodesContainer = objectMapper.createObjectNode();
        ArrayNode arrayNodes = objectMapper.createArrayNode();
        ObjectNode nodeVersion;
        JsonNode json;
        List<String> versions = new ArrayList<>();
        for (String node : nodes) {
            nodeVersion = objectMapper.createObjectNode();
            String ip = node;
            try {
                url_service_version = url_service_version.replace("@host", ip);
                response = this.getVersionNode(url_service_version);
                if (!response.equals("")) {
                    json = objectMapper.readTree(response);
                    versions.add(json.get("Version").asText());
                } else {
                    json = objectMapper.createObjectNode();
                }
                nodeVersion.put("Node", ip);
                nodeVersion.set("NodeVersion", json);
            } catch (IOException ex) {
                logger.info("Algo paso al convertir la respuesta de la version del Nodo " + ip + " " + ex.getMessage());
            }
            arrayNodes.add(nodeVersion);
        }
        nodesContainer.set("Nodes", arrayNodes);
        Map<String, Long> result = versions.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        nodesContainer.put("UnpairingVersions", (result.isEmpty() ? null : (result.size() > 1)));
        try {
            jsonOut = objectMapper.writeValueAsString(nodesContainer);
        } catch (JsonProcessingException ex) {
            logger.info("No fue posible procesar la respuesta final de la version de los nodos " + nodes.toString() + ex.getMessage());
        }
        return jsonOut;
    }

    private String getVersionNode(String url_service) {
        String jsonOut = "";
        try {
            URL url = new URL(url_service);
            BufferedReader rd;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "text/html");
            conn.setConnectTimeout(3000);
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                jsonOut += line;
            }
        } catch (Exception e) {
            logger.info("No se obtuvo version " + e.getMessage());
        }
        return jsonOut;
    }

}
