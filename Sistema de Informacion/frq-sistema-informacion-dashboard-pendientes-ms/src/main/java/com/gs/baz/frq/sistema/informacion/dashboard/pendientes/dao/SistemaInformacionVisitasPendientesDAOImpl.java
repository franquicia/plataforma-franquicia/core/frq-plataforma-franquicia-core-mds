/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendienteParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.VisitasPendientesRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionVisitasPendientesDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertVisitasPendientes;
    private DefaultJdbcCall jdbcUpdateVisitasPendientes;
    private DefaultJdbcCall jdbcSelectVisitasPendientes;
    private DefaultJdbcCall jdbcSelectVisitasPendientesAll;
    private DefaultJdbcCall jdbcDeleteVisitasPendientes;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMVISIT_PEN";
        jdbcInsertVisitasPendientes = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertVisitasPendientes.withSchemaName(schema);
        jdbcInsertVisitasPendientes.withCatalogName(catalogo);
        jdbcInsertVisitasPendientes.withProcedureName("SPINSVISIT_PEND");

        jdbcUpdateVisitasPendientes = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateVisitasPendientes.withSchemaName(schema);
        jdbcUpdateVisitasPendientes.withCatalogName(catalogo);
        jdbcUpdateVisitasPendientes.withProcedureName("SPACTVISIT_PEND");

        jdbcDeleteVisitasPendientes = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteVisitasPendientes.withSchemaName(schema);
        jdbcDeleteVisitasPendientes.withCatalogName(catalogo);
        jdbcDeleteVisitasPendientes.withProcedureName("SPDELVISIT_PEND");

        jdbcSelectVisitasPendientes = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectVisitasPendientes.withSchemaName(schema);
        jdbcSelectVisitasPendientes.withCatalogName(catalogo);
        jdbcSelectVisitasPendientes.withProcedureName("SPGETVISIT_PEND");
        jdbcSelectVisitasPendientes.returningResultSet("PA_CDATOS", new VisitasPendientesRowMapper());

        jdbcSelectVisitasPendientesAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectVisitasPendientesAll.withSchemaName(schema);
        jdbcSelectVisitasPendientesAll.withCatalogName(catalogo);
        jdbcSelectVisitasPendientesAll.withProcedureName("SPGETVISIT_PEND");
        jdbcSelectVisitasPendientesAll.returningResultSet("PA_CDATOS", new VisitasPendientesRowMapper());

    }

    public VisitaPendiente selectRow(VisitaPendienteParametros bean) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_BITACORA", bean.getIdBitacora());
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", bean.getIdUsuario());
        mapSqlParameterSource.addValue("PA_FCID_CECO", bean.getIdCeco());
        Map<String, Object> out = jdbcSelectVisitasPendientes.execute(mapSqlParameterSource);
        List<VisitaPendiente> data = (List<VisitaPendiente>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }


    public List<VisitaPendiente> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_BITACORA", null);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        Map<String, Object> out = jdbcSelectVisitasPendientesAll.execute(mapSqlParameterSource);
        return (List<VisitaPendiente>) out.get("PA_CDATOS");
    }

    public void insertRow(VisitaPendiente entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_BITACORA", entityDTO.getIdBitacora());
            mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FDFECHA", entityDTO.getFecha());
            mapSqlParameterSource.addValue("PA_FIORDEN_GRUPO", entityDTO.getOrdenGrupo());
            Map<String, Object> out = jdbcInsertVisitasPendientes.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } 
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(VisitaPendiente entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_BITACORA", entityDTO.getIdBitacora());
            mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FDFECHA", entityDTO.getFecha());
            mapSqlParameterSource.addValue("PA_FIORDEN_GRUPO", entityDTO.getOrdenGrupo());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcUpdateVisitasPendientes.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer idBitacora) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_BITACORA", idBitacora);
            Map<String, Object> out = jdbcDeleteVisitasPendientes.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
