package com.gs.baz.frq.dashboard.mprs.filtro;

import com.gs.baz.frq.dashboard.dto.filtros.FiltroNegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FiltroNegocioRowMapper implements RowMapper<FiltroNegocioDTO> {

    private FiltroNegocioDTO negocioDTO;

    @Override
    public FiltroNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocioDTO = new FiltroNegocioDTO();
        negocioDTO.setIdNegocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        negocioDTO.setDescripcion(rs.getString("FCNEGOCIODISC"));
        return negocioDTO;
    }
}
