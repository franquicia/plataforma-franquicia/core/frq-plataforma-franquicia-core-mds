/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class RutaBase {

    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Codificacion de la ruta basada al modelo de negocio", example = "FRA-CYC")
    private String codificacion;

    @JsonProperty(value = "idPais")
    @ApiModelProperty(notes = "Identificados del pais", example = "1")
    private Integer idPais;

    @JsonProperty(value = "idModelo")
    @ApiModelProperty(notes = "Identificados del modelo", example = "1")
    private Integer idModelo;

    @JsonProperty(value = "idUnidadNegocio")
    @ApiModelProperty(notes = "Identificados de la unidad de negocio", example = "1")
    private Integer idUnidadNegocio;

    public String getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(String codificacion) {
        this.codificacion = codificacion;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Integer idModelo) {
        this.idModelo = idModelo;
    }

    public Integer getIdUnidadNegocio() {
        return idUnidadNegocio;
    }

    public void setIdUnidadNegocio(Integer idUnidadNegocio) {
        this.idUnidadNegocio = idUnidadNegocio;
    }

}
