/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.colorimetria.dao.util;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @param entityID
     * @return
     * @throws com.gs.baz.colorimetria.util.CustomException
     */
    public dto selectRow(Long entityID) throws CustomException;

    /**
     *
     * @param entityID
     * @return
     * @throws com.gs.baz.colorimetria.util.CustomException
     */
    public dto selectRowIcono(Long entityID) throws CustomException;

    /**
     *
     * @param jsonParams
     * @return
     * @throws CustomException
     */
    public dto selectRow(ObjectNode jsonParams) throws CustomException;

    /**
     *
     * @param jsonParams
     * @return
     * @throws CustomException
     */
    public List<dto> selectRows(ObjectNode jsonParams) throws CustomException;

    /**
     *
     * @return @throws CustomException
     */
    public List<dto> selectRows() throws CustomException;

    /**
     *
     * @param jsonParams
     * @return
     * @throws CustomException
     */
    public dto insertRow(ObjectNode jsonParams) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param jsonParams
     * @return
     * @throws CustomException
     */
    public dto updateRow(ObjectNode jsonParams) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param jsonParams
     * @return
     * @throws CustomException
     */
    public dto deleteRow(ObjectNode jsonParams) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
