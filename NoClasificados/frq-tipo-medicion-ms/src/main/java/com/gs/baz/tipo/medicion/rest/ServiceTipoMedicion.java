/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.tipo.medicion.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.tipo.medicion.dao.TipoMedicionDAOImpl;
import com.gs.baz.tipo.medicion.dto.TipoMedicionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/tipo/medicion")
public class ServiceTipoMedicion {

    @Autowired
    private TipoMedicionDAOImpl tipoMedicionDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/{id_tipo_medicion}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoMedicionDTO getTipoMedicion(@PathVariable("id_tipo_medicion") Long id_tipo_medicion) throws CustomException {
        return tipoMedicionDAOImpl.selectRow(id_tipo_medicion);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TipoMedicionDTO> getTipoMediciones() throws CustomException {
        return tipoMedicionDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoMedicionDTO postTipoMedicion(@RequestBody TipoMedicionDTO tipoMedicionDTO) throws CustomException {
        if (tipoMedicionDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return tipoMedicionDAOImpl.insertRow(tipoMedicionDTO);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoMedicionDTO putTipoMedicion(@RequestBody TipoMedicionDTO tipoMedicionDTO) throws CustomException {
        if (tipoMedicionDTO.getIdTipoMedicion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_tipo_medicion"));
        }
        if (tipoMedicionDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return tipoMedicionDAOImpl.updateRow(tipoMedicionDTO);
    }
}
