/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author cescobarh
 */
public class ParametrosGrafica {

    @JsonProperty(value = "idGrafica")
    private transient Integer idGrafica;

    @JsonProperty(value = "clave")
    @ApiModelProperty(notes = "Clave de la grafica", example = "generico_odometro")
    @NotEmpty
    private String clave;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la grafica", example = "Odometro")
    @NotEmpty
    private String descripcion;

    @JsonProperty(value = "notas")
    @ApiModelProperty(notes = "Notas de la grafica", example = "La fuente de datos...")
    @NotEmpty
    private String notas;

    public Integer getIdGrafica() {
        return idGrafica;
    }

    public void setIdGrafica(Integer idGrafica) {
        this.idGrafica = idGrafica;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "ParametrosGrafica{" + "idGrafica=" + idGrafica + ", clave=" + clave + ", descripcion=" + descripcion + ", notas=" + notas + '}';
    }

}
