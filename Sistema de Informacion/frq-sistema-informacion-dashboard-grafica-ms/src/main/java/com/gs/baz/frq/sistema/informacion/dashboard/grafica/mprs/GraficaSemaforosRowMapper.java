/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaSemaforo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class GraficaSemaforosRowMapper implements RowMapper<GraficaSemaforo> {

    @Override
    public GraficaSemaforo mapRow(ResultSet rs, int rowNum) throws SQLException {
        GraficaSemaforo graficaSemaforo = new GraficaSemaforo();
        graficaSemaforo.setIdSemaforo(rs.getInt("FIIDSEMAFORO"));
        graficaSemaforo.setRegla(rs.getString("FCNOMBREREGLA"));
        return graficaSemaforo;
    }
}
