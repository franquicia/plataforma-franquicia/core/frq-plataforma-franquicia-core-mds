/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class Documento {

    @JsonProperty(value = "titulo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Titulo del documento", example = "Ejemplo")
    private String titulo;

    @JsonProperty(value = "vigencia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Fecha en la que se libera el documento", example = "01-03-2021")
    private String vigencia;

    @JsonProperty(value = "objetivo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Objetivo del documento", example = "Ejemplo")
    private String objetivo;

    @JsonProperty(value = "version")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "version del documento", example = "1.0")
    private String version;

    @JsonProperty(value = "controlCambios")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Control de cambios del documento", example = "1.0 Creacion del documento")
    private String controlCambios;

    public Documento() {
    }

    public Documento(String titulo) {
        this.titulo = titulo;
    }

    public Documento(String titulo, String objetivo, String version, String controlCambios) {
        this.titulo = titulo;
        this.objetivo = objetivo;
        this.version = version;
        this.controlCambios = controlCambios;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getControlCambios() {
        return controlCambios;
    }

    public void setControlCambios(String controlCambios) {
        this.controlCambios = controlCambios;
    }

    @Override
    public String toString() {
        return "Documento{" + "titulo=" + titulo + ", vigencia=" + vigencia + ", objetivo=" + objetivo + ", version=" + version + ", controlCambios=" + controlCambios + '}';
    }

}
