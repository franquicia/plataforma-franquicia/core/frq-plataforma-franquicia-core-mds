/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.regla7s.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.regla7s.dao.Regla7SDAOImpl;
import com.gs.baz.frq.regla7s.dto.Regla7SDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/regla7s")
@Component("FRQServiceRegla7S")
public class ServiceRegla7S {

    @Autowired
    private Regla7SDAOImpl regla7SDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Regla7SDTO postRegla7S(@RequestBody Regla7SDTO regla7S) throws CustomException {
        if (regla7S.getPreguntaBase() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("pregunta_base"));
        }
        if (regla7S.getProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("protocolo"));
        }
        if (regla7S.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        if (regla7S.getIncumplimiento() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("incumplimiento"));
        }
        if (regla7S.getReglaNueve() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("regla_nueve"));
        }
        if (regla7S.getIdNegocioDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio_disciplina"));
        }
        if (regla7S.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (regla7S.getImperdonable() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("imperdonable"));
        }
        if (regla7S.getOportunidad() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("oportunidad"));
        }

        return regla7SDAOImpl.insertRow(regla7S);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Regla7SDTO putRegla7S(@RequestBody Regla7SDTO regla7S) throws CustomException {
        if (regla7S.getIdClave() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_clave"));
        }
        if (regla7S.getPreguntaBase() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("pregunta_base"));
        }
        if (regla7S.getProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("protocolo"));
        }
        if (regla7S.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        if (regla7S.getIncumplimiento() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("incumplimiento"));
        }
        if (regla7S.getReglaNueve() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("regla_nueve"));
        }
        if (regla7S.getIdNegocioDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio_disciplina"));
        }
        if (regla7S.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (regla7S.getImperdonable() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("imperdonable"));
        }
        if (regla7S.getOportunidad() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("oportunidad"));
        }
        return regla7SDAOImpl.updateRow(regla7S);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Regla7SDTO> getRegla7S() throws CustomException {
        return regla7SDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_clave}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Regla7SDTO getRegla7S(@PathVariable("id_clave") Long idClave, Long protocolo) throws CustomException {
        return regla7SDAOImpl.selectRow(idClave, protocolo);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Regla7SDTO deleteRegla7S(@RequestBody Regla7SDTO negocio) throws CustomException {
        if (negocio.getIdClave() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_clave"));
        }
        return regla7SDAOImpl.deleteRow(negocio);
    }
}
