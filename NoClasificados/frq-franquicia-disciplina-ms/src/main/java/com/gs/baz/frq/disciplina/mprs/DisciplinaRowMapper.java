package com.gs.baz.frq.disciplina.mprs;

import com.gs.baz.frq.disciplina.dto.DisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DisciplinaRowMapper implements RowMapper<DisciplinaDTO> {

    private DisciplinaDTO status;

    @Override
    public DisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new DisciplinaDTO();
        status.setIdDisciplina(((BigDecimal) rs.getObject("FIDISCIPLINA_ID")));
        status.setDescripcion(rs.getString("FCDISCIPLINA"));
        status.setBanderaGek(((BigDecimal) rs.getObject("FIBANDERA_GEK")));
        return status;
    }
}
