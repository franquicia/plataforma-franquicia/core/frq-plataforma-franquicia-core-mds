/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class ElementoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre")
    private String nombre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "directorioPadre")
    private String directorioPadre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rutaCompleta")
    private String rutaCompleta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipo")
    private TiposElemento tipo;

    public ElementoDTO() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirectorioPadre() {
        return directorioPadre;
    }

    public void setDirectorioPadre(String directorioPadre) {
        this.directorioPadre = directorioPadre;
    }

    public String getRutaCompleta() {
        return rutaCompleta;
    }

    public void setRutaCompleta(String rutaCompleta) {
        this.rutaCompleta = rutaCompleta;
    }

    public TiposElemento getTipo() {
        return tipo;
    }

    public void setTipo(TiposElemento tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "ElementoDTO{" + "nombre=" + nombre + ", directorioPadre=" + directorioPadre + ", rutaCompleta=" + rutaCompleta + ", tipo=" + tipo + '}';
    }

}
