/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.sup.rest;

import com.gs.baz.frq.asignacion.sup.dao.AsignacionSupDAOImpl;
import com.gs.baz.frq.asignacion.sup.dto.ArchivoAsignacionesDTO;
import com.gs.baz.frq.asignacion.sup.dto.AsignacionSupDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/asignacion-sup")
@Component("FRQServiceAsignacionSup")
public class ServiceAsignacionSup {

    @Autowired
    private AsignacionSupDAOImpl asignacionSupDAO;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /*
//negocio
	@RequestMapping(value = "secure/get/negocio", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionSupDTO > getProyectosCeco(@RequestBody AsignacionSupDTO negocio) throws CustomException {
		 

        return (List<AsignacionSupDTO>) asignacionSupDAO.selectRowDTO(negocio);
        
    }
	
    @RequestMapping(value = "secure/post/alta/negocio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaNegocio(@RequestBody AsignacionSupDTO asignacion) throws CustomException {

        if (asignacion.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Negocio-Supervison"));
        }
        return asignacionSupDAO.insertRow(asignacion);
    }
    
    @RequestMapping(value = "secure/put/negocio", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionSupDTO putUpdateNegocio(@RequestBody AsignacionSupDTO updateNego) throws CustomException {
        if (updateNego.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update Negocio-Supervison"));
        }
        
        return asignacionSupDAO.updateRow(updateNego);
    }

    
    @RequestMapping(value = "secure/delete/negocio", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaNegocio(@RequestBody AsignacionSupDTO elimina) throws CustomException {
        if (elimina.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Dep Negocio-Supervison"));
        }
       
        return asignacionSupDAO.eliminaRows(elimina);
    }
    //termina negocio
    
  //Versiones Sup
  	
    @RequestMapping(value = "secure/post/alta/version", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaVersionSup(@RequestBody AsignacionSupDTO version) throws CustomException {

        if (version.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta version-Supervison"));
        }
        return asignacionSupDAO.insertVersionSup(version);
    }
  	
    @RequestMapping(value = "secure/put/version", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public AsignacionSupDTO putVersionSup(@RequestBody AsignacionSupDTO updateVersion) throws CustomException {
        if (updateVersion.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update version-Supervison"));
        }
        
        return asignacionSupDAO.updateVersionSup(updateVersion);
    }
    @RequestMapping(value = "secure/delete/version", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaVersionSup(@RequestBody AsignacionSupDTO elimina) throws CustomException {
    	if (elimina.getParametro() == null && elimina.getIdVersion()!= null) {
    		  return asignacionSupDAO.eliminaVersionSup(elimina);
            
        }else if(elimina.getIdVersion() == null && elimina.getParametro()!=null) {
        	return asignacionSupDAO.eliminaVersionSup(elimina);
        	
        }else {

    	throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("delete version-Supervison"));
        }
    }
     */
    @RequestMapping(value = "secure/post/alta/asignacion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaAsignacion(@RequestBody AsignacionSupDTO asignacion) throws CustomException {

        if (asignacion.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Asignacion-Supervison"));
        }

        AsignacionSupDTO asignacion1 = new AsignacionSupDTO();
        asignacion1.setIdCeco(asignacion.getIdCeco());
        asignacion1.setIdUsuario(asignacion.getIdUsuario());
        asignacion1.setIdPerfil(1);
        asignacion1.setIdNegocio(asignacion.getIdNegocio());
        asignacion1.setIdGerente(asignacion.getIdGerente());

        AsignacionSupDTO asignacion2 = new AsignacionSupDTO();
        asignacion2.setIdCeco(asignacion.getIdCeco());
        asignacion2.setIdUsuario(asignacion.getIdGerente());
        asignacion2.setIdPerfil(2);
        asignacion2.setIdNegocio(asignacion.getIdNegocio());
        asignacion2.setIdGerente(asignacion.getIdGerente());

        int estatusAsignaciones = asignacionSupDAO.insertAsignacion(asignacion1);
        int estatusAsignaciones2 = asignacionSupDAO.insertAsignacion(asignacion2);

        //Eliminar asignación geografía
        asignacionSupDAO.eliminaAsignacionGeo(asignacion.getIdCeco() + "");
        //Insertar asignación geografía
        asignacionSupDAO.insertaAsignacionGeo(asignacion.getIdCeco() + "", asignacion.getNombreCeco(), asignacion.getTerritorio(), asignacion.getZona());

        asignacionSupDAO.desactivaAsignaciones();
        asignacionSupDAO.cargaAsignacion();

        return estatusAsignaciones;
    }

    @RequestMapping(value = "secure/post/carga/asignacion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postCargaAsignacion() throws CustomException {

        return asignacionSupDAO.cargaAsignacion();
    }

    @RequestMapping(value = "secure/post/desactiva/asignacion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postDesactivaAsignacion() throws CustomException {

        return asignacionSupDAO.desactivaAsignaciones();
    }

    @RequestMapping(value = "secure/delete/asignacion", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaAsignacion(@RequestBody AsignacionSupDTO elimina) throws CustomException {
        if (elimina.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("delete asignacion-Supervison"));

        } else {

            return asignacionSupDAO.eliminaAsignacion(elimina);
        }
    }

    @RequestMapping(value = "secure/post/asignacionsup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionSupDTO> getAsignacionesSup(@RequestBody AsignacionSupDTO asignacion) throws CustomException {

        if (asignacion.getIdPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Asignacion-Supervison"));
        }
        return (List<AsignacionSupDTO>) asignacionSupDAO.selectAsignacion(asignacion);
    }

    @RequestMapping(value = "secure/post/ejecuta/asignaciones", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postEjecutaAsignaciones(@RequestBody List<ArchivoAsignacionesDTO> asignacion) throws CustomException {
        int estatusAsignaciones = 0;
        int estatusAsignaciones2 = 0;
        if (asignacion == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Asignacion-Supervison"));
        } else {

            for (ArchivoAsignacionesDTO aux : asignacion) {
                String ceco = "" + aux.getNumeroEconomico();
                int negocio = 0;
                if (aux.getTipoVisita().equals("PV")) {
                    if (ceco.length() == 3) {
                        ceco = "920" + ceco;
                    } else {
                        ceco = "92" + ceco;
                    }
                    negocio = 1;
                } else {

                    if (aux.getTipoVisita().equals("GCC")) {
                        negocio = 5;
                        if (ceco.length() == 3) {
                            ceco = "230" + ceco;
                        } else {
                            ceco = "23" + ceco;
                        }
                    } else {

                        if (ceco.length() == 3) {
                            ceco = "550" + ceco;
                        } else {
                            ceco = "55" + ceco;
                        }

                        if (aux.getTipoVisita().equals("CDT")) {
                            negocio = 7;
                        }

                        if (aux.getTipoVisita().equals("ITK")) {
                            negocio = 8;
                        }

                    }

                }

                AsignacionSupDTO asignacion1 = new AsignacionSupDTO();
                asignacion1.setIdCeco(Integer.parseInt(ceco));
                asignacion1.setIdUsuario(aux.getNumeroEmpleado());
                asignacion1.setIdPerfil(1);
                asignacion1.setIdNegocio(negocio);
                asignacion1.setIdGerente(aux.getNumeroGerente());

                AsignacionSupDTO asignacion2 = new AsignacionSupDTO();
                asignacion2.setIdCeco(Integer.parseInt(ceco));
                asignacion2.setIdUsuario(aux.getNumeroGerente());
                asignacion2.setIdPerfil(2);
                asignacion2.setIdNegocio(negocio);
                asignacion2.setIdGerente(aux.getNumeroGerente());

                estatusAsignaciones = asignacionSupDAO.insertAsignacion(asignacion1);
                estatusAsignaciones2 = asignacionSupDAO.insertAsignacion(asignacion2);

                //Eliminar asignación geografía
                asignacionSupDAO.eliminaAsignacionGeo(ceco);
                //Insertar asignación geografía
                asignacionSupDAO.insertaAsignacionGeo(ceco, aux.getNombreSucursal(), aux.getTerritorio(), aux.getZona());

            }

            asignacionSupDAO.desactivaAsignaciones();
            asignacionSupDAO.cargaAsignacion();

        }

        return estatusAsignaciones;
    }

    @RequestMapping(value = "secure/post/carga/asignacionexcel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)

    public List<ArchivoAsignacionesDTO> postCargaExcelAsigna(@RequestBody ArchivoAsignacionesDTO asignacion) throws CustomException {

        List<ArchivoAsignacionesDTO> listaAsignaciones = null;

        if (asignacion.getArchivoAsignaciones() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta carga Asignacion-Supervison"));
        } else {
            String archivo = asignacion.getArchivoAsignaciones();
            try {
                byte[] archivoDecodificada = Base64.getDecoder().decode(archivo);

                Date fechaActual = new Date();
                DateFormat formatoAnio = new SimpleDateFormat("yyyy");
                DateFormat formatoMes = new SimpleDateFormat("MM");
                DateFormat formatoFCompleta = new SimpleDateFormat("yyyyMMdd");
                String anio = formatoAnio.format(fechaActual);
                String mes = formatoMes.format(fechaActual);
                String fActual = formatoFCompleta.format(fechaActual);

                File r2 = null;
                String rootPath = File.listRoots()[0].getAbsolutePath();
                String rutaArchivo = "/franquicia/ArchivosAsignaciones/" + anio + "/" + mes + "/";

                String rutaPATHArchivo = rootPath + rutaArchivo;

                r2 = new File(rutaPATHArchivo);

                if (r2.mkdirs()) {
                    logger.info("SE HA CREADA LA CARPETA");
                } else {
                    logger.info("EL DIRECTORIO YA EXISTE");
                }

                String nomArchivo = "Asignaciones_" + fActual + ".xlsx";

                String rutaTotalF = rutaPATHArchivo + nomArchivo;
                FileOutputStream fileOutputF = null;
                try {
                    fileOutputF = new FileOutputStream(rutaTotalF);

                    BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                    bufferOutputF.write(archivoDecodificada);
                    bufferOutputF.close();

                } catch (Exception ex) {

                    logger.info("Ocurrio algo: " + ex);
                } finally {
                    try {
                        fileOutputF.close();
                    } catch (Exception e) {
                        logger.info("AP" + e);
                    }
                }

                File file = new File(rutaTotalF);
                listaAsignaciones = leerExcellAsignaciones(file);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return listaAsignaciones;
    }

    /*

    @RequestMapping(value = "secure/post/obtiene/informacionCalendario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getDatosCalendario(@RequestBody AsignacionSupDTO asignaciones) throws CustomException {
       
        if (asignaciones.getFecha() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("asignaciones"));
        }
        return  asignacionSupDAO.selectCalendarioRows(asignaciones);
    }
     */
    @SuppressWarnings("deprecation")
    public ArrayList<ArchivoAsignacionesDTO> leerExcellAsignaciones(File file) {
        ArrayList<ArchivoAsignacionesDTO> lista = new ArrayList<ArchivoAsignacionesDTO>();
        try {

            // leer archivo excel
            XSSFWorkbook worbook = new XSSFWorkbook(file);
            // obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheetAt(0);
            int filas = 0;
            boolean pregError = false;

            ArrayList<String> nomColums = new ArrayList<String>();
            String idVisita = "";
            //System.out.println(sheet.getro);
            for (Row row : sheet) {
                ArchivoAsignacionesDTO obj = new ArchivoAsignacionesDTO();
                filas++;
                int column = 0;
                for (Cell cell : row) {
                    column++;
                    String format = "";
                    DataFormatter formatter = new DataFormatter();
                    // calcula el resultado de la formula
                    format = formatter.formatCellValue(cell);
                    if (column == 1 && format.equals("")) {
                        break;
                    }

                    if (filas != 1) {

                        switch (column) {
                            case 1:

                                obj.setNumeroEconomico(Integer.parseInt(format));

                                break;
                            case 2:

                                obj.setNombreSucursal(format);
                                ;
                                break;
                            case 3:
                                obj.setNumeroEmpleado(Integer.parseInt(format));

                                break;
                            case 4:
                                obj.setNombreCertificador(format);

                                break;
                            case 5:
                                //Territorio
                                obj.setTerritorio(format);
                                break;
                            case 6:
                                //zona
                                obj.setZona(format);
                                break;
                            case 7:
                                //Numero Gerente
                                obj.setNumeroGerente(Integer.parseInt(format));
                                break;
                            case 8:
                                //Nombre Gerente
                                obj.setNombreGerente(format);
                                break;
                            case 9:
                                //Formato visita
                                obj.setTipoVisita(format);
                                lista.add(obj);
                                break;

                        }

                    } else {
                        nomColums.add(format);
                    }

                } // Cierra for cell

            } // Cierra for row

            System.out.println(filas);

        } catch (Exception e) {
            logger.info("Ocurrio algo en LeerExcell: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return lista;
    }

}
