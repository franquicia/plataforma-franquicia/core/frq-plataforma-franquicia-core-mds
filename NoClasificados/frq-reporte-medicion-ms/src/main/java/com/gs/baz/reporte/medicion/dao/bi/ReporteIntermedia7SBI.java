/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dao.bi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ProcessMonitor;
import com.gs.baz.model.bucket.client.services.dto.DisciplinaDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinaFranquicia;
import com.gs.baz.reporte.medicion.dao.NegocioProtocoloDAOImpl;
import com.gs.baz.reporte.medicion.dao.ProtocoloDisciplinaDAOImpl;
import com.gs.baz.reporte.medicion.dao.ReporteMedicion7sDAO;
import com.gs.baz.reporte.medicion.dao.ReporteMedicionDAO;
import com.gs.baz.reporte.medicion.dao.VersionChecklistDAOImpl;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import com.gs.baz.reporte.medicion.dto.NegocioDTO;
import com.gs.baz.reporte.medicion.dto.ProtocoloDisciplinaDTO;
import com.gs.baz.reporte.medicion.dto.VersionChecklistDTO;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author MADA
 */
public class ReporteIntermedia7SBI {

    private ProcessMonitor processMonitor;

    @Autowired
    ProtocoloDisciplinaDAOImpl protocoloDisciplinaDAOImpl;
    @Autowired
    NegocioProtocoloDAOImpl negocioProtocoloDAOImpl;
    @Autowired
    VersionChecklistDAOImpl versionChecklistDAOImpl;
    @Autowired
    ReporteMedicion7sDAO reporteMedicion7sDAO;
    @Autowired
    ReporteMedicionDAO reporteMedicionDAO;

    public void init() throws Exception {
    }

    public ObjectNode reporteMedicionIntermedia(SubServiceDisciplinaFranquicia subServiceDisciplinaFranquicia, FiltersDTO filtersDTO) throws CustomException {

        ObjectMapper objectMapper = new ObjectMapper();

        ObjectNode visitasPreguntaRespuestas = objectMapper.createObjectNode();
        DisciplinaDTO disciplinaDTO = new DisciplinaDTO();//subServiceDisciplinaFranquicia.getDisciplina(filtersDTO.getIdDisciplina());
        disciplinaDTO.setIdDisciplina(BigDecimal.valueOf(5));
        disciplinaDTO.setDescripcion("7S");
        String disciplinaDescripcion = "Reporte Medición";
        if (disciplinaDTO != null) {
            disciplinaDescripcion = disciplinaDTO.getDescripcion();
        }
        visitasPreguntaRespuestas.put("name_file", disciplinaDescripcion);
        processMonitor = new ProcessMonitor("Reporte Medición");
        processMonitor.startWatch("Consulta protocolos disciplina");

        //Protocolos
        List<ProtocoloDisciplinaDTO> protocolos = protocoloDisciplinaDAOImpl.selectProtocoloDisciplina(filtersDTO);

        for (ProtocoloDisciplinaDTO protocoloDisciplinaDTO : protocolos) {

            filtersDTO.setIdProtocolo(protocoloDisciplinaDTO.getIdProtocolo());
            filtersDTO.setProtocoloDescripcion(protocoloDisciplinaDTO.getDescripcion());

            List<VersionChecklistDTO> versionesChecklist = versionChecklistDAOImpl.selectVersionChecklist(filtersDTO);

            for (VersionChecklistDTO checklist : versionesChecklist) {

                FiltersDTO filterDTOChecklist = new FiltersDTO();
                filterDTOChecklist.setAnio(filtersDTO.getAnio());
                filterDTOChecklist.setDisciplinaDescripcion(filtersDTO.getDisciplinaDescripcion());
                filterDTOChecklist.setIdCeco(filtersDTO.getIdCeco());
                filterDTOChecklist.setFechaInicio(checklist.getFechaInicio());
                filterDTOChecklist.setFechaFin(checklist.getFechaFin());
                filterDTOChecklist.setIdChecklist(checklist.getIdChecklist());
                filterDTOChecklist.setIdDashboard(filtersDTO.getIdDashboard());
                filterDTOChecklist.setIdNegocio(filtersDTO.getIdNegocio());
                filterDTOChecklist.setIdPeriodo(filtersDTO.getIdPeriodo());
                filterDTOChecklist.setIdProtocolo(filtersDTO.getIdProtocolo());
                filterDTOChecklist.setNegocioDescripcion(filtersDTO.getNegocioDescripcion());
                filterDTOChecklist.setNivel(filtersDTO.getNivel());
                filterDTOChecklist.setProtocoloDescripcion(filtersDTO.getProtocoloDescripcion());

                List<NegocioDTO> negocios = negocioProtocoloDAOImpl.selectNegocioProtocolo(filtersDTO);

                //negocios.removeIf(item -> item.getIdNegocio() == 4);
                visitasPreguntaRespuestas.setAll(reporteMedicionProcess(filterDTOChecklist, negocios));

            }
        }

        processMonitor.resumeInfo();
        return visitasPreguntaRespuestas;

    }

    public ObjectNode reporteMedicionProcess(FiltersDTO filtersDTO, List<NegocioDTO> negocioDTOs) {

        ObjectMapper objectMapper = new ObjectMapper();

        List<ObjectNode> respuestas;
        List<ObjectNode> preguntas;
        List<ObjectNode> calificaciones;
        ArrayNode visitas;
        // ArrayNode visitasAux = objectMapper.createArrayNode();

        processMonitor.startWatch("Consulta informacion reporte medicion query");

        processMonitor.stopWatch();
        processMonitor.startWatch("Convierte informacion");
        visitas = objectMapper.convertValue(reporteMedicion7sDAO.selectVisitas(filtersDTO), ArrayNode.class);

        respuestas = convertValue(reporteMedicionDAO.selectRespuestas(filtersDTO));
        preguntas = convertValue(reporteMedicionDAO.selectPreguntas(filtersDTO));
        calificaciones = convertValue(reporteMedicion7sDAO.selectCalificaciones(filtersDTO));
        processMonitor.stopWatch();
        processMonitor.startWatch("Procesa informacion");
        visitas.forEach(item -> {
            String noEconomico = item.get("SUCURSAL").asText();
            negocioDTOs.forEach(itemNegocio -> {
                ((ObjectNode) item).setAll(calificacionByVisita(calificaciones, itemNegocio, noEconomico));
            });
        });
        HashMap<String, JsonNode> visitasMap = new HashMap<>();

        visitas.forEach(item -> {
            String idVisita = item.get("FIVISITA_ID").asText();
            String noEconomico = item.get("SUCURSAL").asText();

            if (visitasMap.get(noEconomico) == null) {
                visitasMap.put(noEconomico, item);
            }
            ((ObjectNode) visitasMap.get(noEconomico)).setAll(pregutasRespuestaByVisita(respuestas, preguntas, idVisita));

        });

        ArrayNode visitasAux = objectMapper.convertValue(visitasMap, ArrayNode.class);

        processMonitor.stopWatch();
        processMonitor.startWatch("Crea objeto final");
        ObjectNode visitasPreguntaRespuestas = objectMapper.createObjectNode();
        String nameTab = filtersDTO.getIdProtocolo() + " - " + filtersDTO.getFechaInicio().replace("/", "-");
        visitasPreguntaRespuestas.set(nameTab, visitasAux);
        processMonitor.stopWatch();
        return visitasPreguntaRespuestas;
    }

    private ObjectNode pregutasRespuestaByVisita(List<ObjectNode> respuestas, List<ObjectNode> preguntas, String idVisita) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode preguntasItem;
        List<ObjectNode> respuestasVisita;
        preguntasItem = objectMapper.createObjectNode();
        respuestasVisita = respuestas.stream().filter(item -> item.get("FIVISITA_ID").asText().equals(idVisita)).collect(Collectors.toList());
        preguntas.forEach(itemPregunta -> {
            String noSerie = itemPregunta.get("FINUMSERIE").asText();
            ObjectNode itemRespuestaPregunta = respuestasVisita.stream().filter(item -> item.get("FINUMSERIE").asText().equals(noSerie)).findFirst().orElse(null);
            String respuesta = (itemRespuestaPregunta != null ? itemRespuestaPregunta.get("FCRESPUESTA").asText() : "");
            preguntasItem.setAll(objectMapper.createObjectNode().put(itemPregunta.get("FCDESCRIPCION").asText(), respuesta));
            respuestasVisita.removeIf(item -> item.get("FINUMSERIE").asText().equals(noSerie));
        });
        respuestas.removeIf(item -> item.get("FIVISITA_ID").asText().equals(idVisita));
        return preguntasItem;
    }

    private ObjectNode calificacionByVisita(List<ObjectNode> calificaciones, NegocioDTO negocioDTO, String noEconomico) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode preguntasItem = objectMapper.createObjectNode();
        String idNegocio = negocioDTO.getIdNegocio() + "";
        ObjectNode calificacionVisita = calificaciones.stream().filter(item -> item.get("ECONOMICO_SUCURSAL").asText().equals(noEconomico) && item.get("NEGOCIO").asText().equals(idNegocio)).findFirst().orElse(null);
        String calificacion = "";
        if (calificacionVisita != null) {
            calificacion = calificacionVisita.get("CALIFICACION").asText();
        }
        preguntasItem.setAll(objectMapper.createObjectNode().put("Calificación " + negocioDTO.getDescripcion(), calificacion));
        //calificaciones.removeIf(item -> item.get("ECONOMICO_SUCURSAL").asText().equals(noEconomico) && item.get("NEGOCIO").asText().equals(idNegocio));
        return preguntasItem;
    }

    private List<ObjectNode> convertValue(Object value) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(value, objectMapper.getTypeFactory().constructCollectionLikeType(List.class, ObjectNode.class));
    }

}
