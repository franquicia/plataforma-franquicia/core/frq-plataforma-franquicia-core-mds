/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.punto.contacto.protocolo.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.punto.contacto.protocolo.dto.PuntoContactoProtocoloDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @return @throws CustomException
     */
    public List<PuntoContactoProtocoloDTO> selectRows() throws CustomException;

    /**
     *
     * @param entityID
     *
     * @return
     * @throws CustomException
     */
    public List<PuntoContactoProtocoloDTO> selectRow(Long entityID) throws CustomException;

}
