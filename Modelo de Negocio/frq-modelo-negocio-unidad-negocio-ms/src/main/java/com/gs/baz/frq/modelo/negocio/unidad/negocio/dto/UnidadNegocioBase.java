/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de unidad de negocio", value = "UnidadNegocioBase")
public class UnidadNegocioBase {

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion la unidad de negocio", example = "Cajas")
    private String descripcion;

    @JsonProperty(value = "acronimo", required = true)
    @ApiModelProperty(notes = "Aronimo de la unidad de negocio", example = "CYF")
    private String acronimo;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }
}
