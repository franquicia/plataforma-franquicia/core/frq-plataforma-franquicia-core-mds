/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class DirectoryProtectedException extends Exception{

    public DirectoryProtectedException() {
    }

    public DirectoryProtectedException(String string) {
        super(string);
    }

    public DirectoryProtectedException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public DirectoryProtectedException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
