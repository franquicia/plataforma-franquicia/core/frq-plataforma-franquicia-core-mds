/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de los archivos asociados a centro de costos y poveedor", value = "Archivos")
public class ArchivoEliminar {

    @JsonProperty(value = "idArchivo")
    @ApiModelProperty(notes = "Identificador del archivo", example = "1", position = -1)
    private Integer idArchivo;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -2)
    private Integer idFolio;
    
    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Ruta del archivo.", example = "", position = -3)
    private String ruta;
    
    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "", example = "MEGA DF LA LUNA", position = -4)
    private Integer tipo;
    
    @JsonProperty(value = "peso")
    @ApiModelProperty(notes = "Identificador del proveedor.", example = "1", position = -5)
    private Integer peso;

    @JsonProperty(value = "idEstado")
    @ApiModelProperty(notes = "", example = "", position = -6)
    private Integer idEstado;
    
    @JsonProperty(value = "auxiliarUno")
    @ApiModelProperty(notes = "", example = "", position = -7)
    private String auxiliarUno;
     
    @JsonProperty(value = "auxiliarDos")
    @ApiModelProperty(notes = "", example = "", position = -7)
    private String auxiliarDos;    

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getAuxiliarUno() {
        return auxiliarUno;
    }

    public void setAuxiliarUno(String auxiliarUno) {
        this.auxiliarUno = auxiliarUno;
    }

    public String getAuxiliarDos() {
        return auxiliarDos;
    }

    public void setAuxiliarDos(String auxiliarDos) {
        this.auxiliarDos = auxiliarDos;
    }

    @Override
    public String toString() {
        return "ArchivoEliminar{" + "idArchivo=" + idArchivo + ", idFolio=" + idFolio + ", ruta=" + ruta + ", tipo=" + tipo + ", peso=" + peso + ", idEstado=" + idEstado + ", auxiliarUno=" + auxiliarUno + ", auxiliarDos=" + auxiliarDos + '}';
    }

}
