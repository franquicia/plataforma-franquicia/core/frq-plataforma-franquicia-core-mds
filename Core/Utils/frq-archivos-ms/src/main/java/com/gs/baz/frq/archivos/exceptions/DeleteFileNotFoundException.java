/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class DeleteFileNotFoundException extends Exception{

    public DeleteFileNotFoundException() {
    }

    public DeleteFileNotFoundException(String string) {
        super(string);
    }

    public DeleteFileNotFoundException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public DeleteFileNotFoundException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
