/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author cescobarh
 */
public class Cabezera {

    @JsonProperty(value = "valor")
    @NotEmpty
    private String valor;

    @JsonProperty(value = "descripcion")
    @NotEmpty
    private String descripcion;

    private transient Integer index;

    public Cabezera() {
    }

    public Cabezera(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Cabezera{" + "valor=" + valor + ", descripcion=" + descripcion + ", index=" + index + '}';
    }

}
