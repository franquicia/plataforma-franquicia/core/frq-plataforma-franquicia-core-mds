package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.CecoProyectoFolio;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.CecoProyectoFolioFormato;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.Formato;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.mprs.CecoFormatoRowMapper;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.mprs.CecoProyectoFolioRowMapper;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class CecoProyectoFolioDAOImpl extends DefaultDAO {

    private final Logger logger = LogManager.getLogger();

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectCeco;
    private DefaultJdbcCall jdbcSelectAll;

    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalago = "PA_ADFORMATO";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalago);
        jdbcInsert.withProcedureName("SP_INS_CECOPROY");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalago);
        jdbcDelete.withProcedureName("SP_DEL_CECOPROY");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalago);
        jdbcSelect.withProcedureName("SP_SEL_CECOPROY");
        jdbcSelect.returningResultSet("RCL_FORM", new CecoProyectoFolioRowMapper());

        jdbcSelectCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCeco.withSchemaName(schema);
        jdbcSelectCeco.withCatalogName(catalago);
        jdbcSelectCeco.withProcedureName("SP_SEL_FORMCECO");
        jdbcSelectCeco.returningResultSet("RCL_FORM", new CecoFormatoRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalago);
        jdbcUpdate.withProcedureName("SP_ACT_CECOPROY");

    }

    public List<CecoProyectoFolio> selectRows(String ceco, String idProyecto, String idFormato) {

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_IDCECO", ceco);
        in.addValue("PA_IDPROY", idProyecto);
        in.addValue("PA_FORMATO", idFormato);

        Map<String, Object> out = jdbcSelect.execute(in);

        List<CecoProyectoFolio> data = (List<CecoProyectoFolio>) out.get("RCL_FORM");

        if (data != null && !data.isEmpty()) {
            return data;
        } else {
            return null;
        }

    }

    public List<Formato> selectRowsCeco(Formato ceco) {

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_IDCECO", ceco.getIdCeco());

        Map<String, Object> out = jdbcSelectCeco.execute(in);

        List<Formato> data = (List<Formato>) out.get("RCL_FORM");

        if (data != null && !data.isEmpty()) {
            return data;
        } else {
            return null;
        }

    }

    public void deleteRow(CecoProyectoFolio cecoProyectoFolio) throws DataNotDeletedException {

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_IDCECO", cecoProyectoFolio.getIdCeco());
        in.addValue("PA_IDPROY", cecoProyectoFolio.getIdProyecto());
        in.addValue("PA_FOLIO", cecoProyectoFolio.getIdFolio());
        in.addValue("PA_FORMATO", cecoProyectoFolio.getIdFormato());

        Map<String, Object> out = jdbcDelete.execute(in);

        int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();

        if (success == 0) {
            throw new DataNotDeletedException();
        } else if (success == -2) {
            throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
        }

    }

    public void insertRow(CecoProyectoFolio cecoProyectoFolio) throws DataNotInsertedException {

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_IDCECO", cecoProyectoFolio.getIdCeco());
        in.addValue("PA_IDPROY", cecoProyectoFolio.getIdProyecto());
        in.addValue("PA_FOLIO", cecoProyectoFolio.getIdFolio());
        in.addValue("PA_FORMATO", cecoProyectoFolio.getIdFormato());

        Map<String, Object> out = jdbcInsert.execute(in);

        int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();

        if (success == 0) {
            throw new DataNotInsertedException();
        } else if (success == -2) {
            throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
        }

    }

    public void updateRow(CecoProyectoFolioFormato cecoProyectoFolio) throws DataNotUpdatedException {

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_IDCECO", cecoProyectoFolio.getIdCeco());
        in.addValue("PA_IDPROY", cecoProyectoFolio.getIdProyecto());
        in.addValue("PA_FOLIO", cecoProyectoFolio.getIdFolio());
        in.addValue("PA_FORMATO", cecoProyectoFolio.getIdFormato());
        in.addValue("PA_IDDOC", cecoProyectoFolio.getIdDocumento());
        in.addValue("PA_STATUS", cecoProyectoFolio.getIdEstatus());

        Map<String, Object> out = jdbcUpdate.execute(in);

        int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();

        if (success == 0) {
            throw new DataNotUpdatedException();
        } else if (success == -2) {
            throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
        }

    }

}
