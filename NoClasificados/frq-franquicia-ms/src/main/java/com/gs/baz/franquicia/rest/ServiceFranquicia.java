/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.rest;

import com.gs.baz.franquicia.dao.FranquiciaDAOImpl;
import com.gs.baz.franquicia.dto.FranquiciaDTO;
import com.gs.baz.franquicia.rest.dto.DataFranquiciaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import com.gs.baz.model.bucket.client.services.dto.NegocioDTO;
import com.gs.baz.model.bucket.client.services.dto.UsuarioDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceCategoria;
import com.gs.baz.model.bucket.client.services.rest.SubServiceNegocio;
import com.gs.baz.model.bucket.client.services.rest.SubServiceUsuario;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia")
public class ServiceFranquicia {

    @Autowired
    private FranquiciaDAOImpl franquiciaDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceNegocio subServiceNegocio;

    @Autowired
    private SubServiceCategoria subServiceCategoria;

    @Autowired
    private SubServiceUsuario subServiceUsuario;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @param headers
     * @return @throws CustomException
     * @throws java.io.IOException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FranquiciaDTO> getFranquicias(@RequestHeader HttpHeaders headers) throws CustomException, IOException {
        final List<FranquiciaDTO> franquiciasDTO;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            franquiciasDTO = franquiciaDAOImpl.selectRows();
            if (franquiciasDTO != null) {
                subServiceUsuario.init(basePath, httpEntity);
                for (FranquiciaDTO franquicia : franquiciasDTO) {
                    this.setEmpleado(franquicia);
                }
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return franquiciasDTO;
    }

    private void setEmpleado(FranquiciaDTO franquiciasDTO) throws CustomException {
        if (franquiciasDTO.getIdEmpleadoCrea() != null) {
            UsuarioDTO usuarioCrea = subServiceUsuario.getUsuario(franquiciasDTO.getIdEmpleadoCrea());
            franquiciasDTO.setEmpleadoCrea(usuarioCrea);
            franquiciasDTO.setIdEmpleadoCrea(null);
        }
        if (franquiciasDTO.getIdEmpleadoModifica() != null) {
            UsuarioDTO usuarioModifica = subServiceUsuario.getUsuario(franquiciasDTO.getIdEmpleadoModifica());
            franquiciasDTO.setEmpleadoModificacion(usuarioModifica);
            franquiciasDTO.setIdEmpleadoModifica(null);
        }
    }

    /**
     * @param headers
     * @param id_franquicia
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/data/{id_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataFranquiciaDTO getDataFranquicia(@RequestHeader HttpHeaders headers, @PathVariable("id_franquicia") Long id_franquicia) throws CustomException {
        final DataFranquiciaDTO dataFranquiciaDTO = new DataFranquiciaDTO();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            FranquiciaDTO franquiciaDTO = franquiciaDAOImpl.selectRow(id_franquicia);
            subServiceNegocio.init(basePath, httpEntity);
            subServiceCategoria.init(basePath, httpEntity);
            List<NegocioDTO> negocios = subServiceNegocio.getNegocios();
            metadata.setNegocios(negocios);
            dataFranquiciaDTO.setFranquiciaDTO(franquiciaDTO);
            dataFranquiciaDTO.setMetadata(metadata);
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return dataFranquiciaDTO;
    }

    @RequestMapping(value = "secure/get/{id_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FranquiciaDTO getFranquicia(@PathVariable("id_franquicia") Long idfranquicia) throws CustomException {
        return franquiciaDAOImpl.selectSimpleRow(idfranquicia);
    }

    /**
     * @param headers
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/data/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataFranquiciaDTO getData(@RequestHeader HttpHeaders headers) throws CustomException {
        final DataFranquiciaDTO dataFranquiciaDTO = new DataFranquiciaDTO();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            subServiceNegocio.init(basePath, httpEntity);
            subServiceCategoria.init(basePath, httpEntity);
            List<NegocioDTO> negocios = subServiceNegocio.getNegocios();
            metadata.setNegocios(negocios);
            dataFranquiciaDTO.setMetadata(metadata);
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return dataFranquiciaDTO;
    }

    /**
     *
     * @param franquicia
     * @return
     * @throws com.gs.baz.frq.model.commons.CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FranquiciaDTO postFranquicia(@RequestBody FranquiciaDTO franquicia) throws CustomException {
        if (franquicia.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return franquiciaDAOImpl.insertRow(franquicia);
    }

    /**
     *
     * @param franquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public FranquiciaDTO putModelo(@RequestBody FranquiciaDTO franquicia) throws CustomException {
        if (franquicia.getIdFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_franquicia"));
        }
        return franquiciaDAOImpl.updateRow(franquicia);
    }

    /**
     *
     * @param franquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FranquiciaDTO deleteEjecuta(@RequestBody FranquiciaDTO franquicia) throws CustomException {
        if (franquicia.getIdFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_franquicia"));
        }
        if (franquicia.getIdStatus() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_status"));
        }
        if (franquicia.getIdEmpleadoModifica() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_empleado_modifica"));
        }

        return franquiciaDAOImpl.deleteRow(franquicia);

    }

    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FranquiciaDTO> getFranquiciaByNombre(@RequestHeader HttpHeaders headers, @QueryParam("value") String value) throws CustomException {
        return franquiciaDAOImpl.selectRowsByName(value);
    }
}
