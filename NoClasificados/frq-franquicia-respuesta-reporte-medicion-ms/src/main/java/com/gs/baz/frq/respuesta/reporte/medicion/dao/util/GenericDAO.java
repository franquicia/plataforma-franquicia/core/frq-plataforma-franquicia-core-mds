/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.respuesta.reporte.medicion.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.respuesta.reporte.medicion.dto.RespuestaReporteMedicionDTO;
import java.util.List;

/**
 *
 * @author jlfr
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @return @throws CustomException
     */
    public List<RespuestaReporteMedicionDTO> selectRows() throws CustomException;

    public List<RespuestaReporteMedicionDTO> selectRow(Long entityID, Long entityID2) throws CustomException;

    /**
     *
     * @param idCeco
     * @param idProtocolo
     * @param modulo
     * @return
     * @throws CustomException
     */
    public List<RespuestaReporteMedicionDTO> selectRowsRespuestaReporteMedicion(RespuestaReporteMedicionDTO entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
