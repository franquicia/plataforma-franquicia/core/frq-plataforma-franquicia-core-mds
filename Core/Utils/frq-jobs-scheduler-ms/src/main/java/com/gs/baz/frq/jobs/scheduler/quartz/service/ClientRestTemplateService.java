package com.gs.baz.frq.jobs.scheduler.quartz.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.jobs.scheduler.dto.Execution;
import com.gs.baz.frq.jobs.scheduler.dto.ExecutionURL;
import com.gs.baz.frq.jobs.scheduler.dto.ScheduleJob;
import com.gs.baz.frq.jobs.scheduler.rest.client.internal.SubServiceGenericEntity;
import com.gs.baz.frq.jobs.scheduler.rest.client.internal.SubServiceToken;
import java.net.URI;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
public class ClientRestTemplateService {

    private final Logger logger = LogManager.getLogger();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private String basePath;
    private String body;
    private String requestMethod;
    private ExecutionURL executionURL;
    private final SubServiceToken subServiceToken = new SubServiceToken();
    private final SubServiceGenericEntity subServiceGenericEntity = new SubServiceGenericEntity();

    private String getToken() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("frontendapp", "12345");
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("username", "202622");
        bodyMap.add("password", "202622");
        bodyMap.add("grant_type", "password");
        final HttpEntity<Object> httpEntityToken = new HttpEntity<>(bodyMap, headers);
        URI uri = new URI(basePath);
        subServiceToken.init(uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort() + "/plataforma-franquicia-core/service/security/oauth/token", httpEntityToken);
        return subServiceToken.getToken();
    }

    private String getEntity() throws Exception {
        String token = this.getToken();
        if (token != null && token.contains("access_token")) {
            ObjectNode tokenObjectNode = objectMapper.readValue(token, ObjectNode.class);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(tokenObjectNode.get("access_token").asText());
            final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
            subServiceGenericEntity.init(basePath, httpEntity);
            return subServiceGenericEntity.getEntity();
        }
        return null;
    }

    private String postEntity() throws Exception {
        String token = this.getToken();
        if (token != null && token.contains("access_token")) {
            ObjectNode tokenObjectNode = objectMapper.readValue(token, ObjectNode.class);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(tokenObjectNode.get("access_token").asText());
            final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
            subServiceGenericEntity.init(basePath, httpEntity);
            return subServiceGenericEntity.addEntity();
        }
        return null;
    }

    private String putEntity() throws Exception {
        String token = this.getToken();
        if (token != null && token.contains("access_token")) {
            ObjectNode tokenObjectNode = objectMapper.readValue(token, ObjectNode.class);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(tokenObjectNode.get("access_token").asText());
            final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
            subServiceGenericEntity.init(basePath, httpEntity);
            return subServiceGenericEntity.updateEntity();
        }
        return null;
    }

    private String deleteEntity() throws Exception {
        String token = this.getToken();
        if (token != null && token.contains("access_token")) {
            ObjectNode tokenObjectNode = objectMapper.readValue(token, ObjectNode.class);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(tokenObjectNode.get("access_token").asText());
            final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
            subServiceGenericEntity.init(basePath, httpEntity);
            return subServiceGenericEntity.deleteEntity();
        }
        return null;
    }

    public void run(ScheduleJob scheduleJob) {
        this.executionURL = scheduleJob.getExecutionURL();
        this.basePath = executionURL.getUri();
        this.body = executionURL.getRequestBody();
        this.requestMethod = executionURL.getRequestMethod();
        Execution execution = scheduleJob.getExecution();
        if (execution == null) {
            execution = new Execution();
        }
        String response = "none";
        try {
            if (this.requestMethod.toLowerCase().equals("get")) {
                response = this.getEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            if (this.requestMethod.toLowerCase().equals("post")) {
                response = this.postEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            if (this.requestMethod.toLowerCase().equals("put")) {
                response = this.putEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            if (this.requestMethod.toLowerCase().equals("delete")) {
                response = this.deleteEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            execution.setLastExecutionSucccess(new Date());
            execution.setLastExecutionSucccessResult(response);
        } catch (Exception ex) {
            response = ex.getMessage();
            execution.setLastExecutionError(new Date());
            execution.setLastExecutionErrorResult(response);
            logger.trace(response, ex);
        }
        scheduleJob.setExecution(execution);
    }
}
