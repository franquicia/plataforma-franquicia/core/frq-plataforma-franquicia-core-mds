package com.gs.baz.frq.dashboard.mprs.graficos.g015;

import com.gs.baz.frq.dashboard.dto.graficos.g015.CumplimientoCalificacionDisciplinaNegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoCalificacionDisciplinaNegocioRowMapper implements RowMapper<CumplimientoCalificacionDisciplinaNegocioDTO> {

    private CumplimientoCalificacionDisciplinaNegocioDTO cumplimientoCalificacionDisciplinaNegocioDTO;

    @Override
    public CumplimientoCalificacionDisciplinaNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoCalificacionDisciplinaNegocioDTO = new CumplimientoCalificacionDisciplinaNegocioDTO();
        cumplimientoCalificacionDisciplinaNegocioDTO.setIdCeco(rs.getString("CECO"));
        cumplimientoCalificacionDisciplinaNegocioDTO.setDescripcion(rs.getString("DESCRIPCION"));
        cumplimientoCalificacionDisciplinaNegocioDTO.setCalificacion((BigDecimal) rs.getObject("CALIFICACION"));
        cumplimientoCalificacionDisciplinaNegocioDTO.setIdRango((BigDecimal) rs.getObject("RANGO"));
        return cumplimientoCalificacionDisciplinaNegocioDTO;
    }
}
