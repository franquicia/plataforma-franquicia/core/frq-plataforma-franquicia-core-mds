package com.gs.baz.frq.dashboard.mprs.graficos.g012;

import com.gs.baz.frq.dashboard.dto.graficos.g012.IncumplimientoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class TopIncumplimientoRowMapper implements RowMapper<IncumplimientoDTO> {

    private IncumplimientoDTO topIncumplimientoDTO;

    @Override
    public IncumplimientoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        topIncumplimientoDTO = new IncumplimientoDTO();
        topIncumplimientoDTO.setConteo((BigDecimal) rs.getObject("CONTEO"));
        topIncumplimientoDTO.setPregunta(rs.getString("FCPREGUNTA"));
        return topIncumplimientoDTO;
    }
}
