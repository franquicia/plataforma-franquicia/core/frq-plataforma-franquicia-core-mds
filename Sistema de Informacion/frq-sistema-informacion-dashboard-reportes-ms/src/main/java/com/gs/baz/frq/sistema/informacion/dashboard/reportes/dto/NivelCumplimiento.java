/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del nivel cumplimiento", value = "NivelCumplimiento")
public class NivelCumplimiento {

    @JsonProperty(value = "idRango")
    @ApiModelProperty(notes = "Identificador del rango", example = "1")
    private Integer idRango;

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Total de puntos de venta", example = "90")
    private Integer puntosVenta;

    @JsonProperty(value = "porcentaje")
    @ApiModelProperty(notes = "Porcentaje", example = "90")
    private Double porcentaje;

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    public Double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public String toString() {
        return "NivelCumplimiento{" + "idRango=" + idRango + ", puntosVenta=" + puntosVenta + ", porcentaje=" + porcentaje + '}';
    }

}
