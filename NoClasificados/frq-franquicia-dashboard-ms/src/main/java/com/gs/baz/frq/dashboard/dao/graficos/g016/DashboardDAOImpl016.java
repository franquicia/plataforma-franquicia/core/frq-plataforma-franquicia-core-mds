package com.gs.baz.frq.dashboard.dao.graficos.g016;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g016.CecoDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g016.DistribucionCumplimientoCecoNegocioNivelDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g016.DistribucionCumplimientoNegocioNivelDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g016.RangoDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g016.DistribucionCumplimientoNegocioNivelRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl016 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectDistribucionCumplimientoNegocioNivel;
    private final Logger logger = LogManager.getLogger();
    private String schema;
    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectDistribucionCumplimientoNegocioNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDistribucionCumplimientoNegocioNivel.withSchemaName(schema);
        jdbcSelectDistribucionCumplimientoNegocioNivel.withCatalogName("PAGRAF_CUMPLIM");
        jdbcSelectDistribucionCumplimientoNegocioNivel.withProcedureName("SPDISTCUMP");
        jdbcSelectDistribucionCumplimientoNegocioNivel.returningResultSet("RCL_INFO", new DistribucionCumplimientoNegocioNivelRowMapper());

    }

    private List<DistribucionCumplimientoCecoNegocioNivelDTO> selectRowsDistribucionCumplimientoNacional(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtrosDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtrosDTO.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectDistribucionCumplimientoNegocioNivel.execute(mapSqlParameterSource);
        return (List<DistribucionCumplimientoCecoNegocioNivelDTO>) out.get("RCL_INFO");

    }

    private DistribucionCumplimientoNegocioNivelDTO getDistribucionCumplimientoNivel(List<DistribucionCumplimientoCecoNegocioNivelDTO> distribucionesCumplimientoCecoNacional) throws CustomException {
        DistribucionCumplimientoNegocioNivelDTO distribucionCumplimientoDTO = new DistribucionCumplimientoNegocioNivelDTO();
        Map<Integer, List<DistribucionCumplimientoCecoNegocioNivelDTO>> temp = distribucionesCumplimientoCecoNacional.stream().collect(Collectors.groupingBy(DistribucionCumplimientoCecoNegocioNivelDTO::getIdCeco));
        if (temp != null) {
            List<CecoDTO> cumplimientoCecos = new ArrayList<>();
            temp.forEach((idCeco, cumplimientosNegocioDTO) -> {
                List<RangoDTO> rangos = new ArrayList<>();
                cumplimientosNegocioDTO.forEach(item -> {
                    rangos.add(new RangoDTO(item.getIdRango(), item.getPorcentaje(), item.getPuntosContacto()));
                });
                DistribucionCumplimientoCecoNegocioNivelDTO ceco = distribucionesCumplimientoCecoNacional.stream().filter(item -> item.getIdCeco().equals(idCeco)).collect(Collectors.toList()).get(0);
                cumplimientoCecos.add(new CecoDTO(ceco.getIdCeco(), ceco.getDescripcion(), rangos));
            });
            distribucionCumplimientoDTO.setCecos(cumplimientoCecos);
        }
        return distribucionCumplimientoDTO;
    }

    public DistribucionCumplimientoNegocioNivelDTO getDistribucionCumplimientoNivel(DistribucionCumplimientoCecoNegocioNivelDTO distribucionCumplimientoCecoNacionalDTO) throws CustomException {
        FiltrosDTO filtrosDTO = distribucionCumplimientoCecoNacionalDTO.getFiltros();
        return this.getDistribucionCumplimientoNivel(this.selectRowsDistribucionCumplimientoNacional(filtrosDTO));

    }

}
