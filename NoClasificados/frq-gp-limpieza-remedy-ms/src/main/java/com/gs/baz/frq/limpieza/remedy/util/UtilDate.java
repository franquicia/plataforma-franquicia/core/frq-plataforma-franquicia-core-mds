package com.gs.baz.frq.limpieza.remedy.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

public class UtilDate {

    @SuppressWarnings("all")
    public static String getSysDate(String format) {
        Calendar cal = Calendar.getInstance();
        String formatted = "";
        if (format != null && !format.equals("")) {
            SimpleDateFormat format1 = new SimpleDateFormat(format);
            formatted = format1.format(cal.getTime());
        }
        return formatted;
    }

    @SuppressWarnings("all")
    public static int getAnioActual() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year;
    }

    @SuppressWarnings("all")
    public int getMesActual() {
        int month = Calendar.getInstance().get(Calendar.MONTH);
        return month;
    }

    @SuppressWarnings("all")
    public static Date stringToDate(String fechaOriginal) {
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy HH:mm:ss");
        Date fecha = null;
        try {
            fecha = format.parse(fechaOriginal);
        } catch (ParseException e) {

        }
        return fecha;
    }

    @SuppressWarnings("all")
    public static String dateToString(Date fecha, String format) {
        String formatted = "";
        if (format != null && !format.equals("")) {
            SimpleDateFormat format1 = new SimpleDateFormat(format);
            formatted = format1.format(fecha);
        }
        return formatted;
    }

    @SuppressWarnings("all")
    public String diferenciaFecha(String fechaStart, String fechaFinal) {
        String dateStart = fechaStart;
        String dateFinal = fechaFinal;
        String respuesta = "";
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateFinal);
            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);
            respuesta = (Days.daysBetween(dt1, dt2).getDays() + " dd, ");
            respuesta = respuesta + (Hours.hoursBetween(dt1, dt2).getHours() % 24 + " HH, ");
            respuesta = respuesta + (Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 + " mm, ");
            respuesta = respuesta + (Seconds.secondsBetween(dt1, dt2).getSeconds() % 60 + " ss.");

        } catch (Exception e) {

        }
        return respuesta;
    }

    @SuppressWarnings("all")
    public boolean validaFecha(String fechaStart, String fechaFinal) {
        String dateStart = fechaStart;
        String dateFinal = fechaFinal;
        boolean validacion = false;
        int dia = 0, hor = 0, min = 0, seg = 0;
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date d1 = null;
        Date d2 = null;
        int minDif = 10;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateFinal);
            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);
            dia = Days.daysBetween(dt1, dt2).getDays();

            if (dia == 0) {
                hor = Hours.hoursBetween(dt1, dt2).getHours() % 24;
                if (hor == 0) {
                    min = Minutes.minutesBetween(dt1, dt2).getMinutes() % 60;
                    if (min < (minDif + 1) && min > -(minDif + 1)) {
                        seg = Seconds.secondsBetween(dt1, dt2).getSeconds() % 60;
                        if ((min == minDif && seg == 0) || (min == -minDif && seg == 0)) {
                            validacion = true;
                        } else {
                            validacion = false;
                        }
                        if (min < minDif && min > -minDif) {
                            validacion = true;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return validacion;
    }

    @SuppressWarnings("all")
    public static String validaFecha(Calendar fechaCalendar) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String fechaReturn = "";

        if (fechaCalendar != null) {
            fechaReturn = sdf.format(fechaCalendar.getTime());
        } else {
            fechaReturn = "01-01-0001 00:00:00";
        }

        return fechaReturn;

    }

}
