/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.rest;

import com.gs.baz.frq.documento.sucursal.documentos.dao.DoucmentoSucursalDocumentoDAOImpl;
import com.gs.baz.frq.documento.sucursal.documentos.dto.AltaDocumento;
import com.gs.baz.frq.documento.sucursal.documentos.dto.DocumentoBase;
import com.gs.baz.frq.documento.sucursal.documentos.dto.Documentos;
import com.gs.baz.frq.documento.sucursal.documentos.dto.DocumentosCeco;
import com.gs.baz.frq.documento.sucursal.documentos.dto.ParametrosDocumento;
import com.gs.baz.frq.documento.sucursal.documentos.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "documentos", value = "documentos", description = "Api para la gestión del catalogo de documentos")
@RestController
@RequestMapping("/api-local/documento-sucursal/documento/v1")
public class DocumentoSucursalDocumentoApi {

    @Autowired
    private DoucmentoSucursalDocumentoDAOImpl documentoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idDocumento
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documento", notes = "Obtiene un documento", nickname = "obtieneDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idDocumento}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentoBase obtieneDocumento(@ApiParam(name = "idDocumento", value = "Identificador del documento", example = "1", required = true) @PathVariable("idDocumento") Long idDocumento) throws CustomException, DataNotFoundException {
        DocumentoBase documentoBase = documentoDAOImpl.selectRow(idDocumento);
        if (documentoBase == null) {
            throw new DataNotFoundException();
        }
        return documentoBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documentos", notes = "Obtiene todos los documentos", nickname = "obtieneDocumentos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Documentos obtieneDocumentos() throws CustomException, DataNotFoundException {
        Documentos documentos = new Documentos(documentoDAOImpl.selectAllRows());
        if (documentos.getDocumentos() != null && documentos.getDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return documentos;
    }

    /**
     *
     * @param parametrosDocumento
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear documento", notes = "Agrega un documento", nickname = "creaDocumento")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaDocumento creaDocumento(@ApiParam(name = "ParametrosDocumento", value = "Paramentros para el alta del documento", required = true) @RequestBody ParametrosDocumento parametrosDocumento) throws DataNotInsertedException {
        return documentoDAOImpl.insertRow(parametrosDocumento);
    }

    /**
     *
     * @param parametrosDocumento
     * @param idDocumento
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar documento", notes = "Actualiza un documento", nickname = "actualizaDocumento")
    @RequestMapping(value = "/{idDocumento}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaDocumento(@ApiParam(name = "ParametrosDocumento", value = "Paramentros para la actualización del documento", required = true) @RequestBody ParametrosDocumento parametrosDocumento, @ApiParam(name = "idDocumento", value = "Identificador del documento", example = "1", required = true) @PathVariable("idDocumento") Integer idDocumento) throws DataNotUpdatedException {
        parametrosDocumento.setIdDocumento(idDocumento);
        documentoDAOImpl.updateRow(parametrosDocumento);
        return new SinResultado();
    }

    /**
     *
     * @param idDocumento
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar documento", notes = "Elimina un item de los documentos", nickname = "eliminaDocumento")
    @RequestMapping(value = "/{idDocumento}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaDocumento(@ApiParam(name = "idDocumento", value = "Identificador del documento", example = "1", required = true) @PathVariable("idDocumento") Integer idDocumento) throws DataNotDeletedException {
        documentoDAOImpl.deleteRow(idDocumento);
        return new SinResultado();
    }

    /**
     *
     * @param idCeco
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     * @ApiOperation(value = "Obtiene Documento Ceco", notes = "Obtiene un
     * Documento Ceco", nickname = " documentoCeco")
     */
    @ApiOperation(value = "Obtiene proyectoDocumento", notes = "Obtiene un proyectoDocumento", nickname = "obtieneProyectoDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/ceco/{idCeco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentosCeco obtieneArchivoDocumento(
            @ApiParam(name = "idCeco", value = "Identificador del Ceco", example = "920100", required = true)
            @PathVariable("idCeco") Integer idCeco) throws CustomException, DataNotFoundException {

        DocumentosCeco archivoDocumento = new DocumentosCeco(documentoDAOImpl.selectDocumentoCeco(idCeco));
        if (archivoDocumento.getDocumentos() == null || archivoDocumento.getDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return archivoDocumento;
    }

}
