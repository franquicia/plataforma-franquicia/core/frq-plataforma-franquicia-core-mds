package com.gs.baz.frq.dashboard.dao.graficos.g013;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g013.CumplimientoPuntajeNacionalDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g013.CumplimientoPuntajeNacionalRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl013 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectNegociosDisciplinaTopIncumplimiento;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectNegociosDisciplinaTopIncumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegociosDisciplinaTopIncumplimiento.withSchemaName(schema);
        jdbcSelectNegociosDisciplinaTopIncumplimiento.withCatalogName("PAREPODASHBINC");
        jdbcSelectNegociosDisciplinaTopIncumplimiento.withProcedureName("SPCUMP_PUNTAJE");
        jdbcSelectNegociosDisciplinaTopIncumplimiento.returningResultSet("RCL_INFO", new CumplimientoPuntajeNacionalRowMapper());
    }

    public List<CumplimientoPuntajeNacionalDTO> selectRowsCumplimientoPuntajeNacional(CumplimientoPuntajeNacionalDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEGOCIO", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectNegociosDisciplinaTopIncumplimiento.execute(mapSqlParameterSource);
        return (List<CumplimientoPuntajeNacionalDTO>) out.get("RCL_INFO");
    }

}
