/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.util;

/**
 *
 * @author MADA
 */
public enum Version {

    v1("/v1"),
    v2("/v2"),
    v3("/v3");

    private final String numero;

    private Version(String numero) {
        this.numero = numero;
    }

    /**
     *
     * @param path
     * @return
     */
    public String getNumero() {
        return numero;
    }

}
