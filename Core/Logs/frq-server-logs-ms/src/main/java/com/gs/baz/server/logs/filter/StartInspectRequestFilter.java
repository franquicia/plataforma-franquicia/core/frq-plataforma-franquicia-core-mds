/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.Base64;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class StartInspectRequestFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String requestId = httpServletRequest.getAttribute("requestID").toString();
        String ipOrigin = httpServletRequest.getHeader("x-forwarded-for");
        if (ipOrigin == null) {
            ipOrigin = httpServletRequest.getRemoteAddr();
        }
        String tokenHeader = getBearerTokenHeader(httpServletRequest);
        if (tokenHeader != null && tokenHeader.contains("Bearer")) {
            try {
                tokenHeader = tokenHeader.replace("Bearer ", "");
                String[] partsToken = tokenHeader.split("\\.");
                byte[] tokenDecoded = Base64.getDecoder().decode(partsToken[1]);
                ObjectNode token = new ObjectMapper().readValue(tokenDecoded, ObjectNode.class);
                String empleado = token.get("empleado").asText();
                ThreadContext.put("user", empleado);
            } catch (Exception ex) {

            }
        }
        ThreadContext.put("originIp", ipOrigin);
        ThreadContext.put("requestId", requestId);
        chain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getBearerTokenHeader(HttpServletRequest request) {
        return request.getHeader("Authorization");
    }

    @Override
    public void destroy() {
    }

}
