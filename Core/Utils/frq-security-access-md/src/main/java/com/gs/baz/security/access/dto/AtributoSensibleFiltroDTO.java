/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Filtro para obtener lista de atributos sensibles", value = "AtributoSensibleRequest")
public class AtributoSensibleFiltroDTO {

    @JsonProperty(value = "nombreApi", required = true)
    @ApiModelProperty(notes = "Nombre del Api", example = "sistemas-franquicia-remedy-limpieza-v1", required = true)
    private String nombreApi;

    @JsonProperty(value = "nombreRecurso", required = true)
    @ApiModelProperty(notes = "Nombre del recurso", example = "/incidencias/busquedas", required = true)
    private String nombreRecurso;

    @JsonProperty(value = "tipo", required = true)
    @ApiModelProperty(notes = "Tipo de atributo", example = "entrada", required = true)
    private String tipo;

    public AtributoSensibleFiltroDTO() {
    }

    public AtributoSensibleFiltroDTO(String nombreApi, String nombreRecurso, String tipo) {
        this.nombreApi = nombreApi;
        this.nombreRecurso = nombreRecurso;
        this.tipo = tipo;
    }

    public String getNombreApi() {
        return nombreApi;
    }

    public void setNombreApi(String nombreApi) {
        this.nombreApi = nombreApi;
    }

    public String getNombreRecurso() {
        return nombreRecurso;
    }

    public void setNombreRecurso(String nombreRecurso) {
        this.nombreRecurso = nombreRecurso;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
