package com.gs.baz.frq.dashboard.mprs.graficos.g012;

import com.gs.baz.frq.dashboard.dto.graficos.g012.ImperdonableDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class TopImperdonableRowMapper implements RowMapper<ImperdonableDTO> {

    private ImperdonableDTO topImperdonableDTO;

    @Override
    public ImperdonableDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        topImperdonableDTO = new ImperdonableDTO();
        topImperdonableDTO.setPorcentaje((BigDecimal) rs.getObject("PORCENTAJE"));
        topImperdonableDTO.setPregunta(rs.getString("FCPREGUNTA"));
        topImperdonableDTO.setSucursales((BigDecimal) rs.getObject("SUCURSALES"));
        return topImperdonableDTO;
    }
}
