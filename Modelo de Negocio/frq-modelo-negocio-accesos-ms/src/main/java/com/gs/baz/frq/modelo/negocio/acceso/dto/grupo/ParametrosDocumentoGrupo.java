/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.grupo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosDocumentoGrupo {

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del documento", example = "1", position = -1)
    private Integer idDocumento;

    private transient Integer idGrupo;

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public String toString() {
        return "ParametrosDocumentoGrupo{" + "idDocumento=" + idDocumento + ", idGrupo=" + idGrupo + '}';
    }

}
