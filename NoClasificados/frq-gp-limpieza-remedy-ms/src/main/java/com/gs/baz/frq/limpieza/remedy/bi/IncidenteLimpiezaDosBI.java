package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dto.BandejaIncidentesResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.DatosEmpMttoDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteCuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteLimpiezaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfInformacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Informacion;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class IncidenteLimpiezaDosBI {

    @Autowired
    private IncidenteCuadrillaBI incidenteCuadrillaBI;
    @Autowired
    private ProveedoresBI proveedoresBI;
    @Autowired
    private ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI;
    @Autowired
    private ServiciosRemedyLimpiezaDosBI serviciosRemedyLimpiezaDosBI;
    @Autowired
    private DatosEmpMttoBI datosEmpMttoBI;

    private static final Logger LOGGER = LogManager.getLogger();
    public static final Integer SUPERVISOR = 2;
    private static final String SININFO = "* Sin información";
    private static final String FECHADEFAULT = "0001-01-01 00:00:00";
    private static final String FORMATOFECHA = "yyyy-MM-dd HH:mm:ss";
    private static final Integer UNO = 1, DOS = 2, TRES = 3, CUATRO = 4, CINCO = 5;

    @SuppressWarnings("all")
    public String getBandejaCuadrilla(String proveedor, int idCuadrilla, int zona) {

        String res = null;

        ArrayOfInformacion arrInformacions;
        ArrayOfInformacion arrInformacionsCerrados;

        List<IncidenteCuadrillaDTO> lista
                = incidenteCuadrillaBI.obtieneCuadrillaTK(idCuadrilla, Integer.parseInt(proveedor), zona);

        List<ProveedoresDTO> lista2 = proveedoresBI.obtieneDatos(Integer.parseInt(proveedor));
        String razonSocial = "";
        for (ProveedoresDTO aux2 : lista2) {
            razonSocial = aux2.getNombreCorto();
        }

        arrInformacions = serviciosRemedyLimpiezaDosBI.consultaFoliosProveedor(razonSocial.trim(), "1");
        arrInformacionsCerrados = serviciosRemedyLimpiezaDosBI.consultaFoliosProveedor(razonSocial.trim(), "2");

        BandejaIncidentesResponseDTO envoltorioJsonObj = new BandejaIncidentesResponseDTO();

        // ----------------------Agrupar folios por tipo de
        // estado-----------------------
        ArrayList<Informacion> recibidosPorAtender = new ArrayList<>();
        ArrayList<Informacion> enProceso = new ArrayList<>();
        ArrayList<Informacion> enProcesoDeCierre = new ArrayList<>();
        ArrayList<Informacion> atendidos = new ArrayList<>();
        ArrayList<Informacion> pendientePorAutorizar = new ArrayList<>();

        ArrayList<String> supervisoresGlobal = new ArrayList<>();

        /*
		 * Poner nombres de los supervisores en base al ID
         */
        ArrayList<String> supervisores = matchSupervisores(supervisoresGlobal);

        if (arrInformacions != null) {

            /*
			 * Separa incidentes de la cuadrilla del proveedor
             */
            Informacion[] incidentesAbiertos = separaTicketCuadrilla(arrInformacions, lista);
            Informacion[] incidentesCerrados = separaTicketCuadrilla(arrInformacionsCerrados, lista);

            arrInformacions.setInformacion(incidentesAbiertos);
            arrInformacionsCerrados.setInformacion(incidentesCerrados);

            separaBandejasTecnico(arrInformacions, supervisores, recibidosPorAtender, enProceso,
                    enProcesoDeCierre, atendidos, pendientePorAutorizar);
            separaBandejasTecnico(arrInformacionsCerrados, supervisores, recibidosPorAtender,
                    enProceso, enProcesoDeCierre, atendidos, pendientePorAutorizar);

            ArrayList<IncidenteLimpiezaDTO> arrayRPA = trabajaBandeja(recibidosPorAtender);
            ArrayList<IncidenteLimpiezaDTO> arrayEP = trabajaBandeja(enProceso);
            ArrayList<IncidenteLimpiezaDTO> arrayEPC = trabajaBandeja(enProcesoDeCierre);
            ArrayList<IncidenteLimpiezaDTO> arrayAtendidos = trabajaBandeja(atendidos);
            ArrayList<IncidenteLimpiezaDTO> arrayPPA = trabajaBandeja(pendientePorAutorizar);

            envoltorioJsonObj.setRecibidoPorAtender(arrayRPA == null ? new ArrayList<>() : arrayRPA);
            envoltorioJsonObj.setEnProceso(arrayEP == null ? new ArrayList<>() : arrayEP);
            envoltorioJsonObj.setEnProcesoDeCierre(arrayEPC == null ? new ArrayList<>() : arrayEPC);
            envoltorioJsonObj.setAtendidos(arrayAtendidos == null ? new ArrayList<>() : arrayAtendidos);
            envoltorioJsonObj.setPendientesPorAutorizar(arrayPPA == null ? new ArrayList<>() : arrayPPA);
            envoltorioJsonObj.setSupervisores((supervisores == null ? new ArrayList<>() : supervisores));
        }

        res = envoltorioJsonObj.toString();

        if (res != null) {
            return res;
        } else {
            return "{}";
        }

    }

    @SuppressWarnings("all")
    public ArrayList<String> matchSupervisores(ArrayList<String> supervisores) {

        // -------------------------------------------------------------------------------
        ArrayList<String> supervisoresJsonArray = new ArrayList<>();
        if (!supervisores.isEmpty()) {
            for (String sup : supervisores) {
                if (!sup.matches("[^0-9]*")) {

                    List<DatosEmpMttoDTO> datosSupervisor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(sup));
                    if (datosSupervisor != null && !datosSupervisor.isEmpty()) {
                        for (DatosEmpMttoDTO emp_aux : datosSupervisor) {
                            supervisoresJsonArray.add(emp_aux.getNombre());
                        }
                    }
                } else {
                    LOGGER.info("No es un número");
                }
            }
        }

        return supervisoresJsonArray;

    }

    @SuppressWarnings("all")
    public Informacion[] separaTicketCuadrilla(ArrayOfInformacion incidentes, List<IncidenteCuadrillaDTO> incidentesCuadrillaLista) {

        Informacion[] incidentesArray = incidentes.getInformacion();
        ArrayList<Informacion> incidentesCuadrilla = new ArrayList<>();

        if (incidentesArray != null) {

            if (incidentesCuadrillaLista != null && !incidentesCuadrillaLista.isEmpty()) {
                //Separa los incidente del proveddor
                for (IncidenteCuadrillaDTO incidente : incidentesCuadrillaLista) {
                    for (int i = 0; i < incidentesArray.length; i++) {
                        if (incidente.getIncidente().equals(String.valueOf(incidentesArray[i].getIdIncidencia()))) {
                            incidentesCuadrilla.add(incidentesArray[i]);
                            break;
                        }
                    }

                }
            }
        }

        Informacion[] regreso = null;

        if (!incidentesCuadrilla.isEmpty()) {
            regreso = incidentesCuadrilla.toArray(new Informacion[incidentesCuadrilla.size()]);
        }

        return regreso;
    }

    @SuppressWarnings("all")
    public void separaBandejasTecnico(ArrayOfInformacion bandejaPrincipal, ArrayList<String> supervisores,
            ArrayList<Informacion> recibidosPorAtender,
            ArrayList<Informacion> enProceso,
            ArrayList<Informacion> enProcesoDeCierre,
            ArrayList<Informacion> atendidos,
            ArrayList<Informacion> PPA) {

        if (bandejaPrincipal != null) {
            Informacion[] arregloInc = bandejaPrincipal.getInformacion();
            if (arregloInc != null) {
                for (Informacion aux : arregloInc) {
                    if (aux.getEstado().contains("Recibido por atender")
                            && (aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("abierto")
                            || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("revisado"))) {
                        recibidosPorAtender.add(aux);
                    }
                    if (aux.getEstado().contains("En Proceso")) {
                    }
                    if (aux.getEstado().contains("En Recepción")
                            && (aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("en validacion")
                            || aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("rechazado por cliente"))) {
                        enProcesoDeCierre.add(aux);
                    }
                    if (aux.getEstado().contains("Atendido") && aux.getMotivoEstado().toLowerCase(Locale.ROOT).contains("atendido")) {
                        atendidos.add(aux);
                    }
                    if (aux.getEstado().contains("Pendiente")) {
                    }
                    if (supervisores.isEmpty()) {
                        supervisores.add(aux.getIdCorpSupervisor());
                    } else {
                        boolean flagSup = false;
                        for (String sup : supervisores) {
                            if (aux.getIdCorpSupervisor() != null) {
                                if (sup != null && sup.equals(aux.getIdCorpSupervisor())) {
                                    flagSup = true;
                                    break;
                                }
                            }
                        }
                        if (!flagSup) {
                            if (aux.getIdCorpSupervisor() != null) {
                                supervisores.add(aux.getIdCorpSupervisor());
                            }
                        }
                    }

                }
            }
        }
    }

    @SuppressWarnings("all")
    public ArrayList<IncidenteLimpiezaDTO> trabajaBandeja(ArrayList<Informacion> bandeja) {

        ArrayList<IncidenteLimpiezaDTO> respuesta = null;

        if (!bandeja.isEmpty()) {

            respuesta = arrOrdenadoSupervisorJuntos(bandeja);

        }

        return respuesta;

    }

    @SuppressWarnings("all")
    public ArrayList<IncidenteLimpiezaDTO> arrOrdenadoSupervisorJuntos(ArrayList<Informacion> juntos) {

        ArrayList<IncidenteLimpiezaDTO> arrJsonArray = new ArrayList<>();

        armaJson(juntos, arrJsonArray);

        return arrJsonArray;
    }

    @SuppressWarnings("all")
    public void armaJson(ArrayList<Informacion> listaTickets, ArrayList<IncidenteLimpiezaDTO> arrayRespuesta) {

        List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
        HashMap<String, DatosEmpMttoDTO> empleadosHashMap = new HashMap<>();

        while (!listaTickets.isEmpty()) {
            int pos = 0;

            for (int i = 0; i < listaTickets.size(); i++) {
                if (listaTickets.get(pos).getFechaCreacion().compareTo(listaTickets.get(i).getFechaCreacion()) < 0) {
                    pos = i;
                }
            }

            Informacion aux = listaTickets.get(pos);

            IncidenteLimpiezaDTO incJsonObj = new IncidenteLimpiezaDTO();

            incJsonObj.setIdIncidencia(aux.getIdIncidencia());
            incJsonObj.setEstado(aux.getEstado());
            incJsonObj.setFalla(aux.getFalla());
            incJsonObj.setIncidencia(aux.getIncidencia());
            incJsonObj.setMotivoEstado(aux.getMotivoEstado());

            incJsonObj.setNombreCliente(aux.getNombreCliente());
            incJsonObj.setIdCliente(aux.getIdCorpCliente());
            incJsonObj.setIdSupervisor(aux.getIdCorpSupervisor());
            incJsonObj.setIdCoordinador("0");

            String idCliente = aux.getIdCorpCliente();

            if (idCliente != null) {
                if (!empleadosHashMap.containsKey(idCliente.trim())) {
                    List<DatosEmpMttoDTO> datosCliente = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpCliente()));

                    if (!datosCliente.isEmpty() && datosCliente != null) {
                        for (DatosEmpMttoDTO emp_aux : datosCliente) {
                            incJsonObj.setPuestoCliente(emp_aux.getDescripcion());
                            incJsonObj.setTelefonoCliente(emp_aux.getTelefono());
                            empleadosHashMap.put(idCliente.trim(), emp_aux);
                        }
                    } else {
                        incJsonObj.setPuestoCliente(SININFO);
                        incJsonObj.setTelefonoCliente(SININFO);
                    }
                } else {
                    DatosEmpMttoDTO empAux = empleadosHashMap.get(idCliente.trim());
                    incJsonObj.setPuestoCliente(empAux.getDescripcion());
                    incJsonObj.setTelefonoCliente(empAux.getTelefono());
                }
            } else {
                incJsonObj.setPuestoCliente(SININFO);
                incJsonObj.setTelefonoCliente(SININFO);
            }
            String idCord = null;
            if (idCord == null) {
                incJsonObj.setTelefonoCoordinador(SININFO);
            }

            if (idCord == null) {
                incJsonObj.setNombreCoordinador(SININFO);
            }

            String idSup = null;

            if (idSup == null) {
                incJsonObj.setTelefonoSupervisor(SININFO);
            }
            if (idSup != null) {
                if (!idSup.matches("[^0-9]*")) {

                    if (!empleadosHashMap.containsKey(idSup.trim())) {
                        List<DatosEmpMttoDTO> datosSupervidor = datosEmpMttoBI.obtieneDatos(Integer.parseInt(aux.getIdCorpSupervisor()));

                        if (!datosSupervidor.isEmpty() && datosSupervidor != null) {
                            for (DatosEmpMttoDTO emp_aux : datosSupervidor) {
                                incJsonObj.setNombreSupervisor(emp_aux.getDescripcion() + " " + emp_aux.getNombre());
                                incJsonObj.setNombreSupervisorAuxiliar(emp_aux.getNombre());
                                empleadosHashMap.put(idSup.trim(), emp_aux);
                            }
                        } else {
                            incJsonObj.setNombreSupervisor(SININFO);
                            incJsonObj.setNombreSupervisorAuxiliar(SININFO);
                        }
                    } else {
                        DatosEmpMttoDTO empAux = empleadosHashMap.get(idSup.trim());
                        incJsonObj.setNombreSupervisor(empAux.getDescripcion() + " " + empAux.getNombre());
                        incJsonObj.setNombreSupervisorAuxiliar(empAux.getNombre());
                    }

                } else {
                    incJsonObj.setNombreSupervisor(SININFO);
                    incJsonObj.setNombreSupervisorAuxiliar(SININFO);
                }
            } else {
                incJsonObj.setNombreSupervisor(SININFO);
                incJsonObj.setNombreSupervisorAuxiliar(SININFO);
            }

            incJsonObj.setNotas(aux.getNotas());
            incJsonObj.setNumeroIncidenteProveedor(aux.getNoTicketProveedor());
            incJsonObj.setProveedor(aux.getProveedor());

            if (aux.getProveedor() != null) {
                if (!lista.isEmpty()) {
                    for (ProveedoresDTO aux3 : lista) {
                        if (aux3.getStatus().contains("Activo")) {
                            if (aux.getProveedor().toLowerCase(Locale.ROOT).trim()
                                    .contains(aux3.getNombreCorto().toLowerCase(Locale.ROOT).trim())) {//cambieRazonSocial
                                incJsonObj.setNombreCortoProveedor(aux3.getNombreCorto());
                                incJsonObj.setMenu(aux3.getMenu());
                                incJsonObj.setRazonSocial(aux3.getRazonSocial());
                                incJsonObj.setIdProveedor(aux3.getIdProveedor() + "");
                                break;
                            }

                        }
                    }
                }
            } else {
                String nomCorto = null;
                incJsonObj.setNombreCortoProveedor(nomCorto);
                incJsonObj.setMenu(nomCorto);
                incJsonObj.setRazonSocial(nomCorto);
                incJsonObj.setIdProveedor(nomCorto);
            }

            incJsonObj.setPuestoAlterno(aux.getPuestoAlterno());
            incJsonObj.setNombreSucursal(aux.getSucursal());
            incJsonObj.setTipoFalla(aux.getTipoFalla());
            incJsonObj.setUsuarioAlterno(aux.getUsrAlterno());
            incJsonObj.setTelefonoContactoAlterno(aux.getTelCliente());

            incJsonObj.setMontoEstimado(aux.getMontoEstimado().toString());
            incJsonObj.setNumeroSucursal(aux.getNoSucursal());
            Date date;
            SimpleDateFormat format1 = new SimpleDateFormat(FORMATOFECHA);
            String date1;
            incJsonObj.setFechaCierreTecnicoDos(FORMATOFECHA);
            date = aux.getFechaCreacion().getTime();
            date1 = format1.format(date);
            incJsonObj.setFechaCreacion(date1);
            date = aux.getFechaModificacion().getTime();
            date1 = format1.format(date);
            incJsonObj.setFechaModificacion(date1);
            incJsonObj.setFechaEstimada(FORMATOFECHA);
            incJsonObj.setPrioridad(aux.getPrioridad().getValue());
            incJsonObj.setTipoCierreProvedor(aux.getTipoCierreProveedor());

            String autCleinte = aux.getAutorizacionCliente();
            if (autCleinte != null && !autCleinte.equals("")) {
                if (autCleinte.contains("Rechazado")) {
                    incJsonObj.setAutorizacionCliente("2");
                } else {
                    incJsonObj.setAutorizacionCliente("1");
                }
            } else {
                incJsonObj.setAutorizacionCliente("0");
            }

            if (aux.getTipoAtencion() != null) {
                incJsonObj.setTipoAtencion(aux.getTipoAtencion().getValue());
            } else {
                String x = null;
                incJsonObj.setTipoAtencion(x);
            }
            if (aux.getOrigen() != null) {
                incJsonObj.setOrigen(aux.getOrigen().getValue());
            } else {
                String x = null;
                incJsonObj.setOrigen(x);
            }

            //-----------------Obtiene coordenadas de inicio y fin de incidente -------------------
            incJsonObj.setLatitudFin("");
            incJsonObj.setLongitudFin("");

            incJsonObj.setFechaInicioAtencion(FECHADEFAULT);

            incJsonObj.setIdCuadrilla(0);
            incJsonObj.setLider("");
            incJsonObj.setCorreo("");
            incJsonObj.setSegundoMando("");
            incJsonObj.setIdZona(0);

            incJsonObj.setBanderaMontoAutorizado(0);
            incJsonObj.setBanderaAplazamiento(0);
            incJsonObj.setBanderas(0);

            incJsonObj.setFechaAtencion(FECHADEFAULT);
            incJsonObj.setRechazoCliente(FECHADEFAULT);
            incJsonObj.setFechaCierreTecnico(FECHADEFAULT);
            incJsonObj.setFechaCierre(FECHADEFAULT);
            incJsonObj.setFechaAtencionGarantia(FECHADEFAULT);
            incJsonObj.setFechaCierreGarantia(FECHADEFAULT);
            incJsonObj.setFechaRechazoProveedor(FECHADEFAULT);
            incJsonObj.setBanderaRechazoCierrePrevio(false);

            if (aux.getEvalCliente() != null) {

                int cal = 0;
                if (aux.getEvalCliente().getValue().contains("NA")) {
                    cal = UNO;
                }
                if (aux.getEvalCliente().getValue().contains("E0a50")) {
                    cal = DOS;
                }
                if (aux.getEvalCliente().getValue().contains("E51a69")) {
                    cal = TRES;
                }
                if (aux.getEvalCliente().getValue().contains("E70a89")) {
                    cal = CUATRO;
                }
                if (aux.getEvalCliente().getValue().contains("E90a100")) {
                    cal = CINCO;
                }

                incJsonObj.setEvaluacionCliente(cal + "");

            } else {
                incJsonObj.setEvaluacionCliente(0 + "");
            }

            incJsonObj.setMontoReal("0.0");
            incJsonObj.setFechaCierreAdministrativo(FECHADEFAULT);
            incJsonObj.setDiasAplazammiento("0");

            incJsonObj.setMensajeProveedor("");
            incJsonObj.setAutorizacionProveedor("");

            incJsonObj.setFechaAutorizaProveedor(FECHADEFAULT);
            incJsonObj.setDeclinarProveedor("");

            incJsonObj.setLatitudInicio("");
            incJsonObj.setLongitudInicio("");

            incJsonObj.setFechaCancelacion(FECHADEFAULT);

            //-------------------------------------------------------------------------------------
            incJsonObj.setClienteAvisado("");
            incJsonObj.setResolucion("");
            incJsonObj.setMontoEstimadoAutorizado("0.0");

            arrayRespuesta.add(incJsonObj);

            listaTickets.remove(pos);
        }

    }

}
