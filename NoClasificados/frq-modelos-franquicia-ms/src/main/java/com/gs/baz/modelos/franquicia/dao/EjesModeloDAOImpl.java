package com.gs.baz.modelos.franquicia.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.modelos.franquicia.dao.util.GenericEjesModeloDAO;
import com.gs.baz.modelos.franquicia.dto.EjeModeloDTO;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class EjesModeloDAOImpl extends DefaultDAO implements GenericEjesModeloDAO<EjeModeloDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINMODEJE");
        jdbcInsert.withProcedureName("SP_INS_MODEJE");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINMODEJE");
        jdbcUpdate.withProcedureName("SP_ACT_MODEJE");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINMODEJE");
        jdbcDelete.withProcedureName("SP_DEL_MODEJE");

    }

    @Override
    public EjeModeloDTO insertRow(EjeModeloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            mapSqlParameterSource.addValue("PA_FIIDEJE", entityDTO.getIdEje());
            mapSqlParameterSource.addValue("PA_FIPONDE", entityDTO.getPonderacion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdModeloEje((BigDecimal) out.get("PA_FIIDMODEJE"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of Ejes Modelo ejeModeloDTO " + entityDTO));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Ejes Modelo ejeModeloDTO " + entityDTO), ex);
        }
    }

    @Override
    public EjeModeloDTO updateRow(EjeModeloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODEJE", entityDTO.getIdModeloEje());
            mapSqlParameterSource.addValue("PA_FIIDEJE", entityDTO.getIdEje());
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            mapSqlParameterSource.addValue("PA_FIPONDE", entityDTO.getPonderacion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Ejes Modelo idModEje " + entityDTO.getIdModeloEje()));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Ejes Modelo idModEje " + entityDTO.getIdModeloEje()), ex);
        }
    }

    @Override
    public EjeModeloDTO deleteRow(EjeModeloDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODEJE", entityDTO.getIdModeloEje());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete row of Ejes Modelo idModEje " + entityDTO.getIdModeloEje()));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception to delete row of Ejes Modelo idModEje " + entityDTO.getIdModeloEje()), ex);
        }
    }
}
