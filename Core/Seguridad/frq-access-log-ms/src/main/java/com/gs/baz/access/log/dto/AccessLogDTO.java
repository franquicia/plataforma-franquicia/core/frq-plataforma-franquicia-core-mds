/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class AccessLogDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_access_log")
    private Integer idAccessLog;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "http_session")
    private String httpSession;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_creacion")
    private String fechaCreacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_fin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ip_origin")
    private String ipOrigin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ip_local")
    private String ipLocal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_dispositivo")
    private Integer idDispositivo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_navegador")
    private Integer idNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_navegador")
    private String nombreNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipo_navegador")
    private String tipoNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "grupo_navegador")
    private String grupoNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "manofactura_navegador")
    private String manofacturaNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "motor_renderizacion_navegador")
    private String motorRenderizacionNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "version_navegador")
    private String versionNavegador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_sistema_operativo")
    private Integer idSistemaOperativo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_sistema_operativo")
    private String nombreSistemaOperativo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipo_dispositivo")
    private String tipoDispositivo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "grupo_sistema_operativo")
    private String grupoSistemaOperativo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "manofactura_sistema_operativo")
    private String manofacturaSistemaOperativo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_tipo_cierre")
    private Integer idTipoCierre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public AccessLogDTO() {
    }

    public void setUserAgent(UserAgent userAgent) {
        this.setGrupoNavegador(userAgent.getBrowser().getGroup().getName());
        this.setGrupoSistemaOperativo(userAgent.getOperatingSystem().getGroup().getName());
        this.setIdDispositivo(new BigDecimal(userAgent.getId()));
        this.setIdNavegador(new BigDecimal(userAgent.getBrowser().getId()));
        this.setIdSistemaOperativo(new BigDecimal(userAgent.getOperatingSystem().getId()));
        this.setManofacturaNavegador(userAgent.getBrowser().getManufacturer().getName());
        this.setManofacturaSistemaOperativo(userAgent.getOperatingSystem().getManufacturer().getName());
        this.setMotorRenderizacionNavegador(userAgent.getBrowser().getRenderingEngine().getName());
        this.setNombreNavegador(userAgent.getBrowser().getName());
        this.setNombreSistemaOperativo(userAgent.getOperatingSystem().getName());
        this.setTipoDispositivo(userAgent.getOperatingSystem().getDeviceType().getName());
        this.setTipoNavegador(userAgent.getBrowser().getBrowserType().getName());
        this.setVersionNavegador((userAgent.getBrowserVersion() != null) ? userAgent.getBrowserVersion().getVersion() : new Version("0", "0", "0").getVersion());
    }

    public Integer getIdAccessLog() {
        return idAccessLog;
    }

    public void setIdAccessLog(BigDecimal idAccessLog) {
        this.idAccessLog = (idAccessLog == null ? null : idAccessLog.intValue());;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public String getHttpSession() {
        return httpSession;
    }

    public void setHttpSession(String httpSession) {
        this.httpSession = httpSession;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIpOrigin() {
        return ipOrigin;
    }

    public void setIpOrigin(String ipOrigin) {
        this.ipOrigin = ipOrigin;
    }

    public String getIpLocal() {
        return ipLocal;
    }

    public void setIpLocal(String ipLocal) {
        this.ipLocal = ipLocal;
    }

    public Integer getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(BigDecimal idDispositivo) {
        this.idDispositivo = (idDispositivo == null ? null : idDispositivo.intValue());
    }

    public Integer getIdNavegador() {
        return idNavegador;
    }

    public void setIdNavegador(BigDecimal idNavegador) {
        this.idNavegador = (idNavegador == null ? null : idNavegador.intValue());
    }

    public String getNombreNavegador() {
        return nombreNavegador;
    }

    public void setNombreNavegador(String nombreNavegador) {
        this.nombreNavegador = nombreNavegador;
    }

    public String getTipoNavegador() {
        return tipoNavegador;
    }

    public void setTipoNavegador(String tipoNavegador) {
        this.tipoNavegador = tipoNavegador;
    }

    public String getGrupoNavegador() {
        return grupoNavegador;
    }

    public void setGrupoNavegador(String grupoNavegador) {
        this.grupoNavegador = grupoNavegador;
    }

    public String getManofacturaNavegador() {
        return manofacturaNavegador;
    }

    public void setManofacturaNavegador(String manofacturaNavegador) {
        this.manofacturaNavegador = manofacturaNavegador;
    }

    public String getMotorRenderizacionNavegador() {
        return motorRenderizacionNavegador;
    }

    public void setMotorRenderizacionNavegador(String motorRenderizacionNavegador) {
        this.motorRenderizacionNavegador = motorRenderizacionNavegador;
    }

    public String getVersionNavegador() {
        return versionNavegador;
    }

    public void setVersionNavegador(String versionNavegador) {
        this.versionNavegador = versionNavegador;
    }

    public Integer getIdSistemaOperativo() {
        return idSistemaOperativo;
    }

    public void setIdSistemaOperativo(BigDecimal idSistemaOperativo) {
        this.idSistemaOperativo = (idNavegador == null ? null : idNavegador.intValue());
    }

    public String getNombreSistemaOperativo() {
        return nombreSistemaOperativo;
    }

    public void setNombreSistemaOperativo(String nombreSistemaOperativo) {
        this.nombreSistemaOperativo = nombreSistemaOperativo;
    }

    public String getTipoDispositivo() {
        return tipoDispositivo;
    }

    public void setTipoDispositivo(String tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }

    public String getGrupoSistemaOperativo() {
        return grupoSistemaOperativo;
    }

    public void setGrupoSistemaOperativo(String grupoSistemaOperativo) {
        this.grupoSistemaOperativo = grupoSistemaOperativo;
    }

    public String getManofacturaSistemaOperativo() {
        return manofacturaSistemaOperativo;
    }

    public void setManofacturaSistemaOperativo(String manofacturaSistemaOperativo) {
        this.manofacturaSistemaOperativo = manofacturaSistemaOperativo;
    }

    public Integer getIdTipoCierre() {
        return idTipoCierre;
    }

    public void setIdTipoCierre(BigDecimal idTipoCierre) {
        this.idTipoCierre = (idTipoCierre == null ? null : idTipoCierre.intValue());;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "AccessLogDTO{" + "idAccessLog=" + idAccessLog + ", idUsuario=" + idUsuario + ", httpSession=" + httpSession + ", fechaCreacion=" + fechaCreacion + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", ipOrigin=" + ipOrigin + ", ipLocal=" + ipLocal + ", idDispositivo=" + idDispositivo + ", idNavegador=" + idNavegador + ", nombreNavegador=" + nombreNavegador + ", tipoNavegador=" + tipoNavegador + ", grupoNavegador=" + grupoNavegador + ", manofacturaNavegador=" + manofacturaNavegador + ", motorRenderizacionNavegador=" + motorRenderizacionNavegador + ", versionNavegador=" + versionNavegador + ", idSistemaOperativo=" + idSistemaOperativo + ", nombreSistemaOperativo=" + nombreSistemaOperativo + ", tipoDispositivo=" + tipoDispositivo + ", grupoSistemaOperativo=" + grupoSistemaOperativo + ", manofacturaSistemaOperativo=" + manofacturaSistemaOperativo + ", idTipoCierre=" + idTipoCierre + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
