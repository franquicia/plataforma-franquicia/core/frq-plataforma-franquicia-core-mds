package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.LogDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.LogDTO;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class LogBI {

    @Autowired
    private LogDAOImpl logDAO;

    private static final Logger LOGGER = LogManager.getLogger();

    public LogBI() {
        super();
    }

    public List<LogDTO> obtieneErrores(String fecha) {
        List<LogDTO> listaErrores = null;
        try {
            listaErrores = logDAO.obtieneErrores(fecha);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla de Errores", e);
        }

        return listaErrores;
    }

    public boolean insertaerror(int codigo, String mensaje, String origen) {
        boolean respuesta = false;
        try {
            respuesta = logDAO.inserta(codigo, mensaje, origen);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible insertar los datos", e);
        }

        return respuesta;
    }

}
