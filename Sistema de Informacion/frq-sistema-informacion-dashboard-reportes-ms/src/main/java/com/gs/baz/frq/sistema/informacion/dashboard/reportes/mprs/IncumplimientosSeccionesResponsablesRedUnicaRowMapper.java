/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.IncumplimientoSeccionResponsableRedUnica;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class IncumplimientosSeccionesResponsablesRedUnicaRowMapper implements RowMapper<IncumplimientoSeccionResponsableRedUnica> {

    @Override
    public IncumplimientoSeccionResponsableRedUnica mapRow(ResultSet rs, int rowNum) throws SQLException {
        IncumplimientoSeccionResponsableRedUnica incumplimientoSeccionResponsableRedUnica = new IncumplimientoSeccionResponsableRedUnica();
        incumplimientoSeccionResponsableRedUnica.setResponsable(rs.getString("FCRESPONSABLE"));
        incumplimientoSeccionResponsableRedUnica.setIdSeccion(rs.getInt("FIID_MODULO"));
        incumplimientoSeccionResponsableRedUnica.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        incumplimientoSeccionResponsableRedUnica.setDescripcion(rs.getString("FCDESCRIPCION"));
        incumplimientoSeccionResponsableRedUnica.setPuntosVenta(rs.getInt("TOTALPDVINCUMPLIO"));
        incumplimientoSeccionResponsableRedUnica.setPorcentaje(rs.getInt("PORCENTAJE"));
        return incumplimientoSeccionResponsableRedUnica;
    }
}
