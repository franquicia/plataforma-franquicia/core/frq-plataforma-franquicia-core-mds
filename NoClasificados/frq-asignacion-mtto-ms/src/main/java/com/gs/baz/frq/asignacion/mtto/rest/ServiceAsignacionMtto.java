/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.rest;

import com.gs.baz.frq.asignacion.mtto.dao.AsignacionMttoDAOImpl;
import com.gs.baz.frq.asignacion.mtto.dto.ArbolUsuarioDTO;
import com.gs.baz.frq.asignacion.mtto.dto.AsignacionMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ExcelRespuestasDTO;
import com.gs.baz.frq.asignacion.mtto.dto.LugarMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteCheckAsistMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteCheckInnMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteChecklistMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralSemanalDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoTerritorioDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteProgramadasDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/asignacion-mtto")
@Component("FRQServiceAsignacionMtto")

public class ServiceAsignacionMtto {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    private AsignacionMttoDAOImpl asignacionMttoDAO;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post/alta/asignacionSupMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaAsignacionSupMtto(@RequestBody AsignacionMttoDTO asignacion) throws Exception {

        if (asignacion.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Asignacion-Mtto"));
        }
        return asignacionMttoDAO.insertaAsignacionMtto(asignacion);
    }

    @RequestMapping(value = "secure/put/asignacionSupMtto", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int actualizaAsignacionSupMtto(@RequestBody AsignacionMttoDTO asignacion) throws Exception {

        if (asignacion.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("actualiza Asignacion-Mtto"));
        }
        return asignacionMttoDAO.actualizaAsignacionMtto(asignacion);
    }

    @RequestMapping(value = "secure/post/obtieneAsignacionesSupMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AsignacionMttoDTO> postObtieneAsignacionesMtto(@RequestBody AsignacionMttoDTO asignacion)
            throws CustomException {

        return (List<AsignacionMttoDTO>) asignacionMttoDAO.obtieneAsignacionesSupMtto(asignacion);
    }

    @RequestMapping(value = "secure/delete/asignacionMtto", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaAsignacionMtto(@RequestBody AsignacionMttoDTO elimina) throws CustomException {
        if (elimina.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("delete asignacion-Mtto"));

        } else {

            return asignacionMttoDAO.eliminaAsignacionMtto(elimina);
        }
    }
    
    @RequestMapping(value = "secure/post/ejecuta/eliminaAsignacionMttoArray", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaAsignacionMttoArray(@RequestBody List<AsignacionMttoDTO> asignacion) throws Exception {
        ArrayList<AsignacionMttoDTO> listaNoCargados = new ArrayList<AsignacionMttoDTO>();

        int estatusAsignaciones = 0;

        if (asignacion == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("elimina asignacion sup-mtto"));
        } else {

            for (AsignacionMttoDTO aux : asignacion) {
            	AsignacionMttoDTO asignacion1 = new AsignacionMttoDTO();
                asignacion1.setIdProyecto(aux.getIdProyecto());
                asignacion1.setIdCeco(aux.getIdCeco());


                estatusAsignaciones = asignacionMttoDAO.eliminaAsignacionMtto(asignacion1);

            }
        }
        return estatusAsignaciones;
    }


    @RequestMapping(value = "secure/post/ejecuta/asignacionesSupMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postEjecutaAsignaciones(@RequestBody List<AsignacionMttoDTO> asignacion) throws Exception {
        ArrayList<AsignacionMttoDTO> listaNoCargados = new ArrayList<AsignacionMttoDTO>();
        // List<AsignacionMttoDTO> listaNoCargados = null;
        int estatusAsignaciones = 0;
        int estatusAsignaciones2 = 0;
        if (asignacion == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Asignacion-mtto"));
        } else {

            for (AsignacionMttoDTO aux : asignacion) {
                String ceco = "" + aux.getIdCeco();

                int agrupa = 0;
                int proyecto = 0;

                // (aux.getTipoVisita().equals("EKT");
                if (aux.getNegocio().equals("EKT")) {
                    if (ceco.length() == 3) {
                        ceco = "920" + ceco;
                    } else {
                        ceco = "92" + ceco;
                    }
                    agrupa = 15;
                    proyecto = 15;
                } else if (aux.getNegocio().equals("DAZ")) {
                    if (ceco.length() == 3) {
                        ceco = "480" + ceco;
                    } else {
                        ceco = "48" + ceco;
                    }
                    agrupa = 16;
                    proyecto = 16;
                } else if (aux.getNegocio().equals("OCC")) {
                    if (ceco.length() == 3) {
                        ceco = "230" + ceco;
                    } else {
                        ceco = "23" + ceco;
                    }
                    agrupa = 17;
                    proyecto = 17;
//CHECAR LOS PREFIJOS
                } else if (aux.getNegocio().equals("PP")) {
                    if (ceco.length() == 3) {
                        ceco = "620" + ceco;
                    } else {
                        ceco = "62" + ceco;
                    }
                    agrupa = 19;
                    proyecto = 19;

                } else if (aux.getNegocio().equals("ATM")) {
                    if (ceco.length() == 3) {
                        ceco = "690" + ceco;
                    } else {
                        ceco = "69" + ceco;
                    }
                    agrupa = 20;
                    proyecto = 20;

                } else if (aux.getNegocio().equals("MAZ")) {
                    if (ceco.length() == 3) {
                        ceco = "770" + ceco;
                    } else {
                        ceco = "77" + ceco;
                    }
                    agrupa = 21;
                    proyecto = 21;

                } else if (aux.getNegocio().equals("C3")) {
                    //CANALES DE TERCEROS
                    if (ceco.length() == 3) {
                        ceco = "550" + ceco;
                    } else {
                        ceco = "55" + ceco;
                    }
                    agrupa = 22;
                    proyecto = 22;

                } else if (aux.getNegocio().equals("ITK")) {
                    // ITALIKAS
                    if (ceco.length() == 3) {
                        ceco = "670" + ceco;
                    } else {
                        ceco = "67" + ceco;
                    }
                    agrupa = 18;
                    proyecto = 18;

                } else {

                    if (ceco.length() == 3) {
                        ceco = "920" + ceco;
                    } else {
                        ceco = "92" + ceco;
                    }
                    agrupa = 15;
                    proyecto = 15;
                }

                /*
				 * if(aux.getTipoVisita().equals("CDT")) { negocio=2; }
				 * if(aux.getTipoVisita().equals("ITK")) { negocio=3; }
                 */
                AsignacionMttoDTO asignacion1 = new AsignacionMttoDTO();
                asignacion1.setIdCeco(Integer.parseInt(ceco));
                asignacion1.setUsuarioAsigna(aux.getUsuarioAsigna());
                asignacion1.setTipoVisita(aux.getTipoVisita());
                // proyecto PROD SUPERVISION MTTO
                asignacion1.setIdProyecto(proyecto);
                asignacion1.setIdUsuario(aux.getIdUsuario());
                asignacion1.setIdAgrupa(agrupa);
                asignacion1.setDescripcion(aux.getDescripcion()); // aux.getDescripcion()
                asignacion1.setFechaProgramada(aux.getFechaProgramada());
                asignacion1.setIdPerfil(1);
                asignacion1.setNegocio(aux.getNegocio());
                asignacion1.setOrdenActivos(aux.getOrdenActivos());
                asignacion1.setComentarioTicketRelacionados(aux.getComentarioTicketRelacionados());
                asignacion1.setComentarioReprogramacion(aux.getComentarioReprogramacion());

                /*
				 * SqlParameterSource in = new MapSqlParameterSource() .addValue("PA_CECO",
				 * bean.getIdCeco()) .addValue("PA_USU",bean.getUsuarioAsigna())
				 * .addValue("PA_PROY",bean.getIdProyecto())
				 * .addValue("PA_USUASIG",bean.getIdUsuario())
				 * .addValue("PA_OBSERV",bean.getDescripcion())
				 * .addValue("PA_IDAGRUPA",bean.getIdAgrupa()) //YYYYMMDD
				 * .addValue("PA_FECHAPROG",bean.getFechaProgramada());
				 * 
				 * AsignacionMttoDTO asignacion2= new AsignacionMttoDTO();
				 * asignacion2.setIdCeco(Integer.parseInt(ceco));
				 * asignacion2.setIdUsuario(aux.getNumeroGerente()); asignacion2.setIdPerfil(2);
				 * asignacion2.setIdNegocio(negocio);
				 * asignacion2.setIdGerente(aux.getNumeroGerente());
				 *
                 */
                estatusAsignaciones = asignacionMttoDAO.insertaAsignacionMtto(asignacion1);
                // verificar si funciona
                // if(estatusAsignaciones !=1) {
                // listaNoCargados.add(asignacion1);

                // }
                // estatusAsignaciones2=asignacionMttoDAO.insertaAsignacionMtto(asignacion2);
                // asignacionMttoDAO.cargaAsignacion();
            }
        }

        return estatusAsignaciones;
    }

    @RequestMapping(value = "secure/post/carga/asignacionexcelSupMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)

    public List<AsignacionMttoDTO> postCargaExcelAsignaSupMtto(@RequestBody AsignacionMttoDTO asignacion)
            throws CustomException {

        List<AsignacionMttoDTO> listaAsignaciones = null;

        if (asignacion.getArchivoAsignaciones() == null) {
            throw new CustomException(
                    ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta carga Asignacion-Supervison"));
        } else {
            String archivo = asignacion.getArchivoAsignaciones();
            try {
                byte[] archivoDecodificada = Base64.getDecoder().decode(archivo);

                Date fechaActual = new Date();
                DateFormat formatoAnio = new SimpleDateFormat("yyyy");
                DateFormat formatoMes = new SimpleDateFormat("MM");
                DateFormat formatoFCompleta = new SimpleDateFormat("yyyyMMdd");
                String anio = formatoAnio.format(fechaActual);
                String mes = formatoMes.format(fechaActual);
                String fActual = formatoFCompleta.format(fechaActual);

                File r2 = null;
                String rootPath = File.listRoots()[0].getAbsolutePath();
                String rutaArchivo = "/franquicia/ArchivosAsignaciones/" + anio + "/" + mes + "/";

                String rutaPATHArchivo = rootPath + rutaArchivo;

                r2 = new File(rutaPATHArchivo);

                if (r2.mkdirs()) {
                    logger.info("SE HA CREADA LA CARPETA");
                } else {
                    logger.info("EL DIRECTORIO YA EXISTE");
                }

                String nomArchivo = "Asignaciones_" + fActual + ".xlsx";

                String rutaTotalF = rutaPATHArchivo + nomArchivo;
                FileOutputStream fileOutputF = null;
                try {
                    fileOutputF = new FileOutputStream(rutaTotalF);

                    BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                    bufferOutputF.write(archivoDecodificada);
                    bufferOutputF.close();

                } catch (Exception ex) {

                    logger.info("Ocurrio algo: " + ex);
                } finally {
                    try {
                        fileOutputF.close();
                    } catch (Exception e) {
                        logger.info("AP" + e);
                    }
                }

                File file = new File(rutaTotalF);
                listaAsignaciones = leerExcellAsignacionesSupMtto(file);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return listaAsignaciones;
    }

    @SuppressWarnings("deprecation")
    public ArrayList<AsignacionMttoDTO> leerExcellAsignacionesSupMtto(File file) {
        ArrayList<AsignacionMttoDTO> lista = new ArrayList<AsignacionMttoDTO>();
        try {

            // leer archivo excel
            XSSFWorkbook worbook = new XSSFWorkbook(file);
            // obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheetAt(0);
            int filas = 0;
            boolean pregError = false;

            ArrayList<String> nomColums = new ArrayList<String>();
            String idVisita = "";
            // System.out.println(sheet.getro);
            for (Row row : sheet) {
                AsignacionMttoDTO obj = new AsignacionMttoDTO();
                filas++;
                int column = 0;
                for (Cell cell : row) {
                    column++;
                    String format = "";
                    DataFormatter formatter = new DataFormatter();
                    // calcula el resultado de la formula
                    format = formatter.formatCellValue(cell);
                    if (column == 1 && format.equals("")) {
                        break;
                    }

                    if (filas != 1) {

                        switch (column) {
                            case 1:
                                obj.setNumeroEconomico(Integer.parseInt(format));
                                break;
                            case 2:

                                obj.setNombreSucursal(format);
                                ;
                                break;
                            case 3:
                                obj.setNumeroEmpleado(Integer.parseInt(format));

                                break;
                            case 4:
                                obj.setNombreCertificador(format);
                                break;
                            case 5:
                                // Territorio
                                break;
                            case 6:
                                // zona
                                obj.setZona(format);
                                break;
                            case 7:
                                // Numero Gerente
                                obj.setNumeroGerente(Integer.parseInt(format));
                                break;
                            case 8:
                                // Nombre Gerente
                                obj.setNombreGerente(format);
                                break;
                            case 9:
                                // Formato visita
                                obj.setTipoVisita(format);
                                break;
                            case 10:
                                // numero de semana
                                obj.setWeek_n(format);
                                lista.add(obj);
                                break;
                        }

                    } else {
                        nomColums.add(format);
                    }
                } // Cierra for cell

            } // Cierra for row

            System.out.println(filas);

        } catch (Exception e) {
            logger.info("Ocurrio algo en LeerExcell: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return lista;
    }

    // REPORTERIA
    @RequestMapping(value = "secure/post/obtieneReporteAsistenciaMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteCheckAsistMttoDTO> postObtieneReporteAsistenciaMtto(
            @RequestBody ReporteCheckAsistMttoDTO asignacion) throws CustomException {

        if (asignacion.getFechaInicio() == null && asignacion.getFechaFin() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Reporte Asistencia-Mtto"));
        }

        return (List<ReporteCheckAsistMttoDTO>) asignacionMttoDAO.obtieneReporteAsistencia(asignacion);
    }

    @RequestMapping(value = "secure/get/reporteCheckInn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteCheckInnMttoDTO> selectReporteCheckInn(@RequestBody ReporteCheckInnMttoDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo Reporte Check Inn"));
        }
        return asignacionMttoDAO.selectReporteCheckInn(reporte);
    }

    @RequestMapping(value = "secure/get/reporteCheckList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteChecklistMttoDTO> postObtieneReporteChecklistMtto(@RequestBody ReporteChecklistMttoDTO reporte)
            throws CustomException {

        if (reporte.getFecha() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo Reporte Check list"));
        }
        return asignacionMttoDAO.selectReporteCheckList(reporte);
    }

    @RequestMapping(value = "secure/get/conteoGeneralProgramadas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoGralDTO> getConteoGeneralProgramadas(@RequestBody ReporteConteoGralDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(
                    ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral Programadas"));
        }
        return asignacionMttoDAO.selectConteoGralProgramadas(reporte);
    }
//n
    @RequestMapping(value = "secure/get/conteoGeneralProgramadasCeco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoGralDTO> getConteoGeneralProgCeco(@RequestBody ReporteConteoGralDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(
                    ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral Programadas Ceco"));
        }
        return asignacionMttoDAO.selectConteoGralProgramadasCeco(reporte);
    }

    @RequestMapping(value = "secure/get/conteoGeneralEchas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoGralDTO> getConteoGeneralEchas(@RequestBody ReporteConteoGralDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral Echas"));
        }
        return asignacionMttoDAO.selectConteoGralEchas(reporte);
    }

    //n
    @RequestMapping(value = "secure/get/conteoGeneralEchasCeco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoGralDTO> getConteoGeneralEchasCeco(@RequestBody ReporteConteoGralDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral Echas Ceco"));
        }
        return asignacionMttoDAO.selectConteoGralEchasCeco(reporte);
    }

    @RequestMapping(value = "secure/get/conteoGeneralSemanal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoGralSemanalDTO> getConteoGeneralSemanal(@RequestBody ReporteConteoGralSemanalDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral Semanal"));
        }
        return asignacionMttoDAO.selectConteoGralSemanal(reporte);
    }
    
    //n
    @RequestMapping(value = "secure/get/conteoGeneralSemanalCeco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoGralSemanalDTO> getConteoGeneralSemanalCeco(@RequestBody ReporteConteoGralSemanalDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral Semanal Ceco"));
        }
        return asignacionMttoDAO.selectConteoGralSemanalCeco(reporte);
    }


    @RequestMapping(value = "secure/get/conteoGeneralTerritorio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoTerritorioDTO> getConteoGeneralTerritorio(@RequestBody ReporteConteoTerritorioDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo Gral territorio"));
        }
        return asignacionMttoDAO.selectConteoGralTerritorio(reporte);
    }

    @RequestMapping(value = "secure/get/geografia", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoTerritorioDTO> getGeografia(@RequestBody ReporteConteoTerritorioDTO reporte)
            throws CustomException {

        return asignacionMttoDAO.selectGeografia(reporte);
    }

    @RequestMapping(value = "secure/get/semanaTerritorio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoTerritorioDTO> getSemanaTerritorio(@RequestBody ReporteConteoTerritorioDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo semana territorio"));
        }
        return asignacionMttoDAO.selectSemanaTerritorio(reporte);
    }

    @RequestMapping(value = "secure/get/semanaUsuario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteConteoTerritorioDTO> getSemanaUsuario(@RequestBody ReporteConteoTerritorioDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo conteo semana usuario"));
        }
        return asignacionMttoDAO.selectSemanaUsuario(reporte);
    }

    @RequestMapping(value = "secure/get/reporteProgramadas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteProgramadasDTO> getReporteProgramadas(@RequestBody ReporteProgramadasDTO reporte)
            throws CustomException {

        if (reporte.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo reporte programadas "));
        }
        return asignacionMttoDAO.selectProgramadas(reporte);
    }

    @RequestMapping(value = "secure/get/lugarMtto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LugarMttoDTO> getLugarMtto(@RequestBody LugarMttoDTO reporte)
            throws CustomException {

        return asignacionMttoDAO.selectLugarMtto(reporte);
    }

    @RequestMapping(value = "secure/get/ReporteRespuestas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ExcelRespuestasDTO> getReporteRespuestas(@RequestBody ExcelRespuestasDTO reporte)
            throws CustomException {
    	  if (reporte.getPeriodo()== null) {
              throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo reporte RESPUESTAS "));
          }
        return asignacionMttoDAO.selectReporteRespuestas(reporte);
    }
    
    @RequestMapping(value = "secure/get/arbolUsuario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArbolUsuarioDTO> getArbolUsuario(@RequestBody ArbolUsuarioDTO reporte)
            throws CustomException {
    	  if (reporte.getIdSupervisor()== null) {
              throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("No trajo Estructura del usuario "));
          }
        return asignacionMttoDAO.selectArbolUsu(reporte);
    }

}
