/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class VisitasPendientesRowMapper implements RowMapper<VisitaPendiente> {

    @Override
    public VisitaPendiente mapRow(ResultSet rs, int rowNum) throws SQLException {
        VisitaPendiente visitaPendiente = new VisitaPendiente();
        visitaPendiente.setIdBitacora(rs.getInt("FIID_BITACORA"));
        visitaPendiente.setIdUsuario(rs.getInt("FIID_USUARIO"));
        visitaPendiente.setIdCeco(rs.getString("FCID_CECO"));
        visitaPendiente.setFecha(rs.getString("FDFECHA"));
        visitaPendiente.setOrdenGrupo(rs.getInt("FIORDEN_GRUPO"));
        visitaPendiente.setEstatus(rs.getInt("FIESTATUS"));
        return visitaPendiente;
    }
}
