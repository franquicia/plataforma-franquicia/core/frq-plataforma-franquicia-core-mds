/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosDocumento {

    @JsonProperty(value = "idDocumento")
    private transient Integer idDocumento;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion nombre del documento", example = "EKT", required = true)
    private String descripcion;

    @JsonProperty(value = "estadoHistorico", required = true)
    @ApiModelProperty(notes = "Estado de historico del documento", example = "1", required = true)
    private Integer estadoHistorico;

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstadoHistorico() {
        return estadoHistorico;
    }

    public void setEstadoHistorico(Integer estadoHistorico) {
        this.estadoHistorico = estadoHistorico;
    }
}
