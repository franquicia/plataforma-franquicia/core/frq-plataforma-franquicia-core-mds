/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosFolio {

    @JsonProperty(value = "idFolio")
    private transient Integer idFolio;

    @JsonProperty(value = "numeroEmpleadoCreador")
    @ApiModelProperty(notes = "numero de empleado del creador del documento", example = "331952", required = true)
    private Integer numeroEmpleadoCreador;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getNumeroEmpleadoCreador() {
        return numeroEmpleadoCreador;
    }

    public void setNumeroEmpleadoCreador(Integer numeroEmpleadoCreador) {
        this.numeroEmpleadoCreador = numeroEmpleadoCreador;
    }

}
