/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PeriodoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "dia_semana")
    private Integer diaSemana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "dia_mes")
    private Integer diaMes;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semana")
    private Integer semana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mes")
    private Integer mes;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "bimestre")
    private Integer bimestre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "trimestre")
    private Integer trimestre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "anio")
    private Integer anio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "festivo")
    private Boolean festivo;

    public PeriodoDTO() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(BigDecimal diaSemana) {
        this.diaSemana = (diaSemana == null ? null : diaSemana.intValue());
    }

    public Integer getDiaMes() {
        return diaMes;
    }

    public void setDiaMes(BigDecimal diaMes) {
        this.diaMes = (diaMes == null ? null : diaMes.intValue());
    }

    public Integer getSemana() {
        return semana;
    }

    public void setSemana(BigDecimal semana) {
        this.semana = (semana == null ? null : semana.intValue());
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(BigDecimal mes) {
        this.mes = (mes == null ? null : mes.intValue());
    }

    public Integer getBimestre() {
        return bimestre;
    }

    public void setBimestre(BigDecimal bimestre) {
        this.bimestre = (bimestre == null ? null : bimestre.intValue());
    }

    public Integer getTrimestre() {
        return trimestre;
    }

    public void setTrimestre(BigDecimal trimestre) {
        this.trimestre = (trimestre == null ? null : trimestre.intValue());
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(BigDecimal anio) {
        this.anio = (anio == null ? null : anio.intValue());
    }

    public Boolean getFestivo() {
        return festivo;
    }

    public void setFestivo(BigDecimal festivo) {
        int isFestivo = (festivo == null ? null : festivo.intValue());
        this.festivo = isFestivo == 1;
    }

    @Override
    public String toString() {
        return "PeriodoDTO{" + "fecha=" + fecha + ", diaSemana=" + diaSemana + ", diaMes=" + diaMes + ", semana=" + semana + ", mes=" + mes + ", bimestre=" + bimestre + ", trimestre=" + trimestre + ", anio=" + anio + '}';
    }

}
