/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del modelo negocio", value = "ModeloNegocio")
public class ModeloNegocio {

    @JsonProperty(value = "idArbol")
    @ApiModelProperty(notes = "identificador del nivel", example = "1")
    private Integer idArbol;

    @JsonProperty(value = "descripcion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Descripcion del nivel", example = "Estrategia")
    private String descripcion;

    @JsonProperty(value = "color")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Descripcion del nivel", example = "Estrategia")
    private String color;

    public ModeloNegocio() {
    }

    public ModeloNegocio(Integer idArbol, String descripcion, String color) {
        this.idArbol = idArbol;
        this.descripcion = descripcion;
        this.color = color;
    }

    public Integer getIdArbol() {
        return idArbol;
    }

    public void setIdArbol(Integer idArbol) {
        this.idArbol = idArbol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "ModeloNegocio{" + "idArbol=" + idArbol + ", descripcion=" + descripcion + ", color=" + color + '}';
    }

}
