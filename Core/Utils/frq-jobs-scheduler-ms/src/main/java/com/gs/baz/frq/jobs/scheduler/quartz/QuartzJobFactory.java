package com.gs.baz.frq.jobs.scheduler.quartz;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.jobs.scheduler.dto.ScheduleJob;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.gs.baz.frq.jobs.scheduler.util.DateDifference;
import com.gs.baz.frq.jobs.scheduler.dto.Execution;
import com.gs.baz.frq.jobs.scheduler.quartz.service.ClientRestTemplateExternalService;
import com.gs.baz.frq.jobs.scheduler.quartz.service.ClientRestTemplateService;
import com.gs.baz.frq.jobs.scheduler.quartz.service.GenericService;
import com.gs.baz.frq.jobs.scheduler.quartz.service.ScheduleJobService;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.SchedulerException;

public class QuartzJobFactory implements Job {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    private GenericService genericService;

    @Autowired
    private ClientRestTemplateService clientRestTemplateService;

    @Autowired
    private ClientRestTemplateExternalService clientRestTemplateExternalService;

    @Autowired
    private ScheduleJobService scheduleJobService;

    public static List<ScheduleJob> jobList = Lists.newArrayList();

    public static List<ScheduleJob> getInitAllJobs() {
        return jobList;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        ScheduleJob scheduleJob = (ScheduleJob) jobExecutionContext.getMergedJobDataMap().get("scheduleJob");
        Date beginDate = new Date();
        if (scheduleJob.getExecutionURL() != null) {
            if (scheduleJob.getExecutionURL().getUrlExecute() != null && scheduleJob.getExecutionURL().getUrlExecute()) {
                if (scheduleJob.getExecutionURL().getUrlExternal() != null && scheduleJob.getExecutionURL().getUrlExternal()) {
                    clientRestTemplateExternalService.run(scheduleJob);
                } else {
                    clientRestTemplateService.run(scheduleJob);
                }
            } else {
                genericService.run(scheduleJob);
            }
        } else {
            genericService.run(scheduleJob);
        }
        Date endDate = new Date();
        ObjectNode duration = new DateDifference(beginDate, endDate).getDiff();
        try {
            Execution execution = scheduleJob.getExecution();
            if (execution == null) {
                execution = new Execution();
            }
            execution.setLastDuration(duration.get("minutes").asText() + " Min con " + duration.get("seconds").asText() + " sec");
            scheduleJob.setExecution(execution);
            scheduleJobService.updateJobDataMap(scheduleJob);
        } catch (SchedulerException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

}
