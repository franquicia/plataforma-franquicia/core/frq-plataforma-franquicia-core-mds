/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class FranquiciaDTO {

    @JsonProperty(value = "id_franquicia")
    private Integer idFranquicia;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public FranquiciaDTO(BigDecimal idFranquicia) {
        this.idFranquicia = (idFranquicia == null ? null : idFranquicia.intValue());
    }

    public FranquiciaDTO() {
    }

    public Integer getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(BigDecimal idFranquicia) {
        this.idFranquicia = (idFranquicia == null ? null : idFranquicia.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "FranquiciaDTO{" + "idFranquicia=" + idFranquicia + ", descripcion=" + descripcion + "}";
    }

}
