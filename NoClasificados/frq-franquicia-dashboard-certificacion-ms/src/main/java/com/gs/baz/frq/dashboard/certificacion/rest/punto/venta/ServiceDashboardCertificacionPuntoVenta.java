/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.punto.venta;

import com.gs.baz.frq.dashboard.certificacion.dao.punto.venta.DashboardCertificacionPuntoVentaDAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.punto.venta.DashboardCertificacionPuntoVentaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/punto/venta")
public class ServiceDashboardCertificacionPuntoVenta {

    @Autowired
    private DashboardCertificacionPuntoVentaDAOImpl dashboardCertificacionPuntoVentaDAOImpl;

    @RequestMapping(value = "secure/grafico/get/ultima/certificacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatosUltimaCertificacion() throws CustomException {
        return dashboardCertificacionPuntoVentaDAOImpl.selectRowsDashboardCertificacionUltCerti();
    }

    @RequestMapping(value = "secure/grafico/get/ultima/certificacion/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatoUltimaCertificacion(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "idCecoBase", required = false) Long idCecoBase) throws CustomException {

        return dashboardCertificacionPuntoVentaDAOImpl.selectRowDashboardCertificacionUltCerti(idCeco, idCecoBase);
    }

    @RequestMapping(value = "secure/grafico/get/detalle/certificacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatosDetalleCertificacion() throws CustomException {
        return dashboardCertificacionPuntoVentaDAOImpl.selectRowsDashboardCertificacionDetalleCert();
    }

    @RequestMapping(value = "secure/grafico/get/detalle/certificacion/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatoDetalleCertificacion(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "idCecoBase", required = false) Long idCecoBase,
            @RequestParam(value = "fecha", required = false) String fecha,
            @RequestParam(value = "protocolo", required = false) Long protocolo) throws CustomException {

        return dashboardCertificacionPuntoVentaDAOImpl.selectRowDashboardCertificacionDetalleCert(idCeco, idCecoBase, fecha, protocolo);
    }

    @RequestMapping(value = "secure/grafico/get/respuesta/certificacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatosRespuestaCertificacion() throws CustomException {
        return dashboardCertificacionPuntoVentaDAOImpl.selectRowsDashboardCertificacionRespCert();
    }

    @RequestMapping(value = "secure/grafico/get/respuesta/certificacion/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatoRespuestaCertificacion(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "idCecoBase", required = false) Long idCecoBase,
            @RequestParam(value = "fecha", required = false) String fecha,
            @RequestParam(value = "protocolo", required = false) Long protocolo) throws CustomException {

        return dashboardCertificacionPuntoVentaDAOImpl.selectRowDashboardCertificacionRespCert(idCeco, idCecoBase, fecha, protocolo);
    }

    @RequestMapping(value = "secure/grafico/get/calificacion/ceco", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatosCalifCecoCertificacion() throws CustomException {
        return dashboardCertificacionPuntoVentaDAOImpl.selectRowsDashboardCertificacionCalifCeco();
    }

    @RequestMapping(value = "secure/grafico/get/calificacion/ceco/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatoCalifCecoCertificacion(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "idCecoBase", required = false) Long idCecoBase,
            @RequestParam(value = "fecha", required = false) String fecha,
            @RequestParam(value = "protocolo", required = false) Long protocolo) throws CustomException {

        return dashboardCertificacionPuntoVentaDAOImpl.selectRowDashboardCertificacionCalifCeco(idCeco, idCecoBase, fecha, protocolo);
    }

    @RequestMapping(value = "secure/grafico/get/evidencia", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatosEvidenciaCertificacion() throws CustomException {
        return dashboardCertificacionPuntoVentaDAOImpl.selectRowsDashboardCertificacionEvidencia();
    }

    @RequestMapping(value = "secure/grafico/get/evidencia/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatoEvidenciaCertificacion(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "idCecoBase", required = false) Long idCecoBase,
            @RequestParam(value = "fecha", required = false) String fecha,
            @RequestParam(value = "protocolo", required = false) Long protocolo) throws CustomException {

        return dashboardCertificacionPuntoVentaDAOImpl.selectRowDashboardCertificacionEvidencia(idCeco, idCecoBase, fecha, protocolo);
    }

    @RequestMapping(value = "secure/grafico/get/informe/certificacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatosInformeCertificacion() throws CustomException {
        return dashboardCertificacionPuntoVentaDAOImpl.selectRowsDashboardCertificacionInformeCert();
    }

    @RequestMapping(value = "secure/grafico/get/informe/certificacion/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionPuntoVentaDTO> getDatoInformeCertificacion(
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "fecha", required = false) String fecha,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario) throws CustomException {

        return dashboardCertificacionPuntoVentaDAOImpl.selectRowDashboardCertificacionInformeCert(idCeco, fecha, idUsuario);
    }
}
