/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de rangos", value = "Rangos")
public class Rangos {

    @JsonProperty(value = "rangos")
    @ApiModelProperty(notes = "rangos")
    private List<Rango> rangos;

    public Rangos(List<Rango> rangos) {
        this.rangos = rangos;
    }

    public List<Rango> getRangos() {
        return rangos;
    }

    public void setRangos(List<Rango> rangos) {
        this.rangos = rangos;
    }

    @Override
    public String toString() {
        return "Rangos{" + "rangos=" + rangos + '}';
    }

}
