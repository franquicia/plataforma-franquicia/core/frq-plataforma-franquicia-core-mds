/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Bandeja de todos los incidentes", value = "RespuestaBandejaIncidentes")
public class BandejaIncidentesResponseDTO {

    @JsonProperty("recibidoPorAtender")
    @ApiModelProperty(notes = "Incidentes recibidos en estatus Por Atender")
    private List<IncidenteLimpiezaDTO> recibidoPorAtender;

    @JsonProperty("enProceso")
    @ApiModelProperty(notes = "Incidentes recibidos en estatus En Proceso")
    private List<IncidenteLimpiezaDTO> enProceso;

    @JsonProperty("enProcesoDeCierre")
    @ApiModelProperty(notes = "Incidentes recibidos en estatus En Proceso De Cierre")
    private List<IncidenteLimpiezaDTO> enProcesoDeCierre;

    @JsonProperty("atendidos")
    @ApiModelProperty(notes = "Incidentes recibidos en estatus Atendidos")
    private List<IncidenteLimpiezaDTO> atendidos;

    @JsonProperty("pendientesPorAutorizar")
    @ApiModelProperty(notes = "Incidentes recibidos en estatus Pendientes Por Autorizar")
    private List<IncidenteLimpiezaDTO> pendientesPorAutorizar;

    @JsonProperty("supervisores")
    @ApiModelProperty(notes = "Listado de supervisores", example = "[\"Jose Hernandez\",\"Manuel Gonzalez\"]")
    private List<String> supervisores;

    public BandejaIncidentesResponseDTO() {
        super();

    }

    public List<IncidenteLimpiezaDTO> getRecibidoPorAtender() {
        return recibidoPorAtender;
    }

    public void setRecibidoPorAtender(List<IncidenteLimpiezaDTO> recibidoPorAtender) {
        this.recibidoPorAtender = recibidoPorAtender;
    }

    public List<IncidenteLimpiezaDTO> getEnProceso() {
        return enProceso;
    }

    public void setEnProceso(List<IncidenteLimpiezaDTO> enProceso) {
        this.enProceso = enProceso;
    }

    public List<IncidenteLimpiezaDTO> getEnProcesoDeCierre() {
        return enProcesoDeCierre;
    }

    public void setEnProcesoDeCierre(List<IncidenteLimpiezaDTO> enProcesoDeCierre) {
        this.enProcesoDeCierre = enProcesoDeCierre;
    }

    public List<IncidenteLimpiezaDTO> getAtendidos() {
        return atendidos;
    }

    public void setAtendidos(List<IncidenteLimpiezaDTO> atendidos) {
        this.atendidos = atendidos;
    }

    public List<IncidenteLimpiezaDTO> getPendientesPorAutorizar() {
        return pendientesPorAutorizar;
    }

    public void setPendientesPorAutorizar(List<IncidenteLimpiezaDTO> pendientesPorAutorizar) {
        this.pendientesPorAutorizar = pendientesPorAutorizar;
    }

    public List<String> getSupervisores() {
        return supervisores;
    }

    public void setSupervisores(List<String> supervisores) {
        this.supervisores = supervisores;
    }

}
