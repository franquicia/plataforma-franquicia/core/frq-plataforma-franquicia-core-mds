/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.tipos.informacion.mprs;

import com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto.TipoInformacionBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class TipoInformacionRowMapper implements RowMapper<TipoInformacionBase> {

    private TipoInformacionBase tipoInformacionBase;

    @Override
    public TipoInformacionBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        tipoInformacionBase = new TipoInformacionBase();
        tipoInformacionBase.setDescripcion(rs.getString("FCDESCRIPCION"));

        return tipoInformacionBase;
    }
}
