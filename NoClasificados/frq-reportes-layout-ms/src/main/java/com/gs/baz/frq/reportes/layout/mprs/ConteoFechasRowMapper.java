/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reportes.layout.mprs;

import com.gs.baz.frq.reportes.layout.dto.ConteoFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ConteoFechasRowMapper implements RowMapper<ConteoFechas> {

    private ConteoFechas conteoFechas;

    @Override
    public ConteoFechas mapRow(ResultSet rs, int rowNum) throws SQLException {
        conteoFechas = new ConteoFechas();
        conteoFechas.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        conteoFechas.setNombreProyecto(rs.getString("NOMPROY"));
        conteoFechas.setValorConteo(rs.getInt("CONTEO"));

        return conteoFechas;
    }
}
