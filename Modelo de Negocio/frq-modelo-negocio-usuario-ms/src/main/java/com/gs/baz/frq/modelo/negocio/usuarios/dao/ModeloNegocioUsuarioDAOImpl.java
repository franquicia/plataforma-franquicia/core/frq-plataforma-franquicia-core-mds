/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.usuarios.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.modelo.negocio.usuarios.dto.Usuario;
import com.gs.baz.frq.modelo.negocio.usuarios.dto.UsuarioCorreo;
import com.gs.baz.frq.modelo.negocio.usuarios.mprs.ModeloNegocioCorreoUsuariosRowMapper;
import com.gs.baz.frq.modelo.negocio.usuarios.mprs.ModeloNegocioUsuariosRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class ModeloNegocioUsuarioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectAllMail;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private final String schema = "MODFRANQ";

    public void init() {
        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAUSUARIO");
        jdbcSelect.withProcedureName("SPGETAUSUARIO");
        jdbcSelect.returningResultSet("PA_CDATOS", new ModeloNegocioUsuariosRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectAll.withProcedureName("SPGETUSUARIOS");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new ModeloNegocioUsuariosRowMapper());

        jdbcSelectAllMail = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllMail.withSchemaName(schema);
        jdbcSelectAllMail.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectAllMail.withProcedureName("SPGETUSUARIOSCOMP");
        jdbcSelectAllMail.returningResultSet("PA_CDATOS", new ModeloNegocioCorreoUsuariosRowMapper());

    }

    public Usuario selectRow(Integer usuario) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuario);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<Usuario> data = (List<Usuario>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Usuario> selectAllRows(String usuario) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_USUARIO", usuario);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public List<UsuarioCorreo> selectAllMailRows(String usuario) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_USUARIO", usuario);
        Map<String, Object> out = jdbcSelectAllMail.execute(mapSqlParameterSource);
        return (List<UsuarioCorreo>) out.get("PA_CDATOS");
    }

}
