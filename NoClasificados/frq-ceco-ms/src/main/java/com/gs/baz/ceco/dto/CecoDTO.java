/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ceco.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class CecoDTO {

    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_superior")
    private Integer idCecoSuperior;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calle")
    private String calle;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ciudad")
    private String ciudad;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "codigo_postal")
    private String codigoPostal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_contacto")
    private String nombreContacto;

    @JsonProperty(value = "idCanal")
    private Integer idCanal;

    public Integer getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(Integer idCanal) {
        this.idCanal = idCanal;
    }

    public CecoDTO() {
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdCecoSuperior() {
        return idCecoSuperior;
    }

    public void setIdCecoSuperior(BigDecimal IdCecoSuperior) {
        this.idCecoSuperior = (IdCecoSuperior == null ? null : IdCecoSuperior.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    @Override
    public String toString() {
        return "CecoDTO{" + "idCeco=" + idCeco + ", idCecoSuperior=" + idCecoSuperior + ", descripcion=" + descripcion + ", calle=" + calle + ", ciudad=" + ciudad + ", codigoPostal=" + codigoPostal + ", nombreContacto=" + nombreContacto + '}';
    }

}
