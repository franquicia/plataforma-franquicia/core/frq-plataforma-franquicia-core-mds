/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.bi;

/**
 *
 * @author cescobarh
 */
public class ApigeeJWTException extends Exception {

    public ApigeeJWTException() {
        super();
    }

    public ApigeeJWTException(String message) {
        super(message);
    }

    public ApigeeJWTException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApigeeJWTException(Throwable cause) {
        super(cause);
    }

}
