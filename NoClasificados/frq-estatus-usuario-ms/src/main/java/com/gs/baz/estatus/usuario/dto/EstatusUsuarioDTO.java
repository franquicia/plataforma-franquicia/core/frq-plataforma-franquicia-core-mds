/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.estatus.usuario.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class EstatusUsuarioDTO {

    @JsonProperty(value = "id_estatus_usuario")
    private Integer idEstatusUsuario;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonProperty(value = "id_estatus")
    private Integer idEstatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public EstatusUsuarioDTO() {
    }

    public Integer getIdEstatusUsuario() {
        return idEstatusUsuario;
    }

    public void setIdEstatusUsuario(BigDecimal idEstatusUsuario) {
        this.idEstatusUsuario = (idEstatusUsuario == null ? null : idEstatusUsuario.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(BigDecimal idEstatus) {
        this.idEstatus = (idEstatus == null ? null : idEstatus.intValue());
    }

    @Override
    public String toString() {
        return "EstatusUsuarioDTO{" + "idEstatusUsuario=" + idEstatusUsuario + ", descripcion=" + descripcion + ", idEstatus=" + idEstatus + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
