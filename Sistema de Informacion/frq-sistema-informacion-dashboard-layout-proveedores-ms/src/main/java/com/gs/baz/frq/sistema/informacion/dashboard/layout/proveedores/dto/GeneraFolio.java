/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta de genración de folio", value = "BanderaFolio")
public class GeneraFolio {

    @JsonProperty(value = "banderaFolio")
    @ApiModelProperty(notes = "Bandera de la generacion del folio.", example = "1", position = -1)
    private Integer banderaFolio;
    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Indicador del folio generado.", example = "1", position = -2)
    private Integer idFolio;

    public Integer getBanderaFolio() {
        return banderaFolio;
    }

    public void setBanderaFolio(Integer banderaFolio) {
        this.banderaFolio = banderaFolio;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    @Override
    public String toString() {
        return "GeneraFolio{" + "banderaFolio=" + banderaFolio + ", idFolio=" + idFolio + '}';
    }

}
