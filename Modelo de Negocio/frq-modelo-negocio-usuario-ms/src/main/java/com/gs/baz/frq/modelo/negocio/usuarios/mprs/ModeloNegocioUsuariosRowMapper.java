/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.usuarios.mprs;

import com.gs.baz.frq.modelo.negocio.usuarios.dto.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ModeloNegocioUsuariosRowMapper implements RowMapper<Usuario> {

    private Usuario usuario;

    @Override
    public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
        usuario = new Usuario();
        usuario.setNumeroEmpleado(rs.getInt("FIID_EMPLEADO"));
        usuario.setNombre(rs.getString("FCNOMBRE"));
        return usuario;
    }
}
