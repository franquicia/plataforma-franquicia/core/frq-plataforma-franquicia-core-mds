package com.gs.baz.frq.calificacion.disciplina.mprs;

import com.gs.baz.frq.calificacion.disciplina.dto.CalificacionDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionDisciplinaRowMapper implements RowMapper<CalificacionDisciplinaDTO> {

    private CalificacionDisciplinaDTO calificacionDisciplina;

    @Override
    public CalificacionDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        calificacionDisciplina = new CalificacionDisciplinaDTO();
        calificacionDisciplina.setIdDisciplina((BigDecimal) (rs.getObject("FIDISCIPLINA_ID")));
        calificacionDisciplina.setIdCeco((rs.getString("FCID_CECO")));
        calificacionDisciplina.setCalificacionDisciplina((BigDecimal) rs.getObject("FICALIF_DISC"));
        calificacionDisciplina.setFecha(rs.getString("FDFECHA"));
        calificacionDisciplina.setPuntosObtenidos(rs.getInt("FIPUNTOSOBT"));
        calificacionDisciplina.setPuntosEsperados(rs.getInt("FIPUNTOSESP"));
        calificacionDisciplina.setNumeroDefectos(rs.getInt("FINUMDEFECTOS"));
        calificacionDisciplina.setNumeroOportunidades(rs.getInt("FINUMOPORTUNID"));
        calificacionDisciplina.setIdRango(rs.getInt("FIRANGO_ID"));
        return calificacionDisciplina;
    }
}
