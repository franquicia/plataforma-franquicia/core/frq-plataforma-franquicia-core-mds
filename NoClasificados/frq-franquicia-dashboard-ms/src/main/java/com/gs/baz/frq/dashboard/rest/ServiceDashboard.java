/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.dashboard.dao.DashboardDAOImpl;
import com.gs.baz.frq.dashboard.dto.DashboardDTO;
import com.gs.baz.frq.dashboard.dto.Entrada;
import com.gs.baz.frq.dashboard.dto.graficos.DisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard {

    @Autowired
    private DashboardDAOImpl dashboardDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardDTO> getDasboard(@RequestBody DashboardDTO dashboard) throws CustomException {
        return dashboardDAOImpl.selectRows(dashboard);
    }

    @RequestMapping(value = "secure/grafico/post/disciplina", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DisciplinaDTO> getDisciplinas(@RequestBody DisciplinaDTO disciplina) throws CustomException {
        return dashboardDAOImpl.selectRowsDisciplina(disciplina);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean putDashboard(@RequestBody DashboardDTO dashboard) throws CustomException {
        if (dashboard.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (dashboard.getIdConfiguracionDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_configuracion_disciplina"));
        }
        if (dashboard.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        return dashboardDAOImpl.updateRowDashboard(dashboard);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean deleteDashboard(@RequestBody DashboardDTO dashboard) throws CustomException {
        if (dashboard.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (dashboard.getIdConfiguracionDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_configuracion_disciplina"));
        }
        if (dashboard.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        return dashboardDAOImpl.deleteRowDashboard(dashboard);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DashboardDTO postDashboard(@RequestBody DashboardDTO dashboard) throws CustomException {
        if (dashboard.getIdConfiguracionDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_configuracion_disciplina"));
        }
        if (dashboard.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (dashboard.getIdProotocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (dashboard.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (dashboard.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (dashboard.getIdTerritorio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_territorio"));
        }
        if (dashboard.getTerritorio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("territorio"));
        }
        if (dashboard.getIdZona() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_zona"));
        }
        if (dashboard.getZona() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("zona"));
        }
        if (dashboard.getIdRegion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_region"));
        }
        if (dashboard.getRegion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("region"));
        }
        if (dashboard.getIdNegocioDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio_disciplina"));
        }
        if (dashboard.getCalificacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("calificacion"));
        }
        if (dashboard.getNumeroImperdonable() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_imperdonable"));
        }
        if (dashboard.getNumeroIncumplimiento() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_imcumplimiento"));
        }
        if (dashboard.getFechaTermino() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fecha_termino"));
        }
        if (dashboard.getPuntosObtenidos() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("puntos_obtenidos"));
        }
        if (dashboard.getPuntosEsperados() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("puntos_esperados"));
        }
        if (dashboard.getIdEfectos() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_efectos"));
        }
        if (dashboard.getOportunidades() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("oportunidades"));
        }
        if (dashboard.getIdCecoNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco_negocio"));
        }

        if (dashboard.getPrecalificacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("precalificacion"));
        }

        if (dashboard.getCalificacionLineal() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("calificacion_lineal"));
        }

        return dashboardDAOImpl.insertRow(dashboard);
    }

    @RequestMapping(value = "secure/execute/url", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String postDashboard(@RequestBody String urlStr) throws CustomException {
        String respuesta = null;
        HttpURLConnection conexion = null;
        OutputStream os = null;

        BufferedReader in = null;
        ObjectMapper mapper = new ObjectMapper();

        try {
            Entrada jsonObj = mapper.readValue(urlStr, Entrada.class);
            System.out.println("Objeto entrante: " + jsonObj.toString());

            URL url = new URL(jsonObj.getUrl());
            //     URL url = new URL("http://localhost:8080/frq-franquicia-dashboard-ms/service/franquicia/dashboard/secure/get");

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setDoOutput(true);
            conexion.setRequestMethod("POST");
            conexion.setRequestProperty("Content-Type", "application/json");
            os = conexion.getOutputStream();
            os.write(jsonObj.getCadenaJson().getBytes("UTF-8"));
            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {
                System.out.println("responseCode " + responseCode);
            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                String inputLine;

                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                respuesta = response.toString();
                System.out.println("RESPUESTA SERVICIO::" + respuesta);
            }

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                    conexion.disconnect();
                }
                if (conexion != null) {
                    conexion.disconnect();
                }
                if (os != null) {
                    os.close();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        return respuesta;
    }

    @RequestMapping(value = "secure/grafico/get/disciplina/dashboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DisciplinaDTO> getDisciplinasDashboard() throws CustomException {
        return dashboardDAOImpl.selectRowsDisciplinaDas();
    }
}
