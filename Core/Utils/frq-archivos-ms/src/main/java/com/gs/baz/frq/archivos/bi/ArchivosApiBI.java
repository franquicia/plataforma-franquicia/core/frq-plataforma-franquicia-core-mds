package com.gs.baz.frq.archivos.bi;

import com.gs.baz.frq.archivos.dto.ElementoDTO;
import com.gs.baz.frq.archivos.dto.ParamentrosArchivoDTO;
import com.gs.baz.frq.archivos.dto.TiposElemento;
import com.gs.baz.frq.archivos.exceptions.DeleteFileNotFoundException;
import com.gs.baz.frq.archivos.exceptions.DirectoryProtectedException;
import com.gs.baz.frq.archivos.exceptions.FileNotFoundException;
import com.gs.baz.frq.archivos.exceptions.MakeDirectoryException;
import com.gs.baz.frq.archivos.exceptions.OverwriteFileException;
import com.gs.baz.frq.archivos.exceptions.ReadDirectoryException;
import com.gs.baz.frq.archivos.exceptions.ReadFileException;
import com.gs.baz.frq.archivos.exceptions.UpdateFileException;
import com.gs.baz.frq.archivos.exceptions.WriteFileException;
import com.gs.baz.frq.model.commons.Environment;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ArchivosApiBI {

    private final Logger logger = LogManager.getLogger();
    private String rootPath;
    private Path trashDirectory;
    private Path controlVersionDirectory;

    public ArchivosApiBI() {
    }

    public void init() {
        if (Environment.CURRENT.valueReal().equals(Environment.LOCAL)) {
            try {
                rootPath = System.getProperty("user.home") + "/franquicia/";
                Path path = Paths.get(rootPath);
                if (!Files.exists(path)) {
                    try {
                        Files.createDirectories(path);
                    } catch (IOException ex) {
                        throw new MakeDirectoryException(ex);
                    }
                }
            } catch (MakeDirectoryException ex) {
                logger.error(ex);
                rootPath = "/franquicia/";
            }
        } else {
            rootPath = "/franquicia/";
        }
        trashDirectory = Paths.get(rootPath + "/trash");
        controlVersionDirectory = Paths.get(rootPath + "/control-version");
    }

    public Path readFile(String fullPathFile) throws FileNotFoundException, ReadFileException {
        Path path = Paths.get(rootPath + fullPathFile);
        try {
            verifyProtectedDirectory(path.normalize().toString());
        } catch (DirectoryProtectedException ex) {
            throw new ReadFileException(ex);
        }
        if (Files.exists(path)) {
            return path;
        } else {
            throw new FileNotFoundException("File not found...");
        }
    }

    public void writeFile(ParamentrosArchivoDTO paramentrosArchivoDTO) throws MakeDirectoryException, WriteFileException, OverwriteFileException {
        Path path = Paths.get(rootPath + paramentrosArchivoDTO.getDestino() + "/" + paramentrosArchivoDTO.getFile().getOriginalFilename());
        try {
            verifyProtectedDirectory(paramentrosArchivoDTO.getDestino());
        } catch (DirectoryProtectedException ex) {
            throw new WriteFileException(ex);
        }
        if (Files.exists(path)) {
            throw new OverwriteFileException("File exits overwrite not allowed...");
        }
        if (!Files.exists(path.getParent())) {
            try {
                Files.createDirectories(path.getParent());
            } catch (IOException ex) {
                throw new MakeDirectoryException(ex);
            }
        }
        try {
            Files.createFile(path);
            byte[] fileBytes = paramentrosArchivoDTO.getFile().getBytes();
            Files.write(path, fileBytes, StandardOpenOption.WRITE);
        } catch (IOException ex) {
            throw new WriteFileException(ex);
        }
    }

    public void updateFile(ParamentrosArchivoDTO paramentrosArchivoDTO) throws FileNotFoundException, UpdateFileException {
        Path path = Paths.get(rootPath + paramentrosArchivoDTO.getDestino() + "/" + paramentrosArchivoDTO.getFile().getOriginalFilename());
        try {
            verifyProtectedDirectory(path.normalize().toString());
        } catch (DirectoryProtectedException ex) {
            throw new UpdateFileException(ex);
        }
        if (!Files.exists(path)) {
            throw new FileNotFoundException("File not found...");
        }
        try {
            UUID uuid = UUID.randomUUID();
            Path versionFile = Paths.get(controlVersionDirectory + "/" + uuid + "/" + paramentrosArchivoDTO.getDestino() + "/" + path.getFileName());
            Files.createDirectories(versionFile.getParent());
            Files.copy(path, versionFile);
            byte[] fileBytes = paramentrosArchivoDTO.getFile().getBytes();
            Files.write(path, fileBytes, StandardOpenOption.WRITE);
            File fileReference = new File(path.getParent() + "/.version-" + uuid + "-" + path.getFileName());
            fileReference.createNewFile();
        } catch (IOException ex) {
            throw new UpdateFileException(ex);
        }
    }

    public void verifyProtectedDirectory(String pathCheck) throws DirectoryProtectedException {
        if (!pathCheck.startsWith("/")) {
            pathCheck = "/" + pathCheck;
        }
        String firstDirectory = (pathCheck.split("/").length > 0 ? pathCheck.split("/")[0] : "");
        boolean protectedDirectories = firstDirectory.equals("control-version");
        protectedDirectories |= firstDirectory.equals("trash");
        if (protectedDirectories) {
            throw new DirectoryProtectedException("Root directory is protected");
        }
    }

    public void deleteFile(String fullPathFile) throws FileNotFoundException, DeleteFileNotFoundException {
        Path path = Paths.get(rootPath + fullPathFile);
        try {
            verifyProtectedDirectory(fullPathFile);
        } catch (DirectoryProtectedException ex) {
            throw new DeleteFileNotFoundException(ex);
        }
        if (!Files.exists(path)) {
            throw new FileNotFoundException("File not found...");
        }
        try {
            UUID uuid = UUID.randomUUID();
            Path deletedFile = Paths.get(trashDirectory + "/" + uuid + "/" + fullPathFile);
            Files.createDirectories(deletedFile.getParent());
            Files.move(path, deletedFile);
            Path fileReference = Paths.get(path.getParent() + "/.trash-" + uuid);
            Files.createFile(fileReference);
        } catch (IOException ex) {
            throw new DeleteFileNotFoundException(ex);
        }
    }

    public void makeDirectory(String pathDir) throws MakeDirectoryException {
        String absolutePath = rootPath + pathDir;
        Path path = Paths.get(absolutePath);
        try {
            verifyProtectedDirectory(pathDir);
        } catch (DirectoryProtectedException ex) {
            throw new MakeDirectoryException(ex);
        }
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                throw new MakeDirectoryException(ex);
            }
        }
    }

    public List<ElementoDTO> readDirectory(String pathDir) throws ReadDirectoryException {
        try {
            verifyProtectedDirectory(pathDir);
        } catch (DirectoryProtectedException ex) {
            throw new ReadDirectoryException(ex);
        }
        List<ElementoDTO> files = new ArrayList<>();
        String absolutePath = rootPath + pathDir;
        Path path = Paths.get(absolutePath);
        if (!Files.exists(path)) {
            throw new ReadDirectoryException("Directory '" + pathDir + "' not exist");
        }
        try ( Stream<Path> paths = Files.walk(Paths.get(absolutePath), Integer.MAX_VALUE)) {
            paths.filter(item -> {
                boolean excludeItems = !item.normalize().toString().contains(".trash");
                excludeItems &= !item.normalize().toString().contains(".version");
                return excludeItems;
            }).forEach(item -> {
                boolean isFile = item.toFile().isFile();
                ElementoDTO archivoDTO = new ElementoDTO();
                String rootPathNormalized = rootPath.substring(0, rootPath.length() - 1);
                archivoDTO.setRutaCompleta(item.normalize().toString().replace(rootPathNormalized, ""));
                archivoDTO.setNombre(item.getFileName().toString());
                archivoDTO.setTipo(isFile ? TiposElemento.archivo : TiposElemento.directorio);
                archivoDTO.setDirectorioPadre(item.getParent().normalize().toString().replace(rootPathNormalized, ""));
                files.add(archivoDTO);
            });
        } catch (IOException ex) {
            throw new ReadDirectoryException(ex);
        }
        return files;
    }

}
