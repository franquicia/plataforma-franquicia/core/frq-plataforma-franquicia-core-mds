/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.imperdonable.dashboard.rest;

import com.gs.baz.frq.imperdonable.dashboard.dao.ImperdonableDashboardDAOImpl;
import com.gs.baz.frq.imperdonable.dashboard.dto.ImperdonableDashboardDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/imperdonable/dashboard")
@Component("FRQServiceImperdonableDashboard")
public class ServiceImperdonableDashboard {

    @Autowired
    private ImperdonableDashboardDAOImpl imperdonableDashboardDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ImperdonableDashboardDTO> getImperdonableDashboard() throws CustomException {
        return imperdonableDashboardDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_dashboard}/{id_pregunta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ImperdonableDashboardDTO getImperdonableDashboard(@PathVariable("id_dashboard") Long id_dashboard, @PathVariable("id_pregunta") Long id_pregunta) throws CustomException {
        return imperdonableDashboardDAOImpl.selectRow(id_dashboard, id_pregunta);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ImperdonableDashboardDTO postImperdonableDashboard(@RequestBody ImperdonableDashboardDTO cumplimientoDashboard) throws CustomException {
        if (cumplimientoDashboard.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (cumplimientoDashboard.getIdPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta"));
        }
        if (cumplimientoDashboard.getIdChecklist() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_checklist"));
        }
        if (cumplimientoDashboard.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (cumplimientoDashboard.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        return imperdonableDashboardDAOImpl.insertRow(cumplimientoDashboard);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ImperdonableDashboardDTO putImperdonableDashboard(@RequestBody ImperdonableDashboardDTO cumplimientoDashboard) throws CustomException {
        if (cumplimientoDashboard.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (cumplimientoDashboard.getIdPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta"));
        }
        if (cumplimientoDashboard.getIdChecklist() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_checklist"));
        }
        if (cumplimientoDashboard.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (cumplimientoDashboard.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        return imperdonableDashboardDAOImpl.updateRow(cumplimientoDashboard);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ImperdonableDashboardDTO deleteImperdonableDashboard(@RequestBody ImperdonableDashboardDTO cumplimientoDashboard) throws CustomException {
        if (cumplimientoDashboard.getIdDashboard() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_dashboard"));
        }
        if (cumplimientoDashboard.getIdPregunta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_pregunta"));
        }
        return imperdonableDashboardDAOImpl.deleteRow(cumplimientoDashboard);
    }

}
