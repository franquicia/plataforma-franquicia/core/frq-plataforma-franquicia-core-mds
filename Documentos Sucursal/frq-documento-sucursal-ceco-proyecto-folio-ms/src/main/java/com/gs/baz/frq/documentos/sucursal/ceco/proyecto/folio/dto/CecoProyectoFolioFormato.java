package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion sobre la relacion entre centro de costos, proyecto y numero de Folio", value = "CecoProyectoFolio")
public class CecoProyectoFolioFormato {

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del centro de costo", example = "1")
    private String idCeco;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1")
    private String nombreProyecto;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "folio")
    @ApiModelProperty(notes = "Número de folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Número de idFormato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Número de idDocumento", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "idEstatus")
    @ApiModelProperty(notes = "Estado  la relacion ", example = "1")
    private Integer idEstatus;

    public CecoProyectoFolioFormato() {

    }

    public CecoProyectoFolioFormato(String idCeco, Integer idProyecto, Integer idFolio, Integer idFormato, Integer idDocumento, Integer idEstatus) {
        this.idCeco = idCeco;
        this.idProyecto = idProyecto;
        this.idFolio = idFolio;
        this.idFormato = idFormato;
        this.idDocumento = idDocumento;
        this.idEstatus = idEstatus;
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(Integer idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

}
