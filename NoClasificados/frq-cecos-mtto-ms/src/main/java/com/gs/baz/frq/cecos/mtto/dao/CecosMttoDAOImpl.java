package com.gs.baz.frq.cecos.mtto.dao;

import com.gs.baz.frq.cecos.mtto.dto.CecosMttoDTO;
import com.gs.baz.frq.cecos.mtto.mprs.CecosMttoRowMapper;
import com.gs.baz.frq.cecos.mtto.mprs.JefeSuperiorMttoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class CecosMttoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertCeco;
    private DefaultJdbcCall jdbcUpdateCeco;
    private DefaultJdbcCall jdbcDeleteCeco;
    private DefaultJdbcCall jdbcObtieneCeco;

    private DefaultJdbcCall jdbcObtieneLugarMtto;
    private DefaultJdbcCall jdbcUpdateJefeUsuario;
    private DefaultJdbcCall jdbcDeleteJefeUsuario;
    private DefaultJdbcCall jdbcInsertaJefeUsuario;

    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertCeco.withSchemaName(schema);
        jdbcInsertCeco.withCatalogName("PACECOMTTO");
        jdbcInsertCeco.withProcedureName("SP_INS_CECO");

        jdbcUpdateCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateCeco.withSchemaName(schema);
        jdbcUpdateCeco.withCatalogName("PACECOMTTO");
        jdbcUpdateCeco.withProcedureName("SP_ACT_CECO");

        jdbcDeleteCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteCeco.withSchemaName(schema);
        jdbcDeleteCeco.withCatalogName("PACECOMTTO");
        jdbcDeleteCeco.withProcedureName("SP_DEL_CECO");

        jdbcObtieneCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneCeco.withSchemaName(schema);
        jdbcObtieneCeco.withCatalogName("PACECOMTTO");
        jdbcObtieneCeco.withProcedureName("SP_SEL_CECO");
        jdbcObtieneCeco.returningResultSet("RCL_REPO", new CecosMttoRowMapper());

        jdbcObtieneLugarMtto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneLugarMtto.withSchemaName(schema);
        jdbcObtieneLugarMtto.withCatalogName("PACECOMTTO");
        jdbcObtieneLugarMtto.withProcedureName("SP_SEL_JEFE");
        jdbcObtieneLugarMtto.returningResultSet("RCL_USUARIOS", new JefeSuperiorMttoRowMapper());

        jdbcUpdateJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateJefeUsuario.withSchemaName(schema);
        jdbcUpdateJefeUsuario.withCatalogName("PACECOMTTO");
        jdbcUpdateJefeUsuario.withProcedureName("SP_ACT_JEFE");
        // jdbcObtieneAsignacionCeco.returningResultSet("RCL_ASIGN", new AsignacionRowMapper());

        jdbcDeleteJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteJefeUsuario.withSchemaName(schema);
        jdbcDeleteJefeUsuario.withCatalogName("PACECOMTTO");
        jdbcDeleteJefeUsuario.withProcedureName("SP_DEL_JEFE");
        // jdbcObtieneDesbloqueaFase.returningResultSet("SP_SEL_ASIG", new AsignacionRowMapper());

        jdbcInsertaJefeUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertaJefeUsuario.withSchemaName(schema);
        jdbcInsertaJefeUsuario.withCatalogName("PACECOMTTO");
        jdbcInsertaJefeUsuario.withProcedureName("SP_INS_JEFE");

    }

    public int insertRow(CecosMttoDTO entityDTO) throws CustomException {
        try {

            int error = 0;
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getNegocio());
            mapSqlParameterSource.addValue("PA_OBSERV", entityDTO.getObservaciones());
            mapSqlParameterSource.addValue("PA_CECOSUP", entityDTO.getIdCecoSuperior());
            mapSqlParameterSource.addValue("PA_IDCECO", entityDTO.getIdCeco());
            Map<String, Object> out = jdbcInsertCeco.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Ceco"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Ceco"), ex);
        }
    }

    public CecosMttoDTO updateRow(CecosMttoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getNegocio());
            mapSqlParameterSource.addValue("PA_OBSERV", entityDTO.getObservaciones());
            mapSqlParameterSource.addValue("PA_CECOSUP", entityDTO.getIdCecoSuperior());
            mapSqlParameterSource.addValue("PA_IDCECO", entityDTO.getIdCeco());

            Map<String, Object> out = jdbcUpdateCeco.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Ceco"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Ceco"), ex);
        }
    }

    public int eliminaRows(CecosMttoDTO entityDTO) throws CustomException {
        try {
            int error = 0;
            mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_IDCECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_CECOSUP", entityDTO.getIdCecoSuperior());

            Map<String, Object> out = jdbcDeleteCeco.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Ceco"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Ceco"), ex);
        }
    }

//trae Cecos 
    public List<CecosMttoDTO> selectRowDTO(CecosMttoDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();

        mapSqlParameterSource.addValue("PA_IDCECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_CECOSUP", entityDTO.getIdCecoSuperior());

        Map<String, Object> out = jdbcObtieneCeco.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<CecosMttoDTO> data = (List<CecosMttoDTO>) out.get("RCL_REPO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    /*
    public int insertJefeRow(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        try {
        	
      int error=0;
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
            mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getIdNivel());
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_OBSERVA", entityDTO.getObservaciones());
           
            Map<String, Object> out = jdbcInsertaJefeUsuario.execute(mapSqlParameterSource);
            
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

    		BigDecimal resultado =(BigDecimal) out.get("PA_EJECUCION");
    		error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

               
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  JefeSuperior"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  JefeSuperior"), ex);
        }
    }
    
    public JefeSuperiorMttoDTO updateJefeRow(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
            mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getIdNivel());
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_OBSERVA", entityDTO.getObservaciones());

            Map<String, Object> out = jdbcUpdateJefeUsuario.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of JefeSuperior"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of JefeSuperior"), ex);
        }
    }
    
    public int eliminaJefeRows(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        try {
        	int error=0;
            mapSqlParameterSource = new MapSqlParameterSource();
            
            mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
            
        
            Map<String, Object> out = jdbcDeleteJefeUsuario.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado =(BigDecimal) out.get("PA_EJECUCION");
    		error = resultado.intValue();

            if (success) {
               
                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of JefeSuperior"));
                
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of JefeSuperior"), ex);
            
        }
    }


//trae Cecos 
   
    public List<JefeSuperiorMttoDTO >selectRowJefe(JefeSuperiorMttoDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_SUPERIOR", entityDTO.getIdUsuarioJefe());
        mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
        Map<String, Object> out = jdbcObtieneJefeUsuario.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
		List<JefeSuperiorMttoDTO> data = (List<JefeSuperiorMttoDTO>) out.get("RCL_USUARIOS");
        if (data.size() > 0) {
            return data ;
        } else {
            return null;
        }
    }
     */
}
