package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.init;

import com.gs.baz.frq.data.sources.config.GenericConfig;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionArchivosLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionCecosLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionFirmasLayoutDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao.SistemaInformacionProveedoresLayoutDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Import(GenericConfig.class)
@Configuration
@ComponentScan("com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores")
public class ConfigLayoutProveedores {

    private final Logger logger = LogManager.getLogger();

    public ConfigLayoutProveedores() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public SistemaInformacionCecosLayoutDAOImpl sistemaInformacionCecosLayoutDAOImpl() {
        return new SistemaInformacionCecosLayoutDAOImpl();
    }
    
    @Bean(initMethod = "init")
    public SistemaInformacionProveedoresLayoutDAOImpl sistemaInformacionProveedoresLayoutDAOImpl() {
        return new SistemaInformacionProveedoresLayoutDAOImpl();
    }

     @Bean(initMethod = "init")
    public SistemaInformacionArchivosLayoutDAOImpl sistemaInformacionArchivosLayoutDAOImpl() {
        return new SistemaInformacionArchivosLayoutDAOImpl();
    }

       @Bean(initMethod = "init")
    public SistemaInformacionFirmasLayoutDAOImpl sistemaInformacionFirmasLayoutDAOImpl() {
        return new SistemaInformacionFirmasLayoutDAOImpl();
    }

    
    
}
