/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Imperdonable;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ImperdonablesRowMapper implements RowMapper<Imperdonable> {

    @Override
    public Imperdonable mapRow(ResultSet rs, int rowNum) throws SQLException {
        Imperdonable imperdonable = new Imperdonable();
        imperdonable.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        imperdonable.setDescripcion(rs.getString("FCDESCRIPCION"));
        imperdonable.setPuntosVenta(rs.getInt("TOTALPDV"));
        return imperdonable;
    }
}
