/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.mprs;

import com.gs.baz.frq.accesos.sistema.usuarios.dto.UsuarioBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UsuarioRowMapper implements RowMapper<UsuarioBase> {

    @Override
    public UsuarioBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsuarioBase usuarioBase = new UsuarioBase();
        usuarioBase.setTipoEmpleado(rs.getString("FCTIPOEMPLEADO"));
        usuarioBase.setNombreUsuario(rs.getString("FCNOMBRE"));
        usuarioBase.setCentroCostos(rs.getString("FCID_CECO"));
        usuarioBase.setCorreo(rs.getString("FCCORREO"));
        return usuarioBase;
    }
}
