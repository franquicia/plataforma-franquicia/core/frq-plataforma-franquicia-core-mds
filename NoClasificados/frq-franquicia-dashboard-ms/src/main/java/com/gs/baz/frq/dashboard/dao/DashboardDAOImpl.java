package com.gs.baz.frq.dashboard.dao;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.ConteoDTO;
import com.gs.baz.frq.dashboard.dto.DashboardDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.DisciplinaDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.dto.util.PeriodoDTO;
import com.gs.baz.frq.dashboard.mprs.ConteoRowMapper;
import com.gs.baz.frq.dashboard.mprs.DashboardRowMapper;
import com.gs.baz.frq.dashboard.mprs.graficos.DisciplinaRowMapper;
import com.gs.baz.frq.dashboard.mprs.util.PeriodoRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectDisciplina;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelectFechas;
    private DefaultJdbcCall jdbcSelectConteo;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMDASHBOARD");
        jdbcInsert.withProcedureName("SPINSDASHBOARD");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMDASHBOARD");
        jdbcUpdate.withProcedureName("SPACTDASHBOARD");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMDASHBOARD");
        jdbcDelete.withProcedureName("SPDELDASHBOARD");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMDASHBOARD");
        jdbcSelect.withProcedureName("SPGETDASHBOARD");
        jdbcSelect.returningResultSet("PA_CDATOS", new DashboardRowMapper());

        jdbcSelectDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDisciplina.withSchemaName(schema);
        jdbcSelectDisciplina.withCatalogName("PACONDASHGRAL");
        jdbcSelectDisciplina.withProcedureName("SPGETDISCIPLINA");
        jdbcSelectDisciplina.returningResultSet("RCL_INFO", new DisciplinaRowMapper());

        jdbcSelectFechas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFechas.withSchemaName(schema);
        jdbcSelectFechas.withCatalogName("PAFECHADASH");
        jdbcSelectFechas.withProcedureName("SPGETFECHA");
        jdbcSelectFechas.returningResultSet("PA_CURSOR", new PeriodoRowMapper());

        jdbcSelectConteo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectConteo.withSchemaName(schema);
        jdbcSelectConteo.withCatalogName("PAVISTADISC");
        jdbcSelectConteo.withProcedureName("SPGETCONTDISCI");
        jdbcSelectConteo.returningResultSet("RCL_INFO", new ConteoRowMapper());
    }

    public List<DashboardDTO> selectRows(DashboardDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
        mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfiguracionDisciplina());
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<DashboardDTO>) out.get("PA_CDATOS");
    }

    public List<DisciplinaDTO> selectRowsDisciplina(DisciplinaDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectDisciplina.execute(mapSqlParameterSource);
        return (List<DisciplinaDTO>) out.get("RCL_INFO");
    }

    public boolean updateRowDashboard(DashboardDTO entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
        mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfiguracionDisciplina());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProotocolo());
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getDescripcion());
        mapSqlParameterSource.addValue("PA_FCID_TERRITORIO", entityDTO.getIdTerritorio());
        mapSqlParameterSource.addValue("PA_FCTERRITORIO", entityDTO.getTerritorio());
        mapSqlParameterSource.addValue("PA_FCID_ZONA", entityDTO.getIdZona());
        mapSqlParameterSource.addValue("PA_FCZONA", entityDTO.getZona());
        mapSqlParameterSource.addValue("PA_FCID_REGION", entityDTO.getIdRegion());
        mapSqlParameterSource.addValue("PA_FCREGION", entityDTO.getRegion());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        mapSqlParameterSource.addValue("PA_FICALIFICACION", entityDTO.getCalificacion());
        mapSqlParameterSource.addValue("PA_FINUM_IMPERDON", entityDTO.getNumeroImperdonable());
        mapSqlParameterSource.addValue("PA_FINUM_INCUMPLM", entityDTO.getNumeroIncumplimiento());
        mapSqlParameterSource.addValue("PA_FDTERMINO", entityDTO.getFechaTermino());
        mapSqlParameterSource.addValue("PA_FIPUNTOSOBT", entityDTO.getPuntosObtenidos());
        mapSqlParameterSource.addValue("PA_FIPUNTOSESP", entityDTO.getPuntosEsperados());
        mapSqlParameterSource.addValue("PA_FIDEFECTOS", entityDTO.getIdEfectos());
        mapSqlParameterSource.addValue("PA_FIOPORTUNIDADES", entityDTO.getOportunidades());
        mapSqlParameterSource.addValue("PA_FCCECONEG_ID", entityDTO.getIdCecoNegocio());
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
        mapSqlParameterSource.addValue("PA_FCNOMBREUSR", entityDTO.getNombreUsuario());
        mapSqlParameterSource.addValue("PA_FIID_PUESTO", entityDTO.getIdPuesto());

        Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;
        if (success) {
            entityDTO.setUpdated(success);
            return true;
        } else {
            throw new CustomException(ModelCodes.ATTRIBUTE_DATA_NOT_FOUND.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of dashboard"));
        }
    }

    public boolean deleteRowDashboard(DashboardDTO entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDASHBOARD_ID", entityDTO.getIdDashboard());
        mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfiguracionDisciplina());
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;
        if (success) {
            entityDTO.setUpdated(success);
            return true;
        } else {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of dashboard"));
        }
    }

    public DashboardDTO insertRow(DashboardDTO entityDTO) throws CustomException {
        try {

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfiguracionDisciplina());
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProotocolo());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCID_TERRITORIO", entityDTO.getIdTerritorio());
            mapSqlParameterSource.addValue("PA_FCTERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_FCID_ZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_FCZONA", entityDTO.getZona());
            mapSqlParameterSource.addValue("PA_FCID_REGION", entityDTO.getIdRegion());
            mapSqlParameterSource.addValue("PA_FCREGION", entityDTO.getRegion());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
            mapSqlParameterSource.addValue("PA_FICALIFICACION", entityDTO.getCalificacion());
            mapSqlParameterSource.addValue("PA_FINUM_IMPERDON", entityDTO.getNumeroImperdonable());
            mapSqlParameterSource.addValue("PA_FINUM_INCUMPLM", entityDTO.getNumeroIncumplimiento());
            mapSqlParameterSource.addValue("PA_FDTERMINO", entityDTO.getFechaTermino());
            mapSqlParameterSource.addValue("PA_FIPUNTOSOBT", entityDTO.getPuntosObtenidos());
            mapSqlParameterSource.addValue("PA_FIPUNTOSESP", entityDTO.getPuntosEsperados());
            mapSqlParameterSource.addValue("PA_FIDEFECTOS", entityDTO.getIdEfectos());
            mapSqlParameterSource.addValue("PA_FIOPORTUNIDADES", entityDTO.getOportunidades());
            mapSqlParameterSource.addValue("PA_FCCECONEG_ID", entityDTO.getIdCecoNegocio());
            mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FCNOMBREUSR", entityDTO.getNombreUsuario());
            mapSqlParameterSource.addValue("PA_FIID_PUESTO", entityDTO.getIdPuesto());
            mapSqlParameterSource.addValue("PA_FIPRECALIFICA", entityDTO.getPrecalificacion());
            mapSqlParameterSource.addValue("PA_FICALIF_LINEAL", entityDTO.getCalificacionLineal());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdDashboard((BigDecimal) out.get("PA_FIDASHBOARD_ID"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  dashboard"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  dashboard"), ex);
        }
    }

    public List<DisciplinaDTO> selectRowsDisciplinaDas() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAI", null);
        mapSqlParameterSource.addValue("PA_FECHAF", null);
        Map<String, Object> out = jdbcSelectDisciplina.execute(mapSqlParameterSource);
        //TRAER TODAS LAS DISCIPLINAS QUE EXISTEN EN DASHBOARD
        List<DisciplinaDTO> disciplinasExistentes = (List<DisciplinaDTO>) out.get("RCL_INFO");
        for (DisciplinaDTO disc : disciplinasExistentes) {

            FiltrosDTO filtro = new FiltrosDTO();

            //PeriodoDTO per = selectRowPeriodosAnteriores();
            PeriodoDTO per = periodoDAOImpl.selectRowActual();
            if (disc.getIdFrecuenciaMedicion() == 1) { //Semanal
                filtro.setNumeroPeriodo(per.getSemana());
            }
            if (disc.getIdFrecuenciaMedicion() == 2) {//mensual
                filtro.setNumeroPeriodo(per.getMes());
            }
            if (disc.getIdFrecuenciaMedicion() == 3) {//bimestral
                filtro.setNumeroPeriodo(per.getBimestre());
            }
            if (disc.getIdFrecuenciaMedicion() == 4) { //trimestral
                filtro.setNumeroPeriodo(per.getTrimestre());
            }

            filtro.setAnio(per.getAnio());
            filtro.setIdDisciplina(disc.getIdDisciplina());
            disc.setFiltros(filtro);
            FechasDTO fechas = periodoDAOImpl.getFechas(filtro);
            disc.setConteoDatos(selectRowConteoDatos(disc, fechas));
        }

        disciplinasExistentes.removeIf(d -> d.getConteoDatos().equals(0));

        return disciplinasExistentes;
    }

    public PeriodoDTO selectRowPeriodosAnteriores() throws CustomException {
        Map<String, Object> out = jdbcSelectFechas.execute();
        List<PeriodoDTO> data = (List<PeriodoDTO>) out.get("PA_CURSOR");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public Integer selectRowConteoDatos(DisciplinaDTO entityDTO, FechasDTO fechas) throws CustomException {
        int conteoDatos = 0;
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDDISCI", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FRECMED", entityDTO.getIdFrecuenciaMedicion());
            mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
            mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());

            Map<String, Object> out = jdbcSelectConteo.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            if (success) {
                List<ConteoDTO> data = (List<ConteoDTO>) out.get("RCL_INFO");
                if (data.size() > 0) {
                    return data.get(0).getConteo();
                } else {
                    return null;
                }

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to get conteo datos dashboard por disciplina"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to get conteo datos dashboard por disciplina"), ex);
        }
    }

}
