package com.gs.baz.frq.limpieza.remedy.dto;

public class CuadrillaDTO {

    private Integer idCuadrilla;
    private Integer idProveedor;
    private Integer zona;
    private String liderCuadrilla;
    private String correo;
    private String segundo;
    private String ps5;

    public CuadrillaDTO() {
        super();

    }

    public String getPs5() {
        return ps5;
    }

    public void setPs5(String ps5) {
        this.ps5 = ps5;
    }

    public int getIdCuadrilla() {
        return idCuadrilla;
    }

    public void setIdCuadrilla(Integer idCuadrilla) {
        this.idCuadrilla = idCuadrilla;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Integer getZona() {
        return zona;
    }

    public void setZona(Integer zona) {
        this.zona = zona;
    }

    public String getLiderCuad() {
        return liderCuadrilla;
    }

    public void setLiderCuad(String liderCuadrilla) {
        this.liderCuadrilla = liderCuadrilla;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSegundo() {
        return segundo;
    }

    public void setSegundo(String segundo) {
        this.segundo = segundo;
    }

    @Override
    public String toString() {
        return "CuadrillaDTO [idCuadrilla=" + idCuadrilla + ", idProveedor=" + idProveedor + ", zona=" + zona
                + ", liderCuadrilla=" + liderCuadrilla + ", correo=" + correo + ", segundo=" + segundo + ", ps5=" + ps5
                + "]";
    }

}
