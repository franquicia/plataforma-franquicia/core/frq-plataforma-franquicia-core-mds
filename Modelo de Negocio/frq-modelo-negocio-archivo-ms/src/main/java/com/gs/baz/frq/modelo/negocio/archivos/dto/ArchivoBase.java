/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class ArchivoBase {

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Ruta en la que se guardó el archivo", example = "/franquicia/modelo-negocio/")
    private String ruta;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "descripcion del archivo", example = "archivo principal foda")
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del archivo", example = "foda")
    private String nombre;

    @JsonProperty(value = "extension")
    @ApiModelProperty(notes = "extension del archivo", example = ".pdf")
    private String extension;

    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Tipo del archivo", example = "1")
    private Integer tipo;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "indice del archivo", example = "1")
    private Integer indice;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

}
