package com.gs.baz.frq.depuracion.mprs;

import com.gs.baz.frq.depuracion.dto.DepuracionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DepuracionRowMapper implements RowMapper<DepuracionDTO> {

    private DepuracionDTO depuracionDTO;

    @Override
    public DepuracionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        depuracionDTO = new DepuracionDTO();
        depuracionDTO.setBitacoras_eliminadas((BigDecimal) rs.getObject("PA_ELIMINADAS"));
        depuracionDTO.setEjecucion((BigDecimal) rs.getObject("PA_EJECUCION"));
        depuracionDTO.setRespuestas_eliminadas((BigDecimal) rs.getObject("PA_RESELIMIN"));
        return depuracionDTO;
    }
}
