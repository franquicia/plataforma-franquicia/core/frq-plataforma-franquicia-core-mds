package com.gs.baz.frq.dashboard.certificacion.dao.graficos.g006;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g006.DashboardCertificacionG006DTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g006.DashboardCertificacionG006RowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG006DAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PADASHCCPVCOB");
        jdbcSelect.withProcedureName("SPPUNTOSPENDIEN");
        jdbcSelect.returningResultSet("PA_CDATOS", new DashboardCertificacionG006RowMapper());
    }

    public List<DashboardCertificacionG006DTO> selectRowDashboardCertificacionG006(String entityDTO1, String entityDTO2, Long entityDTO3, Long entityDTO4, Long entityDTO5) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO1);
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO2);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO3);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO4);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO5);

        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionG006DTO>) out.get("PA_CDATOS");

    }

    public List<DashboardCertificacionG006DTO> selectRowsDashboardCertificacionG006() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", null);
        mapSqlParameterSource.addValue("PA_FECHAFIN", null);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", null);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", null);
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionG006DTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }

}
