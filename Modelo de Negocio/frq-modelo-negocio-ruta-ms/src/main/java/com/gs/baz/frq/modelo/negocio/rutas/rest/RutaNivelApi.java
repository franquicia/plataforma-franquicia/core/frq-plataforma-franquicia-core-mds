/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.rutas.dao.RutaCompuestaDAOImpl;
import com.gs.baz.frq.modelo.negocio.rutas.dao.RutaNivelDAOImpl;
import com.gs.baz.frq.modelo.negocio.rutas.dto.AltaRutaNivel;
import com.gs.baz.frq.modelo.negocio.rutas.dto.ParametrosRutaNivel;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaNivelBase;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutasCompuesta;
import com.gs.baz.frq.modelo.negocio.rutas.dto.RutasNivel;
import com.gs.baz.frq.modelo.negocio.rutas.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "rutasNivel", value = "rutasNivel", description = "Gestión de rutasNivel")
@RestController
@RequestMapping("/api-local/modelo-negocio/folio-rutas-niveles/v1")
public class RutaNivelApi {

    @Autowired
    private RutaNivelDAOImpl rutaNivelDAOImpl;

    @Autowired
    private RutaCompuestaDAOImpl rutaCompuestaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @param idRutaNivel
     * @param idModeloNegocio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutaNivel", notes = "Obtiene un rutaNivel", nickname = "obtieneRutaNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}/niveles/{idModeloNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RutaNivelBase obtieneRutaNivel(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Long idRutaNivel,
            @ApiParam(name = "idModeloNegocio", value = "Identificador de modelo negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idModeloNegocio) throws CustomException, DataNotFoundException {
        RutaNivelBase rutaNivelBase = rutaNivelDAOImpl.selectRow(idFolio, idRutaNivel, idModeloNegocio);
        if (rutaNivelBase == null) {
            throw new DataNotFoundException();
        }
        return rutaNivelBase;
    }

    /**
     *
     * @param idFolio
     * @param idRuta
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene rutasNivel", notes = "Obtiene todos los rutasNivel", nickname = "obtieneRutasNivel")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}/niveles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RutasNivel obtieneRutasNivel(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Long idRuta) throws CustomException, DataNotFoundException {
        RutasNivel rutasNivel = new RutasNivel(rutaNivelDAOImpl.selectAllRows(idFolio, idRuta));
        if (rutasNivel.getRutasNivel() != null && rutasNivel.getRutasNivel().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutasNivel;
    }

    /**
     *
     * @param idRuta
     * @param parametrosRutaNivel
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear rutaNivel", notes = "Agrega un rutaNivel", nickname = "creaRutaNivel")
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}/niveles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaRutaNivel creaRutaNivel(@ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Long idRuta, @ApiParam(name = "ParametrosRutaNivel", value = "Paramentros para el alta del rutaNivel", required = true) @RequestBody ParametrosRutaNivel parametrosRutaNivel) throws DataNotInsertedException {
        parametrosRutaNivel.setIdRuta(idRuta.intValue());
        return rutaNivelDAOImpl.insertRow(parametrosRutaNivel);
    }

    /**
     *
     * @param parametrosRutaNivel
     * @param idRutaNivel
     * @param idModeloNegocio
     * @param idRuta
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar rutaNivel", notes = "Actualiza un rutaNivel", nickname = "actualizaRutaNivel")
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}/niveles/{idModeloNegocio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaRutaNivel(@ApiParam(name = "ParametrosRutaNivel", value = "Paramentros para la actualización del rutaNivel", required = true) @RequestBody ParametrosRutaNivel parametrosRutaNivel,
            @ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Long idRuta,
            @ApiParam(name = "idModeloNegocio", value = "Identificador de modelo negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idModeloNegocio) throws DataNotUpdatedException {
        parametrosRutaNivel.setIdRuta(idRuta.intValue());
        parametrosRutaNivel.setIdModeloNegocio(idModeloNegocio.intValue());
        rutaNivelDAOImpl.updateRow(parametrosRutaNivel);
        return new SinResultado();
    }

    /**
     *
     * @param idRuta
     * @param idModeloNegocio
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar rutaNivel", notes = "Elimina un item de las rutasNivel", nickname = "eliminaRutaNivel")
    @RequestMapping(value = "/{idFolio}/rutas/{idRuta}/niveles/{idModeloNegocio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaRutaNivel(@ApiParam(name = "idRuta", value = "Identificador del ruta", example = "1", required = true) @PathVariable("idRuta") Integer idRuta,
            @ApiParam(name = "idModeloNegocio", value = "Identificador de modelo negocio", example = "1", required = true) @PathVariable("idModeloNegocio") Long idModeloNegocio) throws DataNotDeletedException {
        rutaNivelDAOImpl.deleteRow(idRuta);
        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @param idRuta
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene todas las rutas compuestas de un folio", notes = "Obtiene todas las rutas compuestas de un folio", nickname = "obtieneRutasCompuestas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/compuestas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RutasCompuesta obtieneRutasCompuestas(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        RutasCompuesta rutasCompuestas = rutaCompuestaDAOImpl.selectRutasCompuesta(idFolio);
        if (rutasCompuestas.getRutas() != null && rutasCompuestas.getRutas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return rutasCompuestas;
    }

}
