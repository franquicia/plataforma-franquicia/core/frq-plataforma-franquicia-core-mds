package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g004;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g004.DashboardCertificacionG004DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG004RowMapper implements RowMapper<DashboardCertificacionG004DTO> {

    private DashboardCertificacionG004DTO dashboardCertificacionG004DTO;

    @Override
    public DashboardCertificacionG004DTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        dashboardCertificacionG004DTO = new DashboardCertificacionG004DTO();
        dashboardCertificacionG004DTO.setCoberturaTotal((BigDecimal) rs.getObject("TOTAL_ASIGNADOS"));
        dashboardCertificacionG004DTO.setCoberturaCertificacion((BigDecimal) rs.getObject("TOTAL_CERTIFICADOS"));
        dashboardCertificacionG004DTO.setRango((BigDecimal) rs.getObject("RANGO"));
        dashboardCertificacionG004DTO.setRangoColor(rs.getString("RANGOCOLOR"));
        dashboardCertificacionG004DTO.setDiasRestantes((BigDecimal) rs.getObject("DIAS_RESTANTES"));
        return dashboardCertificacionG004DTO;
    }
}
