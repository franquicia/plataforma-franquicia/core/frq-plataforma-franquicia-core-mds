/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.acceso.dao.GrupoDAOImpl;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.AltaDocumentoGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.AltaGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.AltaUsuarioGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.DocumentosGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.GrupoBase;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.Grupos;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.ParametrosDocumentoGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.ParametrosGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.ParametrosUsuarioGrupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.SinResultado;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.UsuariosGrupo;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "accesos", value = "accesos", description = "Gestión de grupos")
@RestController
@RequestMapping("/api-local/modelo-negocio/accesos/grupos/v1")
public class ModeloNegocioAccesoGrupoApi {

    @Autowired
    private GrupoDAOImpl documentoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documentos", notes = "Obtiene todos los grupos", nickname = "obtieneGrupos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Grupos obtieneGrupos() throws CustomException, DataNotFoundException {
        Grupos grupos = new Grupos(documentoDAOImpl.selectAllRows());
        if (grupos.getGrupos() != null && grupos.getGrupos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return grupos;
    }

    /**
     *
     * @param idGrupo
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene grupo", notes = "Obtiene un grupo", nickname = "obtieneGrupo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrupo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GrupoBase obtieneGrupo(
            @ApiParam(name = "idGrupo", value = "Identificador de grupo", example = "1", required = true) @PathVariable("idGrupo") Long idGrupo) throws CustomException, DataNotFoundException {
        GrupoBase documentoBase = documentoDAOImpl.selectRow(idGrupo);
        if (documentoBase == null) {
            throw new DataNotFoundException();
        }
        return documentoBase;
    }

    /**
     *
     * @param parametrosGrupo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear grupo", notes = "Agrega un grupo", nickname = "creaGrupo")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaGrupo creaGrupo(
            @ApiParam(name = "ParametrosGrupo", value = "Paramentros para el alta del grupo", required = true) @RequestBody ParametrosGrupo parametrosGrupo) throws DataNotInsertedException {
        return documentoDAOImpl.insertRow(parametrosGrupo);
    }

    /**
     *
     * @param idGrupo
     * @param parametrosGrupo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar grupo", notes = "Actualiza un grupo", nickname = "actualizaGrupo")
    @RequestMapping(value = "/{idGrupo}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaGrupo(
            @ApiParam(name = "idGrupo", value = "Identificador de grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo,
            @ApiParam(name = "ParametrosGrupo", value = "Paramentros para la actualización del grupo", required = true) @RequestBody ParametrosGrupo parametrosGrupo) throws DataNotUpdatedException {
        parametrosGrupo.setIdGrupo(idGrupo);
        documentoDAOImpl.updateRow(parametrosGrupo);
        return new SinResultado();
    }

    /**
     *
     * @param idGrupo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grupo", notes = "Elimina un item de los grupos", nickname = "eliminaGrupo")
    @RequestMapping(value = "/{idGrupo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaGrupo(@ApiParam(name = "idGrupo", value = "Identificador del grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo) throws DataNotDeletedException {
        documentoDAOImpl.deleteRow(idGrupo);
        return new SinResultado();
    }

    /**
     *
     * @param idGrupo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios grupo", notes = "Obtiene todos los usuarios del grupo", nickname = "obtieneUsuariosGrupos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrupo}/usuarios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuariosGrupo obtieneUsuariosGrupos(@ApiParam(name = "idGrupo", value = "Identificador del grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo) throws CustomException, DataNotFoundException {
        UsuariosGrupo usuariosGrupo = new UsuariosGrupo(documentoDAOImpl.selectAllUsuarios(idGrupo));
        if (usuariosGrupo.getUsuarios() != null && usuariosGrupo.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuariosGrupo;
    }

    /**
     *
     * @param idGrupo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios disponibles grupo", notes = "Obtiene todos los usuarios disponibles del grupo", nickname = "obtieneUsuariosDisponiblesGrupos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrupo}/usuarios/disponibles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuariosGrupo obtieneUsuariosDisponiblesGrupos(@ApiParam(name = "idGrupo", value = "Identificador del grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo) throws CustomException, DataNotFoundException {
        UsuariosGrupo usuariosGrupo = new UsuariosGrupo(documentoDAOImpl.selectAllUsuariosDisponibles(idGrupo));
        if (usuariosGrupo.getUsuarios() != null && usuariosGrupo.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuariosGrupo;
    }

    /**
     *
     * @param idGrupo
     * @param parametrosUsuarioGrupo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crea grupo usuario", notes = "Agrega un grupo usuario", nickname = "creaUsuarioGrupo")
    @RequestMapping(value = "/{idGrupo}/usuarios", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaUsuarioGrupo creaUsuarioGrupo(
            @ApiParam(name = "idGrupo", value = "Identificador del grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo,
            @ApiParam(name = "ParametrosUsuarioGrupo", value = "Paramentros para el alta del usuario en un grupo", required = true) @RequestBody ParametrosUsuarioGrupo parametrosUsuarioGrupo) throws DataNotInsertedException {
        parametrosUsuarioGrupo.setIdGrupo(idGrupo);
        return documentoDAOImpl.insertGrupoUsuario(parametrosUsuarioGrupo);
    }

    /**
     *
     * @param idGrupoUsuario
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar grupo usuario", notes = "Elimina un item de los grupos del usuario", nickname = "eliminaUsuarioGrupo")
    @RequestMapping(value = "/{idGrupo}/usuarios/{idGrupoUsuario}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaUsuarioGrupo(@ApiParam(name = "idGrupoUsuario", value = "Identificador del grupo usuario", example = "1", required = true) @PathVariable("idGrupoUsuario") Integer idGrupoUsuario) throws DataNotDeletedException {
        documentoDAOImpl.deleteUsuario(idGrupoUsuario);
        return new SinResultado();
    }

    /**
     *
     * @param headers
     * @param idGrupo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documentos por grupo", notes = "Obtiene documentos por grupo", nickname = "obtieneDocumentosGrupo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrupo}/documentos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentosGrupo obtieneDocumentosGrupo(@RequestHeader HttpHeaders headers, @ApiParam(name = "idGrupo", value = "Identificador del grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo) throws CustomException, DataNotFoundException {
        DocumentosGrupo documentosGrupo = new DocumentosGrupo(documentoDAOImpl.selectAllDocumentos(headers, idGrupo));
        if (documentosGrupo.getDocumentos() != null && documentosGrupo.getDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return documentosGrupo;
    }

    /**
     *
     * @param headers
     * @param idGrupo
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene documentos por grupo", notes = "Obtiene documentos disponibles por grupo", nickname = "obtieneDocumentosDisponiblesGrupo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrupo}/documentos/disponibles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentosGrupo obtieneDocumentosDisponiblesGrupo(@RequestHeader HttpHeaders headers, @ApiParam(name = "idGrupo", value = "Identificador del grupo", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo) throws CustomException, DataNotFoundException {
        DocumentosGrupo documentosGrupo = new DocumentosGrupo(documentoDAOImpl.selectAllDocumentosDisponibles(headers, idGrupo));
        if (documentosGrupo.getDocumentos() != null && documentosGrupo.getDocumentos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return documentosGrupo;
    }

    /**
     *
     * @param idGrupo
     * @param parametrosDocumentoGrupo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crea documento grupo", notes = "Crea documento grupo", nickname = "creaDocumentoGrupo")
    @RequestMapping(value = "/{idGrupo}/documentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaDocumentoGrupo creaDocumentoGrupo(
            @ApiParam(name = "idGrupo", value = "Identificador del empleado", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo,
            @ApiParam(name = "ParametrosDocumentoGrupo", value = "Paramentros para el alta del grupo usuario", required = true) @RequestBody ParametrosDocumentoGrupo parametrosDocumentoGrupo) throws DataNotInsertedException {
        parametrosDocumentoGrupo.setIdGrupo(idGrupo);
        return documentoDAOImpl.insertDocumentoGrupo(parametrosDocumentoGrupo);
    }

    /**
     *
     * @param idGrupo
     * @param idDocumentoGrupo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Elimina documento grupo", notes = "Elimina documento grupo", nickname = "eliminaDocumentoGrupo")
    @RequestMapping(value = "/{idGrupo}/documentos/{idDocumentoGrupo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaDocumentoGrupo(
            @ApiParam(name = "idGrupo", value = "Identificador del empleado", example = "1", required = true) @PathVariable("idGrupo") Integer idGrupo,
            @ApiParam(name = "idDocumentoGrupo", value = "Identificador del documento grupo", example = "1", required = true) @PathVariable("idDocumentoGrupo") Integer idDocumentoGrupo) throws DataNotDeletedException {
        documentoDAOImpl.deleteDocumento(idDocumentoGrupo);
        return new SinResultado();
    }

}
