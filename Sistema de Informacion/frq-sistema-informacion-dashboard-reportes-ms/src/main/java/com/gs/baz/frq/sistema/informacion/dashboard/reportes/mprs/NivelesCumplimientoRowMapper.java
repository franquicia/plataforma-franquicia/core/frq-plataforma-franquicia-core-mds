/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NivelCumplimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NivelesCumplimientoRowMapper implements RowMapper<NivelCumplimiento> {

    @Override
    public NivelCumplimiento mapRow(ResultSet rs, int rowNum) throws SQLException {
        NivelCumplimiento nivelCumplimiento = new NivelCumplimiento();
        nivelCumplimiento.setPuntosVenta(rs.getInt("TOTALPDV"));
        nivelCumplimiento.setIdRango(rs.getInt("RANGO_ID"));
        nivelCumplimiento.setPorcentaje(rs.getDouble("PORCENTAJE"));
        return nivelCumplimiento;
    }
}
