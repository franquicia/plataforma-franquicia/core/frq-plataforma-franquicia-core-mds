package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteCheckAsistMttoDTO;

/**
 *
 * @author cescobarh
 */
public class RepoCheckAsistenciaRowMapper implements RowMapper<ReporteCheckAsistMttoDTO> {

    private ReporteCheckAsistMttoDTO status;

    @Override
    public ReporteCheckAsistMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteCheckAsistMttoDTO();

        status.setIdAsistencia(rs.getInt("IDASIST"));
        status.setIdUsuario(rs.getString("FIID_USUARIO"));
        status.setNombreUsuario(rs.getString("NOMBRE_USU"));
        status.setFecha(rs.getString("FECHA"));
        status.setDia(rs.getString("DIA"));
        status.setPeriodo(rs.getString("FCPERIODO"));
        status.setSemana(rs.getString("SEMANA"));
        //entrada
        status.setLatitudEntrada(rs.getDouble("LATITUD_ENT"));
        status.setLongitudEntrada(rs.getDouble("LONGITUD_ENT"));
        status.setFechaInicio(rs.getString("FECHA_INICIO"));
        status.setHoraEntrada(rs.getString("HORA_ENTRADA"));
        status.setStatusEntrada(rs.getString("STATUS_ENTRADA"));
        status.setObservacionesEntrada(rs.getString("OBS_ENTRADA"));
        status.setRutaSalida(rs.getString("RUTA_ENT"));
        status.setLugarEntrada(rs.getString("LUGAR_ENT"));

        //salida
        status.setLatitudSalida(rs.getDouble("LATITUD_SAL"));
        status.setLongitudSalida(rs.getDouble("LONGITUD_SAL"));
        status.setFechaFin(rs.getString("FECHA_FIN"));
        status.setHoraSalida(rs.getString("HORA_SAL"));
        status.setStatusSalida(rs.getString("STATUS_SALIDA"));
        status.setObservacionesSalida(rs.getString("OBS_SALIDA"));
        status.setRutaSalida(rs.getString("RUTA_SAL"));
        status.setLugarSalida(rs.getString("LUGAR_SAL"));

        status.setDescuento(rs.getString("FCDESCUENTO"));
        status.setTotalVisitas(rs.getInt("VISITAS"));
        status.setTotalModulos(rs.getInt("MODULOS"));
        status.setHorasTrabajadas(rs.getString("HORAS_TRABAJADAS"));
        status.setTipoProyecto(rs.getString("FIID_TIPOPROY"));
        status.setConteoSucursales(rs.getInt("CONTEOSUC"));

        status.setIdTerritorio(rs.getInt("IDTERRITORIO"));
        status.setIdZona(rs.getInt("IDZONA"));
        status.setFcTerritorio(rs.getString("FCTERRITORIO"));
        status.setFcZona(rs.getString("FCZONA"));
        status.setNombreJefeZona(rs.getString("NOMJEFEZONA"));
        
        return status;
    }
}
