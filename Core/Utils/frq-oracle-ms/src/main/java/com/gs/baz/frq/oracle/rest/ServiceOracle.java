/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.oracle.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.oracle.dao.OracleBlock;
import java.util.Map;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.gs.baz.frq.oracle.dto.Oracle;
import com.gs.baz.frq.oracle.dto.Connection;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.ArrayList;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/oracle")
@Component("ServiceOracle")
public class ServiceOracle {

    final VersionBI versionBI = new VersionBI();

    @Autowired
    @Qualifier("frqDS")
    private DataSource frqDS;

    @Autowired
    @Qualifier("gtnDS")
    private DataSource gtnDS;

    @Autowired
    @Qualifier("mdfqrDS")
    private DataSource mdfqrDS;

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "/execute", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> execute(@RequestBody Oracle anonymusPLSQL) throws CustomException {
        Connection connection = anonymusPLSQL.getProperties().getConnection();
        if (connection == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("connection"));
        }
        String plSQL = anonymusPLSQL.getPlSQL();
        if (plSQL == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("plsql"));
        }
        DataSource dataSource;
        if (connection.getValue().equals(1)) {
            dataSource = frqDS;
        } else if (connection.getValue().equals(2)) {
            dataSource = gtnDS;
        } else {
            dataSource = mdfqrDS;
        }
        OracleBlock block = new OracleBlock(dataSource, anonymusPLSQL);
        return block.executePLSQLBlock();
    }

    @RequestMapping(value = "/connections", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Connection> connections() throws JsonProcessingException {
        List<Connection> connections = new ArrayList<>();
        connections.add(new Connection(1, "Franquicia"));
        connections.add(new Connection(2, "Gestion"));
        connections.add(new Connection(3, "Modelo Franquicia"));
        return connections;
    }

}
