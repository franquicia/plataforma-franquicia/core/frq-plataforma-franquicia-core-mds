/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author kramireza
 */
@ApiModel(description = "Datos de entrada para el metodo de bandeja de incidentes", value = "BandejaIncidentes")
public class BandejaIncidentesRequestDTO {

    @JsonProperty(value = "numeroEmpleado", required = true)
    @ApiModelProperty(notes = "Es el identificador del Usuario o número de empleado. "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "202622", position = -1, required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String numEmpleado;

    @JsonProperty(value = "tipoUsuario", required = true)
    @ApiModelProperty(notes = "Es el tipo de usuario, si es supervisor o proveedor", example = "1", required = true)
    private Integer tipoUsuario;

    @JsonProperty(value = "idProveedor", required = true)
    @ApiModelProperty(notes = "Es el identificador del proveedor", example = "6200247", position = -2, required = true)
    private Integer idProveedor;

    public BandejaIncidentesRequestDTO() {
        super();
    }

    public String getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(String numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public Integer getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(Integer tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

}
