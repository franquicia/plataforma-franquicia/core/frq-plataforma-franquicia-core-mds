/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.disciplinas.negocios.dto.ModeloFranquiciaDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;

/**
 *
 * @author B73601
 */
public class DataDisciplinasEjesNegociosModeloDTO {

    @JsonProperty(value = "metadata")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Metadata metadata;

    @JsonProperty(value = "modelo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ModeloFranquiciaDTO modeloDTO;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public ModeloFranquiciaDTO getModeloDTO() {
        return modeloDTO;
    }

    public void setModeloDTO(ModeloFranquiciaDTO modeloDTO) {
        this.modeloDTO = modeloDTO;
    }

    @Override
    public String toString() {
        return "DataModelosFranquiciaDTO{" + "metadata=" + metadata + ", modeloDTO=" + modeloDTO + '}';
    }

}
