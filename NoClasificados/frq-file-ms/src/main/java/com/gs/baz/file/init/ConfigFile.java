package com.gs.baz.file.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Configuration
@Import(ConfigBucketServices.class)
@ComponentScan("com.gs.baz.file")
public class ConfigFile {

    private final Logger logger = LogManager.getLogger();

    public ConfigFile() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
