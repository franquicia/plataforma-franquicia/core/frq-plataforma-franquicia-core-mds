/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.punto.contacto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoDTO {

    @JsonProperty(value = "id_punto_contacto")
    private Integer idPuntoContacto;

    @JsonProperty(value = "ceco")
    private Integer ceco;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonProperty(value = "id_estatus")
    private Integer idEstatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "detalle")
    private PuntoContactoDetalleDTO puntoContactoDetalleDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "asignacion")
    private PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "resultado_ok")
    private Boolean resultadoOk;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "usuarios_modificados")
    private Integer usuariosModificados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public PuntoContactoDTO() {
    }

    public Integer getIdPuntoContacto() {
        return idPuntoContacto;
    }

    public void setIdPuntoContacto(BigDecimal idPuntoContacto) {
        this.idPuntoContacto = (idPuntoContacto == null ? null : idPuntoContacto.intValue());
    }

    public Integer getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = new Integer(ceco);
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(BigDecimal idEstatus) {
        this.idEstatus = (idEstatus == null ? null : idEstatus.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public PuntoContactoDetalleDTO getPuntoContactoDetalleDTO() {
        return puntoContactoDetalleDTO;
    }

    public void setPuntoContactoDetalleDTO(PuntoContactoDetalleDTO puntoContactoDetalleDTO) {
        this.puntoContactoDetalleDTO = puntoContactoDetalleDTO;
    }

    public PuntoContactoFranquiciaModeloDTO getPuntoContactoFranquiciaModeloDTO() {
        return puntoContactoFranquiciaModeloDTO;
    }

    public void setPuntoContactoFranquiciaModeloDTO(PuntoContactoFranquiciaModeloDTO puntoContactoFranquiciaModeloDTO) {
        this.puntoContactoFranquiciaModeloDTO = puntoContactoFranquiciaModeloDTO;
    }

    public Boolean getResultadoOk() {
        return resultadoOk;
    }

    public void setResultadoOk(Boolean resultadoOk) {
        this.resultadoOk = resultadoOk;
    }

    public Integer getUsuariosModificados() {
        return usuariosModificados;
    }

    public void setUsuariosModificados(Integer usuariosModificados) {
        this.usuariosModificados = usuariosModificados;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "PuntoContactoDTO{" + "idPuntoContacto=" + idPuntoContacto + ", ceco=" + ceco + ", descripcion=" + descripcion + ", idEstatus=" + idEstatus + ", puntoContactoDetalleDTO=" + puntoContactoDetalleDTO + ", puntoContactoFranquiciaModeloDTO=" + puntoContactoFranquiciaModeloDTO + ", resultadoOk=" + resultadoOk + ", usuariosModificados=" + usuariosModificados + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
