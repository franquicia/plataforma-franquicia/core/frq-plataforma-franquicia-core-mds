/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.seccion.cumplimiento.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.seccion.cumplimiento.dao.SeccionCumplimientoDAOImpl;
import com.gs.baz.frq.seccion.cumplimiento.dto.SeccionCumplimientoDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/seccion/cumplimiento")
public class ServiceSeccionCumplimiento {

    @Autowired
    private SeccionCumplimientoDAOImpl respuestaVisitaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SeccionCumplimientoDTO> getSeccionesCumplimientos() throws CustomException {
        return respuestaVisitaDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/seccion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SeccionCumplimientoDTO> getSeccionCumplimiento(@RequestBody SeccionCumplimientoDTO seccionCumplimiento) throws CustomException {
        return respuestaVisitaDAOImpl.selectRowsSeccionCumplimiento(seccionCumplimiento);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeccionCumplimientoDTO post(@RequestBody SeccionCumplimientoDTO seccionCumplimiento) throws CustomException {
        if (seccionCumplimiento.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (seccionCumplimiento.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (seccionCumplimiento.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (seccionCumplimiento.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        if (seccionCumplimiento.getFechaVisista() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fecha_visita"));
        }
        if (seccionCumplimiento.getPonderacionEsperado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion_esperado"));
        }

        return respuestaVisitaDAOImpl.insertRow(seccionCumplimiento);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeccionCumplimientoDTO putSeccionCumplimiento(@RequestBody SeccionCumplimientoDTO seccionCumplimiento) throws CustomException {
        if (seccionCumplimiento.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (seccionCumplimiento.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (seccionCumplimiento.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (seccionCumplimiento.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        if (seccionCumplimiento.getFechaVisista() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fecha_visita"));
        }
        if (seccionCumplimiento.getPonderacionEsperado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion_esperado"));
        }

        return respuestaVisitaDAOImpl.updateRow(seccionCumplimiento);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeccionCumplimientoDTO deleteSeccionCumplimiento(@RequestBody SeccionCumplimientoDTO seccionCumplimiento) throws CustomException {
        if (seccionCumplimiento.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (seccionCumplimiento.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (seccionCumplimiento.getModulo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("modulo"));
        }
        if (seccionCumplimiento.getFechaVisista() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fecha_visita"));
        }
        return respuestaVisitaDAOImpl.deleteRow(seccionCumplimiento);
    }
}
