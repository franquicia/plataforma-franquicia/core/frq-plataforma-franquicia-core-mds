package com.gs.baz.frq.dashboard.mprs.graficos.g002;

import com.gs.baz.frq.dashboard.dto.graficos.g002.CalificacionNegociosDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionNegociosDisciplinaRowMapper implements RowMapper<CalificacionNegociosDisciplinaDTO> {

    private CalificacionNegociosDisciplinaDTO calificacionNegociosDisciplinaDTO;

    @Override
    public CalificacionNegociosDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        calificacionNegociosDisciplinaDTO = new CalificacionNegociosDisciplinaDTO();
        calificacionNegociosDisciplinaDTO.setIdDisciplina((BigDecimal) rs.getObject("FIDISCIPLINAID"));
        calificacionNegociosDisciplinaDTO.setCalificacionFinal((BigDecimal) rs.getObject("RESULTADO"));
        return calificacionNegociosDisciplinaDTO;
    }
}
