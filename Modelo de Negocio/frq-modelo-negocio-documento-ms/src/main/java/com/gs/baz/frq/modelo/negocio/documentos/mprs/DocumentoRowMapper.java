/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.documentos.mprs;

import com.gs.baz.frq.modelo.negocio.documentos.dto.DocumentoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoRowMapper implements RowMapper<DocumentoBase> {

    private DocumentoBase documentoBase;

    @Override
    public DocumentoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        documentoBase = new DocumentoBase();
        documentoBase.setTitulo(rs.getString("FCTITULO"));
        documentoBase.setObjetivo(rs.getString("FCOBJETIVO"));
        documentoBase.setVersion(rs.getString("FCVERSION"));
        documentoBase.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        documentoBase.setFechaInicio(rs.getString("FDVIGENTE"));
        //documentoBase.setEliminado(rs.getInt("FIELIMINADO"));
        return documentoBase;
    }
}
