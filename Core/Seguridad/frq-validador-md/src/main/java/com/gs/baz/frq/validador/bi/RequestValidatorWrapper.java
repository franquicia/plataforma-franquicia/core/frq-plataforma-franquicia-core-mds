/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.bi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Specifications;
import com.gs.baz.frq.validador.util.Base64Exception;
import com.gs.baz.frq.validador.util.Base64Utils;
import com.gs.baz.frq.validador.util.ForceStringDeserializer;
import com.gs.baz.frq.validador.util.NegativeParamValidationException;
import com.gs.baz.frq.validador.util.NegativePathParamValidationException;
import com.gs.baz.frq.validador.util.ParamValidationException;
import com.gs.baz.frq.validador.util.PathParamValidationException;
import com.gs.baz.frq.validador.util.RequiredDataException;
import com.gs.baz.frq.validador.util.SensibleDataBase64Exception;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 *
 * @author cescobarh
 */
public class RequestValidatorWrapper extends HttpServletRequestWrapper {

    private final Logger logger = LogManager.getLogger();
    private String body;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    private final Base64Utils.Validator validator = new Base64Utils.Validator();

    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws CustomException
     */
    public RequestValidatorWrapper(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws CustomException {
        super(httpServletRequest);
        try {
            body = new BufferedReader(new InputStreamReader(httpServletRequest.getInputStream())).lines().parallel().collect(Collectors.joining("\n"));
        } catch (IOException ex) {
            throw new CustomException(ex);
        }
        this.applyValidations(httpServletRequest);
    }

    private void applyValidations(HttpServletRequest httpServletRequest) throws CustomException {
        if (requestMappingHandlerMapping == null) {
            ServletContext servletContext = httpServletRequest.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            if (webApplicationContext != null) {
                requestMappingHandlerMapping = webApplicationContext.getBean(RequestMappingHandlerMapping.class);
            }
        }
        if (requestMappingHandlerMapping != null) {
            try {
                HandlerExecutionChain handlerExecutionChain = requestMappingHandlerMapping.getHandler(httpServletRequest);
                if (handlerExecutionChain != null) {
                    HandlerMethod handlerMethod = (HandlerMethod) handlerExecutionChain.getHandler();
                    if (handlerMethod != null) {
                        Parameter[] methodParameters = handlerMethod.getMethod().getParameters();
                        List<String> fieldsMissing = new ArrayList<>();
                        List<String> fieldsSensiblesNotBase64 = new ArrayList<>();
                        List<String> fieldsNotBase64 = new ArrayList<>();
                        for (Parameter itemMethodParameter : methodParameters) {
                            Map pathVariables = (Map) httpServletRequest.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
                            if (!itemMethodParameter.getType().isPrimitive()) {
                                RequestBody requestBody = itemMethodParameter.getAnnotation(RequestBody.class);
                                if (requestBody != null) {
                                    String mt = httpServletRequest.getContentType();
                                    if (body != null && !body.isEmpty()) {
                                        if (mt != null && mt.equals(MediaType.APPLICATION_JSON_VALUE)) {
                                            try {
                                                SimpleModule module = new SimpleModule();
                                                module.addDeserializer(String.class, new ForceStringDeserializer());
                                                objectMapper.configure(DeserializationFeature.ACCEPT_FLOAT_AS_INT, false);
                                                objectMapper.registerModule(module);
                                                Object out = objectMapper.readValue(body, itemMethodParameter.getType());
                                                for (Field itemField : itemMethodParameter.getType().getDeclaredFields()) {
                                                    Specifications specifications = itemField.getAnnotation(Specifications.class);
                                                    JsonProperty jsonProperty = itemField.getAnnotation(JsonProperty.class);
                                                    if (jsonProperty != null) {
                                                        try {
                                                            itemField.setAccessible(true);
                                                            Object value = itemField.get(out);
                                                            if (jsonProperty.required()) {
                                                                if (value == null) {
                                                                    fieldsMissing.add(jsonProperty.value());
                                                                } else {
                                                                    if (value.toString().isEmpty()) {
                                                                        fieldsMissing.add(jsonProperty.value());
                                                                    } else {
                                                                        String paramName = jsonProperty.value();
                                                                        String paramValue = value.toString();
                                                                        String originalType = itemField.getType().getName();
                                                                        originalType = originalType.replace("java.lang.", "");
                                                                        Class<?> typeData = itemField.getType();
                                                                        try {
                                                                            boolean isNegativeValue = false;
                                                                            if (typeData == Integer.class) {
                                                                                Integer valueObject = Integer.parseInt(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == Double.class) {
                                                                                Double valueObject = Double.parseDouble(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == Float.class) {
                                                                                Float valueObject = Float.parseFloat(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == Short.class) {
                                                                                Short valueObject = Short.parseShort(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == Long.class) {
                                                                                Long valueObject = Long.parseLong(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == int.class) {
                                                                                Integer valueObject = Integer.parseInt(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == double.class) {
                                                                                Double valueObject = Double.parseDouble(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == float.class) {
                                                                                Float valueObject = Float.parseFloat(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == short.class) {
                                                                                Short valueObject = Short.parseShort(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            } else if (typeData == long.class) {
                                                                                Long valueObject = Long.parseLong(paramValue);
                                                                                isNegativeValue = valueObject < 0;
                                                                            }
                                                                            if (isNegativeValue) {
                                                                                NegativeParamValidationException negativeParamValidationException = new NegativeParamValidationException();
                                                                                negativeParamValidationException.setName(paramName);
                                                                                negativeParamValidationException.setValue(paramValue);
                                                                                throw new CustomException(negativeParamValidationException);
                                                                            }
                                                                        } catch (Exception ex) {
                                                                            if (ex instanceof CustomException) {
                                                                                throw ex;
                                                                            }
                                                                            ParamValidationException paramValidationException = new ParamValidationException();
                                                                            paramValidationException.setOriginalType(originalType);
                                                                            paramValidationException.setName(paramName);
                                                                            paramValidationException.setValue(paramValue);
                                                                            throw new CustomException(paramValidationException);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (specifications != null) {
                                                                if (specifications.isSensible()) {
                                                                    if (specifications.typeOfString().equals(Specifications.TypeOfString.Base64)) {
                                                                        if (value instanceof String) {
                                                                            String valueString = (String) value;
                                                                            if (!(validator.isBase64(valueString) && valueString.length() > 100)) {
                                                                                fieldsSensiblesNotBase64.add(jsonProperty.value());
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (specifications.typeOfString().equals(Specifications.TypeOfString.Base64)) {
                                                                        if (value instanceof String) {
                                                                            if (!validator.isBase64((String) value)) {
                                                                                fieldsNotBase64.add(jsonProperty.value());
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } catch (IllegalArgumentException ex) {

                                                        }
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                throw new CustomException(ex);
                                            }
                                        }
                                    }
                                } else {
                                    RequestParam requestParam = itemMethodParameter.getAnnotation(RequestParam.class);
                                    PathVariable pathVariable = itemMethodParameter.getAnnotation(PathVariable.class);
                                    Class<?> typeData = itemMethodParameter.getType();
                                    if (requestParam != null) {
                                        String paramName = requestParam.value();
                                        if (paramName == null) {
                                            paramName = requestParam.name();
                                        } else if (paramName.isEmpty()) {
                                            paramName = requestParam.name();
                                        }
                                        if (paramName != null && !paramName.isEmpty()) {
                                            String paramValue = httpServletRequest.getParameter(paramName);
                                            String originalType = typeData.getName();
                                            originalType = originalType.replace("java.lang.", "");
                                            try {
                                                boolean isNegativeValue = false;
                                                if (typeData == Integer.class) {
                                                    Integer value = Integer.parseInt(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Double.class) {
                                                    Double value = Double.parseDouble(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Float.class) {
                                                    Float value = Float.parseFloat(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Short.class) {
                                                    Short value = Short.parseShort(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Long.class) {
                                                    Long value = Long.parseLong(paramValue);
                                                    isNegativeValue = value < 0;
                                                }
                                                if (isNegativeValue) {
                                                    NegativeParamValidationException negativeParamValidationException = new NegativeParamValidationException();
                                                    negativeParamValidationException.setName(paramName);
                                                    negativeParamValidationException.setValue(paramValue);
                                                    throw new CustomException(negativeParamValidationException);
                                                }
                                            } catch (Exception ex) {
                                                if (ex instanceof CustomException) {
                                                    throw ex;
                                                }
                                                ParamValidationException paramValidationException = new ParamValidationException();
                                                paramValidationException.setOriginalType(originalType);
                                                paramValidationException.setName(paramName);
                                                paramValidationException.setValue(paramValue);
                                                throw new CustomException(paramValidationException);
                                            }
                                        }
                                    }
                                    if (pathVariable != null) {
                                        String paramName = pathVariable.value();
                                        if (paramName == null) {
                                            paramName = pathVariable.name();
                                        } else if (paramName.isEmpty()) {
                                            paramName = pathVariable.name();
                                        }
                                        if (paramName != null && !paramName.isEmpty()) {
                                            String paramValue = pathVariables.get(paramName).toString();
                                            String originalType = typeData.getName();
                                            originalType = originalType.replace("java.lang.", "");
                                            try {
                                                boolean isNegativeValue = false;
                                                if (typeData == Integer.class) {
                                                    Integer value = Integer.parseInt(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Double.class) {
                                                    Double value = Double.parseDouble(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Float.class) {
                                                    Float value = Float.parseFloat(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Short.class) {
                                                    Short value = Short.parseShort(paramValue);
                                                    isNegativeValue = value < 0;
                                                } else if (typeData == Long.class) {
                                                    Long value = Long.parseLong(paramValue);
                                                    isNegativeValue = value < 0;
                                                }
                                                if (isNegativeValue) {
                                                    NegativePathParamValidationException negativePathParamValidationException = new NegativePathParamValidationException();
                                                    negativePathParamValidationException.setName(paramName);
                                                    negativePathParamValidationException.setValue(paramValue);
                                                    throw new CustomException(negativePathParamValidationException);
                                                }
                                            } catch (Exception ex) {
                                                if (ex instanceof CustomException) {
                                                    throw ex;
                                                }
                                                PathParamValidationException pathParamValidationException = new PathParamValidationException();
                                                pathParamValidationException.setOriginalType(originalType);
                                                pathParamValidationException.setName(paramName);
                                                pathParamValidationException.setValue(paramValue);
                                                throw new CustomException(pathParamValidationException);
                                            }
                                        }
                                    }
                                }
                            } else {
                                RequestParam requestParam = itemMethodParameter.getAnnotation(RequestParam.class);
                                Class<?> typeData = itemMethodParameter.getType();
                                if (requestParam != null) {
                                    String paramName = requestParam.value();
                                    if (paramName == null) {
                                        paramName = requestParam.name();
                                    } else if (paramName.isEmpty()) {
                                        paramName = requestParam.name();
                                    }
                                    if (paramName != null && !paramName.isEmpty()) {
                                        String paramValue = httpServletRequest.getParameter(paramName);
                                        String originalType = typeData.getName();
                                        originalType = originalType.replace("java.lang.", "");
                                        try {
                                            boolean isNegativeValue = false;
                                            if (typeData == int.class) {
                                                Integer value = Integer.parseInt(paramValue);
                                                isNegativeValue = value < 0;
                                            } else if (typeData == double.class) {
                                                Double value = Double.parseDouble(paramValue);
                                                isNegativeValue = value < 0;
                                            } else if (typeData == float.class) {
                                                Float value = Float.parseFloat(paramValue);
                                                isNegativeValue = value < 0;
                                            } else if (typeData == short.class) {
                                                Short value = Short.parseShort(paramValue);
                                                isNegativeValue = value < 0;
                                            } else if (typeData == long.class) {
                                                Long value = Long.parseLong(paramValue);
                                                isNegativeValue = value < 0;
                                            }
                                            if (isNegativeValue) {
                                                NegativeParamValidationException negativeParamValidationException = new NegativeParamValidationException();
                                                negativeParamValidationException.setName(paramName);
                                                negativeParamValidationException.setValue(paramValue);
                                                throw new CustomException(negativeParamValidationException);
                                            }
                                        } catch (Exception ex) {
                                            if (ex instanceof CustomException) {
                                                throw ex;
                                            }
                                            ParamValidationException paramValidationException = new ParamValidationException();
                                            paramValidationException.setOriginalType(originalType);
                                            paramValidationException.setName(paramName);
                                            paramValidationException.setValue(paramValue);
                                            throw new CustomException(paramValidationException);
                                        }
                                    }
                                }
                            }
                            if (!fieldsMissing.isEmpty()) {
                                RequiredDataException requiredDataException = new RequiredDataException("Fields are required");
                                requiredDataException.setFields(fieldsMissing);
                                throw new CustomException(requiredDataException);
                            } else if (!fieldsNotBase64.isEmpty()) {
                                Base64Exception base64Exception = new Base64Exception("Fields format Base64 is required");
                                base64Exception.setFields(fieldsNotBase64);
                                throw new CustomException(base64Exception);
                            } else if (!fieldsSensiblesNotBase64.isEmpty()) {
                                SensibleDataBase64Exception sensibleDataBase64Exception = new SensibleDataBase64Exception("Fields sensible data in format Base64 is required");
                                sensibleDataBase64Exception.setFields(fieldsSensiblesNotBase64);
                                throw new CustomException(sensibleDataBase64Exception);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                throw new CustomException(ex);
            }
        }
    }

    /**
     *
     * @return @throws IOException
     */
    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new ServletInputStreamRequestWrapper(body.getBytes());
    }

    /**
     *
     * @return @throws IOException
     */
    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }
}
