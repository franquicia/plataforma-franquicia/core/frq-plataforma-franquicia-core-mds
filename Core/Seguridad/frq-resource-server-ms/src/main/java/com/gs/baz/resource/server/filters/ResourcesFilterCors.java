/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.resource.server.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ResourcesFilterCors implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (!httpServletRequest.getRequestURI().contains("core/oauth/token")) {
            if (!httpServletRequest.getRequestURI().contains("websocket")) {
                if (!httpServletResponse.containsHeader("Access-Control-Allow-Origin")) {
                    httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
                }
                if (!httpServletResponse.containsHeader("Access-Control-Allow-Headers")) {
                    httpServletResponse.addHeader("Access-Control-Allow-Headers", "*");
                }
                if (!httpServletResponse.containsHeader("Access-Control-Allow-Methods")) {
                    httpServletResponse.addHeader("Access-Control-Allow-Methods", "*");
                }
                if (!httpServletResponse.containsHeader("Access-Control-Expose-Headers")) {
                    httpServletResponse.addHeader("Access-Control-Expose-Headers", "*");
                }
                if (httpServletRequest.getMethod().equals("OPTIONS")) {
                    httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
                    return;
                }
            }
        }
        chain.doFilter(httpServletRequest, httpServletResponse);
    }

    @Override
    public void destroy() {
    }

}
