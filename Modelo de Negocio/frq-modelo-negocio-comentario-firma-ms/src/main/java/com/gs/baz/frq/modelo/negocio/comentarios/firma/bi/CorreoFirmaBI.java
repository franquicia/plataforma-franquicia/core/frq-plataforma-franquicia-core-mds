/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.bi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dao.ComentarioFirmaDAOImpl;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.FirmaBase;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.FolioBase;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ResumenDocumento;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.util.UtilMail;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author carlos
 */
public class CorreoFirmaBI {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ComentarioFirmaDAOImpl ComentarioFirmaDAOImpl;

    public boolean enviaCorreo(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio, Integer idFirma, String Comentario, Integer idEstadoFirma) throws Exception {
        FirmaBase firma = ComentarioFirmaDAOImpl.selectFirmaRow(new Long("" + idFolio), new Long("" + idFirma));

        System.out.println(" Resultado Firma: " + firma.toString());

        FolioBase folio = ComentarioFirmaDAOImpl.selectFolioRow(new Long("" + idFolio));

        System.out.println("Folio: " + folio.toString());

        Empleado ceador = ComentarioFirmaDAOImpl.selectEmpleadoRows("" + folio.getNumeroEmpleadoCreador());
        Empleado firmante = ComentarioFirmaDAOImpl.selectEmpleadoRows("" + firma.getNumeroEmpleado());

        System.out.println("Creador: " + ceador.toString());

        ResumenDocumento documento = ComentarioFirmaDAOImpl.selectResumenDocumento(new Long("" + idFolio));

        System.out.println("Documento: " + documento.toString());

        String resolucion = "";

        if (idEstadoFirma == 2) {
            resolucion = "AUTORIZADO";
        } else if (idEstadoFirma == 3) {
            resolucion = "RECHAZADO";
        } else if (idEstadoFirma == 4) {
            resolucion = "ENVIADO A MODIFICACIÓN";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = sdf.format(new Date());
        File origen = new File("/franquicia/plataforma_franquicia/correo/plantilla/mailAutorizacionMoeloNegocio.html");
        InputStream in = new FileInputStream(origen);

        String uriContexto;
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-app/modelo-negocio/documentos/estados";
        if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
            uriContexto = "http://10.53.33.83:82/plataforma-franquicia-app/modelo-negocio/publicados";
        } else {
            uriContexto = basePath;
        }
        String strCorreoOwner = inputStreamAsString(in);
        strCorreoOwner = strCorreoOwner.replace("@uriContexto", uriContexto);
        strCorreoOwner = strCorreoOwner.replace("@Nombre", ceador.getNombre());
        strCorreoOwner = strCorreoOwner.replace("@Documento", documento.getDocumento().getTitulo());
        strCorreoOwner = strCorreoOwner.replace("@folio", "" + idFolio);
        strCorreoOwner = strCorreoOwner.replace("@Fecha", "" + fecha);
        strCorreoOwner = strCorreoOwner.replace("@Accion", "" + resolucion);
        strCorreoOwner = strCorreoOwner.replace("@Comentarios", "" + Comentario);
        strCorreoOwner = strCorreoOwner.replace("@firmante", "" + firmante.getNombre());
        String destinatario = "";
        if (ceador.getCorreo() != null) {
            if (ceador.getCorreo().equals("")) {
                destinatario = "caaguilar@elektra.com.mx";
            } else {
                destinatario = ceador.getCorreo();
            }
        } else {
            destinatario = "caaguilar@elektra.com.mx";
        }
        UtilMail.enviaCorreo(destinatario, null, "!" + resolucion + "documento con folio " + idFolio + "!", strCorreoOwner, null);
        UtilMail.enviaCorreo("caaguilar@elektra.com.mx", null, "!" + resolucion + "documento con folio " + idFolio + "!", strCorreoOwner, null);
        System.out.println("[ Mail enviado ]");
        return true;
    }

    public FirmaBase consultaFirma(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio, Integer idFirma) throws Exception {
        FirmaBase listaComentarios = null;
        System.out.println("Entro a firma");
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folio-firmas/v1/" + idFolio + "/firmas/" + idFirma;
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.GET, httpEntityUsuario, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);

            if (objectNode.get("status_code").asInt() == HttpStatus.OK.value()) {
                //System.out.println("Se ejecuto el proceso de comentarios 1");
                if (objectNode.get("data") != null) {
                    //System.out.println("Se ejecuto el proceso de comentarios 2");
                    listaComentarios = objectMapper.readValue(objectNode.get("data").toString(), FirmaBase.class);
                    //System.out.println("Resultado: " + listaComentarios.toString());
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("comentarios firma idFolio " + idFolio + " response code error"));
            }

        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        } catch (IOException ex) {
            throw new Exception(ex);
        }
        return listaComentarios;
    }

    public FolioBase consultaFolio(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio) throws Exception {
        FolioBase folio = null;
        System.out.println("Entro a buscar folio");
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folios/v1/" + idFolio;
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.GET, httpEntityUsuario, String.class);
            System.out.println("Se ejecuto el proceso de comentarios");
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);
            if (objectNode.get("status_code").asInt() == HttpStatus.OK.value()) {
                if (objectNode.get("data") != null) {
                    System.out.println("Llego a sacar parametros");
                    folio = objectMapper.readValue(objectNode.get("data").toString(), FolioBase.class);

                    System.out.println("Resultado Empleado: " + folio.toString());
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("Folio  idFolio " + folio + " response code error"));
            }

        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        } catch (IOException ex) {
            throw new Exception(ex);
        }
        return folio;
    }

    public Empleado consultaEmpleado(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idUsuario) throws Exception {
        List<Empleado> empleados = null;
        Empleado empleado = null;
        System.out.println("Entro a buscar empleado");
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/usuarios-negocio/v1/busquedas/" + idUsuario;
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.GET, httpEntityUsuario, String.class);
            System.out.println("Se ejecuto el proceso de comentarios");
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);
            if (objectNode.get("status_code").asInt() == HttpStatus.OK.value()) {
                System.out.println("Se ejecuto el proceso de comentarios 1");
                if (objectNode.get("data") != null) {
                    empleados = objectMapper.readValue(objectNode.get("data").get("usuarios").toString(), objectMapper.getTypeFactory().constructCollectionLikeType(List.class, Empleado.class));
                    empleado = empleados.stream().findFirst().orElse(null);
                    System.out.println("Resultado Empleado: " + empleado.toString());
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("comentarios firma idFolio " + idUsuario + " response code error"));
            }

        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        } catch (IOException ex) {
            throw new Exception(ex);
        }
        return empleado;
    }

    public ResumenDocumento consultaDocumento(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio) throws Exception {
        ResumenDocumento folio = null;
        System.out.println("Entro a buscar documento");
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folios/v1/" + idFolio + "/resumen";
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(httpHeaders);
            ResponseEntity<String> out = restTemplate.exchange(basePath, HttpMethod.GET, httpEntityUsuario, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.readValue(out.getBody(), ObjectNode.class);
            if (objectNode.get("status_code").asInt() == HttpStatus.OK.value()) {
                if (objectNode.get("data") != null) {
                    System.out.println("Llego a sacar parametros");
                    folio = objectMapper.readValue(objectNode.get("data").toString(), ResumenDocumento.class);

                    System.out.println("Resultado Empleado: " + folio.toString());
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("ResumenDocumento  idFolio " + folio + " response code error"));
            }

        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        } catch (IOException ex) {
            throw new Exception(ex);
        }
        return folio;
    }

    public static String inputStreamAsString(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public boolean enviaCorreoFallo(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio, Integer idFirma, String Comentario) throws Exception {
        FirmaBase firma = consultaFirma(httpServletRequest, httpHeaders, idFolio, idFirma);

        System.out.println(" Resultado Firma: " + firma.toString());

        FolioBase folio = consultaFolio(httpServletRequest, httpHeaders, idFolio);

        System.out.println("Folio: " + folio.toString());

        Empleado ceador = consultaEmpleado(httpServletRequest, httpHeaders, folio.getNumeroEmpleadoCreador());

        System.out.println("Creador: " + ceador.toString());

        ResumenDocumento documento = consultaDocumento(httpServletRequest, httpHeaders, idFolio);

        System.out.println("Documento: " + documento.toString());

        String resolucion = "";

        if (firma.getIdEstadoFirma() == 2) {
            resolucion = "AUTORIZADO";
        } else if (firma.getIdEstadoFirma() == 3) {
            resolucion = "RECHAZADO";
        } else if (firma.getIdEstadoFirma() == 4) {
            resolucion = "ENVIADO A MODIFICACIÓN";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = sdf.format(new Date());
        File origen = new File("/franquicia/plataforma_franquicia/correo/plantilla/mailAutorizacionMoeloNegocio.html");
        InputStream in = new FileInputStream(origen);

        String uriContexto;
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-app/modelo-negocio/documentos/estados";
        if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
            uriContexto = "http://10.53.33.83:82/plataforma-franquicia-app/modelo-negocio/publicados";
        } else {
            uriContexto = basePath;
        }
        String strCorreoOwner = inputStreamAsString(in);
        strCorreoOwner = strCorreoOwner.replace("@uriContexto", uriContexto);
        strCorreoOwner = strCorreoOwner.replace("@Nombre", ceador.getNombre());
        strCorreoOwner = strCorreoOwner.replace("@Documento", documento.getDocumento().getTitulo());
        strCorreoOwner = strCorreoOwner.replace("@folio", "" + idFolio);
        strCorreoOwner = strCorreoOwner.replace("@Fecha", "" + fecha);
        strCorreoOwner = strCorreoOwner.replace("@Accion", "" + resolucion);
        strCorreoOwner = strCorreoOwner.replace("@Comentarios", "" + Comentario);
        String destinatario = "";
        if (ceador.getCorreo() != null) {
            if (ceador.getCorreo().equals("")) {
                destinatario = "caaguilar@elektra.com.mx";
            } else {
                destinatario = ceador.getCorreo();
            }
        } else {
            destinatario = "caaguilar@elektra.com.mx";
        }
        UtilMail.enviaCorreo(destinatario, null, "¡El documento con folio " + idFolio + "  fue " + resolucion + "!", strCorreoOwner, null);
        UtilMail.enviaCorreo("caaguilar@elektra.com.mx", null, "¡El documento con folio " + idFolio + "  fue " + resolucion + "!", strCorreoOwner, null);
        System.out.println("[ Mail enviado ]");
        return true;
    }

}
