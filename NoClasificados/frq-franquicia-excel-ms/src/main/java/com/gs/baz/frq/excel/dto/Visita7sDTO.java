/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class Visita7sDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_visita")
    private Integer idVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_quien_realizo")
    private String nombreQuienRealizo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion_puesto")
    private String descripcionPuesto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_pais")
    private String nombrePais;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_ceco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion_canal")
    private String descripcionCanal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio_disciplina")
    private String negocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "precalificacion")
    private Integer precalificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion_final")
    private Integer calificacionFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_si")
    private Integer conteoSi;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_no")
    private Integer conteoNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteo_Imperdonable")
    private Integer conteoImperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calif_esperada")
    private Integer calif_esperada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calif_lineal")
    private Integer calif_lineal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer id_negocio;

    public Integer getCalif_esperada() {
        return calif_esperada;
    }

    public void setCalif_esperada(BigDecimal calif_esperada) {
        this.calif_esperada = (calif_esperada == null ? null : calif_esperada.intValue());
    }

    public Integer getCalif_lineal() {
        return calif_lineal;
    }

    public void setCalif_lineal(BigDecimal calif_lineal) {
        this.calif_lineal = (calif_lineal == null ? null : calif_lineal.intValue());
    }

    public Integer getId_negocio() {
        return id_negocio;
    }

    public void setId_negocio(BigDecimal id_negocio) {
        this.id_negocio = (id_negocio == null ? null : id_negocio.intValue());
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(BigDecimal idVisita) {
        this.idVisita = (idVisita == null ? null : idVisita.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreQuienRealizo() {
        return nombreQuienRealizo;
    }

    public void setNombreQuienRealizo(String nombreQuienRealizo) {
        this.nombreQuienRealizo = nombreQuienRealizo;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public String getDescripcionPuesto() {
        return descripcionPuesto;
    }

    public void setDescripcionPuesto(String descripcionPuesto) {
        this.descripcionPuesto = descripcionPuesto;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getDescripcionCanal() {
        return descripcionCanal;
    }

    public void setDescripcionCanal(String descripcionCanal) {
        this.descripcionCanal = descripcionCanal;
    }

    public String getNegocioDisciplina() {
        return negocioDisciplina;
    }

    public void setNegocioDisciplina(String negocioDisciplina) {
        this.negocioDisciplina = negocioDisciplina;
    }

    public Integer getPrecalificacion() {
        return precalificacion;
    }

    public void setPrecalificacion(BigDecimal precalificacion) {
        this.precalificacion = (precalificacion == null ? null : precalificacion.intValue());
    }

    public Integer getCalificacionFinal() {
        return calificacionFinal;
    }

    public void setCalificacionFinal(BigDecimal calificacionFinal) {
        this.calificacionFinal = (calificacionFinal == null ? null : calificacionFinal.intValue());
    }

    public Integer getConteoSi() {
        return conteoSi;
    }

    public void setConteoSi(BigDecimal conteoSi) {
        this.conteoSi = (conteoSi == null ? null : conteoSi.intValue());
    }

    public Integer getConteoNo() {
        return conteoNo;
    }

    public void setConteoNo(BigDecimal conteoNo) {
        this.conteoNo = (conteoNo == null ? null : conteoNo.intValue());
    }

    public Integer getConteoImperdonable() {
        return conteoImperdonable;
    }

    public void setConteoImperdonable(BigDecimal conteoImperdonable) {
        this.conteoImperdonable = (conteoImperdonable == null ? null : conteoImperdonable.intValue());
    }

    @Override
    public String toString() {
        return "Visita7sDTO{" + "idVisita=" + idVisita + ", fecha=" + fecha + ", nombreQuienRealizo=" + nombreQuienRealizo + ", idUsuario=" + idUsuario + ", descripcionPuesto=" + descripcionPuesto + ", nombrePais=" + nombrePais + ", territorio=" + territorio + ", zona=" + zona + ", region=" + region + ", idCeco=" + idCeco + ", nombreCeco=" + nombreCeco + ", descripcionCanal=" + descripcionCanal + ", negocioDisciplina=" + negocioDisciplina + ", precalificacion=" + precalificacion + ", calificacionFinal=" + calificacionFinal + ", conteoSi=" + conteoSi + ", conteoNo=" + conteoNo + ", conteoImperdonable=" + conteoImperdonable + '}';
    }

}
