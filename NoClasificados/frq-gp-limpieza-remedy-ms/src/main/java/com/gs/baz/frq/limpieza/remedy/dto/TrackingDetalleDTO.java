/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Detalle del seguimiento del incidente", value = "TrackingDetalle")
public class TrackingDetalleDTO {

    @JsonProperty(value = "estadoIncidente")
    @ApiModelProperty(notes = "Estado del seguimiento del incidente",
            example = "Por Atender", position = -1)
    private String estado;

    @JsonProperty(value = "fechaDetalle")
    @ApiModelProperty(notes = "Fecha del seguimiento del incidente",
            example = "0001-01-01 00:00:00")
    private String fecha;

    @JsonProperty(value = "justificacionEstado")
    @ApiModelProperty(notes = "La justificación del estado del seguimiento",
            example = "Debido a que se acaba de levantar el incidente")
    private String motivoEstado;

    public TrackingDetalleDTO() {
        super();

    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
