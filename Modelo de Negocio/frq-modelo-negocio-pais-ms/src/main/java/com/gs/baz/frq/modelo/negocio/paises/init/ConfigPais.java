package com.gs.baz.frq.modelo.negocio.paises.init;

import com.gs.baz.frq.modelo.negocio.paises.dao.PaisDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.paises")
public class ConfigPais {

    private final Logger logger = LogManager.getLogger();

    public ConfigPais() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public PaisDAOImpl paisDAOImpl() {
        return new PaisDAOImpl();
    }

}
