package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.IncidenciasDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class IncidenciasRowMapper implements RowMapper<IncidenciasDTO> {

    @Override
    public IncidenciasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        IncidenciasDTO incidencia = new IncidenciasDTO();

        incidencia.setIdTipo(rs.getInt("FIIDTIPO"));
        incidencia.setServicio(rs.getString("FCTIPOSERVICIO"));
        incidencia.setUbicacion(rs.getString("FCUBICACION"));
        incidencia.setIncidencia(rs.getString("FCINCIDENCIA"));
        incidencia.setPlantilla(rs.getString("FCPLANTILLA"));
        incidencia.setStatus(rs.getString("FCSTATUS"));

        return incidencia;
    }

}
