/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dao;

import com.gs.baz.frq.api.portal.cautivo.dto.AppDTO;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author MADA
 */
public class ClienteDAOImpl extends DefaultDAO {

    private String schema;
    private DefaultJdbcCall jdbcInsertCliente;
    private DefaultJdbcCall jdbcSelectCliente;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertCliente = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertCliente.withSchemaName(schema);
        jdbcInsertCliente.withCatalogName("PAADMUSRBDIGIT");
        jdbcInsertCliente.withProcedureName("SPINSUSRBDIGITAL");

    }

    public boolean insertRow(AppDTO cliente) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCUSERNAME", cliente.getUsuario());
        mapSqlParameterSource.addValue("PA_FDINICIO_SESION", cliente.getStartTime());
        mapSqlParameterSource.addValue("PA_FCSUCURSAL", cliente.getTienda());
        mapSqlParameterSource.addValue("PA_FCMAC", cliente.getMac());
        //mapSqlParameterSource.addValue("PA_FIUSUARIO_ID", cliente.getClientId());

        Map<String, Object> out = jdbcInsertCliente.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

        return success;

    }

}
