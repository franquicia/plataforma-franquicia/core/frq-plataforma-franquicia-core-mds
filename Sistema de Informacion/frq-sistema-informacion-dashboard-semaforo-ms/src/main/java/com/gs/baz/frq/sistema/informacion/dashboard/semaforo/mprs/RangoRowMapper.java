/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.RangoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RangoRowMapper implements RowMapper<RangoBase> {

    @Override
    public RangoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        RangoBase rangoBase = new RangoBase();
        rangoBase.setColor(rs.getString("FCDETALLE"));
        rangoBase.setLimiteInferior(rs.getInt("FILIMITE_INF"));
        rangoBase.setLimiteSuperior(rs.getInt("FILIMITE_SUP"));
        rangoBase.setDescripcion(rs.getString("FCVALOR_RANGO"));
        return rangoBase;
    }
}
