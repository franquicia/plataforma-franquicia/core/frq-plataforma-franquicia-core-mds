package com.gs.baz.frq.limpieza.remedy.dto;

public class IncidenteCuadrillaDTO {

    private Integer idIncidenteCuadrilla;
    private Integer idCuadrilla;
    private Integer idProveedor;
    private String incidente;
    private Integer zona;
    private String liderCuadrilla;
    private Integer status;

    public IncidenteCuadrillaDTO() {
        super();

    }

    public Integer getIdCuadrilla() {
        return idCuadrilla;
    }

    public void setIdCuadrilla(Integer idCuadrilla) {
        this.idCuadrilla = idCuadrilla;
    }

    public Integer getIdIncidenteCuadrilla() {
        return idIncidenteCuadrilla;
    }

    public void setIdIncidenteCuadrilla(Integer idIncidenteCuadrilla) {
        this.idIncidenteCuadrilla = idIncidenteCuadrilla;
    }

    public String getIncidente() {
        return incidente;
    }

    public void setIncidente(String incidente) {
        this.incidente = incidente;
    }

    public String getLiderCuadrilla() {
        return liderCuadrilla;
    }

    public void setLiderCuadrilla(String liderCuadrilla) {
        this.liderCuadrilla = liderCuadrilla;
    }

    public Integer getZona() {
        return zona;
    }

    public void setZona(Integer zona) {
        this.zona = zona;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public String toString() {
        return "IncidenteCuadrillaDTO [idIncidenteCuadrilla="
                + idIncidenteCuadrilla + ", idCuadrilla=" + idCuadrilla
                + ", idProveedor=" + idProveedor
                + ", incidente=" + incidente + ", zona="
                + zona + ", liderCuadrilla="
                + liderCuadrilla + ", status=" + status + "]";
    }

}
