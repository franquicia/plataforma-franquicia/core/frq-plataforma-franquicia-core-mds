/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Ruta", value = "Ruta")
public class RutaCompuesta {

    @JsonProperty(value = "idRuta")
    @ApiModelProperty(notes = "Identificador del Ruta", example = "1", position = -1)
    private Integer idRuta;

    @JsonProperty(value = "codificacion")
    @ApiModelProperty(notes = "codificacion de la Ruta")
    private Codificacion codificacion;

    @JsonProperty(value = "pais")
    @ApiModelProperty(notes = "Pais de la Ruta")
    private Pais pais;

    @JsonProperty(value = "modelo")
    @ApiModelProperty(notes = "Modelo de la Ruta")
    private Modelo modelo;

    @JsonProperty(value = "unidadNegocio")
    @ApiModelProperty(notes = "Unidad de negocio de la Ruta")
    private UnidadNegocio unidadNegocio;

    @JsonProperty(value = "modelosNegocio")
    @ApiModelProperty(notes = "Modelos negocio la Ruta")
    private List<ModeloNegocio> modelosNegocio;

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    public Codificacion getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(Codificacion codificacion) {
        this.codificacion = codificacion;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public UnidadNegocio getUnidadNegocio() {
        return unidadNegocio;
    }

    public void setUnidadNegocio(UnidadNegocio unidadNegocio) {
        this.unidadNegocio = unidadNegocio;
    }

    public List<ModeloNegocio> getModelosNegocio() {
        return modelosNegocio;
    }

    public void setModelosNegocio(List<ModeloNegocio> modelosNegocio) {
        this.modelosNegocio = modelosNegocio;
    }

    @Override
    public String toString() {
        return "RutaCompuesta{" + "idRuta=" + idRuta + ", codificacion=" + codificacion + ", pais=" + pais + ", modelo=" + modelo + ", unidadNegocio=" + unidadNegocio + ", modelosNegocio=" + modelosNegocio + '}';
    }

}
