/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.flujo.perfil.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlujoPerfilDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_flujo_perfil")
    private Integer idFlujoPerfil;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_flujo")
    private Integer idFlujo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "flujo")
    private FlujoDTO flujoDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_perfil")
    private Integer idPerfil;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private Integer estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public FlujoPerfilDTO() {
    }

    public Integer getIdFlujoPerfil() {
        return idFlujoPerfil;
    }

    public void setIdFlujoPerfil(BigDecimal idFlujoPerfil) {
        this.idFlujoPerfil = (idFlujoPerfil == null ? null : idFlujoPerfil.intValue());
    }

    public Integer getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(BigDecimal idFlujo) {
        this.idFlujo = (idFlujo == null ? null : idFlujo.intValue());
    }

    public FlujoDTO getFlujoDTO() {
        return flujoDTO;
    }

    public void setFlujoDTO(FlujoDTO flujoDTO) {
        this.flujoDTO = flujoDTO;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(BigDecimal idPerfil) {
        this.idPerfil = (idPerfil == null ? null : idPerfil.intValue());
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(BigDecimal estatus) {
        this.estatus = (estatus == null ? null : estatus.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "FlujoPerfilDTO{" + "idFlujoPerfil=" + idFlujoPerfil + ", idFlujo=" + idFlujo + ", flujoDTO=" + flujoDTO + ", idPerfil=" + idPerfil + ", estatus=" + estatus + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
