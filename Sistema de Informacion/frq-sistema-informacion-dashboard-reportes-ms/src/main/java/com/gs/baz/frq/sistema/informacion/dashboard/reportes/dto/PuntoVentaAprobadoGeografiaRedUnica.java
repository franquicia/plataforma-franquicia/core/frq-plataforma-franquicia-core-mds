/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class PuntoVentaAprobadoGeografiaRedUnica {

    @JsonProperty(value = "meta")
    @ApiModelProperty(notes = "Meta", example = "90")
    private Integer meta;

    @JsonProperty(value = "puntosVentaAprobadosGeografia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Puntos de venta aprobados por geografia de red unica")
    private List<PuntoVentaAprobadoGeografia> puntosVentaAprobadosGeografia;

    public Integer getMeta() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta = meta;
    }

    public List<PuntoVentaAprobadoGeografia> getPuntosVentaAprobadosGeografia() {
        return puntosVentaAprobadosGeografia;
    }

    public void setPuntosVentaAprobadosGeografia(List<PuntoVentaAprobadoGeografia> puntosVentaAprobadosGeografia) {
        this.puntosVentaAprobadosGeografia = puntosVentaAprobadosGeografia;
    }

    @Override
    public String toString() {
        return "PuntoVentaAprobadoGeografiaRedUnica{" + "meta=" + meta + ", puntosVentaAprobadosGeografia=" + puntosVentaAprobadosGeografia + '}';
    }

}
