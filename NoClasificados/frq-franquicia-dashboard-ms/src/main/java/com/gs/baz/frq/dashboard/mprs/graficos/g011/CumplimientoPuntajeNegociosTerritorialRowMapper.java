package com.gs.baz.frq.dashboard.mprs.graficos.g011;

import com.gs.baz.frq.dashboard.dto.graficos.g011.CumplimientoPuntajeNegociosTerritorialDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CumplimientoPuntajeNegociosTerritorialRowMapper implements RowMapper<CumplimientoPuntajeNegociosTerritorialDTO> {

    private CumplimientoPuntajeNegociosTerritorialDTO cumplimientoPuntajeNegociosTerritorialDTO;

    @Override
    public CumplimientoPuntajeNegociosTerritorialDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        cumplimientoPuntajeNegociosTerritorialDTO = new CumplimientoPuntajeNegociosTerritorialDTO();
        cumplimientoPuntajeNegociosTerritorialDTO.setIdCeco(rs.getString("CECO"));
        cumplimientoPuntajeNegociosTerritorialDTO.setDescripcionCeco(rs.getString("DESCRIPCIONCECO"));
        cumplimientoPuntajeNegociosTerritorialDTO.setPuntoContacto((BigDecimal) rs.getObject("PUNTOS_CONTACTO"));
        cumplimientoPuntajeNegociosTerritorialDTO.setCumple((BigDecimal) rs.getObject("CUMPLEN"));
        cumplimientoPuntajeNegociosTerritorialDTO.setPorcentajeCumplimiento((BigDecimal) rs.getObject("PORCENTAJECUMP"));
        cumplimientoPuntajeNegociosTerritorialDTO.setPuntaje((BigDecimal) rs.getObject("PUNTAJE"));
        cumplimientoPuntajeNegociosTerritorialDTO.setIdRango((BigDecimal) rs.getObject("RANGO"));
        return cumplimientoPuntajeNegociosTerritorialDTO;
    }
}
