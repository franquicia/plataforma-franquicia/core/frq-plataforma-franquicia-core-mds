/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author MADA
 */
//NegocioFranquicia
@JsonIgnoreProperties(ignoreUnknown = true)
public class NegocioFranquiciaDTO {

    @JsonProperty(value = "id_negocio")
    protected Integer idNegocio;

    @JsonProperty(value = "ids_categorias")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Integer> idsCategorias;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public List<Integer> getIdsCategorias() {
        return idsCategorias;
    }

    public void setIdsCategorias(List<Integer> idsCategorias) {
        this.idsCategorias = idsCategorias;
    }

}
