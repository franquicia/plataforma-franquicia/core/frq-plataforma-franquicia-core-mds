/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folio.etiquetas.mprs;

import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto.FolioEtiqueta;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FoliosEtiquetaRowMapper implements RowMapper<FolioEtiqueta> {

    private FolioEtiqueta rutaNivel;

    @Override
    public FolioEtiqueta mapRow(ResultSet rs, int rowNum) throws SQLException {
        rutaNivel = new FolioEtiqueta();
        rutaNivel.setIdFolio(rs.getInt("FIIDFOLIO"));
        rutaNivel.setIdEtiqueta(rs.getInt("FIIDETIQUETA"));
        return rutaNivel;
    }
}
