package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Detalle del Incidente de Limpieza", value = "IncidenteLimpieza")
public class IncidenteLimpiezaDTO {

    @JsonProperty("idCuadrilla")
    @ApiModelProperty(notes = "Clave de la cuadrilla", example = "2", position = -3)
    private Integer idCuadrilla;

    @JsonProperty("idProveedor")
    @ApiModelProperty(notes = "Clave del proveedor", example = "80800815", position = -2)
    private String idProveedor;

    @JsonProperty("idIncidente")
    @ApiModelProperty(notes = "Clave de incidente", example = "13857496", position = -1)
    private Integer idIncidencia;

    @JsonProperty("aplazamientoEstatus")
    @ApiModelProperty(notes = "Bandera Aplazamiento", example = "1")
    private Integer banderaAplazamiento;

    @JsonProperty("aprobacionEstatus")
    @ApiModelProperty(notes = "Banderas Aprobacion", example = "1")
    private Integer banderas;

    @JsonProperty("autorizacionCliente")
    @ApiModelProperty(notes = "Autorización del cliente", example = "1")
    private String autorizacionCliente;

    @JsonProperty("autorizacionPipa")
    @ApiModelProperty(notes = "Autorización Pipa", example = "AUTORIZADO")
    private String autorizacionPipa;

    @JsonProperty("autorizacionProveedor")
    @ApiModelProperty(notes = "Autorización del proveedor", example = "AUTORIZADO")
    private String autorizacionProveedor;

    @JsonProperty("calificacionServicio")
    @ApiModelProperty(notes = "Evaluación del cliente", example = "0")
    private String evaluacionCliente;

    @JsonProperty("centroCostos")
    @ApiModelProperty(notes = "Centro de costos. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "237425")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String centroCostos;

    @JsonProperty("clienteAvisado")
    @ApiModelProperty(notes = "Nombre del cliente avisado. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "Juan Pérez")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String clienteAvisado;

    @JsonProperty("correoLider")
    @ApiModelProperty(notes = "Correo del líder de la cuadrilla."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "Alejandro.morales@elektraa.com")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String correo;

    @JsonProperty("costoPipa")
    @ApiModelProperty(notes = "Costo Pipa. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "2500.00")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String costoPipa;

    @JsonProperty("declinarProveedor")
    @ApiModelProperty(notes = "Indicación de declinado por el proveedor", example = "DECLINADO")
    private String declinarProveedor;

    @JsonProperty("diasAplazammiento")
    @ApiModelProperty(notes = "Días de aplazamiento", example = "25")
    private String diasAplazammiento;

    @JsonProperty("estadoIncidente")
    @ApiModelProperty(notes = "Estado del Incidente", example = "ATENDIDO")
    private String estado;

    @JsonProperty("falla")
    @ApiModelProperty(notes = "Falla", example = "Dispensadores")
    private String falla;

    @JsonProperty("fechaAtencion")
    @ApiModelProperty(notes = "Fecha de atención", example = "2020-11-24 18:49:50")
    private String fechaAtencion;

    @JsonProperty("fechaAtencionGarantia")
    @ApiModelProperty(notes = "Fecha de garantía de la atención", example = "2020-11-24 18:49:50")
    private String fechaAtencionGarantia;

    @JsonProperty("fechaAutorizaProveedor")
    @ApiModelProperty(notes = "Fecha de autorización del Proveedor", example = "2020-11-24 18:49:50")
    private String fechaAutorizaProveedor;

    @JsonProperty("fechaCancelacion")
    @ApiModelProperty(notes = "Fecha de cancelación", example = "2020-11-24 18:49:50")
    private String fechaCancelacion;

    @JsonProperty("fechaCierre")
    @ApiModelProperty(notes = "Fecha del cierre", example = "2020-11-24 18:49:50")
    private String fechaCierre;

    @JsonProperty("fechaCierreAdministrativo")
    @ApiModelProperty(notes = "Fecha del cierre administrativo", example = "2020-11-24 18:49:50")
    private String fechaCierreAdministrativo;

    @JsonProperty("fechaCierreGarantia")
    @ApiModelProperty(notes = "Fecha de garantía de cierre", example = "2020-11-24 18:49:50")
    private String fechaCierreGarantia;

    @JsonProperty("fechaCierreTecnico")
    @ApiModelProperty(notes = "Fecha del cierre técnico", example = "2020-11-24 18:49:50")
    private String fechaCierreTecnico;

    @JsonProperty("fechaCierreTecnicoDos")
    @ApiModelProperty(notes = "Fecha del cierre técnico dos", example = "2020-11-24 18:49:50")
    private String fechaCierreTecnicoDos;

    @JsonProperty("fechaCreacion")
    @ApiModelProperty(notes = "Fecha de creación", example = "2020-11-24 18:49:50")
    private String fechaCreacion;

    @JsonProperty("fechaInicioAtencion")
    @ApiModelProperty(notes = "Fecha de comienzo de la atención", example = "2020-11-24 18:49:50")
    private String fechaInicioAtencion;

    @JsonProperty("fechaModificacion")
    @ApiModelProperty(notes = "Fecha de última modificación", example = "2020-11-24 18:49:50")
    private String fechaModificacion;

    @JsonProperty("fechaProgramada")
    @ApiModelProperty(notes = "Fecha programada del cierre", example = "2020-11-24 18:49:50")
    private String fechaEstimada;

    @JsonProperty("fechaRechazoCliente")
    @ApiModelProperty(notes = "Fecha del rechazo del cliente", example = "2020-11-24 18:49:50")
    private String rechazoCliente;

    @JsonProperty("fechaRechazoProveedor")
    @ApiModelProperty(notes = "Fecha de rechazo del Proveedor", example = "2020-11-24 18:49:50")
    private String fechaRechazoProveedor;

    @JsonProperty("fechaUltimoIncidente")
    @ApiModelProperty(notes = "Fecha del último Incidente", example = "2020-11-24 18:49:50")
    private String fechaUltimoIncidente;

    @JsonProperty("incidenciaDetalle")
    @ApiModelProperty(notes = "Especificación de la falla", example = "Insumos")
    private String incidencia;

    @JsonProperty("incidenteAnterior")
    @ApiModelProperty(notes = "Incidente anterior", example = "13748598")
    private String incidenteAnterior;

    @JsonProperty("latitudFin")
    @ApiModelProperty(notes = "Latitud Fin", example = "19.298289")
    private String latitudFin;

    @JsonProperty("latitudInicio")
    @ApiModelProperty(notes = "Latitud de inicio", example = "19.298289")
    private String latitudInicio;

    @JsonProperty("longitudFin")
    @ApiModelProperty(notes = "Longitud Fin", example = "-99.152034")
    private String longitudFin;

    @JsonProperty("longitudInicio")
    @ApiModelProperty(notes = "Longitud de inicio", example = "-99.152034")
    private String longitudInicio;

    @JsonProperty("mensajeParaProveedor")
    @ApiModelProperty(notes = "Mensaje para el proveedor", example = "Se repara el sonido del aire acondicionado")
    private String mensajeProveedor;

    @JsonProperty("menuProveedor")
    @ApiModelProperty(notes = "Indetificador del proveedor conformado por el número de proveedor y"
            + " el nombre del proveedor, se utiliza para el llenado de un menú desplegable",
            example = "80800815 Lava Tap")
    private String menu;

    @JsonProperty("montoAutorizadoEstatus")
    @ApiModelProperty(notes = "Bandera del Monto Autorizado", example = "1")
    private Integer banderaMontoAutorizado;

    @JsonProperty("montoEstimado")
    @ApiModelProperty(notes = "Monto Estimado. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "2000.00")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String montoEstimado;

    @JsonProperty("montoEstimadoAutorizado")
    @ApiModelProperty(notes = "Monto Estimado Autorizado."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "2500.00")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String montoEstimadoAutorizado;

    @JsonProperty("montoReal")
    @ApiModelProperty(notes = "Monto Real. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "2,500.00")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String montoReal;

    @JsonProperty("motivoEstado")
    @ApiModelProperty(notes = "Motivo del Estado", example = "No cierra")
    private String motivoEstado;

    @JsonProperty("nombreCliente")
    @ApiModelProperty(notes = "Nombre del cliente."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "CARLOS ANTONIO ESCOBAR")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String nombreCliente;

    @JsonProperty("nombreCoordinador")
    @ApiModelProperty(notes = "Nombre del Coordinador. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "LEONARDO DANIEL ")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String nombreCoordinador;

    @JsonProperty("nombreCorto")
    @ApiModelProperty(notes = "Nombre corto del proveedor", example = "Lava Tap")
    private String nombreCortoProveedor;

    @JsonProperty("nombreLider")
    @ApiModelProperty(notes = "Líder de la cuadrilla."
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "Alejandro Morales Dominguez")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String lider;

    @JsonProperty("nombreSucursal")
    @ApiModelProperty(notes = "Nombre de la Sucursal", example = "BA MEGA DF LA LUNA")
    private String nombreSucursal;

    @JsonProperty("nombreSupervisor")
    @ApiModelProperty(notes = "Nombre del supervisor."
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "CESAR LIZANDRO VIDAL")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String nombreSupervisor;

    @JsonProperty("nombreSupervisorAuxiliar")
    @ApiModelProperty(notes = "Nombre del Supervisor auxiliar."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "KAREN SARAHI")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String nombreSupervisorAuxiliar;

    @JsonProperty("notasIncidente")
    @ApiModelProperty(notes = "Notas extras del incidente", example = "Sin comentarios")
    private String notas;

    @JsonProperty("notificacion")
    @ApiModelProperty(notes = "Notificación del Incidente", example = "NOTIFICADO")
    private String notificacion;

    @JsonProperty("numeroEmpleadoCliente")
    @ApiModelProperty(notes = "Número de empleado del cliente. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "748512")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String idCliente;

    @JsonProperty("numeroEmpleadoCoordinador")
    @ApiModelProperty(notes = "Número de empleado del coordinador. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "987452")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String idCoordinador;

    @JsonProperty("numeroEmpleadoSupervisor")
    @ApiModelProperty(notes = "Número de empleado del Supervisor. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "857469")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String idSupervisor;

    @JsonProperty("numeroIncidenteProveedor")
    @ApiModelProperty(notes = "Número del incidente del proveedor", example = "13748598")
    private String numeroIncidenteProveedor;

    @JsonProperty("numeroSucursal")
    @ApiModelProperty(notes = "Número de sucursal.", example = "0100")
    private Integer numeroSucursal;

    @JsonProperty("origenIncidente")
    @ApiModelProperty(notes = "Origen del Incidente", example = "1")
    private String origen;

    @JsonProperty("prioridad")
    @ApiModelProperty(notes = "Tipo de prioridad del Incidente", example = "ALTA")
    private String prioridad;

    @JsonProperty("proveedor")
    @ApiModelProperty(notes = "Proveedor", example = "Lava Tap")
    private String proveedor;

    @JsonProperty("puestoAlterno")
    @ApiModelProperty(notes = "Puesto del contacto alterno", example = "Jefe de Limpieza")
    private String puestoAlterno;

    @JsonProperty("puestoCliente")
    @ApiModelProperty(notes = "Puesto del cliente.", example = "Encargado de Limpieza")
    private String puestoCliente;

    @JsonProperty("razonSocial")
    @ApiModelProperty(notes = "Razón social del proveedor", example = "Lava Tap SA de CV")
    private String razonSocial;

    @JsonProperty("rechazoCierrePrevioEstatus")
    @ApiModelProperty(notes = "Bandera de rechazo de cierre previo", example = "false")
    private Boolean banderaRechazoCierrePrevio;

    @JsonProperty("resolucionIncidente")
    @ApiModelProperty(notes = "Resolución del Proveedor", example = "ATENDIDO")
    private String resolucion;

    @JsonProperty("segundoACargo")
    @ApiModelProperty(notes = "Segunda persona a cargo. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "CARLOS AGUILAR")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String segundoMando;

    @JsonProperty("telefonoCliente")
    @ApiModelProperty(notes = "Teléfono de contacto del cliente. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "5574874596")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoCliente;

    @JsonProperty("telefonoCoordinador")
    @ApiModelProperty(notes = "Teléfono del Coordinador."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "7485967485")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoCoordinador;

    @JsonProperty("telefonoSucursal")
    @ApiModelProperty(notes = "Teléfono de la sucursal."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "5544789745")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoSucursal;

    @JsonProperty("telefonoSupervisor")
    @ApiModelProperty(notes = "Teléfono del supervisor."
            + " `Descifre el valor de éste campo con la llave privada (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "5574875963")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoSupervisor;

    @JsonProperty("telefonoUsuarioAlterno")
    @ApiModelProperty(notes = "Teléfono del Usuario alterno. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "2227485796")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String telefonoContactoAlterno;

    @JsonProperty("tipoAtencion")
    @ApiModelProperty(notes = "Tipo de atención", example = "Servicios - Atendidos - Elevador")
    private String tipoAtencion;

    @JsonProperty("tipoCierre")
    @ApiModelProperty(notes = "Tipo de Cierre por el proveedor", example = "Técnico")
    private String tipoCierreProvedor;

    @JsonProperty("tipoFalla")
    @ApiModelProperty(notes = "Tipo de Falla", example = "Papel Higiénico")
    private String tipoFalla;

    @JsonProperty("usuarioAlterno")
    @ApiModelProperty(notes = "Nombre de Contacto alterno. "
            + "`Descifre el valor de éste campo con la llave privada (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "MIGUEL ÁNGEL DELGADO")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String usuarioAlterno;

    @JsonProperty("zona")
    @ApiModelProperty(notes = "Clave de la Zona", example = "2")
    private Integer idZona;

    public IncidenteLimpiezaDTO() {
        super();

    }

    public Integer getIdIncidencia() {
        return idIncidencia;
    }

    public void setIdIncidencia(Integer idIncidencia) {
        this.idIncidencia = idIncidencia;
    }

    public Integer getNumeroSucursal() {
        return numeroSucursal;
    }

    public void setNumeroSucursal(Integer numeroSucursal) {
        this.numeroSucursal = numeroSucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getTelefonoSucursal() {
        return telefonoSucursal;
    }

    public void setTelefonoSucursal(String telefonoSucursal) {
        this.telefonoSucursal = telefonoSucursal;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    public String getFalla() {
        return falla;
    }

    public void setFalla(String falla) {
        this.falla = falla;
    }

    public String getTipoFalla() {
        return tipoFalla;
    }

    public void setTipoFalla(String tipoFalla) {
        this.tipoFalla = tipoFalla;
    }

    public String getIncidenteAnterior() {
        return incidenteAnterior;
    }

    public void setIncidenteAnterior(String incidenteAnterior) {
        this.incidenteAnterior = incidenteAnterior;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public String getPuestoCliente() {
        return puestoCliente;
    }

    public void setPuestoCliente(String puestoCliente) {
        this.puestoCliente = puestoCliente;
    }

    public String getAutorizacionCliente() {
        return autorizacionCliente;
    }

    public void setAutorizacionCliente(String autorizacionCliente) {
        this.autorizacionCliente = autorizacionCliente;
    }

    public String getEvaluacionCliente() {
        return evaluacionCliente;
    }

    public void setEvaluacionCliente(String evaluacionCliente) {
        this.evaluacionCliente = evaluacionCliente;
    }

    public String getUsuarioAlterno() {
        return usuarioAlterno;
    }

    public void setUsuarioAlterno(String usuarioAlterno) {
        this.usuarioAlterno = usuarioAlterno;
    }

    public String getPuestoAlterno() {
        return puestoAlterno;
    }

    public void setPuestoAlterno(String puestoAlterno) {
        this.puestoAlterno = puestoAlterno;
    }

    public String getTelefonoContactoAlterno() {
        return telefonoContactoAlterno;
    }

    public void setTelefonoContactoAlterno(String telefonoContactoAlterno) {
        this.telefonoContactoAlterno = telefonoContactoAlterno;
    }

    public String getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(String idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public String getNombreSupervisorAuxiliar() {
        return nombreSupervisorAuxiliar;
    }

    public void setNombreSupervisorAuxiliar(String nombreSupervisorAuxiliar) {
        this.nombreSupervisorAuxiliar = nombreSupervisorAuxiliar;
    }

    public String getNombreSupervisor() {
        return nombreSupervisor;
    }

    public void setNombreSupervisor(String nombreSupervisor) {
        this.nombreSupervisor = nombreSupervisor;
    }

    public String getTelefonoSupervisor() {
        return telefonoSupervisor;
    }

    public void setTelefonoSupervisor(String telefonoSupervisor) {
        this.telefonoSupervisor = telefonoSupervisor;
    }

    public String getIdCoordinador() {
        return idCoordinador;
    }

    public void setIdCoordinador(String idCoordinador) {
        this.idCoordinador = idCoordinador;
    }

    public String getNombreCoordinador() {
        return nombreCoordinador;
    }

    public void setNombreCoordinador(String nombreCoordinador) {
        this.nombreCoordinador = nombreCoordinador;
    }

    public String getTelefonoCoordinador() {
        return telefonoCoordinador;
    }

    public void setTelefonoCoordinador(String telefonoCoordinador) {
        this.telefonoCoordinador = telefonoCoordinador;
    }

    public String getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(String idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getNombreCortoProveedor() {
        return nombreCortoProveedor;
    }

    public void setNombreCortoProveedor(String nombreCortoProveedor) {
        this.nombreCortoProveedor = nombreCortoProveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getNumeroIncidenteProveedor() {
        return numeroIncidenteProveedor;
    }

    public void setNumeroIncidenteProveedor(String numeroIncidenteProveedor) {
        this.numeroIncidenteProveedor = numeroIncidenteProveedor;
    }

    public String getAutorizacionProveedor() {
        return autorizacionProveedor;
    }

    public void setAutorizacionProveedor(String autorizacionProveedor) {
        this.autorizacionProveedor = autorizacionProveedor;
    }

    public String getTipoCierreProvedor() {
        return tipoCierreProvedor;
    }

    public void setTipoCierreProvedor(String tipoCierreProvedor) {
        this.tipoCierreProvedor = tipoCierreProvedor;
    }

    public String getAutorizacionPipa() {
        return autorizacionPipa;
    }

    public void setAutorizacionPipa(String autorizacionPipa) {
        this.autorizacionPipa = autorizacionPipa;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(String notificacion) {
        this.notificacion = notificacion;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getTipoAtencion() {
        return tipoAtencion;
    }

    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    public String getDeclinarProveedor() {
        return declinarProveedor;
    }

    public void setDeclinarProveedor(String declinarProveedor) {
        this.declinarProveedor = declinarProveedor;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaInicioAtencion() {
        return fechaInicioAtencion;
    }

    public void setFechaInicioAtencion(String fechaInicioAtencion) {
        this.fechaInicioAtencion = fechaInicioAtencion;
    }

    public String getFechaUltimoIncidente() {
        return fechaUltimoIncidente;
    }

    public void setFechaUltimoIncidente(String fechaUltimoIncidente) {
        this.fechaUltimoIncidente = fechaUltimoIncidente;
    }

    public String getFechaEstimada() {
        return fechaEstimada;
    }

    public void setFechaEstimada(String fechaEstimada) {
        this.fechaEstimada = fechaEstimada;
    }

    public String getFechaCierreAdministrativo() {
        return fechaCierreAdministrativo;
    }

    public void setFechaCierreAdministrativo(String fechaCierreAdministrativo) {
        this.fechaCierreAdministrativo = fechaCierreAdministrativo;
    }

    public String getFechaCierreTecnico() {
        return fechaCierreTecnico;
    }

    public void setFechaCierreTecnico(String fechaCierreTecnico) {
        this.fechaCierreTecnico = fechaCierreTecnico;
    }

    public String getFechaCierreTecnicoDos() {
        return fechaCierreTecnicoDos;
    }

    public void setFechaCierreTecnicoDos(String fechaCierreTecnicoDos) {
        this.fechaCierreTecnicoDos = fechaCierreTecnicoDos;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getRechazoCliente() {
        return rechazoCliente;
    }

    public void setRechazoCliente(String rechazoCliente) {
        this.rechazoCliente = rechazoCliente;
    }

    public String getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public String getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(String fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public String getFechaAutorizaProveedor() {
        return fechaAutorizaProveedor;
    }

    public void setFechaAutorizaProveedor(String fechaAutorizaProveedor) {
        this.fechaAutorizaProveedor = fechaAutorizaProveedor;
    }

    public String getFechaRechazoProveedor() {
        return fechaRechazoProveedor;
    }

    public void setFechaRechazoProveedor(String fechaRechazoProveedor) {
        this.fechaRechazoProveedor = fechaRechazoProveedor;
    }

    public Integer getIdCuadrilla() {
        return idCuadrilla;
    }

    public void setIdCuadrilla(Integer idCuadrilla) {
        this.idCuadrilla = idCuadrilla;
    }

    public String getLider() {
        return lider;
    }

    public void setLider(String lider) {
        this.lider = lider;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSegundoMando() {
        return segundoMando;
    }

    public void setSegundoMando(String segundoMando) {
        this.segundoMando = segundoMando;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public String getMensajeProveedor() {
        return mensajeProveedor;
    }

    public void setMensajeProveedor(String mensajeProveedor) {
        this.mensajeProveedor = mensajeProveedor;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public String getClienteAvisado() {
        return clienteAvisado;
    }

    public void setClienteAvisado(String clienteAvisado) {
        this.clienteAvisado = clienteAvisado;
    }

    public String getLatitudFin() {
        return latitudFin;
    }

    public void setLatitudFin(String latitudFin) {
        this.latitudFin = latitudFin;
    }

    public String getLongitudFin() {
        return longitudFin;
    }

    public void setLongitudFin(String longitudFin) {
        this.longitudFin = longitudFin;
    }

    public Integer getBanderaMontoAutorizado() {
        return banderaMontoAutorizado;
    }

    public void setBanderaMontoAutorizado(Integer banderaMontoAutorizado) {
        this.banderaMontoAutorizado = banderaMontoAutorizado;
    }

    public Integer getBanderaAplazamiento() {
        return banderaAplazamiento;
    }

    public void setBanderaAplazamiento(Integer banderaAplazamiento) {
        this.banderaAplazamiento = banderaAplazamiento;
    }

    public Integer getBanderas() {
        return banderas;
    }

    public void setBanderas(Integer banderas) {
        this.banderas = banderas;
    }

    public String getFechaAtencionGarantia() {
        return fechaAtencionGarantia;
    }

    public void setFechaAtencionGarantia(String fechaAtencionGarantia) {
        this.fechaAtencionGarantia = fechaAtencionGarantia;
    }

    public String getFechaCierreGarantia() {
        return fechaCierreGarantia;
    }

    public void setFechaCierreGarantia(String fechaCierreGarantia) {
        this.fechaCierreGarantia = fechaCierreGarantia;
    }

    public Boolean getBanderaRechazoCierrePrevio() {
        return banderaRechazoCierrePrevio;
    }

    public void setBanderaRechazoCierrePrevio(Boolean banderaRechazoCierrePrevio) {
        this.banderaRechazoCierrePrevio = banderaRechazoCierrePrevio;
    }

    public String getDiasAplazammiento() {
        return diasAplazammiento;
    }

    public void setDiasAplazammiento(String diasAplazammiento) {
        this.diasAplazammiento = diasAplazammiento;
    }

    public String getLatitudInicio() {
        return latitudInicio;
    }

    public void setLatitudInicio(String latitudInicio) {
        this.latitudInicio = latitudInicio;
    }

    public String getLongitudInicio() {
        return longitudInicio;
    }

    public void setLongitudInicio(String longitudInicio) {
        this.longitudInicio = longitudInicio;
    }

    public String getCostoPipa() {
        return costoPipa;
    }

    public void setCostoPipa(String costoPipa) {
        this.costoPipa = costoPipa;
    }

    public String getMontoEstimado() {
        return montoEstimado;
    }

    public void setMontoEstimado(String montoEstimado) {
        this.montoEstimado = montoEstimado;
    }

    public String getMontoReal() {
        return montoReal;
    }

    public void setMontoReal(String montoReal) {
        this.montoReal = montoReal;
    }

    public String getMontoEstimadoAutorizado() {
        return montoEstimadoAutorizado;
    }

    public void setMontoEstimadoAutorizado(String montoEstimadoAutorizado) {
        this.montoEstimadoAutorizado = montoEstimadoAutorizado;
    }

    @Override
    public String toString() {
        return "IncidenteLimpiezaDTO{" + "idCuadrilla=" + idCuadrilla + ", idProveedor="
                + idProveedor + ", idIncidencia=" + idIncidencia + ", banderaAplazamiento=" + banderaAplazamiento
                + ", banderas=" + banderas + ", autorizacionCliente=" + autorizacionCliente + ", autorizacionPipa="
                + autorizacionPipa + ", autorizacionProveedor=" + autorizacionProveedor + ", evaluacionCliente="
                + evaluacionCliente + ", centroCostos=" + centroCostos + ", clienteAvisado=" + clienteAvisado
                + ", correo=" + correo + ", costoPipa=" + costoPipa + ", declinarProveedor=" + declinarProveedor
                + ", diasAplazammiento=" + diasAplazammiento + ", estado=" + estado + ", falla=" + falla
                + ", fechaAtencion=" + fechaAtencion + ", fechaAtencionGarantia=" + fechaAtencionGarantia
                + ", fechaAutorizaProveedor=" + fechaAutorizaProveedor + ", fechaCancelacion=" + fechaCancelacion
                + ", fechaCierre=" + fechaCierre + ", fechaCierreAdministrativo=" + fechaCierreAdministrativo
                + ", fechaCierreGarantia=" + fechaCierreGarantia + ", fechaCierreTecnico=" + fechaCierreTecnico
                + ", fechaCierreTecnicoDos=" + fechaCierreTecnicoDos + ", fechaCreacion=" + fechaCreacion
                + ", fechaInicioAtencion=" + fechaInicioAtencion + ", fechaModificacion=" + fechaModificacion
                + ", fechaEstimada=" + fechaEstimada + ", rechazoCliente=" + rechazoCliente + ", fechaRechazoProveedor="
                + fechaRechazoProveedor + ", fechaUltimoIncidente=" + fechaUltimoIncidente + ", incidencia=" + incidencia
                + ", incidenteAnterior=" + incidenteAnterior + ", latitudFin=" + latitudFin + ", latitudInicio="
                + latitudInicio + ", longitudFin=" + longitudFin + ", longitudInicio=" + longitudInicio + ", mensajeProveedor="
                + mensajeProveedor + ", menu=" + menu + ", banderaMontoAutorizado=" + banderaMontoAutorizado + ", montoEstimado="
                + montoEstimado + ", montoEstimadoAutorizado=" + montoEstimadoAutorizado + ", montoReal=" + montoReal
                + ", motivoEstado=" + motivoEstado + ", nombreCliente=" + nombreCliente + ", nombreCoordinador="
                + nombreCoordinador + ", nombreCortoProveedor=" + nombreCortoProveedor + ", lider=" + lider
                + ", nombreSucursal=" + nombreSucursal + ", nombreSupervisor=" + nombreSupervisor + ", nombreSupervisorAuxiliar="
                + nombreSupervisorAuxiliar + ", notas=" + notas + ", notificacion=" + notificacion + ", idCliente=" + idCliente
                + ", idCoordinador=" + idCoordinador + ", idSupervisor=" + idSupervisor + ", numeroIncidenteProveedor="
                + numeroIncidenteProveedor + ", numeroSucursal=" + numeroSucursal + ", origen=" + origen + ", prioridad="
                + prioridad + ", proveedor=" + proveedor + ", puestoAlterno=" + puestoAlterno + ", puestoCliente="
                + puestoCliente + ", razonSocial=" + razonSocial + ", banderaRechazoCierrePrevio=" + banderaRechazoCierrePrevio
                + ", resolucion=" + resolucion + ", segundoMando=" + segundoMando + ", telefonoCliente=" + telefonoCliente
                + ", telefonoCoordinador=" + telefonoCoordinador + ", telefonoSucursal=" + telefonoSucursal
                + ", telefonoSupervisor="
                + telefonoSupervisor + ", telefonoContactoAlterno=" + telefonoContactoAlterno + ", tipoAtencion=" + tipoAtencion
                + ", tipoCierreProvedor=" + tipoCierreProvedor + ", tipoFalla=" + tipoFalla + ", usuarioAlterno=" + usuarioAlterno
                + ", idZona=" + idZona + '}';
    }

}
