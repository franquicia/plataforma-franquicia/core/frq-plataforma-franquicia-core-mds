package com.gs.baz.frq.modelo.negocio.archivos.init;

import com.gs.baz.frq.modelo.negocio.archivos.dao.ArchivoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.archivos")
public class ConfigArchivo {

    private final Logger logger = LogManager.getLogger();

    public ConfigArchivo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ArchivoDAOImpl archivoDAOImpl() {
        return new ArchivoDAOImpl();
    }

}
