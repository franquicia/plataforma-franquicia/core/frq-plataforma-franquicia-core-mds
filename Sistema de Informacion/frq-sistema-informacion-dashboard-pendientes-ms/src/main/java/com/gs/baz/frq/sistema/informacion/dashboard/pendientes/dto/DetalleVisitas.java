/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de visitas", value = "VisitasDetalle")
public class DetalleVisitas {

    @JsonProperty(value = "detalleVisitas")
    @ApiModelProperty(notes = "detalleVisitas")
    private List<DetalleVisita> detalleVisita;

    public DetalleVisitas(List<DetalleVisita> detalleVisita) {
        this.detalleVisita = detalleVisita;
    }

    public List<DetalleVisita> getDetalleVisita() {
        return detalleVisita;
    }

    public void setDetalleVisita(List<DetalleVisita> detalleVisita) {
        this.detalleVisita = detalleVisita;
    }

    @Override
    public String toString() {
        return "DetalleVisitas{" + "detalleVisita=" + detalleVisita + '}';
    }

}
