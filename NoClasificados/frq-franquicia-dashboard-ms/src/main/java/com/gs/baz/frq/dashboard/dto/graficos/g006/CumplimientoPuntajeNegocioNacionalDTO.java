/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g006;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author B73601
 */
public class CumplimientoPuntajeNegocioNacionalDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocios")
    private List<NegocioDTO> negocios;

    public List<NegocioDTO> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<NegocioDTO> negocios) {
        this.negocios = negocios;
    }

    @Override
    public String toString() {
        return "CumplimientoPuntajeNegocioNacionalDTO{" + "negocios=" + negocios + '}';
    }

}
