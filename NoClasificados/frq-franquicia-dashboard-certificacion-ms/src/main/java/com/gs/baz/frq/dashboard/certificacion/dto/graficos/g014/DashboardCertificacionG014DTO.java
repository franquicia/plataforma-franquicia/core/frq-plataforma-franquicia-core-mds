/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.graficos.g014;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionG014DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_base")
    private Integer idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicial")
    private String fechaInicial;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_final")
    private String fechaFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_invitado")
    private Integer idInvitado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_ceco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_usuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "calificacion")
    private Integer calificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango_color")
    private String rangoColor;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "hora_entrada")
    private String horaEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "hora_salida")
    private String horaSalida;

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(BigDecimal calificacion) {
        this.calificacion = (calificacion == null ? null : calificacion.intValue());
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(BigDecimal idRango) {
        this.idRango = (idRango == null ? null : idRango.intValue());
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    public Integer getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(BigDecimal idCecoBase) {
        this.idCecoBase = (idCecoBase == null ? null : idCecoBase.intValue());
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public Integer getIdInvitado() {
        return idInvitado;
    }

    public void setIdInvitado(BigDecimal idInvitado) {
        this.idInvitado = (idInvitado == null ? null : idInvitado.intValue());
    }

    @Override
    public String toString() {
        return "DashboardCertificacionG014DTO{" + "idCeco=" + idCeco + ", idCecoBase=" + idCecoBase + ", fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", idUsuario=" + idUsuario + ", idInvitado=" + idInvitado + ", fecha=" + fecha + ", nombreCeco=" + nombreCeco + ", nombreUsuario=" + nombreUsuario + ", calificacion=" + calificacion + ", idRango=" + idRango + ", rangoColor=" + rangoColor + ", horaEntrada=" + horaEntrada + ", horaSalida=" + horaSalida + '}';
    }

}
