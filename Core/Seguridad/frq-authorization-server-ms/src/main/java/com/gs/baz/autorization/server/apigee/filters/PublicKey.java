/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.filters;

/**
 *
 * @author cescobarh
 */
public enum PublicKey {

    PUBLIC_KEY("");

    private final String value;

    PublicKey(String value) {
        this.value = value;
    }

    public String value(String apiName) {
        if (apiName.equals("sistemas-franquicia-gestion-incidentes-catalogos")) {
            return PublicKeys.PUBLIC_KEY_GESTION_INCIDENTES_CATALOGOS.value();
        } else if (apiName.equals("sistemas-franquicia-gestion-incidentes-limpieza")) {
            return PublicKeys.PUBLIC_KEY_GESTION_INCIDENTES_LIMPIEZA.value();
        } else if (apiName.equals("sistemas-franquicia-seguridad")) {
            return PublicKeys.PUBLIC_KEY_SEGURIDAD.value();
        } else if (apiName.equals("sistemas-franquicia-utilerias")) {
            return PublicKeys.PUBLIC_KEY_UTILERIAS.value();
        } else {
            return value;
        }
    }

}
