/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.bi;

import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.frq.modelo.negocio.folios.dao.FolioDAOImpl;
import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioResumen;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.FirmaCorreo;
import com.gs.baz.frq.modelo.negocio.folios.dto.FolioBase;
import com.gs.baz.frq.modelo.negocio.folios.dto.ParametrosActualizaEstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.util.UtilMail;
import com.gs.baz.frq.modelo.negocio.folios.util.UtilObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

/**
 *
 * @author carlos
 */
@Component
public class CorreoBI {

    @Autowired
    private FolioDAOImpl folioDAOImpl3;

    @Autowired
    private HttpServletRequest httpServletRequest;

    public boolean enviaCorreo(HttpHeaders headers, Integer idFolio, Integer idEstado, Boolean primeraVez) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = sdf.format(new Date());
        if (idEstado != 2) {
            String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-app/modelo-negocio/bandeja/autorizacion/detalle/folio/";
            System.out.println("Entro al metodo");
            List<FirmaCorreo> firmas = folioDAOImpl3.selectAllFirmas(idFolio);
            System.out.println("Firmas: " + firmas);
            Integer numeroEmpleado = null;
            FirmaCorreo firmaUnica = null;
            if (primeraVez) {
                List<FirmaCorreo> firmasJerarquia1 = firmas.stream().filter(item -> item.getIdJerarquia() == 1).collect(Collectors.toList());
                firmasJerarquia1 = firmasJerarquia1.stream().sorted((FirmaCorreo o1, FirmaCorreo o2) -> o1.getIndice().compareTo(o2.getIndice())).collect(Collectors.toList());
                firmaUnica = firmasJerarquia1.stream().filter(item -> item.getIdEstadoFirma() == 1).findFirst().orElse(null);

                if (firmaUnica == null) {
                    List<FirmaCorreo> firmasJerarquia2 = firmas.stream().filter(item -> item.getIdJerarquia() == 2).collect(Collectors.toList());
                    firmasJerarquia2 = firmasJerarquia2.stream().sorted((FirmaCorreo o1, FirmaCorreo o2) -> o1.getIndice().compareTo(o2.getIndice())).collect(Collectors.toList());
                    firmaUnica = firmasJerarquia2.stream().filter(item -> item.getIdEstadoFirma() == 1).findFirst().orElse(null);
                    if (firmaUnica != null) {
                        idEstado = 4;
                        ParametrosActualizaEstadoFolio parametrosFolio = new ParametrosActualizaEstadoFolio();
                        parametrosFolio.setIdFolio(idFolio);
                        parametrosFolio.setIdEstadoFolio(idEstado);
                        folioDAOImpl3.updateRowEstado(parametrosFolio);
                    } else {
                        idEstado = 5;
                        ParametrosActualizaEstadoFolio parametrosFolio = new ParametrosActualizaEstadoFolio();
                        parametrosFolio.setIdFolio(idFolio);
                        parametrosFolio.setIdEstadoFolio(idEstado);
                        folioDAOImpl3.updateRowEstado(parametrosFolio);
                        primeraVez = false;
                        FolioBase folioBase = folioDAOImpl3.selectRow(idFolio);
                        numeroEmpleado = folioBase.getCreador().getNumeroEmpleado();

                    }
                }

            } else {
                if (idEstado == 3) {
                    firmas = firmas.stream().filter(item -> item.getIdJerarquia() == 1).collect(Collectors.toList());
                    FirmaCorreo firmaCorreo = firmas.stream().filter(item -> item.getIdJerarquia() == 1).findFirst().orElse(null);
                    if (firmaCorreo != null) {
                        numeroEmpleado = firmaCorreo.getNumeroEmpleado();
                    }
                } else if (idEstado == 4) {
                    firmas = firmas.stream().filter(item -> item.getIdJerarquia() == 2).collect(Collectors.toList());
                    FirmaCorreo firmaCorreo = firmas.stream().filter(item -> item.getIdJerarquia() == 2).findFirst().orElse(null);
                    if (firmaCorreo != null) {
                        numeroEmpleado = firmaCorreo.getNumeroEmpleado();
                    }
                } else if (idEstado == 5) {
                    FolioBase folioBase = folioDAOImpl3.selectRow(idFolio);
                    numeroEmpleado = folioBase.getCreador().getNumeroEmpleado();
                }
            }
            BusquedaFolioResumen folio = folioDAOImpl3.selectResumenDocumento(idFolio);
            if (idEstado == 3 || idEstado == 4 || primeraVez) {
                Empleado datosEmpleado = folioDAOImpl3.selectUsuario("" + firmaUnica.getNumeroEmpleado());

                File origen = new File("/franquicia/plataforma_franquicia/correo/plantilla/mailMoeloNegocio.html");
                InputStream in = new FileInputStream(origen);
                String jsonaFormar = "{\"idFolio\": " + idFolio + ",\"documento\":{\"titulo\":\"" + folio.getDocumento().getTitulo() + "\"},\"estado\":{\"idEstadoFolio\":" + idEstado + ",\"descripcion\":\"POR REVISAR\"},\"firma\":{\"idFirma\":" + firmaUnica.getIdFirma() + "}}   ";

                byte[] bytesEncoded = Base64.encodeBase64(jsonaFormar.getBytes());
                System.out.println("encoded value is " + new String(bytesEncoded));
                String uriContexto;
                if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
                    uriContexto = "http://10.53.33.83:82/plataforma-franquicia-app/modelo-negocio/bandeja/autorizacion/detalle/folio/" + new String(bytesEncoded);
                } else {
                    uriContexto = basePath + new String(bytesEncoded);
                }
                String strCorreoOwner = UtilObject.inputStreamAsString(in);
                strCorreoOwner = strCorreoOwner.replace("@uriContexto", uriContexto);
                strCorreoOwner = strCorreoOwner.replace("@Nombre", datosEmpleado.getNombre());
                strCorreoOwner = strCorreoOwner.replace("@Documento", folio.getDocumento().getTitulo());
                strCorreoOwner = strCorreoOwner.replace("@folio", "" + idFolio);
                strCorreoOwner = strCorreoOwner.replace("@Fecha", fecha);
                String destinatario;
                if (firmaUnica.getCorreo() != null) {
                    if (firmaUnica.getCorreo().equals("")) {
                        destinatario = "caaguilar@elektra.com.mx";
                    } else {
                        destinatario = firmaUnica.getCorreo();
                    }
                } else {
                    destinatario = "caaguilar@elektra.com.mx";
                }
                UtilMail.enviaCorreo(destinatario, null, "¡Autorización pendiente en Modelo de Negocio!", strCorreoOwner, null);
                UtilMail.enviaCorreo("caaguilar@elektra.com.mx", null, "¡Autorización pendiente en Modelo de Negocio!", strCorreoOwner, null);
                System.out.println("[ Mail enviado ]");

            } else if (idEstado == 5) {
                Empleado datosEmpleado = folioDAOImpl3.selectUsuario("" + numeroEmpleado);
                BusquedaFolioResumen resumenDocumento = folioDAOImpl3.selectResumenDocumento(idFolio);
                File origen = new File("/franquicia/plataforma_franquicia/correo/plantilla/maiPublicadoMoeloNegocio.html");
                InputStream in = new FileInputStream(origen);
                String uriContexto;
                basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-app/modelo-negocio/publicados";
                if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
                    uriContexto = "http://10.53.33.83:82/plataforma-franquicia-app/modelo-negocio/publicados";
                } else {
                    uriContexto = basePath;
                }
                String strCorreoOwner = UtilObject.inputStreamAsString(in);
                strCorreoOwner = strCorreoOwner.replace("@uriContexto", uriContexto);
                strCorreoOwner = strCorreoOwner.replace("@Nombre", datosEmpleado.getNombre());
                strCorreoOwner = strCorreoOwner.replace("@Documento", resumenDocumento.getDocumento().getTitulo());
                strCorreoOwner = strCorreoOwner.replace("@folio", "" + idFolio);
                strCorreoOwner = strCorreoOwner.replace("@Fecha", "" + fecha);
                String destinatario;
                if (datosEmpleado.getCorreo() != null) {
                    if (datosEmpleado.getCorreo().equals("")) {
                        destinatario = "caaguilar@elektra.com.mx";
                    } else {
                        destinatario = datosEmpleado.getCorreo();
                    }
                } else {
                    destinatario = "caaguilar@elektra.com.mx";
                }
                UtilMail.enviaCorreo(destinatario, null, "¡Gracias por tu participación! Documento publicado", strCorreoOwner, null);
                UtilMail.enviaCorreo("caaguilar@elektra.com.mx", null, "¡Gracias por tu participación! Documento publicado", strCorreoOwner, null);
                System.out.println("[ Mail enviado ]");
            }
        }
        return true;
    }

    public boolean enviaCorreo(HttpHeaders headers, Integer idFolio, Integer idEstado, Integer numEmpleado) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = sdf.format(new Date());
        if (idEstado != 2) {
            String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-app/modelo-negocio/bandeja/autorizacion/detalle/folio/";
            System.out.println("Entro al metodo");
            List<FirmaCorreo> firmas = folioDAOImpl3.selectAllFirmas(idFolio);
            System.out.println("Firmas: " + firmas);
            Integer numeroEmpleado = null;
            if (idEstado == 3) {
                firmas = firmas.stream().filter(item -> item.getIdJerarquia() == 1 && Objects.equals(item.getNumeroEmpleado(), numEmpleado)).collect(Collectors.toList());
                FirmaCorreo firmaCorreo = firmas.stream().filter(item -> item.getIdJerarquia() == 1).findFirst().orElse(null);
                if (firmaCorreo != null) {
                    numeroEmpleado = firmaCorreo.getNumeroEmpleado();
                }
            } else if (idEstado == 4) {
                firmas = firmas.stream().filter(item -> item.getIdJerarquia() == 2 && Objects.equals(item.getNumeroEmpleado(), numEmpleado)).collect(Collectors.toList());
                FirmaCorreo firmaCorreo = firmas.stream().filter(item -> item.getIdJerarquia() == 2).findFirst().orElse(null);
                if (firmaCorreo != null) {
                    numeroEmpleado = firmaCorreo.getNumeroEmpleado();
                }
            } else if (idEstado == 5) {
                FolioBase folioBase = folioDAOImpl3.selectRow(idFolio);
                numeroEmpleado = folioBase.getCreador().getNumeroEmpleado();
            }
            BusquedaFolioResumen folio = folioDAOImpl3.selectResumenDocumento(idFolio);
            if (idEstado == 3 || idEstado == 4) {
                for (FirmaCorreo firmaCorreo : firmas) {
                    Empleado datosEmpleado = folioDAOImpl3.selectUsuario("" + firmaCorreo.getNumeroEmpleado());

                    File origen = new File("/franquicia/plataforma_franquicia/correo/plantilla/mailMoeloNegocio.html");
                    InputStream in = new FileInputStream(origen);
                    String jsonaFormar = "{\"idFolio\": " + idFolio + ",\"documento\":{\"titulo\":\"" + folio.getDocumento().getTitulo() + "\"},\"estado\":{\"idEstadoFolio\":" + idEstado + ",\"descripcion\":\"POR REVISAR\"},\"firma\":{\"idFirma\":" + firmaCorreo.getIdFirma() + "}}   ";

                    byte[] bytesEncoded = Base64.encodeBase64(jsonaFormar.getBytes());
                    System.out.println("encoded value is " + new String(bytesEncoded));
                    String uriContexto;
                    if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
                        uriContexto = "http://10.53.33.83:82/plataforma-franquicia-app/modelo-negocio/bandeja/autorizacion/detalle/folio/" + new String(bytesEncoded);
                    } else {
                        uriContexto = basePath + new String(bytesEncoded);
                    }
                    String strCorreoOwner = UtilObject.inputStreamAsString(in);
                    strCorreoOwner = strCorreoOwner.replace("@uriContexto", uriContexto);
                    strCorreoOwner = strCorreoOwner.replace("@Nombre", datosEmpleado.getNombre());
                    strCorreoOwner = strCorreoOwner.replace("@Documento", folio.getDocumento().getTitulo());
                    strCorreoOwner = strCorreoOwner.replace("@folio", "" + idFolio);
                    strCorreoOwner = strCorreoOwner.replace("@Fecha", fecha);
                    String destinatario;
                    if (firmaCorreo.getCorreo() != null) {
                        if (firmaCorreo.getCorreo().equals("")) {
                            destinatario = "caaguilar@elektra.com.mx";
                        } else {
                            destinatario = firmaCorreo.getCorreo();
                        }
                    } else {
                        destinatario = "caaguilar@elektra.com.mx";
                    }
                    UtilMail.enviaCorreo(destinatario, null, "¡Autorización pendiente en Modelo de Negocio!", strCorreoOwner, null);
                    UtilMail.enviaCorreo("caaguilar@elektra.com.mx", null, "¡Autorización pendiente en Modelo de Negocio!", strCorreoOwner, null);
                    System.out.println("[ Mail enviado ]");
                }
            } else if (idEstado == 5) {
                Empleado datosEmpleado = folioDAOImpl3.selectUsuario("" + numeroEmpleado);
                BusquedaFolioResumen resumenDocumento = folioDAOImpl3.selectResumenDocumento(idFolio);
                File origen = new File("/franquicia/plataforma_franquicia/correo/plantilla/maiPublicadoMoeloNegocio.html");
                InputStream in = new FileInputStream(origen);
                String uriContexto;
                basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-app/modelo-negocio/publicados";
                if (Environment.CURRENT.valueReal().equals(Environment.PRODUCTION)) {
                    uriContexto = "http://10.53.33.83:82/plataforma-franquicia-app/modelo-negocio/publicados";
                } else {
                    uriContexto = basePath;
                }
                String strCorreoOwner = UtilObject.inputStreamAsString(in);
                strCorreoOwner = strCorreoOwner.replace("@uriContexto", uriContexto);
                strCorreoOwner = strCorreoOwner.replace("@Nombre", datosEmpleado.getNombre());
                strCorreoOwner = strCorreoOwner.replace("@Documento", resumenDocumento.getDocumento().getTitulo());
                strCorreoOwner = strCorreoOwner.replace("@folio", "" + idFolio);
                strCorreoOwner = strCorreoOwner.replace("@Fecha", "" + fecha);
                String destinatario;
                if (datosEmpleado.getCorreo() != null) {
                    if (datosEmpleado.getCorreo().equals("")) {
                        destinatario = "caaguilar@elektra.com.mx";
                    } else {
                        destinatario = datosEmpleado.getCorreo();
                    }
                } else {
                    destinatario = "caaguilar@elektra.com.mx";
                }
                UtilMail.enviaCorreo(destinatario, null, "¡Gracias por tu participación! Documento publicado", strCorreoOwner, null);
                UtilMail.enviaCorreo("caaguilar@elektra.com.mx", null, "¡Gracias por tu participación! Documento publicado", strCorreoOwner, null);
                System.out.println("[ Mail enviado ]");
            }
        }
        return true;
    }

}
