/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g002;

import com.gs.baz.frq.dashboard.dao.graficos.g002.DashboardDAOImpl002;
import com.gs.baz.frq.dashboard.dto.graficos.g002.CalificacionNegociosDisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard002 {

    @Autowired
    private DashboardDAOImpl002 dashboardDAOImpl002;

    @RequestMapping(value = "secure/grafico/get/calificacion/disciplina", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CalificacionNegociosDisciplinaDTO getCalificacionDisciplina(@RequestBody CalificacionNegociosDisciplinaDTO calificacionNegociosDisciplinaDTO) throws CustomException {
        return dashboardDAOImpl002.selectRowsCalificacionNegociosDisciplina(calificacionNegociosDisciplinaDTO);
    }

}
