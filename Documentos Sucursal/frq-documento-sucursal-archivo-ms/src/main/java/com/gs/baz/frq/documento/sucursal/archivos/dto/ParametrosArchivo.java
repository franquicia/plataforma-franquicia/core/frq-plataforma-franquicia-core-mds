/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.validador.Base64;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author carlos
 */
public class ParametrosArchivo {

    private transient Integer idFolio;

    private transient Long uid;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del archivo", example = "documento.docx", required = true)
    @NotBlank
    private String nombre;

    @JsonProperty(value = "archivo")
    @ApiModelProperty(notes = "archivo base 64", example = "DIwMTExMDAzMDYxNjc2OS9fLnhsc3g=", required = true)
    @Base64
    @NotBlank
    private String archivo;

    @JsonProperty(value = "tipoContenido")
    @ApiModelProperty(notes = "Tipo contenido", example = "application/pdf", required = true)
    @NotBlank
    private String tipoContenido;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    @Override
    public String toString() {
        return "ParametrosArchivo{" + "idFolio=" + idFolio + ", uid=" + uid + ", nombre=" + nombre + ", archivo=" + archivo + ", tipoContenido=" + tipoContenido + '}';
    }

}
