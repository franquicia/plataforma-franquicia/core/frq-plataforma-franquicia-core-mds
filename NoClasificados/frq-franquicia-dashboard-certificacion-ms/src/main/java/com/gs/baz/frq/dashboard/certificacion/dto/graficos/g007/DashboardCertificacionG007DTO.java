/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.graficos.g007;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionG007DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicial")
    private String fechaInicial;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_final")
    private String fechaFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_gerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_base")
    private String idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "punto_contacto")
    private String puntoContacto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "certificador")
    private Integer certificador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_certificador")
    private String nombreCertificador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ultima_certificacion")
    private String ultimaCertificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "hora_entrada")
    private String horaEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "hora_salida")
    private String horaSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_visita")
    private Integer idVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(BigDecimal idGerente) {
        this.idGerente = (idGerente == null ? null : idGerente.intValue());
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(String idCecoBase) {
        this.idCecoBase = idCecoBase;
    }

    public String getPuntoContacto() {
        return puntoContacto;
    }

    public void setPuntoContacto(String puntoContacto) {
        this.puntoContacto = puntoContacto;
    }

    public Integer getCertificador() {
        return certificador;
    }

    public void setCertificador(BigDecimal certificador) {
        this.certificador = (certificador == null ? null : certificador.intValue());
    }

    public String getNombreCertificador() {
        return nombreCertificador;
    }

    public void setNombreCertificador(String nombreCertificador) {
        this.nombreCertificador = nombreCertificador;
    }

    public String getUltimaCertificacion() {
        return ultimaCertificacion;
    }

    public void setUltimaCertificacion(String ultimaCertificacion) {
        this.ultimaCertificacion = ultimaCertificacion;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(BigDecimal idVisita) {
        this.idVisita = (idVisita == null ? null : idVisita.intValue());
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    @Override
    public String toString() {
        return "DashboardCertificacionG005DTO{" + "fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", idGerente=" + idGerente + ", idNegocio=" + idNegocio + ", idCeco=" + idCeco + ", puntoContacto=" + puntoContacto + ", certificador=" + certificador + ", nombreCertificador=" + nombreCertificador + ", ultimaCertificacion=" + ultimaCertificacion + ", horaEntrada=" + horaEntrada + ", horaSalida=" + horaSalida + ", idVisita=" + idVisita + ", idUsuario=" + idUsuario + '}';
    }

}
