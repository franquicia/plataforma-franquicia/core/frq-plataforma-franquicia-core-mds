/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g005;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author B73601
 */
public class NegocioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rangos")
    private List<RangoDTO> rangos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "total_evaluados")
    private Integer totalEvaluados;

    public NegocioDTO() {
    }

    public NegocioDTO(Integer idNegocio, List<RangoDTO> rangos, Integer totalEvaluados) {
        this.idNegocio = idNegocio;
        this.rangos = rangos;
        this.totalEvaluados = totalEvaluados;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public List<RangoDTO> getRangos() {
        return rangos;
    }

    public void setRangos(List<RangoDTO> rangos) {
        this.rangos = rangos;
    }

    public Integer getTotalEvaluados() {
        return totalEvaluados;
    }

    public void setTotalEvaluados(Integer totalEvaluados) {
        this.totalEvaluados = totalEvaluados;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", rangos=" + rangos + ", totalEvaluados=" + totalEvaluados + '}';
    }

}
