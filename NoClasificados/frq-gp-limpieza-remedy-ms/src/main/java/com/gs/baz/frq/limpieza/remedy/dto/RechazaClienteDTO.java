/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Detalle del Rechazo por el cliente", value = "RechazaCliente")
public class RechazaClienteDTO {

    @JsonProperty(value = "idIncidente", required = true)
    @ApiModelProperty(notes = "Clave del folio", example = "13008031", position = -1, required = true)
    private Integer idIncidente;

    @JsonProperty(value = "justificacionRechazo", required = true)
    @ApiModelProperty(notes = "Motivos del rechazo",
            example = "Se rechaza el folio por no especificar la falla correctamente",
            required = true)
    private String justificacion;

    @JsonProperty(value = "nombreAdjunto", required = true)
    @ApiModelProperty(notes = "Nombre del archivo adjunto", example = "Evidencia.jpeg", required = true)
    private String nombreAdjunto;

    @JsonProperty(value = "archivoAdjunto", required = true)
    @ApiModelProperty(notes = "Archivo adjunto", example = "Archivo adjunto en base64", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String adjunto;

    @JsonProperty(value = "nombreSolicitante", required = true)
    @ApiModelProperty(notes = "Nombre del solicitante."
            + " `Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "Carlos Antonio Escobar", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String solicitante;

    @JsonProperty(value = "nombreAprobador", required = true)
    @ApiModelProperty(notes = "Nombre del . "
            + "`Cifre el valor de éste campo con la llave pública (accesoPublico)"
            + " con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "Miguel Ángel Delgado", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String aprobador;

    public RechazaClienteDTO() {
        super();
    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public String getNombreAdjunto() {
        return nombreAdjunto;
    }

    public void setNombreAdjunto(String nombreAdjunto) {
        this.nombreAdjunto = nombreAdjunto;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getAprobador() {
        return aprobador;
    }

    public void setAprobador(String aprobador) {
        this.aprobador = aprobador;
    }

    @Override
    public String toString() {
        return "RechazaClienteDTO{" + "idIncidente=" + idIncidente
                + ", justificacion=" + justificacion + ", nombreAdjunto="
                + nombreAdjunto + ", adjunto=" + adjunto + ", solicitante="
                + solicitante + ", aprobador=" + aprobador + '}';
    }

}
