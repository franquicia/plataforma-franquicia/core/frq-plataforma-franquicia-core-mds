package com.gs.baz.frq.documento.sucursal.proyectos.init;

import com.gs.baz.frq.documento.sucursal.proyectos.dao.ProyectoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.proyectos")
public class ConfigDocumentoSucursalProyecto {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalProyecto() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ProyectoDAOImpl documentoSucursalProyectoDAOImpl() {
        return new ProyectoDAOImpl();
    }

}
