package com.gs.baz.frq.respuesta.pregunta.visita.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.respuesta.pregunta.visita.dao.util.GenericDAO;
import com.gs.baz.frq.respuesta.pregunta.visita.dto.RespuestaPreguntaVisitaDTO;
import com.gs.baz.frq.respuesta.pregunta.visita.mprs.RespuestaPreguntaVisitaRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class RespuestaPreguntaVisitaDAOImpl extends DefaultDAO implements GenericDAO<RespuestaPreguntaVisitaDTO> {

    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMRESPVISIT");
        jdbcSelect.withProcedureName("SPGETRESPVISITA");
        jdbcSelect.returningResultSet("PA_CDATOS", new RespuestaPreguntaVisitaRowMapper());
    }

    @Override
    public List<RespuestaPreguntaVisitaDTO> selectRows(RespuestaPreguntaVisitaDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIRESPVISITA_ID", entityDTO.getIdRespuestaVisita());
        mapSqlParameterSource.addValue("PA_FIVISITA_ID", entityDTO.getIdVisita());
        mapSqlParameterSource.addValue("PA_FIPREGUNTA_ID", entityDTO.getIdPregunta());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<RespuestaPreguntaVisitaDTO>) out.get("PA_CDATOS");
    }
}
