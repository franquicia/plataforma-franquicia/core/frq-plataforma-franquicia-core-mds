/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de propiedades de la vista", value = "VistaPropiedades")
public class VistaPropiedades {

    @JsonProperty(value = "propiedades")
    @ApiModelProperty(notes = "propiedades")
    private List<VistaPropiedad> vistaPropiedades;

    public VistaPropiedades(List<VistaPropiedad> vistaPropiedades) {
        this.vistaPropiedades = vistaPropiedades;
    }

    public List<VistaPropiedad> getVistaPropiedades() {
        return vistaPropiedades;
    }

    public void setVistaPropiedades(List<VistaPropiedad> vistaPropiedades) {
        this.vistaPropiedades = vistaPropiedades;
    }

    @Override
    public String toString() {
        return "VistaPropiedades{" + "vistaPropiedades=" + vistaPropiedades + '}';
    }

}
