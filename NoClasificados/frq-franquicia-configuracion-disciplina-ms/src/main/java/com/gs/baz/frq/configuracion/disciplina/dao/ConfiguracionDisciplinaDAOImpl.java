package com.gs.baz.frq.configuracion.disciplina.dao;

import com.gs.baz.frq.configuracion.disciplina.dao.util.GenericDAO;
import com.gs.baz.frq.configuracion.disciplina.dto.ConfiguracionDisciplinaDTO;
import com.gs.baz.frq.configuracion.disciplina.mprs.ConfiguracionDisciplinaRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ConfiguracionDisciplinaDAOImpl extends DefaultDAO implements GenericDAO<ConfiguracionDisciplinaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectConfiguracionDisciplina;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMCONFIG_DI");
        jdbcInsert.withProcedureName("SPINSCONFIG_DISC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMCONFIG_DI");
        jdbcUpdate.withProcedureName("SPACTCONFIG_DISC");

        jdbcSelectConfiguracionDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectConfiguracionDisciplina.withSchemaName(schema);
        jdbcSelectConfiguracionDisciplina.withCatalogName("PAADMCONFIG_DI");
        jdbcSelectConfiguracionDisciplina.withProcedureName("SPGETCONFIG_DISC");
        jdbcSelectConfiguracionDisciplina.returningResultSet("PA_CDATOS", new ConfiguracionDisciplinaRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMCONFIG_DI");
        jdbcSelectByName.withProcedureName("SPGETCONFIG_DISC");
        jdbcSelectByName.returningResultSet("PA_CDATOS", new ConfiguracionDisciplinaRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMCONFIG_DI");
        jdbcDelete.withProcedureName("SPDELCONFIG_DISC");
    }

    @Override
    public ConfiguracionDisciplinaDTO insertRow(ConfiguracionDisciplinaDTO entityDTO) throws CustomException {
        try {

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityDTO.getIdFrecuencia());
            mapSqlParameterSource.addValue("PA_FCPROTOCOLO", entityDTO.getProtocolo());
            mapSqlParameterSource.addValue("PA_FIREG_ACTIVO", entityDTO.getRegistroActivo());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdConfiguracion(((BigDecimal) out.get("PA_FICONFIGDIS_ID")));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  ConfiguracionDisciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  ConfiguracionDisciplina"), ex);
        }
    }

    @Override
    public ConfiguracionDisciplinaDTO updateRow(ConfiguracionDisciplinaDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICONFIGDIS_ID", entityDTO.getIdConfiguracion());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityDTO.getIdFrecuencia());
            mapSqlParameterSource.addValue("PA_FCPROTOCOLO", entityDTO.getProtocolo());
            mapSqlParameterSource.addValue("PA_FIREG_ACTIVO", entityDTO.getRegistroActivo());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of ConfiguracionDisciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of ConfiguracionDisciplina"), ex);
        }
    }

    @Override
    public List<ConfiguracionDisciplinaDTO> selectRows(ConfiguracionDisciplinaDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", entityDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        Map<String, Object> out = jdbcSelectConfiguracionDisciplina.execute(mapSqlParameterSource);
        return (List<ConfiguracionDisciplinaDTO>) out.get("PA_CDATOS");
    }

    @Override
    public ConfiguracionDisciplinaDTO deleteRow(ConfiguracionDisciplinaDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of ConfiguracionDisciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of ConfiguracionDisciplina"), ex);
        }
    }

}
