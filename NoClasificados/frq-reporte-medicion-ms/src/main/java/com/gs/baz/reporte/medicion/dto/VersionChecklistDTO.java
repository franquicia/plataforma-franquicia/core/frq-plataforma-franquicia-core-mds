/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class VersionChecklistDTO {

    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonProperty(value = "id_checklist")
    private Integer idChecklist;

    @JsonProperty(value = "fecha_inicio")
    private String fechaInicio;

    @JsonProperty(value = "fecha_fin")
    private String fechaFin;

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public Integer getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(BigDecimal idChecklist) {
        this.idChecklist = (idChecklist == null ? null : idChecklist.intValue());
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Override
    public String toString() {
        return "VersionChecklistDTO{" + "idProtocolo=" + idProtocolo + ", idChecklist=" + idChecklist + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + '}';
    }

}
