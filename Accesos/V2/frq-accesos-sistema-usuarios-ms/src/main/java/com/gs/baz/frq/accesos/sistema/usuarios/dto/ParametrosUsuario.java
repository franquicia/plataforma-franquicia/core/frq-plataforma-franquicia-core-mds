/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Email;

/**
 *
 * @author carlos
 */
public class ParametrosUsuario {

    private transient Integer numeroEmpleado;

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Centro de costos", example = "954673")
    @NotEmpty
    private String centroCostos;

    @JsonProperty(value = "nombreUsuario")
    @ApiModelProperty(notes = "Nombre del Usuario", example = "Carlos Antonio Escobar Hernandez")
    @NotEmpty
    private String nombreUsuario;

    @JsonProperty(value = "tipoEmpleado")
    @ApiModelProperty(notes = "Tipo de empleado", example = "Interno")
    @NotEmpty
    private String tipoEmpleado;

    @JsonProperty(value = "puesto")
    @ApiModelProperty(notes = "Puesto de trabajo", example = "Desarrollador")
    @NotEmpty
    private String puesto;

    @JsonProperty(value = "correo")
    @ApiModelProperty(notes = "Correo del empleado", example = "cescobarh@eleketra.com.mx")
    @NotEmpty
    @Email
    private String correo;

    public ParametrosUsuario() {
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(String tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "ParametrosUsuario{" + "numeroEmpleado=" + numeroEmpleado + ", centroCostos=" + centroCostos + ", nombreUsuario=" + nombreUsuario + ", tipoEmpleado=" + tipoEmpleado + ", puesto=" + puesto + ", correo=" + correo + '}';
    }

}
