/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author MADA
 */
public class SeccionCumplimientoDTO {

    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ponderacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_visita")
    private String fechaVisista;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ponderacion_esperado")
    private Integer ponderacionEsperado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public SeccionCumplimientoDTO() {
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public String getFechaVisista() {
        return fechaVisista;
    }

    public void setFechaVisista(String fechaVisista) {
        this.fechaVisista = fechaVisista;
    }

    public Integer getPonderacionEsperado() {
        return ponderacionEsperado;
    }

    public void setPonderacionEsperado(BigDecimal ponderacionEsperado) {
        this.ponderacionEsperado = (ponderacionEsperado == null ? null : ponderacionEsperado.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "SeccionCumplimientoDTO{" + "idCeco=" + idCeco + ", idProtocolo=" + idProtocolo + ", modulo=" + modulo + ", ponderacion=" + ponderacion + ", fechaVisista=" + fechaVisista + ", ponderacionEsperado=" + ponderacionEsperado + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }
}
