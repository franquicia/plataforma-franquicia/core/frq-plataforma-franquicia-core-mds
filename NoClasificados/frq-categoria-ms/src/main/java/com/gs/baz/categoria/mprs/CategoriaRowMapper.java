package com.gs.baz.categoria.mprs;

import com.gs.baz.categoria.dto.CategoriaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CategoriaRowMapper implements RowMapper<CategoriaDTO> {

    private CategoriaDTO categoria;

    @Override
    public CategoriaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        categoria = new CategoriaDTO();
        categoria.setIdCategoria(((BigDecimal) rs.getObject("FIIDCATEG")));
        categoria.setDescripcion(rs.getString("FCDESCRIPCION"));
        categoria.setIdStatus((BigDecimal) rs.getObject("FIIDESTATUS"));
        categoria.setIdNegocio(((BigDecimal) rs.getObject("FIIDNEG")));
        return categoria;
    }
}
