/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.response.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.Respuesta;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorServer;
import com.gs.baz.frq.swagger.responses.RespuestaErrorValidationClient;
import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import com.gs.baz.security.access.util.EncryptException;
import com.gs.baz.security.access.util.SecurityJsonReaderUtil;
import com.gs.baz.security.access.util.SecurityUtilsException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@ControllerAdvice
public class ResponseApiHandler implements ResponseBodyAdvice<Object> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Logger logger = LogManager.getLogger();
    @Autowired
    HttpServletRequest httpServletRequest;
    private SecurityJsonReaderUtil securityJsonReaderUtil;

    /**
     *
     * @param mp
     * @param type
     * @return
     */
    @Override
    public boolean supports(MethodParameter mp, Class<? extends HttpMessageConverter<?>> type) {
        return true;
    }

    /**
     *
     * @param t
     * @param mp
     * @param mt
     * @param type
     * @param request
     * @param response
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object t, MethodParameter mp, MediaType mt, Class<? extends HttpMessageConverter<?>> type, ServerHttpRequest request, ServerHttpResponse response) {
        if (t instanceof Respuesta) {
            AccesoAsimetricoDTO accesoAsimetricoDTO = (AccesoAsimetricoDTO) httpServletRequest.getAttribute("rsa_keys");
            try {
                Respuesta respuesta = (Respuesta) t;
                Object result = respuesta.getResultado();
                JsonNode jsonNode = objectMapper.convertValue(result, JsonNode.class);
                if (accesoAsimetricoDTO != null) {
                    List<String> sensibleAttributesOut = (List<String>) httpServletRequest.getAttribute("sensible_attributes_out");
                    securityJsonReaderUtil = new SecurityJsonReaderUtil(accesoAsimetricoDTO, sensibleAttributesOut);
                    jsonNode = securityJsonReaderUtil.encryptJsonNodeValues(jsonNode);
                }
                respuesta.setResultado(jsonNode);
                return respuesta;
            } catch (SecurityUtilsException ex) {
                ResponseApiCodes apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05015;
                String requestID = (String) httpServletRequest.getAttribute("requestID");
                String nameApi = (String) httpServletRequest.getAttribute("nameApi");
                ResponseMessage responseMessage = ResponseMensages.getByStatus(500);
                String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
                RespuestaAPI respuesta = new RespuestaErrorServer(codeError, responseMessage.getMessage(), requestID);
                return respuesta;
            } catch (EncryptException ex) {
                ResponseApiCodes apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05025;
                String requestID = (String) httpServletRequest.getAttribute("requestID");
                String nameApi = (String) httpServletRequest.getAttribute("nameApi");
                ResponseMessage responseMessage = ResponseMensages.getByStatus(500);
                List<ErrorDetalle> erroresDetalles = new ArrayList<>();
                erroresDetalles.add(new ErrorDetalle("El atributo '" + ex.getField() + "' no pudo ser cifrado"));
                String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
                RespuestaAPI respuesta = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
                return respuesta;
            }
        }
        return t;
    }

}
