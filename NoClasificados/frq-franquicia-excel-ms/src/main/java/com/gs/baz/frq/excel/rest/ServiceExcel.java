/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.rest;

import com.gs.baz.frq.excel.dao.ExcelDAOImpl;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/excel")
@Component("FRQServiceExcel")
public class ServiceExcel {

    @Autowired
    @Qualifier("frqExcelDAOImpl")
    private ExcelDAOImpl ExcelDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/reporte/medicion/{disc}/{anio}/{periodo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity readFile(@RequestHeader HttpHeaders headers, @PathVariable("disc") Integer disc, @PathVariable("anio") Integer anio, @PathVariable("periodo") Integer periodo) throws CustomException, IOException {
        File temp;
        try (Workbook reporte = ExcelDAOImpl.generaReporteMedicion7s(disc, anio, periodo)) {
            temp = File.createTempFile("Reporte_Medicion_", ".xlsx");
            try (FileOutputStream fileOut = new FileOutputStream(temp)) {
                reporte.write(fileOut);
            }
        }
        URLConnection urlConnection = temp.toURI().toURL().openConnection();
        MediaType mediaType = MediaType.asMediaType(MimeType.valueOf(urlConnection.getContentType()));
        Resource resource = new ByteArrayResource(StreamUtils.copyToByteArray(urlConnection.getInputStream()));
        headers.set("Content-Disposition", "inline;filename=Reporte_Medicion.xlsx");
        return ResponseEntity.status(HttpStatus.OK).headers(headers).contentType(mediaType).body(resource);
    }

    @RequestMapping(value = "secure/reporte/medicion/excel/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity readFile3(@RequestHeader HttpHeaders headers, @PathVariable("id") Integer id) throws CustomException, IOException {

        String idNeg = "";
        String nombreBase = "Reporte_Disciplina7S_";
        String rutaBase = "C:/franquicia/reporte_medicion/";

        if (id == 1) {
            idNeg = "BAZ";
        } else if (id == 2) {
            idNeg = "EKT";
        } else if (id == 3) {
            idNeg = "PPR";
        } else if (id == 4) {
            idNeg = "GEK";
        } else if (id == 5) {
            idNeg = "GCC";
        } else if (id == 6) {
            idNeg = "CYF";
        }
        String nombre = nombreBase + idNeg + ".xlsx";
        //Directorio del fichero original
        File origen = new File(rutaBase + nombre);

        //Creación de un archivo temporal para realizar el acceso a la información
        File temp = File.createTempFile("Reporte_Disciplina7S_" + idNeg, ".xlsx");

        //Comprobamos que el archivo existe
        if (origen.exists()) {
            try {
                InputStream in = new FileInputStream(origen);
                OutputStream out = new FileOutputStream(temp);
                // Usamos un buffer para la copia.
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        URLConnection urlConnection = temp.toURI().toURL().openConnection();
        MediaType mediaType = MediaType.asMediaType(MimeType.valueOf(urlConnection.getContentType()));
        Resource resource = new ByteArrayResource(StreamUtils.copyToByteArray(urlConnection.getInputStream()));
        headers.set("Content-Disposition", "attachment;filename=Reporte_Disciplina7S_" + idNeg + ".xlsx");
        return ResponseEntity.status(HttpStatus.OK).headers(headers).contentType(mediaType).body(resource);
    }

    @RequestMapping(value = "secure/reporte/medicion/json/{disc}/{anio}/{periodo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Object> readFileJSON(@RequestHeader HttpHeaders headers, @PathVariable("disc") Integer disc, @PathVariable("anio") Integer anio, @PathVariable("periodo") Integer periodo) throws CustomException, IOException {
        return ExcelDAOImpl.generaReporteMedicion7sArray(disc, anio, periodo);
    }

    @RequestMapping(value = "secure/otros", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean procesaOtros() throws CustomException, IOException {
        return ExcelDAOImpl.procesaHtml();
    }

}
