/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.grupo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de documentos del grupo", value = "DocumentosGrupo")
public class DocumentosGrupo {

    @JsonProperty(value = "documentos")
    @ApiModelProperty(notes = "documentos")
    private List<DocumentoGrupo> documentos;

    public DocumentosGrupo(List<DocumentoGrupo> documentos) {
        this.documentos = documentos;
    }

    public List<DocumentoGrupo> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoGrupo> documentos) {
        this.documentos = documentos;
    }

    @Override
    public String toString() {
        return "DocumentosGrupo{" + "documentos=" + documentos + '}';
    }

}
