/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del documento usuario", value = "AltaDocumentoUsuario")
public class AltaDocumentoUsuario {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador del documento usuario", example = "1")
    private Integer id;

    public AltaDocumentoUsuario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AltaGrupoUsuario{" + "id=" + id + '}';
    }

}
