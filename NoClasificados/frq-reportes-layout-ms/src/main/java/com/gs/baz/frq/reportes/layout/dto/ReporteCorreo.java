/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reportes.layout.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ReporteCorreo {

    @JsonProperty(value = "idNegocio")
    private transient String idNegocio;

    @JsonProperty(value = "idFolio", required = true)
    @ApiModelProperty(notes = "Identificador del folio", example = "1", required = true)
    private Integer idFolio;

    @JsonProperty(value = "idCeco", required = true)
    @ApiModelProperty(notes = "Identificador del idCeco", example = "920100", required = true)
    private Integer idCeco;

    @JsonProperty(value = "idProyecto", required = true)
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1", required = true)
    private Integer idProyecto;

    @JsonProperty(value = "idFormato", required = true)
    @ApiModelProperty(notes = "Identificador del Formato", example = "1", required = true)
    private Integer idFormato;

    @JsonProperty(value = "Fecha", required = true)
    @ApiModelProperty(notes = "Fecha del proyecto", example = "23/08/2021", required = true)
    private String fecha;

    @JsonProperty(value = "nombreFormato", required = true)
    @ApiModelProperty(notes = "Nombre del Formato", example = "Operativa", required = true)
    private String nombreFormato;

    @JsonProperty(value = "nombreNegocio", required = true)
    @ApiModelProperty(notes = "Nombre del Negocio", example = "EKT", required = true)
    private String nombreNegocio;

    @JsonProperty(value = "nombreProyecto", required = true)
    @ApiModelProperty(notes = "Nombre del nombreProyecto", example = "Reconstrucciones", required = true)
    private String nombreProyecto;

    @JsonProperty(value = "nombreCeco", required = true)
    @ApiModelProperty(notes = "Nombre del nombreCeco", example = "MEGA LA LUNA", required = true)
    private String nombreCeco;

    @JsonProperty(value = "conteoProyecto", required = true)
    @ApiModelProperty(notes = "Conteo de proyectos por ceco", example = "1", required = true)
    private Integer conteoProyecto;

    @JsonProperty(value = "fechaInicio")
    @ApiModelProperty(notes = " fecha Inicio reporte", example = "23/08/21")
    private String fechaInicio;

    @JsonProperty(value = "fechaFin")
    @ApiModelProperty(notes = " fecha fin reporte", example = "23/08/21")
    private String fechaFin;

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public Integer getConteoProyecto() {
        return conteoProyecto;
    }

    public void setConteoProyecto(Integer conteoProyecto) {
        this.conteoProyecto = conteoProyecto;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

}
