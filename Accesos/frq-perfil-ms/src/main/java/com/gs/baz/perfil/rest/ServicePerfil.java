/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.perfil.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.perfil.dao.PerfilDAOImpl;
import com.gs.baz.perfil.dto.PerfilDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/perfil")
public class ServicePerfil {

    @Autowired
    private PerfilDAOImpl perfilDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PerfilDTO> getPerfiles() throws CustomException {
        return perfilDAOImpl.selectRows();
    }

    /**
     *
     * @param id_perfil
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_perfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilDTO getPerfil(@PathVariable("id_perfil") Long id_perfil) throws CustomException {
        return perfilDAOImpl.selectRow(id_perfil);
    }

    /**
     *
     * @param perfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilDTO putPerfil(@RequestBody PerfilDTO perfilDTO) throws CustomException {
        if (perfilDTO.getIdPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_perfil"));
        }
        if (perfilDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (perfilDTO.getIdEmpleado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_empleado"));
        }
        return perfilDAOImpl.updateRow(perfilDTO);
    }

    /**
     *
     * @param perfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilDTO deletePerfil(@RequestBody PerfilDTO perfilDTO) throws CustomException {
        if (perfilDTO.getIdPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_perfil"));
        }
        return perfilDAOImpl.deleteRow(perfilDTO);
    }

    /**
     *
     * @param perfilDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilDTO postPerfil(@RequestBody PerfilDTO perfilDTO) throws CustomException {

        if (perfilDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (perfilDTO.getIdEmpleado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_empleado"));
        }
        return perfilDAOImpl.insertRow(perfilDTO);
    }

}
