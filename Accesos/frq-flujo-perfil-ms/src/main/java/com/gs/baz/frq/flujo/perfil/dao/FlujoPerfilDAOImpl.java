/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.flujo.perfil.dao;

/**
 *
 * @author jfernandor
 */
import com.gs.baz.frq.flujo.perfil.dto.FlujoPerfilDTO;
import com.gs.baz.frq.flujo.perfil.mprs.FlujoPerfilRowMapper;
import com.gs.baz.frq.flujo.perfil.mprs.FlujoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class FlujoPerfilDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAvailable;
    private DefaultJdbcCall jdbcSelectAssociated;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAFLUJOPER");
        jdbcInsert.withProcedureName("SPINSAFLUJOPERFI");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAFLUJOPER");
        jdbcSelect.withProcedureName("SPGETAFLUJOPERFI");
        jdbcSelect.returningResultSet("PA_CDATOS", new FlujoPerfilRowMapper());

        jdbcSelectAvailable = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAvailable.withSchemaName(schema);
        jdbcSelectAvailable.withCatalogName("PAADMAFLUJOPER");
        jdbcSelectAvailable.withProcedureName("SPGETFLUJOFALTA");
        jdbcSelectAvailable.returningResultSet("PA_CDATOS", new FlujoRowMapper());

        jdbcSelectAssociated = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAssociated.withSchemaName(schema);
        jdbcSelectAssociated.withCatalogName("PAADMAFLUJOPER");
        jdbcSelectAssociated.withProcedureName("SPGETFLUJOASIG");
        jdbcSelectAssociated.returningResultSet("PA_CDATOS", new FlujoRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAFLUJOPER");
        jdbcUpdate.withProcedureName("SPACTAFLUJOPERFI");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAFLUJOPER");
        jdbcDelete.withProcedureName("SPDELAFLUJOPERFI");
    }

    public FlujoPerfilDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJO_PERFIL", entityID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FlujoPerfilDTO> data = (List<FlujoPerfilDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<FlujoPerfilDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJO_PERFIL", null);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<FlujoPerfilDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilDTO> selectRows(Integer entityID, Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJO_PERFIL", entityID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<FlujoPerfilDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilDTO> selectRowsAvailable(Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelectAvailable.execute(mapSqlParameterSource);
        return (List<FlujoPerfilDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilDTO> selectRowsAssociated(Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelectAssociated.execute(mapSqlParameterSource);
        return (List<FlujoPerfilDTO>) out.get("PA_CDATOS");
    }

    public FlujoPerfilDTO insertRow(FlujoPerfilDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFLUJO_PERFIL", null);
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getFlujoDTO().getIdFlujo());
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setIdFlujoPerfil(((BigDecimal) out.get("PA_NRESEJECUCION")));
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Flujo Perfil"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Flujo Perfil"), ex);
        }
    }

    public FlujoPerfilDTO updateRow(FlujoPerfilDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFLUJO_PERFIL", entityDTO.getIdFlujoPerfil());
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Flujo Perfil"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Flujo Perfil"), ex);
        }
    }

    public FlujoPerfilDTO deleteRow(FlujoPerfilDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFLUJO_PERFIL", entityDTO.getIdFlujoPerfil());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Flujo Perfil"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Flujo Perfil"), ex);
        }
    }

}
