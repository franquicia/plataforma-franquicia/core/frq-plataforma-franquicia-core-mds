/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g012;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class ImperdonableDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "porcentaje")
    private Integer porcentaje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pregunta")
    private String pregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "sucursales")
    private Integer sucursales;

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = (porcentaje == null ? null : porcentaje.intValue());
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Integer getSucursales() {
        return sucursales;
    }

    public void setSucursales(BigDecimal sucursales) {
        this.sucursales = (sucursales == null ? null : sucursales.intValue());
    }

    @Override
    public String toString() {
        return "TopImperdonableDTO{" + "porcentaje=" + porcentaje + ", pregunta=" + pregunta + ", sucursales=" + sucursales + '}';
    }

}
