/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del negocio", value = "Negocio")
public class Negocio {

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del negocio", example = "1")
    private Integer idNegocio;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del negocio", example = "Elektra")
    private String descripcion;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Negocio{" + "idNegocio=" + idNegocio + ", descripcion=" + descripcion + '}';
    }

}
