/*
 * Copyright 2004 - 2012 Mirko Nasato and contributors
 *           2016 - 2020 Simon Braconnier and contributors
 *
 * This file is part of JODConverter - Java OpenDocument Converter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.pdf.rest;

import com.gs.baz.frq.utilerias.archivos.convertidor.base64.dto.SalidaArchivoBase64;
import com.gs.baz.frq.utilerias.archivos.convertidor.pdf.dto.ParametrosArchivo;
import com.gs.baz.frq.utilerias.archivos.convertidor.pdf.bi.Convertidor;
import io.swagger.annotations.ApiParam;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.util.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api-local/utilerias/archivos")
public class ConvertidorApi {

    @Autowired
    private Convertidor convertidor;

    @RequestMapping(value = "/convertir/archivo/a/pdf", method = RequestMethod.POST, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity convertirdorPDF(@ApiParam(value = "The input document to convert.", required = true) @RequestParam("archivo") final MultipartFile inputFile, @ApiParam("The custom options to apply to the conversion.") @RequestParam(required = false) final Map<String, String> parameters) throws IOException, HttpMediaTypeNotSupportedException {
        if (inputFile.getContentType().contains("officedocument")) {
            try {
                final HttpHeaders headers = new HttpHeaders();
                String nameFile = FileUtils.getBaseName(inputFile.getOriginalFilename()) + ".pdf";
                headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF.toString()));
                headers.set("Content-Disposition", "inline;filename=\"" + nameFile + "\"");
                System.out.println(inputFile.getInputStream());
                byte[] file = convertidor.convertirdorPDF(inputFile.getInputStream(), "pdf", parameters);
                return ResponseEntity.ok().headers(headers).body(new ByteArrayResource(file));
            } catch (IOException | OfficeException ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
            }
        } else {
            throw new HttpMediaTypeNotSupportedException("'" + inputFile.getContentType() + "', no soportado");
        }
    }

    @RequestMapping(value = "/convertir/a/pdf", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SalidaArchivoBase64 convertirdorPDF(@RequestBody ParametrosArchivo parametrosArchivo) throws OfficeException, IOException, HttpMediaTypeNotSupportedException {
        if (parametrosArchivo.getTipoContenido().contains("officedocument")) {
            SalidaArchivoBase64 salidaArchivoBase64 = new SalidaArchivoBase64();
            String nameFile = new Date().getTime() + ".pdf";
            if (parametrosArchivo.getNombre() == null) {
                parametrosArchivo.setNombre(nameFile);
            } else {
                parametrosArchivo.setNombre(parametrosArchivo.getNombre() + ".pdf");
            }
            byte[] fileArray = Base64.getDecoder().decode(parametrosArchivo.getArchivo());
            InputStream fileInputStream = new ByteArrayInputStream(fileArray);
            salidaArchivoBase64.setNombre(parametrosArchivo.getNombre());
            salidaArchivoBase64.setTipoContenido("application/pdf");
            byte[] resource = convertidor.convertirdorPDF(fileInputStream, "pdf", parametrosArchivo.getParametros());
            String fileBase64 = Base64.getEncoder().encodeToString(resource);
            salidaArchivoBase64.setArchivo(fileBase64);
            return salidaArchivoBase64;
        } else {
            throw new HttpMediaTypeNotSupportedException("'" + parametrosArchivo.getTipoContenido() + "', no soportado");
        }
    }

}
