package com.gs.baz.categoria.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.categoria.dao.CategoriaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.categoria")
public class ConfigCategoria {

    private final Logger logger = LogManager.getLogger();

    public ConfigCategoria() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public CategoriaDAOImpl categoriaDAOImpl() {
        return new CategoriaDAOImpl();
    }
}
