/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PerfilDTO {

    @JsonProperty(value = "id_perfil")
    private Integer idPerfil;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public PerfilDTO() {
    }

    public PerfilDTO(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public PerfilDTO(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(BigDecimal idPerfil) {
        this.idPerfil = (idPerfil == null ? null : idPerfil.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "PerfilDTO{" + "idPerfil=" + idPerfil + ", descripcion=" + descripcion + '}';
    }

}
