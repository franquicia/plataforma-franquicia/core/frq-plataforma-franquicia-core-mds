/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyecto.documentos.mprs;

import com.gs.baz.frq.documento.sucursal.proyecto.documentos.dto.ProyectoDocumentoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ProyectoDocumentoRowMapper implements RowMapper<ProyectoDocumentoBase> {

    private ProyectoDocumentoBase proyectoDocumentoBase;

    @Override
    public ProyectoDocumentoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        proyectoDocumentoBase = new ProyectoDocumentoBase();
        proyectoDocumentoBase.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        return proyectoDocumentoBase;
    }
}
