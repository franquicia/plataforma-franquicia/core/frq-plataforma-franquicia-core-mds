/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.AltaArchivoParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.ArchivoEliminar;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.GeneraFolio;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.InsertaTransaccion;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.ArchivosEliminarLayoutRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionArchivosLayoutDAOImpl extends DefaultDAO {

   
    private DefaultJdbcCall jdbcGeneraFolio;
    private DefaultJdbcCall jdbcobtieneArchivosEliminar;
    private DefaultJdbcCall jdbcEliminaArchivos;
    private DefaultJdbcCall jdbcInsertaArchivos;
    
    private DefaultJdbcCall jdbcInsertaTransaccion;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMLYARCHIVO";
       
        jdbcGeneraFolio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcGeneraFolio.withSchemaName(schema);
        jdbcGeneraFolio.withCatalogName(catalogo);
        jdbcGeneraFolio.withProcedureName("SPINSFLUJO");
        
        jdbcobtieneArchivosEliminar = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcobtieneArchivosEliminar.withSchemaName(schema);
        jdbcobtieneArchivosEliminar.withCatalogName(catalogo);
        jdbcobtieneArchivosEliminar.withProcedureName("SPGETLYARCHIVOS");
        jdbcobtieneArchivosEliminar.returningResultSet("PA_CDATOS", new ArchivosEliminarLayoutRowMapper()); 
        
        jdbcEliminaArchivos = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcEliminaArchivos.withSchemaName(schema);
        jdbcEliminaArchivos.withCatalogName(catalogo);
        jdbcEliminaArchivos.withProcedureName("SPDELLYARCHIVOS");
        
        jdbcInsertaArchivos = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertaArchivos.withSchemaName(schema);
        jdbcInsertaArchivos.withCatalogName(catalogo);
        jdbcInsertaArchivos.withProcedureName("SPINSLYARCHIVOS");
        
        jdbcInsertaTransaccion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertaTransaccion.withSchemaName(schema);
        jdbcInsertaTransaccion.withCatalogName(catalogo);
        jdbcInsertaTransaccion.withProcedureName("SPINSLYTRANSACCIO");

    }

    

    public GeneraFolio generaFolio(Integer ceco, Integer proveedor) throws DataNotInsertedException {
        try {
            GeneraFolio generaFolio = new GeneraFolio();
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CECO", ceco);
            mapSqlParameterSource.addValue("PA_PROVEEDOR", proveedor);
            Map<String, Object> out = jdbcGeneraFolio.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
             int banderaFolio = ((BigDecimal) out.get("PA_BANDERAFOLIO")).intValue();
            Integer respuesta = 0;
            if (success > 0) {
                generaFolio.setBanderaFolio(banderaFolio);
                generaFolio.setIdFolio(success);
        
                
            } else {
            
                throw new DataNotInsertedException();
            }
            return generaFolio;
        } catch (Exception ex) {
            throw ex;
        }
        
    }

     public List<ArchivoEliminar> obtieneArchivosEliminar(Integer idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDARCHIVO", idFolio);
        Map<String, Object> out = jdbcobtieneArchivosEliminar.execute(mapSqlParameterSource);
        return (List<ArchivoEliminar>) out.get("PA_CDATOS");
     
    }
     
     public void eliminaArchivos(Integer idFolio) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_FOLIO", idFolio);
            Map<String, Object> out = jdbcEliminaArchivos.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }
     
     
    public Integer insertRow(AltaArchivoParametros entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FCRUTA", entityDTO.getRuta());
            mapSqlParameterSource.addValue("PA_FITIPO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_FIPESO", entityDTO.getPeso());
            mapSqlParameterSource.addValue("PA_FIESTADO", entityDTO.getIdEstado());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            Map<String, Object> out = jdbcInsertaArchivos.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            int idArchivo = ((BigDecimal) out.get("PA_FIIDARCHIVO")).intValue();
            
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } 
            return idArchivo;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    
    public Integer insertTransaccion(InsertaTransaccion entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_FDFECHA", entityDTO.getFecha());
            mapSqlParameterSource.addValue("PA_FITIPO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            Map<String, Object> out = jdbcInsertaTransaccion.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            int idTransaccion = ((BigDecimal) out.get("PA_FIIDTRANSAC")).intValue();
            
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } 
            return idTransaccion;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
