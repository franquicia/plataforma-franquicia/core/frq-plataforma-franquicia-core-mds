/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.grupo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.Usuario;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del grupo usuario", value = "GrupoUsuario")
public class UsuarioGrupo {

    @JsonProperty(value = "idGrupoUsuario")
    @ApiModelProperty(notes = "Identificador del grupo", example = "1", position = -1)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idGrupoUsuario;

    @JsonProperty(value = "usuario")
    @ApiModelProperty(notes = "Datos del usuario")
    private Usuario usuario;

    public Integer getIdGrupoUsuario() {
        return idGrupoUsuario;
    }

    public void setIdGrupoUsuario(Integer idGrupoUsuario) {
        this.idGrupoUsuario = idGrupoUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "UsuarioGrupo{" + "idGrupoUsuario=" + idGrupoUsuario + ", usuario=" + usuario + '}';
    }

}
