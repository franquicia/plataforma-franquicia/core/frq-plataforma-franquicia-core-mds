package com.gs.baz.frq.modelo.negocio.rutas.init;

import com.gs.baz.frq.modelo.negocio.rutas.dao.RutaCompuestaDAOImpl;
import com.gs.baz.frq.modelo.negocio.rutas.dao.RutaDAOImpl;
import com.gs.baz.frq.modelo.negocio.rutas.dao.RutaNivelDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.rutas")
public class ConfigRuta {

    private final Logger logger = LogManager.getLogger();

    public ConfigRuta() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public RutaDAOImpl rutaDAOImpl() {
        return new RutaDAOImpl();
    }

    @Bean(initMethod = "init")
    public RutaNivelDAOImpl rutaNivelDAOImpl() {
        return new RutaNivelDAOImpl();
    }

    @Bean(initMethod = "init")
    public RutaCompuestaDAOImpl rutaCompuestaDAOImpl() {
        return new RutaCompuestaDAOImpl();
    }
}
