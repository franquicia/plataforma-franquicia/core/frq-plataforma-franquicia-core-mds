/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Cierre del Incidente", value = "CerrarSinOt")
public class CerrarSinOtDTO {

    @JsonProperty(value = "idIncidente", required = true)
    @ApiModelProperty(notes = "Clave del incidente", example = "13007962", position = -1, required = true)
    private Integer idIncidente;

    @JsonProperty(value = "resolucionIncidente", required = true)
    @ApiModelProperty(notes = "Resolución del incidente", example = "ATENDIDO", required = true)
    private String resolucion;

    @JsonProperty(value = "motivoEstado", required = true)
    @ApiModelProperty(notes = "Motivo del estado actual", example = "Rechazado proveedor", required = true)
    private String motivoEstado;

    public CerrarSinOtDTO() {
        super();

    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    @Override
    public String toString() {
        return "CerrarSinOtDTO{" + "idIncidente=" + idIncidente + ", resolucion=" + resolucion
                + ", motivoEstado=" + motivoEstado + '}';
    }

}
