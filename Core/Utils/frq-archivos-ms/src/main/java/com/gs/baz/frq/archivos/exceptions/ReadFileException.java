/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class ReadFileException extends Exception{

    public ReadFileException() {
    }

    public ReadFileException(String string) {
        super(string);
    }

    public ReadFileException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public ReadFileException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
