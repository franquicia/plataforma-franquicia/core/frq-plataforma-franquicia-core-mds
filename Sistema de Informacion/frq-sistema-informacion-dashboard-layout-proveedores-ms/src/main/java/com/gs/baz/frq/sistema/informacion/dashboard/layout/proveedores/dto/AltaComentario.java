/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de firma", value = "IdentificadorAltaArchivo")
public class AltaComentario {


    @JsonProperty(value = "idComentario")
    @ApiModelProperty(notes = "Identificador del comentario", example = "1", position = -2)
    private Integer idComentario;

    public Integer getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(Integer idComentario) {
        this.idComentario = idComentario;
    }

    @Override
    public String toString() {
        return "AltaComentario{" + "idComentario=" + idComentario + '}';
    }

}
