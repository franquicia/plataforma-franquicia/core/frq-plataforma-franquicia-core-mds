package com.gs.baz.frq.depuracion.dao;

import com.gs.baz.frq.depuracion.dto.DepuracionDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DepuracionDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcExecute;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcExecute = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcExecute.withSchemaName(schema);
        jdbcExecute.withCatalogName("PADEPURACION");
        jdbcExecute.withProcedureName("SPBITACORADIA");
    }

    public DepuracionDTO executeStore(DepuracionDTO depuracionDTO) throws CustomException {
        DepuracionDTO salida = new DepuracionDTO();
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FECHA", depuracionDTO.getFecha());
            Map<String, Object> out = jdbcExecute.execute(mapSqlParameterSource);
            salida.setEjecucion((BigDecimal) out.get("PA_EJECUCION"));
            salida.setRespuestas_eliminadas((BigDecimal) out.get("PA_RESELIMIN"));
            salida.setBitacoras_eliminadas((BigDecimal) out.get("PA_ELIMINADAS"));
            return salida;
        } catch (Exception ex) {
            logger.info(ex);
            throw new CustomException(ex);
        }
    }

}
