/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frecuencia.medicion.rest;

import com.gs.baz.frecuencia.medicion.dao.FrecuenciaMedicionDAOImpl;
import com.gs.baz.frecuencia.medicion.dto.FrecuenciaMedicionDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/frecuencia/medicion")
public class ServiceFrecuenciaMedicion {

    @Autowired
    private FrecuenciaMedicionDAOImpl frecuenciaMedicionDAOImpl;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FrecuenciaMedicionDTO> getFrecuenciasMedicion() throws CustomException {
        return frecuenciaMedicionDAOImpl.selectRows();
    }

    /**
     *
     * @param id_frecuencia_medicion
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_frecuencia_medicion}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO getFrecuenciaMedicion(@PathVariable("id_frecuencia_medicion") Long id_frecuencia_medicion) throws CustomException {
        return frecuenciaMedicionDAOImpl.selectRow(id_frecuencia_medicion);
    }

    /**
     *
     * @param frecuenciaMedicionDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO postFrecuenciaMedicion(@RequestBody FrecuenciaMedicionDTO frecuenciaMedicionDTO) throws CustomException {
        if (frecuenciaMedicionDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return frecuenciaMedicionDAOImpl.insertRow(frecuenciaMedicionDTO);
    }

    /**
     *
     * @param frecuenciaMedicionDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO putFrecuenciaMedicion(@RequestBody FrecuenciaMedicionDTO frecuenciaMedicionDTO) throws CustomException {
        if (frecuenciaMedicionDTO.getIdFrecuenciaMedicion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia_medicion"));
        }
        if (frecuenciaMedicionDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return frecuenciaMedicionDAOImpl.updateRow(frecuenciaMedicionDTO);
    }

    /**
     *
     * @param frecuenciaMedicionDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO deleteFrecuenciaMedicion(@RequestBody FrecuenciaMedicionDTO frecuenciaMedicionDTO) throws CustomException {
        return frecuenciaMedicionDAOImpl.deleteRow(frecuenciaMedicionDTO);
    }
}
