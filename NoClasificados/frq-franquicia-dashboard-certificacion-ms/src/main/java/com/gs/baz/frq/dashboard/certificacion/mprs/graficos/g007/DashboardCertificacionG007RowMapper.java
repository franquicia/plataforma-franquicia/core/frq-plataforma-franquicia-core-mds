package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g007;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g007.DashboardCertificacionG007DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG007RowMapper implements RowMapper<DashboardCertificacionG007DTO> {

    private DashboardCertificacionG007DTO dashboardCertificacionG007DTO;

    @Override
    public DashboardCertificacionG007DTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionG007DTO = new DashboardCertificacionG007DTO();
        dashboardCertificacionG007DTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionG007DTO.setPuntoContacto(rs.getString("PUNTO_CONTACTO"));
        dashboardCertificacionG007DTO.setCertificador((BigDecimal) rs.getObject("CERTIFICADOR"));
        dashboardCertificacionG007DTO.setNombreCertificador(rs.getString("NOMBRE_CERTIFICADOR"));
        dashboardCertificacionG007DTO.setIdCecoBase(rs.getString("FICECOBASE_ID"));
        dashboardCertificacionG007DTO.setUltimaCertificacion(rs.getString("FECHA"));
        dashboardCertificacionG007DTO.setHoraEntrada(rs.getString("ENTRADA"));
        dashboardCertificacionG007DTO.setHoraSalida(rs.getString("SALIDA"));
        dashboardCertificacionG007DTO.setIdGerente((BigDecimal) rs.getObject("FIGERENTEID"));
        return dashboardCertificacionG007DTO;
    }
}
