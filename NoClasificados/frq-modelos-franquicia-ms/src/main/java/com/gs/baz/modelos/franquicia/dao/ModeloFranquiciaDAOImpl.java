package com.gs.baz.modelos.franquicia.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.modelos.franquicia.dao.util.GenericModeloFranquiciaDAO;
import com.gs.baz.modelos.franquicia.dto.DisciplinaEjeDTO;
import com.gs.baz.modelos.franquicia.dto.DisciplinaNegocioDTO;
import com.gs.baz.modelos.franquicia.dto.EjeModeloDTO;
import com.gs.baz.modelos.franquicia.dto.EjeModeloNegocioDTO;
import com.gs.baz.modelos.franquicia.dto.ModeloFranquiciaDTO;
import com.gs.baz.modelos.franquicia.dto.NegocioModeloFranquiciaDTO;
import com.gs.baz.modelos.franquicia.dto.ProtocoloDisciplinaDTO;
import com.gs.baz.modelos.franquicia.mprs.ModelosFranquiciaRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cescobarh
 */
@Component
public class ModeloFranquiciaDAOImpl extends DefaultDAO implements GenericModeloFranquiciaDAO<ModeloFranquiciaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcUpdateModelo;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectByName;
    private MapSqlParameterSource mapSqlParameterSource;
    private String schema;

    @Autowired
    @Qualifier("disciplinasEjeModeloFranquiciaDAOImpl")
    private DisciplinasEjeDAOImpl disciplinasEjeDAOImpl;

    @Autowired
    @Qualifier("ejesModeloFranquiciaDAOImpl")
    private EjesModeloDAOImpl ejesModeloDAOImpl;

    @Autowired
    @Qualifier("disciplinasNegociosModeloFranquiciaDAOImpl")
    private DisciplinasNegociosModeloDAOImpl disciplinasNegociosModeloDAOImpl;

    @Autowired
    @Qualifier("protocoloDisciplinaDAOImpl")
    private ProtocoloDisciplinaDAOImpl protocoloDisciplinaDAOImpl;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINMODELO");
        jdbcInsert.withProcedureName("SP_INS_MODELO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINMODELO");
        jdbcUpdate.withProcedureName("SP_ACT_MODELO");

        jdbcUpdateModelo = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdateModelo.withSchemaName(schema);
        jdbcUpdateModelo.withCatalogName("PAADMINMODELO");
        jdbcUpdateModelo.withProcedureName("SP_DEL_MODELO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINMODELO");
        jdbcSelect.withProcedureName("SP_SEL_MODELO");
        jdbcSelect.returningResultSet("RCL_INFO", new ModelosFranquiciaRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMINMODELO");
        jdbcSelectByName.withProcedureName("SP_MODELO_NOMBRE");
        jdbcSelectByName.returningResultSet("RCL_INFO", new ModelosFranquiciaRowMapper());
    }

    @Override
    public ModeloFranquiciaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODELO", entityID);
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ModeloFranquiciaDTO> data = (List<ModeloFranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<ModeloFranquiciaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODELO", null);
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ModeloFranquiciaDTO> data = (List<ModeloFranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public ModeloFranquiciaDTO insertRow(ModeloFranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCNOMBREMODELO", entityDTO.getNombreModelo());
            mapSqlParameterSource.addValue("PA_FIIDEMCREA", entityDTO.getIdEmpleadoCrea());
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getIdFranquicia());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdModelo((BigDecimal) out.get("PA_FIIDMODELO"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  modelo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  modelo"), ex);
        }
    }

    @Override
    public ModeloFranquiciaDTO updateRow(ModeloFranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            mapSqlParameterSource.addValue("PA_FCNOMBREMODELO", entityDTO.getNombreModelo());
            mapSqlParameterSource.addValue("PA_FIIDEMMOD", entityDTO.getIdEmpleadoModifica());
            mapSqlParameterSource.addValue("PA_FIIDFRANQ", entityDTO.getIdFranquicia());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of modelo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of modelo"), ex);
        }
    }

    @Override
    public List<ModeloFranquiciaDTO> selectRows(Long franquiciaID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", franquiciaID);
        mapSqlParameterSource.addValue("PA_FIIDMODELO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ModeloFranquiciaDTO> data = (List<ModeloFranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public ModeloFranquiciaDTO deleteRowModelo(ModeloFranquiciaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDMODELO", entityDTO.getIdModelo());
            mapSqlParameterSource.addValue("PA_FIIDEMMOD", entityDTO.getIdEmpleadoModifica());
            Map<String, Object> out = jdbcUpdateModelo.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete row of modelo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error to delete row of modelo"), ex);
        }
    }

    @Override
    @Transactional(rollbackFor = {CustomException.class})
    public ModeloFranquiciaDTO cudModelo(ModeloFranquiciaDTO entityDTO) throws CustomException {
        try {
            if (entityDTO.getInserted() != null && entityDTO.getInserted() && entityDTO.getIdModelo() == null) {
                entityDTO = this.insertRow(entityDTO);
            }
            if (entityDTO.getUpdated() != null && entityDTO.getUpdated()) {
                entityDTO = this.updateRow(entityDTO);
            }
            if (entityDTO.getDeleted() != null && entityDTO.getDeleted()) {
                entityDTO = this.deleteRowModelo(entityDTO);
            }
            List<EjeModeloDTO> ejesModelo = entityDTO.getEjesModelo();
            List<DisciplinaEjeDTO> disciplinasEje;
            if (ejesModelo != null) {
                for (EjeModeloDTO ejeModeloDTO : ejesModelo) {
                    if (ejeModeloDTO.getInserted() != null && ejeModeloDTO.getInserted() && ejeModeloDTO.getIdModeloEje() == null) {
                        ejeModeloDTO.setIdModelo(new BigDecimal(entityDTO.getIdModelo()));
                        ejeModeloDTO = ejesModeloDAOImpl.insertRow(ejeModeloDTO);
                    }
                    if (ejeModeloDTO.getUpdated() != null && ejeModeloDTO.getUpdated()) {
                        ejeModeloDTO = ejesModeloDAOImpl.updateRow(ejeModeloDTO);
                    }
                    if (ejeModeloDTO.getDeleted() != null && ejeModeloDTO.getDeleted()) {
                        ejeModeloDTO = ejesModeloDAOImpl.deleteRow(ejeModeloDTO);
                    }
                    disciplinasEje = ejeModeloDTO.getDisciplinasEje();
                    if (disciplinasEje != null) {
                        for (DisciplinaEjeDTO disciplinaEjeDTO : disciplinasEje) {
                            if (disciplinaEjeDTO.getInserted() != null && disciplinaEjeDTO.getInserted() && disciplinaEjeDTO.getIdDisciplinaEje() == null) {
                                disciplinaEjeDTO.setIdModeloEje(new BigDecimal(ejeModeloDTO.getIdModeloEje()));
                                disciplinaEjeDTO = disciplinasEjeDAOImpl.insertRow(disciplinaEjeDTO);
                            }
                            if (disciplinaEjeDTO.getUpdated() != null && disciplinaEjeDTO.getUpdated()) {
                                disciplinaEjeDTO = disciplinasEjeDAOImpl.updateRow(disciplinaEjeDTO);
                            }
                            if (disciplinaEjeDTO.getDeleted() != null && disciplinaEjeDTO.getDeleted()) {
                                disciplinaEjeDTO = disciplinasEjeDAOImpl.deleteRow(disciplinaEjeDTO);
                            }
                        }
                    }
                    ejeModeloDTO.setDisciplinasEje(disciplinasEje);
                }
            }
            List<NegocioModeloFranquiciaDTO> negociosModelo = entityDTO.getNegociosModelo();
            List<ProtocoloDisciplinaDTO> protocoloDisciplinaDTO;
            if (negociosModelo != null) {
                for (NegocioModeloFranquiciaDTO negocioModelo : negociosModelo) {
                    if (negocioModelo != null) {
                        int indexEje = 0;
                        for (EjeModeloNegocioDTO ejeModeloNegocioDTO : negocioModelo.getEjesNegocio()) {
                            if (ejeModeloNegocioDTO.getIdModEje() == null && ejesModelo != null && ejesModelo.get(indexEje) != null) {
                                ejeModeloNegocioDTO.setIdModEje(new BigDecimal(ejesModelo.get(indexEje).getIdModeloEje()));
                            }
                            List<DisciplinaNegocioDTO> disciplinasNegocio = ejeModeloNegocioDTO.getDisciplinaEjeDTO();
                            if (disciplinasNegocio != null) {
                                int indexDisciplina = 0;
                                for (DisciplinaNegocioDTO disciplinaNegocioDTO : disciplinasNegocio) {
                                    if (disciplinaNegocioDTO.getIdDisciplinaEje() == null && ejesModelo != null && ejesModelo.get(indexEje) != null && ejesModelo.get(indexEje).getDisciplinasEje() != null && ejesModelo.get(indexEje).getDisciplinasEje().get(indexDisciplina) != null) {
                                        disciplinaNegocioDTO.setIdDisciplinaEje(new BigDecimal(ejesModelo.get(indexEje).getDisciplinasEje().get(indexDisciplina).getIdDisciplinaEje()));
                                    }
                                    if (disciplinaNegocioDTO.getIdDisciplinaNegocio() == null) {
                                        disciplinaNegocioDTO.setIdModelo(new BigDecimal(entityDTO.getIdModelo()));
                                        disciplinaNegocioDTO.setIdNegocio(new BigDecimal(negocioModelo.getIdNegocio()));
                                        disciplinaNegocioDTO = disciplinasNegociosModeloDAOImpl.insertRow(disciplinaNegocioDTO);
                                    }
                                    protocoloDisciplinaDTO = disciplinaNegocioDTO.getProtocolosDisciplinaDTO();
                                    if (protocoloDisciplinaDTO != null) {
                                        for (ProtocoloDisciplinaDTO protocoloDisciplina : protocoloDisciplinaDTO) {
                                            if (protocoloDisciplina.getIdProtocoloDisciplina() == null) {
                                                protocoloDisciplina.setIdDisciplinaNegocio(new BigDecimal(disciplinaNegocioDTO.getIdDisciplinaNegocio()));
                                                protocoloDisciplina = protocoloDisciplinaDAOImpl.insertRow(protocoloDisciplina);
                                            }
                                            if (protocoloDisciplina.getDeleted() != null && protocoloDisciplina.getDeleted()) {
                                                protocoloDisciplina = protocoloDisciplinaDAOImpl.deleteRow(protocoloDisciplina);
                                            }
                                        }
                                    }
                                    disciplinaNegocioDTO.setProtocolosDisciplinaDTO(protocoloDisciplinaDTO);
                                    indexDisciplina++;
                                }
                            }
                            indexEje++;
                        }
                    }
                }
            }
            entityDTO.setEjesModelo(ejesModelo);
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
        return entityDTO;
    }

    @Override
    public List<ModeloFranquiciaDTO> selectRowsByName(String name, Long idFranquicia) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFRANQ", idFranquicia);
        mapSqlParameterSource.addValue("PA_FCNOMBREMODELO", name);
        Map<String, Object> out = jdbcSelectByName.execute(mapSqlParameterSource);
        List<ModeloFranquiciaDTO> data = (List<ModeloFranquiciaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
}
