package com.gs.baz.punto.contacto.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.punto.contacto.dto.TipoFormatoDTO;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper.TipoFormatoRowMapper;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class TipoFormatoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    private PuntoContactoFormatoDAOImpl puntoContactoFormatoDAOImpl;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINTIPFORMATO");
        jdbcSelect.withProcedureName("SP_SEL_TIPOFORM");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoRowMapper().new TipoFormatoRowMapper());
    }

    public List<TipoFormatoDTO> selectRows(Integer idPuntoContactoFranquiciaModelo) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDTIFORM", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<TipoFormatoDTO> tipoFormatos = (List<TipoFormatoDTO>) out.get("RCL_INFO");
        tipoFormatos = tipoFormatos.stream().filter(item -> item.getIdTipoFormato() != 2).collect(Collectors.toList());
        for (TipoFormatoDTO tipoFormato : tipoFormatos) {
            tipoFormato.setPuntoContactoFormatoDTO(puntoContactoFormatoDAOImpl.selectRow(idPuntoContactoFranquiciaModelo, tipoFormato.getIdTipoFormato()));
        }
        return tipoFormatos;
    }

}
