/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.etiqueta.dao.EtiquetaDAOImpl;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.AltaEtiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.Etiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.EtiquetaBase;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.Etiquetas;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.ParametrosEtiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "catalogos-etiqueta", value = "catalogos-etiqueta", description = "Gestión del catalogo de etiquetas")
@RestController
@RequestMapping("/api-local/modelo-negocio/catalogos/etiquetas/v1")
public class EtiquetaApi {

    @Autowired
    private EtiquetaDAOImpl etiquetaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idEtiqueta
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene etiqueta", notes = "Obtiene un etiqueta", nickname = "obtieneEtiqueta")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idEtiqueta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EtiquetaBase obtieneEtiqueta(@ApiParam(name = "idEtiqueta", value = "Identificador del etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Long idEtiqueta) throws CustomException, DataNotFoundException {
        EtiquetaBase etiquetaBase = etiquetaDAOImpl.selectRow(idEtiqueta);
        if (etiquetaBase == null) {
            throw new DataNotFoundException();
        }
        return etiquetaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene etiquetas", notes = "Obtiene todos los etiquetas", nickname = "obtieneEtiquetaes")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Etiquetas obtieneEtiqueta() throws CustomException, DataNotFoundException {
        Etiquetas etiquetas = new Etiquetas(etiquetaDAOImpl.selectAllRows());
        if (etiquetas.getEtiquetas() != null && etiquetas.getEtiquetas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return etiquetas;
    }

    /**
     *
     * @param parametrosEtiqueta
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear etiqueta", notes = "Agrega un etiqueta", nickname = "creaEtiqueta")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaEtiqueta creaEtiqueta(@ApiParam(name = "ParametrosEtiqueta", value = "Paramentros para el alta del etiqueta", required = true) @RequestBody ParametrosEtiqueta parametrosEtiqueta) throws DataNotInsertedException {
        return etiquetaDAOImpl.insertRow(parametrosEtiqueta);
    }

    /**
     *
     * @param parametrosEtiqueta
     * @param idEtiqueta
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar etiqueta", notes = "Actualiza un etiqueta", nickname = "actualizaEtiqueta")
    @RequestMapping(value = "/{idEtiqueta}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaEtiqueta(@ApiParam(name = "ParametrosEtiqueta", value = "Paramentros para la actualización del etiqueta", required = true) @RequestBody ParametrosEtiqueta parametrosEtiqueta, @ApiParam(name = "idEtiqueta", value = "Identificador del etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Integer idEtiqueta) throws DataNotUpdatedException {
        parametrosEtiqueta.setIdEtiqueta(idEtiqueta);
        etiquetaDAOImpl.updateRow(parametrosEtiqueta);
        return new SinResultado();
    }

    /**
     *
     * @param idEtiqueta
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar etiqueta", notes = "Elimina un item de los etiquetas", nickname = "eliminaEtiqueta")
    @RequestMapping(value = "/{idEtiqueta}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaEtiqueta(@ApiParam(name = "idEtiqueta", value = "Identificador del etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Integer idEtiqueta) throws DataNotDeletedException {
        etiquetaDAOImpl.deleteRow(idEtiqueta);
        return new SinResultado();
    }

    /**
     *
     * @param descripcionEtiqueta
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene etiqueta por texto", notes = "Obtiene una etiqueta por texto", nickname = "obtieneEtiquetaTexto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{descripcionEtiqueta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Etiqueta obtieneEtiquetaTexto(@ApiParam(name = "descripcionEtiqueta", value = "Descripcion de la etiqueta", example = "1", required = true) @PathVariable("descripcionEtiqueta") String descripcionEtiqueta) throws CustomException, DataNotFoundException {
        Etiqueta etiquetaBase = etiquetaDAOImpl.selectEtiqueta(descripcionEtiqueta);
        if (etiquetaBase == null) {
            throw new DataNotFoundException();
        }
        return etiquetaBase;
    }

    /**
     *
     * @param idEtiqueta
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene etiquetas relacionadas", notes = "Obtiene todas las etiqueta relacionadas", nickname = "obtieneEtiquetasRelacionadas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idEtiqueta}/relacionadas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Etiquetas obtieneEtiquetasRelacionadas(@ApiParam(name = "idEtiqueta", value = "Identificador del etiqueta", example = "1", required = true) @PathVariable("idEtiqueta") Long idEtiqueta) throws CustomException, DataNotFoundException {
        List<Etiqueta> etiqueta = etiquetaDAOImpl.selectEtiquetasRelacionadas(idEtiqueta);
        Etiquetas etiquetas = new Etiquetas(etiqueta);
        if (etiquetas.getEtiquetas() != null && etiquetas.getEtiquetas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return etiquetas;
    }

    /**
     *
     * @param idFolio
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene etiquetas relacionadas", notes = "Obtiene todas las etiqueta relacionadas", nickname = "obtieneEtiquetasRelacionadasFolios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/relacionadas/folios/{idFolio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Etiquetas obtieneEtiquetasRelacionadasFolios(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        List<Etiqueta> etiqueta = etiquetaDAOImpl.selectEtiquetasRelacionadasFolios(idFolio);
        Etiquetas etiquetas = new Etiquetas(etiqueta);
        if (etiquetas.getEtiquetas() != null && etiquetas.getEtiquetas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return etiquetas;
    }
}
