package com.gs.baz.frq.flujo.mprs;

import com.gs.baz.frq.flujo.dto.FlujoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FlujoRowMapper implements RowMapper<FlujoDTO> {

    private FlujoDTO flujoDTO;

    @Override
    public FlujoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        flujoDTO = new FlujoDTO();
        flujoDTO.setIdFlujo(((BigDecimal) rs.getObject("FIID_FLUJO")));
        flujoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        flujoDTO.setTipoApp(rs.getString("FCTIPOAPP"));
        return flujoDTO;
    }
}
