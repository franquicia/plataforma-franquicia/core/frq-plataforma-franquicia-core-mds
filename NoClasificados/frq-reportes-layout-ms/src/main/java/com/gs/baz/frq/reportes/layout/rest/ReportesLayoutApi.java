/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reportes.layout.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.reportes.layout.dao.ReportesLayoutDAOImpl;
import com.gs.baz.frq.reportes.layout.dto.ReporteExcel;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "reportesLayouts", value = "reportesLayout", description = "Api para la gestión de reportes de Layout")
@RestController
@RequestMapping("/api-local/reportes-layout/v1")
public class ReportesLayoutApi {

    @Autowired
    private ReportesLayoutDAOImpl reportesLayoutDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    @RequestMapping(value = "/excel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteExcel> selectRowReporteExcel(@ApiParam(name = "reporteExcel", value = "Paramentros para traer reporte excel", required = true) @RequestBody ReporteExcel reporteExcel) throws CustomException, DataNotFoundException {

        return (List<ReporteExcel>) reportesLayoutDAOImpl.selectAllReporteExcel(reporteExcel);
    }

    @RequestMapping(value = "/excelError", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReporteExcel> selectRowReporteExcelError(@ApiParam(name = "reporteExcel", value = "Paramentros para traer reporte excel", required = true) @RequestBody ReporteExcel reporteExcel) throws CustomException, DataNotFoundException {

        return (List<ReporteExcel>) reportesLayoutDAOImpl.selectAllReporteExcelError(reporteExcel);
    }

}
