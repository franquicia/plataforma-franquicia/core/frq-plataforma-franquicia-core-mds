/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.EstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.FolioBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FolioRowMapper implements RowMapper<FolioBase> {

    private FolioBase folioBase;

    @Override
    public FolioBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        folioBase = new FolioBase();
        folioBase.setFechaCreacion(rs.getString("FDFECHACREACION"));
        folioBase.setCreador(new Empleado(rs.getInt("FIIDCREADOR")));
        folioBase.setEstado(new EstadoFolio(rs.getInt("FIIDEDODOCTO")));
        folioBase.setIndicePaso(rs.getInt("FIINDICEPASO"));
        folioBase.setFechaModificacion(rs.getString("FDFECHA_MOD"));
        return folioBase;
    }
}
