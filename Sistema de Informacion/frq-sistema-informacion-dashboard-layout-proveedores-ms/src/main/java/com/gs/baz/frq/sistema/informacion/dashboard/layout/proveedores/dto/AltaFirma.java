/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de firma", value = "IdentificadorAltaArchivo")
public class AltaFirma {


    @JsonProperty(value = "idFirmante")
    @ApiModelProperty(notes = "Identificador del firmante", example = "1", position = -2)
    private Integer idFirmante;

    public Integer getIdFirmante() {
        return idFirmante;
    }

    public void setIdFirmante(Integer idFirmante) {
        this.idFirmante = idFirmante;
    }

    @Override
    public String toString() {
        return "AltaFirma{" + "idFirmante=" + idFirmante + '}';
    }

}
