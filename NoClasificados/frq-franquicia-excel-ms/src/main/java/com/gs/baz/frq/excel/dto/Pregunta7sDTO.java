/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class Pregunta7sDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_Pregunta")
    private Integer idPregunta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_serie")
    private Integer numeroSerie;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "critica")
    private Integer critica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "regal_nueve")
    private Integer regal_nueve;

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(BigDecimal idPregunta) {
        this.idPregunta = (idPregunta == null ? null : idPregunta.intValue());
    }

    public Integer getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(BigDecimal numeroSerie) {
        this.numeroSerie = (numeroSerie == null ? null : numeroSerie.intValue());
    }

    public Integer getRegal_nueve() {
        return regal_nueve;
    }

    public void setRegal_nueve(BigDecimal regal_nueve) {
        this.regal_nueve = (regal_nueve == null ? null : regal_nueve.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCritica() {
        return critica;
    }

    public void setCritica(BigDecimal critica) {
        this.critica = (critica == null ? null : critica.intValue());
    }

    @Override
    public String toString() {
        return "Pregunta7sDTO{" + "idPregunta=" + idPregunta + ", numeroSerie=" + numeroSerie + ", descripcion=" + descripcion + ", critica=" + critica + '}';
    }

}
