package com.gs.baz.frq.jobs.scheduler.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class ScheduleJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "id_job")
    private String jobId;

    @JsonProperty(value = "name")
    private String jobName;

    @JsonProperty(value = "group")
    private String jobGroup;

    @JsonProperty(value = "status")
    private String jobStatus;

    @JsonProperty(value = "cron_expression")
    private String cronExpression;

    @JsonProperty(value = "description")
    private String desc;

    @JsonProperty(value = "execution_url")
    private ExecutionURL executionURL;

    @JsonProperty(value = "execution")
    private Execution execution;

    @JsonProperty(value = "notification")
    private Notification notification;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ExecutionURL getExecutionURL() {
        return executionURL;
    }

    public void setExecutionURL(ExecutionURL executionURL) {
        this.executionURL = executionURL;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        return "ScheduleJob{" + "jobId=" + jobId + ", jobName=" + jobName + ", jobGroup=" + jobGroup + ", jobStatus=" + jobStatus + ", cronExpression=" + cronExpression + ", desc=" + desc + ", executionURL=" + executionURL + ", execution=" + execution + ", notification=" + notification + '}';
    }

}
