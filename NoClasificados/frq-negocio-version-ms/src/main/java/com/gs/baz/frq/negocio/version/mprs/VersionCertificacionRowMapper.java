package com.gs.baz.frq.negocio.version.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.negocio.version.dto.NegocioVersionDTO;

/**
 *
 * @author cescobarh
 */
public class VersionCertificacionRowMapper implements RowMapper<NegocioVersionDTO> {

    private NegocioVersionDTO status;

    @Override
    public NegocioVersionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new NegocioVersionDTO();

        status.setIdVersion(rs.getInt("FIID_VERSUP"));
        status.setIdCheckList(rs.getInt("FIID_CHECKLIST"));
        status.setIdNegocio(rs.getInt("FIID_NEGOCIO"));;
        status.setDescripcion(rs.getString("NEGOCIO"));
        status.setAcronimo(rs.getString("FCDESCRIPCION"));
        status.setFecha(rs.getString("FCPERIODO"));

        return status;
    }
}
