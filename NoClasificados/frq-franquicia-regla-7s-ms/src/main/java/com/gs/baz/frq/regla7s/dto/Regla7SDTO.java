/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.regla7s.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class Regla7SDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_clave")
    private Integer idClave;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pregunta_base")
    private Integer preguntaBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "protocolo")
    private Integer protocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ponderacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "incumplimiento")
    private Integer incumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "regla_nueve")
    private Integer reglaNueve;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_disciplina")
    private Integer idNegocioDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "imperdonable")
    private Integer imperdonable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oportunidad")
    private Integer oportunidad;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public Regla7SDTO() {
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getPreguntaBase() {
        return preguntaBase;
    }

    public void setPreguntaBase(BigDecimal preguntaBase) {
        this.preguntaBase = (preguntaBase == null ? null : preguntaBase.intValue());
    }

    public Integer getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(BigDecimal protocolo) {
        this.protocolo = (protocolo == null ? null : protocolo.intValue());
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public Integer getIncumplimiento() {
        return incumplimiento;
    }

    public void setIncumplimiento(BigDecimal incumplimiento) {
        this.incumplimiento = (incumplimiento == null ? null : incumplimiento.intValue());
    }

    public Integer getReglaNueve() {
        return reglaNueve;
    }

    public void setReglaNueve(BigDecimal reglaNueve) {
        this.reglaNueve = (reglaNueve == null ? null : reglaNueve.intValue());
    }

    public Integer getIdNegocioDisciplina() {
        return idNegocioDisciplina;
    }

    public void setIdNegocioDisciplina(BigDecimal idNegocioDisciplina) {
        this.idNegocioDisciplina = (idNegocioDisciplina == null ? null : idNegocioDisciplina.intValue());
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getImperdonable() {
        return imperdonable;
    }

    public void setImperdonable(BigDecimal imperdonable) {
        this.imperdonable = (imperdonable == null ? null : imperdonable.intValue());
    }

    public Integer getOportunidad() {
        return oportunidad;
    }

    public void setOportunidad(BigDecimal oportunidad) {
        this.oportunidad = (oportunidad == null ? null : oportunidad.intValue());
    }

    public Integer getIdClave() {
        return idClave;
    }

    public void setIdClave(BigDecimal idClave) {
        this.idClave = (idClave == null ? null : idClave.intValue());
    }

    @Override
    public String toString() {
        return "Regla7SDTO{" + "idClave=" + idClave + ", preguntaBase=" + preguntaBase + ", protocolo=" + protocolo + ", ponderacion=" + ponderacion + ", incumplimiento=" + incumplimiento + ", reglaNueve=" + reglaNueve + ", idNegocioDisciplina=" + idNegocioDisciplina + ", modulo=" + modulo + ", imperdonable=" + imperdonable + ", oportunidad=" + oportunidad + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
