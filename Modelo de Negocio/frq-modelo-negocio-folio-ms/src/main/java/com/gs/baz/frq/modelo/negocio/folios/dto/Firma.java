/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la firma", value = "Firma")
public class Firma {

    @JsonProperty(value = "idFirma")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificador del la firma", example = "1", position = -1)
    private Integer idFirma;

    @JsonProperty(value = "empleado")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Datos del empleado")
    private Empleado empleado;

    @JsonProperty(value = "idJerarquia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificador de la jeraquia", example = "1")
    private Integer idJerarquia;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1")
    private Integer indice;

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Firma() {
    }

    public Firma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Firma(Empleado empleado) {
        this.empleado = empleado;
    }

    public Firma(Integer idFirma, Empleado empleado, Integer idJerarquia) {
        this.idFirma = idFirma;
        this.empleado = empleado;
        this.idJerarquia = idJerarquia;
    }

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Integer getIdJerarquia() {
        return idJerarquia;
    }

    public void setIdJerarquia(Integer idJerarquia) {
        this.idJerarquia = idJerarquia;
    }

    @Override
    public String toString() {
        return "Firma{" + "idFirma=" + idFirma + ", empleado=" + empleado + ", idJerarquia=" + idJerarquia + '}';
    }

}
