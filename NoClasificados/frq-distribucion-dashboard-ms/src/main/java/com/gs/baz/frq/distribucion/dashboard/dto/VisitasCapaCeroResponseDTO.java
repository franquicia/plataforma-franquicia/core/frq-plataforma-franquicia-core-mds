package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

@ApiModel(description = "Visitas de Capa Cero", value = "VisitasCapaCero")
public class VisitasCapaCeroResponseDTO {

    @JsonProperty(value = "visitasCapaCero")
    @ApiModelProperty(notes = "Indicador de la distribución de las visitas.", example = "1")
    private Integer visitasCaapaCero;

    @JsonProperty(value = "respuestaEjecucion")
    @ApiModelProperty(notes = "Indicador de respuesta de la ejecución.", example = "1")
    private Integer respuestaEjecucion;

    @JsonProperty(value = "envioCorreo")
    @ApiModelProperty(notes = "Indicador del envío de notificación por correo electrónico.", example = "true")
    private Boolean envioCorreo;

    public Integer getVisitasCaapaCero() {
        return visitasCaapaCero;
    }

    public void setVisitasCaapaCero(Integer visitasCaapaCero) {
        this.visitasCaapaCero = visitasCaapaCero;
    }

    public Integer getRespuestaEjecucion() {
        return respuestaEjecucion;
    }

    public void setRespuestaEjecucion(Integer respuestaEjecucion) {
        this.respuestaEjecucion = respuestaEjecucion;
    }

    public Boolean getEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(Boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    @Override
    public String toString() {
        return "VisitasCapaCeroResponseDTO{" + "visitasCaapaCero=" + visitasCaapaCero + ", respuestaEjecucion=" + respuestaEjecucion + ", envioCorreo=" + envioCorreo + '}';
    }

}
