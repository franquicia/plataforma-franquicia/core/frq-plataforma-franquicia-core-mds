/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Detalle de las Aprobaciones", value = "RespuestaDetalleAprobaciones")
public class DetalleAprobacionesResponseDTO {

    @JsonProperty(value = "archivoCierrePrimero")
    @ApiModelProperty(notes = "Archivo Adjunto en Base64", example = "***Cadena de caracteres en Base64***")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String archivoCierreUno;

    @JsonProperty(value = "archivoCierreRechazadoSegundo")
    @ApiModelProperty(notes = "Archivo Adjunto del rechazo Base64", example = "***Cadena de caracteres Base64***")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String archivoCierreRechazado1;

    @JsonProperty(value = "archivoCierreSegundo")
    @ApiModelProperty(notes = "Archivo Adjunto dos en Base64", example = "***Cadena de caracteres Base64***")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64)
    private String archivoCierreDos;

    @JsonProperty(value = "fechaCalificacion")
    @ApiModelProperty(notes = "Fecha en que se da la calificacion incidente", example = "2020-10-22 12:30:00")
    private String fechaCalificacion;

    @JsonProperty(value = "fechaCierre")
    @ApiModelProperty(notes = "Fecha cierre del incidente", example = "2020-10-22 12:30:00")
    private String fechaCierre;

    @JsonProperty(value = "fechaCierreRechazo")
    @ApiModelProperty(notes = "Fecha cierre del rechazo del incidente", example = "2020-10-22 12:30:00")
    private String fechaCierreRechazo;

    @JsonProperty(value = "justificacionCalificacion")
    @ApiModelProperty(notes = "Justificación de la calificacion del incidente",
            example = "Cinco puntos porque el trabajo esta impecable")
    private String justificacionCalificacion;

    @JsonProperty(value = "justificacionCierre")
    @ApiModelProperty(notes = "Justificacion del cierre", example = "El mantenimiento esta realizado")
    private String justificacionCierre;

    @JsonProperty(value = "justificacionCierreRechazado")
    @ApiModelProperty(notes = "Justificación del rechazo de cierre del incidente", example = "No se realizo el trabajo")
    private String justificacionCierreRechazado;

    public DetalleAprobacionesResponseDTO() {
        super();

    }

    public String getJustificacionCierre() {
        return justificacionCierre;
    }

    public void setJustificacionCierre(String justificacionCierre) {
        this.justificacionCierre = justificacionCierre;
    }

    public String getArchivoCierreUno() {
        return archivoCierreUno;
    }

    public void setArchivoCierreUno(String archivoCierreUno) {
        this.archivoCierreUno = archivoCierreUno;
    }

    public String getArchivoCierreDos() {
        return archivoCierreDos;
    }

    public void setArchivoCierreDos(String archivoCierreDos) {
        this.archivoCierreDos = archivoCierreDos;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getJustificacionCierreRechazado() {
        return justificacionCierreRechazado;
    }

    public void setJustificacionCierreRechazado(String justificacionCierreRechazado) {
        this.justificacionCierreRechazado = justificacionCierreRechazado;
    }

    public String getArchivoCierreRechazado1() {
        return archivoCierreRechazado1;
    }

    public void setArchivoCierreRechazado1(String archivoCierreRechazado1) {
        this.archivoCierreRechazado1 = archivoCierreRechazado1;
    }

    public String getFechaCierreRechazo() {
        return fechaCierreRechazo;
    }

    public void setFechaCierreRechazo(String fechaCierreRechazo) {
        this.fechaCierreRechazo = fechaCierreRechazo;
    }

    public String getJustificacionCalificacion() {
        return justificacionCalificacion;
    }

    public void setJustificacionCalificacion(String justificacionCalificacion) {
        this.justificacionCalificacion = justificacionCalificacion;
    }

    public String getFechaCalificacion() {
        return fechaCalificacion;
    }

    public void setFechaCalificacion(String fechaCalificacion) {
        this.fechaCalificacion = fechaCalificacion;
    }

}
