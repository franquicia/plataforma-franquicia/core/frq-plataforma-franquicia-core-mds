package com.gs.baz.frq.documento.sucursal.documentos.init;

import com.gs.baz.frq.documento.sucursal.documentos.dao.DoucmentoSucursalDocumentoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.documentos")
public class ConfigDocumentoSucursalDocumento {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalDocumento() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DoucmentoSucursalDocumentoDAOImpl documentoSucursalDocumentoDAOImpl() {
        return new DoucmentoSucursalDocumentoDAOImpl();
    }

}
