package com.gs.baz.frq.respuesta.pregunta.visita.mprs;

import com.gs.baz.frq.respuesta.pregunta.visita.dto.RespuestaPreguntaVisitaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RespuestaPreguntaVisitaRowMapper implements RowMapper<RespuestaPreguntaVisitaDTO> {

    private RespuestaPreguntaVisitaDTO respuestaPreguntaVisitaDTO;

    @Override
    public RespuestaPreguntaVisitaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        respuestaPreguntaVisitaDTO = new RespuestaPreguntaVisitaDTO();
        respuestaPreguntaVisitaDTO.setIdRespuestaVisita(((BigDecimal) rs.getObject("FIRESPVISITA_ID")));
        respuestaPreguntaVisitaDTO.setIdVisita((BigDecimal) rs.getObject("FIVISITA_ID"));
        respuestaPreguntaVisitaDTO.setIdPregunta((BigDecimal) rs.getObject("FIPREGUNTA_ID"));
        respuestaPreguntaVisitaDTO.setPregunta(rs.getString("FCPREGUNTA"));
        respuestaPreguntaVisitaDTO.setRespuesta(rs.getString("FCRESPUESTA"));
        respuestaPreguntaVisitaDTO.setIdNegocioDisciplina((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        respuestaPreguntaVisitaDTO.setPonderacion((BigDecimal) rs.getObject("FIPONDERACION"));
        respuestaPreguntaVisitaDTO.setModulo(rs.getString("FCMODULO"));
        respuestaPreguntaVisitaDTO.setIncumplimiento((BigDecimal) rs.getObject("FINCUMPLIMIENTO"));
        respuestaPreguntaVisitaDTO.setIdChecklist((BigDecimal) rs.getObject("FIID_CHECKLIST"));
        respuestaPreguntaVisitaDTO.setImperdonable((BigDecimal) rs.getObject("FIMPERDONABLE"));
        respuestaPreguntaVisitaDTO.setReglaNueve((BigDecimal) rs.getObject("FIREGLANUEVE"));
        return respuestaPreguntaVisitaDTO;
    }
}
