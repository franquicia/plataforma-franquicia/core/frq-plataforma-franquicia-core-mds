/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.gs.baz.frq.archivos.exceptions;

/**
 *
 * @author cescobarh
 */
public class WriteFileException extends Exception{

    public WriteFileException() {
    }

    public WriteFileException(String string) {
        super(string);
    }

    public WriteFileException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public WriteFileException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
