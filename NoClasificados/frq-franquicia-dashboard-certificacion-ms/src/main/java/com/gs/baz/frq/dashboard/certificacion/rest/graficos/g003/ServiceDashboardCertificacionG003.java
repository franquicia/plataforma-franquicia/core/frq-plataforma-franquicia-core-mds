/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g003;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g003.DashboardCertificacionG003DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g003.DashboardCertificacionG003DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g003")
public class ServiceDashboardCertificacionG003 {

    @Autowired
    private DashboardCertificacionG003DAOImpl dashboardCertificacionG003DAOImpl;

    @RequestMapping(value = "secure/grafico/get/circulo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG003DTO> getDatosOdometroG002() throws CustomException {

        return dashboardCertificacionG003DAOImpl.selectRowsDashboardCertificacionG003();
    }

    @RequestMapping(value = "secure/grafico/get/circulo/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG003DTO> getDatoOdometroG002(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @RequestParam(value = "idGerente", required = false) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio) throws CustomException {
        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFin == null || fechaFin.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFin"));
        }
        return dashboardCertificacionG003DAOImpl.selectRowDashboardCertificacionG003(fechaInicial, fechaFin, idGerente, idNegocio);
    }
}
