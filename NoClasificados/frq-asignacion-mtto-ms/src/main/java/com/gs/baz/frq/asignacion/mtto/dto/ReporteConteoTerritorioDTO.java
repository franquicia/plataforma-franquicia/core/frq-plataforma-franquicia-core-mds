/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReporteConteoTerritorioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteoRealizadas")
    private Integer conteoRealizadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteoProgramadas")
    private Integer conteoProgramadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idZona")
    private Integer idZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idTerritorio")
    private Integer idTerritorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreResponsable")
    private String nombreResponsable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semana")
    private Integer semana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreJefeZona")
    private String nombreJefeZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idJefeZona")
    private Integer idJefeZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idResponsable")
    private Integer idResponsable;

    public Integer getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(Integer idResponsable) {
        this.idResponsable = idResponsable;
    }

    public Integer getIdJefeZona() {
        return idJefeZona;
    }

    public void setIdJefeZona(Integer idJefeZona) {
        this.idJefeZona = idJefeZona;
    }

    public String getNombreJefeZona() {
        return nombreJefeZona;
    }

    public void setNombreJefeZona(String nombreJefeZona) {
        this.nombreJefeZona = nombreJefeZona;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getSemana() {
        return semana;
    }

    public void setSemana(Integer semana) {
        this.semana = semana;
    }

    public String getNombreResponsable() {
        return nombreResponsable;
    }

    public void setNombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
    }

    public Integer getConteoRealizadas() {
        return conteoRealizadas;
    }

    public void setConteoRealizadas(Integer conteoRealizadas) {
        this.conteoRealizadas = conteoRealizadas;
    }

    public Integer getConteoProgramadas() {
        return conteoProgramadas;
    }

    public void setConteoProgramadas(Integer conteoProgramadas) {
        this.conteoProgramadas = conteoProgramadas;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(Integer idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    @Override
    public String toString() {
        return "ReporteConteoTerritorioDTO [conteoRealizadas=" + conteoRealizadas + ", conteoProgramadas="
                + conteoProgramadas + ", zona=" + zona + ", idZona=" + idZona + ", fechaInicio=" + fechaInicio
                + ", fechaFin=" + fechaFin + ", idTerritorio=" + idTerritorio + ", territorio=" + territorio
                + ", nombreResponsable=" + nombreResponsable + ", idCeco=" + idCeco + ", semana=" + semana
                + ", idUsuario=" + idUsuario + "]";
    }

}
