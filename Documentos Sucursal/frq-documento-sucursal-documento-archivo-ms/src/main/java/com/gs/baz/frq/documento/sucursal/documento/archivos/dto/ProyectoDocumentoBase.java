/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de ProyectoDocumento", value = "ProyectoBase")
public class ProyectoDocumentoBase {

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "identificador del proyecto", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "identificador del documento", example = "1")
    private Integer idDocumento;

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }
}
