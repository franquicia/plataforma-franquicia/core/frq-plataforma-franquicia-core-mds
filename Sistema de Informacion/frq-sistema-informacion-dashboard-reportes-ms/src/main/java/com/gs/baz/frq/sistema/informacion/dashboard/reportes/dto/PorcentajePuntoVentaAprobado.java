/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class PorcentajePuntoVentaAprobado {

    @JsonProperty(value = "aprobados")
    @ApiModelProperty(notes = "Total de puntos de venta aprobados", example = "90")
    private Integer aprobados;

    @JsonProperty(value = "porcentaje")
    @ApiModelProperty(notes = "Porcentaje de puntos de venta", example = "90")
    private Integer porcentaje;

    public Integer getAprobados() {
        return aprobados;
    }

    public void setAprobados(Integer aprobados) {
        this.aprobados = aprobados;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public String toString() {
        return "PuntoVentaAprobado{" + "aprobados=" + aprobados + ", porcentaje=" + porcentaje + '}';
    }

}
