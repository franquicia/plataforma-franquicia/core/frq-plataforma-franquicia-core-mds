/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.mprs;

import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.Grupo;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.GrupoUsuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class GrupoUsuarioRowMapper implements RowMapper<GrupoUsuario> {

    private GrupoUsuario grupoUsuario;

    @Override
    public GrupoUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
        grupoUsuario = new GrupoUsuario();
        grupoUsuario.setIdGrupoUsuario(rs.getInt("FIIDGPOUSUA"));
        grupoUsuario.setGrupo(new Grupo(rs.getInt("FIIDGRUPO"), rs.getString("FCDESCRIPCION")));
        return grupoUsuario;
    }
}
