/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.client.bi;

import com.gs.baz.frq.modelo.negocio.firmas.client.dto.ParametrosEstadoFolio;
import com.gs.baz.frq.modelo.negocio.firmas.dto.ParametrosCorreoFirma;
import java.math.BigDecimal;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Component
public class FoliosBI {

    @Autowired
    private RestTemplate restTemplate;

    public void putEstadoFolio(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio, Integer idEstadoFolio) throws Exception {
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folios/v1/" + idFolio + "/estados";
        ParametrosEstadoFolio parametrosEstadoFolio = new ParametrosEstadoFolio(idEstadoFolio);
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(parametrosEstadoFolio, httpHeaders);
            restTemplate.exchange(basePath, HttpMethod.PUT, httpEntityUsuario, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        }
    }

    public void putEstadoCorreo(HttpServletRequest httpServletRequest, HttpHeaders httpHeaders, Integer idFolio, Map<String, BigDecimal> resultado) throws Exception {
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + "/plataforma-franquicia-core/api-local/modelo-negocio/folios/v1/" + idFolio + "/estados/correo";
        ParametrosCorreoFirma parametrosEnviaCorreo = new ParametrosCorreoFirma();
        Integer numeroEmpleado = (resultado.get("EMPLEADO_SIGUIENTE") != null) ? resultado.get("EMPLEADO_SIGUIENTE").intValue() : 0;
        Integer estadoFolio = resultado.get("ESTADO_FOLIO").intValue();

        parametrosEnviaCorreo.setNumeroEmpleado(numeroEmpleado);
        parametrosEnviaCorreo.setIdEstadoFolio(estadoFolio);
        try {
            final HttpEntity<Object> httpEntityUsuario = new HttpEntity<>(parametrosEnviaCorreo, httpHeaders);
            restTemplate.exchange(basePath, HttpMethod.PUT, httpEntityUsuario, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            String out = ex.getResponseBodyAsString();
            System.out.println(out);
            throw new Exception(ex);
        }
    }
}
