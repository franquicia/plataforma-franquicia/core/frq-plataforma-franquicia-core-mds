/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g016;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class CecoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rangos")
    private List<RangoDTO> rangos;

    public CecoDTO() {
    }

    public CecoDTO(Integer idCeco, String descripcion) {
        this.idCeco = idCeco;
        this.descripcion = descripcion;
    }

    public CecoDTO(Integer idCeco, String descripcion, List<RangoDTO> rangos) {
        this.idCeco = idCeco;
        this.descripcion = descripcion;
        this.rangos = rangos;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<RangoDTO> getRangos() {
        return rangos;
    }

    public void setRangos(List<RangoDTO> rangos) {
        this.rangos = rangos;
    }

    @Override
    public String toString() {
        return "CecoDTO{" + "idCeco=" + idCeco + ", descripcion=" + descripcion + ", rangos=" + rangos + '}';
    }

}
