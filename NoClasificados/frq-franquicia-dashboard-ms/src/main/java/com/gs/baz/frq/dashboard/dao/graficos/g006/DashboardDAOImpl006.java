package com.gs.baz.frq.dashboard.dao.graficos.g006;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g006.NegocioDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g006.CumplimientoPuntajeNegocioNacionalRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl006 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCumplimientoPuntajeNacionalNegocio;
    private final Logger logger = LogManager.getLogger();
    private String schema;
    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCumplimientoPuntajeNacionalNegocio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoPuntajeNacionalNegocio.withSchemaName(schema);
        jdbcSelectCumplimientoPuntajeNacionalNegocio.withCatalogName("PACONSDASHBOARD");
        jdbcSelectCumplimientoPuntajeNacionalNegocio.withProcedureName("SPCUMPUNTNACNEG");
        jdbcSelectCumplimientoPuntajeNacionalNegocio.returningResultSet("RCL_INFO", new CumplimientoPuntajeNegocioNacionalRowMapper());
    }

    public List<NegocioDTO> selectRowsCumplimientoPuntajeNegocioNacional(NegocioDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();

        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoPuntajeNacionalNegocio.execute(mapSqlParameterSource);
        return (List<NegocioDTO>) out.get("RCL_INFO");
    }

}
