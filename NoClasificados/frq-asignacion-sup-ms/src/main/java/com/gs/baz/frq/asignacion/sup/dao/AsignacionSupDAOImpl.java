package com.gs.baz.frq.asignacion.sup.dao;

import com.gs.baz.frq.asignacion.sup.dto.AsignacionSupDTO;
import com.gs.baz.frq.asignacion.sup.mprs.AsignacionCertificacionRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class AsignacionSupDAOImpl extends DefaultDAO {

    /*
    
    private DefaultJdbcCall jdbcInsertNegocioSup;
    private DefaultJdbcCall jdbcUpdateNegocioSup;
    private DefaultJdbcCall jdbcDeleteNegocioSup;
    private DefaultJdbcCall jdbcObtieneNegocioSup;
    
    private DefaultJdbcCall jdbcInsertVersionSup;
    private DefaultJdbcCall jdbcUpdatetVersionSup;
    private DefaultJdbcCall jdbcDeletetVersionSup;
    private DefaultJdbcCall jdbcObtienetVersionSup;
     */
    private DefaultJdbcCall jdbcInsertAsignacion;
    private DefaultJdbcCall jdbcInsertCargaAsignacion;
    private DefaultJdbcCall jdbcDesactivaAsignacion;
    private DefaultJdbcCall jdbcEliminaAsignacion;
    private DefaultJdbcCall jdbcObtieneAsignacion;

    private DefaultJdbcCall jdbcEliminaAsignacionGeo;
    private DefaultJdbcCall jdbcInsertAsignacionGeo;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        /*
        jdbcInsertNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertNegocioSup.withSchemaName(schema);
        jdbcInsertNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcInsertNegocioSup.withProcedureName("SP_INS_NEGOSUP");

        jdbcUpdateNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateNegocioSup.withSchemaName(schema);
        jdbcUpdateNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcUpdateNegocioSup.withProcedureName("SP_ACT_NEGOSUP");

        jdbcDeleteNegocioSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteNegocioSup.withSchemaName(schema);
        jdbcDeleteNegocioSup.withCatalogName("PAVERNEGOCERTI");
        jdbcDeleteNegocioSup.withProcedureName("SP_DEL_NEGO");

        //version
        jdbcInsertVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertVersionSup.withSchemaName(schema);
        jdbcInsertVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcInsertVersionSup.withProcedureName("SP_INS_VERSUP");
 
        jdbcUpdatetVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdatetVersionSup.withSchemaName(schema);
        jdbcUpdatetVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcUpdatetVersionSup.withProcedureName("SP_ACT_VERSUP");
        
       
        
        jdbcObtienetVersionSup = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtienetVersionSup.withSchemaName(schema);
        jdbcObtienetVersionSup.withCatalogName("PAVERNEGOCERTI");
        jdbcObtienetVersionSup.withProcedureName("SP_SELVERSUP");
        jdbcObtienetVersionSup.returningResultSet("RCL_ASIGN", new VersionCertificacionRowMapper());

         */
        //  jdbcObtieneDatosCalendario.returningResultSet("RCL_SOFT", new SoftHistoCalendarioRowMapper());
        jdbcInsertAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertAsignacion.withSchemaName(schema);
        jdbcInsertAsignacion.withCatalogName("PAASIGN_SUP");
        jdbcInsertAsignacion.withProcedureName("SP_INS_ASIGNSUP");

        jdbcInsertCargaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertCargaAsignacion.withSchemaName(schema);
        jdbcInsertCargaAsignacion.withCatalogName("PAASIGN_SUP");
        jdbcInsertCargaAsignacion.withProcedureName("SP_CARGAPROCESO");

        jdbcDesactivaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDesactivaAsignacion.withSchemaName(schema);
        jdbcDesactivaAsignacion.withCatalogName("PAASIGN_SUP");
        jdbcDesactivaAsignacion.withProcedureName("SP_DESACTIVAPROCESO");

        jdbcEliminaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcEliminaAsignacion.withSchemaName(schema);
        jdbcEliminaAsignacion.withCatalogName("PAASIGN_SUP");
        jdbcEliminaAsignacion.withProcedureName("SP_DES_ASIGNSUP");

        jdbcObtieneAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneAsignacion.withSchemaName(schema);
        jdbcObtieneAsignacion.withCatalogName("PAASIGN_SUP");
        jdbcObtieneAsignacion.withProcedureName("SP_SEL_ASIGNA");
        jdbcObtieneAsignacion.returningResultSet("RCL_ASIGNA", new AsignacionCertificacionRowMapper());

        jdbcInsertAsignacionGeo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertAsignacionGeo.withSchemaName(schema);
        jdbcInsertAsignacionGeo.withCatalogName("PAADM_ASIGNGEO");
        jdbcInsertAsignacionGeo.withProcedureName("SPINS_ASIGNGEO");

        jdbcEliminaAsignacionGeo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcEliminaAsignacionGeo.withSchemaName(schema);
        jdbcEliminaAsignacionGeo.withCatalogName("PAADM_ASIGNGEO");
        jdbcEliminaAsignacionGeo.withProcedureName("SPDEL_ASIGNGEO");

    }

    /*
    public int insertRow(AsignacionSupDTO entityDTO) throws CustomException {
        try {
        	
      int error=0;
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_ACRONIMO", entityDTO.getAcronimo());
           
            Map<String, Object> out = jdbcInsertNegocioSup.execute(mapSqlParameterSource);
           
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

    		BigDecimal resultado =(BigDecimal) out.get("PA_EJECUCION");
    		error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

               
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  negocio"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  negocio"), ex);
        }
    }
    
    public AsignacionSupDTO updateRow(AsignacionSupDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getStatus());
            mapSqlParameterSource.addValue("PA_DESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_ACRONIMO", entityDTO.getAcronimo());

            Map<String, Object> out = jdbcUpdateNegocioSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of negocio"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of negocio"), ex);
        }
    }
    
    public int eliminaRows(AsignacionSupDTO entityDTO) throws CustomException {
        try {
        	int error=0;
            mapSqlParameterSource = new MapSqlParameterSource();
            
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
        
            Map<String, Object> out = jdbcDeleteNegocioSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado =(BigDecimal) out.get("PA_EJECUCION");
    		error = resultado.intValue();

            if (success) {
               
                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of negocio"));
                
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of negocio"), ex);
            
        }
    }


//trae negocios tabla negociossup
   
   
    //version
    public int insertVersionSup(AsignacionSupDTO entityDTO) throws CustomException {
        try {
        	
      int error=0;
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CHEKLIST", entityDTO.getIdCheckList());
            mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_OBSERVACION", entityDTO.getDescripcion());
            
           
            Map<String, Object> out = jdbcInsertVersionSup.execute(mapSqlParameterSource);
           
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

    		BigDecimal resultado =(BigDecimal) out.get("PA_EJECUCION");
    		error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

               
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  version sup"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  version sup"), ex);
        }
    }
    
    public AsignacionSupDTO updateVersionSup(AsignacionSupDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CHEKLIST", entityDTO.getIdCheckList());
            mapSqlParameterSource.addValue("PA_NEGOCIO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_IDVERS", entityDTO.getIdVersion());

            Map<String, Object> out = jdbcUpdatetVersionSup.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of version sup"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of version sup"), ex);
        }
    }
    
    
   
    }

    public List<AsignacionSupDTO >selectVersionesSup(AsignacionSupDTO entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_NEGO", entityID.getIdNegocio());
        Map<String, Object> out = jdbcObtienetVersionSup.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
		List<AsignacionSupDTO> data = (List<AsignacionSupDTO>) out.get("RCL_VERS");
        if (data.size() > 0) {
            return data ;
        } else {
            return null;
        }
    }
     */
    //Asignacion
    public int insertAsignacion(AsignacionSupDTO entityDTO) throws CustomException {
        try {
            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDUSUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_IDPERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_IDNEGO", entityDTO.getIdNegocio());
            mapSqlParameterSource.addValue("PA_GERENTE", entityDTO.getIdGerente());

            Map<String, Object> out = jdbcInsertAsignacion.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  asignacion sup"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  asignacion sup"), ex);
        }
    }

    public int cargaAsignacion() throws CustomException {
        try {
            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            Map<String, Object> out = jdbcInsertCargaAsignacion.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  carga asignacion sup"));
            }
        } catch (Exception ex) {

            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  carga asignacion sup = " + ex.getMessage()), ex);
        }
    }

    public int desactivaAsignaciones() throws CustomException {
        try {
            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            Map<String, Object> out = jdbcDesactivaAsignacion.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  desactiva asignacion sup"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  desactiva asignacion sup"), ex);
        }
    }

    public int insertaAsignacionGeo(String ceco, String nombre, String territorio, String zona) throws CustomException {

        try {
            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", ceco);
            mapSqlParameterSource.addValue("PA_FCNOMBRE", nombre);
            mapSqlParameterSource.addValue("PA_FCTERRITORIO", territorio);
            mapSqlParameterSource.addValue("PA_FCZONA", zona);

            Map<String, Object> out = jdbcInsertAsignacionGeo.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  asignacion sup geo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  asignacion sup geo"), ex);
        }
    }

    public int eliminaAsignacionGeo(String ceco) throws CustomException {

        try {

            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", ceco);

            Map<String, Object> out = jdbcEliminaAsignacionGeo.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error not success to delete row of  asignacion sup geo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error to delete row of  asignacion sup geo"), ex);
        }

    }

    public int eliminaAsignacion(AsignacionSupDTO entityDTO) throws CustomException {
        try {

            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_IDUSUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

            Map<String, Object> out = jdbcEliminaAsignacion.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to desactiva row of asignacion sup"));

            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to desactiva row of asignacion sup"), ex);

        }
    }

    public List<AsignacionSupDTO> selectAsignacion(AsignacionSupDTO entityID) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDPERFIL", entityID.getIdPerfil());
        Map<String, Object> out = jdbcObtieneAsignacion.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<AsignacionSupDTO> data = (List<AsignacionSupDTO>) out.get("RCL_ASIGNA");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    /*

    //fases proyecto
    @SuppressWarnings("unchecked")
    public  Map<String, Object> selectCalendarioRows(AsignacionSupDTO entityDTO) throws CustomException {
    	int error=0;
    	Map<String, Object> out = null;
		Map<String, Object> lista = null;
		List<AsignacionSupDTO> listaAsignaciones = null;
		List<AsignacionSupDTO> listaSoftHistorico = null;
        mapSqlParameterSource = new MapSqlParameterSource();
        //dia mes año
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getFecha());  
        out = jdbcObtieneDatosCalendario.execute(mapSqlParameterSource);
        
        listaAsignaciones = (List<AsignacionSupDTO>) out.get("RCL_ASIGN");
        listaSoftHistorico = (List<AsignacionSupDTO>) out.get("RCL_SOFT");
        
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
		error = resultado.intValue();
		if (error != 1) {
			logger.info("No se cargo la informacion Correctamente");
			return null;
		}
        lista = new HashMap<String, Object>();
        lista.put("listaAsignaciones", listaAsignaciones);
        lista.put("listaSoftHistorico", listaSoftHistorico);
        
        return lista;
    }
     */

}
