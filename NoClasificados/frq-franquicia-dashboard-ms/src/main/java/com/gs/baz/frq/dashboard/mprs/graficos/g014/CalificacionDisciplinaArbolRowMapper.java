package com.gs.baz.frq.dashboard.mprs.graficos.g014;

import com.gs.baz.frq.dashboard.dto.graficos.g014.CalificacionDisciplinaArbolDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CalificacionDisciplinaArbolRowMapper implements RowMapper<CalificacionDisciplinaArbolDTO> {

    private CalificacionDisciplinaArbolDTO calificacionDisciplinaArbolDTO;

    @Override
    public CalificacionDisciplinaArbolDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        calificacionDisciplinaArbolDTO = new CalificacionDisciplinaArbolDTO();
        calificacionDisciplinaArbolDTO.setIdDisciplina((BigDecimal) rs.getObject("FIDISCIPLINA_ID"));
        calificacionDisciplinaArbolDTO.setResultado((BigDecimal) rs.getObject("RESULTADO"));
        return calificacionDisciplinaArbolDTO;
    }
}
