/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author carlos
 */
public class ParametrosBusquedaUsuario {

    @JsonProperty(value = "usuario")
    @ApiModelProperty(notes = "Dato de busqueda de usuario", example = "189140 PEDRO PEREZ GOMEZ", required = true)
    @NotEmpty
    @Size(min = 3, max = 20)
    private String usuario;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
