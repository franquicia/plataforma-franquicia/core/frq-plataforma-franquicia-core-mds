/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de folios por busqueda del modelo de negocio", value = "BusquedaFoliosUsuario")
public class BusquedaFoliosAutorizacion {

    @JsonProperty(value = "folios")
    @ApiModelProperty(notes = "folios")
    private List<BusquedaFolioAutorizacion> folios;

    public BusquedaFoliosAutorizacion(List<BusquedaFolioAutorizacion> folios) {
        this.folios = folios;
    }

    public List<BusquedaFolioAutorizacion> getFolios() {
        return folios;
    }

    public void setFolios(List<BusquedaFolioAutorizacion> folios) {
        this.folios = folios;
    }

    @Override
    public String toString() {
        return "BusquedaFoliosEtiqueta{" + "folios=" + folios + '}';
    }

}
