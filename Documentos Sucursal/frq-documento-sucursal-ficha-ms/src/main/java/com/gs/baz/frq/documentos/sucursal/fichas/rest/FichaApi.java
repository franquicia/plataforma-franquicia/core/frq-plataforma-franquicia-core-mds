/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.rest;

import com.gs.baz.frq.documentos.sucursal.fichas.dao.DocumentoSucursalFichaDAOImpl;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.AltaFicha;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.FichaBase;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.Fichas;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.ParametrosFicha;
import com.gs.baz.frq.documentos.sucursal.fichas.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "fichas", value = "fichas", description = "Api para la gestión del catalogo de fichas")
@RestController
@RequestMapping("/api-local/documento-sucursal/ficha/v1")
public class FichaApi {

    @Autowired
    private DocumentoSucursalFichaDAOImpl fichaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFicha
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ficha", notes = "Obtiene un ficha", nickname = "obtieneFicha")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFicha}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FichaBase obtieneFicha(@ApiParam(name = "idFicha", value = "Identificador del ficha", example = "1", required = true) @PathVariable("idFicha") Long idFicha) throws CustomException, DataNotFoundException {
        FichaBase fichaBase = fichaDAOImpl.selectRow(idFicha);
        if (fichaBase == null) {
            throw new DataNotFoundException();
        }
        return fichaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene fichas", notes = "Obtiene todos los fichas", nickname = "obtieneFichas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Fichas obtieneFichas() throws CustomException, DataNotFoundException {
        Fichas fichas = new Fichas(fichaDAOImpl.selectAllRows());
        if (fichas.getFichas() != null && fichas.getFichas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return fichas;
    }

    /**
     *
     * @param parametrosFicha
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear ficha", notes = "Agrega un ficha", nickname = "creaFicha")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFicha creaFicha(@ApiParam(name = "ParametrosFicha", value = "Paramentros para el alta del ficha", required = true) @RequestBody ParametrosFicha parametrosFicha) throws DataNotInsertedException {
        return fichaDAOImpl.insertRow(parametrosFicha);
    }

    /**
     *
     * @param parametrosFicha
     * @param idFicha
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar ficha", notes = "Actualiza un ficha", nickname = "actualizaFicha")
    @RequestMapping(value = "/{idFicha}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFicha(@ApiParam(name = "ParametrosFicha", value = "Paramentros para la actualización del ficha", required = true) @RequestBody ParametrosFicha parametrosFicha, @ApiParam(name = "idFicha", value = "Identificador del ficha", example = "1", required = true) @PathVariable("idFicha") Integer idFicha) throws DataNotUpdatedException {
        parametrosFicha.setIdFicha(idFicha);
        fichaDAOImpl.updateRow(parametrosFicha);
        return new SinResultado();
    }

    /**
     *
     * @param idFicha
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar ficha", notes = "Elimina un item de los fichas", nickname = "eliminaFicha")
    @RequestMapping(value = "/{idFicha}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaFicha(@ApiParam(name = "idFicha", value = "Identificador del ficha", example = "1", required = true) @PathVariable("idFicha") Integer idFicha) throws DataNotDeletedException {
        fichaDAOImpl.deleteRow(idFicha);
        return new SinResultado();
    }

}
