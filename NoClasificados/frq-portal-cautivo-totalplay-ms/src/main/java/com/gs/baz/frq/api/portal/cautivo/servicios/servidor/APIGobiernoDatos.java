/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.servicios.servidor;

import com.gs.baz.frq.api.portal.cautivo.bi.GobiernoDatosBI;
import com.gs.baz.frq.api.portal.cautivo.dto.BancaDigitalDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.ICUDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.PortalCautivoDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.RespuestaBancaDigitalDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.RespuestaICUDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.RespuestaPortalCautivoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MADA
 */
@Api(tags = "extreme-cloud", value = "extreme-cloud", description = "API dedicada al consumo de información de Extreme Cloud")
@RestController
@RequestMapping("/service/wifi-sucursales/v1")
public class APIGobiernoDatos {

    @Autowired
    GobiernoDatosBI gobiernoDatosBI;

    private static final Logger LOGGER = LoggerFactory.getLogger(APIGobiernoDatos.class);

    @ApiOperation(value = "Descarga información de ICU obtenida por la conexión wifi de sucursales y almacenada por Franquicia", notes = "Descarga todos los dispositivos AP registrados en Extreme Cloud", nickname = "monitor-devices")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/gobierno-datos/icu", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaICUDTO obtieneDatosICU(
            @ApiParam(name = "fechaInicio", value = "Fecha Inicio en formato DD/MM/AAAA", required = true)
            @RequestParam(value = "fechaInicio", required = true) String fechaIni,
            @ApiParam(name = "fechaFin", value = "Fecha Fin en formato DD/MM/AAAA", required = true)
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @ApiParam(name = "identificadorCliente", value = "f4be026de09440e683f619e4ea83f14e", required = false)
            @RequestParam(value = "identificadorCliente", required = false) String identificador
    ) throws DataNotFoundException, CustomException {

        RespuestaICUDTO respuesta = null;

        List<ICUDTO> icu = gobiernoDatosBI.obtieneICU(fechaIni, fechaFin, identificador);
        respuesta = new RespuestaICUDTO();
        respuesta.setIcu(icu);
        if (respuesta == null || icu == null) {
            throw new DataNotFoundException();
        }

        return respuesta;
    }

    @ApiOperation(value = "Descarga información de PortalCautivo obtenida por la conexión wifi de sucursales y almacenada por Franquicia", notes = "Descarga todos los dispositivos AP registrados en Extreme Cloud", nickname = "monitor-devices")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/gobierno-datos/conexiones/portal-cautivo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaPortalCautivoDTO obtienePC(
            @ApiParam(name = "fechaInicio", value = "Fecha Inicio en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaInicio", required = true) String fechaIni,
            @ApiParam(name = "fechaFin", value = "Fecha Fin en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @ApiParam(name = "mac", value = "", required = false)
            @RequestParam(value = "mac", required = false) String mac
    ) throws DataNotFoundException, CustomException {

        RespuestaPortalCautivoDTO respuesta = null;

        List<PortalCautivoDTO> portal = gobiernoDatosBI.obtienePortalCautivo(fechaIni, fechaFin, mac);
        if (portal == null || portal.isEmpty()) {

            throw new DataNotFoundException();
        }
        respuesta = new RespuestaPortalCautivoDTO();
        respuesta.setPortalCautivo(portal);

        return respuesta;
    }

    @ApiOperation(value = "Descarga información de Banca Digital obtenida por la conexión wifi de sucursales y almacenada por Franquicia", notes = "Descarga todos los dispositivos AP registrados en Extreme Cloud", nickname = "monitor-devices")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente.", response = RespuestaLessResult200.class),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @ResponseBody
    @RequestMapping(value = "/gobierno-datos/conexiones/banca-digital", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaBancaDigitalDTO obtieneBC(
            @ApiParam(name = "fechaInicio", value = "Fecha Inicio en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaInicio", required = true) String fechaIni,
            @ApiParam(name = "fechaFin", value = "Fecha Fin en formato YYYY-MM-DD HH:MI:SS", required = true)
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @ApiParam(name = "mac", value = "", required = false)
            @RequestParam(value = "mac", required = false) String mac
    ) throws DataNotFoundException, CustomException {

        RespuestaBancaDigitalDTO respuesta = null;

        List<BancaDigitalDTO> banca = gobiernoDatosBI.obtieneBancaDigital(fechaIni, fechaFin, mac);
        if (banca == null || banca.isEmpty()) {

            throw new DataNotFoundException();
        }
        respuesta = new RespuestaBancaDigitalDTO();
        respuesta.setBancaDigital(banca);

        return respuesta;
    }

}
