package com.gs.baz.ddl.oracle.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.ddl.oracle.dao.DDLOracleDAO;
import com.gs.baz.ddl.oracle.process.UtilZip;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.ddl.oracle")
public class ConfigDDLOracle {

    private final Logger logger = LogManager.getLogger();

    public ConfigDDLOracle() {
        logger.info("Loading " + getClass().getName() + "...!");
    }







    @Bean
    public DDLOracleDAO ddlOracleDAO() {
        return new DDLOracleDAO();
    }

    @Bean
    public UtilZip UtilZip() {
        return new UtilZip();
    }

}
