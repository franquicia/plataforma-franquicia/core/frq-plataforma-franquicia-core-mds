/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de cecos", value = "cecos")
public class Cecos {

    @JsonProperty(value = "cecos")
    @ApiModelProperty(notes = "cecos")
    private List<Ceco> cecos;

    public Cecos(List<Ceco> cecos) {
        this.cecos = cecos;
    }

    public List<Ceco> getCecos() {
        return cecos;
    }

    public void setCecos(List<Ceco> cecos) {
        this.cecos = cecos;
    }

    @Override
    public String toString() {
        return "Cecos{" + "cecos=" + cecos + '}';
    }

}
