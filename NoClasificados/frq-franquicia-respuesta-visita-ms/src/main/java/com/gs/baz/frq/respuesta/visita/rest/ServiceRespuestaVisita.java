/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.respuesta.visita.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.respuesta.visita.dao.RespuestaVisitaDAOImpl;
import com.gs.baz.frq.respuesta.visita.dto.RespuestaVisitaDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/respuesta/visita")
public class ServiceRespuestaVisita {

    @Autowired
    private RespuestaVisitaDAOImpl respuestaVisitaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RespuestaVisitaDTO> getRespuestaVisista(@RequestBody RespuestaVisitaDTO respuestaVisista) throws CustomException {
        return respuestaVisitaDAOImpl.selectRows(respuestaVisista);
    }

    @RequestMapping(value = "secure/get/detalle", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RespuestaVisitaDTO> getDetalle(@RequestBody RespuestaVisitaDTO vistaPuntoContacto) throws CustomException {
        return respuestaVisitaDAOImpl.getDetalle(vistaPuntoContacto);
    }
}
