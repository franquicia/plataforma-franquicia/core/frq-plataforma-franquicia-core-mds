/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.paises.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.paises.dao.PaisDAOImpl;
import com.gs.baz.frq.modelo.negocio.paises.dto.AltaPais;
import com.gs.baz.frq.modelo.negocio.paises.dto.PaisBase;
import com.gs.baz.frq.modelo.negocio.paises.dto.Paises;
import com.gs.baz.frq.modelo.negocio.paises.dto.ParametrosPais;
import com.gs.baz.frq.modelo.negocio.paises.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "paises-negocio", value = "paises-negocio", description = "Api para la gestión del catalogo de paises")
@RestController
@RequestMapping("/api-local/paises-negocio/v1")
public class PaisApi {

    @Autowired
    private PaisDAOImpl paisDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idPais
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene pais", notes = "Obtiene un pais", nickname = "obtienePais")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idPais}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PaisBase obtienePais(@ApiParam(name = "idPais", value = "Identificador del pais", example = "1", required = true) @PathVariable("idPais") Long idPais) throws CustomException, DataNotFoundException {
        PaisBase paisBase = paisDAOImpl.selectRow(idPais);
        if (paisBase == null) {
            throw new DataNotFoundException();
        }
        return paisBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene paises", notes = "Obtiene todos los paises", nickname = "obtienePaises")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Paises obtienePaises() throws CustomException, DataNotFoundException {
        Paises paises = new Paises(paisDAOImpl.selectAllRows());
        if (paises.getPaises() != null && paises.getPaises().isEmpty()) {
            throw new DataNotFoundException();
        }
        return paises;
    }

    /**
     *
     * @param parametrosPais
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear pais", notes = "Agrega un pais", nickname = "creaPais")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaPais creaPais(@ApiParam(name = "ParametrosPais", value = "Paramentros para el alta del pais", required = true) @RequestBody ParametrosPais parametrosPais) throws DataNotInsertedException {
        return paisDAOImpl.insertRow(parametrosPais);
    }

    /**
     *
     * @param parametrosPais
     * @param idPais
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar pais", notes = "Actualiza un pais", nickname = "actualizaPais")
    @RequestMapping(value = "/{idPais}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaPais(@ApiParam(name = "ParametrosPais", value = "Paramentros para la actualización del pais", required = true) @RequestBody ParametrosPais parametrosPais, @ApiParam(name = "idPais", value = "Identificador del pais", example = "1", required = true) @PathVariable("idPais") Integer idPais) throws DataNotUpdatedException {
        parametrosPais.setIdPais(idPais);
        paisDAOImpl.updateRow(parametrosPais);
        return new SinResultado();
    }

    /**
     *
     * @param idPais
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar pais", notes = "Elimina un item de los paises", nickname = "eliminaPais")
    @RequestMapping(value = "/{idPais}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaPais(@ApiParam(name = "idPais", value = "Identificador del pais", example = "1", required = true) @PathVariable("idPais") Integer idPais) throws DataNotDeletedException {
        paisDAOImpl.deleteRow(idPais);
        return new SinResultado();
    }

}
