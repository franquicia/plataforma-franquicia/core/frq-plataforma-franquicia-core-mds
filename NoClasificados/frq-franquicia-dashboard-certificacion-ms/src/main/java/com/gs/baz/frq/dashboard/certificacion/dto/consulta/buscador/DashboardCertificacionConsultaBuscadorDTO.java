/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.consulta.buscador;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionConsultaBuscadorDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_gerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_gerente")
    private String nombreGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_certificador")
    private Integer idCertificador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_certificador")
    private String nombreCertificador;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco_base")
    private Integer idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "punto_contacto")
    private String puntoContacto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "direccion")
    private String direccion;

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(BigDecimal idGerente) {
        this.idGerente = (idGerente == null ? null : idGerente.intValue());
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    public Integer getIdCertificador() {
        return idCertificador;
    }

    public void setIdCertificador(BigDecimal idCertificador) {
        this.idCertificador = (idCertificador == null ? null : idCertificador.intValue());
    }

    public String getNombreCertificador() {
        return nombreCertificador;
    }

    public void setNombreCertificador(String nombreCertificador) {
        this.nombreCertificador = nombreCertificador;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(BigDecimal idCecoBase) {
        this.idCecoBase = (idCecoBase == null ? null : idCecoBase.intValue());
    }

    public String getPuntoContacto() {
        return puntoContacto;
    }

    public void setPuntoContacto(String puntoContacto) {
        this.puntoContacto = puntoContacto;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
