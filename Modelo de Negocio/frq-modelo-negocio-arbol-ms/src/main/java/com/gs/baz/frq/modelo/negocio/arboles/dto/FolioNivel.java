/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del folio y documento", value = "FoliosEtiqueta")
public class FolioNivel {

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "tituloDocumento")
    @ApiModelProperty(notes = "Titulo del documento", example = "Nivel 1")
    private String tituloDocumento;

    @JsonProperty(value = "unidadNegocio")
    @ApiModelProperty(notes = "Lista de unidades de negocio")
    private List<UnidadNegocio> unidadesNegocio;

    @JsonProperty(value = "creadorDocumento")
    @ApiModelProperty(notes = "Nombre del creador documento", example = "Carlos Escobar")
    private String creadorDocumento;

    @JsonProperty(value = "estadoDocumento")
    @ApiModelProperty(notes = "Estado del documento", example = "Nivel 1")
    private String estadoDocumento;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getTituloDocumento() {
        return tituloDocumento;
    }

    public void setTituloDocumento(String tituloDocumento) {
        this.tituloDocumento = tituloDocumento;
    }

    public List<UnidadNegocio> getUnidadesNegocio() {
        return unidadesNegocio;
    }

    public void setUnidadesNegocio(List<UnidadNegocio> unidadesNegocio) {
        this.unidadesNegocio = unidadesNegocio;
    }

    public String getCreadorDocumento() {
        return creadorDocumento;
    }

    public void setCreadorDocumento(String creadorDocumento) {
        this.creadorDocumento = creadorDocumento;
    }

    public String getEstadoDocumento() {
        return estadoDocumento;
    }

    public void setEstadoDocumento(String estadoDocumento) {
        this.estadoDocumento = estadoDocumento;
    }
}
