/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Incumplimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class IncumplimientosRowMapper implements RowMapper<Incumplimiento> {

    @Override
    public Incumplimiento mapRow(ResultSet rs, int rowNum) throws SQLException {
        Incumplimiento incumplimiento = new Incumplimiento();
        incumplimiento.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        incumplimiento.setDescripcion(rs.getString("FCDESCRIPCION"));
        incumplimiento.setPonderacion(rs.getInt("PONDERACION"));
        incumplimiento.setPuntosVenta(rs.getInt("PDV_INCUMPLIMIENTO"));
        return incumplimiento;
    }
}
