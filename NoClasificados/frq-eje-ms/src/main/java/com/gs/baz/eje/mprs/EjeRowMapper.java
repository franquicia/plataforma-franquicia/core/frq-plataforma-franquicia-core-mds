package com.gs.baz.eje.mprs;

import com.gs.baz.eje.dto.EjeDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class EjeRowMapper implements RowMapper<EjeDTO> {

    private EjeDTO status;

    @Override
    public EjeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new EjeDTO();
        status.setIdEje(((BigDecimal) rs.getObject("FIIDEJE")));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        return status;
    }
}
