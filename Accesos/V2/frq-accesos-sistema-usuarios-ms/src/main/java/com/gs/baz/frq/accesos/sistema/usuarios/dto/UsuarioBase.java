/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de usuario", value = "UsuarioBase")
public class UsuarioBase {

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Centro de costos", example = "954673")
    private String centroCostos;

    @ApiModelProperty(notes = "Nombre del Usuario", example = "Carlos Antonio Escobar Hernandez")
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonProperty(value = "tipoEmpleado")
    @ApiModelProperty(notes = "Tipo de empleado", example = "Interno")
    private String tipoEmpleado;

    @JsonProperty(value = "puesto")
    @ApiModelProperty(notes = "Puesto de trabajo", example = "Desarrollador")
    private String puesto;

    @JsonProperty(value = "correo")
    @ApiModelProperty(notes = "Correo del empleado", example = "cescobarh@eleketra.com.mx")
    private String correo;

    public UsuarioBase() {
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(String tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "UsuarioBase{" + "centroCostos=" + centroCostos + ", nombreUsuario=" + nombreUsuario + ", tipoEmpleado=" + tipoEmpleado + ", puesto=" + puesto + ", correo=" + correo + '}';
    }

}
