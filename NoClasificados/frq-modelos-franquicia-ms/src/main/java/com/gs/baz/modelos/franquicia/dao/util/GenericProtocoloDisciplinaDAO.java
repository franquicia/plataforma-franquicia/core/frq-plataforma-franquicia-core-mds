/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.modelos.franquicia.dao.util;

import com.gs.baz.frq.model.commons.CustomException;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericProtocoloDisciplinaDAO<dto> {

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
