/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Estado de la firma", value = "EstadoFirma")
public class EstadoFirma extends EstadoFirmaBase {

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificador del Estado de la firma", example = "1", position = -1)
    private Integer idEstadoFirma;

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    @Override
    public String toString() {
        return "EstadoFirma{" + "idEstadoFirma=" + idEstadoFirma + '}';
    }

}
