/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.dto.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosDocumentoUsuario {

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del documento", example = "1", position = -1)
    private Integer idDocumento;

    private transient Integer numeroEmpleado;

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    @Override
    public String toString() {
        return "ParametrosDocumentoUsuario{" + "idDocumento=" + idDocumento + ", numeroEmpleado=" + numeroEmpleado + '}';
    }

}
