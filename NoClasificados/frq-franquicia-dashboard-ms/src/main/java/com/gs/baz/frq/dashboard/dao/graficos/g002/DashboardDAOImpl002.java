package com.gs.baz.frq.dashboard.dao.graficos.g002;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g002.CalificacionNegociosDisciplinaDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g002.NegocioDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g002.CalificacionNegociosDisciplinaRowMapper;
import com.gs.baz.frq.dashboard.mprs.graficos.g002.NegocioRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl002 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCalificacionDisciplina;
    private DefaultJdbcCall jdbcSelectCalificacionNegocio;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCalificacionDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionDisciplina.withSchemaName(schema);
        jdbcSelectCalificacionDisciplina.withCatalogName("PACONDASHGRAL");
        jdbcSelectCalificacionDisciplina.withProcedureName("SPSELCALPORDISC");
        jdbcSelectCalificacionDisciplina.returningResultSet("RCL_INFO", new CalificacionNegociosDisciplinaRowMapper());

        jdbcSelectCalificacionNegocio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionNegocio.withSchemaName(schema);
        jdbcSelectCalificacionNegocio.withCatalogName("PACONDASHGRAL");
        jdbcSelectCalificacionNegocio.withProcedureName("SPSELCALPORNEGOCIO");
        jdbcSelectCalificacionNegocio.returningResultSet("RCL_INFO", new NegocioRowMapper());
    }

    public CalificacionNegociosDisciplinaDTO selectRowsCalificacionNegociosDisciplina(CalificacionNegociosDisciplinaDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCalificacionDisciplina.execute(mapSqlParameterSource);
        List<CalificacionNegociosDisciplinaDTO> data = (List<CalificacionNegociosDisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            data.get(0).setNegocios(this.selectRowsCalificacionTotalDisciplinaNegocios(filtros));
            return data.get(0);
        } else {
            return null;
        }
    }

    private List<NegocioDTO> selectRowsCalificacionTotalDisciplinaNegocios(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCalificacionNegocio.execute(mapSqlParameterSource);
        return (List<NegocioDTO>) out.get("RCL_INFO");
    }

}
