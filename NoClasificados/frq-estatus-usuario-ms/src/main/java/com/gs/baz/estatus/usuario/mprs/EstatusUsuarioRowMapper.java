package com.gs.baz.estatus.usuario.mprs;

import com.gs.baz.estatus.usuario.dto.EstatusUsuarioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class EstatusUsuarioRowMapper implements RowMapper<EstatusUsuarioDTO> {

    private EstatusUsuarioDTO estatusUsuario;

    @Override
    public EstatusUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        estatusUsuario = new EstatusUsuarioDTO();
        estatusUsuario.setIdEstatusUsuario(((BigDecimal) rs.getObject("FIID_ESTATUSUSU")));
        estatusUsuario.setDescripcion(rs.getString("FCDESCRIPCION"));
        estatusUsuario.setIdEstatus((BigDecimal) rs.getObject("FIESTATUS"));
        return estatusUsuario;
    }
}
