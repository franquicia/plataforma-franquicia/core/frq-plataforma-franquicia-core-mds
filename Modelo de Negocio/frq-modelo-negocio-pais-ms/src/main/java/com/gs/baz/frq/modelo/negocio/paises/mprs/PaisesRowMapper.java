/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.paises.mprs;

import com.gs.baz.frq.modelo.negocio.paises.dto.Pais;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class PaisesRowMapper implements RowMapper<Pais> {

    private Pais pais;

    @Override
    public Pais mapRow(ResultSet rs, int rowNum) throws SQLException {
        pais = new Pais();
        pais.setIdPais(rs.getInt("FIIDPAIS"));
        pais.setDescripcion(rs.getString("FCDESCRIPCION"));
        pais.setAcronimo(rs.getString("FCACRONIMO"));
        return pais;
    }
}
