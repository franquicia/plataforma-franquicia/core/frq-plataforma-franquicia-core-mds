/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de conteos de bandejas asociados a un proveedor", value = "ConteosBandejaProveedores")
public class DashboardProveedores {

    @JsonProperty(value = "conteoProveedores")
    @ApiModelProperty(notes = "conteoProveedores")
    private List<DashboardProveedor> conteoProveedores;

    public DashboardProveedores(List<DashboardProveedor> conteoProveedores) {
        this.conteoProveedores = conteoProveedores;
    }

    public List<DashboardProveedor> getConteoProveedores() {
        return conteoProveedores;
    }

    public void setConteoProveedores(List<DashboardProveedor> conteoProveedores) {
        this.conteoProveedores = conteoProveedores;
    }

    @Override
    public String toString() {
        return "DashboardProveedores{" + "conteoProveedores=" + conteoProveedores + '}';
    }


}
