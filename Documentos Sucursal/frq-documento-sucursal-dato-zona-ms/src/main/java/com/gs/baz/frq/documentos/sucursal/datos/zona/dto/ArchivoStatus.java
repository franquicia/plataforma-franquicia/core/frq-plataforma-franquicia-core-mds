/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Verifica las zonas que existen por Version", value = "ZonasPorVersion")
public class ArchivoStatus {

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Indica el valor del ceco", example = "1")
    private Integer idCeco;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Indica el valor del formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Indicador del id de documento ", example = "1")
    private Integer idDocumento;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Indicador del id de Proyecto ", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador delfolio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Nombre del Proyecto", example = "Nuevas")
    private String nombreProyecto;

    @JsonProperty(value = "nombreFormato")
    @ApiModelProperty(notes = "Nombre del Formato", example = "Retail")
    private String nombreFormato;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Nombre del Documento", example = "LAYOUT")
    private String nombreDocumento;

    @JsonProperty(value = "negocio")
    @ApiModelProperty(notes = "Nombre del negocio", example = "EKT")
    private String negocio;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "Identificador del idNegocio", example = "1")
    private Integer idNegocio;

    @JsonProperty(value = "idEstatusEdicion")
    @ApiModelProperty(notes = "Identificador del idEstatusEdicion", example = "1")
    private Integer idEstatusEdicion;

    @JsonProperty(value = "idVersion")
    @ApiModelProperty(notes = "Identificador del la Version", example = "1")
    private Integer idVersion;

    @JsonProperty(value = "descripcionArchivo")
    @ApiModelProperty(notes = "Descripcion Archivo", example = "1")
    private String descripcionArchivo;

    @JsonProperty(value = "idArchivoPadre")
    @ApiModelProperty(notes = "Identificador del idArchivoPadre", example = "1")
    private Integer idArchivoPadre;

    @JsonProperty(value = "idArchivo")
    @ApiModelProperty(notes = "Identificador del idArchivo", example = "1")
    private Integer idArchivo;

    @JsonProperty(value = "ruta")
    @ApiModelProperty(notes = "Identificador de la Ruta", example = "1628887621063")
    private String ruta;

    @JsonProperty(value = "extension")
    @ApiModelProperty(notes = "Identificador de la extension", example = ".pdf")
    private String extension;

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdEstatusEdicion() {
        return idEstatusEdicion;
    }

    public void setIdEstatusEdicion(Integer idEstatusEdicion) {
        this.idEstatusEdicion = idEstatusEdicion;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }

    public Integer getIdArchivoPadre() {
        return idArchivoPadre;
    }

    public void setIdArchivoPadre(Integer idArchivoPadre) {
        this.idArchivoPadre = idArchivoPadre;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

}
