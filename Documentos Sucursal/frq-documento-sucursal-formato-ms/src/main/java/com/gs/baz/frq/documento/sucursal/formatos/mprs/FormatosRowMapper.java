/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.formatos.mprs;

import com.gs.baz.frq.documento.sucursal.formatos.dto.Formato;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FormatosRowMapper implements RowMapper<Formato> {

    private Formato formato;

    @Override
    public Formato mapRow(ResultSet rs, int rowNum) throws SQLException {
        formato = new Formato();
        formato.setIdFormato(rs.getInt("FIID_FORMATO"));
        formato.setDescripcion(rs.getString("FCDESCRIPCION"));
        formato.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        formato.setNombreNegocio(rs.getString("NEGOCIO"));

        return formato;
    }
}
