package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.ProveedoresDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProveedoresBI {

    @Autowired
    private ProveedoresDAOImpl proveedoresDAO;
    private static final Logger LOGGER = LogManager.getLogger();

    public ProveedoresBI() {
        super();
    }

    public int inserta(ProveedoresDTO bean) {
        int respuesta = 0;

        try {
            respuesta = proveedoresDAO.inserta(bean);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible insertar ", e);

        }

        return respuesta;
    }

    public int insertaLimpieza(ProveedoresDTO bean) {
        int respuesta = 0;

        try {
            respuesta = proveedoresDAO.insertaLimpieza(bean);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible insertar ", e);

        }

        return respuesta;
    }

    public boolean elimina(String idProveedor) {
        boolean respuesta = false;

        try {
            respuesta = proveedoresDAO.elimina(idProveedor);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible eliminar", e);

        }

        return respuesta;
    }

    public boolean depura() {
        boolean respuesta = false;

        try {
            respuesta = proveedoresDAO.depura();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible eliminar", e);

        }

        return respuesta;
    }

    public List<ProveedoresDTO> obtieneDatos(int idProveedor) {
        List<ProveedoresDTO> listafila = null;
        try {
            listafila = proveedoresDAO.obtieneDatos(idProveedor);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener los datos", e);

        }

        return listafila;
    }

    public List<ProveedoresDTO> obtieneInfo() {
        List<ProveedoresDTO> listafila = null;
        try {
            listafila = proveedoresDAO.obtieneInfo();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla Incidencia", e);

        }

        return listafila;
    }

    public List<ProveedoresDTO> obtieneInfoLimpieza() {
        List<ProveedoresDTO> listafila = null;
        try {
            listafila = proveedoresDAO.obtieneInfoLimpieza();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible obtener datos de la tabla Incidencia", e);

        }

        return listafila;
    }

    public boolean actualiza(ProveedoresDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = proveedoresDAO.actualiza(bean);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "No fue posible actualizar", e);

        }

        return respuesta;
    }

}
