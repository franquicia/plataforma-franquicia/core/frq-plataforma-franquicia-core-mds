/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class DisciplinaNegocioDTO {

    @JsonProperty(value = "id_disciplina_negocio")
    private Integer idDisciplinaNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina_eje")
    private Integer idDisciplinaEje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "protocolos")
    private List<ProtocoloDisciplinaDTO> protocolosDisciplinaDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public DisciplinaNegocioDTO() {
    }

    public Integer getIdDisciplinaNegocio() {
        return idDisciplinaNegocio;
    }

    public void setIdDisciplinaNegocio(BigDecimal idDisciplinaNegocio) {
        this.idDisciplinaNegocio = (idDisciplinaNegocio == null ? null : idDisciplinaNegocio.intValue());
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getIdDisciplinaEje() {
        return idDisciplinaEje;
    }

    public void setIdDisciplinaEje(BigDecimal idDisciplinaEje) {
        this.idDisciplinaEje = (idDisciplinaEje == null ? null : idDisciplinaEje.intValue());
    }

    public List<ProtocoloDisciplinaDTO> getProtocolosDisciplinaDTO() {
        return protocolosDisciplinaDTO;
    }

    public void setProtocolosDisciplinaDTO(List<ProtocoloDisciplinaDTO> protocolosDisciplinaDTO) {
        this.protocolosDisciplinaDTO = protocolosDisciplinaDTO;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "DisciplinaNegocioDTO{" + "idDisciplinaNegocio=" + idDisciplinaNegocio + ", idDisciplina=" + idDisciplina + ", idDisciplinaEje=" + idDisciplinaEje + ", protocolosDisciplinaDTO=" + protocolosDisciplinaDTO + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
