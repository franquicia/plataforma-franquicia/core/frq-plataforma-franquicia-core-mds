/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.eje.rest;

import com.gs.baz.eje.dao.EjeDAOImpl;
import com.gs.baz.eje.dto.EjeDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/eje")
public class ServiceEje {

    @Autowired
    private EjeDAOImpl ejeDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EjeDTO> getEjes() throws CustomException {
        return ejeDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_eje}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeDTO getEje(@PathVariable("id_eje") Long id_eje) throws CustomException {
        return ejeDAOImpl.selectRow(id_eje);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeDTO postEje(@RequestBody EjeDTO eje) throws CustomException {
        if (eje.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return ejeDAOImpl.insertRow(eje);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeDTO putEje(@RequestBody EjeDTO eje) throws CustomException {
        if (eje.getIdEje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_eje"));
        }
        if (eje.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return ejeDAOImpl.updateRow(eje);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeDTO deleteEje(@RequestBody EjeDTO eje) throws CustomException {
        if (eje.getIdEje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_eje"));
        }
        return ejeDAOImpl.deleteRow(eje);
    }

    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EjeDTO> getDisciplinaByNombre(@RequestHeader HttpHeaders headers, @QueryParam("value") String value) throws CustomException {
        return ejeDAOImpl.selectRowsByName(value);
    }
}
