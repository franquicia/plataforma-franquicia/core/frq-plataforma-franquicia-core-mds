/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosEtiqueta {

    @JsonProperty(value = "idEtiqueta")
    private transient Integer idEtiqueta;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion de la etiqueta", example = "Nivel 1", required = true)
    private String descripcion;

    public Integer getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(Integer idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ParametrosEtiqueta{" + "descripcion=" + descripcion + '}';
    }

}
