/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.validador.util;

import java.util.Base64;

/**
 *
 * @author cescobarh
 */
public final class Base64Utils {

    private static final Base64.Decoder decoder = Base64.getDecoder();

    private Base64Utils() {
    }

    public static class Validator {

        public boolean isBase64(String value) {
            try {
                decoder.decode(value);
                return true;
            } catch (IllegalArgumentException ex) {
                return false;
            }
        }
    }
}
