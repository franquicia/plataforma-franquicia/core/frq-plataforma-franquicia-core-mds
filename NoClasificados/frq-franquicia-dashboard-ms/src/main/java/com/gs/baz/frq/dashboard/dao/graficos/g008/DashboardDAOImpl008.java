package com.gs.baz.frq.dashboard.dao.graficos.g008;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g008.CalificacionDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g008.CumplimientoSeccionNegocioDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g008.CumplimientoSeccionNivelDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g008.ModuloDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g008.NegocioDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g008.CumplimientoSeccionNivelRowMapper;
import com.gs.baz.frq.dashboard.mprs.graficos.g008.NegociosRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl008 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCumplimientoSeccionNacional;
    private DefaultJdbcCall jdbcSelectNegocios;
    private DefaultJdbcCall jdbcSelectCumplimientoSeccionNegocios;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCumplimientoSeccionNacional = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoSeccionNacional.withSchemaName(schema);
        jdbcSelectCumplimientoSeccionNacional.withCatalogName("PACUMPSEC");
        jdbcSelectCumplimientoSeccionNacional.withProcedureName("SPCUMPSECNEG");
        jdbcSelectCumplimientoSeccionNacional.returningResultSet("RCL_INFO", new CumplimientoSeccionNivelRowMapper());

        jdbcSelectNegocios = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNegocios.withSchemaName(schema);
        jdbcSelectNegocios.withCatalogName("PAREPODASHBCUMP");
        jdbcSelectNegocios.withProcedureName("SPGET_NEGOCIODISCID");
        jdbcSelectNegocios.returningResultSet("RCL_INFO", new NegociosRowMapper());

        jdbcSelectCumplimientoSeccionNegocios = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoSeccionNegocios.withSchemaName(schema);
        jdbcSelectCumplimientoSeccionNegocios.withCatalogName("PACUMPSECNEG");
        jdbcSelectCumplimientoSeccionNegocios.withProcedureName("SPCUMPSECNEGOCIO");
        jdbcSelectCumplimientoSeccionNegocios.returningResultSet("RCL_INFO", new CumplimientoSeccionNivelRowMapper());
    }

    private List<NegocioDTO> selectRowsNegociosDisciplina(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectNegocios.execute(mapSqlParameterSource);
        return (List<NegocioDTO>) out.get("RCL_INFO");
    }

    private List<CumplimientoSeccionNivelDTO> selectRowsCumplimientoSeccionNivel(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtrosDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtrosDTO.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoSeccionNacional.execute(mapSqlParameterSource);
        return (List<CumplimientoSeccionNivelDTO>) out.get("RCL_INFO");
    }

    private List<CumplimientoSeccionNivelDTO> selectRowsCumplimientoSeccionNivelNegocio(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtrosDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtrosDTO.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoSeccionNegocios.execute(mapSqlParameterSource);
        return (List<CumplimientoSeccionNivelDTO>) out.get("RCL_INFO");
    }

    private CumplimientoSeccionNegocioDTO getCumplimientoSeccionNivel(List<CumplimientoSeccionNivelDTO> cumplimientosSeccionNivel) throws CustomException {
        CumplimientoSeccionNegocioDTO cumplimientoSeccionNegocioDTO = new CumplimientoSeccionNegocioDTO();
        List<CalificacionDTO> calificacion = new ArrayList<>();
        List<ModuloDTO> modulos = new ArrayList<>();
        cumplimientosSeccionNivel.forEach((itemCumplimientoSeccionNivelDTO) -> {
            modulos.add(new ModuloDTO(itemCumplimientoSeccionNivelDTO.getRowNumber(), itemCumplimientoSeccionNivelDTO.getModulo(), itemCumplimientoSeccionNivelDTO.getValorSeccion()));
        });
        cumplimientosSeccionNivel.forEach((itemCumplimientoSeccionNivelDTO) -> {
            calificacion.add(new CalificacionDTO(itemCumplimientoSeccionNivelDTO.getRowNumber(), itemCumplimientoSeccionNivelDTO.getPuntoObtenido(), itemCumplimientoSeccionNivelDTO.getCalificacion()));
        });
        cumplimientoSeccionNegocioDTO.setModulos(modulos);
        cumplimientoSeccionNegocioDTO.setCalificaciones(calificacion);
        return cumplimientoSeccionNegocioDTO;
    }

    public List<NegocioDTO> getCumplimientoSeccionNivel(NegocioDTO negocioDTO) throws CustomException {
        FiltrosDTO filtrosDTO = negocioDTO.getFiltros();

        if (!filtrosDTO.getIdDisciplina().equals(5)) {
            List<NegocioDTO> negocios = this.selectRowsNegociosDisciplina(filtrosDTO);
            for (NegocioDTO itemNegocio : negocios) {
                filtrosDTO.setIdNegocio(itemNegocio.getIdNegocio());
                List<CumplimientoSeccionNivelDTO> cumplimientosSecciones = this.selectRowsCumplimientoSeccionNivel(filtrosDTO);
                itemNegocio.setCumplimientoSeccionNegocioDTO(this.getCumplimientoSeccionNivel(cumplimientosSecciones));
            }
            return negocios;
        } else {
            List<NegocioDTO> negocios = new ArrayList<>();

            negocios.add(new NegocioDTO(4));
            for (NegocioDTO itemNegocio : negocios) {
                filtrosDTO.setIdNegocio(itemNegocio.getIdNegocio());
                List<CumplimientoSeccionNivelDTO> cumplimientosSecciones = this.selectRowsCumplimientoSeccionNivel(filtrosDTO);
                itemNegocio.setCumplimientoSeccionNegocioDTO(this.getCumplimientoSeccionNivel(cumplimientosSecciones));
            }
            return negocios;
        }

    }

    public List<NegocioDTO> getCumplimientoSeccionNivelNegocio(NegocioDTO negocioDTO) throws CustomException {
        FiltrosDTO filtrosDTO = negocioDTO.getFiltros();
        List<NegocioDTO> negocios = new ArrayList<>();
        negocios.add(new NegocioDTO(filtrosDTO.getIdNegocio()));
        for (NegocioDTO itemNegocio : negocios) {
            filtrosDTO.setIdNegocio(itemNegocio.getIdNegocio());
            List<CumplimientoSeccionNivelDTO> cumplimientosSecciones = this.selectRowsCumplimientoSeccionNivelNegocio(filtrosDTO);
            itemNegocio.setCumplimientoSeccionNegocioDTO(this.getCumplimientoSeccionNivel(cumplimientosSecciones));
        }
        return negocios;
    }

}
