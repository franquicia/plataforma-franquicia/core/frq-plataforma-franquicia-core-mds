/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Parametros de la ruta", value = "ParametrosRuta")
public class ParametrosRuta {

    private Integer idRuta;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre de la ruta", example = "accesos_sistema_rutas_edicion")
    @NotNull
    private String nombre;

    @JsonProperty(value = "rutaAbsoluta")
    @ApiModelProperty(notes = "Ruta absoluta declarada", example = "/accesos/sistema/rutas/edicion/:ruta")
    @NotNull
    private String rutaAbsoluta;

    public ParametrosRuta() {
    }

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaAbsoluta() {
        return rutaAbsoluta;
    }

    public void setRutaAbsoluta(String rutaAbsoluta) {
        this.rutaAbsoluta = rutaAbsoluta;
    }

    @Override
    public String toString() {
        return "ParametrosRuta{" + "idRuta=" + idRuta + ", nombre=" + nombre + ", rutaAbsoluta=" + rutaAbsoluta + '}';
    }

}
