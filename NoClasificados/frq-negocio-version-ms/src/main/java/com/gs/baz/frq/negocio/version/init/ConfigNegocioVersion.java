package com.gs.baz.frq.negocio.version.init;

import com.gs.baz.frq.negocio.version.dao.NegocioVersionDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.negocio.version")
public class ConfigNegocioVersion {

    private final Logger logger = LogManager.getLogger();

    public ConfigNegocioVersion() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqNegocioVersionDAOImpl")
    public NegocioVersionDAOImpl negocioVersionDAOImpl() {
        return new NegocioVersionDAOImpl();
    }
}
