/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author cescobarh
 */
public class ParametrosSemaforo {

    @JsonProperty(value = "idSemaforo")
    private transient Integer idSemaforo;

    @JsonProperty(value = "clave", required = true)
    @ApiModelProperty(notes = "Clave del semaforo", example = "semaforo-oficial", required = true)
    @NotEmpty
    private String clave;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion nombre del semaforo", example = "Semaforo", required = true)
    @NotEmpty
    private String descripcion;

    public Integer getIdSemaforo() {
        return idSemaforo;
    }

    public void setIdSemaforo(Integer idSemaforo) {
        this.idSemaforo = idSemaforo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ParametrosSemaforo{" + "idSemaforo=" + idSemaforo + ", clave=" + clave + ", descripcion=" + descripcion + '}';
    }

}
