/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReporteConteoGralSemanalDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteoRealizadas")
    private Integer conteoRealizadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteoProgramadas")
    private Integer conteoProgramadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semana")
    private String semana;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private Integer territorio;

    
    @JsonInclude(JsonInclude.Include.NON_NULL)//se indica el tipo de conteo  programado o realizado
    @JsonProperty(value = "tipoRealizado")
    private String tipoRealizado;

    
    
    public Integer getTerritorio() {
		return territorio;
	}

	public void setTerritorio(Integer territorio) {
		this.territorio = territorio;
	}

	public Integer getConteoProgramadas() {
        return conteoProgramadas;
    }

    public void setConteoProgramadas(Integer conteoProgramadas) {
        this.conteoProgramadas = conteoProgramadas;
    }

    public Integer getConteoRealizadas() {
        return conteoRealizadas;
    }

    public void setConteoRealizadas(Integer conteoRealizadas) {
        this.conteoRealizadas = conteoRealizadas;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getSemana() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana = semana;
    }

    public String getTipoRealizado() {
        return tipoRealizado;
    }

    public void setTipoRealizado(String tipoRealizado) {
        this.tipoRealizado = tipoRealizado;
    }

    @Override
    public String toString() {
        return "ReporteConteoGralSemanalDTO [conteoRealizadas=" + conteoRealizadas + ", conteoProgramadas="
                + conteoProgramadas + ", zona=" + zona + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
                + ", semana=" + semana + ", tipoRealizado=" + tipoRealizado + "]";
    }

}
