/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.flujo.rest;

import com.gs.baz.frq.flujo.dao.FlujoDAOImpl;
import com.gs.baz.frq.flujo.dto.FlujoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/flujo")
public class ServiceFlujo {

    @Autowired
    private FlujoDAOImpl flujoDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;
    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoDTO> getFlujos() throws CustomException {
        return flujoDAOImpl.selectRows();
    }

    /**
     *
     * @param idFlujo
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_flujo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoDTO getFlujo(@PathVariable("id_flujo") Long idFlujo) throws CustomException {
        return flujoDAOImpl.selectRow(idFlujo);
    }

    /**
     *
     * @param flujoDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoDTO postFlujo(@RequestBody FlujoDTO flujoDTO) throws CustomException {
        if (flujoDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (flujoDTO.getTipoApp() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("tipo_app"));
        }
        return flujoDAOImpl.insertRow(flujoDTO);
    }

    /**
     *
     * @param flujoDTO
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoDTO putFlujo(@RequestBody FlujoDTO flujoDTO) throws CustomException {
        if (flujoDTO.getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo"));
        }
        if (flujoDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (flujoDTO.getTipoApp() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("tipo_app"));
        }
        return flujoDAOImpl.updateRow(flujoDTO);
    }

    /**
     *
     * @param flujoDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoDTO deleteFlujo(@RequestBody FlujoDTO flujoDTO) throws CustomException {
        if (flujoDTO.getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo"));
        }
        return flujoDAOImpl.deleteRow(flujoDTO);
    }

}
