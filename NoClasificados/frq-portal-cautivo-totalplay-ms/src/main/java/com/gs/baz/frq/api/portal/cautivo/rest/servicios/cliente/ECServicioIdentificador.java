/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente;

import com.fasterxml.jackson.databind.JsonNode;
import com.gs.baz.frq.api.portal.cautivo.dto.IdentificadorDTO;
import com.gs.baz.frq.api.portal.cautivo.util.BaseUrl;
import com.gs.baz.frq.api.portal.cautivo.util.DefaultRestTemplate;
import com.gs.baz.frq.api.portal.cautivo.util.Relasionship;
import com.gs.baz.frq.model.commons.CustomException;
import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author MADA
 */
public class ECServicioIdentificador extends DefaultRestTemplate {

    private final Logger logger = LogManager.getLogger();

    public List<IdentificadorDTO> getListaClientes(String startTime, String endTime, String noEco) throws CustomException {

        try {

            String url = Relasionship.APP.url(BaseUrl.va2.getBaseUrl());

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);

            if (startTime != null) {
                builder = builder.queryParam("fechaIni", startTime);
            }

            if (endTime != null) {
                builder = builder.queryParam("fechaFin", endTime);
            }
            builder = builder.queryParam("noEco", noEco);

            responseString = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, String.class);

            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("data") != null) {
                    List<IdentificadorDTO> clientes = objectMapper.readValue(responseStringBody.get("data").toString(), objectMapper.getTypeFactory().constructCollectionLikeType(List.class, IdentificadorDTO.class));
                    return clientes;
                } else {
                    //throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE.detalle("Error on data service"));
                }
            } else {
                // throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE);
            }

        } catch (IOException | HttpServerErrorException ex) {
            //throw new CustomException(ex);
        }

        return null;

    }

}
