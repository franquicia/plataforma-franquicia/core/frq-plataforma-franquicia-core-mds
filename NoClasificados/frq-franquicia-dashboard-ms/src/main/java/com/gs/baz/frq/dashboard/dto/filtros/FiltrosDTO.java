/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.filtros;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author B73601
 */
public class FiltrosDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_periodo")
    private Integer numeroPeriodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "anio")
    private Integer anio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nivel_geografico")
    private Integer nivelGeografico;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_frecuencia_medicion")
    private Integer idFrecuenciaMedicion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_fin")
    private String fechaFin;

    public Integer getNumeroPeriodo() {
        return numeroPeriodo;
    }

    public void setNumeroPeriodo(Integer numeroPeriodo) {
        this.numeroPeriodo = numeroPeriodo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getNivelGeografico() {
        return nivelGeografico;
    }

    public void setNivelGeografico(Integer nivelGeografico) {
        this.nivelGeografico = nivelGeografico;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public Integer getIdFrecuenciaMedicion() {
        return idFrecuenciaMedicion;
    }

    public void setIdFrecuenciaMedicion(Integer idFrecuenciaMedicion) {
        this.idFrecuenciaMedicion = idFrecuenciaMedicion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Override
    public String toString() {
        return "FiltrosDTO{" + "numeroPeriodo=" + numeroPeriodo + ", anio=" + anio + ", nivelGeografico=" + nivelGeografico + ", idNegocio=" + idNegocio + ", idCeco=" + idCeco + ", idDisciplina=" + idDisciplina + ", idFrecuenciaMedicion=" + idFrecuenciaMedicion + ", descripcion=" + descripcion + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + '}';
    }

}
