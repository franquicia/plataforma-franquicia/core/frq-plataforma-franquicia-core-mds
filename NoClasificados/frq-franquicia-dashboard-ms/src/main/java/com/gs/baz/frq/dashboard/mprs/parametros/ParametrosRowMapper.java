/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.mprs.parametros;

import com.gs.baz.frq.dashboard.dto.parametros.ParametroDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author MADA
 */
public class ParametrosRowMapper implements RowMapper<ParametroDTO> {

    ParametroDTO parametroDTO;

    @Override
    public ParametroDTO mapRow(ResultSet rs, int i) throws SQLException {
        parametroDTO = new ParametroDTO();
        parametroDTO.setClave(rs.getString("FCCVE_PARAM"));
        parametroDTO.setValor(rs.getString("FCVALOR"));
        parametroDTO.setEstatus(rs.getInt("FIACTIVO"));
        return parametroDTO;
    }

}
