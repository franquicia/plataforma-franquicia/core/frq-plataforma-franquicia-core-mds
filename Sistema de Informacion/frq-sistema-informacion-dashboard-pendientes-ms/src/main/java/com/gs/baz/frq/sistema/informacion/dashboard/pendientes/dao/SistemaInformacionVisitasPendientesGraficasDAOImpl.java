/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CertificacionesGerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.Gerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.GraficaGeneral;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesGerente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PendientesSemanal;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PuntoVentaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PuntosVentaPendientes;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendienteParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.PendientesGerenteRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.PendientesSemanalRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.PuntosVentaPendientesRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs.VisitasPendientesRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionVisitasPendientesGraficasDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectGrafica01General;
    private DefaultJdbcCall jdbcSelectGrafica02;
    private DefaultJdbcCall jdbcSelectGrafica03;
    private DefaultJdbcCall jdbcSelectGrafica04;
    private DefaultJdbcCall jdbcSelectGraficaGerente04;
    private DefaultJdbcCall jdbcSelectGrafica05;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PADASHBMONITPEND";

        jdbcSelectGrafica01General = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGrafica01General.withSchemaName(schema);
        jdbcSelectGrafica01General.withCatalogName(catalogo);
        jdbcSelectGrafica01General.withProcedureName("SPGETGRA01GENERAL");

        jdbcSelectGrafica02 = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGrafica02.withSchemaName(schema);
        jdbcSelectGrafica02.withCatalogName(catalogo);
        jdbcSelectGrafica02.withProcedureName("SPGETGRA02PENDXSEM");
        jdbcSelectGrafica02.returningResultSet("PA_CDATOS", new PendientesSemanalRowMapper());

        jdbcSelectGrafica03 = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGrafica03.withSchemaName(schema);
        jdbcSelectGrafica03.withCatalogName(catalogo);
        jdbcSelectGrafica03.withProcedureName("SPGETGRA03CERTXGER");
        jdbcSelectGrafica03.returningResultSet("PA_CDATOS", new PendientesGerenteRowMapper());
        
        jdbcSelectGrafica04 = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGrafica04.withSchemaName(schema);
        jdbcSelectGrafica04.withCatalogName(catalogo);
        jdbcSelectGrafica04.withProcedureName("SPGETGRA04PENDXGER");
        
        jdbcSelectGraficaGerente04 = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGraficaGerente04.withSchemaName(schema);
        jdbcSelectGraficaGerente04.withCatalogName(catalogo);
        jdbcSelectGraficaGerente04.withProcedureName("SPGETGRA04GERENTE");
        
        jdbcSelectGrafica05 = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectGrafica05.withSchemaName(schema);
        jdbcSelectGrafica05.withCatalogName(catalogo);
        jdbcSelectGrafica05.withProcedureName("SPGETGRA05PDVPENDIENTES");
        jdbcSelectGrafica05.returningResultSet("PA_CDATOS", new PuntosVentaPendientesRowMapper());
    }

    public GraficaGeneral selectGrafica01() throws CustomException {

        Map<String, Object> out = jdbcSelectGrafica01General.execute();
        Integer certificadores = ((BigDecimal) out.get("PA_CERTFICADORES")).intValue();
        Integer gerentes = ((BigDecimal) out.get("PA_GERENTES")).intValue();

        GraficaGeneral grafica01 = new GraficaGeneral();
        grafica01.setCertificadores(certificadores);
        grafica01.setGerentes(gerentes);
        return grafica01;
    }

    public List<PendientesSemanal> selectGrafica02(Integer semanasAtras, String fechaActual) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_SEMANASATRAS", semanasAtras);
        mapSqlParameterSource.addValue("PA_FECHA_ACTUAL", fechaActual);
        Map<String, Object> out = jdbcSelectGrafica02.execute(mapSqlParameterSource);
        return (List<PendientesSemanal>) out.get("PA_CDATOS");
    }

    public List<CertificacionesGerente> selectGrafica03(Integer numeroEmpleadoGerente) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIGERENTEID", numeroEmpleadoGerente);
        Map<String, Object> out = jdbcSelectGrafica03.execute(mapSqlParameterSource);
        return (List<CertificacionesGerente>) out.get("PA_CDATOS");
    }
    
    public PendientesGerente selectGrafica04(Integer numeroEmpleadoGerente) throws CustomException {
 MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIGERENTEID", numeroEmpleadoGerente);
        Map<String, Object> out = jdbcSelectGrafica04.execute(mapSqlParameterSource);
        Integer certificadores = ((BigDecimal) out.get("PA_CERTFICADORES")).intValue();
        Integer certPendientes = ((BigDecimal) out.get("PA_CERT_PEND")).intValue();

        PendientesGerente pendientesGerente = new PendientesGerente();
        pendientesGerente.setCertificadores(certificadores);
        pendientesGerente.setCertificacionesPendientes(certPendientes);
        return pendientesGerente;
    } 

     public Gerente selectGraficaGerente04(Integer numeroEmpleadoGerente) throws CustomException {
 MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIGERENTEID", numeroEmpleadoGerente);
        Map<String, Object> out = jdbcSelectGraficaGerente04.execute(mapSqlParameterSource);
        String nombreGerente = ((String) out.get("PA_GERENTE"));

        Gerente gerente = new Gerente();
        gerente.setNombreGerente(nombreGerente);
       
        return gerente;
    }
     
     public List<PuntoVentaPendiente> selectGrafica05(Integer numeroEmpleadoGerente) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIGERENTEID", numeroEmpleadoGerente);
        Map<String, Object> out = jdbcSelectGrafica05.execute(mapSqlParameterSource);
        return (List<PuntoVentaPendiente>) out.get("PA_CDATOS");
    }
}
