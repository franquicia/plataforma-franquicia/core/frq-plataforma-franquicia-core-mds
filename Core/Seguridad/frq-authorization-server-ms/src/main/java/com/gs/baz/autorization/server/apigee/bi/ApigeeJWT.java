/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.bi;

import com.gobierno.apis.seguridad.JsonWebToken;
import com.gobierno.apis.seguridad.exceptions.MessageException;
import com.gs.baz.security.access.util.SecurityUtils;
import com.gs.baz.security.access.util.SecurityUtilsException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ApigeeJWT {

    private final Logger logger = LogManager.getLogger();
    private final String privateKeyB64 = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALj5uScO98o7K3vLqMqEudEHLIr29CJLR7Gey/42IRaY0JfewkpFAtIvd59mkLW0mtAvprvcNIn8aeZdnYWYO4vzF7C0oPYRg4PzbvUQMdCI0AB4TlhlAEN+g0NLgm7N0LGEWNUJC13HFeRxvEIqXab5vgnL5XXqhp1baU6PXHlLAgMBAAECgYA5FwH5NxptNTPngmT3FF3RUNiDj1KsHvXisTMeo5A0zDddGCh5CbGkM3hFHz+8hSBe7sYUJxOkHt6EdhI/AEwoEBgYbp9AAh2+02adiIy54YSqRf0BNfj70MHGxX3wKGPGXcN7lPQvCYCHEdcwh2L3QrUCuQHLWro+uXh4XiXiKQJBAOFI3Ao8XLog/6DW19z/Iv2rQBfyLdYyn4GSO05r949OCbvYKoSrJS/IPVcgvM3W9b19GSsbag+RJNikF82A0kUCQQDSMfLXh499dU/y5NmwXbD+4QRvGeUjVfPAbIztcrwqoMsTMIwI+rraqnrXC2x4xNOqLuyt2BPfcbzb1KTfUZ5PAkBYzSh3HG4MrD83Phiesy8uwJcmGT86JIA9Sc/4zJQrt74cfkvcOb4RxKNxOaVSmf4JsEsAc/Dtq+/TOXNsfO/lAkBOIECZuYos4Hip2EPNWNWbtrk2LlLLsnvaDr7G1vl3grJYXJgAEml3MxxbZ6SWstnVjIFuED06tQS0VLI/Sw/jAkEApjdhjjOOLfnsDsWDIH5qeunJ6ULbuwdgxmCgQFjNfyd9RZ4joalq2q3ai9q0lXrIC6IkDJyC3awCjXrrK4xfBQ==";
    private final String publicKeyB64 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC4+bknDvfKOyt7y6jKhLnRByyK9vQiS0exnsv+NiEWmNCX3sJKRQLSL3efZpC1tJrQL6a73DSJ/GnmXZ2FmDuL8xewtKD2EYOD8271EDHQiNAAeE5YZQBDfoNDS4JuzdCxhFjVCQtdxxXkcbxCKl2m+b4Jy+V16oadW2lOj1x5SwIDAQAB";

    public String generarJWT(Map<String, Object> claims, String usuarioToken) throws SecurityUtilsException {
        SecurityUtils.Generator generator = new SecurityUtils.Generator();
        generator.getPairsFromPrivateKey(privateKeyB64);
        SecurityUtils.Encrypter encrypter = new SecurityUtils.Encrypter(generator.getPublicKey());
        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setHeaderParam("typ", "JWT");
        jwtBuilder.setClaims(claims);
        jwtBuilder.claim("sub", encrypter.encrypt(usuarioToken));
        jwtBuilder.setIssuedAt(new Date());
        jwtBuilder.setExpiration(new Date(System.currentTimeMillis() + 21600000L));
        jwtBuilder.signWith(SignatureAlgorithm.RS512, generator.getPrivateKey());
        String pr = Base64.getEncoder().encodeToString(generator.getPrivateKey().getEncoded());
        String pu = Base64.getEncoder().encodeToString(generator.getPublicKey().getEncoded());
        System.out.println(convertToPublicKey(pu));
        System.out.println(convertToPrivateKey(pr));
        return jwtBuilder.compact();
    }

    public void validateJWTApigeeUser(String token, String usuarioToken, String publicKeyB64) throws ApigeeJWTException {
        if (token == null || token.isEmpty()) {
            throw new ApigeeJWTException("AuthorizationMissing");
        }
        if (usuarioToken == null || usuarioToken.isEmpty()) {
            throw new ApigeeJWTException("UserMissing");
        }
        if (publicKeyB64 == null || publicKeyB64.isEmpty()) {
            throw new ApigeeJWTException("PublicKeyMissing");
        }
        try {
            JsonWebToken.validaJWT(token, publicKeyB64, usuarioToken);
        } catch (SignatureException e) {
            throw new ApigeeJWTException("SignatureException", e);
        } catch (MalformedJwtException e) {
            throw new ApigeeJWTException("MalformedJwtException", e);
        } catch (ExpiredJwtException e) {
            throw new ApigeeJWTException("ExpiredJwtException", e);
        } catch (MessageException e) {
            throw new ApigeeJWTException("MessageException", e);
        }
    }

    public void validateJWT(String token) throws ApigeeJWTException {
        validateJWT(token, publicKeyB64);
    }

    public void validateJWT(String token, String publicKeyB64) throws ApigeeJWTException {
        if (token == null || token.isEmpty()) {
            throw new ApigeeJWTException("AuthorizationMissing");
        }
        if (publicKeyB64 == null || publicKeyB64.isEmpty()) {
            throw new ApigeeJWTException("PublicKeyMissing");
        }
        try {
            JsonWebToken.validaJWT(token, publicKeyB64);
        } catch (SignatureException e) {
            throw new ApigeeJWTException("SignatureException", e);
        } catch (MalformedJwtException e) {
            throw new ApigeeJWTException("MalformedJwtException", e);
        } catch (ExpiredJwtException e) {
            throw new ApigeeJWTException("ExpiredJwtException", e);
        } catch (MessageException e) {
            throw new ApigeeJWTException("MessageException", e);
        }
    }

    private String convertToPublicKey(String key) {
        StringBuilder result = new StringBuilder();
        result.append("-----BEGIN PUBLIC KEY-----\n");
        result.append(key);
        result.append("\n-----END PUBLIC KEY-----");
        return result.toString();
    }

    private String convertToPrivateKey(String key) {
        StringBuilder result = new StringBuilder();
        result.append("-----BEGIN RSA PRIVATE KEY-----\n");
        result.append(key);
        result.append("\n-----END RSA PRIVATE KEY-----");
        return result.toString();
    }

}
