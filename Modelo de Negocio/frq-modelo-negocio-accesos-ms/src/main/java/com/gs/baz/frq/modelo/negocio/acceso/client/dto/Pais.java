/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class Pais {

    @JsonProperty(value = "idPais")
    @ApiModelProperty(notes = "identificador del pais", example = "21")
    private Integer idPais;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del pais", example = "MEXICO")
    private String descripcion;

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
