/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReporteCheckInnMttoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCheckInn")
    private Integer idCheckInn;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private String idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "dia")
    private String dia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semana")
    private String semana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaEntrada")
    private String horaEntrada;

    //hora salida del 2do nivel
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaSalida")
    private String horaSalida;

    //hora salida del protocolo
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaSalidaProtocolo")
    private String horaSalidaProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horasTrabajadas")
    private String horasTrabajadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idBitacora")
    private Integer idBitacora;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaEntradaProtocolo")
    private String horaEntradaProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horasTrabajadasProtocolo")
    private String horasTrabajadasProtocolo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreFase")
    private String nombreFase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idFase")
    private Integer idFase;

    public Integer getIdFase() {
        return idFase;
    }

    public void setIdFase(Integer idFase) {
        this.idFase = idFase;
    }

    public Integer getIdCheckInn() {
        return idCheckInn;
    }

    public void setIdCheckInn(Integer idCheckInn) {
        this.idCheckInn = idCheckInn;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getSemana() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana = semana;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraSalidaProtocolo() {
        return horaSalidaProtocolo;
    }

    public void setHoraSalidaProtocolo(String horaSalidaProtocolo) {
        this.horaSalidaProtocolo = horaSalidaProtocolo;
    }

    public String getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(String horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Integer idBitacora) {
        this.idBitacora = idBitacora;
    }

    public String getHoraEntradaProtocolo() {
        return horaEntradaProtocolo;
    }

    public void setHoraEntradaProtocolo(String horaEntradaProtocolo) {
        this.horaEntradaProtocolo = horaEntradaProtocolo;
    }

    public String getHorasTrabajadasProtocolo() {
        return horasTrabajadasProtocolo;
    }

    public void setHorasTrabajadasProtocolo(String horasTrabajadasProtocolo) {
        this.horasTrabajadasProtocolo = horasTrabajadasProtocolo;
    }

    public String getNombreFase() {
        return nombreFase;
    }

    public void setNombreFase(String nombreFase) {
        this.nombreFase = nombreFase;
    }

    @Override
    public String toString() {
        return "ReporteCheckInnMttoDTO [idCheckInn=" + idCheckInn + ", idUsuario=" + idUsuario + ", nombreUsuario="
                + nombreUsuario + ", idCeco=" + idCeco + ", nombreCeco=" + nombreCeco + ", dia=" + dia + ", semana="
                + semana + ", fecha=" + fecha + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
                + ", horaEntrada=" + horaEntrada + ", horaSalida=" + horaSalida + ", horaSalidaProtocolo="
                + horaSalidaProtocolo + ", horasTrabajadas=" + horasTrabajadas + ", periodo=" + periodo + ", zona="
                + zona + ", idBitacora=" + idBitacora + ", horaEntradaProtocolo=" + horaEntradaProtocolo
                + ", horasTrabajadasProtocolo=" + horasTrabajadasProtocolo + ", nombreFase=" + nombreFase
                + ", idFase=" + idFase + "]";
    }

}
