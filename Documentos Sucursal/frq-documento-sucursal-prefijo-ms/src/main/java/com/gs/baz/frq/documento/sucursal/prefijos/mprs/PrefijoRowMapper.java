/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.mprs;

import com.gs.baz.frq.documento.sucursal.prefijos.dto.PrefijoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class PrefijoRowMapper implements RowMapper<PrefijoBase> {

    private PrefijoBase negocioBase;

    @Override
    public PrefijoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocioBase = new PrefijoBase();
        negocioBase.setPrefijo(rs.getString("FCDESCRIPCION"));
        negocioBase.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        return negocioBase;
    }
}
