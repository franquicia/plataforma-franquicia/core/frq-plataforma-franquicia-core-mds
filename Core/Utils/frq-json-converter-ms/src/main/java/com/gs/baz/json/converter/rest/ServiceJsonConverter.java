/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.json.converter.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.UserToken;
import com.gs.baz.frq.model.commons.UserTokenDTO;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.json.converter.excel.dto.ExcelFileDTO;
import com.gs.baz.json.converter.util.WriteExcelUtil;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/json/convert")
public class ServiceJsonConverter {

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    WriteExcelUtil writeExcelUtil;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    private final String APPLICATION_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private final String APPLICATION_CSV = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private final String APPLICATION_EXCEL_OLD = "application/vnd.ms-excel";

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/template/excel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertFromTemplate(HttpServletRequest request, @RequestBody ExcelFileDTO excelFileDTO) throws CustomException {
        HttpHeaders outHeaders = new HttpHeaders();
        String nameFile = new Date().getTime() + ".xlsx";
        UserTokenDTO userTokenDTO = new UserToken(request).getUserToken();
        if (excelFileDTO.getAutor() == null) {
            excelFileDTO.setAutor(userTokenDTO.getNombreUsuario());
        }
        if (excelFileDTO.getNameFile() == null) {
            excelFileDTO.setNameFile(nameFile);
        } else {
            excelFileDTO.setNameFile(excelFileDTO.getNameFile() + ".xlsx");
        }
        outHeaders.set("Content-Disposition", "inline;filename=\"" + excelFileDTO.getNameFile() + "\"");
        outHeaders.set("file-name", nameFile);
        Resource resource = writeExcelUtil.writeExcel(excelFileDTO);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(resource);
    }

    @RequestMapping(value = "secure/array/excel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertFromArray(@RequestBody JsonNode jsonNode) throws CustomException {
        HttpHeaders outHeaders = new HttpHeaders();
        String nameFile = new Date().getTime() + "_excelFile.xlsx";
        if (jsonNode != null) {
            if (jsonNode.isArray()) {
                ObjectNode on = new ObjectMapper().createObjectNode();
                on.set("SIMPLE-DATA", new ObjectMapper().convertValue(jsonNode, JsonNode.class));
                jsonNode = on;
            }
            if (jsonNode.get("name_file") != null) {
                nameFile = jsonNode.get("name_file").asText() + ".xlsx";
            } else if (jsonNode.get("NAME_FILE") != null) {
                nameFile = jsonNode.get("NAME_FILE").asText() + ".xlsx";
            }
        } else {
            throw new CustomException(new Exception("Object is null"));
        }
        outHeaders.set("Content-Disposition", "inline;filename=\"" + nameFile + "\"");
        outHeaders.set("file-name", nameFile);
        Resource resource = writeExcelUtil.writeExcel(jsonNode);
        return ResponseEntity.status(HttpStatus.OK).headers(outHeaders).body(resource);
    }

    @RequestMapping(value = "secure/array/file/excel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertFromFileArray(HttpServletRequest request) throws CustomException, IOException {
        JsonNode jn = new ObjectMapper().readValue(request.getInputStream(), JsonNode.class);
        return this.convertFromArray(jn);
    }

    @RequestMapping(value = "secure/array/form/data/excel", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_EXCEL)
    public ResponseEntity convertFromFormDataFileArray(@RequestPart("file") MultipartFile file) throws CustomException, IOException {
        JsonNode jn = new ObjectMapper().readValue(file.getInputStream(), JsonNode.class);
        return this.convertFromArray(jn);
    }
}
