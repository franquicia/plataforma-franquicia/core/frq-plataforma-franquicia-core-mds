package com.gs.baz.frq.limpieza.remedy.dto;

public class DatosEmpMttoDTO {

    private Integer idUsuario;
    private String nombre;
    private String descripcion;
    private Integer ceco;
    private String telefono;
    private Integer idPuesto;

    public DatosEmpMttoDTO() {
        super();

    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCeco() {
        return ceco;
    }

    public void setCeco(Integer ceco) {
        this.ceco = ceco;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(Integer idPuesto) {
        this.idPuesto = idPuesto;
    }

    @Override
    public String toString() {
        return "DatosEmpMttoDTO [idUsuario=" + idUsuario
                + ", nombre=" + nombre + ", descripcion=" + descripcion
                + ", ceco=" + ceco + ", telefono=" + telefono + ", idPuesto=" + idPuesto + "]";
    }

}
