/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Estructura de salida para descifra algun valor", value = "DescifraResponse")
public class DescifraResponseDTO {

    @JsonProperty(value = "valor")
    @ApiModelProperty(notes = "Valor en texto plano descifrado", example = "dato sensible")
    private String valor;

    public DescifraResponseDTO(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
