/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.mprs;

import com.gs.baz.frq.dashboard.dto.ConteoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kramireza
 */
public class ConteoRowMapper implements RowMapper<ConteoDTO> {

    private ConteoDTO conteoDTO;

    @Override
    public ConteoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        conteoDTO = new ConteoDTO();
        conteoDTO.setConteo((BigDecimal) rs.getObject("CONTEO"));
        return conteoDTO;

    }

}
