package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralSemanalDTO;

/**
 *
 * @author cescobarh
 */
public class ConteoGralSemanalRowMapper implements RowMapper<ReporteConteoGralSemanalDTO> {

    private ReporteConteoGralSemanalDTO status;

    @Override
    public ReporteConteoGralSemanalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteConteoGralSemanalDTO();

        status.setConteoRealizadas(rs.getInt("CONTEO_ECHAS"));
        status.setSemana(rs.getString("SEMANA"));
        status.setConteoProgramadas(rs.getInt("CONTEO_PROGRAMADAS"));

        return status;
    }
}
