package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Aprobacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Authentication;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarAprobaciones;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarAprobacionesResponse;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsCAprobacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._Accion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AprobacionesRemedyLimpiezaHilos extends Thread {

    private Integer idFolio;
    private Integer opcion;
    private Aprobacion respuesta;
    private static final Logger LOGGER = LogManager.getLogger();

    public AprobacionesRemedyLimpiezaHilos(int idFolio, int opcion) {
        this.idFolio = idFolio;
        this.opcion = opcion;
    }

    public Aprobacion getRespuesta() {
        return this.respuesta;
    }

    public int getIdFolio() {
        return this.idFolio;
    }

    @Override
    public void run() {

        consultarAprobaciones(this.idFolio, this.opcion);

    }

    public void consultarAprobaciones(int idIncidente, int opcion) {
        Aprobacion result = null;
        try {

            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(autenticatiosRemedy);

            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(opcion);

            aprobacionesRequest.setTipo(auxB);

            RemedyStub consulta = new RemedyStub();

            ConsultarAprobacionesResponse response = consulta.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            LOGGER.info("AprobacionesRemedyHilos||consultarAprobaciones"
                    + "||Ocurrio algo al consultar la aprobacion el incidente: ", e);
            this.respuesta = null;
        }

        this.respuesta = result;
    }
}
