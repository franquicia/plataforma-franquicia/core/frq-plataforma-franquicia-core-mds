/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de un territorio.", value = "Territorio")
public class Territorio {

    @JsonProperty(value = "idTerritorio")
    @ApiModelProperty(notes = "Identificador del territorio", example = "1", position = -1)
    private Integer idTerritorio;

    @JsonProperty(value = "nombreTerritorio")
    @ApiModelProperty(notes = "Nombre del territorio", example = "TERRITORIAL RU METRO NORTE", position = -2)
    private String nombreTerritorio;

    public Integer getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(Integer idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getNombreTerritorio() {
        return nombreTerritorio;
    }

    public void setNombreTerritorio(String nombreTerritorio) {
        this.nombreTerritorio = nombreTerritorio;
    }

    @Override
    public String toString() {
        return "Territorio{" + "idTerritorio=" + idTerritorio + ", nombreTerritorio=" + nombreTerritorio + '}';
    }
}
