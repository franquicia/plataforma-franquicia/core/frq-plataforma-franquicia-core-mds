/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.usuarios.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.modelo.negocio.usuarios.dao.ModeloNegocioUsuarioDAOImpl;
import com.gs.baz.frq.modelo.negocio.usuarios.dto.ParametrosUsuario;
import com.gs.baz.frq.modelo.negocio.usuarios.dto.Usuario;
import com.gs.baz.frq.modelo.negocio.usuarios.dto.Usuarios;
import com.gs.baz.frq.modelo.negocio.usuarios.dto.UsuariosCorreo;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "usuarios-negocio", value = "usuarios-negocio", description = "Api para busqueda de usuarios")
@RestController
@RequestMapping("/api-local/usuarios-negocio/v1")
public class ModeloNegocioUsuarioApi {

    @Autowired
    private ModeloNegocioUsuarioDAOImpl usuarioDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param usuario
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/{usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuario obtieneUsuarios(@ApiParam(name = "usuario", value = "usuario", example = "1", required = true) @PathVariable("usuario") Integer usuario) throws CustomException, DataNotFoundException {
        Usuario usuarios = usuarioDAOImpl.selectRow(usuario);
        if (usuarios == null) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param parametrosUsuario
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuariosParam(@ApiParam(name = "ParametrosUsuario", value = "Paramentros para la consulta de usuarios", required = true) @RequestBody ParametrosUsuario parametrosUsuario) throws CustomException, DataNotFoundException {
        Usuarios usuarios = new Usuarios(usuarioDAOImpl.selectAllRows(parametrosUsuario.getUsuario()));
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param parametrosUsuario
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene correo usuarios", notes = "Obtiene todos los usuarios con correo", nickname = "obtieneUsuariosCorreo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/correo/busquedas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuariosCorreo obtieneUsuariosCorreoParam(@ApiParam(name = "ParametrosUsuario", value = "Paramentros para la consulta de usuarios", required = true) @RequestBody ParametrosUsuario parametrosUsuario) throws CustomException, DataNotFoundException {

        UsuariosCorreo usuarios = new UsuariosCorreo(usuarioDAOImpl.selectAllMailRows(parametrosUsuario.getUsuario()));

        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

}
