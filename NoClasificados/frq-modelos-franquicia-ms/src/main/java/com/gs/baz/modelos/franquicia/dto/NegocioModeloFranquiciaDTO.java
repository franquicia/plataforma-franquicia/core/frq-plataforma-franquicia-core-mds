/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.modelos.franquicia.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class NegocioModeloFranquiciaDTO {

    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejes_modelo")
    private List<EjeModeloNegocioDTO> ejesNegocio;

    public NegocioModeloFranquiciaDTO() {
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public List<EjeModeloNegocioDTO> getEjesNegocio() {
        return ejesNegocio;
    }

    public void setEjesNegocio(List<EjeModeloNegocioDTO> ejesNegocio) {
        this.ejesNegocio = ejesNegocio;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", ejesNegocio=" + ejesNegocio + '}';
    }

}
