/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.mprs;

import com.gs.baz.security.access.dto.AtributoSensible;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kramireza
 */
public class AtributosSensiblesRowMapper implements RowMapper<AtributoSensible> {

    private AtributoSensible atributoSensible;

    @Override
    public AtributoSensible mapRow(ResultSet rs, int rowNum) throws SQLException {
        atributoSensible = new AtributoSensible();
        //   atributoSensible.setIdAtributo(rs.getInt("FIIDCAMPO"));
        atributoSensible.setNombre(rs.getString("FCNOMBRECAMPO"));
        atributoSensible.setTipo(AtributoSensible.TipoAtributo.valueOf(rs.getString("FCORIGENDATO")));
        atributoSensible.setApiOrigen(rs.getString("FCSCOPEDATO"));
        atributoSensible.setNombreRecurso(rs.getString("FCNOMBRRECURSO"));
        return atributoSensible;
    }

}
