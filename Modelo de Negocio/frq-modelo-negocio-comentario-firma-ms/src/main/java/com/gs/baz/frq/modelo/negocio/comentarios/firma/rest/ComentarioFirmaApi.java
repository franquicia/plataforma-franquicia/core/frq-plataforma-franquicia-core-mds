/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.bi.CorreoFirmaBI;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dao.ComentarioFirmaDAOImpl;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.AltaComentarioFirma;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ComentrioFirmaBase;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ComentriosFirma;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ParametrosComentrioFirma;
import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "ComentariosFirma", value = "ComentariosFirma", description = "Gestión de ComentariosFirma")
@RestController
@RequestMapping("/api-local/modelo-negocio/comentarios-firma/v1")
public class ComentarioFirmaApi {

    @Autowired
    private ComentarioFirmaDAOImpl ComentarioFirmaDAOImpl;

    @Autowired
    private CorreoFirmaBI correoFirmaBI;

    @Autowired
    private HttpServletRequest httpServletRequest;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @param idFirma
     * @param idComentario
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ComentarioFirma", notes = "Obtiene un ComentarioFirma", nickname = "obtieneComentarioFirma")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/comentarios/{idComentario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ComentrioFirmaBase obtieneComentarioFirma(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Long idFirma,
            @ApiParam(name = "idComentario", value = "Identificador del Comentario", example = "1", required = true) @PathVariable("idComentario") Long idComentario) throws CustomException, DataNotFoundException {
        ComentrioFirmaBase ComentarioFirmaBase = ComentarioFirmaDAOImpl.selectRow(idComentario);
        if (ComentarioFirmaBase == null) {
            throw new DataNotFoundException();
        }
        return ComentarioFirmaBase;
    }

    /**
     *
     * @param idFolio
     * @param idFirma
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ComentariosFirma", notes = "Obtiene todos los ComentariosFirma", nickname = "obtieneComentariosFirma")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/comentarios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ComentriosFirma obtieneComentariosFirma(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Long idFirma) throws CustomException, DataNotFoundException {
        ComentriosFirma ComentariosFirma = new ComentriosFirma(ComentarioFirmaDAOImpl.selectAllRows());
        if (ComentariosFirma.getComentariosFirma() != null && ComentariosFirma.getComentariosFirma().isEmpty()) {
            throw new DataNotFoundException();
        }
        return ComentariosFirma;
    }

    /**
     *
     * @param idFirma
     * @param parametrosComentarioFirma
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear ComentarioFirma", notes = "Agrega un ComentarioFirma", nickname = "creaComentarioFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/comentarios", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaComentarioFirma creaComentarioFirma(@RequestHeader HttpHeaders headers, @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio, @ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Integer idFirma,
            @ApiParam(name = "ParametrosComentarioFirma", value = "Paramentros para el alta del ComentarioFirma", required = true) @RequestBody ParametrosComentrioFirma parametrosComentarioFirma) throws DataNotInsertedException, Exception {
        parametrosComentarioFirma.setIdFirma(idFirma);
        correoFirmaBI.enviaCorreo(httpServletRequest, headers, idFolio, idFirma, parametrosComentarioFirma.getComentario(), parametrosComentarioFirma.getIdEstadoFirma());
        return ComentarioFirmaDAOImpl.insertRow(parametrosComentarioFirma);
    }

    /**
     *
     * @param idFirma
     * @param parametrosComentarioFirma
     * @param idComentario
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar ComentarioFirma", notes = "Actualiza un ComentarioFirma", nickname = "actualizaComentarioFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/comentarios/{idComentario}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaComentarioFirma(@ApiParam(name = "idFirma", value = "Identificador del firma", example = "1", required = true) @PathVariable("idFirma") Integer idFirma,
            @ApiParam(name = "ParametrosComentarioFirma", value = "Paramentros para la actualización del ComentarioFirma", required = true) @RequestBody ParametrosComentrioFirma parametrosComentarioFirma, @ApiParam(name = "idComentario", value = "Identificador del Comentario", example = "1", required = true) @PathVariable("idComentario") Integer idComentario) throws DataNotUpdatedException {
        parametrosComentarioFirma.setIdComentario(idComentario);
        parametrosComentarioFirma.setIdFirma(idFirma);
        ComentarioFirmaDAOImpl.updateRow(parametrosComentarioFirma);
        return new SinResultado();
    }

    /**
     *
     * @param idComentarioFirma
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar ComentarioFirma", notes = "Elimina un item de las ComentariosFirma", nickname = "eliminaComentarioFirma")
    @RequestMapping(value = "/{idFolio}/firmas/{idFirma}/comentarios/{idComentario}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaComentarioFirma(@ApiParam(name = "idComentario", value = "Identificador de la Comentario", example = "1", required = true) @PathVariable("idComentario") Integer idComentarioFirma) throws DataNotDeletedException {
        ComentarioFirmaDAOImpl.deleteRow(idComentarioFirma);
        return new SinResultado();
    }

}
