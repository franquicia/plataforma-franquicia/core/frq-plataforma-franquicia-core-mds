/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la Vista", value = "Vista")
public class ParametroVista {

    @JsonProperty(value = "idVista")
    @ApiModelProperty(notes = "Identificador de la vista", example = "1", position = -1)
    @NotNull
    private Integer idVista;

    public ParametroVista() {
    }

    public Integer getIdVista() {
        return idVista;
    }

    public void setIdVista(Integer idVista) {
        this.idVista = idVista;
    }

    @Override
    public String toString() {
        return "Vista{" + "idVista=" + idVista + '}';
    }

}
