/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.handler.reqres.bi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.ErrorMessageDTO;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.OutMessageDTO;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.Respuesta;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.swagger.web.SwaggerResource;

/**
 *
 * @author cescobarh
 */
@ControllerAdvice
public class CustomResponseHandler implements ResponseBodyAdvice<Object> {

    private OutMessageDTO outMessage;
    private ErrorMessageDTO errorMessage;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Logger logger = LogManager.getLogger();
    @Autowired
    HttpServletRequest httpServletRequest;

    /**
     *
     * @param mp
     * @param type
     * @return
     */
    @Override
    public boolean supports(MethodParameter mp, Class<? extends HttpMessageConverter<?>> type) {
        return true;
    }

    /**
     *
     * @param t
     * @param mp
     * @param mt
     * @param type
     * @param request
     * @param response
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object t, MethodParameter mp, MediaType mt, Class<? extends HttpMessageConverter<?>> type, ServerHttpRequest request, ServerHttpResponse response) {
        if (request.getURI().toString().contains("/v2/api-docs")) {
            return t;
        }
        if (t != null && t.getClass().getPackage().getName().startsWith("springfox")) {
            return t;
        }
        if (t instanceof ArrayList) {
            ArrayList list = (ArrayList) t;
            if (list.size() > 0 && list.get(0) instanceof SwaggerResource) {
                return t;
            }
        }
        if (t instanceof RespuestaAPI && !(t instanceof RespuestaLessResult200)) {
            return t;
        }
        if (request.getURI().toString().contains("/api/") || request.getURI().toString().contains("/api-local/")) {
            if (!(t instanceof Resource)) {
                int status = ((ServletServerHttpResponse) response).getServletResponse().getStatus();
                ResponseMessage responseMessage = ResponseMensages.getByStatus(status);
                String requestID = (String) httpServletRequest.getAttribute("requestID");
                Respuesta respuesta = new Respuesta();
                respuesta.setMensaje(responseMessage.getMessage());
                respuesta.setFolio(requestID);
                if (!(t instanceof RespuestaLessResult200)) {
                    respuesta.setResultado(t);
                } else {
                    return new RespuestaLessResult200(responseMessage.getMessage(), requestID);
                }
                return respuesta;
            }
        }
        if (!(t instanceof Resource)) {
            outMessage = new OutMessageDTO();
            if (t == null) {
                outMessage.setStatusCode(HttpStatus.OK.value());
                outMessage.setTrackCode(ModelCodes.DATA_NOT_FOUND.code());
                outMessage.setDescription(ModelCodes.DATA_NOT_FOUND.description());
                t = outMessage;
            } else if (t instanceof ErrorMessageDTO) {
                errorMessage = (ErrorMessageDTO) t;
                outMessage.setStatusCode(errorMessage.getStatusCode());
                outMessage.setError(errorMessage);
                errorMessage.setStatusCode(null);
                t = outMessage;
            } else if (t instanceof String) {
                outMessage.setStatusCode(HttpStatus.OK.value());
                outMessage.setData((String) t);
                try {
                    t = objectMapper.writeValueAsString(outMessage);
                } catch (Exception ex) {
                    logger.info(ex);
                }
            } else if (t instanceof DefaultOAuth2AccessToken) {
                return t;
            } else if (request.getURI().toString().contains("/oauth/token")) {
                return t;
            } else if (!(t instanceof OutMessageDTO)) {
                outMessage.setStatusCode(HttpStatus.OK.value());
                outMessage.setData(t);
                t = outMessage;
            }
        }
        return t;
    }

}
