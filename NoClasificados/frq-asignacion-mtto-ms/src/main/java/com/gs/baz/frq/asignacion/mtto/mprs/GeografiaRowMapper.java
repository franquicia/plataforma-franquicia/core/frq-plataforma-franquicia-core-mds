package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoTerritorioDTO;

/**
 *
 * @author cescobarh
 */
public class GeografiaRowMapper implements RowMapper<ReporteConteoTerritorioDTO> {

    private ReporteConteoTerritorioDTO status;

    @Override
    public ReporteConteoTerritorioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteConteoTerritorioDTO();

        status.setIdCeco(rs.getInt("FCID_CECO"));
        status.setTerritorio(rs.getString("FCTERRITORIO"));
        status.setIdTerritorio(rs.getInt("FCID_TERRITORIO"));
        status.setZona(rs.getString("FCZONA"));
        status.setIdZona(rs.getInt("FCID_ZONA"));
        status.setNombreResponsable(rs.getString("NOMBRERESP"));
        status.setNombreJefeZona(rs.getString("NOMJEFEZONA"));
        status.setIdJefeZona(rs.getInt("IDJEFEZONA"));
        status.setIdResponsable(rs.getInt("IDRESPONSABLE"));

        return status;
    }
}
