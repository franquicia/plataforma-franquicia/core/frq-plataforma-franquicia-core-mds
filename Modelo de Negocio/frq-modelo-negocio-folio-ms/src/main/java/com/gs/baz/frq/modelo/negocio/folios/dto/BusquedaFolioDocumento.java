/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import io.swagger.annotations.ApiModel;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del folio y documento", value = "BusquedaDocumento")
public class BusquedaFolioDocumento extends FolioBase {

    public BusquedaFolioDocumento() {
    }

}
