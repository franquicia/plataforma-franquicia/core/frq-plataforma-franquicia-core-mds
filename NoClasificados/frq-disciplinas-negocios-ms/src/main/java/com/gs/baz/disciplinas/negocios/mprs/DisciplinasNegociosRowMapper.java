package com.gs.baz.disciplinas.negocios.mprs;

import com.gs.baz.disciplinas.negocios.dto.DisciplinaNegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DisciplinasNegociosRowMapper implements RowMapper<DisciplinaNegocioDTO> {

    private DisciplinaNegocioDTO disciplinasNegocios;

    @Override
    public DisciplinaNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        disciplinasNegocios = new DisciplinaNegocioDTO();
        disciplinasNegocios.setIdDisciplinaNegocio(((BigDecimal) rs.getObject("FIIDDISNEG")));
        disciplinasNegocios.setIdDisciplina(((BigDecimal) rs.getObject("FIIDDISC")));
        disciplinasNegocios.setIdDisciplinaEje(((BigDecimal) rs.getObject("FIIDEJEDIS")));
        return disciplinasNegocios;
    }
}
