package com.gs.baz.frq.rangos.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.rangos.dao.util.GenericDAO;
import com.gs.baz.frq.rangos.dto.RangosDTO;
import com.gs.baz.frq.rangos.mprs.RangosRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class RangosDAOImpl extends DefaultDAO implements GenericDAO<RangosDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectRangos;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMRANGOS");
        jdbcInsert.withProcedureName("SPINSRANGOS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMRANGOS");
        jdbcUpdate.withProcedureName("SPACTRANGOS");

        jdbcSelectRangos = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectRangos.withSchemaName(schema);
        jdbcSelectRangos.withCatalogName("PAADMRANGOS");
        jdbcSelectRangos.withProcedureName("SPGETRANGOS");
        jdbcSelectRangos.returningResultSet("PA_CDATOS", new RangosRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMRANGOS");
        jdbcSelectByName.withProcedureName("SPGETRANGOS");
        jdbcSelectByName.returningResultSet("PA_CDATOS", new RangosRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMRANGOS");
        jdbcDelete.withProcedureName("SPDELRANGOS");
    }

    @Override
    public RangosDTO insertRow(RangosDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO.getIdRango());
            mapSqlParameterSource.addValue("PA_FINUMRANGO", entityDTO.getNumeroRango());
            mapSqlParameterSource.addValue("PA_FILIMITE_INF", entityDTO.getLimiteInferior());
            mapSqlParameterSource.addValue("PA_FILIMITE_SUP", entityDTO.getLimiteSuperior());
            mapSqlParameterSource.addValue("PA_FCVALOR_RANGO", entityDTO.getValorRango());
            mapSqlParameterSource.addValue("PA_FCDETALLE", entityDTO.getDetalle());
            mapSqlParameterSource.addValue("PA_FIREG_ACTIVO", entityDTO.getRegistroActivo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Rangos"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Rangos"), ex);
        }
    }

    @Override
    public RangosDTO updateRow(RangosDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO.getIdRango());
            mapSqlParameterSource.addValue("PA_FINUMRANGO", entityDTO.getNumeroRango());
            mapSqlParameterSource.addValue("PA_FILIMITE_INF", entityDTO.getLimiteInferior());
            mapSqlParameterSource.addValue("PA_FILIMITE_SUP", entityDTO.getLimiteSuperior());
            mapSqlParameterSource.addValue("PA_FCVALOR_RANGO", entityDTO.getValorRango());
            mapSqlParameterSource.addValue("PA_FCDETALLE", entityDTO.getDetalle());
            mapSqlParameterSource.addValue("PA_FIREG_ACTIVO", entityDTO.getRegistroActivo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Rango"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Rango"), ex);
        }
    }

    @Override
    public List<RangosDTO> selectRows(Long entityId, Long idNumero) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityId);
        mapSqlParameterSource.addValue("PA_FINUMRANGO", idNumero);
        Map<String, Object> out = jdbcSelectRangos.execute(mapSqlParameterSource);
        return (List<RangosDTO>) out.get("PA_CDATOS");
    }

    @Override
    public RangosDTO deleteRow(RangosDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIRANGO_ID", entityDTO.getIdRango());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Rango"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Rango"), ex);
        }
    }

}
