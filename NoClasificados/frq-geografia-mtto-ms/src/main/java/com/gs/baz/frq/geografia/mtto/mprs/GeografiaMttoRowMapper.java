package com.gs.baz.frq.geografia.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.geografia.mtto.dto.GeografiaMttoDTO;

/**
 *
 * @author cescobarh
 */
public class GeografiaMttoRowMapper implements RowMapper<GeografiaMttoDTO> {

    private GeografiaMttoDTO status;

    @Override
    public GeografiaMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new GeografiaMttoDTO();

        status.setIdGeografia(rs.getInt("FIID_GEOGRA"));;
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        status.setIdNivel(rs.getInt("FIID_NIVEL"));
        status.setIdNivelPadre(rs.getInt("FIID_NIVELPADRE"));
        status.setIdResponsable(rs.getInt("FIID_RESPONSABLE"));
        status.setObservaciones(rs.getString("FCOBSERVACIONES"));
        status.setStatus(rs.getInt("FIID_AUX"));
        status.setDetalle(rs.getString("FCAUX"));
        status.setPeriodo(rs.getString("FCPERIODO"));

        return status;
    }
}
