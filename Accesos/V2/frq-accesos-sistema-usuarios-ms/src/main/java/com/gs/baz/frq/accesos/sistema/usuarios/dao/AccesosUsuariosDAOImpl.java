/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.dao;

import com.gs.baz.frq.accesos.sistema.usuarios.dto.AltaUsuario;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.AltaUsuarioVistas;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.ParametroVista;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.ParametrosUsuario;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.ParametrosUsuarioVistas;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.Usuario;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.UsuarioBase;
import com.gs.baz.frq.accesos.sistema.usuarios.mprs.UsuarioRowMapper;
import com.gs.baz.frq.accesos.sistema.usuarios.mprs.UsuariosRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class AccesosUsuariosDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectBusqueda;
    private DefaultJdbcCall jdbcSelectUsuariosPerfil;
    private DefaultJdbcCall jdbcSelectUsuariosFijosPerfil;
    private DefaultJdbcCall jdbcSelectUsuariosDisponiblesPerfil;
    private DefaultJdbcCall jdbcSelectUsuariosVista;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcDeleteUsuarioVistas;
    private DefaultJdbcCall jdbcDeleteUsuarioVista;
    private DefaultJdbcCall jdbcInsertUsuarioVistas;
    private final String schema = "MODFRANQ";

    public void init() {
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAUSUARIO");
        jdbcInsert.withProcedureName("SPINSAUSUARIO");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAUSUARIO");
        jdbcUpdate.withProcedureName("SPACTAUSUARIO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAUSUARIO");
        jdbcDelete.withProcedureName("SPDELAUSUARIO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAUSUARIO");
        jdbcSelect.withProcedureName("SPGETAUSUARIO");
        jdbcSelect.returningResultSet("PA_CDATOS", new UsuarioRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAADMAUSUARIO");
        jdbcSelectAll.withProcedureName("SPGETAUSUARIOALL");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new UsuariosRowMapper());

        jdbcSelectBusqueda = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectBusqueda.withSchemaName(schema);
        jdbcSelectBusqueda.withCatalogName("PAADMAUSUARIO");
        jdbcSelectBusqueda.withProcedureName("SPGETABUSQUEDAUSUARIOS");
        jdbcSelectBusqueda.returningResultSet("PA_CDATOS", new UsuariosRowMapper());

        jdbcSelectUsuariosPerfil = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectUsuariosPerfil.withSchemaName(schema);
        jdbcSelectUsuariosPerfil.withCatalogName("PAADMAUSUARIO");
        jdbcSelectUsuariosPerfil.withProcedureName("SPGETUSUAXPERF");
        jdbcSelectUsuariosPerfil.returningResultSet("PA_CDATOS", new UsuariosRowMapper());

        jdbcSelectUsuariosFijosPerfil = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectUsuariosFijosPerfil.withSchemaName(schema);
        jdbcSelectUsuariosFijosPerfil.withCatalogName("PAADMAUSUARIO");
        jdbcSelectUsuariosFijosPerfil.withProcedureName("SPGETUSUAFIJOSXPERF");
        jdbcSelectUsuariosFijosPerfil.returningResultSet("PA_CDATOS", new UsuariosRowMapper());

        jdbcSelectUsuariosDisponiblesPerfil = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectUsuariosDisponiblesPerfil.withSchemaName(schema);
        jdbcSelectUsuariosDisponiblesPerfil.withCatalogName("PAADMAUSUARIO");
        jdbcSelectUsuariosDisponiblesPerfil.withProcedureName("SPGETUSUANOPERF");
        jdbcSelectUsuariosDisponiblesPerfil.returningResultSet("PA_CDATOS", new UsuariosRowMapper());

        jdbcSelectUsuariosVista = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectUsuariosVista.withSchemaName(schema);
        jdbcSelectUsuariosVista.withCatalogName("PAADMAUSUARIO");
        jdbcSelectUsuariosVista.withProcedureName("SPGETUSUAVIS");
        jdbcSelectUsuariosVista.returningResultSet("PA_CDATOS", new UsuariosRowMapper());

        jdbcDeleteUsuarioVistas = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteUsuarioVistas.withSchemaName(schema);
        jdbcDeleteUsuarioVistas.withCatalogName("PAADMDVISUS");
        jdbcDeleteUsuarioVistas.withProcedureName("SPDELDVISSUSU");

        jdbcDeleteUsuarioVista = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDeleteUsuarioVista.withSchemaName(schema);
        jdbcDeleteUsuarioVista.withCatalogName("PAADMDVISUS");
        jdbcDeleteUsuarioVista.withProcedureName("SPDELDVISUSU");

        jdbcInsertUsuarioVistas = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsertUsuarioVistas.withSchemaName(schema);
        jdbcInsertUsuarioVistas.withCatalogName("PAADMDVISUS");
        jdbcInsertUsuarioVistas.withProcedureName("SPINSDVISUSU");

    }

    public UsuarioBase selectRow(Long entityID) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<UsuarioBase> data = (List<UsuarioBase>) out.get("PA_CDATOS");
        if (data != null && data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Usuario> selectBusqueda(String empleado) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCUSUARIO", empleado);
        Map<String, Object> out = jdbcSelectBusqueda.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public List<Usuario> selectAllRows() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUARIO", null);
        mapSqlParameterSource.addValue("PA_FCNOMBRE", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public List<Usuario> selectAllUsuariosFijosPerfil(Integer idPerfil) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", idPerfil);
        Map<String, Object> out = jdbcSelectUsuariosFijosPerfil.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public List<Usuario> selectAllUsuariosPerfil(Integer idPerfil) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", idPerfil);
        Map<String, Object> out = jdbcSelectUsuariosPerfil.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public List<Usuario> selectAllUsuariosDisponibles(Integer idPerfil, String empleado) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", idPerfil);
        mapSqlParameterSource.addValue("PA_FCUSUARIO", empleado);
        Map<String, Object> out = jdbcSelectUsuariosDisponiblesPerfil.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public List<Usuario> selectAllUsuariosVista(Integer idVista) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDVISTA", idVista);
        Map<String, Object> out = jdbcSelectUsuariosVista.execute(mapSqlParameterSource);
        return (List<Usuario>) out.get("PA_CDATOS");
    }

    public AltaUsuario insertRow(ParametrosUsuario entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getNumeroEmpleado());
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", null);
            mapSqlParameterSource.addValue("PA_FCTIPOEMPLEADO", entityDTO.getTipoEmpleado());
            mapSqlParameterSource.addValue("PA_FCPUESTO", entityDTO.getPuesto());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombreUsuario());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getCentroCostos());
            mapSqlParameterSource.addValue("PA_FCCORREO", entityDTO.getCorreo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaUsuario(dato.intValue());
            } else {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosUsuario entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getNumeroEmpleado());
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", null);
            mapSqlParameterSource.addValue("PA_FCTIPOEMPLEADO", entityDTO.getTipoEmpleado());
            mapSqlParameterSource.addValue("PA_FCPUESTO", entityDTO.getPuesto());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombreUsuario());
            mapSqlParameterSource.addValue("PA_FDFECHAULTIMAI", null);
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getCentroCostos());
            mapSqlParameterSource.addValue("PA_FCCORREO", entityDTO.getCorreo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (!success) {
                throw new DataNotUpdatedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRowUsuarioVista(Integer numeroEmpleado, Integer idVista) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDVISTA", idVista);
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
            Map<String, Object> out = jdbcDeleteUsuarioVista.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (!success) {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public AltaUsuarioVistas insertRowUsuarioVistas(ParametrosUsuarioVistas parametrosUsuarioVistas) throws DataNotInsertedException {
        try {
            this.deleteRowUsuarioVistas(parametrosUsuarioVistas.getNumeroEmpleado());
        } catch (DataNotDeletedException ex) {
            throw new DataNotInsertedException(ex);
        }
        for (ParametroVista parametroVista : parametrosUsuarioVistas.getVistas()) {
            this.insertRowUsuarioVista(parametrosUsuarioVistas.getNumeroEmpleado(), parametroVista);
        }
        return new AltaUsuarioVistas(parametrosUsuarioVistas.getNumeroEmpleado());
    }

    private AltaUsuarioVistas insertRowUsuarioVista(Integer numeroEmpleado, ParametroVista parametroVista) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
            mapSqlParameterSource.addValue("PA_FIIDVISTA", parametroVista.getIdVista());
            Map<String, Object> out = jdbcInsertUsuarioVistas.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                BigDecimal dato = (BigDecimal) out.get("PA_NRESEJECUCION");
                return new AltaUsuarioVistas(dato.intValue());
            } else {
                throw new DataNotInsertedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void deleteRowUsuarioVistas(Integer numeroEmpleado) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", numeroEmpleado);
            Map<String, Object> out = jdbcDeleteUsuarioVistas.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (!success) {
                throw new DataNotDeletedException();
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

}
