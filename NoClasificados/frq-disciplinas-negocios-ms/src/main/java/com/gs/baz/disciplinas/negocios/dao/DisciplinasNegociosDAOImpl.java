package com.gs.baz.disciplinas.negocios.dao;

import com.gs.baz.disciplinas.negocios.dao.util.GenericDAO;
import com.gs.baz.disciplinas.negocios.dto.DisciplinaNegocioDTO;
import com.gs.baz.disciplinas.negocios.mprs.DisciplinasNegociosRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class DisciplinasNegociosDAOImpl extends DefaultDAO implements GenericDAO<DisciplinaNegocioDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINDISCNEG");
        jdbcInsert.withProcedureName("SP_INS_DISCNEG");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINDISCNEG");
        jdbcUpdate.withProcedureName("SP_ACT_DISCNEG");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINDISCNEG");
        jdbcSelect.withProcedureName("SP_SEL_DISCNEG");
        jdbcSelect.returningResultSet("RCL_INFO", new DisciplinasNegociosRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINDISCNEG");
        jdbcDelete.withProcedureName("SP_DEL_DISCNEG");
    }

    @Override
    public List<DisciplinaNegocioDTO> selectRow(Long idModelo, Long idEjeModelo, Long idNegocio) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDMODELO", idModelo);
        mapSqlParameterSource.addValue("PA_FIIDNEGOCIO", idNegocio);
        mapSqlParameterSource.addValue("PA_FIIDMODEJE", idEjeModelo);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<DisciplinaNegocioDTO> data = (List<DisciplinaNegocioDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
