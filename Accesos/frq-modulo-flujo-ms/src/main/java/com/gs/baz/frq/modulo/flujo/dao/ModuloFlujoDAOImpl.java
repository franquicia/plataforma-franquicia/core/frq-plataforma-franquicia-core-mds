/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modulo.flujo.dao;

/**
 *
 * @author jfernandor
 */
import com.gs.baz.frq.modulo.flujo.dto.ModuloFlujoDTO;
import com.gs.baz.frq.modulo.flujo.mprs.ModuloFlujoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class ModuloFlujoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectParent;
    private DefaultJdbcCall jdbcSelectFlujo;
    private DefaultJdbcCall jdbcSelectParentArbolAsc;
    private DefaultJdbcCall jdbcSelectParentArbolDesc;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAFLUJO_MO");
        jdbcInsert.withProcedureName("SPINSAFLUJO_MOD");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAFLUJO_MO");
        jdbcSelect.withProcedureName("SPGETAFLUJO_MOD");
        jdbcSelect.returningResultSet("PA_CDATOS", new ModuloFlujoRowMapper());

        jdbcSelectParent = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectParent.withSchemaName(schema);
        jdbcSelectParent.withCatalogName("PAADMAFLUJO_MO");
        jdbcSelectParent.withProcedureName("SPGETAFLUJO_PARENT_MOD");
        jdbcSelectParent.returningResultSet("PA_CDATOS", new ModuloFlujoRowMapper());

        jdbcSelectParentArbolAsc = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectParentArbolAsc.withSchemaName(schema);
        jdbcSelectParentArbolAsc.withCatalogName("PAADMAFLUJO_MO");
        jdbcSelectParentArbolAsc.withProcedureName("SPGETFLUJOARBOLASC");
        jdbcSelectParentArbolAsc.returningResultSet("PA_CDATOS", new ModuloFlujoRowMapper());

        jdbcSelectParentArbolDesc = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectParentArbolDesc.withSchemaName(schema);
        jdbcSelectParentArbolDesc.withCatalogName("PAADMAFLUJO_MO");
        jdbcSelectParentArbolDesc.withProcedureName("SPGETFLUJOARBOLDESC");
        jdbcSelectParentArbolDesc.returningResultSet("PA_CDATOS", new ModuloFlujoRowMapper());

        jdbcSelectFlujo = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFlujo.withSchemaName(schema);
        jdbcSelectFlujo.withCatalogName("PAADMAFLUJO_MO");
        jdbcSelectFlujo.withProcedureName("SPGETAFLUJO_FLUJO_MOD");
        jdbcSelectFlujo.returningResultSet("PA_CDATOS", new ModuloFlujoRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAFLUJO_MO");
        jdbcUpdate.withProcedureName("SPACTAFLUJO_MOD");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAFLUJO_MO");
        jdbcDelete.withProcedureName("SPDELAFLUJO_MOD");
    }

    public ModuloFlujoDTO selectRow(Integer entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ModuloFlujoDTO> data = (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<ModuloFlujoDTO> selectRowsByFlujo(Integer flujoID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_FLUJO", flujoID);
        Map<String, Object> out = jdbcSelectFlujo.execute(mapSqlParameterSource);
        return (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
    }

    public List<ModuloFlujoDTO> selectRowsByFlujoParent(Integer flujoID, Integer entityParentID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_FLUJO", flujoID);
        mapSqlParameterSource.addValue("PA_FIID_PARENT", entityParentID);
        Map<String, Object> out = jdbcSelectParent.execute(mapSqlParameterSource);
        return (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
    }

    public List<ModuloFlujoDTO> selectRowsTreeHierarchicalAsc(Integer flujoID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", flujoID);
        Map<String, Object> out = jdbcSelectParentArbolAsc.execute(mapSqlParameterSource);
        return (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
    }

    public List<ModuloFlujoDTO> selectRowsTreeHierarchicalDesc(Integer flujoID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", flujoID);
        Map<String, Object> out = jdbcSelectParentArbolDesc.execute(mapSqlParameterSource);
        return (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
    }

    public List<ModuloFlujoDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
    }

    public ModuloFlujoDTO insertRow(ModuloFlujoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", entityDTO.getIdFlujoModulo());
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FIID_MODULO", entityDTO.getIdModulo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIID_ORDEN", entityDTO.getOrden());
            mapSqlParameterSource.addValue("PA_FIID_PARENT", entityDTO.getIdParent());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FIDINAMICO", entityDTO.getOcultoMenu() ? 1 : 0);
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setIdFlujoModulo((BigDecimal) out.get("PA_NRESEJECUCION"));
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Modulo Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Modulo Flujo"), ex);
        }
    }

    public ModuloFlujoDTO updateRow(ModuloFlujoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", entityDTO.getIdFlujoModulo());
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FIID_MODULO", entityDTO.getIdModulo());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIID_PARENT", entityDTO.getIdParent());
            mapSqlParameterSource.addValue("PA_FIID_ORDEN", entityDTO.getOrden());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            mapSqlParameterSource.addValue("PA_FIDINAMICO", entityDTO.getOcultoMenu() ? 1 : 0);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Modulo Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Modulo Flujo"), ex);
        }
    }

    public ModuloFlujoDTO deleteRow(ModuloFlujoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFLUJOMODULO", entityDTO.getIdFlujoModulo());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Modulo Flujo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Modulo Flujo"), ex);
        }
    }

}
