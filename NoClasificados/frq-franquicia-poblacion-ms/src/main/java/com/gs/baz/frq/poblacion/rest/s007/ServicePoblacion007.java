/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.poblacion.rest.s007;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.poblacion.dao.d007.PoblacionDAOImpl007;
import com.gs.baz.frq.poblacion.dto.PoblacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luisgomez
 */
@RestController
@RequestMapping("/service/franquicia/poblacion/")
@Component("ServicePoblacion007")
public class ServicePoblacion007 {

    @Autowired
    private PoblacionDAOImpl007 poblacionDAOImpl;

    @RequestMapping(value = "secure/resumen/visita/merge/visita", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PoblacionDTO poblacionDTO(@RequestBody PoblacionDTO poblacionDTO) throws CustomException {
        return poblacionDAOImpl.executeStore(poblacionDTO);
    }

}
