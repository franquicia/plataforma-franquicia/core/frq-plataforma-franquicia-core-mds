/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.BandejaFranquicia;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.BandejaProveedor;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostos;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostosParametros;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostosProveedor;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.DashboardProveedor;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.Territorio;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.BandejasFranquiciaLayoutRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.BandejasProveedoresLayoutRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.CecosProveedoresLayoutRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.CentroCostosLayoutRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.DashboardProveedoresLayoutRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.TerritoriosLayoutRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionCecosLayoutDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertCecoLayout;
    private DefaultJdbcCall jdbcUpdateCecoLayout;
    private DefaultJdbcCall jdbcSelectCecoLayout;
    //private DefaultJdbcCall jdbcSelectCecoLayout;
    private DefaultJdbcCall jdbcDeleteCecoLayout;
    
    private DefaultJdbcCall jdbcSelectDashboardProveedor;
    private DefaultJdbcCall jdbcSelectBandejasProveedor;
    private DefaultJdbcCall jdbcSelectCecosProveedor;
    
    private DefaultJdbcCall jdbcSelectBandejasFranquicia;
    
    
    private DefaultJdbcCall jdbcSelectTerritorios;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMLYCECOLAY";
        jdbcInsertCecoLayout = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertCecoLayout.withSchemaName(schema);
        jdbcInsertCecoLayout.withCatalogName(catalogo);
        jdbcInsertCecoLayout.withProcedureName("SPINSLYCECOLAYOUT");

        jdbcUpdateCecoLayout = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateCecoLayout.withSchemaName(schema);
        jdbcUpdateCecoLayout.withCatalogName(catalogo);
        jdbcUpdateCecoLayout.withProcedureName("SPACTLYCECOLAYOUT");

        jdbcDeleteCecoLayout = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteCecoLayout.withSchemaName(schema);
        jdbcDeleteCecoLayout.withCatalogName(catalogo);
        jdbcDeleteCecoLayout.withProcedureName("SPDELLYCECOLAYOUT");

        jdbcSelectCecoLayout = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCecoLayout.withSchemaName(schema);
        jdbcSelectCecoLayout.withCatalogName(catalogo);
        jdbcSelectCecoLayout.withProcedureName("SPGETLYCECOLAYOUT");
        jdbcSelectCecoLayout.returningResultSet("PA_CDATOS", new CentroCostosLayoutRowMapper());

        jdbcSelectCecoLayout = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCecoLayout.withSchemaName(schema);
        jdbcSelectCecoLayout.withCatalogName(catalogo);
        jdbcSelectCecoLayout.withProcedureName("SPGETLYCECOLAYOUT");
        jdbcSelectCecoLayout.returningResultSet("PA_CDATOS", new CentroCostosLayoutRowMapper());
        
        
        
//API de proveedores 
//getDashboardProveedor
        jdbcSelectDashboardProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDashboardProveedor.withSchemaName(schema);
        jdbcSelectDashboardProveedor.withCatalogName(catalogo);
        jdbcSelectDashboardProveedor.withProcedureName("SPGETCONTPROV");
        jdbcSelectDashboardProveedor.returningResultSet("PA_CDATOS", new DashboardProveedoresLayoutRowMapper());
        
        jdbcSelectBandejasProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectBandejasProveedor.withSchemaName(schema);
        jdbcSelectBandejasProveedor.withCatalogName(catalogo);
        jdbcSelectBandejasProveedor.withProcedureName("SPGETDASHPROV");
        jdbcSelectBandejasProveedor.returningResultSet("PA_CDATOS", new BandejasProveedoresLayoutRowMapper()); 
        
        jdbcSelectCecosProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCecosProveedor.withSchemaName(schema);
        jdbcSelectCecosProveedor.withCatalogName(catalogo);
        jdbcSelectCecosProveedor.withProcedureName("SPGETCECOPROV");
        jdbcSelectCecosProveedor.returningResultSet("PA_CDATOS", new CecosProveedoresLayoutRowMapper());
        
        //API de Franquicia
        //getBandejasFranquicia
        
        jdbcSelectBandejasFranquicia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectBandejasFranquicia.withSchemaName(schema);
        jdbcSelectBandejasFranquicia.withCatalogName(catalogo);
        jdbcSelectBandejasFranquicia.withProcedureName("SPGETARCHIVOS");
        jdbcSelectBandejasFranquicia.returningResultSet("PA_CDATOS", new BandejasFranquiciaLayoutRowMapper()); 
        
        
        
        jdbcSelectTerritorios = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectTerritorios.withSchemaName(schema);
        jdbcSelectTerritorios.withCatalogName(catalogo);
        jdbcSelectTerritorios.withProcedureName("SPGETTERRI");
        jdbcSelectTerritorios.returningResultSet("PA_CDATOS", new TerritoriosLayoutRowMapper()); 
        
    }

    public CentroCostos selectRow(CentroCostosParametros bean) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_CECO", bean.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", bean.getIdProveedor());
            mapSqlParameterSource.addValue("PA_FIIDCERTIFICADOR", bean.getIdUsuario());
        Map<String, Object> out = jdbcSelectCecoLayout.execute(mapSqlParameterSource);
        List<CentroCostos> data = (List<CentroCostos>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }


    public List<CentroCostos> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_CECO", null);
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", null);
            mapSqlParameterSource.addValue("PA_FIIDCERTIFICADOR", null);
        Map<String, Object> out = jdbcSelectCecoLayout.execute(mapSqlParameterSource);
        return (List<CentroCostos>) out.get("PA_CDATOS");
    }

    public void insertRow(CentroCostos entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", entityDTO.getIdProveedor());
            mapSqlParameterSource.addValue("PA_FIIDCERTIFICADOR", entityDTO.getNumeroEmpleadoCertificador());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            Map<String, Object> out = jdbcInsertCecoLayout.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } 
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(CentroCostos entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", entityDTO.getIdProveedor());
            mapSqlParameterSource.addValue("PA_FIIDCERTIFICADOR", entityDTO.getNumeroEmpleadoCertificador());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            Map<String, Object> out = jdbcUpdateCecoLayout.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer idCeco, Integer idProveedor, Integer idCertificador) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_CECO", idCeco);
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", idProveedor);
            mapSqlParameterSource.addValue("PA_FIIDCERTIFICADOR", idCertificador);
            Map<String, Object> out = jdbcDeleteCecoLayout.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }
    
    
    public List<DashboardProveedor> selectDashboardProveedores(Integer idProveedor, Integer idEstatus) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", idProveedor);
            mapSqlParameterSource.addValue("PA_STATUS", idEstatus);
        Map<String, Object> out = jdbcSelectDashboardProveedor.execute(mapSqlParameterSource);
        return (List<DashboardProveedor>) out.get("PA_CDATOS");
        
       
    }
    
    
    public List<BandejaProveedor> selectBandejasProveedores(Integer idProveedor) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", idProveedor);
        Map<String, Object> out = jdbcSelectBandejasProveedor.execute(mapSqlParameterSource);
        return (List<BandejaProveedor>) out.get("PA_CDATOS");
        
       
    }
    
    
   public List<CentroCostosProveedor> selectCecosProveedores(Integer idProveedor, Integer idEstatus) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", idProveedor);
            mapSqlParameterSource.addValue("PA_STATUS", idEstatus);
        Map<String, Object> out = jdbcSelectCecosProveedor.execute(mapSqlParameterSource);
        return (List<CentroCostosProveedor>) out.get("PA_CDATOS");
        
       
    }
    
     public List<DashboardProveedor> selectDashboardFranquicia() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPROVEEDOR", null);
            mapSqlParameterSource.addValue("PA_STATUS", null);
        Map<String, Object> out = jdbcSelectDashboardProveedor.execute(mapSqlParameterSource);
        return (List<DashboardProveedor>) out.get("PA_CDATOS");
        
       
    }
     
      public List<BandejaFranquicia> selectBandejasFranquicia(Integer centroCostos, Integer idProveedor) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCIDCECO", centroCostos);
            mapSqlParameterSource.addValue("PA_PROVEEDOR", idProveedor);
        Map<String, Object> out = jdbcSelectBandejasFranquicia.execute(mapSqlParameterSource);
        return (List<BandejaFranquicia>) out.get("PA_CDATOS");
        
       
    }

      public List<Territorio> selectTerritorios() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_TERRI", null);
        Map<String, Object> out = jdbcSelectTerritorios.execute(mapSqlParameterSource);
        return (List<Territorio>) out.get("PA_CDATOS");
    }
      
      
      public List<Territorio> selectTerritorio(Integer territorio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_TERRI", territorio);
        Map<String, Object> out = jdbcSelectTerritorios.execute(mapSqlParameterSource);
        return (List<Territorio>) out.get("PA_CDATOS");
    }
}
