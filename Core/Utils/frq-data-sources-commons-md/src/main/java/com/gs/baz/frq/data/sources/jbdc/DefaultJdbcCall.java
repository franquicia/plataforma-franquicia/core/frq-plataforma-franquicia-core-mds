package com.gs.baz.frq.data.sources.jbdc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.data.sources.pg.JdbcTemplatePg;
import com.gs.baz.frq.data.sources.pg.SimpleJdbcCallPg;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author cescobarh
 */
public class DefaultJdbcCall extends SimpleJdbcCallPg {

    private String params;
    private final Logger LOGGER = LogManager.getLogger(this.getClass());
    private DefaultJdbcCallOrcl defaultJdbcCallOrcl;

    public DefaultJdbcCall(Object jdbcTemplate) {
        super(jdbcTemplate instanceof JdbcTemplatePg ? (JdbcTemplatePg) jdbcTemplate : null);
        if (jdbcTemplate instanceof JdbcTemplate) {
            this.defaultJdbcCallOrcl = new DefaultJdbcCallOrcl((JdbcTemplate) jdbcTemplate);
        }
    }

    @Override
    public SimpleJdbcCallPg withProcedureName(String procedureName) {
        setProcedureName(procedureName);
        setFunction(false);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withProcedureName(procedureName);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg withFunctionName(String functionName) {
        setProcedureName(functionName);
        setFunction(true);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withFunctionName(functionName);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg withSchemaName(String schemaName) {
        setSchemaName(schemaName);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withSchemaName(schemaName);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg withCatalogName(String catalogName) {
        setCatalogName(catalogName);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withCatalogName(catalogName);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg withReturnValue() {
        setReturnValueRequired(true);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withReturnValue();
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg declareParameters(SqlParameter... sqlParameters) {
        for (SqlParameter sqlParameter : sqlParameters) {
            if (sqlParameter != null) {
                addDeclaredParameter(sqlParameter);
            }
        }
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.declareParameters(sqlParameters);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg useInParameterNames(String... inParameterNames) {
        setInParameterNames(new LinkedHashSet<>(Arrays.asList(inParameterNames)));
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.useInParameterNames(inParameterNames);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg returningResultSet(String parameterName, RowMapper<?> rowMapper) {
        addDeclaredRowMapper(parameterName, rowMapper);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.returningResultSet(parameterName, rowMapper);
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg withoutProcedureColumnMetaDataAccess() {
        setAccessCallParameterMetaData(false);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withoutProcedureColumnMetaDataAccess();
        }
        return this;
    }

    @Override
    public SimpleJdbcCallPg withNamedBinding() {
        setNamedBinding(true);
        if (this.defaultJdbcCallOrcl != null) {
            this.defaultJdbcCallOrcl.withNamedBinding();
        }
        return this;
    }

    @Override
    public <T> T executeFunction(Class<T> returnType, Object... args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.executeFunction(returnType, args);
        }
        return (T) doExecute(args).get(getScalarOutParameterName());
    }

    @Override
    public <T> T executeFunction(Class<T> returnType, Map<String, ?> args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.executeFunction(returnType, args);
        }
        return (T) doExecute(args).get(getScalarOutParameterName());
    }

    @Override
    public <T> T executeFunction(Class<T> returnType, SqlParameterSource args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.executeFunction(returnType, args);
        }
        return (T) doExecute(args).get(getScalarOutParameterName());
    }

    @Override
    public <T> T executeObject(Class<T> returnType, Object... args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.executeObject(returnType, args);
        }
        return (T) doExecute(args).get(getScalarOutParameterName());
    }

    @Override
    public <T> T executeObject(Class<T> returnType, Map<String, ?> args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.executeObject(returnType, args);
        }
        return (T) doExecute(args).get(getScalarOutParameterName());
    }

    @Override
    public <T> T executeObject(Class<T> returnType, SqlParameterSource args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.executeObject(returnType, args);
        }
        return (T) doExecute(args).get(getScalarOutParameterName());
    }

    @Override
    public Map<String, Object> execute(Object... args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.execute(args);
        }
        return doExecute(args);
    }

    @Override
    public Map<String, Object> execute(Map<String, ?> args) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.execute(args);
        }
        return doExecute(args);
    }

    @Override
    public Map<String, Object> execute(SqlParameterSource parameterSource) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.execute(parameterSource);
        }
        MapSqlParameterSource param = (MapSqlParameterSource) parameterSource;
        return execute(param);
    }

    public Map<String, Object> execute(MapSqlParameterSource in) {
        if (defaultJdbcCallOrcl != null) {
            return defaultJdbcCallOrcl.execute(in);
        }
        final StopWatch stopWatch = new StopWatch();
        params = "";
        stopWatch.start();
        String catalogName = super.getCatalogName();
        if (catalogName != null) {
            super.withProcedureName(catalogName + "$" + super.getProcedureName());
            super.withCatalogName(null);
        }
        Map outMap = super.execute(in);
        stopWatch.stop();
        super.getCallParameters().forEach((sqlParameter) -> {
            Object value = null;
            try {
                value = in.getValue(sqlParameter.getName());
            } catch (Exception ex) {
            }
            params += sqlParameter.getName() + " = " + value + ", ";
        });
        String queryExecuted = "{call " + super.getSchemaName() + "." + super.getCatalogName() + "." + super.getProcedureName() + "(" + params + ")" + "}";
        queryExecuted = queryExecuted.replace(", )", ")");
        HttpServletRequest httpServletRequest;
        try {
            httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        } catch (Exception ex) {
            httpServletRequest = null;
        }
        if (httpServletRequest != null) {
            ObjectNode info = new ObjectMapper().createObjectNode();
            List<ObjectNode> traceTrackTime = (List<ObjectNode>) httpServletRequest.getAttribute("traceTrackTimeMethods");
            if (traceTrackTime != null) {
                long time = stopWatch.getTotalTimeMillis();
                String infoTracking = "com.gs.baz.frq.model.commons.DefaultJdbcCall.execute";
                info.put("method", infoTracking);
                info.put("time", time);
                traceTrackTime.add(info);
            }
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            if (requestID != null) {
                String logInfo = "OutHTTPRequest: false, RequestID: " + requestID + ", Query ejecutado: " + queryExecuted + " Tiempo de ejecucion Total: " + stopWatch.getTotalTimeMillis() + " Milisegundos ";
                if (!queryExecuted.toLowerCase().contains("err")) {
                    LOGGER.log(Level.INFO, logInfo);
                }
            }
        } else {
            String logInfo = "OutHTTPRequest: true, Query ejecutado: " + queryExecuted + " Tiempo de ejecucion Total: " + stopWatch.getTotalTimeMillis() + " Milisegundos ";
            if (!logInfo.toLowerCase().contains("err")) {
                LOGGER.log(Level.INFO, logInfo);
            }
        }
        return outMap;
    }

    private class DefaultJdbcCallOrcl extends SimpleJdbcCall {

        public DefaultJdbcCallOrcl(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate);
        }

        public Map<String, Object> execute(MapSqlParameterSource in) {
            final StopWatch stopWatch = new StopWatch();
            params = "";
            stopWatch.start();
            Map outMap = super.execute(in);
            stopWatch.stop();
            super.getCallParameters().forEach((sqlParameter) -> {
                Object value = null;
                try {
                    value = in.getValue(sqlParameter.getName());
                } catch (Exception ex) {
                }
                params += sqlParameter.getName() + " = " + value + ", ";
            });
            String queryExecuted = "{call " + super.getSchemaName() + "." + super.getCatalogName() + "." + super.getProcedureName() + "(" + params + ")" + "}";
            queryExecuted = queryExecuted.replace(", )", ")");
            HttpServletRequest httpServletRequest;
            try {
                httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            } catch (Exception ex) {
                httpServletRequest = null;
            }
            if (httpServletRequest != null) {
                ObjectNode info = new ObjectMapper().createObjectNode();
                List<ObjectNode> traceTrackTime = (List<ObjectNode>) httpServletRequest.getAttribute("traceTrackTimeMethods");
                if (traceTrackTime != null) {
                    long time = stopWatch.getTotalTimeMillis();
                    String infoTracking = "com.gs.baz.frq.model.commons.DefaultJdbcCall.execute";
                    info.put("method", infoTracking);
                    info.put("time", time);
                    traceTrackTime.add(info);
                }
                String requestID = (String) httpServletRequest.getAttribute("requestID");
                if (requestID != null) {
                    String logInfo = "OutHTTPRequest: false, RequestID: " + requestID + ", Query ejecutado: " + queryExecuted + " Tiempo de ejecucion Total: " + stopWatch.getTotalTimeMillis() + " Milisegundos ";
                    if (!queryExecuted.toLowerCase().contains("err")) {
                        LOGGER.log(Level.INFO, logInfo);
                    }
                }
            } else {
                String logInfo = "OutHTTPRequest: true, Query ejecutado: " + queryExecuted + " Tiempo de ejecucion Total: " + stopWatch.getTotalTimeMillis() + " Milisegundos ";
                if (!logInfo.toLowerCase().contains("err")) {
                    LOGGER.log(Level.INFO, logInfo);
                }
            }
            return outMap;
        }
    }

}
