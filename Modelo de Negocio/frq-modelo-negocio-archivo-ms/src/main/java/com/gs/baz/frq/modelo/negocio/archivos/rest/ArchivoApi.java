/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.modelo.negocio.archivos.bi.ArchivoBI;
import com.gs.baz.frq.modelo.negocio.archivos.dto.AltaArchivo;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivoDirectorioBase;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ArchivosVirtuales;
import com.gs.baz.frq.modelo.negocio.archivos.dto.ParametrosArchivo;
import com.gs.baz.frq.modelo.negocio.archivos.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.naming.OperationNotSupportedException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "archivos", value = "archivos", description = "Gestión de archivos")
@RestController
@RequestMapping("/api-local/modelo-negocio/folio-archivos/v1")
public class ArchivoApi {

    @Autowired
    private ArchivoBI archivoBI;

    @Autowired
    HttpServletRequest httpServletRequest;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @param uid
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivo", notes = "Obtiene un archivo", nickname = "obtieneArchivo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/archivos/{uid}/contenido", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivoDirectorioBase obtieneArchivo(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "uid", value = "Identificador del archvo", example = "1", required = true) @PathVariable("uid") Long uid) throws CustomException, DataNotFoundException {
        ArchivoDirectorioBase archivoBase = archivoBI.consultaArchivo(idFolio, uid);
        if (archivoBase == null) {
            throw new DataNotFoundException();
        }
        return archivoBase;
    }

    /**
     *
     * @param idFolio
     * @param uid
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivo", notes = "Obtiene un archivo", nickname = "obtieneArchivo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/archivos/originales/{uid}/contenido", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivoDirectorioBase obtieneOriginalArchivo(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "uid", value = "Identificador del archvo", example = "1", required = true) @PathVariable("uid") Long uid) throws CustomException, DataNotFoundException {
        ArchivoDirectorioBase archivoBase = archivoBI.consultaArchivoOrignal(idFolio, uid);
        if (archivoBase == null) {
            throw new DataNotFoundException();
        }
        return archivoBase;
    }

    /**
     *
     * @param idFolio
     * @return
     */
    @ApiOperation(value = "Obtiene archivos", notes = "Obtiene todos los archivos", nickname = "obtieneArchivos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}/archivos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivosVirtuales obtieneArchivos(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    /**
     *
     * @param idFolio
     * @param parametrosArchivo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear archivo", notes = "Agrega un archivo", nickname = "creaArchivo")
    @RequestMapping(value = "/{idFolio}/archivos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaArchivo creaArchivo(@RequestHeader HttpHeaders headers, @ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "ParametrosArchivo", value = "Paramentros para el alta del archivo", required = true) @RequestBody ParametrosArchivo parametrosArchivo) throws DataNotInsertedException, Exception {
        parametrosArchivo.setIdFolio(idFolio.intValue());
        return archivoBI.creaArchivo(httpServletRequest, headers, parametrosArchivo);
    }

    /**
     *
     * @param idFolio
     * @param uid
     * @param parametrosArchivo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar archivo", notes = "Actualiza un archivo", nickname = "actualizaArchivo")
    @RequestMapping(value = "/{idFolio}/archivos/{uid}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaArchivo(@RequestHeader HttpHeaders headers, @ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio,
            @ApiParam(name = "uid", value = "Identificador del archvo", example = "1", required = true) @PathVariable("uid") Long uid,
            @ApiParam(name = "ParametrosArchivo", value = "Paramentros para la actualización del archivo", required = true) @RequestBody ParametrosArchivo parametrosArchivo) throws DataNotUpdatedException {
        parametrosArchivo.setIdFolio(idFolio);
        parametrosArchivo.setUid(uid);
        archivoBI.actualizaArchivo(httpServletRequest, headers, parametrosArchivo);
        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @param uid
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar archivo", notes = "Elimina un  archivos", nickname = "eliminaArchivo")
    @RequestMapping(value = "/{idFolio}/archivos/{uid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaArchivo(@ApiParam(name = "idFolio", value = "Identificador de folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio,
            @ApiParam(name = "uid", value = "Identificador del archvo", example = "1", required = true) @PathVariable("uid") Long uid) throws DataNotDeletedException {
        archivoBI.eliminaArchivo(idFolio, uid);
        return new SinResultado();
    }

    /**
     *
     * @param uid
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivo", notes = "Obtiene un archivo", nickname = "obtieneArchivo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/iconos/pasos/{uid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivoDirectorioBase obtieneRutaArchivo(@ApiParam(name = "uid", value = "Identificador del archvo", example = "1", required = true) @PathVariable("uid") Long uid) throws CustomException, DataNotFoundException {
        ArchivoDirectorioBase archivoBase = archivoBI.encuentraIcono("", uid);
        System.out.println("Entro al metodo");
        System.out.println(archivoBase);
        if (archivoBase == null) {
            throw new DataNotFoundException();
        }
        return archivoBase;
    }

    /**
     *
     * @param uid
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene archivo", notes = "Obtiene un archivo", nickname = "obtieneArchivoGenerico")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busquedas/archivos/genericos/{uid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArchivoDirectorioBase obtieneArchivoGenerico(@ApiParam(name = "uid", value = "Identificador del archvo", example = "1", required = true) @PathVariable("uid") Long uid) throws CustomException, DataNotFoundException {
        ArchivoDirectorioBase archivoBase = archivoBI.encuentraArchivoGenerica("", uid);
        System.out.println("Entro al metodo");
        System.out.println(archivoBase);
        if (archivoBase == null) {
            throw new DataNotFoundException();
        }
        return archivoBase;
    }

}
