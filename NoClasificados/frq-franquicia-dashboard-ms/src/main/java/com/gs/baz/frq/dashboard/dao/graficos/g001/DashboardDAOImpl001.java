package com.gs.baz.frq.dashboard.dao.graficos.g001;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g001.CalificacionFinalDisciplinaDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g001.CalificacionFinalDisciplinaPuntoContactoRowMapper;
import com.gs.baz.frq.dashboard.mprs.graficos.g001.CalificacionFinalDisciplinaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl001 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCalificacionDisciplina;
    private DefaultJdbcCall jdbcSelectCalificacionDisciplinaNivel;
    private DefaultJdbcCall jdbcSelectCalificacionDisciplinaPuntoContacto;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCalificacionDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionDisciplina.withSchemaName(schema);
        jdbcSelectCalificacionDisciplina.withCatalogName("PACONDASHGRAL");
        jdbcSelectCalificacionDisciplina.withProcedureName("SPSELCALPORDISC");
        jdbcSelectCalificacionDisciplina.returningResultSet("RCL_INFO", new CalificacionFinalDisciplinaRowMapper());

        jdbcSelectCalificacionDisciplinaNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionDisciplinaNivel.withSchemaName(schema);
        jdbcSelectCalificacionDisciplinaNivel.withCatalogName("PAADMINCALDISAR");
        jdbcSelectCalificacionDisciplinaNivel.withProcedureName("SPCALPORDISCXARB");
        jdbcSelectCalificacionDisciplinaNivel.returningResultSet("RCL_INFO", new CalificacionFinalDisciplinaRowMapper());

        jdbcSelectCalificacionDisciplinaPuntoContacto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionDisciplinaPuntoContacto.withSchemaName(schema);
        jdbcSelectCalificacionDisciplinaPuntoContacto.withCatalogName("PAADMINPUNTOC");
        jdbcSelectCalificacionDisciplinaPuntoContacto.withProcedureName("SPCALXDISCXPC");
        jdbcSelectCalificacionDisciplinaPuntoContacto.returningResultSet("RCL_INFO", new CalificacionFinalDisciplinaPuntoContactoRowMapper());

    }

    public CalificacionFinalDisciplinaDTO selectRowsCalificacionTotalDisciplinaPuntoContacto(CalificacionFinalDisciplinaDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCalificacionDisciplinaPuntoContacto.execute(mapSqlParameterSource);
        List<CalificacionFinalDisciplinaDTO> data = (List<CalificacionFinalDisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public CalificacionFinalDisciplinaDTO selectRowsCalificacionTotalDisciplinaNivel(CalificacionFinalDisciplinaDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();

        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCalificacionDisciplinaNivel.execute(mapSqlParameterSource);
        List<CalificacionFinalDisciplinaDTO> data = (List<CalificacionFinalDisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public CalificacionFinalDisciplinaDTO selectRowsCalificacionTotalDisciplinaNacional(CalificacionFinalDisciplinaDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());

        Map<String, Object> out = jdbcSelectCalificacionDisciplina.execute(mapSqlParameterSource);
        List<CalificacionFinalDisciplinaDTO> data = (List<CalificacionFinalDisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public CalificacionFinalDisciplinaDTO selectRowsCalificacionTotalDisciplina(CalificacionFinalDisciplinaDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        if (filtros.getNivelGeografico().equals(4)) {
            return this.selectRowsCalificacionTotalDisciplinaNacional(entityDTO);
        } else if (filtros.getNivelGeografico() >= 1 && filtros.getNivelGeografico() <= 3) {
            return this.selectRowsCalificacionTotalDisciplinaNivel(entityDTO);
        } else {
            return this.selectRowsCalificacionTotalDisciplinaPuntoContacto(entityDTO);
        }
    }

}
