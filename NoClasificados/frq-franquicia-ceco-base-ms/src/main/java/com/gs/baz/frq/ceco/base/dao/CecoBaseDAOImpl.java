package com.gs.baz.frq.ceco.base.dao;

import com.gs.baz.frq.ceco.base.dao.util.GenericDAO;
import com.gs.baz.frq.ceco.base.dto.CecoBaseDTO;
import com.gs.baz.frq.ceco.base.mprs.CecoBaseRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class CecoBaseDAOImpl extends DefaultDAO implements GenericDAO<CecoBaseDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMCECOBASE");
        jdbcInsert.withProcedureName("SPINSCECOBASE");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMCECOBASE");
        jdbcUpdate.withProcedureName("SPACTCECOBASE");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMCECOBASE");
        jdbcDelete.withProcedureName("SPDELCECOBASE");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMCECOBASE");
        jdbcSelect.withProcedureName("SPGETCECOBASE");
        jdbcSelect.returningResultSet("PA_CDATOS", new CecoBaseRowMapper());

    }

    @Override
    public CecoBaseDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<CecoBaseDTO> data = (List<CecoBaseDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<CecoBaseDTO> selectRows(CecoBaseDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO.getIdCecoBase());
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FCID_TERRITORIO", entityDTO.getIdTerritorio());
        mapSqlParameterSource.addValue("PA_FCID_ZONA", entityDTO.getIdZona());
        mapSqlParameterSource.addValue("PA_FCID_REGION", entityDTO.getIdRegion());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<CecoBaseDTO> data = (List<CecoBaseDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public CecoBaseDTO insertRow(CecoBaseDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_PAIS", entityDTO.getIdPais());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCID_TERRITORIO", entityDTO.getIdTerritorio());
            mapSqlParameterSource.addValue("PA_FCTERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_FCID_ZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_FCZONA", entityDTO.getZona());
            mapSqlParameterSource.addValue("PA_FCID_REGION", entityDTO.getIdRegion());
            mapSqlParameterSource.addValue("PA_FCREGION", entityDTO.getRegion());
            mapSqlParameterSource.addValue("PA_FCCANAL", entityDTO.getCanal());
            mapSqlParameterSource.addValue("PA_FDCREACION", entityDTO.getFechaCreacion());
            mapSqlParameterSource.addValue("PA_FIREG_ACTIVO", entityDTO.getRegistroActivo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdCecoBase((BigDecimal) out.get("PA_CECOBASE_ID"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  ceco base"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  ceco base"), ex);
        }
    }

    @Override
    public CecoBaseDTO updateRow(CecoBaseDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO.getIdCecoBase());
            mapSqlParameterSource.addValue("PA_FIID_PAIS", entityDTO.getIdPais());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCID_TERRITORIO", entityDTO.getIdTerritorio());
            mapSqlParameterSource.addValue("PA_FCTERRITORIO", entityDTO.getTerritorio());
            mapSqlParameterSource.addValue("PA_FCID_ZONA", entityDTO.getIdZona());
            mapSqlParameterSource.addValue("PA_FCZONA", entityDTO.getZona());
            mapSqlParameterSource.addValue("PA_FCID_REGION", entityDTO.getIdRegion());
            mapSqlParameterSource.addValue("PA_FCREGION", entityDTO.getRegion());
            mapSqlParameterSource.addValue("PA_FCCANAL", entityDTO.getCanal());
            mapSqlParameterSource.addValue("PA_FDCREACION", entityDTO.getFechaCreacion());
            mapSqlParameterSource.addValue("PA_FIREG_ACTIVO", entityDTO.getRegistroActivo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of ceco base"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of ceco base"), ex);
        }
    }

    @Override
    public CecoBaseDTO deleteRow(CecoBaseDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FICECOBASE_ID", entityDTO.getIdCecoBase());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of ceco base"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of ceco base"), ex);
        }
    }

}
