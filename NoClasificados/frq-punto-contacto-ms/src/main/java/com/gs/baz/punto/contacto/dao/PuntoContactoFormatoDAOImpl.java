package com.gs.baz.punto.contacto.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.punto.contacto.dto.PuntoContactoFormatoDTO;
import com.gs.baz.punto.contacto.mprs.PuntoContactoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PuntoContactoFormatoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINPCFRMT");
        jdbcInsert.withProcedureName("SP_INS_PCFRMT");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINPCFRMT");
        jdbcUpdate.withProcedureName("SP_ACT_PCFRMT");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINPCFRMT");
        jdbcDelete.withProcedureName("SP_DEL_PCFRMT");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPCFRMT");
        jdbcSelect.withProcedureName("SP_SEL_PCFRMT");
        jdbcSelect.returningResultSet("RCL_INFO", new PuntoContactoRowMapper().new PuntoContactoFormatoRowMapper());
    }

    public PuntoContactoFormatoDTO selectRow(Integer idPuntoContactoFranquiciaModelo, Integer idTipoFormato) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDPCFRMT", null);
        mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", idPuntoContactoFranquiciaModelo);
        mapSqlParameterSource.addValue("PA_FIIDTIFORM", idTipoFormato);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<PuntoContactoFormatoDTO> data = (List<PuntoContactoFormatoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public PuntoContactoFormatoDTO insertRow(PuntoContactoFormatoDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPCFRMT", entityDTO.getIdFormato());
            mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", entityDTO.getIdPuntoContactoFranquiciaModelo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  PuntoContactoFormato "));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  PuntoContactoFormato "), ex);
        }
    }

    public PuntoContactoFormatoDTO updateRow(PuntoContactoFormatoDTO entityDTO) throws CustomException {
        try {
            PuntoContactoFormatoDTO puntoContactoFormatoDTO = this.selectRow(entityDTO.getIdPuntoContactoFranquiciaModelo(), entityDTO.getIdTipoFormato());
            if (puntoContactoFormatoDTO == null) {
                puntoContactoFormatoDTO = this.insertRow(entityDTO);
            }
            Integer idFormatoActual = puntoContactoFormatoDTO.getIdFormato();
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDPCFRMT", idFormatoActual);
            mapSqlParameterSource.addValue("PA_FIIDPCFRMTNVO", entityDTO.getIdFormato());
            mapSqlParameterSource.addValue("PA_FIIDPCFRQMOD", entityDTO.getIdPuntoContactoFranquiciaModelo());
            mapSqlParameterSource.addValue("PA_FIIDTIFORM", entityDTO.getIdTipoFormato());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error  not success to update row of PuntoContactoFormato"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of PuntoContactoFormato"), ex);
        }
    }

}
