/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Negocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegociosRowMapper implements RowMapper<Negocio> {

    @Override
    public Negocio mapRow(ResultSet rs, int rowNum) throws SQLException {
        Negocio negocio = new Negocio();
        negocio.setIdNegocio(rs.getInt("FINEGOCIODISCID"));
        negocio.setDescripcion(rs.getString("FCNEGOCIODISC"));
        return negocio;
    }
}
