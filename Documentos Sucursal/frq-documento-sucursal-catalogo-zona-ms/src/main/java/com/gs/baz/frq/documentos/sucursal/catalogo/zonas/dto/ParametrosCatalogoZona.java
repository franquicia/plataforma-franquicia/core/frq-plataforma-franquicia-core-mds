/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosCatalogoZona {

    @JsonProperty(value = "idCatalogoZona")
    private transient Integer idCatalogoZona;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion nombre de la zona", example = "Patio Bancario", required = true)
    private String descripcion;

    @JsonProperty(value = "estatus", required = false)
    @ApiModelProperty(notes = "Estatus de la zona 1 (activa) 0 (inactiva)", example = "1", required = false)
    private Integer estatus;

    @JsonProperty(value = "idTipoInformacion", required = true)
    @ApiModelProperty(notes = "Id del tipo de informacion si es banco (1) o elektra (2)", example = "1", required = true)
    private Integer idTipoInformacion;

    public Integer getIdCatalogoZona() {
        return idCatalogoZona;
    }

    public void setIdCatalogoZona(Integer idCatalogoZona) {
        this.idCatalogoZona = idCatalogoZona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Integer getIdTipoInformacion() {
        return idTipoInformacion;
    }

    public void setIdTipoInformacion(Integer idTipoInformacion) {
        this.idTipoInformacion = idTipoInformacion;
    }

}
