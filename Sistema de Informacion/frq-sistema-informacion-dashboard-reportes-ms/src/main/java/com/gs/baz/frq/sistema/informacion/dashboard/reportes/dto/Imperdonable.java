/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del imperdonable", value = "Imperdonable")
public class Imperdonable {

    @JsonProperty(value = "idPregunta")
    @ApiModelProperty(notes = "Identificador de la pregunta", example = "1")
    private Integer idPregunta;

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Puntos de venta", example = "1")
    private Integer puntosVenta;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion de la pregunta", example = "1. El personal de la sucursal porta prendas ajenas al uniforme")
    private String descripcion;

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Imperdonable{" + "idPregunta=" + idPregunta + ", puntosVenta=" + puntosVenta + ", descripcion=" + descripcion + '}';
    }

}
