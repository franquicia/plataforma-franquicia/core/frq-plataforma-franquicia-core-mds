/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosRutaNivel {

    @JsonProperty(value = "idRuta")
    private transient Integer idRuta;

    @JsonProperty(value = "idModeloNegocio")
    @ApiModelProperty(notes = "Identificador del Modelo de negocio", example = "1", required = true)
    private Integer idModeloNegocio;

    public Integer getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Integer idRuta) {
        this.idRuta = idRuta;
    }

    public Integer getIdModeloNegocio() {
        return idModeloNegocio;
    }

    public void setIdModeloNegocio(Integer idModeloNegocio) {
        this.idModeloNegocio = idModeloNegocio;
    }

}
