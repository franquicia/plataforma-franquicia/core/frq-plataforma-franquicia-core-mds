package com.gs.baz.frq.asignacion.mtto.dao;

import com.gs.baz.frq.asignacion.mtto.dto.ArbolUsuarioDTO;
import com.gs.baz.frq.asignacion.mtto.dto.AsignacionMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ExcelRespuestasDTO;
import com.gs.baz.frq.asignacion.mtto.dto.LugarMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteCheckAsistMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteCheckInnMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteChecklistMttoDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralSemanalDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoTerritorioDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteProgramadasDTO;
import com.gs.baz.frq.asignacion.mtto.mprs.ArbolUsuarioRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.AsignacionSupervisionMttoRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ConteoGralRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ConteoGralSemanalRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ConteoGralTerritorioRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ConteoSemanaUsuarioRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ConteoSemanalTerritorioRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.GeografiaRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.LugarMttoRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.RepoCheckAsistenciaRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ReporteCheckInnRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ReporteCheckListRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ReporteProgramadasRowMapper;
import com.gs.baz.frq.asignacion.mtto.mprs.ReporteRespuestasRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignacionMttoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertaAsignMtto;
    private DefaultJdbcCall jdbcUpdateAsignaMtto;
    private DefaultJdbcCall jdbcDeleteAsignacionMtto;
    private DefaultJdbcCall jdbcObtieneReporte;
    private DefaultJdbcCall jdbcObtieneCheckInn;
    private DefaultJdbcCall jdbcObtieneChecklist;
    private DefaultJdbcCall jdbcObtieneAsignacionSupMtto;

    private DefaultJdbcCall jdbcObtieneConteoGralProgramadas;
    private DefaultJdbcCall jdbcObtieneConteoGralEchas;
    private DefaultJdbcCall jdbcObtieneConteoGralSemanal;
    private DefaultJdbcCall jdbcObtieneConteoGralTerritorio;
    private DefaultJdbcCall jdbcObtieneGeografia;
    private DefaultJdbcCall jdbcObtieneConteoSemanaTerritorio;
    private DefaultJdbcCall jdbcObtieneConteoSemanaUsuario;

    private DefaultJdbcCall jdbcObtieneProgramadas;
    private DefaultJdbcCall jdbcObtieneLugarMtto;
    
    private DefaultJdbcCall jdbcObtieneConteoGralProgCeco;
    private DefaultJdbcCall jdbcObtieneConteoGralEchasCeco;
    private DefaultJdbcCall jdbcObtieneConteoGralSemanalCeco;
    
    private DefaultJdbcCall jdbcReporteRespuestas;
    private DefaultJdbcCall jdbcArbolUsuario;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsertaAsignMtto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsertaAsignMtto.withSchemaName(schema);
        jdbcInsertaAsignMtto.withCatalogName("PAFLUJOMMTO");
        jdbcInsertaAsignMtto.withProcedureName("SP_INS_ASIG");

        jdbcUpdateAsignaMtto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdateAsignaMtto.withSchemaName(schema);
        jdbcUpdateAsignaMtto.withCatalogName("PAFLUJOMMTO");
        jdbcUpdateAsignaMtto.withProcedureName("SP_ACTPROGRAMA");

        jdbcObtieneAsignacionSupMtto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneAsignacionSupMtto.withSchemaName(schema);
        jdbcObtieneAsignacionSupMtto.withCatalogName("PAFLUJOMMTO");
        jdbcObtieneAsignacionSupMtto.withProcedureName("SP_SELASIGNA");
        jdbcObtieneAsignacionSupMtto.returningResultSet("RCL_HISTORICO", new AsignacionSupervisionMttoRowMapper());

        jdbcDeleteAsignacionMtto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteAsignacionMtto.withSchemaName(schema);
        jdbcDeleteAsignacionMtto.withCatalogName("PAFLUJOMMTO");
        jdbcDeleteAsignacionMtto.withProcedureName("SP_DEL_ASIGUSU");
        // jdbcObtieneDatosCalendario.returningResultSet("RCL_SOFT", new
        // SoftHistoCalendarioRowMapper());

        jdbcObtieneReporte = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneReporte.withSchemaName(schema);
        jdbcObtieneReporte.withCatalogName("PA_FLUJOSUPMTTO");
        jdbcObtieneReporte.withProcedureName("SP_SEL_REPO");
        jdbcObtieneReporte.returningResultSet("RCL_REPO", new RepoCheckAsistenciaRowMapper());

        jdbcObtieneCheckInn = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneCheckInn.withSchemaName(schema);
        jdbcObtieneCheckInn.withCatalogName("PA_FLUJOSUPMTTO");
        jdbcObtieneCheckInn.withProcedureName("SP_SEL_REPO2NV");
        jdbcObtieneCheckInn.returningResultSet("RCL_REPO", new ReporteCheckInnRowMapper());

        jdbcObtieneChecklist = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneChecklist.withSchemaName(schema);
        jdbcObtieneChecklist.withCatalogName("PA_FLUJOSUPMTTO");
        jdbcObtieneChecklist.withProcedureName("SP_SEL_REPO3NV");
        jdbcObtieneChecklist.returningResultSet("RCL_CHECKS", new ReporteCheckListRowMapper());

        jdbcObtieneConteoGralProgramadas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralProgramadas.withSchemaName(schema);
        jdbcObtieneConteoGralProgramadas.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralProgramadas.withProcedureName("SP_CONTGRALPROG");
        jdbcObtieneConteoGralProgramadas.returningResultSet("RCL_CONTEO", new ConteoGralRowMapper());

        jdbcObtieneConteoGralEchas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralEchas.withSchemaName(schema);
        jdbcObtieneConteoGralEchas.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralEchas.withProcedureName("SP_CONTGRALREAL");
        jdbcObtieneConteoGralEchas.returningResultSet("RCL_CONTEO", new ConteoGralRowMapper());

        jdbcObtieneConteoGralSemanal = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralSemanal.withSchemaName(schema);
        jdbcObtieneConteoGralSemanal.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralSemanal.withProcedureName("SP_CONTSEMGRAL");
        jdbcObtieneConteoGralSemanal.returningResultSet("RCL_CONTEO", new ConteoGralSemanalRowMapper());

        jdbcObtieneConteoGralTerritorio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralTerritorio.withSchemaName(schema);
        jdbcObtieneConteoGralTerritorio.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralTerritorio.withProcedureName("SP_CONTTERRI");
        jdbcObtieneConteoGralTerritorio.returningResultSet("RCL_CONTEO", new ConteoGralTerritorioRowMapper());

        jdbcObtieneGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneGeografia.withSchemaName(schema);
        jdbcObtieneGeografia.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneGeografia.withProcedureName("SP_SEL_TERRI");
        jdbcObtieneGeografia.returningResultSet("RCL_CONTEO", new GeografiaRowMapper());

        jdbcObtieneGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneGeografia.withSchemaName(schema);
        jdbcObtieneGeografia.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneGeografia.withProcedureName("SP_SEL_TERRI");
        jdbcObtieneGeografia.returningResultSet("RCL_CONTEO", new GeografiaRowMapper());

        jdbcObtieneConteoSemanaTerritorio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoSemanaTerritorio.withSchemaName(schema);
        jdbcObtieneConteoSemanaTerritorio.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoSemanaTerritorio.withProcedureName("SP_SELSEM_TERRI");
        jdbcObtieneConteoSemanaTerritorio.returningResultSet("RCL_CONTEO", new ConteoSemanalTerritorioRowMapper());

        jdbcObtieneConteoSemanaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoSemanaUsuario.withSchemaName(schema);
        jdbcObtieneConteoSemanaUsuario.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoSemanaUsuario.withProcedureName("SP_CONT_USU");
        jdbcObtieneConteoSemanaUsuario.returningResultSet("RCL_CONTEO", new ConteoSemanaUsuarioRowMapper());

        jdbcObtieneProgramadas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneProgramadas.withSchemaName(schema);
        jdbcObtieneProgramadas.withCatalogName("PA_REPOMTTO");
        jdbcObtieneProgramadas.withProcedureName("SP_REPOEXC");
        jdbcObtieneProgramadas.returningResultSet("RCL_REPO", new ReporteProgramadasRowMapper());

        jdbcObtieneLugarMtto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneLugarMtto.withSchemaName(schema);
        jdbcObtieneLugarMtto.withCatalogName("PAADM_LUGARMTTO");
        jdbcObtieneLugarMtto.withProcedureName("SP_SEL_LUGAR");
        jdbcObtieneLugarMtto.returningResultSet("RCL_REPO", new LugarMttoRowMapper());
        
        jdbcObtieneConteoGralProgCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralProgCeco.withSchemaName(schema);
        jdbcObtieneConteoGralProgCeco.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralProgCeco.withProcedureName("SP_CONTPROGCECO");
        jdbcObtieneConteoGralProgCeco.returningResultSet("RCL_CONTEO", new ConteoGralRowMapper());
        
        jdbcObtieneConteoGralEchasCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralEchasCeco.withSchemaName(schema);
        jdbcObtieneConteoGralEchasCeco.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralEchasCeco.withProcedureName("SP_CONTREALCECO");
        jdbcObtieneConteoGralEchasCeco.returningResultSet("RCL_CONTEO", new ConteoGralRowMapper());
        
        jdbcObtieneConteoGralSemanalCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneConteoGralSemanalCeco.withSchemaName(schema);
        jdbcObtieneConteoGralSemanalCeco.withCatalogName("PAREPOGEOMTTO");
        jdbcObtieneConteoGralSemanalCeco.withProcedureName("SP_CONTSEMCECO");
        jdbcObtieneConteoGralSemanalCeco.returningResultSet("RCL_CONTEO", new ConteoGralSemanalRowMapper());
        
        
        
        jdbcReporteRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcReporteRespuestas.withSchemaName(schema);
        jdbcReporteRespuestas.withCatalogName("PA_REPOMTTO");
        jdbcReporteRespuestas.withProcedureName("SP_RESPSUP");
        jdbcReporteRespuestas.returningResultSet("RCL_PROG", new ReporteRespuestasRowMapper());
        
        
        jdbcArbolUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcArbolUsuario.withSchemaName(schema);
        jdbcArbolUsuario.withCatalogName("PA_REPOMTTO");
        jdbcArbolUsuario.withProcedureName("SP_ZONUSU");
        jdbcArbolUsuario.returningResultSet("RCL_PROG", new ArbolUsuarioRowMapper());
        
      
    }


    public int insertaAsignacionMtto(AsignacionMttoDTO bean) throws Exception {
        Map<String, Object> out = null;
        // int error=0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", bean.getIdCeco())
                .addValue("PA_USU", bean.getUsuarioAsigna()).addValue("PA_PROY", bean.getIdProyecto())
                .addValue("PA_USUASIG", bean.getIdUsuario()).addValue("PA_OBSERV", bean.getDescripcion())
                .addValue("PA_IDAGRUPA", bean.getIdAgrupa())
                // YYYYMMDD
                .addValue("PA_FECHAPROG", bean.getFechaProgramada()).addValue("PA_TIPOVISITA", bean.getTipoVisita())
                .addValue("PA_TIPONEGO", bean.getNegocio()).addValue("PA_ORDENTAREA", bean.getOrdenActivos())
                .addValue("PA_TKRELACION", bean.getComentarioTicketRelacionados())
                .addValue("PA_COMENTREPROG", bean.getComentarioReprogramacion());

        // .addValue("PA_BANDFOLIO",bean.getBanderaFolio());
        out = jdbcInsertaAsignMtto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAFLUJOMMTO.SP_INS_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        idEmpFij = resultado.intValue();

        // BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        // idEmpFij = idTipoReturn.intValue();
        if (idEmpFij == 0) {
            logger.info("Algo ocurrió al insertar ASIGNACION-MTTO");
            idEmpFij = 0;
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public int actualizaAsignacionMtto(AsignacionMttoDTO bean) throws Exception {
        Map<String, Object> out = null;
        // int error=0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPROG", bean.getIdProgramacion())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_COMENTREPROG", bean.getComentarioReprogramacion())
                // YYYYMMDD
                .addValue("PA_FECHAPROG", bean.getFechaProgramada());

        out = jdbcUpdateAsignaMtto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAFLUJOMMTO.SP_ACTPROGRAMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        idEmpFij = resultado.intValue();

        if (idEmpFij == 0) {
            logger.info("Algo ocurrió al actualizar Programacion ASIGNACION");
            idEmpFij = 0;
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public List<AsignacionMttoDTO> obtieneAsignacionesSupMtto(AsignacionMttoDTO entityID) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_PROY", entityID.getIdProyecto());
        mapSqlParameterSource.addValue("PA_USUARIO", entityID.getIdUsuario());
        mapSqlParameterSource.addValue("PA_ZONA", entityID.getZona());
        Map<String, Object> out = jdbcObtieneAsignacionSupMtto.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<AsignacionMttoDTO> data = (List<AsignacionMttoDTO>) out.get("RCL_HISTORICO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public int eliminaAsignacionMtto(AsignacionMttoDTO entityDTO) throws CustomException {
        try {
            int error = 0;
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

            mapSqlParameterSource.addValue("PA_PROY", entityDTO.getIdProyecto());
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

            Map<String, Object> out = jdbcDeleteAsignacionMtto.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA
                        .detalle("Error not success to desactiva row of asignacion mtto"));

            }
        } catch (Exception ex) {
            throw new CustomException(
                    ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to desactiva row of asignacion mtto"), ex);

        }
    }

    public List<ReporteCheckAsistMttoDTO> obtieneReporteAsistencia(ReporteCheckAsistMttoDTO entityID)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_USUARIO", entityID.getIdUsuario());
        // --01/01/21
        mapSqlParameterSource.addValue("PA_FECHAINI", entityID.getFechaInicio());
        // 30/01/21
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityID.getFechaFin());
        mapSqlParameterSource.addValue("PA_ZONA", entityID.getZona());
        Map<String, Object> out = jdbcObtieneReporte.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteCheckAsistMttoDTO> data = (List<ReporteCheckAsistMttoDTO>) out.get("RCL_REPO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteCheckInnMttoDTO> selectReporteCheckInn(ReporteCheckInnMttoDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
        Map<String, Object> out = jdbcObtieneCheckInn.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteCheckInnMttoDTO> data = (List<ReporteCheckInnMttoDTO>) out.get("RCL_REPO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteChecklistMttoDTO> selectReporteCheckList(ReporteChecklistMttoDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getFecha());
        mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
        mapSqlParameterSource.addValue("PA_FASE", entityDTO.getIdFase());
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

        Map<String, Object> out = jdbcObtieneChecklist.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteChecklistMttoDTO> data = (List<ReporteChecklistMttoDTO>) out.get("RCL_CHECKS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteConteoGralDTO> selectConteoGralProgramadas(ReporteConteoGralDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());

        Map<String, Object> out = jdbcObtieneConteoGralProgramadas.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoGralDTO> data = (List<ReporteConteoGralDTO>) out.get("RCL_CONTEO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    //N
    public List<ReporteConteoGralDTO> selectConteoGralProgramadasCeco(ReporteConteoGralDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_TERRI", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneConteoGralProgCeco.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoGralDTO> data = (List<ReporteConteoGralDTO>) out.get("RCL_CONTEO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    public List<ReporteConteoGralDTO> selectConteoGralEchas(ReporteConteoGralDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());

        Map<String, Object> out = jdbcObtieneConteoGralEchas.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoGralDTO> data = (List<ReporteConteoGralDTO>) out.get("RCL_CONTEO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    //N
    public List<ReporteConteoGralDTO> selectConteoGralEchasCeco(ReporteConteoGralDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_TERRI", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneConteoGralEchasCeco.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoGralDTO> data = (List<ReporteConteoGralDTO>) out.get("RCL_CONTEO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    public List<ReporteConteoGralSemanalDTO> selectConteoGralSemanal(ReporteConteoGralSemanalDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());

        Map<String, Object> out = jdbcObtieneConteoGralSemanal.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoGralSemanalDTO> data = (List<ReporteConteoGralSemanalDTO>) out.get("RCL_CONTEO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
   //N
    public List<ReporteConteoGralSemanalDTO> selectConteoGralSemanalCeco(ReporteConteoGralSemanalDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_TERRI", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneConteoGralSemanalCeco.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoGralSemanalDTO> data = (List<ReporteConteoGralSemanalDTO>) out.get("RCL_CONTEO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }


    public List<ReporteConteoTerritorioDTO> selectConteoGralTerritorio(ReporteConteoTerritorioDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneConteoGralTerritorio.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoTerritorioDTO> data = (List<ReporteConteoTerritorioDTO>) out.get("RCL_CONTEO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteConteoTerritorioDTO> selectGeografia(ReporteConteoTerritorioDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDPADRE", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneGeografia.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoTerritorioDTO> data = (List<ReporteConteoTerritorioDTO>) out.get("RCL_CONTEO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteConteoTerritorioDTO> selectSemanaTerritorio(ReporteConteoTerritorioDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneConteoSemanaTerritorio.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoTerritorioDTO> data = (List<ReporteConteoTerritorioDTO>) out.get("RCL_CONTEO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteConteoTerritorioDTO> selectSemanaUsuario(ReporteConteoTerritorioDTO entityDTO)
            throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_NIVEL", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneConteoSemanaUsuario.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteConteoTerritorioDTO> data = (List<ReporteConteoTerritorioDTO>) out.get("RCL_CONTEO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ReporteProgramadasDTO> selectProgramadas(ReporteProgramadasDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_ZONA", entityDTO.getTerritorio());

        Map<String, Object> out = jdbcObtieneProgramadas.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ReporteProgramadasDTO> data = (List<ReporteProgramadasDTO>) out.get("RCL_REPO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ExcelRespuestasDTO> selectReporteRespuestas(ExcelRespuestasDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDUSUARIO", entityDTO.getNumeroEmpleado());
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getPeriodo());

        Map<String, Object> out = jdbcReporteRespuestas.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ExcelRespuestasDTO> data = (List<ExcelRespuestasDTO>) out.get("RCL_PROG");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<LugarMttoDTO> selectLugarMtto(LugarMttoDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_LUGAR", entityDTO.getidLugar());

        Map<String, Object> out = jdbcObtieneLugarMtto.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<LugarMttoDTO> data = (List<LugarMttoDTO>) out.get("RCL_REPO");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    
    public List<ArbolUsuarioDTO> selectArbolUsu(ArbolUsuarioDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDUSUARIO", entityDTO.getIdSupervisor());

        Map<String, Object> out = jdbcArbolUsuario.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<ArbolUsuarioDTO> data = (List<ArbolUsuarioDTO>) out.get("RCL_PROG");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    /*
	 * fases proyecto
	 * 
	 * @SuppressWarnings("unchecked") public Map<String, Object>
	 * selectReporteCheckI(ReporteCheckInnMttoDTO entityDTO) throws CustomException
	 * { int error=0; Map<String, Object> out = null; Map<String, Object> lista =
	 * null; List<ReporteCheckInnMttoDTO> listaCheckModulo = null;
	 * List<ReporteChecklistMttoDTO> listaCheckInn = null; mapSqlParameterSource =
	 * new MapSqlParameterSource(); //dia mes año 010121 o 01/01/21
	 * mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getFechaInicio());
	 * mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario()); out =
	 * jdbcObtieneCheckInn.execute(mapSqlParameterSource);
	 * 
	 * listaCheckModulo = (List<ReporteCheckInnMttoDTO>) out.get("RCL_REPO");
	 * listaCheckInn = (List<ReporteChecklistMttoDTO>) out.get("RCL_CHECKS");
	 * 
	 * BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION"); error =
	 * resultado.intValue(); if (error != 1) {
	 * logger.info("No se trajo la informacion Correctamente"); return null; } lista
	 * = new HashMap<String, Object>(); lista.put("listaCheckModulo",
	 * listaCheckModulo); lista.put("listaCheckInn", listaCheckInn);
	 * 
	 * return lista; }
	 *
     */
}
