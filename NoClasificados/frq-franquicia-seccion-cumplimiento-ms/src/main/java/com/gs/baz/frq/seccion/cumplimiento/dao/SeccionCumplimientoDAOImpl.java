package com.gs.baz.frq.seccion.cumplimiento.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.seccion.cumplimiento.dao.util.GenericDAO;
import com.gs.baz.frq.seccion.cumplimiento.dto.SeccionCumplimientoDTO;
import com.gs.baz.frq.seccion.cumplimiento.mprs.SeccionCumplimientoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SeccionCumplimientoDAOImpl extends DefaultDAO implements GenericDAO<SeccionCumplimientoDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMCUMPLIMSE");
        jdbcInsert.withProcedureName("SPINSCUMPLIMSECC");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMCUMPLIMSE");
        jdbcUpdate.withProcedureName("SPACTCUMPLIMSECC");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMCUMPLIMSE");
        jdbcDelete.withProcedureName("SPDELCUMPLIMSECC");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMCUMPLIMSE");
        jdbcSelect.withProcedureName("SPGETCUMPLIMSECC");
        jdbcSelect.returningResultSet("PA_CDATOS", new SeccionCumplimientoRowMapper());
    }

    @Override
    public List<SeccionCumplimientoDTO> selectRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", null);
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", null);
        mapSqlParameterSource.addValue("PA_FCMODULO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<SeccionCumplimientoDTO>) out.get("PA_CDATOS");
    }

    @Override
    public List<SeccionCumplimientoDTO> selectRowsSeccionCumplimiento(SeccionCumplimientoDTO entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<SeccionCumplimientoDTO>) out.get("PA_CDATOS");

    }

    @Override
    public SeccionCumplimientoDTO insertRow(SeccionCumplimientoDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FECHA_VISITA", entityDTO.getFechaVisista());
            mapSqlParameterSource.addValue("PA_FIPONDESPE", entityDTO.getPonderacionEsperado());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisc());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  seccion de cumplimiento"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  seccion de cumplimiento"), ex);
        }
    }

    @Override
    public SeccionCumplimientoDTO updateRow(SeccionCumplimientoDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FIPONDERACION", entityDTO.getPonderacion());
            mapSqlParameterSource.addValue("PA_FECHA_VISITA", entityDTO.getFechaVisista());
            mapSqlParameterSource.addValue("PA_FIPONDESPE", entityDTO.getPonderacionEsperado());
            mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisc());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of seccion de cumplimiento"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of seccion de cumplimiento"), ex);
        }
    }

    @Override
    public SeccionCumplimientoDTO deleteRow(SeccionCumplimientoDTO entityDTO) throws CustomException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
            mapSqlParameterSource.addValue("PA_FCMODULO", entityDTO.getModulo());
            mapSqlParameterSource.addValue("PA_FECHA_VISITA", entityDTO.getFechaVisista());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Seccion Cumplimiento"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Seccion Cumplimiento"), ex);
        }
    }

}
