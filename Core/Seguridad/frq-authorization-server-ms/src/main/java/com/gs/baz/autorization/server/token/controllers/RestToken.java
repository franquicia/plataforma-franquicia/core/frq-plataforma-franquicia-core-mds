/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.token.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.autorization.server.apigee.bi.ApigeeJWT;
import com.gs.baz.frq.model.commons.ErrorMessageDTO;
import com.gs.baz.frq.model.commons.OutMessageDTO;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@RestController
public class RestToken {

    @Autowired
    @Qualifier("restTemplateToken")
    RestTemplate restTemplate;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    ApigeeJWT apigeeJWT;

    /**
     *
     * @param httpHeaders
     * @param body
     * @return
     * @throws java.lang.Exception
     */
    @RequestMapping(value = {"/service/security/oauth/token", "/api-local/security/oauth/token"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity getTokenOptionService(@RequestHeader HttpHeaders httpHeaders, @RequestParam MultiValueMap body) throws Exception {
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + httpServletRequest.getContextPath();
        HttpHeaders newHttpHeaders = new HttpHeaders();
        if (httpHeaders.get(HttpHeaders.AUTHORIZATION) != null) {
            newHttpHeaders.addAll(HttpHeaders.AUTHORIZATION, httpHeaders.get(HttpHeaders.AUTHORIZATION));
        }
        HttpEntity<Object> httpEntity = new HttpEntity<>(body, newHttpHeaders);
        try {
            return restTemplate.exchange(basePath + "/oauth/token", HttpMethod.POST, httpEntity, ObjectNode.class);
        } catch (RestClientException ex) {
            String responseMessage = ex.getMessage();
            if (responseMessage != null) {
                int beginIndex = responseMessage.indexOf("[");
                int endIndex = responseMessage.indexOf("]");
                int status = new Integer(responseMessage.substring(0, 3));
                String responseBody = responseMessage.substring(beginIndex + 1, endIndex);
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    ObjectNode objectNode = objectMapper.readValue(responseBody, ObjectNode.class);
                    OutMessageDTO OutMessageDTO = new OutMessageDTO();
                    OutMessageDTO.setStatusCode(status);
                    OutMessageDTO.setData(objectNode);
                    return new ResponseEntity(OutMessageDTO, HttpStatus.resolve(status));
                } catch (JsonProcessingException jpe) {
                    ErrorMessageDTO errorMessage = new ErrorMessageDTO();
                    errorMessage.setMessage("401 UNAUTHORIZED");
                    errorMessage.setTimestamp(new Date());
                    errorMessage.setStatusCode(status);
                    return new ResponseEntity(errorMessage, HttpStatus.resolve(status));
                }
            }
            throw new Exception(ex);
        }
    }

}
