/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.mprs;

import com.gs.baz.frq.modelo.negocio.arboles.dto.Arbol;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ArbolesRowMapper implements RowMapper<Arbol> {

    private Arbol arbol;

    @Override
    public Arbol mapRow(ResultSet rs, int rowNum) throws SQLException {
        arbol = new Arbol();
        arbol.setIdArbol(rs.getInt("FIIDMODENEGOC"));
        arbol.setIdModeloNegocioPadre(rs.getInt("FIIDPADRE"));
        arbol.setDescripcion(rs.getString("FCDESCRIPCION"));
        arbol.setNombre(rs.getString("FCNOMBRE"));
        arbol.setIcono(rs.getString("FCICONO"));
        arbol.setIdNivel(rs.getInt("FIIDNIVEL"));
        return arbol;
    }
}
