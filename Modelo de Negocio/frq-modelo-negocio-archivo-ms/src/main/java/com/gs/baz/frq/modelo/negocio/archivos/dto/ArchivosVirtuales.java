/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de archivos del modelo de negocio", value = "archivos")
public class ArchivosVirtuales {

    @JsonProperty(value = "folioVirtualesArchivos")
    @ApiModelProperty(notes = "folioVirtualesArchivos")
    private List<ArchivoVirtual> archivos;

    public ArchivosVirtuales(List<ArchivoVirtual> archivos) {
        this.archivos = archivos;
    }

    public List<ArchivoVirtual> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<ArchivoVirtual> archivos) {
        this.archivos = archivos;
    }

    @Override
    public String toString() {
        return "Archivos{" + "archivos=" + archivos + '}';
    }

}
