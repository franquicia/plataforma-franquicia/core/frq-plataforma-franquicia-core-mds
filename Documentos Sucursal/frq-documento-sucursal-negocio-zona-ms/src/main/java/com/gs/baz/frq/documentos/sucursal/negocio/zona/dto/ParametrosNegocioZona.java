/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocio.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosNegocioZona {

    @JsonProperty(value = "idNegocio")
    private transient Integer idNegocio;

    @JsonProperty(value = "idZona", required = true)
    @ApiModelProperty(notes = "Identificador de Zona", example = "1", required = true)
    private Integer idZona;

    @JsonProperty(value = "estatus", required = true)
    @ApiModelProperty(notes = "Identificador del estado de la relacion", example = "1", required = true)
    private Integer estatus;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

}
