/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.dto.graficos.g005;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class DashboardCertificacionG005DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_inicial")
    private String fechaInicial;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_final")
    private String fechaFinal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_gerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semanas_atras")
    private Integer semanasAtras;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_actual")
    private String fechaActual;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semana")
    private Integer semana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "total_certificaciones")
    private Integer totalCertificaciones;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango")
    private Integer rango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rango_color")
    private String rangoColor;

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(BigDecimal idGerente) {
        this.idGerente = (idGerente == null ? null : idGerente.intValue());
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    public Integer getSemanasAtras() {
        return semanasAtras;
    }

    public void setSemanasAtras(BigDecimal semanasAtras) {
        this.semanasAtras = (semanasAtras == null ? null : semanasAtras.intValue());
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public Integer getSemana() {
        return semana;
    }

    public void setSemana(BigDecimal semana) {
        this.semana = (semana == null ? null : semana.intValue());
    }

    public Integer getTotalCertificaciones() {
        return totalCertificaciones;
    }

    public void setTotalCertificaciones(BigDecimal totalCertificaciones) {
        this.totalCertificaciones = (totalCertificaciones == null ? null : totalCertificaciones.intValue());
    }

    public Integer getRango() {
        return rango;
    }

    public void setRango(BigDecimal rango) {
        this.rango = (rango == null ? null : rango.intValue());
    }

    @Override
    public String toString() {
        return "DashboardCertificacionG003DTO{" + "fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", idGerente=" + idGerente + ", idNegocio=" + idNegocio + ", semanasAtras=" + semanasAtras + ", fechaActual=" + fechaActual + ", semana=" + semana + ", totalCertificaciones=" + totalCertificaciones + ", rango=" + rango + ", rangoColor=" + rangoColor + '}';
    }

}
