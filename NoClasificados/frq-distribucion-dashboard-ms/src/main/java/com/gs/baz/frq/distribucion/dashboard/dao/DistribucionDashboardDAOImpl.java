/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dao;

import com.gs.baz.frq.distribucion.dashboard.dto.CargaVisitasRequestDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaRequestDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaCargaVisitasDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaDepuraVisitasDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaDistriDashCalifDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestaMergeDetallePreguntaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasCapaCeroDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasCapaIntermediaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasDistCecoBaseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasHistoricoDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasImperdonableIncumplimientoDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.RespuestasSieteCapaCeroDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasCapaCeroResponseDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasCapaIntermediaDTO;
import com.gs.baz.frq.distribucion.dashboard.dto.VisitasSieteDTO;
import com.gs.baz.frq.distribucion.dashboard.mprs.EstatusVisitasRowMapper;
import com.gs.baz.frq.distribucion.dashboard.util.UtilMail;
import com.gs.baz.frq.model.commons.CustomException;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jfernandor
 */
public class DistribucionDashboardDAOImpl extends DefaultDAO {

    DefaultJdbcCall jdbcCapaCero;
    private DefaultJdbcCall jdbcSieteCapaCero;
    private DefaultJdbcCall jdbcRespuestaHistorico;
    private DefaultJdbcCall jdbcRespuestaCapaCero;
    private DefaultJdbcCall jdbcRespuestaSieteCero;
    private DefaultJdbcCall jdbcCapaIntermedia;
    private DefaultJdbcCall jdbcRespuestaCapaIntermedia;
    private DefaultJdbcCall jdbcEstatusVisita;
    private DefaultJdbcCall jdbcCecoBase;
    private DefaultJdbcCall jdbcCargaVisita;
    private DefaultJdbcCall jdbcDepuraVisita;
    private DefaultJdbcCall jdbcDistritucionDashCalif;
    private DefaultJdbcCall jdbcDistribucionRespImpInc;
    private DefaultJdbcCall jdbcDistribucionDetallePregunta;
    // private List<CuadrillaDTO> listaDetU;

    private final Logger logger = LogManager.getLogger(DistribucionDashboardDAOImpl.class);

    public void init() {

        jdbcCapaCero = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTCORE")
                .withProcedureName("SPDIST_VISITAS");

        jdbcSieteCapaCero = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTCORE")
                .withProcedureName("SPDIST_VISITA7S");

        jdbcRespuestaHistorico = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRES_CORE")
                .withProcedureName("SPDISRESPUESTA");

        jdbcRespuestaCapaCero = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRESPCORE")
                .withProcedureName("SPDISTRESP");

        jdbcRespuestaSieteCero = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRESPCORE")
                .withProcedureName("SPDISTRESP7S");

        jdbcCapaIntermedia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTCORE")
                .withProcedureName("SPDISTRESUMV");

        jdbcRespuestaCapaIntermedia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRES_CORE")
                .withProcedureName("SPDISTINTERM");

        jdbcEstatusVisita = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_DATOSINFO")
                .withProcedureName("SP_GETESTATVISI")
                .returningResultSet("PA_DATOS", new EstatusVisitasRowMapper());

        jdbcCecoBase = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCECOBASE")
                .withProcedureName("SPDISTCECOBASE");

        jdbcCargaVisita = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCTRLVISIT")
                .withProcedureName("SPCARGAVISITAS");

        jdbcDepuraVisita = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHDEPURACION")
                .withProcedureName("SPGETCECOSPEND");

        jdbcDistritucionDashCalif = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADIST_DASHBOARD_2")
                .withProcedureName("SPCARGADIARIA");

        jdbcDistribucionRespImpInc = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADIST_DASHBOARD_3")
                .withProcedureName("SPCARGADIARIA");

        jdbcDistribucionDetallePregunta = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate)
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMDETPREGUN")
                .withProcedureName("SPDISTMERGDETPREG");
    }

    @SuppressWarnings("unchecked")

    public VisitasCapaCeroResponseDTO visitasCapaCero(Integer estatus) throws CustomException {
        VisitasCapaCeroResponseDTO salida = new VisitasCapaCeroResponseDTO();
        Integer respuesta = 0;

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FISTATUS", estatus);
        Map<String, Object> out = jdbcCapaCero.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTCORE.SPDIST_VISITAS}");
        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();
        Integer visitas = 0;
        visitas = ((BigDecimal) out.get("PA_VISITAS")).intValue();
        salida.setRespuestaEjecucion(respuesta);
        salida.setVisitasCaapaCero(visitas);
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        salida.setRespuestaEjecucion(respuesta);
        salida.setVisitasCaapaCero(visitas);
        if (respuesta != 1) {
            logger.info("Algo pasó y falló la distribución de visitas capa cero");

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Visitas capa 0", "Falló la distribución de Visitas Capa 0. \n PAQUETE: FRANQUICIA.PADISTCORE.SPDIST_VISITAS ", null);
                salida.setEnvioCorreo(envio);
            } catch (Exception e) {
                salida.setEnvioCorreo(false);
                logger.info("Envío de Correo no exitoso de fallo en visitas capa cero");
                logger.error(e);
            }
        } else {

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Visitas capa 0", "Se realizó la distribución de Visitas Capa 0 correctamente. \n FRANQUICIA.PADISTCORE.SPDIST_VISITAS \n Visitas: " + visitas.toString(), null);
                salida.setEnvioCorreo(envio);
            } catch (Exception e) {
                salida.setEnvioCorreo(false);
                logger.info("Envío de Correo no exitoso de visitas capa cero correcto");
                logger.error(e);
            }
        }

        return salida;

    }

    @SuppressWarnings("unchecked")
    public VisitasSieteDTO visitasSieteCapaCero(Integer estatus) throws CustomException {
        Integer respuesta = 0;
        Integer visitasSiete = 0;

        VisitasSieteDTO ejecucion = new VisitasSieteDTO();

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FISTATUS", estatus);

        Map<String, Object> out = jdbcSieteCapaCero.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTCORE.SPDIST_VISITA7S}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal visit = (BigDecimal) out.get("PA_VISITAS7S");
        visitasSiete = visit.intValue();

        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");

        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setVisitasSieteSCapaCero(visitasSiete);

        if (respuesta != 1) {
            logger.info("Algo paso con la distribucón de 7s capa cero");

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Visitas 7S capa 0 ", "Falló la distribución de Visitas 7S Capa 0. \n FRANQUICIA.PADISTCORE.SPDIST_VISITA7S", null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {

                ejecucion.setEnvioCorreo(false);
                logger.info("Envío de Correo no exitoso para reportar fallo de distribución visitas capa cero 7s");
                logger.error(e);
            }

        } else {

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Visitas 7s capa 0", "Se realizó la distribución de Visitas 7S Capa 0 correctamente. \n FRANQUICIA.PADISTCORE.SPDIST_VISITA7S \n Visitas: " + visitasSiete.toString(), null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {

                ejecucion.setEnvioCorreo(false);
                logger.info("Envío de Correo no exitoso");
                logger.error(e);
            }

        }

        return ejecucion;
    }

    @SuppressWarnings("unchecked")
    public RespuestasHistoricoDTO respuestasHistorico() throws CustomException {
        Integer respuesta = 0;
        Integer respuestaHistorico = 0;

        RespuestasHistoricoDTO ejecucion = new RespuestasHistoricoDTO();

        Map<String, Object> out = jdbcRespuestaHistorico.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTRES_CORE.SPDISRESPUESTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal respHistorico = (BigDecimal) out.get("PA_NUMRESP");
        respuestaHistorico = respHistorico.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");

        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setNumeroRespuestas(respuestaHistorico);

        if (respuesta != 1) {
            logger.info("Algo paso al distribuir respuestas histórico");

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas Histórico", "Falló la distribución de Respuestas de histórico. \n FRANQUICIA.PADISTRES_CORE.SPDISRESPUESTA", null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {

                ejecucion.setEnvioCorreo(false);
                logger.info("Envío de Correo no exitoso para reportar fallo de respuestas histórico");
                logger.error(e);
            }
        } else {

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas Histórico", "Se realizó la distribución de Respuestas de histórico correctamente. \n FRANQUICIA.PADISTRES_CORE.SPDISRESPUESTA \n Respuestas: " + respuestaHistorico.toString(), null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {

                ejecucion.setEnvioCorreo(false);
                logger.info("Envío de Correo no exitoso para reportar distribución de respuestas histórico corrcta");
                logger.error(e);
            }
        }

        return ejecucion;
    }

    @SuppressWarnings("unchecked")
    public RespuestasCapaCeroDTO respuestaCapaCero() throws CustomException {

        Integer respuesta = 0;
        Integer respuestaCapaCero = 0;
        RespuestasCapaCeroDTO ejecucion = new RespuestasCapaCeroDTO();
        Map<String, Object> out = jdbcRespuestaCapaCero.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTRESPCORE.SPDISTRESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal respuestaCa = (BigDecimal) out.get("PA_NUMRESP");
        respuestaCapaCero = respuestaCa.intValue();

        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");

        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setNumeroRespuestas(respuestaCapaCero);

        if (respuesta != 1) {
            logger.info("Algo paso al distribuir respuestas capa cero");

            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas Capa 0", "Falló la distribución de Respuestas de Capa 0. \n FRANQUICIA.PADISTRESPCORE.SPDISTRESP", null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar falló de distribución respuestas capa cero");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas Capa 0", "Se realizó la distribución de Respuestas de Capa 0 correctamente. \n FRANQUICIA.PADISTRESPCORE.SPDISTRESP \n Respuestas: " + respuestaCapaCero.toString(), null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de respuestas capa cero");
                ejecucion.setEnvioCorreo(true);
                logger.error(e);
            }

        }

        return ejecucion;
    }

    @SuppressWarnings("unchecked")
    public RespuestasSieteCapaCeroDTO respuestaSieteCapaCero() throws CustomException {

        Integer respuesta = 0;
        Integer respuestaSieteCapaCero = 0;
        RespuestasSieteCapaCeroDTO ejecucion = new RespuestasSieteCapaCeroDTO();
        Map<String, Object> out = jdbcRespuestaSieteCero.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTRESPCORE.SPDISTRESP7S}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal respuestaCa = (BigDecimal) out.get("PA_NUMRESP");
        respuestaSieteCapaCero = respuestaCa.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");

        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setNumeroRespuestas(respuestaSieteCapaCero);

        if (respuesta != 1) {
            logger.info("Algo pasó con la distribución de respuestas capa cero 7s");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas 7S Capa 0", "Falló la distribución de Respuestas 7S de Capa 0. \n FRANQUICIA.PADISTRESPCORE.SPDISTRESP7S", null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo de distribución de respuestas capa cero 7s");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }

        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas 7S Capa 0", "Se realizó la distribución de Respuestas de 7S Capa 0 correctamente. \n FRANQUICIA.PADISTRESPCORE.SPDISTRESP7S \n Respuestas: " + respuestaSieteCapaCero.toString(), null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de respuestas de capa cero 7s");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }

        }

        return ejecucion;
    }

    @SuppressWarnings("unchecked")
    public VisitasCapaIntermediaDTO visitasCapaIntermedia() throws CustomException {

        Integer respuesta = 0;
        Integer visitasCapaIntermedia = 0;
        VisitasCapaIntermediaDTO ejecucion = new VisitasCapaIntermediaDTO();
        Map<String, Object> out = jdbcCapaIntermedia.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTCORE.SPDISTRESUMV}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal respuestaCa = (BigDecimal) out.get("PA_VISITAS");
        visitasCapaIntermedia = respuestaCa.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setVisitasCaapaCero(visitasCapaIntermedia);

        if (respuesta != 1) {
            logger.info("Algo paso en la distribución de visitas de capa intermedia");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Visitas Capa Intermedia", "Falló la distribución de Visitas Capa Intermedia. \n FRANQUICIA.PADISTCORE.SPDISTRESUMV", null);

                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en la distribución de visitas capa intermedia");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Visitas Capa Intermedia", "Se realizó la distribución de Visitas de capa intermedia correctamente. \n FRANQUICIA.PADISTCORE.SPDISTRESUMV \n Respuestas: " + visitasCapaIntermedia.toString(), null);

                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución de visitas de capa intermedia");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return ejecucion;
    }

    @SuppressWarnings("unchecked")
    public RespuestasCapaIntermediaDTO respuestasCapaIntermedia() throws CustomException {

        Integer respuesta = 0;
        Integer respuestasCapaIntermedia = 0;
        RespuestasCapaIntermediaDTO ejecucion = new RespuestasCapaIntermediaDTO();
        Map<String, Object> out = jdbcRespuestaCapaIntermedia.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PADISTRES_CORE.SPDISTINTERM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal respuestaCa = (BigDecimal) out.get("PA_NUMRESP");
        respuestasCapaIntermedia = respuestaCa.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");

        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setNumeroRespuestas(respuestasCapaIntermedia);

        if (respuesta != 1) {
            logger.info("Algo pasó con la distribución de resuestas capa intermedia");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas Capa Intermedia", "Falló la distribución de Respuestas Capa Intermedia. \n FRANQUICIA.PADISTRES_CORE.SPDISTINTERM", null);

                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo de distribución de respuestas capa intermedia");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Respuestas Capa Intermedia", "Se realizó la distribución de Respuestas de capa intermedia correctamente. \n FRANQUICIA.PADISTRES_CORE.SPDISTINTERM \n Respuestas: " + respuestasCapaIntermedia.toString(), null);

                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de respuestas capa intermedia");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return ejecucion;
    }

    @SuppressWarnings("all")
    public List<EstatusVisitaDTO> estatusVisita(EstatusVisitaRequestDTO estatusVisitaRequestDTO) throws CustomException {

        Integer respuesta = 0;
        List<EstatusVisitaDTO> respuestaEstatusVisita = null;

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FCID_CECO", estatusVisitaRequestDTO.getCeco());
        in.addValue("PA_FECHA", estatusVisitaRequestDTO.getFecha());
        in.addValue("PA_FIID_USUARIO", estatusVisitaRequestDTO.getNumeroEmpleado());
        in.addValue("PA_FIID_PROTOCOLO", estatusVisitaRequestDTO.getIdProtocolo());

        Map<String, Object> out = jdbcEstatusVisita.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_DATOSINFO.SP_GETESTATVISI }");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");

        respuestaEstatusVisita = (List<EstatusVisitaDTO>) out.get("PA_DATOS");

        if (respuesta != 1) {
            logger.info("Algo pasó en estatus visita");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Estatus Visita incorrecto", "Falló Control Estatus Visita. \n FRANQUICIA.PA_DATOSINFO.SP_GETESTATVISI \n : ", null);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en estatus visita");
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Estatus Visita Correcto", "Control Estatus Visita Finalizó correctamente. \n FRANQUICIA.PA_DATOSINFO.SP_GETESTATVISI", null);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para distribución correcta de estatus visita");
                logger.error(e);
            }
        }

        return respuestaEstatusVisita;
    }

    @SuppressWarnings("unchecked")
    public RespuestasDistCecoBaseDTO cecoBaseDistribucion() throws CustomException {

        Integer respuesta = 0;
        Integer ditribucion = 0;
        RespuestasDistCecoBaseDTO ejecucion = new RespuestasDistCecoBaseDTO();
        Map<String, Object> out = jdbcCecoBase.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCECOBASE.SPDISTCECOBASE }");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        respuesta = resultado.intValue();

        BigDecimal respuestaCa = (BigDecimal) out.get("PA_CONTEREG");
        ditribucion = respuestaCa.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        ejecucion.setRespuestaEjecucion(respuesta);
        ejecucion.setNumeroRespuestas(ditribucion);

        if (respuesta != 1) {
            logger.info("Algo pasó con la distribución de Ceco Base");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Ceco Base", "Falló la distribución de Ceco Base. \n FRANQUICIA.PAADMCECOBASE.SPDISTCECOBASE", null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en la distribución de ceco base");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Ceco Base", "Se realizó la distribución de ceco base correctamente. \n FRANQUICIA.PAADMCECOBASE.SPDISTCECOBASE ", null);
                ejecucion.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de ceco base");
                ejecucion.setEnvioCorreo(false);
                logger.error(e);
            }

        }

        return ejecucion;
    }

    @SuppressWarnings("unchecked")
    public RespuestaCargaVisitasDTO cargaVisitas(CargaVisitasRequestDTO cargaVisitasRequestDTO) throws CustomException {

        RespuestaCargaVisitasDTO salida = new RespuestaCargaVisitasDTO();
        Integer respuesta = 0;

        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FECHA", cargaVisitasRequestDTO.getFecha());
        in.addValue("PA_FICECOBASE_ID", cargaVisitasRequestDTO.getCeco());

        Map<String, Object> out = jdbcCargaVisita.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCTRLVISIT.SPCARGAVISITAS }");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");

        if (resultado != null) {
            respuesta = resultado.intValue();
        } else {
            respuesta = 0;
        }

        salida.setRespuestaEjecucion(respuesta);
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        if (respuesta != 1) {
            logger.info("Algo pasó con carga visitas");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Carga Visitas", "Falló la carga de visitas. \n FRANQUICIA.PAADMCTRLVISIT.SPCARGAVISITAS", null);
                salida.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en carga visitas");
                salida.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Ceco Base", "Se realizó carga de visitas correctamente. \n FRANQUICIA.PAADMCTRLVISIT.SPCARGAVISITAS", null);
                salida.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar carga visitas correcta");
                salida.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return salida;
    }

    @SuppressWarnings("unchecked")
    public RespuestaDepuraVisitasDTO depuraVisitas() throws CustomException {

        RespuestaDepuraVisitasDTO salida = new RespuestaDepuraVisitasDTO();
        Integer respuesta = 0;

        Map<String, Object> out = jdbcDepuraVisita.execute();

        logger.info("Funcion ejecutada: { FRANQUICIA.PADASHDEPURACION.SPGETCECOSPEND }");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        salida.setRespuestaEjecucion(respuesta);

        if (respuesta != 1) {
            try {
                logger.info("Algo paso con depura visitas");
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Depura Visitas", "Falló la depuración de visitas. \n FRANQUICIA.PADASHDEPURACION.SPGETCECOSPEND", null);
                salida.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en depura visitas");
                salida.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Depura Visitas", "Se realizó depuración de visitas correctamente. \n FRANQUICIA.PADASHDEPURACION.SPGETCECOSPEND", null);
                salida.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar depura visitas correcto");
                salida.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return salida;
    }

    @SuppressWarnings("unchecked")
    public RespuestaDistriDashCalifDTO distribuyeDashCalif() throws CustomException {

        RespuestaDistriDashCalifDTO respuestaDistriDashCalifDTO = new RespuestaDistriDashCalifDTO();
        Integer ejecucion;
        String respuesta;

        Map<String, Object> out = jdbcDistritucionDashCalif.execute();

        logger.info("Funcion ejecutada: { FRANQUICIA.PADIST_DASHBOARD_2.SPCARGADIARIA }");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        respuesta = (String) out.get("PA_CEJECUCION");

        respuestaDistriDashCalifDTO.setEjecucion(ejecucion);
        respuestaDistriDashCalifDTO.setRespuestaEjecucion(respuesta);
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        if (ejecucion != 1) {
            logger.info("Algo paso en la distribución de dashboard y calificación por discilina");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Dashboard y Calificación por Disciplina", "Falló la distribución de Dashboard y de Calificación por Discilplina. \n FRANQUICIA.PADIST_DASHBOARD_2.SPCARGADIARIA", null);
                respuestaDistriDashCalifDTO.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en distribución dashboard y calificación");
                respuestaDistriDashCalifDTO.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Dashboard y Calificación por Disciplina", "Se realizó la Distribución Dashboard y Calificación por Disciplina correctamente. \n FRANQUICIA.PADIST_DASHBOARD_2.SPCARGADIARIA", null);
                respuestaDistriDashCalifDTO.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de dashboard y calificación");
                respuestaDistriDashCalifDTO.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return respuestaDistriDashCalifDTO;
    }

    @SuppressWarnings("unchecked")
    public RespuestasImperdonableIncumplimientoDTO distribuyeRespImpInc() throws CustomException {

        RespuestasImperdonableIncumplimientoDTO respuestasImperdonableIncumplimientoDTO = new RespuestasImperdonableIncumplimientoDTO();
        String respuesta;
        Integer ejecucion;

        Map<String, Object> out = jdbcDistribucionRespImpInc.execute();

        logger.info("Funcion ejecutada: { FRANQUICIA.PADIST_DASHBOARD_3.SPCARGADIARIA }");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        respuesta = (String) out.get("PA_CEJECUCION");
        respuestasImperdonableIncumplimientoDTO.setEjecucion(ejecucion);
        respuestasImperdonableIncumplimientoDTO.setRespuestaEjecucion(respuesta);
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        if (ejecucion != 1) {
            logger.info("Algo pasó en la distribución de respuestas, imperdonables e incumplimientos");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Respuestas, Imperdonables e Incumplimiento", "Falló la distribución de Respuestas, imperdonables e incumplimiento. \n FRANQUICIA.PADIST_DASHBOARD_3.SPCARGADIARIA", null);
                respuestasImperdonableIncumplimientoDTO.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo en la distribución de respuestas, imperdonables e incumplimientos");
                respuestasImperdonableIncumplimientoDTO.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Respuestas, Imperdonables e Incumplimiento", "Se realizó la Distribución de Respuestas Imperdonables e Incumplimiento correctamente. \n FRANQUICIA.PADIST_DASHBOARD_3.SPCARGADIARIA", null);
                respuestasImperdonableIncumplimientoDTO.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de respuestas, imperdonables e incumplimientos");
                respuestasImperdonableIncumplimientoDTO.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return respuestasImperdonableIncumplimientoDTO;
    }

    @SuppressWarnings("unchecked")
    public RespuestaMergeDetallePreguntaDTO distMergeDetallePregunta() throws CustomException {

        RespuestaMergeDetallePreguntaDTO respuestaMergeDetallePreguntaDTO = new RespuestaMergeDetallePreguntaDTO();

        Integer ejecucion;
        Integer distribucion;
        Map<String, Object> out = jdbcDistribucionDetallePregunta.execute();

        logger.info("Funcion ejecutada: { FRANQUICIA.PAADMDETPREGUN.SPDISTMERGDETPREG }");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        ejecucion = resultado.intValue();

        BigDecimal respuesta = (BigDecimal) out.get("PA_NREGAFECTADOS");
        distribucion = respuesta.intValue();
        respuestaMergeDetallePreguntaDTO.setEjecucion(ejecucion);
        respuestaMergeDetallePreguntaDTO.setRegistrosAfectados(distribucion);
        List<String> copiados = new ArrayList<String>();
        copiados.add("xelar_fer@hotmail.com");
        copiados.add("vidal.mendoza@elektra.com.mx");
        copiados.add("caaguilar@elektra.com.mx");
        if (ejecucion != 1) {
            logger.info("Algo pasó con distribución de detalle pregunta");
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Detalle Pregunta", "Falló la distribución de Detalle Pregunta. \n FRANQUICIA.PAADMDETPREGUN.SPDISTMERGDETPREG", null);
                respuestaMergeDetallePreguntaDTO.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar fallo con distribución de detalle pregunta");
                respuestaMergeDetallePreguntaDTO.setEnvioCorreo(false);
                logger.error(e);
            }
        } else {
            try {
                Boolean envio = UtilMail.enviaCorreo("jose.fernando@elektra.com.mx", copiados, "Distribución Detalle Pregunta", "Se realizó la distribución de Detalle Pregunta. \n FRANQUICIA.PAADMDETPREGUN.SPDISTMERGDETPREG", null);
                respuestaMergeDetallePreguntaDTO.setEnvioCorreo(envio);
            } catch (Exception e) {
                logger.info("Envío de Correo no exitoso para reportar distribución correcta de detalle pregunta");
                respuestaMergeDetallePreguntaDTO.setEnvioCorreo(false);
                logger.error(e);
            }
        }

        return respuestaMergeDetallePreguntaDTO;
    }
}
