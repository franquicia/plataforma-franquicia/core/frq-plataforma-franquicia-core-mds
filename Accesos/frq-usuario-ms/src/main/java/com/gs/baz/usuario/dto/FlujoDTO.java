/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class FlujoDTO {

    @JsonProperty(value = "id_flujo")
    private Integer idFlujo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private Integer estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipo_app")
    private String tipoApp;

    public FlujoDTO() {
    }

    public Integer getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(BigDecimal idFlujo) {
        this.idFlujo = (idFlujo == null ? null : idFlujo.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(BigDecimal estatus) {
        this.estatus = (estatus == null ? null : estatus.intValue());
    }

    public String getTipoApp() {
        return tipoApp;
    }

    public void setTipoApp(String tipoApp) {
        this.tipoApp = tipoApp;
    }

    @Override
    public String toString() {
        return "FlujoDTO{" + "idFlujo=" + idFlujo + ", descripcion=" + descripcion + ", estatus=" + estatus + ", tipoApp=" + tipoApp + '}';
    }

}
