/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.envia.correo.mprs;

import com.gs.baz.frq.envia.correo.dto.ConteoNegocioSemanalLayoutsDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ConteoNegocioSemanalLayoutsRowMapper implements RowMapper<ConteoNegocioSemanalLayoutsDTO> {

    private ConteoNegocioSemanalLayoutsDTO conteoFechas;

    @Override
    public ConteoNegocioSemanalLayoutsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        conteoFechas = new ConteoNegocioSemanalLayoutsDTO();
        conteoFechas.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        conteoFechas.setNombreProyecto(rs.getString("NOMPROY"));
        conteoFechas.setValorConteo(rs.getInt("CONTEO"));
        conteoFechas.setIdNegocio(rs.getString("NEGOCIO"));

        return conteoFechas;
    }
}
