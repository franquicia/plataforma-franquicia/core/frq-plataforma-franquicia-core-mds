/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.fichas.mprs;

import com.gs.baz.frq.documentos.sucursal.fichas.dto.Ficha;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FichasRowMapper implements RowMapper<Ficha> {

    private Ficha ficha;

    @Override
    public Ficha mapRow(ResultSet rs, int rowNum) throws SQLException {
        ficha = new Ficha();
        ficha.setIdFicha(rs.getInt("FIID_FICHA"));
        ficha.setIdCeco(rs.getInt("FCID_CECO"));
        ficha.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        ficha.setIdFormato(rs.getInt("FIID_FORMATO"));
        ficha.setIdRegion(rs.getInt("FIID_REGION"));
        ficha.setRegion(rs.getString("FCREGION"));
        ficha.setIdZona(rs.getInt("FIID_ZONA"));
        ficha.setZona(rs.getString("FCZONA"));
        ficha.setIdTerritorio(rs.getInt("FIID_TERRITORIO"));
        ficha.setTerritorio(rs.getString("FCTERRITORIO"));
        ficha.setLatitud(rs.getString("FCLATITUD"));
        ficha.setLongitud(rs.getString("FCLONGITUD"));
        ficha.setDireccion(rs.getString("FCDIRECCION"));
        ficha.setIdFolio(rs.getInt("FIID_FOLIO"));
        return ficha;
    }
}
