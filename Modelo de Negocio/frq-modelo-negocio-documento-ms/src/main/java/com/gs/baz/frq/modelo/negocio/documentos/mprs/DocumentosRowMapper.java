/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.documentos.mprs;

import com.gs.baz.frq.modelo.negocio.documentos.dto.Documento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentosRowMapper implements RowMapper<Documento> {

    private Documento documento;

    @Override
    public Documento mapRow(ResultSet rs, int rowNum) throws SQLException {
        documento = new Documento();
        documento.setIdDocumento(rs.getInt("FIIDDOCUMENTO"));
        documento.setTitulo(rs.getString("FCTITULO"));
        documento.setObjetivo(rs.getString("FCOBJETIVO"));
        documento.setVersion(rs.getString("FCVERSION"));
        documento.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        documento.setFechaInicio(rs.getString("FDVIGENTE"));
        //documento.setEliminado(rs.getInt("FIELIMINADO"));
        return documento;
    }
}
