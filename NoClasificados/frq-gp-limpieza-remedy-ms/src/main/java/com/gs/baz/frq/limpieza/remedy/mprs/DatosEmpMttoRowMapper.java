package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.DatosEmpMttoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DatosEmpMttoRowMapper implements RowMapper<DatosEmpMttoDTO> {

    @Override
    public DatosEmpMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        DatosEmpMttoDTO datos = new DatosEmpMttoDTO();

        datos.setIdUsuario(rs.getInt("FIID_USUARIO"));
        datos.setNombre(rs.getString("FCNOMBRE"));
        datos.setDescripcion(rs.getString("FCDESCRIPCION"));
        datos.setCeco(rs.getInt("FCID_CECO"));
        datos.setTelefono(rs.getString("FCTELEFONO"));
        datos.setIdPuesto(rs.getInt("FIID_PUESTO"));

        return datos;
    }

}
