/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.rest.web.socket.controller;

/**
 *
 * @author cescobarh
 */
public class Action {

    private Type action;
    private Object result;

    public enum Type {

        SELF_CONNECT,
        NODES_CONNECT;
    }

    public Action() {
    }

    public Action(Type action) {
        this.action = action;
    }

    public Type getAction() {
        return action;
    }

    public void setAction(Type action) {
        this.action = action;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Action{" + "action=" + action + ", result=" + result + '}';
    }

}
