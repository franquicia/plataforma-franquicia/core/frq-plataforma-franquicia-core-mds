/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g008;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class NegocioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cumplimiento_seccion")
    private CumplimientoSeccionNegocioDTO cumplimientoSeccionNegocioDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public NegocioDTO(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public NegocioDTO() {
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public CumplimientoSeccionNegocioDTO getCumplimientoSeccionNegocioDTO() {
        return cumplimientoSeccionNegocioDTO;
    }

    public void setCumplimientoSeccionNegocioDTO(CumplimientoSeccionNegocioDTO cumplimientoSeccionNegocioDTO) {
        this.cumplimientoSeccionNegocioDTO = cumplimientoSeccionNegocioDTO;
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", cumplimientoSeccionNegocioDTO=" + cumplimientoSeccionNegocioDTO + ", filtros=" + filtros + '}';
    }

}
