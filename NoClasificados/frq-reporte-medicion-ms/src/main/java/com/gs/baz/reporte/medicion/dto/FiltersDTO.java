/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class FiltersDTO {

    @JsonProperty(value = "fecha_inicio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fechaInicio;

    @JsonProperty(value = "fecha_fin")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fechaFin;

    @JsonProperty(value = "nivel_geografico")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer nivel;

    @JsonProperty(value = "id_disciplina")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idDisciplina;

    @JsonProperty(value = "disciplina_descripcion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String disciplinaDescripcion;

    @JsonProperty(value = "id_protocolo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idProtocolo;

    @JsonProperty(value = "protocolo_descripcion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String protocoloDescripcion;

    @JsonProperty(value = "id_ceco")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String idCeco;

    @JsonProperty(value = "id_checklist")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idChecklist;

    @JsonProperty(value = "id_negocio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idNegocio;

    @JsonProperty(value = "negocio_descripcion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String negocioDescripcion;

    @JsonProperty(value = "anio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer anio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_periodo")
    private Integer idPeriodo;

    @JsonProperty(value = "id_dashboard")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idDashboard;

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public String getDisciplinaDescripcion() {
        return disciplinaDescripcion;
    }

    public void setDisciplinaDescripcion(String disciplinaDescripcion) {
        this.disciplinaDescripcion = disciplinaDescripcion;
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(Integer idProtocolo) {
        this.idProtocolo = idProtocolo;
    }

    public String getProtocoloDescripcion() {
        return protocoloDescripcion;
    }

    public void setProtocoloDescripcion(String protocoloDescripcion) {
        this.protocoloDescripcion = protocoloDescripcion;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(Integer idChecklist) {
        this.idChecklist = idChecklist;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getNegocioDescripcion() {
        return negocioDescripcion;
    }

    public void setNegocioDescripcion(String negocioDescripcion) {
        this.negocioDescripcion = negocioDescripcion;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(Integer idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public Integer getIdDashboard() {
        return idDashboard;
    }

    public void setIdDashboard(Integer idDashboard) {
        this.idDashboard = idDashboard;
    }

    @Override
    public String toString() {
        return "FiltersDTO{" + "fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", nivel=" + nivel + ", idDisciplina=" + idDisciplina + ", disciplinaDescripcion=" + disciplinaDescripcion + ", idProtocolo=" + idProtocolo + ", protocoloDescripcion=" + protocoloDescripcion + ", idCeco=" + idCeco + ", idChecklist=" + idChecklist + ", idNegocio=" + idNegocio + ", negocioDescripcion=" + negocioDescripcion + ", anio=" + anio + ", idPeriodo=" + idPeriodo + ", idDashboard=" + idDashboard + '}';
    }

}
