/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.accesos.sistema.vistas.dto.SinResultado;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dao.AccesosSistemaVistasPropiedadesDAOImpl;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.AltaVistaPropiedad;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.ParametrosVistaPropiedad;
import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.VistaPropiedades;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "accesos", value = "accesos", description = "Api para la gestión del catalogo de propiedades de la vista")
@RestController
@RequestMapping("/api-local/accesos/sistema/vistas/v1")
public class AccesosSistemaVistasPropiedadesApi {

    @Autowired
    private AccesosSistemaVistasPropiedadesDAOImpl accesosSistemaVistasPropiedadesDAOImpl;

    /**
     *
     * @param idVista
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene las propiedades de la vista", notes = "Obtiene todas las propiedades de la vista", nickname = "obtienePropiedadesVista")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idVista}/propiedades", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public VistaPropiedades obtieneVistas(@ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws CustomException, DataNotFoundException {
        VistaPropiedades vistaPropiedades = new VistaPropiedades(accesosSistemaVistasPropiedadesDAOImpl.selectAllRows(idVista));
        if (vistaPropiedades.getVistaPropiedades() != null && vistaPropiedades.getVistaPropiedades().isEmpty()) {
            throw new DataNotFoundException();
        }
        return vistaPropiedades;
    }

    /**
     *
     * @param parametrosVistaPropiedad
     * @param idVista
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear propiedad vista", notes = "Agrega una propiedad de la vista", nickname = "creaPropiedadVista")
    @RequestMapping(value = "/{idVista}/propiedades", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaVistaPropiedad creaVistaPropiedad(@ApiParam(name = "ParametrosVistaPropiedad", value = "Paramentros para el alta de propiedad vista", required = true) @RequestBody ParametrosVistaPropiedad parametrosVistaPropiedad, @ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotInsertedException {
        parametrosVistaPropiedad.setIdVista(idVista);
        return accesosSistemaVistasPropiedadesDAOImpl.insertRow(parametrosVistaPropiedad);
    }

    /**
     *
     * @param parametrosVistaPropiedad
     * @param idVista
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar propiedad vista", notes = "Actualiza propiedad de la vista", nickname = "actualizaPropiedadVista")
    @RequestMapping(value = "/{idVista}/propiedades", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaPropiedadVista(@ApiParam(name = "ParametrosVistaPropiedad", value = "Paramentros para la actualización del vista", required = true) @RequestBody ParametrosVistaPropiedad parametrosVistaPropiedad, @ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotUpdatedException {
        parametrosVistaPropiedad.setIdVista(idVista);
        accesosSistemaVistasPropiedadesDAOImpl.updateRow(parametrosVistaPropiedad);
        return new SinResultado();
    }

    /**
     *
     * @param idPropiedad
     * @param idVista
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar vista", notes = "Elimina un item de las propiedades", nickname = "eliminaPropiedadVista")
    @RequestMapping(value = "/{idVista}/propiedades/{idPropiedad}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaPropiedadVista(@ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Integer idVista, @ApiParam(name = "idPropiedad", value = "Identificador de la propiedad", example = "1", required = true) @PathVariable("idPropiedad") Integer idPropiedad) throws DataNotDeletedException {
        accesosSistemaVistasPropiedadesDAOImpl.deleteRow(idVista, idPropiedad);
        return new SinResultado();
    }

}
