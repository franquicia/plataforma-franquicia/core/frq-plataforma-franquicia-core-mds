/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.formatos.rest;

import com.gs.baz.frq.documento.sucursal.formatos.dao.FormatoDAOImpl;
import com.gs.baz.frq.documento.sucursal.formatos.dto.AltaFormato;
import com.gs.baz.frq.documento.sucursal.formatos.dto.FormatoBase;
import com.gs.baz.frq.documento.sucursal.formatos.dto.Formatos;
import com.gs.baz.frq.documento.sucursal.formatos.dto.ParametrosFormato;
import com.gs.baz.frq.documento.sucursal.formatos.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "formatos", value = "formatos", description = "Api para la gestión del catalogo de formatos")
@RestController
@RequestMapping("/api-local/documento-sucursal/formato/v1")
public class FormatoApi {

    @Autowired
    private FormatoDAOImpl formatoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFormato
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene formato", notes = "Obtiene un formato", nickname = "obtieneFormato")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idNegocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FormatoBase> obtieneFormato(@ApiParam(name = "idNegocio", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idNegocio") Long idNegocio) throws CustomException, DataNotFoundException {
        List<FormatoBase> formatoBase = formatoDAOImpl.selectRow(idNegocio);
        if (formatoBase == null) {
            throw new DataNotFoundException();
        }
        return formatoBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene formatos", notes = "Obtiene todos los formatos", nickname = "obtieneFormatos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Formatos obtieneFormatos() throws CustomException, DataNotFoundException {
        Formatos formatos = new Formatos(formatoDAOImpl.selectAllRows());
        if (formatos.getFormatos() != null && formatos.getFormatos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return formatos;
    }

    /**
     *
     * @param parametrosFormato
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear formato", notes = "Agrega un formato", nickname = "creaFormato")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFormato creaFormato(@ApiParam(name = "ParametrosFormato", value = "Paramentros para el alta del formato", required = true) @RequestBody ParametrosFormato parametrosFormato) throws DataNotInsertedException {
        return formatoDAOImpl.insertRow(parametrosFormato);
    }

    /**
     *
     * @param parametrosFormato
     * @param idFormato
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar formato", notes = "Actualiza un formato", nickname = "actualizaFormato")
    @RequestMapping(value = "/{idFormato}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFormato(@ApiParam(name = "ParametrosFormato", value = "Paramentros para la actualización del formato", required = true) @RequestBody ParametrosFormato parametrosFormato, @ApiParam(name = "idFormato", value = "Identificador del formato", example = "1", required = true) @PathVariable("idFormato") Integer idFormato) throws DataNotUpdatedException {
        parametrosFormato.setIdFormato(idFormato);
        formatoDAOImpl.updateRow(parametrosFormato);
        return new SinResultado();
    }

    /**
     *
     * @param idFormato
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar formato", notes = "Elimina un item de los formatos", nickname = "eliminaFormato")
    @RequestMapping(value = "/{idFormato}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaFormato(@ApiParam(name = "idFormato", value = "Identificador del formato", example = "1", required = true) @PathVariable("idFormato") Integer idFormato) throws DataNotDeletedException {
        formatoDAOImpl.deleteRow(idFormato);
        return new SinResultado();
    }

}
