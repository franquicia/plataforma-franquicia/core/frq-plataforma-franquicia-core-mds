package com.gs.baz.ceco.dao;

import com.gs.baz.ceco.dao.util.GenericDAO;
import com.gs.baz.ceco.dto.CecoDTO;
import com.gs.baz.ceco.mprs.CecoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class CecoDAOImpl extends DefaultDAO implements GenericDAO<CecoDTO> {

    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectRegion;
    private DefaultJdbcCall jdbcSelectZona;
    private DefaultJdbcCall jdbcSelectTerritorio;
    private DefaultJdbcCall jdbcSelectAllGenerico;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAADMINCECOS");
        jdbcSelectAll.withProcedureName("SP_SEL_CECOS");
        jdbcSelectAll.returningResultSet("RCL_INFO", new CecoRowMapper().new consultaGeneral());

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINCECOS");
        jdbcSelect.withProcedureName("SP_SEL_CECO");
        jdbcSelect.returningResultSet("RCL_INFO", new CecoRowMapper().new consultaEspecifica());

        jdbcSelectRegion = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectRegion.withSchemaName(schema);
        jdbcSelectRegion.withCatalogName("PAADMINCECOS");
        jdbcSelectRegion.withProcedureName("SP_SEL_REGION");
        jdbcSelectRegion.returningResultSet("RCL_INFO", new CecoRowMapper().new consultaEspecifica());

        jdbcSelectZona = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectZona.withSchemaName(schema);
        jdbcSelectZona.withCatalogName("PAADMINCECOS");
        jdbcSelectZona.withProcedureName("SP_SEL_ZONA");
        jdbcSelectZona.returningResultSet("RCL_INFO", new CecoRowMapper().new consultaEspecifica());

        jdbcSelectTerritorio = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectTerritorio.withSchemaName(schema);
        jdbcSelectTerritorio.withCatalogName("PAADMINCECOS");
        jdbcSelectTerritorio.withProcedureName("SP_SEL_TERRITORIO");
        jdbcSelectTerritorio.returningResultSet("RCL_INFO", new CecoRowMapper().new consultaEspecifica());

        jdbcSelectAllGenerico = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAllGenerico.withSchemaName(schema);
        jdbcSelectAllGenerico.withCatalogName("PAADMINCECOS");
        jdbcSelectAllGenerico.withProcedureName("SP_SEL_CECOSGRAL");
        jdbcSelectAllGenerico.returningResultSet("RCL_INFO", new CecoRowMapper().new consultaGeneral());
    }

    @Override
    public CecoDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<CecoDTO> data = (List<CecoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<CecoDTO> selectRows(String ceco, String nombre) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", ceco);
        mapSqlParameterSource.addValue("PA_FCNOMBRE", nombre);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        List<CecoDTO> data = (List<CecoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public List<CecoDTO> selectRowsRegion() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        Map<String, Object> out = jdbcSelectRegion.execute(mapSqlParameterSource);
        List<CecoDTO> data = (List<CecoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public List<CecoDTO> selectRowsZona() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        Map<String, Object> out = jdbcSelectZona.execute(mapSqlParameterSource);
        List<CecoDTO> data = (List<CecoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public List<CecoDTO> selectRowsTerritorio() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        Map<String, Object> out = jdbcSelectTerritorio.execute(mapSqlParameterSource);
        List<CecoDTO> data = (List<CecoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public List<CecoDTO> selectRowsGen(CecoDTO cecos) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDCECO", cecos.getIdCeco());
        mapSqlParameterSource.addValue("PA_FCNOMBRE", cecos.getDescripcion());
        mapSqlParameterSource.addValue("PA_CANALES", cecos.getIdCanal());
        Map<String, Object> out = jdbcSelectAllGenerico.execute(mapSqlParameterSource);
        List<CecoDTO> data = (List<CecoDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
