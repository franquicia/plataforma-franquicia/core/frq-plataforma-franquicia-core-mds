/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class PortalCautivoDTO {

    @JsonProperty(value = "idUsuario")
    private Integer idUsuario;

    @JsonProperty(value = "ip")
    private String ip;

    @JsonProperty(value = "mac")
    private String mac;

    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonProperty(value = "nombre")
    private String nombre;

    @JsonProperty(value = "apellidoPaterno")
    private String apellidoPaterno;

    @JsonProperty(value = "apellidoMaterno")
    private String apellidoMaterno;

    @JsonProperty(value = "nombreCompleto")
    private String nombreCompleto;

    @JsonProperty(value = "inicioSesion")
    private String inicioSesion;

    @JsonProperty(value = "correo")
    private String correo;

    @JsonProperty(value = "enviar")
    private String enviar;

    @JsonProperty(value = "tienda")
    private String sucursal;

    @JsonProperty(value = "rangoEdad")
    private String rangoEdad;

    @JsonProperty(value = "genero")
    private String genero;

    @JsonProperty(value = "urlFoto")
    private String urlFoto;

    @JsonProperty(value = "tipoSesion")
    private String tipoSesion;

    @JsonProperty(value = "faseSesion")
    private String faseSesion;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getInicioSesion() {
        return inicioSesion;
    }

    public void setInicioSesion(String inicioSesion) {
        this.inicioSesion = inicioSesion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getEnviar() {
        return enviar;
    }

    public void setEnviar(String enviar) {
        this.enviar = enviar;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getRangoEdad() {
        return rangoEdad;
    }

    public void setRangoEdad(String rangoEdad) {
        this.rangoEdad = rangoEdad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getTipoSesion() {
        return tipoSesion;
    }

    public void setTipoSesion(String tipoSesion) {
        this.tipoSesion = tipoSesion;
    }

    public String getFaseSesion() {
        return faseSesion;
    }

    public void setFaseSesion(String faseSesion) {
        this.faseSesion = faseSesion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return "PortalCautivoDTO{" + "idUsuario=" + idUsuario + ", ip=" + ip + ", mac=" + mac + ", nombreUsuario=" + nombreUsuario + ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", nombreCompleto=" + nombreCompleto + ", inicioSesion=" + inicioSesion + ", correo=" + correo + ", enviar=" + enviar + ", sucursal=" + sucursal + ", rangoEdad=" + rangoEdad + ", genero=" + genero + ", urlFoto=" + urlFoto + ", tipoSesion=" + tipoSesion + ", faseSesion=" + faseSesion + '}';
    }

}
