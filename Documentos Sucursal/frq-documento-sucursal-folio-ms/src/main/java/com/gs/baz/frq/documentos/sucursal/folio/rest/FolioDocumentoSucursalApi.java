/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.folio.rest;

import com.gs.baz.frq.documentos.sucursal.folio.dao.DocumentoSucursalFolioDAOImpl;
import com.gs.baz.frq.documentos.sucursal.folio.dto.AltaFolio;
import com.gs.baz.frq.documentos.sucursal.folio.dto.Folio;
import com.gs.baz.frq.documentos.sucursal.folio.dto.Folios;
import com.gs.baz.frq.documentos.sucursal.folio.dto.ParametrosFolio;
import com.gs.baz.frq.documentos.sucursal.folio.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fjaramillo
 */
@Api(tags = "FolioDocumentoSucursalApi", value = "FolioDocumentoSucursalApi", description = "Api para la gestión del catalogo de folios")
@RestController
@RequestMapping("/api-local/documento-sucursal/folio/v1")
public class FolioDocumentoSucursalApi {

    @Autowired
    private DocumentoSucursalFolioDAOImpl documentoSucursalFolioDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ficha", notes = "Obtiene un ficha", nickname = "obtieneFicha")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idFolio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Folio obtieneFicha(@ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {
        Folio fichaBase = documentoSucursalFolioDAOImpl.selectRow(idFolio);
        if (fichaBase == null) {
            throw new DataNotFoundException();
        }
        return fichaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene folios", notes = "Obtiene todos los fichas", nickname = "obtieneFolios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Folios obtieneFolios() throws CustomException, DataNotFoundException {
        Folios folios = new Folios(documentoSucursalFolioDAOImpl.selectAllRows());
        if (folios.getFolios() != null && folios.getFolios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return folios;
    }

    /**
     *
     * @param parametrosFicha
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear folio", notes = "Agrega un folio", nickname = "creaFolio")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaFolio creaFolio(@ApiParam(name = "ParametrosFicha", value = "Paramentros para el alta del ficha", required = true) @RequestBody ParametrosFolio parametrosFicha) throws DataNotInsertedException {
        return documentoSucursalFolioDAOImpl.insertRow(parametrosFicha);
    }

    /**
     *
     * @param parametrosFolio
     * @param idFolio
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar folio", notes = "Actualiza un folio", nickname = "actualizaFolio")
    @RequestMapping(value = "/{idFolio}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaFicha(
            @ApiParam(name = "ParametrosFolio", value = "Paramentros para la actualización del folio", required = true) @RequestBody ParametrosFolio parametrosFolio,
            @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio)
            throws DataNotUpdatedException {

        documentoSucursalFolioDAOImpl.updateRow(idFolio, parametrosFolio);
        return new SinResultado();
    }

    /**
     *
     * @param idFolio
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar folio", notes = "Elimina un item de los fichas", nickname = "eliminaFicha")
    @RequestMapping(value = "/{idFolio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaFicha(
            @ApiParam(name = "idFolio", value = "Identificador del folio", example = "1", required = true) @PathVariable("idFolio") Integer idFolio)
            throws DataNotDeletedException {
        documentoSucursalFolioDAOImpl.deleteRow(idFolio);
        return new SinResultado();
    }

}
