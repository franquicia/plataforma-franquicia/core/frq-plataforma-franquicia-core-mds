/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NegocioDTO {

    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_status")
    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_colorimetria")
    private Integer idColorimetria;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "colorimetria")
    private ColorimetriaDTO colorimetria;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "categorias")
    private List<CategoriaDTO> categorias;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "img_logo")
    private Object logo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "path_logo")
    private String pathLogo;

    public NegocioDTO() {
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdColorimetria() {
        return idColorimetria;
    }

    public void setIdColorimetria(BigDecimal idColorimetria) {
        this.idColorimetria = (idColorimetria == null ? null : idColorimetria.intValue());
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ColorimetriaDTO getColorimetria() {
        return colorimetria;
    }

    public void setColorimetria(ColorimetriaDTO colorimetria) {
        this.colorimetria = colorimetria;
    }

    public List<CategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaDTO> categorias) {
        this.categorias = categorias;
    }

    public Object getLogo() {
        return logo;
    }

    public void setLogo(Object logo) {
        this.logo = logo;
    }

    public String getPathLogo() {
        return pathLogo;
    }

    public void setPathLogo(String pathLogo) {
        this.pathLogo = pathLogo;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", descripcion=" + descripcion + ", idColorimetria=" + idColorimetria + ", logo=" + logo + '}';
    }

}
