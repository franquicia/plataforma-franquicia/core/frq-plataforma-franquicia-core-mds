/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EjecutaDTO {

    @JsonProperty(value = "id_ejecuta")
    private Integer idEjecuta;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public EjecutaDTO() {
    }

    public Integer getIdEjecuta() {
        return idEjecuta;
    }

    public void setIdEjecuta(BigDecimal idEjecuta) {
        this.idEjecuta = (idEjecuta == null ? null : idEjecuta.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "EjecutaDTO{" + "idEjecuta=" + idEjecuta + ", descripcion=" + descripcion + '}';
    }

}
