/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g014;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class CalificacionDisciplinaArbolDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "resultado")
    private Integer resultado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getResutado() {
        return resultado;
    }

    public void setResultado(BigDecimal resultado) {
        this.resultado = (resultado == null ? null : resultado.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "CalificacionDisciplinaArbolDTO{" + "idDisciplina=" + idDisciplina + ", resultado=" + resultado + ", filtros=" + filtros + '}';
    }

}
