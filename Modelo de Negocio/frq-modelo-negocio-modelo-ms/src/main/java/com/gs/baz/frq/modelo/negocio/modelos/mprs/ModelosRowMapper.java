/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.modelos.mprs;

import com.gs.baz.frq.modelo.negocio.modelos.dto.Modelo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ModelosRowMapper implements RowMapper<Modelo> {

    private Modelo modelo;

    @Override
    public Modelo mapRow(ResultSet rs, int rowNum) throws SQLException {
        modelo = new Modelo();
        modelo.setIdModelo(rs.getInt("FIIDMODELO"));
        modelo.setDescripcion(rs.getString("FCDESCRIPCION"));
        modelo.setAcronimo(rs.getString("FCACRONIMO"));
        return modelo;
    }
}
