/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de la  geografia del ceco", value = "GeografiaBase")
public class GeografiaBase {

    @JsonProperty(value = "idRegion")
    @ApiModelProperty(notes = "identificador de la region", example = "236399")
    private Integer idRegion;

    @JsonProperty(value = "region")
    @ApiModelProperty(notes = "Descripcion de la region", example = "REGIONAL SB TLALPAN")
    private String region;

    @JsonProperty(value = "idZona")
    @ApiModelProperty(notes = "identificador de la zona", example = "232822")
    private Integer idZona;

    @JsonProperty(value = "zona")
    @ApiModelProperty(notes = "Descripcion de la zona", example = "ZONA SB TOLUCA MEXICO PONIENTE")
    private String zona;

    @JsonProperty(value = "idTerritorio")
    @ApiModelProperty(notes = "identificador del territorio", example = "237948")
    private Integer idTerritorio;

    @JsonProperty(value = "territorio")
    @ApiModelProperty(notes = "Descripcion del territorio", example = "TERRITORIO SB CENTRO PONIENTE")
    private String territorio;

    public Integer getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Integer idRegion) {
        this.idRegion = idRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region != null ? region : "";
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona != null ? zona : "";
    }

    public Integer getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(Integer idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio != null ? territorio : "";
    }

}
