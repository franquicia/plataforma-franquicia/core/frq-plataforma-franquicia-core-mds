/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dao.SistemaInformacionDashboardGraficaSemaforoDAOImpl;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.AltaGraficaSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaSemaforoBase;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaSemaforos;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.ParametrosGraficaSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.SinResultado;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@Api(tags = "grafica-semaforos", value = "grafica-semafos", description = "Api para la gestión del catalogo de grafica semaforos")
@RestController
@RequestMapping("/api-local/sistema-informacion/tablero/graficas/v1")
public class SistemaInformacionDashboardGraficaSemaforoApi {

    @Autowired
    private SistemaInformacionDashboardGraficaSemaforoDAOImpl sistemaInformacionDashboardGraficaSemaforoDAOImpl;

    /**
     *
     * @param idGrafica
     * @param regla
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene semaforo de una grafica", notes = "Obtiene semaforo de una grafica", nickname = "obtieneGraficaSemaforo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrafica}/semaforos/regla", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GraficaSemaforoBase obtieneGraficaSemaforo(
            @ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica,
            @ApiParam(name = "regla", value = "Regla de la grafica semaforo", example = "defecto", required = false) @RequestParam(value = "regla", defaultValue = "defecto", required = false) String regla) throws CustomException, DataNotFoundException {
        GraficaSemaforoBase graficaBase = sistemaInformacionDashboardGraficaSemaforoDAOImpl.selectRow(idGrafica, regla);
        if (graficaBase == null) {
            throw new DataNotFoundException();
        }
        return graficaBase;
    }

    /**
     *
     * @param idGrafica
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene semaforos grafica", notes = "Obtiene semaforos grafica", nickname = "obtieneGraficaSemaforos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idGrafica}/semaforos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GraficaSemaforos obtieneGraficaSemaforos(@ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica) throws CustomException, DataNotFoundException {
        GraficaSemaforos semaforos = new GraficaSemaforos(sistemaInformacionDashboardGraficaSemaforoDAOImpl.selectAllRows(idGrafica));
        if (semaforos.getSemaforos() != null && semaforos.getSemaforos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return semaforos;
    }

    /**
     *
     * @param idGrafica
     * @param parametrosGraficaSemaforo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Asociar semaforo a una grafica", notes = "Asociar semaforo a una grafica", nickname = "asociarGraficaSemaforo")
    @RequestMapping(value = "/{idGrafica}/semaforos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaGraficaSemaforo asociarGraficaSemaforo(
            @ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica,
            @ApiParam(name = "ParametrosGraficaSemaforo", value = "Paramentros para el alta de la grafica", required = true) @RequestBody ParametrosGraficaSemaforo parametrosGraficaSemaforo) throws DataNotInsertedException {
        parametrosGraficaSemaforo.setIdGrafica(idGrafica);
        return sistemaInformacionDashboardGraficaSemaforoDAOImpl.insertRow(parametrosGraficaSemaforo);
    }

    /**
     *
     * @param idGrafica
     * @param idSemaforo
     * @param regla
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar semaforo de la grafica", notes = "Elimina un item de los semaforos de la grafica", nickname = "eliminaGraficaSemaforo")
    @RequestMapping(value = "/{idGrafica}/semaforos/{idSemaforo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaGraficaSemaforo(
            @ApiParam(name = "idGrafica", value = "Identificador de la grafica", example = "1", required = true) @PathVariable("idGrafica") Integer idGrafica,
            @ApiParam(name = "idSemaforo", value = "Identificador del semaforo", example = "1", required = true) @PathVariable("idSemaforo") Integer idSemaforo,
            @ApiParam(name = "regla", value = "Regla de la grafica semaforo", example = "1", required = true) @RequestParam("regla") String regla) throws DataNotDeletedException {
        sistemaInformacionDashboardGraficaSemaforoDAOImpl.deleteRow(idGrafica, idSemaforo, regla);
        return new SinResultado();
    }

}
