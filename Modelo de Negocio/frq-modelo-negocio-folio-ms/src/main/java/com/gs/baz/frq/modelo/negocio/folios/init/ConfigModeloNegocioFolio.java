package com.gs.baz.frq.modelo.negocio.folios.init;

import com.gs.baz.frq.modelo.negocio.folios.dao.FolioComentarioDAOImpl;
import com.gs.baz.frq.modelo.negocio.folios.dao.FolioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.folios")
public class ConfigModeloNegocioFolio {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioFolio() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public FolioDAOImpl folioDAOImpl() {
        return new FolioDAOImpl();
    }

    @Bean(initMethod = "init")
    public FolioComentarioDAOImpl folioComentarioDAOImpl() {
        return new FolioComentarioDAOImpl();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
