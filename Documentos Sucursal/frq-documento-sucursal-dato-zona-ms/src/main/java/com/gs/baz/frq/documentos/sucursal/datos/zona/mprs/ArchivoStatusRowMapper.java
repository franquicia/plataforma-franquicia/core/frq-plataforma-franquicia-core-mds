/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.mprs;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ArchivoStatus;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ArchivoStatusRowMapper implements RowMapper<ArchivoStatus> {

    private ArchivoStatus archivoStatus;

    @Override
    public ArchivoStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
        archivoStatus = new ArchivoStatus();
        archivoStatus.setIdArchivo(rs.getInt("FIID_ARCHIVO"));
        archivoStatus.setIdArchivoPadre(rs.getInt("FIID_ARCHPADRE"));
        archivoStatus.setIdCeco(rs.getInt("FCID_CECO"));
        archivoStatus.setRuta(rs.getString("FCRUTA"));
        archivoStatus.setIdEstatusEdicion(rs.getInt("FISTATUS_EDICION"));
        archivoStatus.setIdFolio(rs.getInt("FIID_FOLIO"));
        archivoStatus.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        archivoStatus.setNombreProyecto(rs.getString("NOMPROY"));
        archivoStatus.setIdFormato(rs.getInt("FIID_FORMATO"));
        //archivoStatus.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        //archivoStatus.setNombreFormato(rs.getString("NOMFORMATO"));
        archivoStatus.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        archivoStatus.setNombreDocumento(rs.getString("FCNOMBRE"));
        archivoStatus.setDescripcionArchivo(rs.getString("FCDESCRIPCION"));
        archivoStatus.setIdVersion(rs.getInt("FIID_VERSION"));
        archivoStatus.setIdNegocio(rs.getInt("FIID_NEGOCIO"));

        return archivoStatus;
    }
}
