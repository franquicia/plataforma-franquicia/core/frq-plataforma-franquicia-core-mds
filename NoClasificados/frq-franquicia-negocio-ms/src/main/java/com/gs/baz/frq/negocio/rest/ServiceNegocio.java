/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.negocio.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.negocio.dao.NegocioDAOImpl;
import com.gs.baz.frq.negocio.dto.NegocioDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/negocio/disciplina")
@Component("FRQServiceNegocio")
public class ServiceNegocio {

    @Autowired
    private NegocioDAOImpl negocioDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO postNegocio(@RequestBody NegocioDTO negocioFranquicia) throws CustomException {
        if (negocioFranquicia.getIdNegocioFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio_franquicia"));
        }
        if (negocioFranquicia.getNegocioDisciplinaFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("negocio_disciplina_franquicia"));
        }
        return negocioDAOImpl.insertRow(negocioFranquicia);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO putNegocio(@RequestBody NegocioDTO negocioFranquicia) throws CustomException {
        if (negocioFranquicia.getIdNegocioFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio_franquicia"));
        }
        if (negocioFranquicia.getNegocioDisciplinaFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("negocio_disciplina_franquicia"));
        }
        return negocioDAOImpl.updateRow(negocioFranquicia);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getNegocio() throws CustomException {
        return negocioDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_negocio_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO getNegocio(@PathVariable("id_negocio_franquicia") Long id_negocio_franquicia) throws CustomException {
        return negocioDAOImpl.selectRow(id_negocio_franquicia);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO deleteNegocio(@RequestBody NegocioDTO negocio) throws CustomException {
        if (negocio.getIdNegocioFranquicia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina_franquicia"));
        }
        return negocioDAOImpl.deleteRow(negocio);
    }
}
