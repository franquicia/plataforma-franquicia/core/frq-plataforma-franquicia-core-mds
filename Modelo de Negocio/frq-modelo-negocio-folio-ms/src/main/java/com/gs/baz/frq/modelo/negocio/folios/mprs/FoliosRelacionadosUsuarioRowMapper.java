/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioAutorizacion;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FoliosRelacionadosUsuarioRowMapper implements RowMapper<BusquedaFolioAutorizacion> {

    private BusquedaFolioAutorizacion folioBusqueda;

    @Override
    public BusquedaFolioAutorizacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        folioBusqueda = new BusquedaFolioAutorizacion();
        folioBusqueda.setIdFolio(rs.getInt("FIIDFOLIO"));
        folioBusqueda.setDocumento(new Documento(rs.getString("FCTITULO")));
        return folioBusqueda;
    }
}
