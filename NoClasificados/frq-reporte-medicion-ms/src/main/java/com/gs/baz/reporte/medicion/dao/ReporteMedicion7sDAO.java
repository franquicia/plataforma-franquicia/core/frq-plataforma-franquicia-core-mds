/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dao;

import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.reporte.medicion.dto.FiltersDTO;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ReporteMedicion7sDAO extends DefaultDAO {

    private final Logger logger = LogManager.getLogger();
    private String schema;

    private DefaultJdbcCall jdbcSelectVisitas;
    private DefaultJdbcCall jdbcSelectCalificaciones;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectVisitas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectVisitas.withSchemaName(schema);
        jdbcSelectVisitas.withCatalogName("PAREPORTEMEDINT");
        jdbcSelectVisitas.withProcedureName("SPREP_VISITAS_7S");

        jdbcSelectCalificaciones = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificaciones.withSchemaName(schema);
        jdbcSelectCalificaciones.withCatalogName("PAREPORTEMEDINT");
        jdbcSelectCalificaciones.withProcedureName("SPREP_CALIF_7S");

    }

    public Object selectVisitas(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA_INI", filtersDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHA_FIN", filtersDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FCID_CECO", filtersDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", filtersDTO.getIdNegocio());

        Map<String, Object> out = jdbcSelectVisitas.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return out.get("PA_VISITAS");

        } else {
            return null;
        }
    }

    public Object selectCalificaciones(FiltersDTO filtersDTO) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHA_INI", filtersDTO.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHA_FIN", filtersDTO.getFechaFin());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", filtersDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FCID_CECO", filtersDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", filtersDTO.getIdNegocio());

        Map<String, Object> out = jdbcSelectCalificaciones.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return out.get("PA_CALIFICACION");

        } else {
            return null;
        }
    }

}
