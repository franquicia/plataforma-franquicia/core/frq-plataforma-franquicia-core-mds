package com.gs.baz.reporte.medicion.dto.mprs;

import com.gs.baz.reporte.medicion.dto.VersionChecklistDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class VersionChecklistRowMapper implements RowMapper<VersionChecklistDTO> {

    private VersionChecklistDTO versionChecklistDTO;

    @Override
    public VersionChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        versionChecklistDTO = new VersionChecklistDTO();
        versionChecklistDTO.setIdChecklist((BigDecimal) rs.getObject("FIID_CHECKLIST"));
        versionChecklistDTO.setIdProtocolo((BigDecimal) rs.getObject("FIID_PROTOCOLO"));
        versionChecklistDTO.setFechaInicio(rs.getString("FDFECHAINICIO"));
        versionChecklistDTO.setFechaFin(rs.getString("FDFECHAFIN"));
        return versionChecklistDTO;
    }
}
