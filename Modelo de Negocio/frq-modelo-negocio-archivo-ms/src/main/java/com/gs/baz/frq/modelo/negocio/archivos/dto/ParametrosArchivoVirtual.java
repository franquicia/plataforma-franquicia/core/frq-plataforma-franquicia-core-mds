/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosArchivoVirtual {

    private transient Integer idArchivo;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del Folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "uid")
    @ApiModelProperty(notes = "Ruta en la que se guardó el archivo", example = "/franquicia/modelo-negocio/", required = true)
    private Long uid;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "descripcion del archivo", example = "archivo principal foda", required = true)
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del archivo", example = "foda", required = true)
    private String nombre;

    @JsonProperty(value = "peso")
    @ApiModelProperty(notes = "Peso del archivo", example = "1024", required = true)
    private Integer peso;

    @JsonProperty(value = "extension")
    @ApiModelProperty(notes = "extension del archivo", example = ".pdf", required = true)
    private String extension;

    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Tipo del archivo", example = "1", required = true)
    private Integer tipo;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "indicedel archivo", example = "1", required = true)
    private Integer indice;

    @JsonProperty(value = "tipoContenido")
    @ApiModelProperty(notes = "Tipo de contenido del archivo", example = ".pdf")
    private String tipoContenido;

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getIndice() {
        return indice;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    @Override
    public String toString() {
        return "ParametrosArchivoVirtual{" + "idArchivo=" + idArchivo + ", idFolio=" + idFolio + ", uid=" + uid + ", descripcion=" + descripcion + ", nombre=" + nombre + ", peso=" + peso + ", extension=" + extension + ", tipo=" + tipo + ", indice=" + indice + ", tipoContenido=" + tipoContenido + '}';
    }

}
