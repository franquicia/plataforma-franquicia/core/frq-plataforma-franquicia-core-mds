/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.relacion.protocolo.checklist.dao.util;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.relacion.protocolo.checklist.dto.RelacionProtocoloChecklistDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 * @param <dto>
 */
public interface GenericDAO<dto> {

    /**
     *
     * @return @throws CustomException
     */
    public List<RelacionProtocoloChecklistDTO> selectRows() throws CustomException;

    /**
     *
     * @param idCeco
     * @param idProtocolo
     * @param modulo
     * @return
     * @throws CustomException
     */
    public List<RelacionProtocoloChecklistDTO> selectRow(Long entityID, Long entityID2) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto insertRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto updateRow(dto entityDTO) throws CustomException;

    /**
     *
     * @param entityDTO
     * @return
     * @throws CustomException
     */
    public dto deleteRow(dto entityDTO) throws CustomException;

}
