/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g011;

import com.gs.baz.frq.dashboard.dao.graficos.g011.DashboardDAOImpl011;
import com.gs.baz.frq.dashboard.dto.graficos.g011.CumplimientoPuntajeNegociosTerritorialDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard011 {

    @Autowired
    private DashboardDAOImpl011 dashboardDAOImpl011;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/puntaje/negocios/nivel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CumplimientoPuntajeNegociosTerritorialDTO> getCumplimientoPuntajeNegociosNivelGeografico(@RequestBody CumplimientoPuntajeNegociosTerritorialDTO cumplimientoPuntajeNegociosTerritorial) throws CustomException {
        return dashboardDAOImpl011.selectRowsCumplimientoPuntajeNegociosNivelGeografico(cumplimientoPuntajeNegociosTerritorial);
    }
}
