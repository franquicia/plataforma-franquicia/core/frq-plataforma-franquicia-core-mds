/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class GraficaRowMapper implements RowMapper<GraficaBase> {

    @Override
    public GraficaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        GraficaBase graficaBase = new GraficaBase();
        graficaBase.setDescripcion(rs.getString("FCNOMBREGRAFICA"));
        graficaBase.setClave(rs.getString("FCCVEGRAFICA"));
        graficaBase.setNotas(rs.getString("FCNOMBRESP"));
        return graficaBase;
    }
}
