/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.negocio.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.PathsResources;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.CategoriaDTO;
import com.gs.baz.model.bucket.client.services.dto.FileGenericDTO;
import com.gs.baz.model.bucket.client.services.dto.FranquiciaDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceArchivo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceCategoria;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFranquicia;
import com.gs.baz.negocio.dao.NegocioDAOImpl;
import com.gs.baz.negocio.dto.NegocioDTO;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/negocio")
public class ServiceNegocio {

    @Autowired
    private NegocioDAOImpl negocioDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceCategoria sbsCategoria;

    @Autowired
    private SubServiceFranquicia sbsFranquicia;

    @Autowired
    private SubServiceArchivo subServiceArchivo;

    final private VersionBI versionBI = new VersionBI();

    final private ObjectMapper objectMapper = new ObjectMapper();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @param headers
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getNegocios(@RequestHeader HttpHeaders headers) throws CustomException {
        List<NegocioDTO> negocios;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            sbsCategoria.init(basePath, httpEntity);
            sbsFranquicia.init(basePath, httpEntity);
            negocios = negocioDAOImpl.selectRows();
            if (negocios != null) {
                List<CategoriaDTO> categorias;
                for (NegocioDTO negocioDTO : negocios) {
                    categorias = sbsCategoria.getCategorias(negocioDTO.getIdNegocio());
                    negocioDTO.setCategorias(categorias);
                    negocioDTO = this.appendFranquicias(negocioDTO);
                    negocioDTO.setIdsFranquicias(null);
                }
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return negocios;
    }

    /**
     *
     * @param idFranquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/franquicia/get/{id_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getNegociosFranquicia(@PathVariable("id_franquicia") Long idFranquicia) throws CustomException {
        return negocioDAOImpl.selectRowsIdFranquicia(idFranquicia);
    }

    /**
     *
     * @param idFranquicia
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/franquicia/get/data/{id_franquicia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getNegociosFranquiciaData(@RequestHeader HttpHeaders headers, @PathVariable("id_franquicia") Long idFranquicia) throws CustomException {
        List<NegocioDTO> negocios = negocioDAOImpl.selectRowsIdFranquicia(idFranquicia);
        if (negocios != null) {
            for (NegocioDTO negocioDTO : negocios) {
                final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
                final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                sbsCategoria.init(basePath, httpEntity);
                if (negocioDTO != null) {
                    List<CategoriaDTO> categorias = sbsCategoria.getCategorias(negocioDTO.getIdNegocio());
                    negocioDTO.setCategorias(categorias);
                }
            }
        }
        return negocios;
    }

    /**
     *
     * @param headers
     * @param id_negocio
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_negocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO getNegocio(@RequestHeader HttpHeaders headers, @PathVariable("id_negocio") Long id_negocio) throws CustomException {
        NegocioDTO negocioDTO;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            negocioDTO = negocioDAOImpl.selectRow(id_negocio);
            sbsCategoria.init(basePath, httpEntity);
            sbsFranquicia.init(basePath, httpEntity);
            if (negocioDTO != null) {
                List<CategoriaDTO> categorias = sbsCategoria.getCategorias(negocioDTO.getIdNegocio());
                negocioDTO.setCategorias(categorias);
                negocioDTO = this.appendFranquicias(negocioDTO);
                negocioDTO.setIdsFranquicias(null);
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return negocioDTO;
    }

    private NegocioDTO appendFranquicias(NegocioDTO negocioDTO) throws CustomException {
        List<Integer> idsFranquicias = negocioDTO.getIdsFranquicias();
        List<FranquiciaDTO> franquicias = new ArrayList<>();
        for (Integer idFranquicia : idsFranquicias) {
            FranquiciaDTO franquiciaDTO = sbsFranquicia.getFranquicia(idFranquicia);
            franquicias.add(franquiciaDTO);
        }
        franquicias.forEach((franquicia) -> {
            franquicia.setEmpleadoCrea(null);
            franquicia.setIdStatus(null);
            franquicia.setFechaCreacion(null);
            franquicia.setFechaModificacion(null);
            franquicia.setIdEmpleadoCrea(null);
            franquicia.setIdEmpleadoModifica(null);
        });
        negocioDTO.setFranquicias(franquicias);
        return negocioDTO;
    }

    /**
     *
     * @param headers
     * @param id_negocio
     * @param pForceDeveloping
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get/resource/img/{id_negocio}", method = RequestMethod.GET)
    public ResponseEntity getLogo(@RequestHeader HttpHeaders headers, @PathVariable("id_negocio") Long id_negocio, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        try {
            NegocioDTO negocioDTO = negocioDAOImpl.selectRowLogo(id_negocio);
            if (negocioDTO != null) {
                final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
                final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
                subServiceArchivo.init(basePath, httpEntity);
                boolean forceDeveloping = pForceDeveloping != null;
                String fullPath = Base64.getEncoder().encodeToString(negocioDTO.getPathLogo().getBytes());
                FileGenericDTO fileGenericDTO = subServiceArchivo.getFileGeneric(fullPath, forceDeveloping);
                return ResponseEntity.status(HttpStatus.OK).body(fileGenericDTO);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (CustomException ex) {
            logger.error(ex);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     *
     * @param headers
     * @param negocioDTO
     * @param pForceDeveloping
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/cud", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO cudNegocio(@RequestHeader HttpHeaders headers, @RequestBody NegocioDTO negocioDTO, @RequestParam(required = false, name = "force_developing") String pForceDeveloping) throws CustomException {
        if (negocioDTO.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (negocioDTO.getFile() != null && !negocioDTO.getFile().toString().equals("{}")) {
            FileGenericDTO fileGenericDTO = objectMapper.convertValue(negocioDTO.getFile(), FileGenericDTO.class);
            boolean forceDeveloping = pForceDeveloping != null;
            fileGenericDTO = this.guardaArchivo(fileGenericDTO, headers, forceDeveloping);
            negocioDTO.setPathLogo(fileGenericDTO.getPath());
        }
        return negocioDAOImpl.cudRow(negocioDTO);
    }

    /**
     *
     * @param negocio
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegocioDTO deleteEjecuta(@RequestBody NegocioDTO negocio) throws CustomException {
        if (negocio.getIdNegocio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_negocio"));
        }
        return negocioDAOImpl.deleteRow(negocio);
    }

    private FileGenericDTO guardaArchivo(FileGenericDTO fileGenericDTO, HttpHeaders headers, Boolean forceDeveloping) throws CustomException {
        String ruta = PathsResources.NEGOCIOS.getPath();
        long time = new Date().getTime();
        String nomArchivo = time + "__" + fileGenericDTO.getNameFile();
        fileGenericDTO.setNameFile(nomArchivo);
        fileGenericDTO.setPath(ruta);
        String rutaBD = ruta + nomArchivo;
        final HttpEntity<Object> httpEntity = new HttpEntity<>(fileGenericDTO, headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        subServiceArchivo.init(basePath, httpEntity);
        fileGenericDTO = subServiceArchivo.postFileGeneric(forceDeveloping);
        if (!fileGenericDTO.getWrited()) {
            throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("Error to insert " + fileGenericDTO.getPath() + " on Files"));
        }
        fileGenericDTO.setPath(rutaBD); //Se asigna la ruta de BD para que la guarde el DAO
        return fileGenericDTO;
    }

    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getNegocioByNombre(@RequestHeader HttpHeaders headers, @QueryParam("value") String value) throws CustomException {
        return negocioDAOImpl.selectRowsByName(value);
    }
}
