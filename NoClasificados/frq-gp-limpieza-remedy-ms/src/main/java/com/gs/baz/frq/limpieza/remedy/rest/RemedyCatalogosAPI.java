package com.gs.baz.frq.limpieza.remedy.rest;

import com.gs.baz.frq.limpieza.remedy.bi.IncidenciasBI;
import com.gs.baz.frq.limpieza.remedy.bi.ProveedoresBI;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import com.gs.baz.frq.limpieza.remedy.dto.TipoIncidenciasDTO;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kramireza
 */
@Api(tags = "gestion-incidentes-catalogos",
        value = "gestion-incidentes-catalogos",
        description = "API de gestión de catálogos que se utilizan para la creación de una indidencia")
@RestController
@RequestMapping("/api/gestion-incidentes/catalogos/v1")
public class RemedyCatalogosAPI {

    @Autowired
    private IncidenciasBI incidenciasBI;

    @Autowired
    private ProveedoresBI proveedoresBI;

    private final Logger LOGGER = LogManager.getLogger();

    //@RequestMapping(value = "/getProveedoresLimpieza", method = RequestMethod.GET)
    @ApiOperation(value = "Proveedores de Limpieza", notes = "Regresa la lista con información de los Proveedores de Limpieza que pueden atender los servicios de las fallas o incidencias reportadas.", nickname = "getProveedoresLimpieza")
    @RequestMapping(value = "/limpieza/proveedores", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<ProveedoresDTO> getProveedoresLimpieza() {
        List<ProveedoresDTO> listaResponse = new ArrayList<>();
        try {
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfoLimpieza();
            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux : lista) {
                    if (aux.getStatus().contains("Activo")) {
                        listaResponse.add(aux);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            listaResponse = null;
        }
        return listaResponse;
    }

    //@RequestMapping(value = "/getTipoIncidencias", method = RequestMethod.GET)
    @ApiOperation(value = "Tipos de Incidencias de Mantenimiento", notes = "Regresa los tipos de Incidentes de Mantenimiento.", nickname = "getTipoIncidencias")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/mantenimiento/tipos-incidencias", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TipoIncidenciasDTO getTipoIncidencias() throws DataNotFoundException {

        TipoIncidenciasDTO lista = new TipoIncidenciasDTO();
        try {

            lista.setTipoIncidencias(incidenciasBI.obtieneInfo());
            if (lista == null) {
                throw new DataNotFoundException();
            }
            return lista;
        } catch (Exception e) {
            throw new DataNotFoundException();
        }

    }

    //@RequestMapping(value = "/getProveedores", method = RequestMethod.GET)
    @ApiOperation(value = "Detalle de los Proveedores para atender incidentes", notes = "Regresa la información de los proveedores para atender incidentes de mantenimiento.", nickname = "getProveedores")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/mantenimiento/proveedores", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<ProveedoresDTO> getProveedores() {

        List<ProveedoresDTO> listaSalida = null;
        try {

            listaSalida = new ArrayList<>();
            List<ProveedoresDTO> lista = proveedoresBI.obtieneInfo();

            if (!lista.isEmpty()) {
                for (ProveedoresDTO aux : lista) {
                    if (aux.getStatus().contains("Activo")) {

                        listaSalida.add(aux);

                    }
                }
            }

        } catch (Exception e) {
            listaSalida = null;

        }

        return listaSalida;
    }

}
