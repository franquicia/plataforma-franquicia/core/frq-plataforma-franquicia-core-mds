/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documentos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author b191312
 */
@ApiModel(description = "Datos del documento por ceco", value = "DocumentoCeco")
public class DocumentosCeco {

    @JsonProperty(value = "documentos")
    @ApiModelProperty(notes = "Lista de documentos por centro de costos", example = "objetc")
    private java.util.List<DocumentoCeco> documentos;

    public DocumentosCeco(List<DocumentoCeco> documentos) {

        this.documentos = documentos;

    }

    public List<DocumentoCeco> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoCeco> documentos) {
        this.documentos = documentos;
    }

}
