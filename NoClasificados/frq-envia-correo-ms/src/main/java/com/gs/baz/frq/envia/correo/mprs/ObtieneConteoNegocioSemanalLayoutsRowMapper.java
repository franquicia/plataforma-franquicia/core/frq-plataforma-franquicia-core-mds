/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.envia.correo.mprs;

import com.gs.baz.frq.envia.correo.dto.ObtieneConteoNegocioSemanalLayoutsDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ObtieneConteoNegocioSemanalLayoutsRowMapper implements RowMapper<ObtieneConteoNegocioSemanalLayoutsDTO> {

    private ObtieneConteoNegocioSemanalLayoutsDTO reporteCorreo;

    @Override
    public ObtieneConteoNegocioSemanalLayoutsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        reporteCorreo = new ObtieneConteoNegocioSemanalLayoutsDTO();
        reporteCorreo.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        reporteCorreo.setNombreProyecto(rs.getString("NOMPROY"));
        reporteCorreo.setIdFolio(rs.getInt("FIID_FOLIO"));
        reporteCorreo.setIdCeco(rs.getInt("FCID_CECO"));
        reporteCorreo.setIdFormato(rs.getInt("FIID_FORMATO"));
        reporteCorreo.setIdNegocio(rs.getString("FIID_NEGOCIO"));
        reporteCorreo.setNombreFormato(rs.getString("NOMFORMATO"));
        reporteCorreo.setNombreNegocio(rs.getString("NOMBNEGO"));
        reporteCorreo.setNombreCeco(rs.getString("NOMCECO"));
        reporteCorreo.setConteoProyecto(rs.getInt("CONTEO_PROY"));
        reporteCorreo.setFecha(rs.getString("FCPERIODO"));

        return reporteCorreo;
    }
}
