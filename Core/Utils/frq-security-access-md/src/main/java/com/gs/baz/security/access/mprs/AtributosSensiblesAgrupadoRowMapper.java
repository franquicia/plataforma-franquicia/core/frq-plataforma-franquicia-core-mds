/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.mprs;

import com.gs.baz.security.access.dto.AtributoSensibleAgrupado;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kramireza
 */
public class AtributosSensiblesAgrupadoRowMapper implements RowMapper<AtributoSensibleAgrupado> {

    private AtributoSensibleAgrupado atributoSensibleAgrupado;

    @Override
    public AtributoSensibleAgrupado mapRow(ResultSet rs, int rowNum) throws SQLException {
        atributoSensibleAgrupado = new AtributoSensibleAgrupado();
        atributoSensibleAgrupado.setIdAtributo(rs.getInt("FIIDCAMPO"));
        atributoSensibleAgrupado.setNombre(rs.getString("FCNOMBRECAMPO"));
        atributoSensibleAgrupado.setTipo(AtributoSensibleAgrupado.TipoAtributo.valueOf(rs.getString("FCORIGENDATO")));
        atributoSensibleAgrupado.setApiOrigen(rs.getString("FCSCOPEDATO"));
        atributoSensibleAgrupado.setNombreRecurso(rs.getString("FCNOMBRRECURSO"));
        return atributoSensibleAgrupado;
    }
}
