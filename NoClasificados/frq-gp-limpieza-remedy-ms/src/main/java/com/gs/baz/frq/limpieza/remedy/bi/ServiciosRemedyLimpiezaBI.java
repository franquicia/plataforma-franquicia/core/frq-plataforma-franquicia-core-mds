package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dto.CuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.FolioRemedyDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteCuadrillaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenteLimpiezaDTO;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ActualizarIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Adjunto;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Aprobacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfAdjunto;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfInformacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfTracking;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Authentication;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarAdjunto;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarBitacoraIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarIncidentes;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarListaAdjuntos;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.CrearIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Filtro;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Incidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Informacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Registro;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsCAdjuntos;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsCIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsRIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsTracking;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsUIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._Accion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._Calificacion;
import com.gs.baz.frq.limpieza.remedy.util.UtilDate;
import com.gs.baz.frq.limpieza.remedy.util.UtilString;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import javax.activation.DataHandler;
import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiciosRemedyLimpiezaBI {

    @Autowired
    private ParametroBI parametroBi;

    @Autowired
    private PeticionesRemedyBI peticionesRemedyBI;

    @Autowired
    private ProveedoresBI proveedoresBI;

    @Autowired
    private IncidenteCuadrillaLimpiezaBI incidenteCuadrillaLimpiezaBI;

    @Autowired
    private CuadrillaBI cuadrillaBI;

    @Autowired
    private LogBI logbi;

    @Autowired
    ServiciosRemedyLimpiezaDosBI serviciosRemedyLimpiezaDosBI;

    private static final Logger LOGGER = LogManager.getLogger();

    private final Base64.Decoder decoder = Base64.getDecoder();
    private final Base64.Encoder encoder = Base64.getEncoder();
    private static final Integer DOS = 2, TRES = 3, CUATRO = 4, CINCO = 5, SEIS = 6, SIETE = 7, OCHO = 8;

    public int levantaFolio(FolioRemedyDTO bean) {
        int idFolio = 0;

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("levantaFolio()" + " LIMPIEZA");
        }

        try {
            CrearIncidente crearIncitenteRequest = new CrearIncidente();
            Registro regRequest = new Registro();
            regRequest.setCC(Integer.parseInt(bean.getCeco()));
            regRequest.setComentarios(bean.getComentario());
            regRequest.setCorreo(bean.getCorreo());
            regRequest.setFalla(bean.getUbicacion());
            regRequest.setIncidencia(bean.getServicio());
            regRequest.setTipoFalla(bean.getIncidencia());
            regRequest.setNoEmpleado(Integer.parseInt(bean.getNumeroEmpleado()));
            regRequest.setNombre(bean.getNombre());
            regRequest.setNoSucursal(bean.getNumeroSucursal());
            regRequest.setPuesto(bean.getPuesto());
            regRequest.setSucursal(bean.getNombreSucursal());
            regRequest.setTelSucursal(bean.getTelefonoSucursal());
            regRequest.setTelefono(bean.getTelefonoUsuarioAlterno());
            regRequest.setPuestoAlterno(bean.getPuestoAlterno());
            regRequest.setUsrAlterno(bean.getNombreUsuarioAlterno());

            if (bean.getNombreAdjunto() != null) {
                regRequest.setAdjunto1Nombre(bean.getNombreAdjunto());
            } else {
                regRequest.setAdjunto1Nombre("");
            }

            if (bean.getAdjunto() != null) {
                byte[] aux2 = decoder.decode(bean.getAdjunto());

                ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                DataHandler data = new DataHandler(rawData);

                regRequest.setAdjunto1Base(data);
            } else {
                regRequest.setAdjunto1Base(null);
            }

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            crearIncitenteRequest.setAuthentication(autenticatiosRemedy);
            crearIncitenteRequest.setIncidente(regRequest);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.CrearIncidenteResponse response = new RemedyStub.CrearIncidenteResponse();
            response = consulta.crearIncidente(crearIncitenteRequest);
            RsRIncidente crearIncidenteResult = response.getCrearIncidenteResult();

            if (crearIncidenteResult.getExito()) {
                int folio = crearIncidenteResult.getNoIncidente();
                if (folio != 0) {
                    idFolio = folio;
                }
            } else {
                LOGGER.log(Level.INFO, "Info CreaIncidente: Mensaje: ", crearIncidenteResult.getMensaje());
            }
        } catch (IOException e) {
            LOGGER.log(Level.INFO, "Ocurrio algo al crear el incidente ", e);
            return 0;
        }
        return idFolio;
    }

    public ArrayList<IncidenteLimpiezaDTO> ConsultaFoliosSucursal(int numSucursal, String opcEst) {
        ArrayOfInformacion arrIncidentes = null;

        ArrayList<IncidenteLimpiezaDTO> arrayTickets = null;

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaFoliosSucursal()" + " LIMPIEZA");
        }

        try {
            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            LOGGER.log(Level.INFO, "Today is ", today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setNoSucursal(numSucursal);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {

                arrayTickets = new ArrayList<>();
                arrIncidentes = incidentes.getIncidentes();

                IncidenteLimpiezaDTO incidenteLimpieza = null;
                if (arrIncidentes.getInformacion() != null) {
                    for (int i = 0; i < arrIncidentes.getInformacion().length; i++) {

                        Informacion incidenteRemedy = arrIncidentes.getInformacion()[i];
                        incidenteLimpieza = new IncidenteLimpiezaDTO();

                        incidenteLimpieza.setIdIncidencia(incidenteRemedy.getIdIncidencia());
                        incidenteLimpieza.setNumeroSucursal(incidenteRemedy.getNoSucursal());
                        incidenteLimpieza.setNombreSucursal(UtilString.validaStrNull(incidenteRemedy.getSucursal()));
                        incidenteLimpieza.setCentroCostos(UtilString.validaStrNull(incidenteRemedy.getCC()));
                        incidenteLimpieza
                                .setTelefonoSucursal(UtilString.validaStrNull(incidenteRemedy.getTelefonoSucursal()));
                        incidenteLimpieza.setIncidencia(UtilString.validaStrNull(incidenteRemedy.getIncidencia()));
                        incidenteLimpieza.setFalla(UtilString.validaStrNull(incidenteRemedy.getFalla()));
                        incidenteLimpieza.setTipoFalla(UtilString.validaStrNull(incidenteRemedy.getTipoFalla()));
                        incidenteLimpieza.setNotas(UtilString.validaStrNull(incidenteRemedy.getNotas()));
                        incidenteLimpieza
                                .setIncidenteAnterior(UtilString.validaStrNull(incidenteRemedy.getTicketAnterior()));
                        incidenteLimpieza.setIdCliente(UtilString.validaStrNull(incidenteRemedy.getIdCorpCliente()));
                        incidenteLimpieza
                                .setNombreCliente(UtilString.validaStrNull(incidenteRemedy.getNombreCliente()));
                        incidenteLimpieza.setTelefonoCliente(UtilString.validaStrNull(incidenteRemedy.getTelCliente()));
                        incidenteLimpieza.setPuestoCliente(UtilString.validaStrNull(incidenteRemedy.getPuesto()));
                        incidenteLimpieza.setAutorizacionCliente(
                                UtilString.validaStrNull(incidenteRemedy.getAutorizacionCliente()));

                        int cal = 0;
                        _Calificacion auxEval = incidenteRemedy.getEvalCliente();
                        if (auxEval != null) {

                            if (auxEval.getValue().contains("NA")) {
                                cal = 1;
                            }
                            if (auxEval.getValue().contains("E0a50")) {
                                cal = DOS;
                            }
                            if (auxEval.getValue().contains("E51a69")) {
                                cal = TRES;
                            }
                            if (auxEval.getValue().contains("E70a89")) {
                                cal = CUATRO;
                            }
                            if (auxEval.getValue().contains("E90a100")) {
                                cal = CINCO;
                            }
                        }

                        incidenteLimpieza.setEvaluacionCliente(cal + "");

                        incidenteLimpieza.setUsuarioAlterno(UtilString.validaStrNull(incidenteRemedy.getUsrAlterno()));
                        incidenteLimpieza
                                .setPuestoAlterno(UtilString.validaStrNull(incidenteRemedy.getPuestoAlterno()));
                        incidenteLimpieza
                                .setTelefonoContactoAlterno(UtilString.validaStrNull(incidenteRemedy.getTelCliente()));

                        incidenteLimpieza
                                .setIdSupervisor(UtilString.validaStrNull(incidenteRemedy.getIdCorpSupervisor()));
                        incidenteLimpieza
                                .setIdCoordinador(UtilString.validaStrNull(incidenteRemedy.getIdCorpCoordinador()));
                        incidenteLimpieza.setIdProveedor(UtilString.validaStrNull(incidenteRemedy.getIdProveedor()));
                        incidenteLimpieza.setNumeroIncidenteProveedor(
                                UtilString.validaStrNull(incidenteRemedy.getNoTicketProveedor()));
                        incidenteLimpieza.setAutorizacionProveedor(
                                UtilString.validaStrNull(incidenteRemedy.getAutorizacionProveedor()));

                        try {

                            List<ProveedoresDTO> proveedorLimpieza = null;

                            String idProveedor = incidenteRemedy.getIdProveedor();
                            if (idProveedor != null && !idProveedor.equals("0")) {
                                proveedorLimpieza = proveedoresBI.obtieneDatos(Integer.parseInt(idProveedor));

                                if (proveedorLimpieza != null && proveedorLimpieza.size() > 0) {
                                    incidenteLimpieza.setRazonSocial(proveedorLimpieza.get(0).getRazonSocial());
                                    incidenteLimpieza.setMenu(proveedorLimpieza.get(0).getMenu());
                                    incidenteLimpieza
                                            .setNombreCortoProveedor(proveedorLimpieza.get(0).getNombreCorto());

                                }

                            }

                        } catch (Exception e) {
                            LOGGER.info(e);

                        }

                        incidenteLimpieza.setProveedor(UtilString.validaStrNull(incidenteRemedy.getProveedor()));

                        incidenteLimpieza.setTipoCierreProvedor(
                                UtilString.validaStrNull(incidenteRemedy.getTipoCierreProveedor()));
                        incidenteLimpieza
                                .setAutorizacionPipa(UtilString.validaStrNull(incidenteRemedy.getAutorizacionPipa()));
                        incidenteLimpieza.setCostoPipa(incidenteRemedy.getCostoPipa().toString());
                        incidenteLimpieza.setNotificacion(UtilString.validaStrNull(incidenteRemedy.getNotificacion()));
                        incidenteLimpieza.setMontoEstimado(incidenteRemedy.getMontoEstimado().toString());

                        incidenteLimpieza.setPrioridad(incidenteRemedy.getPrioridad() != null
                                ? UtilString.validaStrNull(incidenteRemedy.getPrioridad().getValue())
                                : null);

                        incidenteLimpieza.setTipoAtencion(incidenteRemedy.getTipoAtencion() != null
                                ? UtilString.validaStrNull(incidenteRemedy.getTipoAtencion().getValue())
                                : null);

                        incidenteLimpieza.setOrigen(incidenteRemedy.getOrigen() != null
                                ? UtilString.validaStrNull(incidenteRemedy.getOrigen().getValue())
                                : null);

                        incidenteLimpieza.setEstado(UtilString.validaStrNull(incidenteRemedy.getEstado()));
                        incidenteLimpieza.setMotivoEstado(UtilString.validaStrNull(incidenteRemedy.getMotivoEstado()));

                        incidenteLimpieza.setFechaCreacion(UtilDate.validaFecha(incidenteRemedy.getFechaCreacion()));

                        incidenteLimpieza
                                .setFechaModificacion(UtilDate.validaFecha(incidenteRemedy.getFechaModificacion()));

                        incidenteLimpieza
                                .setFechaInicioAtencion(UtilDate.validaFecha(incidenteRemedy.getFechaInicioAtencion()));

                        incidenteLimpieza
                                .setFechaUltimoIncidente(UtilDate.validaFecha(incidenteRemedy.getFechaUltimoTicket()));

                        incidenteLimpieza.setFechaEstimada(UtilDate.validaFecha(incidenteRemedy.getFechaEstimada()));

                        incidenteLimpieza.setFechaCierreAdministrativo(
                                UtilDate.validaFecha(incidenteRemedy.getFechaCierreAdministrativo()));

                        int idIncidente = incidenteRemedy.getIdIncidencia();

                        List<IncidenteCuadrillaDTO> lista2 = incidenteCuadrillaLimpiezaBI.obtieneDatos(idIncidente);
                        int idProvedor = 0;
                        int idCuadrilla = 0;
                        int idZona = 0;
                        for (IncidenteCuadrillaDTO aux2 : lista2) {
                            if (aux2.getStatus() != 0) {
                                idProvedor = aux2.getIdProveedor();
                                idCuadrilla = aux2.getIdCuadrilla();
                                idZona = aux2.getZona();
                            }

                        }
                        if (idProvedor != 0) {
                            List<CuadrillaDTO> lista3 = cuadrillaBI.obtieneDatos(idProvedor);
                            for (CuadrillaDTO aux2 : lista3) {
                                if (aux2.getZona() == idZona) {
                                    if (aux2.getIdCuadrilla() == idCuadrilla) {
                                        incidenteLimpieza.setIdProveedor(aux2.getIdProveedor() + "");
                                        incidenteLimpieza.setIdCuadrilla(aux2.getIdCuadrilla());
                                        incidenteLimpieza.setLider(aux2.getLiderCuad());
                                        incidenteLimpieza.setCorreo(aux2.getCorreo());
                                        incidenteLimpieza.setSegundoMando(aux2.getSegundo());
                                        incidenteLimpieza.setIdZona(aux2.getZona());

                                    }
                                }
                            }
                        } else {
                            String auxiliar = null;
                            incidenteLimpieza.setIdProveedor("");
                            incidenteLimpieza.setIdCuadrilla(0);
                            incidenteLimpieza.setLider("");
                            incidenteLimpieza.setCorreo("");
                            incidenteLimpieza.setSegundoMando("");
                            incidenteLimpieza.setIdZona(0);
                        }

                        Aprobacion aprobacionObjeto = null;

                        // En caso de ser un incidente cancelado.
                        if (incidenteRemedy.getEstado().equalsIgnoreCase("atendido")
                                && incidenteRemedy.getMotivoEstado().equalsIgnoreCase("cancelado no aplica")) {

                            aprobacionObjeto = serviciosRemedyLimpiezaDosBI.consultaAprobacion(incidenteRemedy.getIdIncidencia(), 2);

                        } else if (incidenteRemedy.getEstado().equalsIgnoreCase("atendido")
                                && incidenteRemedy.getMotivoEstado().equalsIgnoreCase("Cancelado correctivo menor")) {
                            aprobacionObjeto = serviciosRemedyLimpiezaDosBI.consultaAprobacion(incidenteRemedy.getIdIncidencia(), 4);
                        }

                        if (aprobacionObjeto != null) {
                            incidenteLimpieza.setResolucion(aprobacionObjeto.getJustificacion());
                        }

                        incidenteLimpieza.setClienteAvisado(incidenteRemedy.getClienteAvisado());
                        arrayTickets.add(incidenteLimpieza);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Ocurrio algo al adjuntar archivo al incidente", e);
            return arrayTickets;
        }
        return arrayTickets;
    }

    public String ConsultaAdjunto(int idFolio) {
        String res = null;

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaAdjunto()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);

            ConsultarAdjunto consultarAdjuntoRequest = new ConsultarAdjunto();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            consultarAdjuntoRequest.setAuthentication(autenticatiosRemedy);
            consultarAdjuntoRequest.setNoIncidente(idFolio);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarAdjuntoResponse response = new RemedyStub.ConsultarAdjuntoResponse();

            response = consulta.consultarAdjunto(consultarAdjuntoRequest);

            RsCAdjuntos adjuntoR = response.getConsultarAdjuntoResult();
            logbi.insertaerror(0, adjuntoR.getMensaje(), "ConsultaAdjunto()");
            if (adjuntoR.getExito()) {
                ArrayOfAdjunto arrAdjunto = adjuntoR.getAdjuntos();
                if (arrAdjunto != null) {
                    Adjunto[] aux = arrAdjunto.getAdjunto();
                    if (aux != null) {
                        for (Adjunto x : aux) {
                            DataHandler dataAUX = x.getA1_Base();

                            InputStream in = dataAUX.getInputStream();
                            byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
                            String encoded = new String(encoder.encode(byteArray), "UTF-8");
                            res = encoded;
                        }
                    } else {
                        res = null;
                    }
                }
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al adjuntar archivo al incidente", e);

            return res;
        }

        return res;
    }

    public Adjunto[] consultaAdjuntos(int incidente) {

        RsCAdjuntos adjuntos = null;

        Adjunto[] arrayAdjuntos = null;

        Calendar today = Calendar.getInstance();
        today.set(2016, 1, 1);

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaAdjuntos()" + " LIMPIEZA");
        }

        try {
            ConsultarListaAdjuntos consultaradjuntosRequest = new ConsultarListaAdjuntos();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            consultaradjuntosRequest.setAuthentication(autenticatiosRemedy);
            consultaradjuntosRequest.setNoIncidente(incidente);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarListaAdjuntosResponse response = new RemedyStub.ConsultarListaAdjuntosResponse();

            response = consulta.consultarListaAdjuntos(consultaradjuntosRequest);

            adjuntos = response.getConsultarListaAdjuntosResult();

            if (adjuntos != null) {

                ArrayOfAdjunto adjuntosLista = adjuntos.getAdjuntos();

                if (adjuntosLista != null && adjuntosLista.getAdjunto() != null
                        && adjuntosLista.getAdjunto().length > 0) {

                    arrayAdjuntos = adjuntosLista.getAdjunto();

                } else {
                    arrayAdjuntos = null;
                }

            }

        } catch (Exception e) {
            return null;
        }

        return arrayAdjuntos;

    }

    public ArrayOfTracking ConsultaBitacora(int idFolio) {

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaBitacora()" + " LIMPIEZA");
        }

        ArrayOfTracking res = null;
        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);

            ConsultarBitacoraIncidente consultarBitacoraRequest = new ConsultarBitacoraIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            consultarBitacoraRequest.setAuthentication(autenticatiosRemedy);
            consultarBitacoraRequest.setNoIncidente(idFolio);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarBitacoraIncidenteResponse response = new RemedyStub.ConsultarBitacoraIncidenteResponse();

            response = consulta.consultarBitacoraIncidente(consultarBitacoraRequest);

            RsTracking traking = response.getConsultarBitacoraIncidenteResult();

            logbi.insertaerror(0, traking.getMensaje(), "ConsultaBitacora()");
            if (traking.getExito()) {
                ArrayOfTracking arrTraking = traking.getBitacora();
                res = arrTraking;
            }

        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Ocurrio algo al adjuntar archivo al incidente ", e);
            return null;
        }

        return res;
    }

    @SuppressWarnings("static-access")
    public boolean aceptaCliente(int idIncidente, int calificacion, String justificacion) {
        boolean result = false;

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("aceptaCliente()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AceptaCierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();

            incide.setAdjunto1Base(null);
            incide.setAdjunto1Nombre("");

            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setAcepta(true);
            incide.setSupervisor("*Sin informacion");

            _Calificacion calAux = null;

            if (calificacion == 1) {
                incide.setCalificacion(calAux.NA);
            }
            if (calificacion == DOS) {
                incide.setCalificacion(calAux.E0a50);
            }
            if (calificacion == TRES) {
                incide.setCalificacion(calAux.E51a69);
            }
            if (calificacion == CUATRO) {
                incide.setCalificacion(calAux.E70a89);
            }
            if (calificacion == CINCO) {
                incide.setCalificacion(calAux.E90a100);
            }

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AceptaCliente()");

            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo acptar el trabajo cliente ", e);

        }

        return result;
    }

    @SuppressWarnings("static-access")
    public boolean rechazaCliente(int idIncidente, String justificacion, String nomArchivo, String adjuntoBase,
            String solicitante, String aprobador) {

        if (serviciosRemedyLimpiezaDosBI.getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("rechazaCliente()" + " LIMPIEZA");
        }

        boolean result = false;
        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AceptaCierre);
            actIncitenteRequest.setAuthentication(autenticatiosRemedy);

            Incidente incide = new Incidente();
            if (adjuntoBase != null) {
                if (adjuntoBase.trim().toLowerCase().equals("null") || adjuntoBase.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Base(null);
                } else {
                    byte[] aux2 = decoder.decode(adjuntoBase);

                    ByteArrayDataSource rawData = new ByteArrayDataSource(aux2);
                    DataHandler data = new DataHandler(rawData);
                    incide.setAdjunto1Base(data);
                }

            }
            if (nomArchivo != null) {
                if (nomArchivo.trim().toLowerCase().equals("null") || nomArchivo.trim().toLowerCase().equals("")) {
                    incide.setAdjunto1Nombre("");
                } else {
                    incide.setAdjunto1Nombre(nomArchivo);

                }
            } else {
                incide.setAdjunto1Nombre("");
            }

            incide.setAdjunto2Base(null);
            incide.setAdjunto2Nombre("");

            incide.setAcepta(false);
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(justificacion);
            incide.setSupervisor("*Sin informacion");

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();
            response = consulta.actualizarIncidente(actIncitenteRequest);
            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "RechazaCliente()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Ocurrio algo al crear el incidente ", e);

        }

        return result;
    }

}
