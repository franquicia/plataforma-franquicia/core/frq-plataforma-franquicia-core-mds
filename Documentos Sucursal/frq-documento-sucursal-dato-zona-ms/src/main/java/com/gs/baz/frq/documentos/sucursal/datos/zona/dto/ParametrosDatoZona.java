/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosDatoZona {

    @JsonProperty(value = "idCatalogoZona")
    private transient Integer idCatalogoZona;

    @JsonProperty(value = "idDatoZona")
    private Integer idDatoZona;

    @JsonProperty(value = "observaciones", required = true)
    @ApiModelProperty(notes = "Descripcion de la observacion", example = "Patio Bancario", required = true)
    private String observaciones;

    @JsonProperty(value = "idFolio", required = true)
    @ApiModelProperty(notes = "Identificador de folio de Layout", example = "1", required = true)
    private Integer idFolio;

    @JsonProperty(value = "valor", required = true)
    @ApiModelProperty(notes = "Identificador de metraje de la zona", example = "10", required = true)
    private String valor;

    @JsonProperty(value = "idVersion", required = true)
    @ApiModelProperty(notes = "Identificador de la version de zonas", example = "1", required = true)
    private Integer idVersion;

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public Integer getIdCatalogoZona() {
        return idCatalogoZona;
    }

    public void setIdCatalogoZona(Integer idCatalogoZona) {
        this.idCatalogoZona = idCatalogoZona;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setDescripcion(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getIdDatoZona() {
        return idDatoZona;
    }

    public void setIdDatoZona(Integer idDatoZona) {
        this.idDatoZona = idDatoZona;
    }

}
