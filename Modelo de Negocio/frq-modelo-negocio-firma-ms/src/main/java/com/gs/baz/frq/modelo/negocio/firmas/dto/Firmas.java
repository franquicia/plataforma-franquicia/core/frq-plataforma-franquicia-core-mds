/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de firmas del documento", value = "firmas")
public class Firmas {

    @JsonProperty(value = "firmas")
    @ApiModelProperty(notes = "firmas")
    private List<Firma> firmas;

    public Firmas(List<Firma> firmas) {
        this.firmas = firmas;
    }

    public List<Firma> getFirmas() {
        return firmas;
    }

    public void setFirmas(List<Firma> firmas) {
        this.firmas = firmas;
    }

    @Override
    public String toString() {
        return "Firmas{" + "firmas=" + firmas + '}';
    }

}
