/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de archivos", value = "ListaArchivos")
public class ArchivosEliminar {

    @JsonProperty(value = "listaArchivos")
    @ApiModelProperty(notes = "listaArchivos")
    private List<ArchivoEliminar> listaArchivos;

    public ArchivosEliminar(List<ArchivoEliminar> listaArchivos) {
        this.listaArchivos = listaArchivos;
    }

    public List<ArchivoEliminar> getListaArchivos() {
        return listaArchivos;
    }

    public void setListaArchivos(List<ArchivoEliminar> listaArchivos) {
        this.listaArchivos = listaArchivos;
    }

    @Override
    public String toString() {
        return "ArchivosEliminar{" + "listaArchivos=" + listaArchivos + '}';
    }

}
