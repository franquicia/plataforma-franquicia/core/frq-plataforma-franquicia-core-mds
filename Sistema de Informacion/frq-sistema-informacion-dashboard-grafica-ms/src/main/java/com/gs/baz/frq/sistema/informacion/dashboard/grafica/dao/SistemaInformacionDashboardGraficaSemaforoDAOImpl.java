/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.AltaGraficaSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaSemaforoBase;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.ParametrosGraficaSemaforo;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs.GraficaSemaforoRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs.GraficaSemaforosRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionDashboardGraficaSemaforoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcDelete;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMDASHGRASE";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSDASHGRASEM");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELDASHGRASEM");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETDASHGRASEM");
        jdbcSelect.returningResultSet("PA_CDATOS", new GraficaSemaforoRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETDASHGRASEM");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new GraficaSemaforosRowMapper());
    }

    public GraficaSemaforoBase selectRow(Integer idGrafica, String regla) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRAFICA", idGrafica);
        mapSqlParameterSource.addValue("PA_FCNOMBREREGLA", regla);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<GraficaSemaforoBase> data = (List<GraficaSemaforoBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<GraficaSemaforo> selectAllRows(Integer idGrafica) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRAFICA", idGrafica);
        mapSqlParameterSource.addValue("PA_FCNOMBREREGLA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<GraficaSemaforo>) out.get("PA_CDATOS");
    }

    public AltaGraficaSemaforo insertRow(ParametrosGraficaSemaforo entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRAFICA", entityDTO.getIdGrafica());
            mapSqlParameterSource.addValue("PA_FIIDSEMAFORO", entityDTO.getIdSemaforo());
            mapSqlParameterSource.addValue("PA_FCNOMBREREGLA", entityDTO.getRegla());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                return new AltaGraficaSemaforo(entityDTO.getIdGrafica());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer idGrafica, Integer idSemaforo, String regla) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRAFICA", idGrafica);
            mapSqlParameterSource.addValue("PA_FIIDSEMAFORO", idSemaforo);
            mapSqlParameterSource.addValue("PA_FCNOMBREREGLA", regla);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
