package com.gs.baz.frq.accesos.sistema.perfiles.mprs;

import com.gs.baz.frq.accesos.sistema.perfiles.dto.Perfil;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PerfilesRowMapper implements RowMapper<Perfil> {

    @Override
    public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
        Perfil perfil = new Perfil();
        perfil.setIdPerfil(rs.getInt("FIID_PERFIL"));
        perfil.setDescripcion(rs.getString("FCDESCRIPCION"));
        return perfil;
    }
}
