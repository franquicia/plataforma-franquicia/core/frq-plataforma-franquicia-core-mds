/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.Comentario;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ComentarioRowMapper implements RowMapper<Comentario> {

    private Comentario comentario;

    @Override
    public Comentario mapRow(ResultSet rs, int rowNum) throws SQLException {
        comentario = new Comentario();
        comentario.setIdComentario(rs.getInt("FIIDCOMENTDOCTO"));
        comentario.setCalificacion(((BigDecimal) rs.getObject("FICALIFICACION")).intValue());
        comentario.setComentario(rs.getString("FCCOMENTARIO"));
        comentario.setFechaComentario(rs.getString("FDFECHAULTCOMENT"));
        comentario.setFechaUltimaVista(rs.getString("FDFECHAVISUALIZA"));
        comentario.setVisualizaciones(rs.getInt("FINUMVISUALIZACION"));
        return comentario;
    }
}
