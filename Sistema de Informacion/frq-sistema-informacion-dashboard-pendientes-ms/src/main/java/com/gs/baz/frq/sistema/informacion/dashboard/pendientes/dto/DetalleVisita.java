/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Detalle de la Visita", value = "VisitaDetalle")
public class DetalleVisita {

    @JsonProperty(value = "centroCostosBase")
    @ApiModelProperty(notes = "Número del ceco base", example = "", position = -1)
    private String centroCostosBase;

    @JsonProperty(value = "centroCostos")
    @ApiModelProperty(notes = "Número del ceco", example = "", position = -2)
    private String centroCostos;
    
    @JsonProperty(value = "nombreCeco")
    @ApiModelProperty(notes = "Nombre del ceco", example = "", position = -3)
    private String nombreCeco;
    
    @JsonProperty(value = "fechaVisita")
    @ApiModelProperty(notes = "Fecha de la visita", example = "05/11/2021", position = -4)
    private String fechaVisita;
    
    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Número de empleado", example = "", position = -5)
    private Integer numeroEmpleado;
    
    @JsonProperty(value = "nombreEmpleado")
    @ApiModelProperty(notes = "Nombre del empleado", example = "", position = -6)
    private String nombreEmpleado;
    
    @JsonProperty(value = "idProtocolo")
    @ApiModelProperty(notes = "Clave del protocolo", example = "", position = -7)
    private Integer idProtocolo;
    
    @JsonProperty(value = "nombreProtocolo")
    @ApiModelProperty(notes = "Nombre del protocolo", example = "", position = -6)
    private String nombreProtocolo;
    
    @JsonProperty(value = "calificacionProtocolo")
    @ApiModelProperty(notes = "Calificación del protocolo", example = "", position = -7)
    private Integer calificacionProtocolo;

    @JsonProperty(value = "idRango")
    @ApiModelProperty(notes = "Identificador del rango de color", example = "", position = -7)
    private Integer idRango;

    @JsonProperty(value = "rangoColor")
    @ApiModelProperty(notes = "Rango de color", example = "", position = -5)
    private String rangoColor;
    
    @JsonProperty(value = "idVisita")
    @ApiModelProperty(notes = "Identificador de la visita", example = "", position = -7)
    private Integer idVisita;
    
    @JsonProperty(value = "calificacionLineal")
    @ApiModelProperty(notes = "Calificación", example = "", position = -7)
    private Integer calificacionLineal;
    
    @JsonProperty(value = "puntosEsperados")
    @ApiModelProperty(notes = "Puntos esperados", example = "", position = -7)
    private Integer puntosEsperados;

    public String getCentroCostosBase() {
        return centroCostosBase;
    }

    public void setCentroCostosBase(String centroCostosBase) {
        this.centroCostosBase = centroCostosBase;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(Integer idProtocolo) {
        this.idProtocolo = idProtocolo;
    }

    public String getNombreProtocolo() {
        return nombreProtocolo;
    }

    public void setNombreProtocolo(String nombreProtocolo) {
        this.nombreProtocolo = nombreProtocolo;
    }

    public Integer getCalificacionProtocolo() {
        return calificacionProtocolo;
    }

    public void setCalificacionProtocolo(Integer calificacionProtocolo) {
        this.calificacionProtocolo = calificacionProtocolo;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public String getRangoColor() {
        return rangoColor;
    }

    public void setRangoColor(String rangoColor) {
        this.rangoColor = rangoColor;
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(Integer idVisita) {
        this.idVisita = idVisita;
    }

    public Integer getCalificacionLineal() {
        return calificacionLineal;
    }

    public void setCalificacionLineal(Integer calificacionLineal) {
        this.calificacionLineal = calificacionLineal;
    }

    public Integer getPuntosEsperados() {
        return puntosEsperados;
    }

    public void setPuntosEsperados(Integer puntosEsperados) {
        this.puntosEsperados = puntosEsperados;
    }

    @Override
    public String toString() {
        return "DetalleVisita{" + "centroCostosBase=" + centroCostosBase + ", centroCostos=" + centroCostos + ", nombreCeco=" + nombreCeco + ", fechaVisita=" + fechaVisita + ", numeroEmpleado=" + numeroEmpleado + ", nombreEmpleado=" + nombreEmpleado + ", idProtocolo=" + idProtocolo + ", nombreProtocolo=" + nombreProtocolo + ", calificacionProtocolo=" + calificacionProtocolo + ", idRango=" + idRango + ", rangoColor=" + rangoColor + ", idVisita=" + idVisita + ", calificacionLineal=" + calificacionLineal + ", puntosEsperados=" + puntosEsperados + '}';
    }
    
}
