package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.net.ssl.SSLEngineResult.Status;

import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ExcelRespuestasDTO;
import com.gs.baz.frq.asignacion.mtto.dto.ReporteConteoGralDTO;

/**
 *
 * @author cescobarh
 */
public class ReporteRespuestasRowMapper implements RowMapper<ExcelRespuestasDTO> {

    private ExcelRespuestasDTO status;

    @Override
    public ExcelRespuestasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ExcelRespuestasDTO();

        status.setIdSoft(rs.getInt("FIID_SOFTN"));
        status.setNumeroEconomico(rs.getInt("FCID_CECO"));
        status.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        status.setIdFase(rs.getInt("FIID_FASE"));
        status.setNumeroEmpleado(rs.getInt("FIID_USUARIO"));
        status.setRespuesta(rs.getString("FCRESPUESTA"));
        status.setPregunta(rs.getString("FCPREGUNTA"));
        status.setRuta(rs.getString("EVIDENCIA"));
        status.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        status.setPeriodo(rs.getString("FCPERIODO"));
        status.setNombreSucursal(rs.getString("NOMCECO"));
        status.setNombreUsuario(rs.getString("NOMUSU"));
        status.setNombreChecklist(rs.getString("NOMCHECK"));
        status.setFase(rs.getNString("NOMFASE"));
        status.setProyecto(rs.getString("NOMPROY"));
        status.setObservacion(rs.getString("FCOBSERVACION"));
        return status;
    }
}
