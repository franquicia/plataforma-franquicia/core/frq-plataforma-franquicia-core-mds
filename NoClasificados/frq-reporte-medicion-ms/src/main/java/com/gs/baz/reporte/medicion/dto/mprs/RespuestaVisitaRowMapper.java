package com.gs.baz.reporte.medicion.dto.mprs;

import com.gs.baz.reporte.medicion.dto.RespuestaVisitaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RespuestaVisitaRowMapper implements RowMapper<RespuestaVisitaDTO> {

    private RespuestaVisitaDTO respuestaVisitaDTO;

    @Override
    public RespuestaVisitaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        respuestaVisitaDTO = new RespuestaVisitaDTO();
        respuestaVisitaDTO.setIdDashboard((BigDecimal) rs.getObject("FIDASHBOARD_ID"));
        respuestaVisitaDTO.setPregunta(rs.getString("FCDESCRIPCION"));
        respuestaVisitaDTO.setRespuesta(rs.getString("FCRESPUESTA"));
        return respuestaVisitaDTO;
    }
}
