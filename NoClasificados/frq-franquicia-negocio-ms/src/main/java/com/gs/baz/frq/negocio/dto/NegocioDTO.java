/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.negocio.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class NegocioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_negocio_franquicia")
    private Integer idNegocioFranquicia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio_disciplina_franquicia")
    private String negocioDisciplinaFranquicia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public NegocioDTO() {
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * @return the idNegocioFranquicia
     */
    public Integer getIdNegocioFranquicia() {
        return idNegocioFranquicia;
    }

    /**
     * @param idNegocioFranquicia the idNegocioFranquicia to set
     */
    public void setIdNegocioFranquicia(BigDecimal idNegocioFranquicia) {
        this.idNegocioFranquicia = (idNegocioFranquicia == null ? null : idNegocioFranquicia.intValue());
    }

    /**
     * @return the idNegocioFranquicia
     */
    public String getNegocioDisciplinaFranquicia() {
        return negocioDisciplinaFranquicia;
    }

    /**
     * @param negocioDisciplinaFranquicia the negocioDisciplinaFranquicia to set
     */
    public void setNegocioDisciplinaFranquicia(String negocioDisciplinaFranquicia) {
        this.negocioDisciplinaFranquicia = negocioDisciplinaFranquicia;
    }

    @Override
    public String toString() {
        return "NegocioFranquiciaDTO{" + "idNegocioFranquicia=" + idNegocioFranquicia + ", negocioDisciplinaFranquicia=" + negocioDisciplinaFranquicia + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
