package com.gs.baz.usuario.mprs;

import com.gs.baz.usuario.dto.BusquedaUsuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class BusquedaUsuarioRowMapper implements RowMapper<BusquedaUsuario> {

    private BusquedaUsuario busquedaUsuario;

    @Override
    public BusquedaUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
        busquedaUsuario = new BusquedaUsuario();
        busquedaUsuario.setNumeroEmpleado(rs.getInt("FIID_EMPLEADO"));
        busquedaUsuario.setNombre(rs.getString("FCNOMBRE"));
        return busquedaUsuario;
    }
}
