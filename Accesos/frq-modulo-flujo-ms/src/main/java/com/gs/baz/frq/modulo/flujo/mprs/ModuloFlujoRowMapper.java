package com.gs.baz.frq.modulo.flujo.mprs;

import com.gs.baz.frq.modulo.flujo.dto.ModuloFlujoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ModuloFlujoRowMapper implements RowMapper<ModuloFlujoDTO> {

    private ModuloFlujoDTO moduloFlujoDTO;

    @Override
    public ModuloFlujoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        moduloFlujoDTO = new ModuloFlujoDTO();
        moduloFlujoDTO.setIdFlujoModulo(((BigDecimal) rs.getObject("FIIDFLUJOMODULO")));
        moduloFlujoDTO.setIdFlujo((BigDecimal) rs.getObject("FIID_FLUJO"));
        moduloFlujoDTO.setIdModulo((BigDecimal) rs.getObject("FIID_MODULO"));
        moduloFlujoDTO.setIdParent((BigDecimal) rs.getObject("FIID_PARENT"));
        moduloFlujoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        moduloFlujoDTO.setOrden((BigDecimal) rs.getObject("FIID_ORDEN"));
        moduloFlujoDTO.setOcultoMenu(((BigDecimal) rs.getObject("FIDINAMICO")).intValue() == 1);
        return moduloFlujoDTO;
    }
}
