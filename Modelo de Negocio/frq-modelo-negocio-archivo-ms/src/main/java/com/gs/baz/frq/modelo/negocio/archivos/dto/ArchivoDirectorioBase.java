/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de pais", value = "PaisBase")
public class ArchivoDirectorioBase {

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del archivo", example = "/franquicia/modelo-negocio/")
    private String nombre;

    @JsonProperty(value = "archivo")
    @ApiModelProperty(notes = "archivo en base 64", example = "archivo principal foda")
    private String archivo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

}
