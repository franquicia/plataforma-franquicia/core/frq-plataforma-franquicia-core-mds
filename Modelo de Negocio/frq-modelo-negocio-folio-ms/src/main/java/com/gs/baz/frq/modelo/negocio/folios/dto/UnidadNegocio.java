/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la unidad de negocio", value = "UnidadNegocio")
public class UnidadNegocio {

    @JsonProperty(value = "idUnidadNegocio")
    @ApiModelProperty(notes = "identificador de la unidad de negocio", example = "21")
    private Integer idUnidadNegocio;

    @JsonProperty(value = "descripcion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Descripcion de la nudad de negocio", example = "ELEKTRA", position = -1)
    private String descripcion;

    public Integer getIdUnidadNegocio() {
        return idUnidadNegocio;
    }

    public void setIdUnidadNegocio(Integer idUnidadNegocio) {
        this.idUnidadNegocio = idUnidadNegocio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "UnidadNegocio{" + "idUnidadNegocio=" + idUnidadNegocio + ", descripcion=" + descripcion + '}';
    }

}
