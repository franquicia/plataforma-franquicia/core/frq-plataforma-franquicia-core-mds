/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.Ruta;
import com.gs.baz.frq.modelo.negocio.folios.dto.UnidadNegocio;
import com.gs.baz.frq.modelo.negocio.folios.dto.Codificacion;
import com.gs.baz.frq.modelo.negocio.folios.dto.Modelo;
import com.gs.baz.frq.modelo.negocio.folios.dto.Pais;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FolioRutasRowMapper implements RowMapper<Ruta> {

    private Ruta ruta;

    @Override
    public Ruta mapRow(ResultSet rs, int rowNum) throws SQLException {
        ruta = new Ruta();
        ruta.setIdRuta(rs.getInt("FIIDRUTA"));
        Codificacion codificacion = new Codificacion();
        codificacion.setDescripcion(rs.getString("FCCODIFICACION"));
        ruta.setCodificacion(codificacion);
        Pais pais = new Pais();
        pais.setDescripcion(rs.getString("FCDESCRIPCIONPAIS"));
        pais.setIdPais(rs.getInt("FIIDPAIS"));
        ruta.setPais(pais);
        Modelo modelo = new Modelo();
        modelo.setIdModelo(rs.getInt("FIIDMODELO"));
        modelo.setDescripcion(rs.getString("FCDESCRIPCIONMODELO"));
        ruta.setModelo(modelo);
        UnidadNegocio unidadNegocio = new UnidadNegocio();
        unidadNegocio.setIdUnidadNegocio(rs.getInt("FIIDUNIDADNEGOCIO"));
        unidadNegocio.setDescripcion(rs.getString("FCDESCRIPCIONUN"));
        ruta.setUnidadNegocio(unidadNegocio);
        return ruta;
    }
}
