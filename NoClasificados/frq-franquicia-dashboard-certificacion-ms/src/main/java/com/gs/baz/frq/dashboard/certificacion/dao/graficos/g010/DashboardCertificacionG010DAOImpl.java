package com.gs.baz.frq.dashboard.certificacion.dao.graficos.g010;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g010.DashboardCertificacionG010DTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g010.DashboardCertificacionG010RowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG010DAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PADASHCCCERT");
        jdbcSelect.withProcedureName("SPASEGURADOR");
        jdbcSelect.returningResultSet("PA_CDATOS", new DashboardCertificacionG010RowMapper());

    }

    public List<DashboardCertificacionG010DTO> selectRowDashboardCertificacionG010(Long entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", entityDTO);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionG010DTO>) out.get("PA_CDATOS");

    }

    public List<DashboardCertificacionG010DTO> selectRowsDashboardCertificacionG010() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUARIO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);

        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionG010DTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }

}
