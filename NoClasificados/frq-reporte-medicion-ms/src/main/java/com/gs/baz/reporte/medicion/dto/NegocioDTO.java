/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.reporte.medicion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class NegocioDTO {

    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", descripcion=" + descripcion + '}';
    }

}
