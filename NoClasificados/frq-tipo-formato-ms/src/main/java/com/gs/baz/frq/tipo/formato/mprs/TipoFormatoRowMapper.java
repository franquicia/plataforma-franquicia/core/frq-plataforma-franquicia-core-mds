package com.gs.baz.frq.tipo.formato.mprs;

import com.gs.baz.frq.tipo.formato.dto.TipoFormatoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class TipoFormatoRowMapper implements RowMapper<TipoFormatoDTO> {

    private TipoFormatoDTO status;

    @Override
    public TipoFormatoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new TipoFormatoDTO();
        status.setIdTipoFormato(((BigDecimal) rs.getObject("FIIDTIFORM")));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        return status;
    }
}
