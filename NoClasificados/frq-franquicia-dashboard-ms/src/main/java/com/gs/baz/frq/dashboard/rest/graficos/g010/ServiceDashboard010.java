/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g010;

import com.gs.baz.frq.dashboard.dao.graficos.g010.DashboardDAOImpl010;
import com.gs.baz.frq.dashboard.dto.graficos.g010.CumplimientoDisciplinaPuntoContactoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard010 {

    @Autowired
    private DashboardDAOImpl010 dashboardDAOImpl010;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/disciplina/punto/contacto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CumplimientoDisciplinaPuntoContactoDTO> getCumplimientoDisciplinaPuntoContacto(@RequestBody CumplimientoDisciplinaPuntoContactoDTO cumplimientoDisciplinaPuntoContacto) throws CustomException {
        return dashboardDAOImpl010.selectRowsCumplimientoDisciplinaPuntoContacto(cumplimientoDisciplinaPuntoContacto);
    }

}
