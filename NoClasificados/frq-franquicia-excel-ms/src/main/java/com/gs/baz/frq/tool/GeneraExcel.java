/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.tool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ecarrascome
 */
public class GeneraExcel {

    private CellStyle boddyCellStyle;
    private CellStyle headerCellStyle;
    private Workbook workbook;
    private Sheet sheet;
    private ArrayList<Sheet> sheets;

    public GeneraExcel(String tituloPestana, List<String> encabezado, Map<Integer, ArrayList<Object>> cuerpo) {

        this.workbook = new XSSFWorkbook();
        this.sheet = workbook.createSheet(tituloPestana);

        generaEstilos();
        generaCuerpo(cuerpo);
        generaEncabezado(encabezado);

    }

    public GeneraExcel() {

    }

//    public void GeneraExcelMultiplesHojas(ArrayList<String> tituloPestanas, ArrayList<String> encabezado, Map<Integer, ArrayList<Object>> cuerpo) {
//
//        this.workbook = new XSSFWorkbook();
//        if(tituloPestanas!=null && tituloPestanas.size()>0){
//            for (Iterator<String> it = tituloPestanas.iterator(); it.hasNext();) {
//                String titulo = it.next();
//                this.sheet = workbook.createSheet(titulo);
//                sheets.add(sheet);
//            }
//        }
//
//        generaEstilos();
//        generaEncabezado(encabezado);
//        generaCuerpo(cuerpo);
//
//    }
    private void generaEstilos() {
        // Cabecera
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 18);

        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

        // Cuerpo
        Font bodyFont = workbook.createFont();
        bodyFont.setBold(false);
        bodyFont.setFontHeightInPoints((short) 18);

        boddyCellStyle = workbook.createCellStyle();
        boddyCellStyle.setFont(bodyFont);
        boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);
    }

    private void generaEncabezado(List<String> encabezado) {

        Row headerRow = sheet.createRow(0);
        for (int i = 0; i < encabezado.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(encabezado.get(i));
            cell.setCellStyle(headerCellStyle);
            int columnIndex = cell.getColumnIndex();
            sheet.autoSizeColumn(columnIndex);

        }
    }

    public void generaCuerpo(Map<Integer, ArrayList<Object>> cuerpo) {
        int fila = 0;
        if (cuerpo != null) {
            for (Map.Entry<Integer, ArrayList<Object>> entry : cuerpo.entrySet()) {

                Cell cell;
                fila++;
                int columna = 0;

                Row filaExcel = sheet.createRow(fila);
                if (entry.getValue() != null && entry.getValue().size() > 0) {
                    for (Object value : entry.getValue()) {
                        cell = filaExcel.createCell(columna++);
                        if (cell != null) {
                            if (value != null) {
                                cell.setCellValue(value.toString());
                            } else {
                                cell.setCellValue("null");
                            }
                            cell.setCellStyle(boddyCellStyle);
                        }
                    }
                }
                filaExcel.setRowStyle(boddyCellStyle);
                //filaExcel.setHeightInPoints(150);
            }
        }

    }

    public Workbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(Workbook workbook) {
        this.workbook = workbook;
    }

}
