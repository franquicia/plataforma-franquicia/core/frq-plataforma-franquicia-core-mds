package com.gs.baz.frq.frecuencia.medicion.dao;

import com.gs.baz.frq.frecuencia.medicion.dao.util.GenericDAO;
import com.gs.baz.frq.frecuencia.medicion.dto.FrecuenciaMedicionDTO;
import com.gs.baz.frq.frecuencia.medicion.mprs.FrecuenciaMedicionRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class FrecuenciaMedicionDAOImpl extends DefaultDAO implements GenericDAO<FrecuenciaMedicionDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectFrecuenciaMedicion;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMFRECMED");
        jdbcInsert.withProcedureName("SPINSFRECMED");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMFRECMED");
        jdbcUpdate.withProcedureName("SPACTFRECMED");

        jdbcSelectFrecuenciaMedicion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFrecuenciaMedicion.withSchemaName(schema);
        jdbcSelectFrecuenciaMedicion.withCatalogName("PAADMFRECMED");
        jdbcSelectFrecuenciaMedicion.withProcedureName("SPGETFRECMED");
        jdbcSelectFrecuenciaMedicion.returningResultSet("PA_CDATOS", new FrecuenciaMedicionRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMFRECMED");
        jdbcSelectByName.withProcedureName("SPGETFRECMED");
        jdbcSelectByName.returningResultSet("PA_CDATOS", new FrecuenciaMedicionRowMapper());

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMFRECMED");
        jdbcDelete.withProcedureName("SPDELFRECMED");
    }

    @Override
    public FrecuenciaMedicionDTO insertRow(FrecuenciaMedicionDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityDTO.getIdFrecuencia());
            mapSqlParameterSource.addValue("PA_FCFRECUENCIA", entityDTO.getFrecuencia());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  FrecuenciaMedicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  FrecuenciaMedicion"), ex);
        }
    }

    @Override
    public FrecuenciaMedicionDTO updateRow(FrecuenciaMedicionDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityDTO.getIdFrecuencia());
            mapSqlParameterSource.addValue("PA_FCFRECUENCIA", entityDTO.getFrecuencia());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of FrecuenciaMedicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of FrecuenciaMedicion"), ex);
        }
    }

    @Override
    public List<FrecuenciaMedicionDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", null);
        Map<String, Object> out = jdbcSelectFrecuenciaMedicion.execute(mapSqlParameterSource);
        return (List<FrecuenciaMedicionDTO>) out.get("PA_CDATOS");
    }

    @Override
    public FrecuenciaMedicionDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityID);
        Map<String, Object> out = jdbcSelectFrecuenciaMedicion.execute(mapSqlParameterSource);
        List<FrecuenciaMedicionDTO> data = (List<FrecuenciaMedicionDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public FrecuenciaMedicionDTO deleteRow(FrecuenciaMedicionDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIFRECUENCIA_ID", entityDTO.getIdFrecuencia());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of FrecuenciaMedicion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of FrecuenciaMedicion"), ex);
        }
    }

}
