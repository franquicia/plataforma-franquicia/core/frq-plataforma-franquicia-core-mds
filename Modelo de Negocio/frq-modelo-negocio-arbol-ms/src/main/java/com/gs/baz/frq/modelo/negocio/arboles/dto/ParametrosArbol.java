/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosArbol {

    @JsonProperty(value = "idModeloNegocio")
    private transient Integer idModeloNegocio;

    @JsonProperty(value = "idPadre")
    @ApiModelProperty(notes = "identificador del padre del modelo de negocio", example = "1", required = true)
    private Integer idPadre;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del pais", example = "ModeloNegocio 1", required = true)
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "nombre del arbol del modelo de negocio", example = "ModeloNegocio 1", required = true)
    private String nombre;

    @JsonProperty(value = "icono")
    @ApiModelProperty(notes = "icono del arbol del modelo de negocio", example = "/franquicia/modelo-negocio/iconos/prueba.png", required = true)
    private String icono;

    @JsonProperty(value = "idNivel")
    @ApiModelProperty(notes = "identificador del nivel del modelo de negocio", example = "1", required = true)
    private Integer idNivel;

    public Integer getIdModeloNegocio() {
        return idModeloNegocio;
    }

    public void setIdModeloNegocio(Integer idModeloNegocio) {
        this.idModeloNegocio = idModeloNegocio;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }
}
