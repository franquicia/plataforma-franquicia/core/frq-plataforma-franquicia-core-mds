/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos Gerente", value = "Gerente")
public class Gerente {

    @JsonProperty(value = "nombreGerente")
    @ApiModelProperty(notes = "Nombre del gerente", example = "JOSE ALBERTO AGUILAR MORALES", position = -1)
    private String nombreGerente;

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    @Override
    public String toString() {
        return "Gerente{" + "nombreGerente=" + nombreGerente + '}';
    }
    
    
}
