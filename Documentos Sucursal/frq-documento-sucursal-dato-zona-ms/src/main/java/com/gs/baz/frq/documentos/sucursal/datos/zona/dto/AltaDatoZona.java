/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del detalle  de informacion de zona ", value = "AltaDatoZona")
public class AltaDatoZona {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador del dato de la zona", example = "1")
    private Integer id;

    public AltaDatoZona(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
