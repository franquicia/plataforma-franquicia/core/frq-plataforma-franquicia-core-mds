/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author cescobarh
 */
public final class SecurityUtils {

    private static KeyFactory keyFactory;
    private static PKCS8EncodedKeySpec pKCS8EncodedKeySpec;
    private static final Base64.Decoder decoder = Base64.getDecoder();
    private static final Base64.Encoder encoder = Base64.getEncoder();

    private SecurityUtils() {
    }

    public static class Parser {

        private static X509EncodedKeySpec x509EncodedKeySpec;

        public static PublicKey parsePublicKey(String publicKeyB64) throws SecurityUtilsException {
            try {
                byte[] decode = decoder.decode(publicKeyB64);
                x509EncodedKeySpec = new X509EncodedKeySpec(decode);
                keyFactory = KeyFactory.getInstance("RSA");
                return keyFactory.generatePublic(x509EncodedKeySpec);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new SecurityUtilsException(e);
            }
        }

        public static PrivateKey parsePrivateKey(String privateKeyB64) throws SecurityUtilsException {
            try {
                byte[] decode = decoder.decode(privateKeyB64);
                pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(decode);
                keyFactory = KeyFactory.getInstance("RSA");
                return keyFactory.generatePrivate(pKCS8EncodedKeySpec);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new SecurityUtilsException(e);
            }
        }
    }

    public static class Decrypter {

        private Key privateKey;

        public Decrypter(String privateKeyB64) throws SecurityUtilsException {
            this.privateKey = SecurityUtils.Parser.parsePrivateKey(privateKeyB64);
        }

        public Decrypter(PrivateKey privateKey) {
            this.privateKey = privateKey;
        }

        public Decrypter(Key key) {
            this.privateKey = key;
        }

        public String decrypt(String texto) throws SecurityUtilsException {
            try {
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
                return new String(cipher.doFinal(decoder.decode(texto)), StandardCharsets.UTF_8.toString());
            } catch (BadPaddingException | IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | NullPointerException e) {
                throw new SecurityUtilsException(e);
            }
        }

    }

    public static class Encrypter {

        private final Key publicKey;

        public Encrypter(String publicKeyB64) throws SecurityUtilsException {
            this.publicKey = SecurityUtils.Parser.parsePublicKey(publicKeyB64);
        }

        public Encrypter(PublicKey publicKey) {
            this.publicKey = publicKey;
        }

        public Encrypter(Key key) {
            this.publicKey = key;
        }

        public String encrypt(String texto) throws SecurityUtilsException {
            try {
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipher.init(Cipher.ENCRYPT_MODE, this.publicKey);
                return encoder.encodeToString(cipher.doFinal(texto.getBytes(StandardCharsets.UTF_8.toString())));
            } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
                throw new SecurityUtilsException(e);
            }
        }

    }

    public static class Generator {

        private PublicKey publicKey;
        private PrivateKey privateKey;
        private KeyPairGenerator keyPairGenerator;
        private KeyPair keyPair;
        private final int keySize = 2048;

        public Generator() throws SecurityUtilsException {
            this.keyPair();
        }

        public void getPairsFromPrivateKey(String privateKeyB64) throws SecurityUtilsException {
            try {
                byte[] decode = decoder.decode(privateKeyB64);
                pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(decode);
                keyFactory = KeyFactory.getInstance("RSA");
                this.privateKey = keyFactory.generatePrivate(pKCS8EncodedKeySpec);
                RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey) privateKey;
                RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(rsaPrivateCrtKey.getModulus(), rsaPrivateCrtKey.getPublicExponent());
                this.publicKey = keyFactory.generatePublic(rsaPublicKeySpec);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new SecurityUtilsException(e);
            }
        }

        private void keyPair() throws SecurityUtilsException {
            try {
                this.keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                this.keyPairGenerator.initialize(keySize);
                this.keyPair = this.keyPairGenerator.generateKeyPair();
                this.publicKey = this.keyPair.getPublic();
                this.privateKey = this.keyPair.getPrivate();
            } catch (NoSuchAlgorithmException e) {
                throw new SecurityUtilsException(e);
            }
        }

        public PublicKey getPublicKey() {
            return publicKey;
        }

        public PrivateKey getPrivateKey() {
            return privateKey;
        }

        public String publicKeyToString() {
            return encoder.encodeToString(this.publicKey.getEncoded());
        }

        public String privateKeyToString() {
            return encoder.encodeToString(this.privateKey.getEncoded());
        }
    }

}
