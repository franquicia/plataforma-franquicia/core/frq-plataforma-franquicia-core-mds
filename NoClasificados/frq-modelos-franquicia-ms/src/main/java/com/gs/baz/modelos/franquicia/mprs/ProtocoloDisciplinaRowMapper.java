package com.gs.baz.modelos.franquicia.mprs;

import com.gs.baz.modelos.franquicia.dto.ProtocoloDisciplinaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDisciplinaRowMapper implements RowMapper<ProtocoloDisciplinaDTO> {

    private ProtocoloDisciplinaDTO protocoloNegocio;

    @Override
    public ProtocoloDisciplinaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        protocoloNegocio = new ProtocoloDisciplinaDTO();
        protocoloNegocio.setIdProtocoloDisciplina(((BigDecimal) rs.getObject("FIIPROTDIS")));
        protocoloNegocio.setIdProtocolo(((BigDecimal) rs.getObject("FIIDPROT")));
        return protocoloNegocio;
    }
}
