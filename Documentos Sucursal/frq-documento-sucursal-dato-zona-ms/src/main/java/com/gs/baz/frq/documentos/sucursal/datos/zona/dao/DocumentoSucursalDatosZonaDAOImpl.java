/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.dao;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.AltaDatoZona;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ArchivoStatus;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ArchivoUltimos;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.DatoZonaBase;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.Datozona;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ParametrosDatoZona;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ValidaCecoFormatoProyecto;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorCeco;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorDocumento;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorVersion;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.ArchivoStatusRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.DatoZonaRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.DatosZonaRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.UltimoProyectoRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.ValidaCecoFormatoProyectoRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.ZonasPorCecoRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.ZonasPorDocumentoRowMapper;
import com.gs.baz.frq.documentos.sucursal.datos.zona.mprs.ZonasPorVersionRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class DocumentoSucursalDatosZonaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcDelete;

    private DefaultJdbcCall jdbcValida;
    private DefaultJdbcCall jdbcZonasPorDocumento;

    private DefaultJdbcCall jdbcZonasPorVersion;
    private DefaultJdbcCall jdbcZonasPorCeco;

    private DefaultJdbcCall jdbcUltProyecto;
    private DefaultJdbcCall jdbcArchivoStatus;

    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        String catalogo = "PAADDATOZONALY";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SP_INS_DATZONA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SP_ACT_DATZONA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SP_DEL_DATZONA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SP_GET_DATZONA");
        jdbcSelect.returningResultSet("PA_CDATOS", new DatoZonaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SP_GET_DATZONA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new DatosZonaRowMapper());

        jdbcValida = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcValida.withSchemaName(schema);
        jdbcValida.withCatalogName(catalogo);
        jdbcValida.withProcedureName("SP_SEL_STATUSEDIC");
        jdbcValida.returningResultSet("PA_CDATOS", new ValidaCecoFormatoProyectoRowMapper());

        jdbcZonasPorDocumento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcZonasPorDocumento.withSchemaName(schema);
        jdbcZonasPorDocumento.withCatalogName(catalogo);
        jdbcZonasPorDocumento.withProcedureName("SP_SEL_ZONAZDOC");
        jdbcZonasPorDocumento.returningResultSet("PA_CDATOS", new ZonasPorDocumentoRowMapper());

        String soporte = "PA_SOPDOCS";
        jdbcZonasPorVersion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcZonasPorVersion.withSchemaName(schema);
        jdbcZonasPorVersion.withCatalogName(soporte);
        jdbcZonasPorVersion.withProcedureName("SP_SEL_ZONVERS");
        jdbcZonasPorVersion.returningResultSet("RCL_FICHA", new ZonasPorVersionRowMapper());

        jdbcZonasPorCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcZonasPorCeco.withSchemaName(schema);
        jdbcZonasPorCeco.withCatalogName(soporte);
        jdbcZonasPorCeco.withProcedureName("SP_SEL_ZONCECO");
        jdbcZonasPorCeco.returningResultSet("RCL_FICHA", new ZonasPorCecoRowMapper());

        jdbcUltProyecto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUltProyecto.withSchemaName(schema);
        jdbcUltProyecto.withCatalogName(soporte);
        jdbcUltProyecto.withProcedureName("SP_SEL_ARULT");
        jdbcUltProyecto.returningResultSet("RCL_FICHA", new UltimoProyectoRowMapper());

        jdbcArchivoStatus = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcArchivoStatus.withSchemaName(schema);
        jdbcArchivoStatus.withCatalogName(soporte);
        jdbcArchivoStatus.withProcedureName("SP_SEL_ARCHESTAT");
        jdbcArchivoStatus.returningResultSet("RCL_FICHA", new ArchivoStatusRowMapper());

    }

    public DatoZonaBase selectRow(Integer idCampoSensible) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDZONA", idCampoSensible);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<DatoZonaBase> data = (List<DatoZonaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Datozona> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDZONA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Datozona>) out.get("PA_CDATOS");
    }

    public AltaDatoZona insertRow(ParametrosDatoZona entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CATZONA", entityDTO.getIdCatalogoZona());
            mapSqlParameterSource.addValue("PA_IDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_VALOR", entityDTO.getValor());
            mapSqlParameterSource.addValue("PA_OBSERVACION", entityDTO.getObservaciones());
            mapSqlParameterSource.addValue("PA_IDVERSION", entityDTO.getIdVersion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_IDDATZONA");
                return new AltaDatoZona(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosDatoZona entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CATZONA", entityDTO.getIdCatalogoZona());
            mapSqlParameterSource.addValue("PA_IDFOLIO", entityDTO.getIdFolio());
            mapSqlParameterSource.addValue("PA_VALOR", entityDTO.getValor());
            mapSqlParameterSource.addValue("PA_OBSERVACION", entityDTO.getObservaciones());
            mapSqlParameterSource.addValue("PA_IDDATZONA", entityDTO.getIdDatoZona());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_IDZONA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public List<ValidaCecoFormatoProyecto> selectRowValida(ValidaCecoFormatoProyecto entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FORMATO", entityDTO.getIdFormato());
        mapSqlParameterSource.addValue("PA_PROYECTO", entityDTO.getIdProyecto());
        mapSqlParameterSource.addValue("PA_DOCUMENTO", entityDTO.getIdDocumento());
        Map<String, Object> out = jdbcValida.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")

        List<ValidaCecoFormatoProyecto> data = (List<ValidaCecoFormatoProyecto>) out.get("PA_CDATOS");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }

    }

    public List<ZonasPorDocumento> selectRowZonasDocumento(ZonasPorDocumento entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FORMATO", entityDTO.getIdFormato());
        mapSqlParameterSource.addValue("PA_PROYECTO", entityDTO.getIdProyecto());
        mapSqlParameterSource.addValue("PA_DOCUMENTO", entityDTO.getIdDocumento());
        Map<String, Object> out = jdbcZonasPorDocumento.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")

        List<ZonasPorDocumento> data = (List<ZonasPorDocumento>) out.get("PA_CDATOS");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ZonasPorVersion> selectRowZonasVersion(ZonasPorVersion entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FORMATO", entityDTO.getIdFormato());
        mapSqlParameterSource.addValue("PA_PROYECTO", entityDTO.getIdProyecto());
        mapSqlParameterSource.addValue("PA_DOCUMENTO", entityDTO.getIdDocumento());
        mapSqlParameterSource.addValue("PA_IDVERSION", entityDTO.getIdVersion());
        Map<String, Object> out = jdbcZonasPorVersion.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")

        List<ZonasPorVersion> data = (List<ZonasPorVersion>) out.get("RCL_FICHA");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ZonasPorCeco> selectRowZonasCeco(ZonasPorCeco entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

        Map<String, Object> out = jdbcZonasPorCeco.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")

        List<ZonasPorCeco> data = (List<ZonasPorCeco>) out.get("RCL_FICHA");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ArchivoUltimos> selectRowProyectoUltimo(ArchivoUltimos entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());

        Map<String, Object> out = jdbcUltProyecto.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")

        List<ArchivoUltimos> data = (List<ArchivoUltimos>) out.get("RCL_FICHA");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public List<ArchivoStatus> selectRowEstatus(ArchivoStatus entityDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_FORMATO", entityDTO.getIdFormato());
        mapSqlParameterSource.addValue("PA_PROYECTO", entityDTO.getIdProyecto());
        mapSqlParameterSource.addValue("PA_DOCUMENTO", entityDTO.getIdDocumento());

        Map<String, Object> out = jdbcUltProyecto.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")

        List<ArchivoStatus> data = (List<ArchivoStatus>) out.get("RCL_FICHA");

        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
