package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ReporteProgramadasDTO;

/**
 *
 * @author cescobarh
 */
public class ReporteProgramadasRowMapper implements RowMapper<ReporteProgramadasDTO> {

    private ReporteProgramadasDTO status;

    @Override
    public ReporteProgramadasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ReporteProgramadasDTO();

        status.setIdProgramacion(rs.getInt("FIID_HISTORICO"));
        status.setIdCeco(rs.getInt("FCID_CECO"));
        status.setNombreCeco(rs.getString("NOMBRE_CECO"));
        status.setIdUsuario(rs.getInt("FIID_USUARIO"));
        status.setNombreUsuario(rs.getString("NOMBRE_USU"));
        status.setEstatus(rs.getString("ESTATUS"));
        status.setFechaProgramada(rs.getString("FECHAPROG"));
        status.setZona(rs.getString("ZONA"));
        status.setTerritorio(rs.getString("TERRITORIO"));
        status.setFechaRealizada(rs.getString("ECHAS"));
        status.setReprogramada(rs.getString("REPROGRAMADA"));
        status.setComentarioReprogramacion(rs.getString("FCCOMENTAREPROG"));
        status.setIdZona(rs.getInt("FIID_GEOGRA"));
        status.setIdTerritorio(rs.getInt("FIID_NIVELPADRE"));
       
        return status;
    }
}
