/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.folio.mprs;

import com.gs.baz.frq.documentos.sucursal.folio.dto.Folio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FolioRowMapper implements RowMapper<Folio> {

    private Folio folio;

    @Override
    public Folio mapRow(ResultSet rs, int rowNum) throws SQLException {
        folio = new Folio();

        folio.setId(rs.getInt("FIID_FOLIO"));
        folio.setIdCreador(rs.getString("FIID_CREADOR"));
        folio.setStatus(rs.getInt("FIID_STATUS"));

        return folio;
    }

}
