/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.propiedades.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.propiedades.dao.PropiedadesDAOImpl;
import com.gs.baz.propiedades.dto.PropiedadesDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/propiedades")
public class ServicePropiedades {

    @Autowired
    private PropiedadesDAOImpl propiedadesDAOImpl;

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @param id_propiedad
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_propiedad}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PropiedadesDTO getPropiedad(@PathVariable("id_propiedad") Long id_propiedad) throws CustomException {
        return propiedadesDAOImpl.selectRow(id_propiedad);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PropiedadesDTO> getPropiedades() throws CustomException {
        return propiedadesDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PropiedadesDTO postPropiedades(@RequestBody PropiedadesDTO propiedades) throws CustomException {
        if (propiedades.getClave() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("clave"));
        }
        if (propiedades.getValor() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("valor"));
        }
        if (propiedades.getEncriptado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("encriptado"));
        }
        return propiedadesDAOImpl.insertRow(propiedades);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public PropiedadesDTO putPropiedades(@RequestBody PropiedadesDTO propiedades) throws CustomException {
        if (propiedades.getIdPropiedad() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_propiedad"));
        }
        if (propiedades.getClave() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("clave"));
        }
        if (propiedades.getValor() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("valor"));
        }
        if (propiedades.getEncriptado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("encriptado"));
        }
        return propiedadesDAOImpl.updateRow(propiedades);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PropiedadesDTO deletePropiedades(@RequestBody PropiedadesDTO propiedades) throws CustomException {
        if (propiedades.getIdPropiedad() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_propiedad"));
        }
        return propiedadesDAOImpl.deleteRow(propiedades);
    }
}
