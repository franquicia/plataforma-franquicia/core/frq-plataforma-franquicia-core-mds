/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.mprs;

import com.gs.baz.frq.modelo.negocio.comentarios.firma.dto.ComentrioFirma;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ComentariosFirmaRowMapper implements RowMapper<ComentrioFirma> {

    private ComentrioFirma comentrioFirma;

    @Override
    public ComentrioFirma mapRow(ResultSet rs, int rowNum) throws SQLException {
        comentrioFirma = new ComentrioFirma();
        comentrioFirma.setIdComentario(rs.getInt("FIIDCOMENTARIO"));
        comentrioFirma.setIdFirma(rs.getInt("FIIDFIRMA"));
        comentrioFirma.setComentario(rs.getString("FCCOMENTARIO"));
        comentrioFirma.setFecha(rs.getString("FDFECHA"));
        comentrioFirma.setIdEstadoFirma(rs.getInt("FIIDESTADOFIRMA"));
        return comentrioFirma;
    }
}
