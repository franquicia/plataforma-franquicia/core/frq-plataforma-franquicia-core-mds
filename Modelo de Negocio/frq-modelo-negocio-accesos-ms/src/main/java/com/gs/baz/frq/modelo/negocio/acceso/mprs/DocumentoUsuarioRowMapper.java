/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.mprs;

import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Documento;
import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Folio;
import com.gs.baz.frq.modelo.negocio.acceso.dto.usuario.DocumentoUsuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoUsuarioRowMapper implements RowMapper<DocumentoUsuario> {

    private DocumentoUsuario documentoUsuario;

    @Override
    public DocumentoUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
        documentoUsuario = new DocumentoUsuario();
        Folio folio = new Folio();
        folio.setIdFolio(rs.getInt("FIIDFOLIO"));
        documentoUsuario.setFolio(folio);
        Documento documento = new Documento();
        documento.setIdDocumento(rs.getInt("FIIDDOCUMENTO"));
        documento.setTitulo(rs.getString("FCTITULO"));
        documento.setVersion(rs.getString("FCVERSION"));
        documento.setObjetivo(rs.getString("FCOBJETIVO"));
        documento.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        folio.setDocumento(documento);
        documentoUsuario.setFolio(folio);
        documentoUsuario.setIdDocumentoUsuario(rs.getInt("FIIDUSUDOCTO"));
        return documentoUsuario;
    }
}
