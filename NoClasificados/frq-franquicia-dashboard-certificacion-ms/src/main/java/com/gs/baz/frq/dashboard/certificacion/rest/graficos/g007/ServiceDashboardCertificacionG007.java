/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.graficos.g007;

import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g007.DashboardCertificacionG007DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g007.DashboardCertificacionG007DTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/g007")
public class ServiceDashboardCertificacionG007 {

    @Autowired
    private DashboardCertificacionG007DAOImpl dashboardCertificacionG007DAOImpl;

    @RequestMapping(value = "secure/grafico/get/certificados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG007DTO> getDatosPuntosCertificadosG007() throws CustomException {
        return dashboardCertificacionG007DAOImpl.selectRowsDashboardCertificacionG007();
    }

    @RequestMapping(value = "secure/grafico/get/certificados/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionG007DTO> getDatoPuntosCertificadosG007(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFin", required = true) String fechaFin,
            @RequestParam(value = "idGerente", required = false) Long idGerente,
            @RequestParam(value = "idNegocio", required = false) Long idNegocio,
            @RequestParam(value = "idUsuario", required = false) Long idUsuario) throws CustomException {
        if (fechaInicial == null || fechaInicial.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicial"));
        }
        if (fechaFin == null || fechaFin.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaFin"));
        }

        return dashboardCertificacionG007DAOImpl.selectRowDashboardCertificacionG007(fechaInicial, fechaFin, idGerente, idNegocio, idUsuario);
    }

}
