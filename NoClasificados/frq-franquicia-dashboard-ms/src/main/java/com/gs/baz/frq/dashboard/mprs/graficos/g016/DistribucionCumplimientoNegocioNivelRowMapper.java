package com.gs.baz.frq.dashboard.mprs.graficos.g016;

import com.gs.baz.frq.dashboard.dto.graficos.g016.DistribucionCumplimientoCecoNegocioNivelDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DistribucionCumplimientoNegocioNivelRowMapper implements RowMapper<DistribucionCumplimientoCecoNegocioNivelDTO> {

    private DistribucionCumplimientoCecoNegocioNivelDTO distribucionCumplimientoCecoNegocioNivelDTO;

    @Override
    public DistribucionCumplimientoCecoNegocioNivelDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        distribucionCumplimientoCecoNegocioNivelDTO = new DistribucionCumplimientoCecoNegocioNivelDTO();
        distribucionCumplimientoCecoNegocioNivelDTO.setDescripcion(rs.getString("DESCRIPCION"));
        distribucionCumplimientoCecoNegocioNivelDTO.setIdRango((BigDecimal) rs.getObject("FIRANGO_ID"));
        distribucionCumplimientoCecoNegocioNivelDTO.setPorcentaje((BigDecimal) rs.getObject("PORCENTAJE"));
        distribucionCumplimientoCecoNegocioNivelDTO.setPuntosContacto((BigDecimal) rs.getObject("PUNTOSCONTACTO"));
        distribucionCumplimientoCecoNegocioNivelDTO.setIdCeco(new Integer(rs.getString("CECO")));
        return distribucionCumplimientoCecoNegocioNivelDTO;
    }
}
