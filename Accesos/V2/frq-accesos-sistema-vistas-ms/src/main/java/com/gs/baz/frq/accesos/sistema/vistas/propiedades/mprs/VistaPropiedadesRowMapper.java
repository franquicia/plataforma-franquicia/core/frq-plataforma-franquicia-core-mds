/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.mprs;

import com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto.VistaPropiedad;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class VistaPropiedadesRowMapper implements RowMapper<VistaPropiedad> {

    private VistaPropiedad vistaPropiedad;

    @Override
    public VistaPropiedad mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPropiedad = new VistaPropiedad();
        vistaPropiedad.setIdPropiedad(rs.getInt("FIIDPROPVISTA"));
        vistaPropiedad.setNombre(rs.getString("FCNOMBRE"));
        vistaPropiedad.setTipo(rs.getString("FCTIPO"));
        vistaPropiedad.setValor(rs.getString("FCVALOR"));
        return vistaPropiedad;
    }
}
