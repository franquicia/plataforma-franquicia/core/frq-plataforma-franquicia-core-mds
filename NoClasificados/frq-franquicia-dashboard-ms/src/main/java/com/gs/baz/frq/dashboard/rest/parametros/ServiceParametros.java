/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.parametros;

import com.gs.baz.frq.dashboard.dao.parametros.ParametrosDAOImpl;
import com.gs.baz.frq.dashboard.dto.parametros.ParametroDTO;
import com.gs.baz.frq.model.commons.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MADA
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceParametros {

    @Autowired
    private ParametrosDAOImpl parametrosDAOImpl;

    @RequestMapping(value = "secure/parametro/{clave}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ParametroDTO getParametro(@PathVariable("clave") String clave) throws CustomException {
        return parametrosDAOImpl.selectRow(clave);
    }

}
