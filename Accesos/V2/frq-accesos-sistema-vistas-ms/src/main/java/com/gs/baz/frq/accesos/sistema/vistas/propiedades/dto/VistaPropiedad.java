/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la Propiedad", value = "VistaPropiedad")
public class VistaPropiedad extends VistaPropiedadBase {

    @JsonProperty(value = "idPropiedad")
    @ApiModelProperty(notes = "Identificador de la propiedad", example = "1", position = -1)
    private Integer idPropiedad;

    public Integer getIdPropiedad() {
        return idPropiedad;
    }

    public void setIdPropiedad(Integer idPropiedad) {
        this.idPropiedad = idPropiedad;
    }

    @Override
    public String toString() {
        return "VistaPropiedad{" + "idPropiedad=" + idPropiedad + '}';
    }

}
