/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g006;

import com.gs.baz.frq.dashboard.dao.graficos.g002.DashboardDAOImpl002;
import com.gs.baz.frq.dashboard.dao.graficos.g006.DashboardDAOImpl006;
import com.gs.baz.frq.dashboard.dto.graficos.g002.CalificacionNegociosDisciplinaDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g006.CumplimientoPuntajeNegocioNacionalDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g006.NegocioDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard006 {

    @Autowired
    private DashboardDAOImpl006 dashboardDAOImpl006;

    @Autowired
    private DashboardDAOImpl002 dashboardDAOImpl002;

    @RequestMapping(value = "secure/grafico/get/cumplimiento/puntaje/negocio/nacional", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CumplimientoPuntajeNegocioNacionalDTO getCumplimientoPuntajeNacionalNegocio(@RequestBody NegocioDTO cumplimientoPuntajeNacionalNegocio) throws CustomException {
        CumplimientoPuntajeNegocioNacionalDTO cumplimientoPuntajeNegocioNacionalDTO = new CumplimientoPuntajeNegocioNacionalDTO();
        List<NegocioDTO> negocios = dashboardDAOImpl006.selectRowsCumplimientoPuntajeNegocioNacional(cumplimientoPuntajeNacionalNegocio);

        CalificacionNegociosDisciplinaDTO calificacionNegociosDisciplinaDTO = dashboardDAOImpl002.selectRowsCalificacionNegociosDisciplina(new CalificacionNegociosDisciplinaDTO(cumplimientoPuntajeNacionalNegocio.getFiltros()));
        if (calificacionNegociosDisciplinaDTO != null) {
            List<com.gs.baz.frq.dashboard.dto.graficos.g002.NegocioDTO> calificacionNegocios = calificacionNegociosDisciplinaDTO.getNegocios();
            negocios.forEach(item -> {
                com.gs.baz.frq.dashboard.dto.graficos.g002.NegocioDTO negocio = calificacionNegocios.stream().filter(calificacionNegocioDTO -> calificacionNegocioDTO.getIdNegocio().equals(item.getIdNegocio())).collect(Collectors.toList()).get(0);
                item.setPuntaje(negocio.getCalificacion());
            });
        }
        cumplimientoPuntajeNegocioNacionalDTO.setNegocios(negocios);
        return cumplimientoPuntajeNegocioNacionalDTO;
    }

}
