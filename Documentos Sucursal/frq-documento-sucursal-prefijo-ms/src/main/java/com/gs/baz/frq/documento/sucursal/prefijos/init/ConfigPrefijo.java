package com.gs.baz.frq.documento.sucursal.prefijos.init;

import com.gs.baz.frq.documento.sucursal.prefijos.dao.PrefijoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.prefijos")
public class ConfigPrefijo {

    private final Logger logger = LogManager.getLogger();

    public ConfigPrefijo() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public PrefijoDAOImpl prefijoDAOImpl() {
        return new PrefijoDAOImpl();
    }

}
