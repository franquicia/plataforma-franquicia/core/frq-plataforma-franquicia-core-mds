/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos base de semaforo", value = "SemaforoBase")
public class SemaforoBase {

    @JsonProperty(value = "clave")
    @ApiModelProperty(notes = "Clave del semaforo", example = "semaforo-oficial")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String clave;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del semaforo", example = "Semaforo")
    private String descripcion;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "SemaforoBase{" + "clave=" + clave + ", descripcion=" + descripcion + '}';
    }

}
