/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.reportes;

import com.gs.baz.frq.dashboard.certificacion.dao.reportes.DashboardCertificacionReporteCoberturaDAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.reportes.DashboardCertificacionReporteCoberturaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/reportes/")
public class ServiceDashboardCertificacionReporteCobertura {

    @Autowired
    private DashboardCertificacionReporteCoberturaDAOImpl dashboardCertificacionReporteCoberturaDAOImpl;

    @RequestMapping(value = "secure/cobertura", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionReporteCoberturaDTO> getDatosReporteCobertura() throws CustomException {
        return dashboardCertificacionReporteCoberturaDAOImpl.selectRowsDashboardCertificacionReporteCob();
    }

    @RequestMapping(value = "secure/cobertura/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionReporteCoberturaDTO> getDatoReporteCobertura(
            @RequestParam(value = "fechaInicial", required = true) String fechaInicial,
            @RequestParam(value = "fechaFinal", required = true) String fechaFinal,
            @RequestParam(value = "idNegocio", required = true) Long idNegocio,
            @RequestParam(value = "idGerente", required = false) Long idGerente) throws CustomException {

        return dashboardCertificacionReporteCoberturaDAOImpl.selectRowDashboardCertificacionReporteCob(fechaInicial, fechaFinal, idNegocio, idGerente);
    }

}
