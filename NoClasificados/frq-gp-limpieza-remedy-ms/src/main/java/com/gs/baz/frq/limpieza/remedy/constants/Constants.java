/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.constants;

import com.gs.baz.frq.limpieza.remedy.util.Ambientes;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author leodan1991
 */
public class Constants {

    public static final String URL_INCIDENTES_REMEDY_LIMPIEZA_DESARROLLO = "http://10.54.68.156/Arsys/QALimpieza/Service.svc";
    public static final String URL_INCIDENTES_REMEDY_LIMPIEZA_PRODUCCION = "http://10.54.21.38/Arsys/WCFLimpieza/Service.svc";

    private static final Logger LOGGER = LogManager.getLogger();

    public Constants() {
        super();

    }

    public static String getURLIncidentesRemedyLimpieza() {
        String server = URL_INCIDENTES_REMEDY_LIMPIEZA_DESARROLLO;
        if (Ambientes.AMBIENTE == Ambientes.PRODUCTIVO) {
            server = URL_INCIDENTES_REMEDY_LIMPIEZA_PRODUCCION;
        }
        return server;
    }

    public static String getIPLocal() {
        String localIP = null;
        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(nets)) {
                if (!netint.isLoopback()) {
                    Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                    for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                        if (!inetAddress.getHostAddress().contains(":")) {
                            localIP = inetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            LOGGER.info(ex);
        }
        if (localIP == null) {
            localIP = "127.0.0.1";
        }
        return localIP;
    }

}
