/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.server.logs.appender;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author karlo
 */
public class ApplicationThrowEvent {

    private ApplicationListener contextEventListener;

    public ApplicationThrowEvent(ApplicationListener contextEventListener) {
        this.contextEventListener = contextEventListener;
    }

    public void registerEventListener(ApplicationListener contextEventListener) {
        this.contextEventListener = contextEventListener;
    }

    public void waintingForLoadComplete() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ApplicationThrowEvent.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (ApplicationListenerImpl.isContextLoaded) {
                        break;
                    }
                }
                if (ApplicationListenerImpl.isContextLoaded) {
                    contextEventListener.onLoadedEvent();
                }
            }
        }).start();
    }

}
