/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisciplinaEjeDTO {

    @JsonProperty(value = "id_disciplina_eje")
    private Integer idDisciplinaEje;

    @JsonProperty(value = "id_eje_modelo")
    private Integer idModeloEje;

    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    public DisciplinaEjeDTO() {
    }

    public Integer getIdDisciplinaEje() {
        return idDisciplinaEje;
    }

    public void setIdDisciplinaEje(Integer idDisciplinaEje) {
        this.idDisciplinaEje = idDisciplinaEje;
    }

    public Integer getIdModeloEje() {
        return idModeloEje;
    }

    public void setIdModeloEje(Integer idModeloEje) {
        this.idModeloEje = idModeloEje;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    @Override
    public String toString() {
        return "DisciplinaEjeDTO{" + "idDisciplinaEje=" + idDisciplinaEje + ", idModeloEje=" + idModeloEje + ", idDisciplina=" + idDisciplina + '}';
    }

}
