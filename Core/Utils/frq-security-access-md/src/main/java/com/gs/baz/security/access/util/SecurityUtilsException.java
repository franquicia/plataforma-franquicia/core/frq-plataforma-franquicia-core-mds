/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.util;

/**
 *
 * @author cescobarh
 */
public class SecurityUtilsException extends Exception {

    public SecurityUtilsException(String message) {
        super(message);
    }

    public SecurityUtilsException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityUtilsException(Throwable cause) {
        super(cause);
    }

}
