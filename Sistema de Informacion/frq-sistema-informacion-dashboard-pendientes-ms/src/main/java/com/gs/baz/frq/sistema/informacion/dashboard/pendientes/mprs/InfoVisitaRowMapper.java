/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CalificacionCeco;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.InformacionVisita;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class InfoVisitaRowMapper implements RowMapper<InformacionVisita> {

    @Override
    public InformacionVisita mapRow(ResultSet rs, int rowNum) throws SQLException {
        InformacionVisita informacionVisita = new InformacionVisita();
        informacionVisita.setCentroCostosBase(rs.getString("CECOBASE_ID"));
        informacionVisita.setCentroCostos(rs.getString("FCID_CECO"));
        informacionVisita.setNombreCeco(rs.getString("NOMBRE_CECO"));
        informacionVisita.setFechaVisita(rs.getString("FECHA_VISITA"));
        informacionVisita.setNumeroEmpleado(rs.getInt("NUMERO_EMPLEADO"));
        informacionVisita.setNombreEmpleado(rs.getString("NOMBRE_EMPLEADO"));
        return informacionVisita;
    }
}
