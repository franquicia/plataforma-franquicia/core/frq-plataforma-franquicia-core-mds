/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Reportes {

    @JsonProperty(value = "disciplinas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Disciplinas")
    private List<Disciplina> disciplinas;

    @JsonProperty(value = "calificacionesPromedio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Calificaciones promedio")
    private List<CalificacionPromedio> calificacionesPromedio;

    @JsonProperty(value = "nivelesGeograficos")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Niveles geograficos")
    private List<NivelGeografico> nivelesGeograficos;

    @JsonProperty(value = "negocios")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Negocios")
    private List<Negocio> negocios;

    @JsonProperty(value = "negociosDisciplina")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Negocios de una Disciplina")
    private List<NegocioDisciplina> negociosDisciplina;

    @JsonProperty(value = "porcentajePuntosVentaAprobados")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Porcentaje de puntos de venta aprobados")
    private List<PorcentajePuntoVentaAprobado> porcentajePuntosVentaAprobados;

    @JsonProperty(value = "nivelCumplimientoTotal")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Nivel de cumplimiento total")
    private NivelCumplimientoTotal nivelCumplimientoTotal;

    @JsonProperty(value = "calificacionesGeografia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Calificacion por geografia")
    private List<CalificacionGeografia> calificacionesGeografia;

    @JsonProperty(value = "puntosVentaAprobadosGeografia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Puntos de venta aprobados por geografia")
    private List<PuntoVentaAprobadoGeografia> puntosVentaAprobadosGeografia;

    @JsonProperty(value = "incumplimientos")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Incumplimientos")
    private List<Incumplimiento> incumplimientos;

    @JsonProperty(value = "imperdonableTotal")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "ImperdonableTotal")
    private ImperdonableTotal imperdonableTotal;

    @JsonProperty(value = "calificacionGeografiaRedUnica")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Calificacion por geografia de red unica")
    private CalificacionGeografiaRedUnica calificacionGeografiaRedUnica;

    @JsonProperty(value = "puntoVentaAprobadoGeografiaRedUnica")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Punto de venta aprobado por geografia red unica")
    private PuntoVentaAprobadoGeografiaRedUnica puntoVentaAprobadoGeografiaRedUnica;

    @JsonProperty(value = "incumplimientosNegocioRedUnica")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Incumplimientos red unica")
    private List<IncumplimientoNegocioRedUnica> incumplimientosNegocioRedUnica;

    @JsonProperty(value = "incumplimientosSeccionResponsableRedUnica")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Incumplimientos seccion responsable red unica")
    private List<IncumplimientoSeccionResponsableRedUnica> incumplimientosSeccionResponsableRedUnica;

    @JsonProperty(value = "negociosIncumplimiento")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Negocios incumplimiento")
    private List<NegocioIncumplimiento> negociosIncumplimiento;

    @JsonProperty(value = "seccionesIncumplimiento")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Secciones incumplimiento")
    private List<SeccionIncumplimiento> seccionesIncumplimiento;

    @JsonProperty(value = "responsablesIncumplimiento")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Responsables incumplimiento")
    private List<ResponsableIncumplimiento> responsablesIncumplimiento;

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<CalificacionPromedio> getCalificacionesPromedio() {
        return calificacionesPromedio;
    }

    public void setCalificacionesPromedio(List<CalificacionPromedio> calificacionesPromedio) {
        this.calificacionesPromedio = calificacionesPromedio;
    }

    public List<NivelGeografico> getNivelesGeograficos() {
        return nivelesGeograficos;
    }

    public void setNivelesGeograficos(List<NivelGeografico> nivelesGeograficos) {
        this.nivelesGeograficos = nivelesGeograficos;
    }

    public List<Negocio> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<Negocio> negocios) {
        this.negocios = negocios;
    }

    public List<NegocioDisciplina> getNegociosDisciplina() {
        return negociosDisciplina;
    }

    public void setNegociosDisciplina(List<NegocioDisciplina> negociosDisciplina) {
        this.negociosDisciplina = negociosDisciplina;
    }

    public List<PorcentajePuntoVentaAprobado> getPorcentajePuntosVentaAprobados() {
        return porcentajePuntosVentaAprobados;
    }

    public void setPorcentajePuntosVentaAprobados(List<PorcentajePuntoVentaAprobado> porcentajePuntosVentaAprobados) {
        this.porcentajePuntosVentaAprobados = porcentajePuntosVentaAprobados;
    }

    public NivelCumplimientoTotal getNivelCumplimientoTotal() {
        return nivelCumplimientoTotal;
    }

    public void setNivelCumplimientoTotal(NivelCumplimientoTotal nivelCumplimientoTotal) {
        this.nivelCumplimientoTotal = nivelCumplimientoTotal;
    }

    public List<CalificacionGeografia> getCalificacionesGeografia() {
        return calificacionesGeografia;
    }

    public void setCalificacionesGeografia(List<CalificacionGeografia> calificacionesGeografia) {
        this.calificacionesGeografia = calificacionesGeografia;
    }

    public List<PuntoVentaAprobadoGeografia> getPuntosVentaAprobadosGeografia() {
        return puntosVentaAprobadosGeografia;
    }

    public void setPuntosVentaAprobadosGeografia(List<PuntoVentaAprobadoGeografia> puntosVentaAprobadosGeografia) {
        this.puntosVentaAprobadosGeografia = puntosVentaAprobadosGeografia;
    }

    public List<Incumplimiento> getIncumplimientos() {
        return incumplimientos;
    }

    public void setIncumplimientos(List<Incumplimiento> incumplimientos) {
        this.incumplimientos = incumplimientos;
    }

    public ImperdonableTotal getImperdonableTotal() {
        return imperdonableTotal;
    }

    public void setImperdonableTotal(ImperdonableTotal imperdonableTotal) {
        this.imperdonableTotal = imperdonableTotal;
    }

    public CalificacionGeografiaRedUnica getCalificacionGeografiaRedUnica() {
        return calificacionGeografiaRedUnica;
    }

    public void setCalificacionGeografiaRedUnica(CalificacionGeografiaRedUnica calificacionGeografiaRedUnica) {
        this.calificacionGeografiaRedUnica = calificacionGeografiaRedUnica;
    }

    public PuntoVentaAprobadoGeografiaRedUnica getPuntoVentaAprobadoGeografiaRedUnica() {
        return puntoVentaAprobadoGeografiaRedUnica;
    }

    public void setPuntoVentaAprobadoGeografiaRedUnica(PuntoVentaAprobadoGeografiaRedUnica puntoVentaAprobadoGeografiaRedUnica) {
        this.puntoVentaAprobadoGeografiaRedUnica = puntoVentaAprobadoGeografiaRedUnica;
    }

    public List<IncumplimientoNegocioRedUnica> getIncumplimientosNegocioRedUnica() {
        return incumplimientosNegocioRedUnica;
    }

    public void setIncumplimientosNegocioRedUnica(List<IncumplimientoNegocioRedUnica> incumplimientosNegocioRedUnica) {
        this.incumplimientosNegocioRedUnica = incumplimientosNegocioRedUnica;
    }

    public List<IncumplimientoSeccionResponsableRedUnica> getIncumplimientosSeccionResponsableRedUnica() {
        return incumplimientosSeccionResponsableRedUnica;
    }

    public void setIncumplimientosSeccionResponsableRedUnica(List<IncumplimientoSeccionResponsableRedUnica> incumplimientosSeccionResponsableRedUnica) {
        this.incumplimientosSeccionResponsableRedUnica = incumplimientosSeccionResponsableRedUnica;
    }

    public List<NegocioIncumplimiento> getNegociosIncumplimiento() {
        return negociosIncumplimiento;
    }

    public void setNegociosIncumplimiento(List<NegocioIncumplimiento> negociosIncumplimiento) {
        this.negociosIncumplimiento = negociosIncumplimiento;
    }

    public List<SeccionIncumplimiento> getSeccionesIncumplimiento() {
        return seccionesIncumplimiento;
    }

    public void setSeccionesIncumplimiento(List<SeccionIncumplimiento> seccionesIncumplimiento) {
        this.seccionesIncumplimiento = seccionesIncumplimiento;
    }

    public List<ResponsableIncumplimiento> getResponsablesIncumplimiento() {
        return responsablesIncumplimiento;
    }

    public void setResponsablesIncumplimiento(List<ResponsableIncumplimiento> responsablesIncumplimiento) {
        this.responsablesIncumplimiento = responsablesIncumplimiento;
    }

}
