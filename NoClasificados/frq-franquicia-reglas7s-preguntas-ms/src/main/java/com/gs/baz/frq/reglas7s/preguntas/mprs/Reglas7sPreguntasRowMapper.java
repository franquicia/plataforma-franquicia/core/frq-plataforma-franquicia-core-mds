package com.gs.baz.frq.reglas7s.preguntas.mprs;

import com.gs.baz.frq.reglas7s.preguntas.dto.Reglas7sPreguntasDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class Reglas7sPreguntasRowMapper implements RowMapper<Reglas7sPreguntasDTO> {

    private Reglas7sPreguntasDTO status;

    @Override
    public Reglas7sPreguntasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new Reglas7sPreguntasDTO();
        status.setIdClave(((BigDecimal) rs.getObject("FICLAVE_ID")));
        status.setIdPreguntaCorresponde(((BigDecimal) rs.getObject("FIPREGUNTACORRE")));
        status.setIdProtocolo(((BigDecimal) rs.getObject("FIPROTOCOLO")));
        return status;
    }
}
