/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto.SemaforoBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SemaforoRowMapper implements RowMapper<SemaforoBase> {

    @Override
    public SemaforoBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        SemaforoBase semaforoBase = new SemaforoBase();
        semaforoBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        semaforoBase.setClave(rs.getString("FCCLAVE"));
        return semaforoBase;
    }
}
