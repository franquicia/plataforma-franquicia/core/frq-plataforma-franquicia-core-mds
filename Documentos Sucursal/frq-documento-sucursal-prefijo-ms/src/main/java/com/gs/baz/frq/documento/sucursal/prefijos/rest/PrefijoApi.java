/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.rest;

import com.gs.baz.frq.documento.sucursal.prefijos.dao.PrefijoDAOImpl;
import com.gs.baz.frq.documento.sucursal.prefijos.dto.AltaPrefijo;
import com.gs.baz.frq.documento.sucursal.prefijos.dto.ParametrosPrefijo;
import com.gs.baz.frq.documento.sucursal.prefijos.dto.PrefijoBase;
import com.gs.baz.frq.documento.sucursal.prefijos.dto.Prefijos;
import com.gs.baz.frq.documento.sucursal.prefijos.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "prefijos", value = "prefijos", description = "Api para la gestión del catalogo de prefijos")
@RestController
@RequestMapping("/api-local/documento-sucursal/prefijo/v1")
public class PrefijoApi {

    @Autowired
    private PrefijoDAOImpl prefijoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idPrefijo
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene prefijo", notes = "Obtiene un prefijo", nickname = "obtienePrefijo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idPrefijo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PrefijoBase obtienePrefijo(@ApiParam(name = "idPrefijo", value = "Identificador del prefijo", example = "1", required = true) @PathVariable("idPrefijo") Long idPrefijo) throws CustomException, DataNotFoundException {
        PrefijoBase prefijoBase = prefijoDAOImpl.selectRow(idPrefijo);
        if (prefijoBase == null) {
            throw new DataNotFoundException();
        }
        return prefijoBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene prefijos", notes = "Obtiene todos los prefijos", nickname = "obtienePrefijos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Prefijos obtienePrefijos() throws CustomException, DataNotFoundException {
        Prefijos prefijos = new Prefijos(prefijoDAOImpl.selectAllRows());
        if (prefijos.getPrefijos() != null && prefijos.getPrefijos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return prefijos;
    }

    /**
     *
     * @param parametrosPrefijo
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear prefijo", notes = "Agrega un prefijo", nickname = "creaPrefijo")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaPrefijo creaPrefijo(@ApiParam(name = "ParametrosPrefijo", value = "Paramentros para el alta del prefijo", required = true) @RequestBody ParametrosPrefijo parametrosPrefijo) throws DataNotInsertedException {
        return prefijoDAOImpl.insertRow(parametrosPrefijo);
    }

    /**
     *
     * @param parametrosPrefijo
     * @param idPrefijo
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar prefijo", notes = "Actualiza un prefijo", nickname = "actualizaPrefijo")
    @RequestMapping(value = "/{idPrefijo}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaPrefijo(@ApiParam(name = "ParametrosPrefijo", value = "Paramentros para la actualización del prefijo", required = true) @RequestBody ParametrosPrefijo parametrosPrefijo, @ApiParam(name = "idPrefijo", value = "Identificador del prefijo", example = "1", required = true) @PathVariable("idPrefijo") Integer idPrefijo) throws DataNotUpdatedException {
        parametrosPrefijo.setIdNegocioPrefijo(idPrefijo);
        prefijoDAOImpl.updateRow(parametrosPrefijo);
        return new SinResultado();
    }

    /**
     *
     * @param idPrefijo
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar prefijo", notes = "Elimina un item de los prefijos", nickname = "eliminaPrefijo")
    @RequestMapping(value = "/{idPrefijo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaPrefijo(@ApiParam(name = "idPrefijo", value = "Identificador del prefijo", example = "1", required = true) @PathVariable("idPrefijo") Integer idPrefijo) throws DataNotDeletedException {
        prefijoDAOImpl.deleteRow(idPrefijo);
        return new SinResultado();
    }

}
