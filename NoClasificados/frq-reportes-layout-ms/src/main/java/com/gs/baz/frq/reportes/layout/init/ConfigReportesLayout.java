package com.gs.baz.frq.reportes.layout.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gs.baz.frq.reportes.layout.dao.ReportesLayoutDAOImpl;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.reportes.layout")
public class ConfigReportesLayout {

    private final Logger logger = LogManager.getLogger();

    public ConfigReportesLayout() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ReportesLayoutDAOImpl reportesLayoutDAOImpl() {
        return new ReportesLayoutDAOImpl();
    }

}
