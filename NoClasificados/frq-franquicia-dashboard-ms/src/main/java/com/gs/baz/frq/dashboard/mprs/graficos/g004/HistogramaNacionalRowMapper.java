package com.gs.baz.frq.dashboard.mprs.graficos.g004;

import com.gs.baz.frq.dashboard.dto.graficos.g004.HistogramaNacionalDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class HistogramaNacionalRowMapper implements RowMapper<HistogramaNacionalDTO> {

    private HistogramaNacionalDTO histogramaNacionalDTO;

    @Override
    public HistogramaNacionalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        histogramaNacionalDTO = new HistogramaNacionalDTO();
        histogramaNacionalDTO.setFrecuencia((BigDecimal) rs.getObject("FRECUENCIA"));
        histogramaNacionalDTO.setCalificacionFinal((BigDecimal) rs.getObject("CALIFICACION"));
        return histogramaNacionalDTO;
    }
}
