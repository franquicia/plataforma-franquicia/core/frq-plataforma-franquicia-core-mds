/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.formato.rest;

import com.gs.baz.formato.dao.FormatoDAOImpl;
import com.gs.baz.formato.dto.FormatoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/formato")
@Component("FRQServiceFormato")
public class ServiceFormato {

    @Autowired
    private FormatoDAOImpl formatoDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FormatoDTO> getFormatos() throws CustomException {
        return formatoDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_formato}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FormatoDTO getFormato(@PathVariable("id_formato") Long idFormato) throws CustomException {
        return formatoDAOImpl.selectRow(idFormato);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FormatoDTO postFormato(@RequestBody FormatoDTO formato) throws CustomException {
        if (formato.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return formatoDAOImpl.insertRow(formato);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public FormatoDTO putFormato(@RequestBody FormatoDTO formato) throws CustomException {
        if (formato.getIdFormato() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_formato"));
        }
        if (formato.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (formato.getIdTipoFormato() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_tipo_formato"));
        }
        return formatoDAOImpl.updateRow(formato);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FormatoDTO deleteFormato(@RequestBody FormatoDTO formato) throws CustomException {
        if (formato.getIdFormato() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_formato"));
        }
        return formatoDAOImpl.deleteRow(formato);
    }
}
