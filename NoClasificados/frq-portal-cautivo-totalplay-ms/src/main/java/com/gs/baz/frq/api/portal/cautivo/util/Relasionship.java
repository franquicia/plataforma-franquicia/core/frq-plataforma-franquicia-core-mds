/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.util;

/**
 *
 * @author MADA
 */
public enum Relasionship {

    APP(":9354"),
    CONEXIONES(":9354"),
    IDENTIFICADOR(":9345");

    private final String urlService;

    private Relasionship(String urlService) {
        this.urlService = urlService;
    }

    /**
     *
     * @param path
     * @return
     */
    public String url(String path) {
        return path + urlService;
    }

}
