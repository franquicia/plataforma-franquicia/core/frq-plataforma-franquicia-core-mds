package com.gs.baz.frq.regla7s.mprs;

import com.gs.baz.frq.regla7s.dto.Regla7SDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class Regla7SRowMapper implements RowMapper<Regla7SDTO> {

    private Regla7SDTO status;

    @Override
    public Regla7SDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new Regla7SDTO();
        status.setIdClave(((BigDecimal) rs.getObject("FICLAVE_ID")));
        status.setPreguntaBase(((BigDecimal) rs.getObject("FIPREGUNTA_BASE")));
        status.setProtocolo(((BigDecimal) rs.getObject("FIPROTOCOLO")));
        status.setPonderacion(((BigDecimal) rs.getObject("FIPONDERACION")));
        status.setIncumplimiento(((BigDecimal) rs.getObject("FINCUMPLIMIENTO")));
        status.setReglaNueve(((BigDecimal) rs.getObject("FIREGLANUEVE")));
        status.setIdNegocioDisciplina(((BigDecimal) rs.getObject("FINEGOCIODISCID")));
        status.setModulo(rs.getString("FCMODULO"));
        status.setImperdonable(((BigDecimal) rs.getObject("FIMPERDONABLE")));
        status.setOportunidad(((BigDecimal) rs.getObject("FIOPORTUNIDAD")));
        return status;
    }
}
