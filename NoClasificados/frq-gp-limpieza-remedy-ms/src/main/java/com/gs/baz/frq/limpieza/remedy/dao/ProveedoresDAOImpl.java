package com.gs.baz.frq.limpieza.remedy.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.limpieza.remedy.dto.ProveedoresDTO;
import com.gs.baz.frq.limpieza.remedy.mprs.ProveedoresRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProveedoresDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsertaProveedor;
    private DefaultJdbcCall jdbcInsertaProveedorLimpieza;
    private DefaultJdbcCall jdbcEliminaProveedor;
    private DefaultJdbcCall jdbcBuscaProveedor;
    private DefaultJdbcCall jdbcBuscaProveedores;
    private DefaultJdbcCall jdbcBuscaProveedoresLimpieza;
    private DefaultJdbcCall jdbcActualizaProveedor;
    private DefaultJdbcCall jdbcDepuraProveedores;

    private List<ProveedoresDTO> listaDetU;
    private final Logger LOGGER = LogManager.getLogger();

    public ProveedoresDAOImpl() {
        super();
    }

    @SuppressWarnings("all")
    public void init() {

        jdbcInsertaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_INS_PROVEED");

        jdbcInsertaProveedorLimpieza = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_INS_PROVEEDLIMPIEZA");

        jdbcEliminaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_DEL_PROVEED");

        jdbcBuscaProveedores = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_PROVEEDORES", new ProveedoresRowMapper());

        jdbcBuscaProveedoresLimpieza = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_SEL_DETALLE_LIMPIEZA")
                .returningResultSet("RCL_PROVEEDORES", new ProveedoresRowMapper());

        jdbcBuscaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_PROVEEDORES", new ProveedoresRowMapper());

        jdbcActualizaProveedor = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_ACT_PROVEED");

        jdbcDepuraProveedores = (DefaultJdbcCall) new DefaultJdbcCall(this.gtnJdbcTemplate)
                .withSchemaName("GESTION")
                .withCatalogName("PAADMPROVEEDORS")
                .withProcedureName("SP_DEPURA_PROVE");

    }

    @SuppressWarnings("all")
    public int inserta(ProveedoresDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_MENU", bean.getMenu())
                .addValue("PA_IDPROVEED", bean.getIdProveedor())
                .addValue("PA_NOMBCORTO", bean.getNombreCorto())
                .addValue("PA_RAZONSOC", bean.getRazonSocial())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertaProveedor.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_INS_PROVEED}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al insertar el evento");
        }

        return idNota;
    }

    @SuppressWarnings("all")
    public int insertaLimpieza(ProveedoresDTO bean) throws Exception {
        int respuesta = 0;
        int idNota = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_MENU", bean.getMenu())
                .addValue("PA_IDPROVEED", bean.getIdProveedor())
                .addValue("PA_NOMBCORTO", bean.getNombreCorto())
                .addValue("PA_RAZONSOC", bean.getRazonSocial())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_ID_TIPO_PROV", 1);

        out = jdbcInsertaProveedorLimpieza.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_INS_PROVEED}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al insertar el evento");
        }

        return idNota;
    }

    @SuppressWarnings("all")
    public boolean elimina(String idProveedor) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPROVEED", idProveedor);

        out = jdbcEliminaProveedor.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_DEL_PROVEED}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        if (respuesta != 1) {
            LOGGER.info("Algo paso al elimnar el evento ");
        } else {
            return true;
        }

        return false;
    }

    //con un parametro
    @SuppressWarnings("all")
    public List<ProveedoresDTO> obtieneDatos(int idProveedor) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPROVEED", idProveedor);

        out = jdbcBuscaProveedor.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_SEL_PARAM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        listaDetU = (List<ProveedoresDTO>) out.get("RCL_PROVEEDORES");

        if (respuesta != 1) {
            LOGGER.info("Algo paso al consular la incidencia ");
        }

        return listaDetU;
    }
    //Sin ningun parametro

    @SuppressWarnings("all")
    public List<ProveedoresDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<ProveedoresDTO> lista = null;
        int error = 0;

        out = jdbcBuscaProveedores.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_SEL_DETALLE}");
        listaDetU = (List<ProveedoresDTO>) out.get("RCL_PROVEEDORES");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            LOGGER.info("Algo paso al obtener los proveedores");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    //Sin ningun parametro
    @SuppressWarnings("all")
    public List<ProveedoresDTO> obtieneInfoLimpieza() throws Exception {
        Map<String, Object> out = null;
        List<ProveedoresDTO> lista = null;
        int error = 0;

        out = jdbcBuscaProveedoresLimpieza.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_SEL_DETALLE_LIMPIEZA}");
        listaDetU = (List<ProveedoresDTO>) out.get("RCL_PROVEEDORES");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            LOGGER.info("Algo paso al obtener los proveedores de Limpieza");
        } else {
            return listaDetU;
        }

        return listaDetU;
    }

    @SuppressWarnings("all")
    public boolean actualiza(ProveedoresDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_MENU", bean.getMenu())
                .addValue("PA_IDPROVEED", bean.getIdProveedor())
                .addValue("PA_NOMBCORTO", bean.getNombreCorto())
                .addValue("PA_RAZONSOC", bean.getRazonSocial())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcActualizaProveedor.execute(in);

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_ACT_PROVEED}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = resultado.intValue();

        // el 1 es el valor de salida de PA_EJECUCION
        if (respuesta != 1) {
            LOGGER.info("Algo paso al actualizar el evento ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("all")
    public boolean depura() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcDepuraProveedores.execute();

        LOGGER.info("Funcion ejecutada: {GESTION.PAADMPROVEEDORS.SP_DEPURA_PROVE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            LOGGER.info("Algo paso al depurar la tabla de Proveedores");
        } else {
            return true;
        }

        return false;
    }

}
