/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta de la Visita", value = "RespuestaVisita")
public class RespuestaVisita {

    @JsonProperty(value = "idVisita")
    @ApiModelProperty(notes = "Identificador de la visita", example = "2500650", position = -1)
    private Integer idVisita;

    @JsonProperty(value = "idPregunta")
    @ApiModelProperty(notes = "Identificador de la pregunta", example = "81591", position = -2)
    private Integer idPregunta;
    
    @JsonProperty(value = "descripcionPregunta")
    @ApiModelProperty(notes = "Descripción de la pregunta", example = "¿Los cascos del personal de la oficina se encuentran sin abolladura y sin fisuras?", position = -3)
    private String descripcionPregunta;
    
    @JsonProperty(value = "respuesta")
    @ApiModelProperty(notes = "Respuesta a la pregunta", example = "SI", position = -4)
    private String respuesta;
    
    @JsonProperty(value = "negocio")
    @ApiModelProperty(notes = "Clave del negocio de la disciplina", example = "4", position = -5)
    private Integer negocio;
    
    @JsonProperty(value = "ponderacion")
    @ApiModelProperty(notes = "Puntaje ponderado de la pregunta para la calificación", example = "5", position = -6)
    private Integer ponderacion;
    
    @JsonProperty(value = "modulo")
    @ApiModelProperty(notes = "Modulo al que pertenece la pregunta", example = "I. Señalizaciones de seguridad", position = -7)
    private String modulo;
    
    @JsonProperty(value = "incumplimiento")
    @ApiModelProperty(notes = "Indicador de incumplimiento", example = "0", position = -6)
    private Integer incumplimiento;
    
    @JsonProperty(value = "idCuestionario")
    @ApiModelProperty(notes = "Identificador del cuestionario", example = "3802", position = -7)
    private Integer idChecklist;

    @JsonProperty(value = "imperdonable")
    @ApiModelProperty(notes = "Identificador de imperdonable", example = "0", position = -7)
    private Integer imperdonable;

    @JsonProperty(value = "idRespuesta")
    @ApiModelProperty(notes = "Identificador de la respuesta", example = "23101484", position = -5)
    private Integer idRespuesta;
    
    @JsonProperty(value = "tipoEvidencia")
    @ApiModelProperty(notes = "Identificador del tipo de evidencia", example = "21", position = -7)
    private Integer tipoEvidencia;
    
    @JsonProperty(value = "rutaEvidencia")
    @ApiModelProperty(notes = "Ruta donde está almacenada la evidencia", example = "/franquicia/imagenes/2020/196228_148143_400_ORDEN_1_20200716143806.jpg", position = -7)
    private String rutaEvidencia;
    
    @JsonProperty(value = "observaciones")
    @ApiModelProperty(notes = "Observaciones de la evidencia o pregunta", example = "", position = -7)
    private String observaciones;

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(Integer idVisita) {
        this.idVisita = idVisita;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getDescripcionPregunta() {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta) {
        this.descripcionPregunta = descripcionPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getNegocio() {
        return negocio;
    }

    public void setNegocio(Integer negocio) {
        this.negocio = negocio;
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(Integer ponderacion) {
        this.ponderacion = ponderacion;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getIncumplimiento() {
        return incumplimiento;
    }

    public void setIncumplimiento(Integer incumplimiento) {
        this.incumplimiento = incumplimiento;
    }

    public Integer getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(Integer idChecklist) {
        this.idChecklist = idChecklist;
    }

    public Integer getImperdonable() {
        return imperdonable;
    }

    public void setImperdonable(Integer imperdonable) {
        this.imperdonable = imperdonable;
    }

    public Integer getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Integer idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public Integer getTipoEvidencia() {
        return tipoEvidencia;
    }

    public void setTipoEvidencia(Integer tipoEvidencia) {
        this.tipoEvidencia = tipoEvidencia;
    }

    public String getRutaEvidencia() {
        return rutaEvidencia;
    }

    public void setRutaEvidencia(String rutaEvidencia) {
        this.rutaEvidencia = rutaEvidencia;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return "RespuestaVisita{" + "idVisita=" + idVisita + ", idPregunta=" + idPregunta + ", descripcionPregunta=" + descripcionPregunta + ", respuesta=" + respuesta + ", negocio=" + negocio + ", ponderacion=" + ponderacion + ", modulo=" + modulo + ", incumplimiento=" + incumplimiento + ", idChecklist=" + idChecklist + ", imperdonable=" + imperdonable + ", idRespuesta=" + idRespuesta + ", tipoEvidencia=" + tipoEvidencia + ", rutaEvidencia=" + rutaEvidencia + ", observaciones=" + observaciones + '}';
    }

}
