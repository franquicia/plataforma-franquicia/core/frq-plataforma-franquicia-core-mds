/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.accesos.sistema.vistas.dao.AccesosSistemaVistasDAOImpl;
import com.gs.baz.frq.accesos.sistema.vistas.dto.AltaVista;
import com.gs.baz.frq.accesos.sistema.vistas.dto.ParametrosPadreVista;
import com.gs.baz.frq.accesos.sistema.vistas.dto.VistaBase;
import com.gs.baz.frq.accesos.sistema.vistas.dto.Vistas;
import com.gs.baz.frq.accesos.sistema.vistas.dto.ParametrosVista;
import com.gs.baz.frq.accesos.sistema.vistas.dto.SinResultado;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "accesos", value = "accesos", description = "Api para la gestión del catalogo de vistas")
@RestController
@RequestMapping("/api-local/accesos/sistema/vistas/v1")
public class AccesosSistemaVistasApi {

    @Autowired
    private AccesosSistemaVistasDAOImpl accesosSistemaVistasDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idVista
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene vista", notes = "Obtiene una vista", nickname = "obtieneVista")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idVista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public VistaBase obtieneVista(@ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Long idVista) throws CustomException, DataNotFoundException {
        VistaBase vistaBase = accesosSistemaVistasDAOImpl.selectRow(idVista);
        if (vistaBase == null) {
            throw new DataNotFoundException();
        }
        return vistaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene vistas", notes = "Obtiene todas las vistas", nickname = "obtieneVistas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Vistas obtieneVistas() throws CustomException, DataNotFoundException {
        Vistas vistas = new Vistas(accesosSistemaVistasDAOImpl.selectAllRows());
        if (vistas.getVistas() != null && vistas.getVistas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return vistas;
    }

    /**
     *
     * @param parametrosVista
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear vista", notes = "Agrega una vista", nickname = "creaVista")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaVista creaVista(@ApiParam(name = "ParametrosVista", value = "Paramentros para el alta de la vista", required = true) @RequestBody ParametrosVista parametrosVista) throws DataNotInsertedException {
        return accesosSistemaVistasDAOImpl.insertRow(parametrosVista);
    }

    /**
     *
     * @param parametrosVista
     * @param idVista
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar vista", notes = "Actualiza una vista", nickname = "actualizaVista")
    @RequestMapping(value = "/{idVista}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaVista(@ApiParam(name = "ParametrosVista", value = "Paramentros para la actualización de la vista", required = true) @RequestBody ParametrosVista parametrosVista, @ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotUpdatedException {
        parametrosVista.setIdVista(idVista);
        accesosSistemaVistasDAOImpl.updateRow(parametrosVista);
        return new SinResultado();
    }

    /**
     *
     * @param idVista
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar vista", notes = "Elimina un item de las vistas", nickname = "eliminaVista")
    @RequestMapping(value = "/{idVista}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaVista(@ApiParam(name = "idVista", value = "Identificador de la vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotDeletedException {
        accesosSistemaVistasDAOImpl.deleteRow(idVista);
        return new SinResultado();
    }

    /**
     *
     * @param parametrosPadreVista
     * @param idVista
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar padre de la vista", notes = "Actualiza una vista", nickname = "actualizaPadreVista")
    @RequestMapping(value = "/{idVista}/padre", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaPadreVista(@ApiParam(name = "ParametrosPadreVista", value = "Paramentros para la actualización del vista", required = true) @RequestBody ParametrosPadreVista parametrosPadreVista, @ApiParam(name = "idVista", value = "Identificador del vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotUpdatedException {
        parametrosPadreVista.setIdVista(idVista);
        accesosSistemaVistasDAOImpl.updatePadreVista(parametrosPadreVista);
        return new SinResultado();
    }

    /**
     *
     * @param idPerfil
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene vistas del perfil", notes = "Obtiene todas las vistas del perfil", nickname = "obtieneVistasPerfil")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/perfiles/{idPerfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Vistas obtieneVistasPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable("idPerfil") Integer idPerfil) throws CustomException, DataNotFoundException {
        Vistas vistas = new Vistas(accesosSistemaVistasDAOImpl.selectAllRowsVistasPerfil(idPerfil));
        if (vistas.getVistas() != null && vistas.getVistas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return vistas;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene vistas del perfil", notes = "Obtiene todas las vistas del empleado", nickname = "obtieneVistasPerfil")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Vistas obtieneVistasUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws CustomException, DataNotFoundException {
        Vistas vistas = new Vistas(accesosSistemaVistasDAOImpl.selectAllRowsVistasEmpleado(numeroEmpleado));
        if (vistas.getVistas() != null && vistas.getVistas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return vistas;
    }

    /**
     *
     * @param numeroEmpleado
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene vistas del perfil", notes = "Obtiene todas las vistas del empleado", nickname = "obtieneVistasPerfil")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/usuarios/{numeroEmpleado}/finales", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Vistas obtieneVistasFinalesUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del empleado", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws CustomException, DataNotFoundException {
        Vistas vistas = new Vistas(accesosSistemaVistasDAOImpl.selectAllRowsVistasFinalesEmpleado(numeroEmpleado));
        if (vistas.getVistas() != null && vistas.getVistas().isEmpty()) {
            throw new DataNotFoundException();
        }
        return vistas;
    }

}
