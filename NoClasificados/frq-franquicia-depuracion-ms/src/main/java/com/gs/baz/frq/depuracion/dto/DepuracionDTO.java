/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.depuracion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author luisgomez
 */
public class DepuracionDTO {

    @JsonProperty(value = "respuestas_eliminadas")
    private Integer respuestas_eliminadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "bitacoras_eliminadas")
    private Integer bitacoras_eliminadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejecucion")
    private Integer ejecucion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    public Integer getRespuestas_eliminadas() {
        return respuestas_eliminadas;
    }

    public void setRespuestas_eliminadas(BigDecimal respuestas_eliminadas) {
        this.respuestas_eliminadas = (respuestas_eliminadas == null ? null : respuestas_eliminadas.intValue());
    }

    public Integer getBitacoras_eliminadas() {
        return bitacoras_eliminadas;
    }

    public void setBitacoras_eliminadas(BigDecimal bitacoras_eliminadas) {
        this.bitacoras_eliminadas = (bitacoras_eliminadas == null ? null : bitacoras_eliminadas.intValue());
    }

    public Integer getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(BigDecimal ejecucion) {
        this.ejecucion = (ejecucion == null ? null : ejecucion.intValue());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "DepuracionDTO{" + "respuestas_eliminadas=" + respuestas_eliminadas + ", bitacoras_eliminadas=" + bitacoras_eliminadas + ", ejecucion=" + ejecucion + ", fecha=" + fecha + '}';
    }

}
