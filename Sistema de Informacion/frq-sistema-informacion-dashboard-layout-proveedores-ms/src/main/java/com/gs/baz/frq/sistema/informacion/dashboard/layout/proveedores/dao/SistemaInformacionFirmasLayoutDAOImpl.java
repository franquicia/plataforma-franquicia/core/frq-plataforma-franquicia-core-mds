/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dao;

import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.InsertaComentarioFirmas;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.InsertaFirmas;
import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs.CecosCertificadoresLayoutRowMapper;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionFirmasLayoutDAOImpl extends DefaultDAO {

   
    private DefaultJdbcCall jdbcGInsertaFirmas;
    private DefaultJdbcCall jdbcGInsertaComentarioFirmas;
    private DefaultJdbcCall jdbcObtieneInfoCecoCertificador;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMLYFIRMAS";
   
        jdbcGInsertaFirmas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcGInsertaFirmas.withSchemaName(schema);
        jdbcGInsertaFirmas.withCatalogName(catalogo);
        jdbcGInsertaFirmas.withProcedureName("SPINSLYFIRMAS");
        
        jdbcGInsertaComentarioFirmas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcGInsertaComentarioFirmas.withSchemaName(schema);
        jdbcGInsertaComentarioFirmas.withCatalogName(catalogo);
        jdbcGInsertaComentarioFirmas.withProcedureName("SPINSLYCOMENFIRMA");
        
        jdbcObtieneInfoCecoCertificador = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneInfoCecoCertificador.withSchemaName(schema);
        jdbcObtieneInfoCecoCertificador.withCatalogName(catalogo);
        jdbcObtieneInfoCecoCertificador.withProcedureName("SPCECOSXCERT");
        jdbcObtieneInfoCecoCertificador.returningResultSet("PA_CDATOS", new CecosCertificadoresLayoutRowMapper()); 

    }

  
     
     
    public Integer insertaFirmas(InsertaFirmas entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIJERARQUIA", entityDTO.getIdJerarquia());
            mapSqlParameterSource.addValue("PA_FIESTADOFIRMA", entityDTO.getEstadoFirma());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            Map<String, Object> out = jdbcGInsertaFirmas.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            int idFirmante = ((BigDecimal) out.get("PA_FIIDFIRMANTE")).intValue();
            
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } 
            return idFirmante;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
        public Integer insertaComentarioFirmas(InsertaComentarioFirmas entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDFIRMANTE", entityDTO.getIdFirmante());
            mapSqlParameterSource.addValue("PA_FCCOMENTARIO", entityDTO.getComentario());
            mapSqlParameterSource.addValue("PA_FDFECHACOMENT", entityDTO.getFechaComentario());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR1", entityDTO.getAuxiliarUno());
            mapSqlParameterSource.addValue("PA_FCAUXILIAR2", entityDTO.getAuxiliarDos());
            mapSqlParameterSource.addValue("PA_FIIDFOLIO", entityDTO.getIdFolio());
            Map<String, Object> out = jdbcGInsertaComentarioFirmas.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            int idComentario = ((BigDecimal) out.get("PA_FIIDCOMENTARIO")).intValue();
            
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } 
            return idComentario;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
