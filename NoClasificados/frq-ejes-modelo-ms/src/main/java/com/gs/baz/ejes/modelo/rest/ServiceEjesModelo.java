/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ejes.modelo.rest;

import com.gs.baz.ejes.modelo.dao.EjesModeloDAOImpl;
import com.gs.baz.ejes.modelo.dto.EjeModeloDTO;
import com.gs.baz.ejes.modelo.rest.dto.DataEjesModelo;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.EjeDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import com.gs.baz.model.bucket.client.services.dto.ModeloFranquiciaDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEje;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModelosFranquicia;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/ejes/modelo")
public class ServiceEjesModelo {

    @Autowired
    private EjesModeloDAOImpl ejesModeloDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceEje sbsEje;

    @Autowired
    private SubServiceModelosFranquicia sbsModelo;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/{id_modelo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EjeModeloDTO> getEjesModeloPorId(@PathVariable("id_modelo") Long id_modelo) throws CustomException {
        return ejesModeloDAOImpl.selectRows(id_modelo);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeModeloDTO postEjesModelo(@RequestBody EjeModeloDTO ejeModeloDTO) throws CustomException {
        if (ejeModeloDTO.getIdModelo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modelo"));
        }
        if (ejeModeloDTO.getIdEje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_eje"));
        }
        if (ejeModeloDTO.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        return ejesModeloDAOImpl.insertRow(ejeModeloDTO);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeModeloDTO putEjesModelo(@RequestBody EjeModeloDTO ejeModeloDTO) throws CustomException {
        if (ejeModeloDTO.getIdEje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_eje"));
        }
        if (ejeModeloDTO.getIdModEje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modelo_eje"));
        }
        if (ejeModeloDTO.getIdModelo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modelo"));
        }
        if (ejeModeloDTO.getPonderacion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("ponderacion"));
        }
        return ejesModeloDAOImpl.updateRow(ejeModeloDTO);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjeModeloDTO deleteEje(@RequestBody EjeModeloDTO ejeModeloDTO) throws CustomException {
        if (ejeModeloDTO.getIdModEje() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_modelo_eje"));
        }
        return ejesModeloDAOImpl.deleteRow(ejeModeloDTO);
    }

    @RequestMapping(value = "secure/get/data/{id_modelo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DataEjesModelo getDataEjesModelo(@RequestHeader HttpHeaders headers, @PathVariable("id_modelo") Long id_modelo) throws CustomException {
        final DataEjesModelo dataEjesModelo = new DataEjesModelo();
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        final Metadata metadata = new Metadata();
        try {
            sbsModelo.init(basePath, httpEntity);
            ModeloFranquiciaDTO modelo = sbsModelo.getModelo(id_modelo.intValue());
            if (modelo != null) {
                List<EjeModeloDTO> ejesModelo = ejesModeloDAOImpl.selectRows(id_modelo);
                if (!ejesModelo.isEmpty()) {
                    sbsEje.init(basePath, httpEntity);
                    List<EjeDTO> ejes = sbsEje.getEjes();
                    metadata.setEjes(ejes);
                    dataEjesModelo.setMetadata(metadata);
                    dataEjesModelo.setEjesModelo(ejesModelo);
                } else {
                    throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA);
                }
            } else {
                throw new CustomException(ModelCodes.DATA_NOT_FOUND);
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return dataEjesModelo;
    }

}
