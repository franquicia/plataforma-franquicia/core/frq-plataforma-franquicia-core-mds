/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FranquiciaDTO {

    @JsonProperty(value = "id_franquicia")
    protected Integer idFranquicia;

    @JsonProperty(value = "descripcion")
    protected String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_status")
    protected Integer idStatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocios_franquicia")
    private List<NegocioFranquiciaDTO> negociosFranquicia;

    @JsonProperty(value = "fecha_creacion")
    private Date fechaCreacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_empleado_crea")
    private Integer idEmpleadoCrea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_modificacion")
    private Date fechaModificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_empleado_modifica")
    private Integer idEmpleadoModifica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "empleado_crea")
    private Object empleadoCrea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "empleado_modifica")
    private Object empleadoModificacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "action")
    private String action;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    protected String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    protected String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    protected Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    protected Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    protected Boolean deleted;

    public FranquiciaDTO() {
    }

    public Integer getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(BigDecimal idFranquicia) {
        this.idFranquicia = (idFranquicia == null ? null : idFranquicia.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    public List<NegocioFranquiciaDTO> getNegociosFranquicia() {
        return negociosFranquicia;
    }

    public void setNegociosFranquicia(List<NegocioFranquiciaDTO> negociosFranquicia) {
        this.negociosFranquicia = negociosFranquicia;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getIdEmpleadoCrea() {
        return idEmpleadoCrea;
    }

    public void setIdEmpleadoCrea(BigDecimal idEmpleadoCrea) {
        this.idEmpleadoCrea = (idEmpleadoCrea == null ? null : idEmpleadoCrea.intValue());
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getIdEmpleadoModifica() {
        return idEmpleadoModifica;
    }

    public void setIdEmpleadoModifica(BigDecimal idEmpleadoModifica) {
        this.idEmpleadoModifica = (idEmpleadoModifica == null ? null : idEmpleadoModifica.intValue());
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Object getEmpleadoCrea() {
        return empleadoCrea;
    }

    public void setEmpleadoCrea(Object empleadoCrea) {
        this.empleadoCrea = empleadoCrea;
    }

    public Object getEmpleadoModificacion() {
        return empleadoModificacion;
    }

    public void setEmpleadoModificacion(Object empleadoModificacion) {
        this.empleadoModificacion = empleadoModificacion;
    }

    @Override
    public String toString() {
        return "FranquiciaDTO{" + "idFranquicia=" + idFranquicia + ", descripcion=" + descripcion + ", idStatus=" + idStatus + ", negocios_fraquicia =" + negociosFranquicia + ", action=" + action + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
