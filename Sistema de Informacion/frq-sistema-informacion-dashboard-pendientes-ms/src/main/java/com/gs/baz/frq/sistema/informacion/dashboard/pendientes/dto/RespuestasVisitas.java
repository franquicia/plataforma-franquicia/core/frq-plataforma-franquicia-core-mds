/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de respuestas de las visitas", value = "RespuestasVisitas")
public class RespuestasVisitas {

    @JsonProperty(value = "respuestasVisitas")
    @ApiModelProperty(notes = "respuestasVisitas")
    private List<RespuestaVisita> respuestaVisita;

    public RespuestasVisitas(List<RespuestaVisita> respuestaVisita) {
        this.respuestaVisita = respuestaVisita;
    }

    public List<RespuestaVisita> getRespuestaVisita() {
        return respuestaVisita;
    }

    public void setRespuestaVisita(List<RespuestaVisita> respuestaVisita) {
        this.respuestaVisita = respuestaVisita;
    }

    @Override
    public String toString() {
        return "RespuestasVisitas{" + "respuestaVisita=" + respuestaVisita + '}';
    }

}
