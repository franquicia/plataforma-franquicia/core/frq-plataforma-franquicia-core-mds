package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dao.IncidenciasDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dto.IncidenciasDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class IncidenciasBI {

    @Autowired
    private IncidenciasDAOImpl incidenciasDAOImpl;

    private static final Logger LOGGER = LogManager.getLogger();

    public int inserta(IncidenciasDTO bean) {
        int respuesta = 0;

        try {
            respuesta = incidenciasDAOImpl.inserta(bean);
        } catch (Exception e) {
            LOGGER.info(e);
        }

        return respuesta;
    }

    public int insertaLimpieza(IncidenciasDTO bean) {
        int respuesta = 0;

        try {
            respuesta = incidenciasDAOImpl.insertaLimpieza(bean);
        } catch (Exception e) {
            LOGGER.info(e);
        }

        return respuesta;
    }

    public boolean elimina(String idTipo) {
        boolean respuesta = false;

        try {
            respuesta = incidenciasDAOImpl.elimina(idTipo);
        } catch (Exception e) {
            LOGGER.info(e);
        }

        return respuesta;
    }

    public boolean depura() {
        boolean respuesta = false;

        try {
            respuesta = incidenciasDAOImpl.depura();
        } catch (Exception e) {
            LOGGER.info(e);
        }

        return respuesta;
    }

    public List<IncidenciasDTO> obtieneDatos(int idTipo) {
        List<IncidenciasDTO> listafila = null;
        try {
            listafila = incidenciasDAOImpl.obtieneDatos(idTipo);
        } catch (Exception e) {
            LOGGER.info(e);
        }

        return listafila;
    }

    public List<IncidenciasDTO> obtieneInfo() {
        List<IncidenciasDTO> listafila = null;
        try {
            listafila = incidenciasDAOImpl.obtieneInfo();
        } catch (Exception e) {
            LOGGER.info(e);
        }

        return listafila;
    }

    public List<IncidenciasDTO> obtieneInfoLimpieza() {
        List<IncidenciasDTO> listafila = null;
        try {
            listafila = incidenciasDAOImpl.obtieneInfoLimpieza();
        } catch (Exception e) {

            LOGGER.info(e);
        }

        return listafila;
    }

    public boolean actualiza(IncidenciasDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = incidenciasDAOImpl.actualiza(bean);
        } catch (Exception e) {
            LOGGER.info(e);

        }

        return respuesta;
    }

}
