/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos de la grafica semaforo", value = "AltaGraficaSemaforo")
public class AltaGraficaSemaforo {

    @JsonProperty(value = "id")
    @ApiModelProperty(notes = "Identificador de la grafica semaforo", example = "1")
    private Integer id;

    public AltaGraficaSemaforo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
