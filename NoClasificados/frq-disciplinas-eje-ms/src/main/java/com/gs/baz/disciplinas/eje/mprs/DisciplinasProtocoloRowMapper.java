package com.gs.baz.disciplinas.eje.mprs;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DisciplinasProtocoloRowMapper implements RowMapper<Integer> {

    @Override
    public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ((BigDecimal) rs.getObject("FIIDPROT")).intValue();
    }
}
