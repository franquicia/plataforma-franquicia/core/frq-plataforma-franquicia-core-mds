/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ejecuta.rest;

import com.gs.baz.ejecuta.dao.EjecutaDAOImpl;
import com.gs.baz.ejecuta.dto.EjecutaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/ejecuta")
public class ServiceEjecuta {

    @Autowired
    private EjecutaDAOImpl ejecutaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EjecutaDTO> getEjecuta() throws CustomException {
        return ejecutaDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjecutaDTO postEjecuta(@RequestBody EjecutaDTO ejecuta) throws CustomException {
        if (ejecuta.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return ejecutaDAOImpl.insertRow(ejecuta);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjecutaDTO putEjecuta(@RequestBody EjecutaDTO ejecuta) throws CustomException {
        if (ejecuta.getIdEjecuta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ejecuta"));
        }
        if (ejecuta.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return ejecutaDAOImpl.updateRow(ejecuta);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public EjecutaDTO deleteEjecuta(@RequestBody EjecutaDTO ejecuta) throws CustomException {
        if (ejecuta.getIdEjecuta() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ejecuta"));
        }
        return ejecutaDAOImpl.deleteRow(ejecuta);
    }
}
