/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.punto.contacto.protocolo.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.punto.contacto.protocolo.dao.PuntoContactoProtocoloDAOImpl;
import com.gs.baz.frq.punto.contacto.protocolo.dto.PuntoContactoProtocoloDTO;
import com.gs.baz.read.propiedades.commons.Propiedades;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/punto/contacto/protocolo")
public class ServicePuntoContactoProtocolo {

    @Autowired
    private PuntoContactoProtocoloDAOImpl puntoContactoProtocoloDAOImpl;

    @Autowired
    private Propiedades propiedades;

    //@Autowired
    // private PuntoContactoProtocoloDTO puntoContactoProtocoloDTO;
    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PuntoContactoProtocoloDTO> getPuntoContactoProtocolo() throws CustomException {
        return puntoContactoProtocoloDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_ceco}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PuntoContactoProtocoloDTO> getPuntoContactoProtocolo2(@PathVariable("id_ceco") Long idCeco) throws CustomException {

        if ((Long.toString(idCeco).substring(0, 2)).equals("48")) {
            Object valorProp48 = propiedades.getByKey("idProtocoloFichaTecnica");
            Integer newValue = Integer.parseInt(valorProp48.toString());
            List<PuntoContactoProtocoloDTO> puntoContactoProtocoloDTO2 = puntoContactoProtocoloDAOImpl.selectRow(idCeco);
            PuntoContactoProtocoloDTO puntoContactoProtocoloDTO = new PuntoContactoProtocoloDTO();
            puntoContactoProtocoloDTO.setIdProt(newValue);
            puntoContactoProtocoloDTO2.add(puntoContactoProtocoloDTO);
            return puntoContactoProtocoloDTO2;
        } else if ((Long.toString(idCeco).substring(0, 2)).equals("55")) {
            Object valorProp55 = propiedades.getByKey("IDPCDT");
            Integer newValue = Integer.parseInt(valorProp55.toString());
            List<PuntoContactoProtocoloDTO> puntoContactoProtocoloDTO2 = new ArrayList<>();
            PuntoContactoProtocoloDTO puntoContactoProtocoloDTO = new PuntoContactoProtocoloDTO();
            puntoContactoProtocoloDTO.setIdProt(newValue);
            puntoContactoProtocoloDTO2.add(puntoContactoProtocoloDTO);
            return puntoContactoProtocoloDTO2;
        } else if ((Long.toString(idCeco).substring(0, 2)).equals("23")) {
            Object valorProp23 = propiedades.getByKey("IDPGCC");
            Integer newValue = Integer.parseInt(valorProp23.toString());
            List<PuntoContactoProtocoloDTO> puntoContactoProtocoloDTO2 = new ArrayList<>();
            PuntoContactoProtocoloDTO puntoContactoProtocoloDTO = new PuntoContactoProtocoloDTO();
            puntoContactoProtocoloDTO.setIdProt(newValue);
            puntoContactoProtocoloDTO2.add(puntoContactoProtocoloDTO);
            return puntoContactoProtocoloDTO2;
        } else {
            return puntoContactoProtocoloDAOImpl.selectRow(idCeco);
        }
    }
}
