package com.gs.baz.colorimetria.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.colorimetria.dao.ColorimetriaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.colorimetria")
public class ConfigColorimetria {

    private final Logger logger = LogManager.getLogger();

    public ConfigColorimetria() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ColorimetriaDAOImpl colorimetriaDAOImpl() {
        return new ColorimetriaDAOImpl();
    }
}
