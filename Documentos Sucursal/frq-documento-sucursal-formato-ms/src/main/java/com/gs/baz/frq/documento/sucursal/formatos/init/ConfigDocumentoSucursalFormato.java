package com.gs.baz.frq.documento.sucursal.formatos.init;

import com.gs.baz.frq.documento.sucursal.formatos.dao.FormatoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documento.sucursal.formatos")
public class ConfigDocumentoSucursalFormato {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalFormato() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public FormatoDAOImpl documentoSucursalFormatoDAOImpl() {
        return new FormatoDAOImpl();
    }

}
