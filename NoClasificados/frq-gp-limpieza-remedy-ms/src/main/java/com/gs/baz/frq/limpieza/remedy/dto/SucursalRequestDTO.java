/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author kramireza
 */
@ApiModel(description = "Datos de la sucursal como entrada de metodo", value = "Sucursal")
public class SucursalRequestDTO {

    @JsonProperty(value = "centroCostos", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    @ApiModelProperty(notes = "Centro de costos de la sucursal."
            + " `Cifre el valor de éste campo con la llave pública (accesoPublico) "
            + "con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`",
            example = "480100", required = true)
    private String centroCostos;

    public SucursalRequestDTO() {
        super();

    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

}
