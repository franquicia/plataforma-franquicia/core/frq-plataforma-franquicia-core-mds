/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.documento.archivos.rest;

import com.gs.baz.frq.documento.sucursal.documento.archivos.dao.DocumentoArchivoDAOImpl;
import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.DocumentosFolio;
import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.ParametrosDocumentoFolio;
import com.gs.baz.frq.documento.sucursal.documento.archivos.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "proyectoDocumentos", value = "proyectoDocumentos", description = "Api para la gestión del catalogo de proyectoDocumentos")
@RestController
@RequestMapping("/api-local/documento-sucursal/documentoArchivo/v1")
public class DocumentoArchivoApi {

    @Autowired
    private DocumentoArchivoDAOImpl documentoArchivoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idDocumento
     * @param idFolio
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene proyectoDocumento", notes = "Obtiene un proyectoDocumento", nickname = "obtieneProyectoDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "busquedas/documentos/{idDocumento}/folio/{idFolio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentosFolio obtieneProyectoDocumento(
            @ApiParam(name = "idDocumento", value = "Identificador del proyectoDocumento", example = "1", required = true)
            @PathVariable("idDocumento") Long idDocumento,
            @ApiParam(name = "idFolio", value = "Identificador del Folio", example = "1", required = true)
            @PathVariable("idFolio") Long idFolio) throws CustomException, DataNotFoundException {

        DocumentosFolio documentosFolios = new DocumentosFolio(documentoArchivoDAOImpl.selectRow(idDocumento, idFolio));

        if (documentosFolios.getDocumentoArchivos() == null || documentosFolios.getDocumentoArchivos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return documentosFolios;
    }

    /**
     *
     * @param parametros
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear proyectoDocumento", notes = "Agrega un proyectoDocumento", nickname = "creaProyectoDocumento")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public SinResultado creaProyectoDocumento(@ApiParam(name = "ParametrosProyectoDocumento", value = "Paramentros para el alta del proyectoDocumento", required = true) @RequestBody ParametrosDocumentoFolio parametros) throws DataNotInsertedException {
        return documentoArchivoDAOImpl.insertRow(parametros);
    }

    /**
     *
     * @param parametros
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar proyectoDocumento", notes = "Elimina un item de los proyectoDocumentos", nickname = "eliminaProyectoDocumento")
    @RequestMapping(value = "/", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaProyectoDocumento(
            @ApiParam(name = "parametros", value = "Identificador del proyectoDocumento", example = "1", required = true)
            @RequestBody ParametrosDocumentoFolio parametros) throws DataNotDeletedException {
        documentoArchivoDAOImpl.deleteRow(parametros);
        return new SinResultado();
    }

}
