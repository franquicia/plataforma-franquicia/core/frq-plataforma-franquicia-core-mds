package com.gs.baz.status.mprs;

import com.gs.baz.status.dto.StatusDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class StatusRowMapper implements RowMapper<StatusDTO> {

    private StatusDTO status;

    @Override
    public StatusDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new StatusDTO();
        status.setIdStatus(((BigDecimal) rs.getObject("FIIDESTATUS")));
        status.setDescripcion(rs.getString("FCDESCRIPCION"));
        return status;
    }
}
