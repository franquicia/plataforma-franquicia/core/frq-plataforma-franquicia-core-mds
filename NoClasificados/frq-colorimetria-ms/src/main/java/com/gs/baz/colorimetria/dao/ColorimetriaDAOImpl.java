package com.gs.baz.colorimetria.dao;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.colorimetria.dao.util.GenericDAO;
import com.gs.baz.colorimetria.dto.ColorimetriaDTO;
import com.gs.baz.colorimetria.mprs.ColorimetriaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ColorimetriaDAOImpl extends DefaultDAO implements GenericDAO<ColorimetriaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINCOLOR");
        jdbcInsert.withProcedureName("SP_INS_COLOR");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINCOLOR");
        jdbcUpdate.withProcedureName("SP_ACT_COLOR");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINCOLOR");
        jdbcSelect.withProcedureName("SP_SEL_COLOR");
        jdbcSelect.returningResultSet("RCL_INFO", new ColorimetriaRowMapper());
    }

    @Override
    public ColorimetriaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCOLOR", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ColorimetriaDTO> data = (List<ColorimetriaDTO>) out.get("RCL_INFO");
        data.forEach((item) -> {
            item.setIcono(null);
        });
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public ColorimetriaDTO selectRowIcono(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCOLOR", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ColorimetriaDTO> data = (List<ColorimetriaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<ColorimetriaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCOLOR", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<ColorimetriaDTO> data = (List<ColorimetriaDTO>) out.get("RCL_INFO");
        data.forEach((item) -> {
            item.setIcono(null);
        });
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public ColorimetriaDTO selectRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ColorimetriaDTO> selectRows(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ColorimetriaDTO insertRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ColorimetriaDTO insertRow(ColorimetriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCTEMAWEB", entityDTO.getTemaWeb());
            mapSqlParameterSource.addValue("PA_FCCOPRIMARIO", entityDTO.getColorPrim());
            mapSqlParameterSource.addValue("PA_FCCOSECUNDARIO", entityDTO.getColorSegund());
            mapSqlParameterSource.addValue("PA_FCCOTERCIARIO", entityDTO.getColorTerc());
            mapSqlParameterSource.addValue("PA_FCICONO", entityDTO.getIcono());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdColorimetria((BigDecimal) out.get("PA_FIIDCOLOR"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Colorimetria"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Colorimetria"), ex);
        }
    }

    @Override
    public ColorimetriaDTO updateRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ColorimetriaDTO updateRow(ColorimetriaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCOLOR", entityDTO.getIdColorimetria());
            mapSqlParameterSource.addValue("PA_FCTEMAWEB", entityDTO.getTemaWeb());
            mapSqlParameterSource.addValue("PA_FCCOPRIMARIO", entityDTO.getColorPrim());
            mapSqlParameterSource.addValue("PA_FCCOSECUNDARIO", entityDTO.getColorSegund());
            mapSqlParameterSource.addValue("PA_FCCOTERCIARIO", entityDTO.getColorTerc());
            mapSqlParameterSource.addValue("PA_FCICONO", entityDTO.getIcono());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Colorimetria"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Colorimetria"), ex);
        }
    }

    @Override
    public ColorimetriaDTO deleteRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ColorimetriaDTO deleteRow(ColorimetriaDTO entityID) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
