/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.mprs;

import com.gs.baz.frq.modelo.negocio.folios.dto.BusquedaFolioArbol;
import com.gs.baz.frq.modelo.negocio.folios.dto.Documento;
import com.gs.baz.frq.modelo.negocio.folios.dto.Empleado;
import com.gs.baz.frq.modelo.negocio.folios.dto.EstadoFolio;
import com.gs.baz.frq.modelo.negocio.folios.dto.ModeloNegocio;
import com.gs.baz.frq.modelo.negocio.folios.dto.UnidadNegocio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class FoliosArbolUsuarioRowMapper implements RowMapper<BusquedaFolioArbol> {

    private BusquedaFolioArbol busquedaFolioArbol;

    @Override
    public BusquedaFolioArbol mapRow(ResultSet rs, int rowNum) throws SQLException {
        busquedaFolioArbol = new BusquedaFolioArbol();
        busquedaFolioArbol.setIdFolio(rs.getInt("FIIDFOLIO"));
        busquedaFolioArbol.setDocumento(new Documento(rs.getString("FCTITULO")));
        busquedaFolioArbol.setCreador(new Empleado(rs.getString("CREADOR")));
        busquedaFolioArbol.setEstado(new EstadoFolio(rs.getInt("ID_ESTATUS"), rs.getString("ESTATUS")));
        busquedaFolioArbol.setCalificacion(rs.getInt("FICALIFICACION"));
        busquedaFolioArbol.setVistas(rs.getInt("FINUMVISUALIZACION"));
        busquedaFolioArbol.setFechaPublicacion(rs.getString("FDFECHAPUBLICADO"));
        List<UnidadNegocio> listaUnidadNegocio = new ArrayList<>();
        String[] arrayUnidades = rs.getString("UNIDADES_NEGOCIO").split(",");
        for (String unidades : arrayUnidades) {
            UnidadNegocio unidadNegocio = new UnidadNegocio();
            unidadNegocio.setDescripcion(unidades);
            listaUnidadNegocio.add(unidadNegocio);
        }
        busquedaFolioArbol.setUnidadesNegocio(listaUnidadNegocio);
        String color = rs.getString("COLOR");
        String[] modelosNegocioSplit = color.split("\\|");
        List<ModeloNegocio> modelosNegocio = new ArrayList<>();
        for (String modeloNegocioSplit : modelosNegocioSplit) {
            modeloNegocioSplit = modeloNegocioSplit.replace("[", "");
            modeloNegocioSplit = modeloNegocioSplit.replace("]", "");
            String[] atributos = modeloNegocioSplit.split(",");
            Integer idArbol = new Integer(atributos[0]);
            String valueColor = atributos[1];
            modelosNegocio.add(new ModeloNegocio(idArbol, null, valueColor));
        }
        busquedaFolioArbol.setModelosNegocio(modelosNegocio);
        return busquedaFolioArbol;
    }
}
