package com.gs.baz.frq.dashboard.dao.graficos.g005;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g005.CumplimientoDisciplinaNegociosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g005.NegocioDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g005.RangoDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g005.CumplimientoDisciplinaNegocioDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g005.CumplimientoDisciplinaNegocioRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl005 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCumplimientoDisciplinaNegocioNacional;
    private DefaultJdbcCall jdbcSelectCumplimientoDisciplinaNegocioNivel;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCumplimientoDisciplinaNegocioNacional = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoDisciplinaNegocioNacional.withSchemaName(schema);
        jdbcSelectCumplimientoDisciplinaNegocioNacional.withCatalogName("PACONSDASHBOARD");
        jdbcSelectCumplimientoDisciplinaNegocioNacional.withProcedureName("SPCUMNACXNEG");
        jdbcSelectCumplimientoDisciplinaNegocioNacional.returningResultSet("RCL_INFO", new CumplimientoDisciplinaNegocioRowMapper());

        jdbcSelectCumplimientoDisciplinaNegocioNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCumplimientoDisciplinaNegocioNivel.withSchemaName(schema);
        jdbcSelectCumplimientoDisciplinaNegocioNivel.withCatalogName("PACONSDASH");
        jdbcSelectCumplimientoDisciplinaNegocioNivel.withProcedureName("SPCUMNACXNEG");
        jdbcSelectCumplimientoDisciplinaNegocioNivel.returningResultSet("RCL_INFO", new CumplimientoDisciplinaNegocioRowMapper());
    }

    private List<CumplimientoDisciplinaNegocioDTO> selectRowsCumplimientoDisciplinaNegocioNacional(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoDisciplinaNegocioNacional.execute(mapSqlParameterSource);
        return (List<CumplimientoDisciplinaNegocioDTO>) out.get("RCL_INFO");
    }

    private List<CumplimientoDisciplinaNegocioDTO> selectRowsCumplimientoDisciplinaNegocioNivel(FiltrosDTO filtrosDTO) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtrosDTO);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtrosDTO.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtrosDTO.getIdNegocio());
        mapSqlParameterSource.addValue("PA_CECO", filtrosDTO.getIdCeco());
        mapSqlParameterSource.addValue("PA_NIVEL", filtrosDTO.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectCumplimientoDisciplinaNegocioNivel.execute(mapSqlParameterSource);
        return (List<CumplimientoDisciplinaNegocioDTO>) out.get("RCL_INFO");
    }

    public CumplimientoDisciplinaNegociosDTO getCumplimientoNegocioDisciplina(CumplimientoDisciplinaNegocioDTO cumplimientoDisciplinaNegocioDTO) throws CustomException {
        FiltrosDTO filtrosDTO = cumplimientoDisciplinaNegocioDTO.getFiltros();
        if (filtrosDTO.getNivelGeografico().equals(4)) {
            return this.getCumplimientoNegocioDisciplina(this.selectRowsCumplimientoDisciplinaNegocioNacional(filtrosDTO));
        } else {
            return this.getCumplimientoNegocioDisciplina(this.selectRowsCumplimientoDisciplinaNegocioNivel(filtrosDTO));
        }
    }

    private CumplimientoDisciplinaNegociosDTO getCumplimientoNegocioDisciplina(List<CumplimientoDisciplinaNegocioDTO> cumplimientosDisciplinaNegociosNacional) throws CustomException {
        CumplimientoDisciplinaNegociosDTO cumplimientoDisciplinaNegociosDTO = new CumplimientoDisciplinaNegociosDTO();
        Map<Integer, List<CumplimientoDisciplinaNegocioDTO>> temp = cumplimientosDisciplinaNegociosNacional.stream().collect(Collectors.groupingBy(CumplimientoDisciplinaNegocioDTO::getIdNegocio));
        if (temp != null) {
            List<NegocioDTO> cumplimientoNegs = new ArrayList<>();
            temp.forEach((idNegocio, cumplimientosNegocioDTO) -> {
                List<RangoDTO> rangos = new ArrayList<>();
                cumplimientosNegocioDTO.forEach(item -> {
                    rangos.add(new RangoDTO(item.getIdRango(), item.getPorcentaje(), item.getTotalEvaluadas()));
                });
                Integer totalEvaluados = rangos.stream().collect(Collectors.summingInt(RangoDTO::getPuntosContacto));
                cumplimientoNegs.add(new NegocioDTO(idNegocio, rangos, totalEvaluados));
            });
            cumplimientoDisciplinaNegociosDTO.setNegocios(cumplimientoNegs);
            NegocioDTO maxNeg = cumplimientoNegs.stream().max(Comparator.comparing(NegocioDTO::getTotalEvaluados)).orElse(null);
            if (maxNeg != null) {
                cumplimientoDisciplinaNegociosDTO.setTotalEvaluados(maxNeg.getTotalEvaluados());
            }
        }
        return cumplimientoDisciplinaNegociosDTO;
    }

}
