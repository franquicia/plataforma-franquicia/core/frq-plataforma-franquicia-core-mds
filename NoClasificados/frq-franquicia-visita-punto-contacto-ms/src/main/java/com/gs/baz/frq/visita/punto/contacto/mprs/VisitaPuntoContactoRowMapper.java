package com.gs.baz.frq.visita.punto.contacto.mprs;

import com.gs.baz.frq.visita.punto.contacto.dto.VisitaPuntoContactoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class VisitaPuntoContactoRowMapper implements RowMapper<VisitaPuntoContactoDTO> {

    private VisitaPuntoContactoDTO vistaPuntoContactoDTO;

    @Override
    public VisitaPuntoContactoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        vistaPuntoContactoDTO = new VisitaPuntoContactoDTO();
        vistaPuntoContactoDTO.setIdBitacora(((BigDecimal) rs.getObject("FIID_BITACORA")));
        vistaPuntoContactoDTO.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        vistaPuntoContactoDTO.setIdNegocioDisciplina((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        vistaPuntoContactoDTO.setIdCeco(rs.getString("FCID_CECO"));
        vistaPuntoContactoDTO.setIdCheckUsuario((BigDecimal) rs.getObject("FIID_CHECK_USUA"));
        vistaPuntoContactoDTO.setIdCheckList((BigDecimal) rs.getObject("FIID_CHECKLIST"));
        vistaPuntoContactoDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
        vistaPuntoContactoDTO.setFecha(rs.getString("FDFECHA"));
        vistaPuntoContactoDTO.setIdUsuario((BigDecimal) rs.getObject("FIID_USUARIO"));
        return vistaPuntoContactoDTO;
    }
}
