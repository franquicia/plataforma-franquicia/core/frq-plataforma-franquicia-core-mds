package com.gs.baz.frq.asignacion.init;

import com.gs.baz.frq.asignacion.dao.AsignacionDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.asignacion")
public class ConfigAsignacion {

    private final Logger logger = LogManager.getLogger();

    public ConfigAsignacion() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqAsignacionDAOImpl")
    public AsignacionDAOImpl tipoFormatoDAOImpl() {
        return new AsignacionDAOImpl();
    }
}
