/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Usuario", value = "Usuario")
public class Usuario extends UsuarioBase {

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Identificador del Usuario", example = "1", position = -1)
    private Integer numeroEmpleado;

    public Usuario() {
    }

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    @Override
    public String toString() {
        return "Usuario{" + "numeroEmpleado=" + numeroEmpleado + '}';
    }

}
