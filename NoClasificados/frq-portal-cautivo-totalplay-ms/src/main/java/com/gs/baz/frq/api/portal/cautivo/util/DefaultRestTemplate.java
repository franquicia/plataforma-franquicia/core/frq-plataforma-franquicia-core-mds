/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author MADA
 */
public class DefaultRestTemplate {

    @Autowired
    protected RestTemplate restTemplate;
    protected final ObjectMapper objectMapper = new ObjectMapper();
    protected ResponseEntity<String> responseString;
    protected ResponseEntity<byte[]> responseBytes;
    protected JsonNode responseStringBody;
    protected String basePath;
    protected HttpEntity<Object> httpEntity;

    public DefaultRestTemplate() {
        super();
    }

    public void init() {

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("admusr01", "K4n-B3hZ.WoP$09");
        // headers.add("Authorization", "Basic YWRtdXNyMDE6SzRuLUIzaFouV29QJDA5");

        headers.add("Accept", "*/*");
        headers.add("Accept-Encoding", "gzip, deflate, br");
        headers.add("Accept", "application/json");
        headers.add("Cache-Control", "no-cache");
        headers.add("Connection", "keep-alive");
        headers.add("Authorization", "YWRtdXNyMDE6SzRuLUIzaFouV29QJDA5");
        this.httpEntity = new HttpEntity<>(headers);

    }

}
