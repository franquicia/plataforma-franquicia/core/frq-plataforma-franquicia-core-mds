/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.base64.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class SalidaArchivoBase64 {

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del archivo", example = "documento.docx")
    private String nombre;

    @JsonProperty(value = "archivo")
    @ApiModelProperty(notes = "Archivo base 64", example = "DIwMTExMDAzMDYxNjc2OS9fLnhsc3g=")
    private String archivo;

    @JsonProperty(value = "tipoContenido")
    @ApiModelProperty(notes = "Tipo archivo")
    private String tipoContenido;

    public SalidaArchivoBase64() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    @Override
    public String toString() {
        return "SalidaArchivoBase64{" + "nombre=" + nombre + ", archivo=" + archivo + ", tipoContenido=" + tipoContenido + '}';
    }

}
