package com.gs.baz.frq.asignacion.dao;

import com.gs.baz.frq.asignacion.dto.AsignacionDTO;
import com.gs.baz.frq.asignacion.mprs.AsignacionCalendarioRowMapper;
import com.gs.baz.frq.asignacion.mprs.AsignacionRowMapper;
import com.gs.baz.frq.asignacion.mprs.FasesRowMapper;
import com.gs.baz.frq.asignacion.mprs.ProyectosRowMapper;
import com.gs.baz.frq.asignacion.mprs.SoftHistoCalendarioRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class AsignacionDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcObtieneProyecto;
    private DefaultJdbcCall jdbcObtieneFasesProyecto;
    private DefaultJdbcCall jdbcObtieneAsignacionCeco;
    private DefaultJdbcCall jdbcObtieneDesbloqueaFase;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcDeleteAsignacion;
    private DefaultJdbcCall jdbcObtieneDatosCalendario;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PA_SOPASIGPROY");
        jdbcInsert.withProcedureName("SP_INS_ASIG");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PA_SOPASIGPROY");
        jdbcUpdate.withProcedureName("SP_ACT_ASIG");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAASIGTRANSF");
        jdbcDelete.withProcedureName("SP_DESACTIVAVERS");

        jdbcObtieneProyecto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneProyecto.withSchemaName(schema);
        jdbcObtieneProyecto.withCatalogName("PA_SOPASIGPROY");
        jdbcObtieneProyecto.withProcedureName("SP_SEL_PROYEC");
        jdbcObtieneProyecto.returningResultSet("RCL_PROY", new ProyectosRowMapper());

        jdbcObtieneFasesProyecto = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneFasesProyecto.withSchemaName(schema);
        jdbcObtieneFasesProyecto.withCatalogName("PA_SOPASIGPROY");
        jdbcObtieneFasesProyecto.withProcedureName("SP_SEL_FASES");
        jdbcObtieneFasesProyecto.returningResultSet("RCL_FASES", new FasesRowMapper());

        jdbcObtieneAsignacionCeco = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneAsignacionCeco.withSchemaName(schema);
        jdbcObtieneAsignacionCeco.withCatalogName("PA_SOPASIGPROY");
        jdbcObtieneAsignacionCeco.withProcedureName("SP_SEL_ASIG");
        jdbcObtieneAsignacionCeco.returningResultSet("RCL_ASIGN", new AsignacionRowMapper());

        jdbcObtieneDesbloqueaFase = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneDesbloqueaFase.withSchemaName(schema);
        jdbcObtieneDesbloqueaFase.withCatalogName("PA_SOPASIGPROY");
        jdbcObtieneDesbloqueaFase.withProcedureName("SP_SEL_PROYEC");
        jdbcObtieneDesbloqueaFase.returningResultSet("SP_SEL_ASIG", new AsignacionRowMapper());

        jdbcDeleteAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDeleteAsignacion.withSchemaName(schema);
        jdbcDeleteAsignacion.withCatalogName("PAASIGTRANSF");
        jdbcDeleteAsignacion.withProcedureName("SP_DEL_ASIGUSU");

        jdbcObtieneDatosCalendario = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneDatosCalendario.withSchemaName(schema);
        jdbcObtieneDatosCalendario.withCatalogName("PA_SOPASIGPROY");
        jdbcObtieneDatosCalendario.withProcedureName("SP_SEL_ASIGNADOR");
        jdbcObtieneDatosCalendario.returningResultSet("RCL_ASIGN", new AsignacionCalendarioRowMapper());
        jdbcObtieneDatosCalendario.returningResultSet("RCL_SOFT", new SoftHistoCalendarioRowMapper());
    }

//trae proyectos ceco
    public List<AsignacionDTO> selectRowDTO(String entityID) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityID);
        Map<String, Object> out = jdbcObtieneProyecto.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<AsignacionDTO> data = (List<AsignacionDTO>) out.get("RCL_PROY");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }
    //fases proyecto

    public List<AsignacionDTO> selectRows(AsignacionDTO entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getCeco());
        mapSqlParameterSource.addValue("PA_PROY", entityDTO.getProyecto());
        Map<String, Object> out = jdbcObtieneFasesProyecto.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<AsignacionDTO> data = (List<AsignacionDTO>) out.get("RCL_FASES");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    //asignaciones proyecto
    public List<AsignacionDTO> selectRows1(AsignacionDTO entityDTO) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CECO", entityDTO.getCeco());
        mapSqlParameterSource.addValue("PA_PROY", entityDTO.getProyecto());
        //mapSqlParameterSource.addValue("PA_USU", entityDTO.getIdUsuario());
        mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getStatus());
        Map<String, Object> out = jdbcObtieneAsignacionCeco.execute(mapSqlParameterSource);
        @SuppressWarnings("unchecked")
        List<AsignacionDTO> data = (List<AsignacionDTO>) out.get("RCL_ASIGN");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public int insertRow(AsignacionDTO entityDTO) throws CustomException {
        try {
            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getCeco());
            mapSqlParameterSource.addValue("PA_PROY", entityDTO.getProyecto());
            mapSqlParameterSource.addValue("PA_USU", entityDTO.getResponsableAsigno());
            mapSqlParameterSource.addValue("PA_USUASIG", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_OBSERV", entityDTO.getObsAsignacion());
            mapSqlParameterSource.addValue("PA_IDAGRUPA", entityDTO.getIdAgrupa());
            mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getPeriodo());
            mapSqlParameterSource.addValue("PA_FASE", entityDTO.getFaseActual());

            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);

            boolean success = ((BigDecimal) out.get("PA_ERROR")).intValue() != 0;

            BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
            error = resultado.intValue();

            if (success) {
                entityDTO.setUpdated(success);
                return error;

            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Asignacion"), ex);
        }
    }

    public AsignacionDTO updateRow(AsignacionDTO entityDTO) throws CustomException {
        try {

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getCeco());
            mapSqlParameterSource.addValue("PA_PROYECTO", entityDTO.getProyecto());
            mapSqlParameterSource.addValue("PA_USU", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_STATUS", entityDTO.getStatus());

            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_ERROR")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Asignacion"), ex);
        }
    }

    public AsignacionDTO updateRows(AsignacionDTO entityDTO) throws CustomException {
        try {

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_USUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getCeco());
            mapSqlParameterSource.addValue("PA_AGRUPA", entityDTO.getIdAgrupa());

            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_ERROR")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Asignacion"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Asignacion"), ex);
        }
    }

    public int eliminaRows(AsignacionDTO entityDTO) throws CustomException {
        try {
            int error = 0;

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            // es proyecto
            mapSqlParameterSource.addValue("PA_USUASIG", entityDTO.getProyecto());
            mapSqlParameterSource.addValue("PA_CECO", entityDTO.getCeco());

            Map<String, Object> out = jdbcDeleteAsignacion.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_ERROR")).intValue() >= 0;
            BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
            error = resultado.intValue();

            if (success) {

                return error;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to delete row of Asignacion"));

            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception Error to delete row of Asignacion"), ex);

        }
    }

    //fases proyecto
    @SuppressWarnings("unchecked")
    public Map<String, Object> selectCalendarioRows(AsignacionDTO entityDTO) throws CustomException {
        int error = 0;
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<AsignacionDTO> listaAsignaciones = null;
        List<AsignacionDTO> listaSoftHistorico = null;
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        //dia mes año
        mapSqlParameterSource.addValue("PA_FECHA", entityDTO.getFecha());
        out = jdbcObtieneDatosCalendario.execute(mapSqlParameterSource);

        listaAsignaciones = (List<AsignacionDTO>) out.get("RCL_ASIGN");
        listaSoftHistorico = (List<AsignacionDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("No se cargo la informacion Correctamente");
            return null;
        }
        lista = new HashMap<String, Object>();
        lista.put("listaAsignaciones", listaAsignaciones);
        lista.put("listaSoftHistorico", listaSoftHistorico);

        return lista;
    }

}
