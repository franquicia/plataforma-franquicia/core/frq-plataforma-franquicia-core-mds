/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dao;

import com.gs.baz.frq.api.portal.cautivo.dto.BancaDigitalDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.ICUDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.PortalCautivoDTO;
import com.gs.baz.frq.api.portal.cautivo.mprs.RespuestaBancaDigitalRowMapper;
import com.gs.baz.frq.api.portal.cautivo.mprs.RespuestaICURowMapper;
import com.gs.baz.frq.api.portal.cautivo.mprs.RespuestaPortalCautivoRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author MADA
 */
public class GobiernoDatosDAOImpl extends DefaultDAO {

    private String schema;
    private DefaultJdbcCall jdbcObtieneICU;
    private DefaultJdbcCall jdbPortalCautivoICU;
    private DefaultJdbcCall jdbBancaDigitalICU;

    public void init() {

        schema = "FRANQUICIA";

        jdbcObtieneICU = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcObtieneICU.withSchemaName(schema);
        jdbcObtieneICU.withCatalogName("PAICUDIGITAL");
        jdbcObtieneICU.withProcedureName("SPGETICUUSRBDIGITAL");
        jdbcObtieneICU.returningResultSet("PA_CDATOS", new RespuestaICURowMapper());

        jdbPortalCautivoICU = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbPortalCautivoICU.withSchemaName(schema);
        jdbPortalCautivoICU.withCatalogName("PACLIENTESWIFI");
        jdbPortalCautivoICU.withProcedureName("SPGETPORTAL");
        jdbPortalCautivoICU.returningResultSet("PA_CDATOS", new RespuestaPortalCautivoRowMapper());

        jdbBancaDigitalICU = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbBancaDigitalICU.withSchemaName(schema);
        jdbBancaDigitalICU.withCatalogName("PACLIENTESWIFI");
        jdbBancaDigitalICU.withProcedureName("SPGETBDIGITAL");
        jdbBancaDigitalICU.returningResultSet("PA_CDATOS", new RespuestaBancaDigitalRowMapper());
    }

    public List<ICUDTO> obtieneICU(String fechaInicio, String fechaFin, String identificador) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        List<ICUDTO> respuesta = null;

        mapSqlParameterSource.addValue("PA_FDINICIO", fechaInicio);
        mapSqlParameterSource.addValue("PA_FDFIN", fechaFin);

        if (identificador == null || identificador.equals(" ") || identificador.length() == 0) {
            mapSqlParameterSource.addValue("PA_FCCLIENTE_ID", null);
        } else {
            mapSqlParameterSource.addValue("PA_FCCLIENTE_ID", identificador);
        }

        Map<String, Object> out = jdbcObtieneICU.execute(mapSqlParameterSource);

        respuesta = (List<ICUDTO>) out.get("PA_CDATOS");

        return respuesta;

    }

    public List<PortalCautivoDTO> obtienePortalCautivo(String fechaInicio, String fechaFin, String mac) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        List<PortalCautivoDTO> respuesta = null;
        mapSqlParameterSource.addValue("PA_FDINICIO", fechaInicio);
        mapSqlParameterSource.addValue("PA_FDFIN", fechaFin);
        if (mac == null || mac.equals(" ") || mac.length() == 0) {
            mapSqlParameterSource.addValue("PA_MAC", null);
        } else {
            mapSqlParameterSource.addValue("PA_MAC", mac);
        }

        Map<String, Object> out = null;

        out = jdbPortalCautivoICU.execute(mapSqlParameterSource);

        respuesta = (List<PortalCautivoDTO>) out.get("PA_CDATOS");

        return respuesta;

    }

    public List<BancaDigitalDTO> obtieneBancaDigital(String fechaInicio, String fechaFin, String mac) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        List<BancaDigitalDTO> respuesta = null;

        mapSqlParameterSource.addValue("PA_FDINICIO", fechaInicio);
        mapSqlParameterSource.addValue("PA_FDFIN", fechaFin);
        if (mac == null || mac.equals(" ") || mac.length() == 0) {
            mapSqlParameterSource.addValue("PA_MAC", null);
        } else {
            mapSqlParameterSource.addValue("PA_MAC", mac);
        }

        Map<String, Object> out = jdbBancaDigitalICU.execute(mapSqlParameterSource);

        respuesta = (List<BancaDigitalDTO>) out.get("PA_CDATOS");

        return respuesta;

    }
}
