/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del imperdonable total", value = "ImperdonableTotal")
public class ImperdonableTotal {

    @JsonProperty(value = "puntosVenta")
    @ApiModelProperty(notes = "Total de puntos de venta", example = "90")
    private Integer puntosVenta;

    @JsonProperty(value = "imperdonables")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Imperdonables")
    private List<Imperdonable> imperdonables;

    public Integer getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(Integer puntosVenta) {
        this.puntosVenta = puntosVenta;
    }

    public List<Imperdonable> getImperdonables() {
        return imperdonables;
    }

    public void setImperdonables(List<Imperdonable> imperdonables) {
        this.imperdonables = imperdonables;
    }

    @Override
    public String toString() {
        return "ImperdonableTotal{" + "puntosVenta=" + puntosVenta + ", imperdonables=" + imperdonables + '}';
    }

}
