/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.tipo.formato.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.tipo.formato.dao.TipoFormatoDAOImpl;
import com.gs.baz.frq.tipo.formato.dto.TipoFormatoDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/tipo/formato")
@Component("FRQServiceTipoFormato")
public class ServiceTipoFormato {

    @Autowired
    private TipoFormatoDAOImpl tipoFormatoDao;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TipoFormatoDTO> getTiposFormato() throws CustomException {
        return tipoFormatoDao.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_tipo_formato}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoFormatoDTO getTipoFormato(@PathVariable("id_tipo_formato") Long idTipoFormato) throws CustomException {
        return tipoFormatoDao.selectRow(idTipoFormato);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoFormatoDTO postTipoFormato(@RequestBody TipoFormatoDTO tipoFormato) throws CustomException {

        if (tipoFormato.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return tipoFormatoDao.insertRow(tipoFormato);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoFormatoDTO putTipoFormato(@RequestBody TipoFormatoDTO tipoFormato) throws CustomException {
        if (tipoFormato.getIdTipoFormato() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_tipo_formato"));
        }
        if (tipoFormato.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        return tipoFormatoDao.updateRow(tipoFormato);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipoFormatoDTO deleteTipoFormato(@RequestBody TipoFormatoDTO tipoFormato) throws CustomException {
        if (tipoFormato.getIdTipoFormato() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_tipo_formato"));
        }
        return tipoFormatoDao.deleteRow(tipoFormato);
    }
}
