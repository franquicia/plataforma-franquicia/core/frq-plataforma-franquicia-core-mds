/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta Depura Visitas", value = "RespuestDepuraVisitas")
public class RespuestaDepuraVisitasDTO {

    @JsonProperty(value = "respuestaEjecucion")
    @ApiModelProperty(notes = "Indicador de respuesta de la ejecución.", example = "1")
    private Integer respuestaEjecucion;

    @JsonProperty(value = "envioCorreo")
    @ApiModelProperty(notes = "Indicador del envío de notificación por correo electrónico.", example = "true")
    private Boolean envioCorreo;

    public Integer getRespuestaEjecucion() {
        return respuestaEjecucion;
    }

    public void setRespuestaEjecucion(Integer respuestaEjecucion) {
        this.respuestaEjecucion = respuestaEjecucion;
    }

    public Boolean getEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(Boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    @Override
    public String toString() {
        return "RespuestaDepuraVisitasDTO{" + "respuestaEjecucion=" + respuestaEjecucion + ", envioCorreo=" + envioCorreo + '}';
    }

}
