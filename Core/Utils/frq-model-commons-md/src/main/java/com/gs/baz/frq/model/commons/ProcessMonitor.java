/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import org.springframework.util.StopWatch;

/**
 *
 * @author cescobarh
 */
public class ProcessMonitor {

    private StopWatch stopWatch = new StopWatch();
    private StringBuilder infoTask = new StringBuilder();

    public ProcessMonitor() {
    }

    public ProcessMonitor(String id) {
        this.stopWatch = new StopWatch(id);
    }

    public ProcessMonitor newProcessMonitor(String id) {
        return new ProcessMonitor(id);
    }

    public void startWatch(String process) {
        if (stopWatch.isRunning()) {
            stopWatch.stop();
        }
        this.stopWatch.start(process);
    }

    public void stopWatch() {
        if (stopWatch.isRunning()) {
            this.stopWatch.stop();
        }
    }

    public void resumeInfo() {
        infoTask = new StringBuilder();
        infoTask.append("\nStopWatch '").append(stopWatch.getId()).append("': running time = ").append(stopWatch.getTotalTimeSeconds()).append(" seconds \n");
        infoTask.append("---------------------------------\n");
        infoTask.append("  Seconds    |     Task Name     \n");
        infoTask.append("---------------------------------\n");
        for (StopWatch.TaskInfo itemTask : stopWatch.getTaskInfo()) {
            infoTask.append(itemTask.getTimeSeconds()).append("  |  ").append(itemTask.getTaskName()).append("\n");
        }
        infoTask.append("---------------------------------\n");
        System.out.println(infoTask);
        if (stopWatch.isRunning()) {
            stopWatch.stop();
        }
    }
}
