/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Lista de calificaciones por ceco", value = "CalificacionesCeco")
public class CalificacionesCeco {

    @JsonProperty(value = "calificacionesCentroCostos")
    @ApiModelProperty(notes = "calificacionesCentroCostos")
    private List<CalificacionCeco> calificacionCeco;

    public CalificacionesCeco(List<CalificacionCeco> calificacionCeco) {
        this.calificacionCeco = calificacionCeco;
    }

    public List<CalificacionCeco> getCalificacionCeco() {
        return calificacionCeco;
    }

    public void setCalificacionCeco(List<CalificacionCeco> CalificacionCeco) {
        this.calificacionCeco = CalificacionCeco;
    }

    @Override
    public String toString() {
        return "CalificacionesCeco{" + "CalificacionCeco=" + calificacionCeco + '}';
    }
}
