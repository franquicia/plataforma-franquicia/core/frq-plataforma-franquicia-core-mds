package com.gs.baz.frq.relacion.protocolo.checklist.mprs;

import com.gs.baz.frq.relacion.protocolo.checklist.dto.RelacionProtocoloChecklistDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RelacionProtocoloChecklistRowMapper implements RowMapper<RelacionProtocoloChecklistDTO> {

    private RelacionProtocoloChecklistDTO relacionProtocoloChecklist;

    @Override
    public RelacionProtocoloChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        relacionProtocoloChecklist = new RelacionProtocoloChecklistDTO();
        relacionProtocoloChecklist.setIdProtocolo(((BigDecimal) rs.getObject("FIID_PROTOCOLO")));
        relacionProtocoloChecklist.setIdChecklist(((BigDecimal) rs.getObject("FIID_CHECKLIST")));
        relacionProtocoloChecklist.setFechaInicio(rs.getString("FDFECHAINICIO"));
        relacionProtocoloChecklist.setModUsuario((rs.getString("FCUSUARIO_MOD")));
        relacionProtocoloChecklist.setModFecha(rs.getString("FDFECHA_MOD"));
        relacionProtocoloChecklist.setVigente(((BigDecimal) rs.getObject("FIVIGENTE")));
        return relacionProtocoloChecklist;
    }
}
