/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.oracle.dao;

import com.gs.baz.frq.oracle.dto.Oracle;
import com.gs.baz.frq.oracle.dto.ParamType;
import com.gs.baz.frq.oracle.dto.Parameter;
import com.gs.baz.frq.oracle.dto.Properties;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;

/**
 *
 * @author cescobarh
 */
public class OracleBlock extends StoredProcedure {

    private final List<Parameter> parameters;

    public OracleBlock(DataSource dataSource, Oracle anonymusPLSQL) {
        Properties properties = anonymusPLSQL.getProperties();

        this.parameters = anonymusPLSQL.getRegisterParameters();

        Integer queryTimeout = properties.getQueryTimeout();

        if (queryTimeout != null && !queryTimeout.equals(0)) {
            this.setQueryTimeout(queryTimeout);
        }

        this.setDataSource(dataSource);
        this.setSql(anonymusPLSQL.getPlSQL());

        parameters.forEach(param -> {
            if (param.getParamType().equals(ParamType.IN)) {
                this.declareParameter(new SqlParameter(param.getName(), param.getOracleDataType().indexValue()));
            } else {
                this.declareParameter(new SqlOutParameter(param.getName(), param.getOracleDataType().indexValue()));
            }
        });
    }

    public Map<String, Object> executePLSQLBlock() {
        Map<String, Object> numberMapping = new HashMap<>();
        parameters.forEach(param -> {
            if (param.getParamType().equals(ParamType.IN)) {
                numberMapping.put(param.getName(), param.getValue());
            }
        });
        return this.execute(numberMapping);
    }

    @Override
    public String getCallString() {
        return getSql();
    }

    @Override
    public boolean isSqlReadyForUse() {
        return true;
    }
}
