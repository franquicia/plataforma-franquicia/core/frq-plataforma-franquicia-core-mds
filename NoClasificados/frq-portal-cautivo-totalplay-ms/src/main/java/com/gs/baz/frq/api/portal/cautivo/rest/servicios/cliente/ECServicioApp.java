/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.rest.servicios.cliente;

import com.fasterxml.jackson.databind.JsonNode;
import com.gs.baz.frq.api.portal.cautivo.dto.AppDTO;
import com.gs.baz.frq.api.portal.cautivo.util.BaseUrl;
import com.gs.baz.frq.api.portal.cautivo.util.DefaultRestTemplate;
import com.gs.baz.frq.api.portal.cautivo.util.Relasionship;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author MADA
 */
public class ECServicioApp extends DefaultRestTemplate {

    private final Logger logger = LogManager.getLogger();

    public List<AppDTO> getListaClientes(String tipo, String startTime, String endTime, String noEco) throws CustomException {

        try {

            String url = Relasionship.APP.url(BaseUrl.va2.getBaseUrl());
            System.out.println("url  " + url);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                    .queryParam("tipo", tipo);
            System.out.println("tipo  " + tipo);
            if (startTime != null) {
                builder = builder.queryParam("fechaIni", startTime);
            }
            System.out.println("startTime  " + startTime);
            if (endTime != null) {
                builder = builder.queryParam("fechaFin", endTime);
            }
            System.out.println("endTime  " + endTime);
            builder = builder.queryParam("noEco", noEco);
            System.out.println("noEco  " + noEco);
            System.out.println("builder  " + builder.toUriString());
            System.out.println("httpEntity  " + httpEntity.toString());
            System.out.println("restTemplate  " + restTemplate.toString());
            responseString = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, String.class);
            System.out.println("responseString  " + responseString);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("data") != null) {
                    List<AppDTO> clientes = objectMapper.readValue(responseStringBody.get("data").toString(), objectMapper.getTypeFactory().constructCollectionLikeType(List.class, AppDTO.class));
                    return clientes;
                } else {
                    throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE.detalle("Error on data service"));
                }
            } else {
                throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE);
            }

        } catch (IOException | HttpServerErrorException ex) {
            //throw new CustomException(ex);
        }

        return null;

    }

    /*public PaginacionDTO getPaginacionClientes(Long ownerId, Integer page, Integer pageSize, String startTime, String endTime) throws CustomException {

        try {

            String url = Relasionship.CLIENTS.url(BaseUrl.va2.getBaseUrl(), Version.v1.getNumero());

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                    .queryParam("ownerId", ownerId)
                    .queryParam("page", page)
                    .queryParam("pageSize", pageSize);

            if (startTime != null) {
                builder = builder.queryParam("startTime", startTime);
            }

            if (endTime != null) {
                builder = builder.queryParam("endTime", endTime);
            }

            responseString = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, String.class);

            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("data") != null) {
                    PaginacionDTO paginacionDTO = objectMapper.readValue(responseStringBody.get("pagination").toString(), PaginacionDTO.class);
                    return paginacionDTO;
                } else {
                    //throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE.detalle("Error on data service"));
                }
            } else {
                //throw new CustomException(ModelCodes.ERROR_RESPONSE_SERVICE);
            }

        } catch (IOException | HttpServerErrorException ex) {
            //throw new CustomException(ex);
        }

        return null;

    }

     */
}
