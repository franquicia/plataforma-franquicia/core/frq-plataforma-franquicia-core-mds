/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos base de rango", value = "RangoBase")
public class RangoBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion nombre del rango", example = "Rango")
    private String descripcion;

    @JsonProperty(value = "limiteInferior")
    @ApiModelProperty(notes = "Limite inferior del rango", example = "0")
    private Integer limiteInferior;

    @JsonProperty(value = "limiteSuperior")
    @ApiModelProperty(notes = "Limite superior del rango", example = "100")
    private Integer limiteSuperior;

    @JsonProperty(value = "color")
    @ApiModelProperty(notes = "Color del rango", example = "red")
    private String color;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(Integer limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public Integer getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(Integer limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "RangoBase{" + "descripcion=" + descripcion + ", limiteInferior=" + limiteInferior + ", limiteSuperior=" + limiteSuperior + ", color=" + color + '}';
    }

}
