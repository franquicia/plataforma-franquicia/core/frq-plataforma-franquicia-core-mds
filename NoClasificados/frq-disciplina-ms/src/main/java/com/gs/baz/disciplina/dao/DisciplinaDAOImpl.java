package com.gs.baz.disciplina.dao;

import com.gs.baz.disciplina.dao.util.GenericDAO;
import com.gs.baz.disciplina.dto.DisciplinaDTO;
import com.gs.baz.disciplina.mprs.DisciplinaRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DisciplinaDAOImpl extends DefaultDAO implements GenericDAO<DisciplinaDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelectDisciplina;
    private DefaultJdbcCall jdbcSelectByName;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINDISCIP");
        jdbcInsert.withProcedureName("SP_INS_DISCIPLINA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINDISCIP");
        jdbcUpdate.withProcedureName("SP_ACT_DISCIPLINAS");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINDISCIP");
        jdbcDelete.withProcedureName("SP_DEL_DISC");

        jdbcSelectDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectDisciplina.withSchemaName(schema);
        jdbcSelectDisciplina.withCatalogName("PAADMINDISCIP");
        jdbcSelectDisciplina.withProcedureName("SP_SEL_DISCIPLINAS");
        jdbcSelectDisciplina.returningResultSet("RCL_INFO", new DisciplinaRowMapper());

        jdbcSelectByName = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectByName.withSchemaName(schema);
        jdbcSelectByName.withCatalogName("PAADMINDISCIP");
        jdbcSelectByName.withProcedureName("SP_DISCIPLINA_NOMBRE");
        jdbcSelectByName.returningResultSet("RCL_INFO", new DisciplinaRowMapper());
    }

    @Override
    public DisciplinaDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDDISCI", entityID);
        Map<String, Object> out = jdbcSelectDisciplina.execute(mapSqlParameterSource);
        List<DisciplinaDTO> data = (List<DisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<DisciplinaDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDDISCI", null);
        Map<String, Object> out = jdbcSelectDisciplina.execute(mapSqlParameterSource);
        List<DisciplinaDTO> data = (List<DisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public DisciplinaDTO insertRow(DisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdDisciplina((BigDecimal) out.get("PA_FIIDDISCI"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Disciplina "));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Disciplina "), ex);
        }
    }

    @Override
    public DisciplinaDTO updateRow(DisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDDISCI", entityDTO.getIdDisciplina());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error  not success to update row of Disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Disciplina"), ex);
        }
    }

    @Override
    public DisciplinaDTO deleteRow(DisciplinaDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDDISCI", entityDTO.getIdDisciplina());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error  not success to delete row of Disciplina"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error  to delete row of Disciplina"), ex);
        }
    }

    @Override
    public List<DisciplinaDTO> selectRowsByName(String name) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCNOMBREDISCIPL", name);
        Map<String, Object> out = jdbcSelectByName.execute(mapSqlParameterSource);
        List<DisciplinaDTO> data = (List<DisciplinaDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

}
