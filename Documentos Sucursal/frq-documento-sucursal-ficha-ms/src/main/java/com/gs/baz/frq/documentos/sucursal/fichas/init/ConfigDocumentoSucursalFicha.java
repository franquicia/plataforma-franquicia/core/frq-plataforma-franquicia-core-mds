package com.gs.baz.frq.documentos.sucursal.fichas.init;

import com.gs.baz.frq.documentos.sucursal.fichas.dao.DocumentoSucursalFichaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.documentos.sucursal.fichas")
public class ConfigDocumentoSucursalFicha {

    private final Logger logger = LogManager.getLogger();

    public ConfigDocumentoSucursalFicha() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DocumentoSucursalFichaDAOImpl documentoSucursalFichaDAOImpl() {
        return new DocumentoSucursalFichaDAOImpl();
    }

}
