/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Total de Centros de costos por bandeja de un proveedor", value = "ConteoProveedor")
public class DashboardProveedor {

    

    @JsonProperty(value = "idEstado")
    @ApiModelProperty(notes = "Identificador del estado de carga", example = "1", position = -1)
    private Integer idEstado;
    
    @JsonProperty(value = "nombreEstado")
    @ApiModelProperty(notes = "Nombre del estado o bandeja al que pertenece el centro de costos", example = "PENDIENTE DE VALIDACION VISUAL", position = -2)
    private String nombreEstado;
    
    @JsonProperty(value = "total")
    @ApiModelProperty(notes = "Total de centros de costos ubicados en la bandeja", example = "5", position = -3)
    private Integer total;

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "DashboardProveedor{" + "idEstado=" + idEstado + ", nombreEstado=" + nombreEstado + ", total=" + total + '}';
    }
    
}
