/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dao.parametros;

import com.gs.baz.frq.dashboard.dto.parametros.ParametroDTO;
import com.gs.baz.frq.dashboard.mprs.parametros.ParametrosRowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author MADA
 */
public class ParametrosDAOImpl extends DefaultDAO {

    private final Logger logger = LogManager.getLogger();
    private String schema;
    private DefaultJdbcCall jdbcSelectByParam;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectByParam = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectByParam.withSchemaName(schema);
        jdbcSelectByParam.withCatalogName("PA_ADM_PARAM");
        jdbcSelectByParam.withProcedureName("SP_SEL_PARAM");
        jdbcSelectByParam.returningResultSet("RCL_PARAM", new ParametrosRowMapper());

    }

    public ParametroDTO selectRow(String clave) {

        ParametroDTO parametroDTO = new ParametroDTO();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_CVE_PARAM", clave);

        Map<String, Object> out = jdbcSelectByParam.execute(mapSqlParameterSource);

        int error = ((BigDecimal) out.get("PA_ERROR")).intValue();

        if (error == 0) {
            return ((List<ParametroDTO>) out.get("RCL_PARAM")).get(0);
        }

        return parametroDTO;
    }

}
