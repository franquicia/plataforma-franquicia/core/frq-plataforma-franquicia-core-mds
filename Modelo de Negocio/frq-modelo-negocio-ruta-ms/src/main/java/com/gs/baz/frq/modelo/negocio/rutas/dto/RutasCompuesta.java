/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de rutas del modelo de negocio", value = "rutas")
public class RutasCompuesta {

    @JsonProperty(value = "rutas")
    @ApiModelProperty(notes = "rutas")
    private List<RutaCompuesta> rutas;

    public RutasCompuesta(List<RutaCompuesta> rutas) {
        this.rutas = rutas;
    }

    public List<RutaCompuesta> getRutas() {
        return rutas;
    }

    public void setRutas(List<RutaCompuesta> rutas) {
        this.rutas = rutas;
    }

}
