package com.gs.baz.frq.asignacion.mprs;

import com.gs.baz.frq.asignacion.dto.AsignacionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FasesRowMapper implements RowMapper<AsignacionDTO> {

    private AsignacionDTO status;

    @Override
    public AsignacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionDTO();

        status.setProyecto((BigDecimal) rs.getObject("FIID_PROYECTO"));
        status.setNombreProyecto(rs.getString("NOMBRE_PROYECTO"));
        status.setIdFase(rs.getInt("FIID_FASE"));
        status.setNombreFase(rs.getString("NOMBRE_FASE"));
        status.setOrdenFase(rs.getInt("FIID_ORDENFASE"));
        status.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
        status.setFaseActual(rs.getInt("FIID_FASEACT"));
        status.setBanderaFase(rs.getBoolean("FASE_ACTUAL"));
        //status.setIdCargaFin(rs.getInt("FCEDOHALLAFIN"));
        status.setVersion(rs.getInt("FIVERSION"));
        status.setPeriodo(rs.getString("FCPERIODO"));

        return status;
    }
}
