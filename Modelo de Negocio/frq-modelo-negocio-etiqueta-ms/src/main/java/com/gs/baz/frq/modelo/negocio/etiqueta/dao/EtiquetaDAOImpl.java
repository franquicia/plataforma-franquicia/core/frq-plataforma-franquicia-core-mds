/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.etiqueta.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.AltaEtiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.Etiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.EtiquetaBase;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.FolioEtiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.dto.ParametrosEtiqueta;
import com.gs.baz.frq.modelo.negocio.etiqueta.mprs.EtiquetaRowMapper;
import com.gs.baz.frq.modelo.negocio.etiqueta.mprs.EtiquetasRelacionadasRowMapper;
import com.gs.baz.frq.modelo.negocio.etiqueta.mprs.EtiquetasRowMapper;
import com.gs.baz.frq.modelo.negocio.etiqueta.mprs.FoliosEtiquetaRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author carlos
 */
public class EtiquetaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectEtiqueta;
    private DefaultJdbcCall jdbcSelectFoliosEtiqueta;
    private DefaultJdbcCall jdbcSelectEtiquetasRelacionadas;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelectEtiquetasRelacionadasFolios;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        String catalogo = "PAADMMNETIQUET";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSMNETIQUETA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTMNETIQUETA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELMNETIQUETA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETMNETIQUETA");
        jdbcSelect.returningResultSet("PA_CDATOS", new EtiquetaRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETMNETIQUETA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new EtiquetasRowMapper());

        jdbcSelectEtiqueta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectEtiqueta.withSchemaName(schema);
        jdbcSelectEtiqueta.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectEtiqueta.withProcedureName("SPGETETIQUETAS");
        jdbcSelectEtiqueta.returningResultSet("PA_CDATOS", new EtiquetasRowMapper());

        jdbcSelectFoliosEtiqueta = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFoliosEtiqueta.withSchemaName(schema);
        jdbcSelectFoliosEtiqueta.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectFoliosEtiqueta.withProcedureName("SPGETVISUADOCTO");
        jdbcSelectFoliosEtiqueta.returningResultSet("PA_CDATOS", new FoliosEtiquetaRowMapper());

        jdbcSelectEtiquetasRelacionadas = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectEtiquetasRelacionadas.withSchemaName(schema);
        jdbcSelectEtiquetasRelacionadas.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectEtiquetasRelacionadas.withProcedureName("SPSUGIETIQUETAS");
        jdbcSelectEtiquetasRelacionadas.returningResultSet("PA_CDATOS", new EtiquetasRelacionadasRowMapper());

        jdbcSelectEtiquetasRelacionadasFolios = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectEtiquetasRelacionadasFolios.withSchemaName(schema);
        jdbcSelectEtiquetasRelacionadasFolios.withCatalogName("PAMNCONULTDOCTO");
        jdbcSelectEtiquetasRelacionadasFolios.withProcedureName("SPGETETIQXFOLIO");
        jdbcSelectEtiquetasRelacionadasFolios.returningResultSet("PA_CDATOS", new EtiquetasRelacionadasRowMapper());

    }

    public EtiquetaBase selectRow(Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDETIQUETA", idEtiqueta);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<EtiquetaBase> data = (List<EtiquetaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Etiqueta> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDETIQUETA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Etiqueta>) out.get("PA_CDATOS");
    }

    public AltaEtiqueta insertRow(ParametrosEtiqueta entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_NRES_FIIDETIQUETA");
                return new AltaEtiqueta(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosEtiqueta entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDETIQUETA", entityDTO.getIdEtiqueta());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDETIQUETA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

    public Etiqueta selectEtiqueta(String descripcionEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ETIQUETA", descripcionEtiqueta);
        Map<String, Object> out = jdbcSelectEtiqueta.execute(mapSqlParameterSource);
        List<Etiqueta> data = (List<Etiqueta>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<FolioEtiqueta> selectFoliosEtiqueta(Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ETIQUETA", idEtiqueta);
        Map<String, Object> out = jdbcSelectFoliosEtiqueta.execute(mapSqlParameterSource);
        return (List<FolioEtiqueta>) out.get("PA_CDATOS");
    }

    public List<Etiqueta> selectEtiquetasRelacionadas(Long idEtiqueta) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ETIQUETA", idEtiqueta);
        Map<String, Object> out = jdbcSelectEtiquetasRelacionadas.execute(mapSqlParameterSource);
        return (List<Etiqueta>) out.get("PA_CDATOS");
    }

    public List<Etiqueta> selectEtiquetasRelacionadasFolios(Long idFolio) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDFOLIO", idFolio);
        Map<String, Object> out = jdbcSelectEtiquetasRelacionadasFolios.execute(mapSqlParameterSource);
        return (List<Etiqueta>) out.get("PA_CDATOS");
    }
}
