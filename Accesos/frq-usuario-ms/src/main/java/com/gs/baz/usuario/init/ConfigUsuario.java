package com.gs.baz.usuario.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import com.gs.baz.usuario.dao.FlujoPerfilUsuarioDAOImpl;
import com.gs.baz.usuario.dao.PerfilUsuarioDAOImpl;
import com.gs.baz.usuario.dao.UsuarioDAOImpl;
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Configuration
@Import(ConfigBucketServices.class)
@ComponentScan("com.gs.baz.usuario")
public class ConfigUsuario {

    private final Logger logger = LogManager.getLogger();

    public ConfigUsuario() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public UsuarioDAOImpl usuarioDAOImpl() {
        return new UsuarioDAOImpl();
    }

    @Bean(initMethod = "init")
    public PerfilUsuarioDAOImpl perfilUsuarioDAOImpl() {
        return new PerfilUsuarioDAOImpl();
    }

    @Bean(initMethod = "init")
    public FlujoPerfilUsuarioDAOImpl flujoPerfilUsuarioDAOImpl() {
        return new FlujoPerfilUsuarioDAOImpl();
    }

}
