/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.filters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.Specifications;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorServer;
import com.gs.baz.frq.swagger.responses.RespuestaErrorValidationClient;
import com.gs.baz.security.access.bi.RequestSecurityWrapper;
import com.gs.baz.security.access.dao.SecurityAccessDAOImpl;
import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import com.gs.baz.security.access.util.DecryptException;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE - 10)
public class RequestSecurityFilter extends OncePerRequestFilter {

    @Autowired
    private SecurityAccessDAOImpl securityAccessDAOImpl;

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        Boolean sensibleFieldsError = (Boolean) httpServletRequest.getAttribute("sensible_fields_error");
        Boolean rsaKeysError = (Boolean) httpServletRequest.getAttribute("rsa_keys_error");
        Boolean idAccesoRequiredError = (Boolean) httpServletRequest.getAttribute("id_acceso_required_error");
        if (idAccesoRequiredError != null && idAccesoRequiredError) {
            ResponseApiCodes apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04005;
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> erroresDetalles = new ArrayList<>();
            erroresDetalles.add(new ErrorDetalle("El idAcceso es requerido para esta operación"));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            httpServletResponse.setStatus(responseMessage.getCode());
            OutputStream out = httpServletResponse.getOutputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, respuestaErrorClient);
            out.flush();
        } else if (sensibleFieldsError != null && sensibleFieldsError) {
            ResponseApiCodes apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05005;
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(500);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> erroresDetalles = new ArrayList<>();
            erroresDetalles.add(new ErrorDetalle("Los atributos sensibles no pudieron ser consultados"));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            httpServletResponse.setStatus(responseMessage.getCode());
            OutputStream out = httpServletResponse.getOutputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, respuestaErrorClient);
            out.flush();
        } else if (rsaKeysError != null && rsaKeysError) {
            ResponseApiCodes apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04005;
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            ResponseMessage responseMessage = ResponseMensages.getByStatus(400);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            List<ErrorDetalle> erroresDetalles = new ArrayList<>();
            erroresDetalles.add(new ErrorDetalle("El idAcceso no pudo ser verificado"));
            RespuestaAPI respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
            httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            httpServletResponse.setStatus(responseMessage.getCode());
            OutputStream out = httpServletResponse.getOutputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, respuestaErrorClient);
            out.flush();
        } else {
            try {
                httpServletRequest = new RequestSecurityWrapper(httpServletRequest, httpServletResponse);
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } catch (CustomException ex) {
                Throwable cause = ex.getCause();
                ResponseApiCodes apiCode;
                ResponseMessage responseMessage;
                RespuestaAPI respuestaErrorClient;
                String requestID = (String) httpServletRequest.getAttribute("requestID");
                String nameApi = (String) httpServletRequest.getAttribute("nameApi");
                if (cause instanceof DecryptException) {
                    DecryptException decryptException = (DecryptException) cause;
                    apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05005;
                    responseMessage = ResponseMensages.getByStatus(500);
                    String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
                    List<ErrorDetalle> erroresDetalles = new ArrayList<>();
                    erroresDetalles.add(new ErrorDetalle("El atributo '" + decryptException.getField() + "' no pudo ser descifrado"));
                    respuestaErrorClient = new RespuestaErrorValidationClient(codeError, responseMessage.getMessage(), requestID, erroresDetalles);
                } else {
                    apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05005;
                    responseMessage = ResponseMensages.getByStatus(500);
                    String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
                    respuestaErrorClient = new RespuestaErrorServer(codeError, responseMessage.getMessage(), requestID);
                }
                httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                httpServletResponse.setStatus(responseMessage.getCode());
                OutputStream out = httpServletResponse.getOutputStream();
                ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(out, respuestaErrorClient);
                out.flush();
            }
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest httpServletRequest) throws ServletException {
        String uri = httpServletRequest.getRequestURI();
        if (uri.contains("aplicaciones/llaves") || uri.contains("oauth/token")) {
            return true;
        }
        String idAcceso = httpServletRequest.getHeader("x-idAcceso");
        Boolean hasSensibleData = false;
        if (securityAccessDAOImpl == null) {
            ServletContext servletContext = httpServletRequest.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            if (webApplicationContext != null) {
                securityAccessDAOImpl = webApplicationContext.getBean(SecurityAccessDAOImpl.class);
                requestMappingHandlerMapping = webApplicationContext.getBean(RequestMappingHandlerMapping.class);
            }
        }
        if (requestMappingHandlerMapping != null) {
            try {
                HandlerExecutionChain handlerExecutionChain = requestMappingHandlerMapping.getHandler(httpServletRequest);
                if (handlerExecutionChain != null) {
                    HandlerMethod handlerMethod = (HandlerMethod) handlerExecutionChain.getHandler();
                    if (handlerMethod != null) {
                        List<String> atributosSensiblesEntrada = new ArrayList<>();
                        List<String> atributosSensiblesSalida = new ArrayList<>();
                        ApiImplicitParams apiImplicitParams = handlerMethod.getMethod().getAnnotation(ApiImplicitParams.class);
                        if (apiImplicitParams != null) {
                            for (ApiImplicitParam itemApiImplicitParam : apiImplicitParams.value()) {
                                String name = itemApiImplicitParam.name();
                                if (name.equals("x-idAcceso")) {
                                    hasSensibleData = true;
                                    break;
                                }
                            }
                            Parameter[] methodParameters = handlerMethod.getMethod().getParameters();
                            Class<?> returnType = handlerMethod.getMethod().getReturnType().getClass();
                            for (Parameter itemMethodParameters : methodParameters) {
                                for (Field itemField : itemMethodParameters.getType().getDeclaredFields()) {
                                    Specifications specifications = itemField.getAnnotation(Specifications.class);
                                    if (specifications != null && specifications.isSensible()) {
                                        JsonProperty jsonProperty = itemField.getAnnotation(JsonProperty.class);
                                        atributosSensiblesEntrada.add(jsonProperty.value());
                                    }
                                }
                            }
                            Type genericReturnType = handlerMethod.getMethod().getGenericReturnType();
                            if (genericReturnType instanceof ParameterizedType) {
                                ParameterizedType type = (ParameterizedType) genericReturnType;
                                Type[] typeArguments = type.getActualTypeArguments();
                                for (Type typeArgument : typeArguments) {
                                    Class typeArgClass = (Class) typeArgument;
                                    for (Field itemField : typeArgClass.getDeclaredFields()) {
                                        Specifications specifications = itemField.getAnnotation(Specifications.class);
                                        if (specifications != null && specifications.isSensible()) {
                                            JsonProperty jsonProperty = itemField.getAnnotation(JsonProperty.class);
                                            atributosSensiblesSalida.add(jsonProperty.value());
                                        }
                                    }
                                }
                            } else {
                                for (Field itemField : returnType.getDeclaredFields()) {
                                    Specifications specifications = itemField.getAnnotation(Specifications.class);
                                    if (specifications != null && specifications.isSensible()) {
                                        JsonProperty jsonProperty = itemField.getAnnotation(JsonProperty.class);
                                        atributosSensiblesSalida.add(jsonProperty.value());
                                    }
                                }
                            }
                            httpServletRequest.setAttribute("sensible_attributes_in", atributosSensiblesEntrada);
                            httpServletRequest.setAttribute("sensible_attributes_out", atributosSensiblesSalida);

                        }
                    }
                }
            } catch (Exception ex) {

            }
        }
        httpServletRequest.setAttribute("has_sensible_attributes", hasSensibleData);
        if (hasSensibleData) {
            if (idAcceso != null) {
                try {
                    if (securityAccessDAOImpl != null) {
                        AccesoAsimetricoDTO rsaAccesoDTO = securityAccessDAOImpl.selectRow(idAcceso);
                        if (rsaAccesoDTO == null) {
                            throw new CustomException(ModelCodes.DATA_NOT_FOUND);
                        }
                        httpServletRequest.setAttribute("rsa_keys", rsaAccesoDTO);
                    } else {
                        throw new CustomException(new ServletException("Bean SecurityAccessDAOImpl null"));
                    }
                } catch (CustomException ex) {
                    httpServletRequest.setAttribute("rsa_keys_error", true);
                }
            } else {
                httpServletRequest.setAttribute("id_acceso_required_error", true);
            }
        }
        return false;
    }

}
