/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cescobarh
 */
public class ParametrosGraficaSemaforo {

    @JsonProperty(value = "idGrafica")
    private transient Integer idGrafica;

    @JsonProperty(value = "idSemaforo")
    @ApiModelProperty(notes = "Identificador del semaforo", example = "1")
    @NotNull
    private Integer idSemaforo;

    @JsonProperty(value = "regla")
    @ApiModelProperty(notes = "Regla de la grafica semaforo", example = "Defecto")
    @NotEmpty
    private String regla;

    public Integer getIdGrafica() {
        return idGrafica;
    }

    public void setIdGrafica(Integer idGrafica) {
        this.idGrafica = idGrafica;
    }

    public Integer getIdSemaforo() {
        return idSemaforo;
    }

    public void setIdSemaforo(Integer idSemaforo) {
        this.idSemaforo = idSemaforo;
    }

    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    @Override
    public String toString() {
        return "ParametrosGraficaSemaforo{" + "idGrafica=" + idGrafica + ", idSemaforo=" + idSemaforo + ", regla=" + regla + '}';
    }

}
