/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.handler.reqres.ex;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class AttributeMissingException extends Exception {

    private List<String> attributes;

    public AttributeMissingException(String message, List<String> attributes) {
        super(message);
        this.attributes = attributes;
    }

    public AttributeMissingException(String message, Throwable cause) {
        super(message, cause);
    }

    public AttributeMissingException(Throwable cause) {
        super(cause);
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

}
