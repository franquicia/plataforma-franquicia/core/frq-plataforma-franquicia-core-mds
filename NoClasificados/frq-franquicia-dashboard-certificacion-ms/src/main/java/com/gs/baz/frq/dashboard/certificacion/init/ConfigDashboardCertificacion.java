package com.gs.baz.frq.dashboard.certificacion.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g001.DashboardCertificacionG001DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g002.DashboardCertificacionG002DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g003.DashboardCertificacionG003DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g004.DashboardCertificacionG004DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g005.DashboardCertificacionG005DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g006.DashboardCertificacionG006DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g007.DashboardCertificacionG007DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.punto.venta.DashboardCertificacionPuntoVentaDAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g010.DashboardCertificacionG010DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g011.DashboardCertificacionG011DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g012.DashboardCertificacionG012DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g013.DashboardCertificacionG013DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.graficos.g014.DashboardCertificacionG014DAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.consulta.buscador.DashboardCertificacionConsultaBuscadorDAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dao.reportes.DashboardCertificacionReporteCoberturaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.dashboard.certificacion")
public class ConfigDashboardCertificacion {

    private final Logger logger = LogManager.getLogger();

    public ConfigDashboardCertificacion() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG001DAOImpl dashboardCertificacionG001DAOImpl() {
        return new DashboardCertificacionG001DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG002DAOImpl dashboardCertificacionG002DAOImpl() {
        return new DashboardCertificacionG002DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG003DAOImpl dashboardCertificacionG003DAOImpl() {
        return new DashboardCertificacionG003DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG004DAOImpl dashboardCertificacionG004DAOImpl() {
        return new DashboardCertificacionG004DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG005DAOImpl dashboardCertificacionG005DAOImpl() {
        return new DashboardCertificacionG005DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG006DAOImpl dashboardCertificacionG006DAOImpl() {
        return new DashboardCertificacionG006DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG007DAOImpl dashboardCertificacionG007DAOImpl() {
        return new DashboardCertificacionG007DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionPuntoVentaDAOImpl dashboardCertificacionPuntoVentaDAOImpl() {
        return new DashboardCertificacionPuntoVentaDAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG010DAOImpl dashboardCertificacionG010DAOImpl() {
        return new DashboardCertificacionG010DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG011DAOImpl dashboardCertificacionG011DAOImpl() {
        return new DashboardCertificacionG011DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG012DAOImpl dashboardCertificacionG012DAOImpl() {
        return new DashboardCertificacionG012DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG013DAOImpl dashboardCertificacionG013DAOImpl() {
        return new DashboardCertificacionG013DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionG014DAOImpl dashboardCertificacionG014DAOImpl() {
        return new DashboardCertificacionG014DAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionConsultaBuscadorDAOImpl dashboardCertificacionConsultaBuscadorDAOImpl() {
        return new DashboardCertificacionConsultaBuscadorDAOImpl();
    }

    @Bean(initMethod = "init")
    public DashboardCertificacionReporteCoberturaDAOImpl dashboardCertificacionReporteCoberturaDAOImpl() {
        return new DashboardCertificacionReporteCoberturaDAOImpl();
    }
}
