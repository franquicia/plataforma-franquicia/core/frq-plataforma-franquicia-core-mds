/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosCeco {

    @JsonProperty(value = "idNegocio", required = true)
    @ApiModelProperty(notes = "Identificador del negocio", example = "1", required = true)
    private Integer idNegocio;

    @JsonProperty(value = "ceco", required = true)
    @ApiModelProperty(notes = "Compuesto de nombre y descripcion de ceco", example = "920100 MEGA LA LUNA", required = true)
    private String ceco;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

}
