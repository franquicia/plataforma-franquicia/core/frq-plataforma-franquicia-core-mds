/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.disciplina.rest;

import com.gs.baz.frq.disciplina.dao.DisciplinaDAOImpl;
import com.gs.baz.frq.disciplina.dto.DisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/disciplina")
@Component("FRQServiceDisciplina")
public class ServiceDisciplina {

    @Autowired
    private DisciplinaDAOImpl disciplinaDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DisciplinaDTO> getDisciplinas() throws CustomException {
        return disciplinaDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_disciplina}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO getDisciplina(@PathVariable("id_disciplina") Long id_disciplina_franquicia) throws CustomException {
        return disciplinaDAOImpl.selectRow(id_disciplina_franquicia);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO postDisciplina(@RequestBody DisciplinaDTO disciplinaFranquicia) throws CustomException {
        if (disciplinaFranquicia.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (disciplinaFranquicia.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (disciplinaFranquicia.getBanderaGek() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("bandera_gek"));
        }
        return disciplinaDAOImpl.insertRow(disciplinaFranquicia);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO putDisciplina(@RequestBody DisciplinaDTO disciplinaFranquicia) throws CustomException {
        if (disciplinaFranquicia.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        if (disciplinaFranquicia.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("descripcion"));
        }
        if (disciplinaFranquicia.getBanderaGek() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("bandera_gek"));
        }
        return disciplinaDAOImpl.updateRow(disciplinaFranquicia);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DisciplinaDTO deleteDisciplina(@RequestBody DisciplinaDTO disciplinaFranquicia) throws CustomException {
        if (disciplinaFranquicia.getIdDisciplina() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_disciplina"));
        }
        return disciplinaDAOImpl.deleteRow(disciplinaFranquicia);
    }
}
