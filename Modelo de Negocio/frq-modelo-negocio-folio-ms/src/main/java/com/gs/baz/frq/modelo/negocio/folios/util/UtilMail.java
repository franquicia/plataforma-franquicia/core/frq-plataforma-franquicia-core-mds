package com.gs.baz.frq.modelo.negocio.folios.util;

import java.io.File;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

public class UtilMail {

    public static final String CTE_HOST_CORREO = "10.63.200.79"; // Productivo
    public static final String CTE_REMITENTE_SG = "plataforma-franquicia@bancoazteca.com.mx";

    public static boolean enviaCorreo(String destinatario, List<String> copiados, String asunto, String cuerpoCorreo,
            File adjunto) {

        /*
		 * logger.info("DESTINTATARIO ---- " + destinatario);
		 * logger.info("COPIADOS ---- " + copiados); logger.info("ASUNTO ---- " +
		 * asunto);
         */
        // logger.info("CUERPO CORREO ---- " + cuerpoCorreo);
        // log("LLEGANDO A METOD ENVIA MAIL.....");
        String mensaje = "";
        try {
            mensaje = cuerpoCorreo;
            // Propiedades de la conexion
            String host = CTE_HOST_CORREO;
            String from = CTE_REMITENTE_SG;
            Properties props = new Properties();
            props.put("mail.smtp.host", host);
            try {
                Session session = Session.getInstance(props, null);
                MimeMessage msg = new MimeMessage(session);
                // Se setea la direccion de la que viene el correo
                msg.setFrom(new InternetAddress(from, "Franquicia - Modelo - Negocio"));
                // Se setea para quién va el correo
                msg.setRecipients(Message.RecipientType.TO, destinatario);
                // Se setea para quienes se copia el correo
                String copia = "";
                if (copiados != null) {
                    for (String dest : copiados) {
                        copia += dest + ",";
                    }
                    if (copia != null && copia != "") {
                        msg.setRecipients(Message.RecipientType.CC, copia);
                    }
                }
                // Se setea el Asunto del correo
                msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
                // Se setea el mensaje y el tipo de texto
                // creates body part for the message
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setContent(cuerpoCorreo, "text/html");
                msg.setContent(mensaje, "text/html; charset=iso-8859-1");
                // Adjuntar documento
                // JavaMail 1.4
                if (adjunto != null) {
                    Multipart multipart = new MimeMultipart();
                    // creates body part for the attachment
                    MimeBodyPart attachPart = new MimeBodyPart();
                    // code to add attachment...will be revealed later
                    // adds parts to the multipart
                    String attachFile = adjunto.getAbsolutePath();
                    // multipart.addBodyPart(attachPart);
                    multipart.addBodyPart(messageBodyPart);
                    attachPart.attachFile(attachFile);
                    // attachPart.attachFile(adjunto.getName().substring(0, 10));
                    multipart.addBodyPart(attachPart);
                    // sets the multipart as message's content
                    msg.setContent(multipart);
                }
                // Se envía el correo
                Transport.send(msg);
                if (adjunto != null) {
                    adjunto.delete();
                    adjunto.deleteOnExit();
                }

            } catch (Exception e) {
                //logger.info("Algo paso CATCH ANIDADO... " + e);
                e.printStackTrace();
                return false;
            }
        } catch (Exception e) {
            //logger.info("Algo paso CATCH GENERAL... " + e);
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
