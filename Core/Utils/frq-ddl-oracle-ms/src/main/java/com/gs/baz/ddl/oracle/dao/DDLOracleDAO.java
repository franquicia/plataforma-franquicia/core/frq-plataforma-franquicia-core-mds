/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ddl.oracle.dao;

import com.gs.baz.ddl.oracle.dto.Oracle;
import com.gs.baz.ddl.oracle.dto.OracleDataType;
import com.gs.baz.ddl.oracle.dto.ParamType;
import com.gs.baz.ddl.oracle.dto.Parameter;
import com.gs.baz.ddl.oracle.dto.Properties;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author cescobarh
 */
public class DDLOracleDAO {

    private String blockDDLPackagesDefinition() {
        String block = "DECLARE\n"
                + "	PACKAGE_DEFINITION SYS_REFCURSOR;\n"
                + "BEGIN\n"
                + "	OPEN PACKAGE_DEFINITION FOR\n"
                + "    	SELECT \n"
                + "            OBJECT_NAME AS PACKAGE_NAME,\n"
                + "            (\n"
                + "                SELECT dbms_metadata.get_ddl('PACKAGE_SPEC', USER_OBJECTS.OBJECT_NAME,:IN_SCHEMA) AS PACKAGE_BODY from dual\n"
                + "            ) AS PACKAGE_SPEC\n"
                + "        FROM \n"
                + "            USER_OBJECTS\n"
                + "        WHERE\n"
                + "            USER_OBJECTS.OBJECT_NAME = NVL(:IN_OBJECT_NAME, USER_OBJECTS.OBJECT_NAME)\n"
                + "            AND OBJECT_TYPE='PACKAGE';\n"
                + "    :OUT_PACKAGE_DEFINITION := PACKAGE_DEFINITION;\n"
                + "END;";
        return block;
    }

    private String blockDDLPackagesBody() {
        String block = "DECLARE\n"
                + "    PACKAGE_BODY SYS_REFCURSOR;\n"
                + "BEGIN    \n"
                + "    OPEN PACKAGE_BODY FOR\n"
                + "    	SELECT\n"
                + "            OBJECT_NAME AS PACKAGE_NAME,\n"
                + "            (\n"
                + "                SELECT dbms_metadata.get_ddl('PACKAGE_BODY', OBJECT_NAME,:IN_SCHEMA) AS PACKAGE_BODY from dual\n"
                + "            ) AS PACKAGE_BODY\n"
                + "        FROM \n"
                + "            USER_OBJECTS\n"
                + "        WHERE\n"
                + "            USER_OBJECTS.OBJECT_NAME = NVL(:IN_OBJECT_NAME, USER_OBJECTS.OBJECT_NAME)\n"
                + "            AND OBJECT_TYPE='PACKAGE BODY';\n"
                + "    :OUT_PACKAGES_BODY := PACKAGE_BODY;\n"
                + "END;";
        return block;
    }

    public Map<String, Object> ddlPackagesDefinition(DataSource dataSource, String schema, String objectName) {
        Oracle anonymusPLSQL = new Oracle();
        anonymusPLSQL.setPlSQL(this.blockDDLPackagesDefinition());
        Properties properties = new Properties();
        properties.setQueryTimeout(8000);
        anonymusPLSQL.setProperties(properties);
        anonymusPLSQL.addParameter(new Parameter(OracleDataType.VARCHAR, "IN_SCHEMA", schema, ParamType.IN));
        anonymusPLSQL.addParameter(new Parameter(OracleDataType.VARCHAR, "IN_OBJECT_NAME", objectName, ParamType.IN));
        anonymusPLSQL.addParameter(new Parameter(OracleDataType.CURSOR, "OUT_PACKAGE_DEFINITION", ParamType.OUT));
        OracleBlock block = new OracleBlock(dataSource, anonymusPLSQL);
        return block.executePLSQLBlock();
    }

    public Map<String, Object> ddlPackagesBody(DataSource dataSource, String schema, String objectName) {
        Oracle anonymusPLSQL = new Oracle();
        anonymusPLSQL.setPlSQL(this.blockDDLPackagesBody());
        Properties properties = new Properties();
        properties.setQueryTimeout(8000);
        anonymusPLSQL.setProperties(properties);
        anonymusPLSQL.addParameter(new Parameter(OracleDataType.VARCHAR, "IN_SCHEMA", schema, ParamType.IN));
        anonymusPLSQL.addParameter(new Parameter(OracleDataType.VARCHAR, "IN_OBJECT_NAME", objectName, ParamType.IN));
        anonymusPLSQL.addParameter(new Parameter(OracleDataType.CURSOR, "OUT_PACKAGES_BODY", ParamType.OUT));
        OracleBlock block = new OracleBlock(dataSource, anonymusPLSQL);
        return block.executePLSQLBlock();
    }

}
