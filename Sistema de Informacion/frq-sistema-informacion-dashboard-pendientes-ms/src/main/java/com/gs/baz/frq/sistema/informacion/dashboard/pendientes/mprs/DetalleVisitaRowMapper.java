/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.CalificacionCeco;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.DetalleVisita;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.InformacionVisita;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DetalleVisitaRowMapper implements RowMapper<DetalleVisita> {

    @Override
    public DetalleVisita mapRow(ResultSet rs, int rowNum) throws SQLException {
        DetalleVisita detalleVisita = new DetalleVisita();
        detalleVisita.setCentroCostosBase(rs.getString("FICECOBASE_ID"));
        detalleVisita.setCentroCostos(rs.getString("FCID_CECO"));
        detalleVisita.setNombreCeco(rs.getString("NOMBRE_CECO"));
        detalleVisita.setFechaVisita(rs.getString("FDFECHA"));
        detalleVisita.setNumeroEmpleado(rs.getInt("ID_USUARIO"));
        detalleVisita.setNombreEmpleado(rs.getString("NOMBRE_USUARIO"));
        detalleVisita.setIdProtocolo(rs.getInt("ID_PROTOCOLO"));
        detalleVisita.setNombreProtocolo(rs.getString("NOMBRE_PROTOCOLO"));
        detalleVisita.setCalificacionProtocolo(rs.getInt("CALIFICACION_PROTOCOLO"));
        detalleVisita.setIdRango(rs.getInt("RANGO_ID"));
        detalleVisita.setRangoColor(rs.getString("RANGOCOLOR"));
        detalleVisita.setIdVisita(rs.getInt("ID_VISITA"));
        detalleVisita.setCalificacionLineal(rs.getInt("FICALIF_LINEAL"));
        detalleVisita.setPuntosEsperados(rs.getInt("FIPUNTOSESP"));
        return detalleVisita;
    }
}
