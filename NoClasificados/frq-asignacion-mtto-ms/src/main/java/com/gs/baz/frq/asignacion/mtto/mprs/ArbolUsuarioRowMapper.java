package com.gs.baz.frq.asignacion.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.asignacion.mtto.dto.ArbolUsuarioDTO;

/**
 *
 * @author cescobarh
 */
public class ArbolUsuarioRowMapper implements RowMapper<ArbolUsuarioDTO> {

    private ArbolUsuarioDTO status;

    @Override
    public ArbolUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new ArbolUsuarioDTO();

        status.setIdCoordinador(rs.getInt("IDCOORDINADOR"));
        status.setNombreEmpleadoTerritorio(rs.getString("NOMCOORDINADOR"));
        status.setIdTerritorio(rs.getInt("IDTERRITORIO"));
        status.setNombreTerritorio(rs.getString("TERRITORIO"));
        status.setIdJefeZona(rs.getInt("IDUSUZONA"));
        status.setNombreEmpleadoZona(rs.getString("NOMJEFEZONA"));
        status.setIdZona(rs.getInt("IDZONA"));
        status.setNombreZona(rs.getString("ZONA"));
        status.setIdSupervisor(rs.getInt("IDSUPERVISOR"));
        status.setNombreSupervisor(rs.getString("SUPERVISOR"));
        


        return status;
    }
}
