/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.gs.baz.frq.api.portal.cautivo.dto.ExcelFileDTO;
import com.gs.baz.frq.api.portal.cautivo.dto.SheetDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StopWatch;

/**
 *
 * @author MADA
 */
public class Util {

    public static int tiempo = 70000;
    //    private ExcelFileDTO excelFileDTO;
    private int indexColumn;

    private SheetDTO sheetDTO;
    private ExcelFileDTO excelFileDTO;
    private JsonNode jsonNodeRows;
    private final Logger logger = LogManager.getLogger();
    private StopWatch stopWatch;

    public static String convertUTCDateToCTSDate(String UTCDate) throws CustomException {

        try {

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));   // This line converts the given date into UTC time zone
            final java.util.Date dateObj = sdf.parse(UTCDate);

            String fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dateObj);

            return fecha;

        } catch (ParseException p) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error al convertir fecha " + UTCDate));
        }
    }

    public static String convertCTSDateToUTCDate(String CTSDate) throws CustomException {

        try {

            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("CST"));   // This line converts the given date into UTC time zone
            java.util.Date dateObj = sdf.parse(CTSDate);

            String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateObj);
            System.out.println("fecha " + fecha);
            return fecha;

        } catch (ParseException p) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error al convertir fecha " + CTSDate));
        }
    }

}
