/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.ceco.base.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class CecoBaseDTO {

    @JsonProperty(value = "id_ceco_base")
    private Integer idCecoBase;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_pais")
    private Integer idPais;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_territorio")
    private String idTerritorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_zona")
    private String idZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_region")
    private String idRegion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "canal")
    private String canal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha_creacion")
    private String fechaCreacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "registro_activo")
    private Integer registroActivo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public CecoBaseDTO() {
    }

    public Integer getIdCecoBase() {
        return idCecoBase;
    }

    public void setIdCecoBase(BigDecimal idCecoBase) {
        this.idCecoBase = (idCecoBase == null ? null : idCecoBase.intValue());
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(BigDecimal idPais) {
        this.idPais = (idPais == null ? null : idPais.intValue());
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdTerritorio(String idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getIdZona() {
        return idZona;
    }

    public void setIdZona(String idZona) {
        this.idZona = idZona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getRegistroActivo() {
        return registroActivo;
    }

    public void setRegistroActivo(BigDecimal registroActivo) {
        this.registroActivo = (registroActivo == null ? null : registroActivo.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "CecoBaseDTO{" + "idCecoBase=" + idCecoBase + ", idPais=" + idPais + ", idCeco=" + idCeco + ", descripcion=" + descripcion + ", idTerritorio=" + idTerritorio + ", territorio=" + territorio + ", idZona=" + idZona + ", zona=" + zona + ", idRegion=" + idRegion + ", region=" + region + ", canal=" + canal + ", fechaCreacion=" + fechaCreacion + ", registroActivo=" + registroActivo + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
