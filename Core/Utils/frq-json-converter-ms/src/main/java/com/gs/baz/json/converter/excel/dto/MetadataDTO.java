/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.json.converter.excel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class MetadataDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "sheets")
    private List<MetadataSheetDTO> sheets;

    public MetadataDTO() {
    }

    public List<MetadataSheetDTO> getSheets() {
        return sheets;
    }

    public void setSheets(List<MetadataSheetDTO> sheets) {
        this.sheets = sheets;
    }

    public void addSheet(MetadataSheetDTO metadataSheetDTO) {
        if (this.sheets == null) {
            this.sheets = new ArrayList<>();
        }
        this.sheets.add(metadataSheetDTO);
    }

    @Override
    public String toString() {
        return "MetadataDTO{" + "sheets=" + sheets + '}';
    }

}
