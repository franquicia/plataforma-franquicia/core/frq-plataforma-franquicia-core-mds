/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.firmas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base de la firma", value = "FirmaBase")
public class FirmaBase {

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Numero de Empleado de la firma", example = "331952")
    private Integer numeroEmpleado;

    @JsonProperty(value = "idJerarquia")
    @ApiModelProperty(notes = "Identificados de la jerarquia de la firma", example = "1")
    private Integer idJerarquia;

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificados del estado de la firma", example = "1")
    private Integer idEstadoFirma;

    @JsonProperty(value = "indice")
    @ApiModelProperty(notes = "Indice de la firma", example = "1")
    private Integer indice;

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdJerarquia() {
        return idJerarquia;
    }

    public void setIdJerarquia(Integer idJerarquia) {
        this.idJerarquia = idJerarquia;
    }

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    @Override
    public String toString() {
        return "FirmaBase{" + "numeroEmpleado=" + numeroEmpleado + ", idJerarquia=" + idJerarquia + ", idEstadoFirma=" + idEstadoFirma + ", indice=" + indice + '}';
    }

}
