/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.mprs;

import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoVirtualBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ArchivoRowMapper implements RowMapper<ArchivoVirtualBase> {

    private ArchivoVirtualBase archivoBase;

    @Override
    public ArchivoVirtualBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        archivoBase = new ArchivoVirtualBase();
        archivoBase.setIdArchivoPadre(rs.getInt("FIID_ARCHPADRE"));
        archivoBase.setUid(rs.getLong("FCRUTA"));
        archivoBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        archivoBase.setNombre(rs.getString("FCNOMBRE"));
        archivoBase.setExtension(rs.getString("FCEXTENSION"));
        archivoBase.setTipo(rs.getInt("FCTIPO"));
        archivoBase.setIndice(rs.getInt("FCINDICE"));
        archivoBase.setPeso(rs.getInt("FCPESO"));
        archivoBase.setTipoContenido(rs.getString("FCCONTENTYPE"));
        archivoBase.setIdFolio(rs.getInt("FIID_FOLIO"));
        return archivoBase;
    }
}
