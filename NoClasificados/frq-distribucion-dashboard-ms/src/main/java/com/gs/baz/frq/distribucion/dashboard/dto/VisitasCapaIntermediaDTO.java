package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Visitas de Capa Intermedia", value = "VisitasCapaIntermedia")
public class VisitasCapaIntermediaDTO {

    @JsonProperty(value = "respuestaEjecucion")
    @ApiModelProperty(notes = "Indicador de respuesta de la ejecución.", example = "1")
    private Integer respuestaEjecucion;

    @JsonProperty(value = "visitasCapaIntermedia")
    @ApiModelProperty(notes = "Número de visitas de capa intermedia.", example = "1")
    private Integer visitasCaapaCero;

    @JsonProperty(value = "envioCorreo")
    @ApiModelProperty(notes = "Indicador del envío de notificación por correo electrónico.", example = "true")
    private Boolean envioCorreo;

    public Integer getRespuestaEjecucion() {
        return respuestaEjecucion;
    }

    public void setRespuestaEjecucion(Integer respuestaEjecucion) {
        this.respuestaEjecucion = respuestaEjecucion;
    }

    public Integer getVisitasCaapaCero() {
        return visitasCaapaCero;
    }

    public void setVisitasCaapaCero(Integer visitasCaapaCero) {
        this.visitasCaapaCero = visitasCaapaCero;
    }

    public Boolean getEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(Boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    @Override
    public String toString() {
        return "VisitasCapaIntermediaDTO{" + "respuestaEjecucion=" + respuestaEjecucion + ", visitasCaapaCero=" + visitasCaapaCero + ", envioCorreo=" + envioCorreo + '}';
    }

}
