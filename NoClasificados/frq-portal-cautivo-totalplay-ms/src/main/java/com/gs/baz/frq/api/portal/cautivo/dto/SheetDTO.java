/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class SheetDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "description")
    private String description;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rows")
    private List<ObjectNode> rows;

    public SheetDTO(String description) {
        this.description = description;
    }

    public SheetDTO() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ObjectNode> getRows() {
        return rows;
    }

    public void setRows(List<ObjectNode> rows) {
        this.rows = rows;
    }

    public void addRow(ObjectNode row) {
        if (this.rows == null) {
            this.rows = new ArrayList<>();
        }
        this.rows.add(row);
    }

    @Override
    public String toString() {
        return "SheetDTO{" + "description=" + description + ", rows=" + rows + '}';
    }

}
