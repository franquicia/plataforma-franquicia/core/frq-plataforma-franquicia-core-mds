/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.mprs;

import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class RutaRowMapper implements RowMapper<RutaBase> {

    private RutaBase rutaBase;

    @Override
    public RutaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        rutaBase = new RutaBase();
        rutaBase.setCodificacion(rs.getString("FCCODIFICACION"));
        rutaBase.setIdPais(rs.getInt("FIIDPAIS"));
        rutaBase.setIdModelo(rs.getInt("FIIDMODELO"));
        rutaBase.setIdUnidadNegocio(rs.getInt("FIIDUNIDADNEGOCIO"));
        return rutaBase;
    }
}
