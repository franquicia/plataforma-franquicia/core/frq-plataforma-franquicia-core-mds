/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folio.etiquetas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Folio Etiqueta", value = "FolioEtiqueta")
public class FolioEtiqueta extends FolioEtiquetaBase {

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1", position = -1)
    private Integer idFolio;

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }
}
