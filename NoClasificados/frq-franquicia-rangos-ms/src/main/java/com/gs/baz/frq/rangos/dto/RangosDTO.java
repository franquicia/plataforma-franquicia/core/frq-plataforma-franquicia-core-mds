/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.rangos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class RangosDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numero_rango")
    private Integer numeroRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "limite_inferior")
    private Integer limiteInferior;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "limite_superior")
    private Integer limiteSuperior;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "valor_rango")
    private String valorRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "detalle")
    private String detalle;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "registro_activo")
    private Integer registroActivo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public RangosDTO() {
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(BigDecimal idRango) {
        this.idRango = (idRango == null ? null : idRango.intValue());
    }

    public Integer getNumeroRango() {
        return numeroRango;
    }

    public void setNumeroRango(BigDecimal numeroRango) {
        this.numeroRango = (numeroRango == null ? null : numeroRango.intValue());
    }

    public Integer getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(BigDecimal limiteInferior) {
        this.limiteInferior = (limiteInferior == null ? null : limiteInferior.intValue());
    }

    public Integer getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(BigDecimal limiteSuperior) {
        this.limiteSuperior = (limiteSuperior == null ? null : limiteSuperior.intValue());
    }

    public String getValorRango() {
        return valorRango;
    }

    public void setValorRango(String valorRango) {
        this.valorRango = valorRango;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Integer getRegistroActivo() {
        return registroActivo;
    }

    public void setRegistroActivo(BigDecimal registroActivo) {
        this.registroActivo = (registroActivo == null ? null : registroActivo.intValue());
    }

    @Override
    public String toString() {
        return "RangosFranquiciaDTO{" + "idRango=" + idRango + ", numeroRagngo=" + numeroRango + ", limiteInferior=" + limiteInferior + ", limiteSuperior=" + limiteSuperior + ", valorRango=" + valorRango + ", detalle=" + detalle + ", registroActivo=" + registroActivo + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
