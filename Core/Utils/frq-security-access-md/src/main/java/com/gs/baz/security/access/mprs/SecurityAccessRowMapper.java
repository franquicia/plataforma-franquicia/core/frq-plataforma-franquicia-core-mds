package com.gs.baz.security.access.mprs;

import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class SecurityAccessRowMapper implements RowMapper<AccesoAsimetricoDTO> {

    private AccesoAsimetricoDTO rsaAccesoDTO;

    @Override
    public AccesoAsimetricoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        rsaAccesoDTO = new AccesoAsimetricoDTO();
        rsaAccesoDTO.setIdAcceso(rs.getString("FCIDACCESO"));
        rsaAccesoDTO.setPublica(rs.getString("FCACCPUBLICO"));
        rsaAccesoDTO.setPrivada(rs.getString("FCACCPRIVADO"));
        return rsaAccesoDTO;
    }
}
