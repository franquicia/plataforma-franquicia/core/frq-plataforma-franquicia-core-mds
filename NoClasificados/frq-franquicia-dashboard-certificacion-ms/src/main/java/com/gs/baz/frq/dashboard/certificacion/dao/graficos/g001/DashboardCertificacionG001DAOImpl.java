package com.gs.baz.frq.dashboard.certificacion.dao.graficos.g001;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g001.DashboardCertificacionG001DTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g001.DashboardCertificacionG001RowMapper;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG001DAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCoberturaOdometro;

    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCoberturaOdometro = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCoberturaOdometro.withSchemaName(schema);
        jdbcSelectCoberturaOdometro.withCatalogName("PADASHCCCOBERT");
        jdbcSelectCoberturaOdometro.withProcedureName("SPCOBERTURA");
        jdbcSelectCoberturaOdometro.returningResultSet("PA_CDATOS", new DashboardCertificacionG001RowMapper());

    }

    public List<DashboardCertificacionG001DTO> selectRowDashboardCertificacionG001(String entityDTO1, String entityDTO2, Long entityDTO3, Long entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", entityDTO1);
        mapSqlParameterSource.addValue("PA_FECHAFIN", entityDTO2);
        mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO3);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO4);
        Map<String, Object> out = jdbcSelectCoberturaOdometro.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success) {
            return (List<DashboardCertificacionG001DTO>) out.get("PA_CDATOS");
        } else {
            return null;
        }

    }

}
