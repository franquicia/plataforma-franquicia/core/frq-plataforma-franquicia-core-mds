/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.dto.graficos.g011;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import java.math.BigDecimal;

/**
 *
 * @author B73601
 */
public class CumplimientoPuntajeNegociosTerritorialDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion_ceco")
    private String descripcionCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_region")
    private String idRegion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "region")
    private String region;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_zona")
    private String idZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "punto_contacto")
    private Integer puntoContacto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "cumple")
    private Integer cumple;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_rango")
    private Integer idRango;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "porcentaje_cumplimiento")
    private Integer porcentajeCumplimiento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "puntaje")
    private Integer puntaje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "filtros")
    private FiltrosDTO filtros;

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescripcionCeco() {
        return descripcionCeco;
    }

    public void setDescripcionCeco(String descripcionCeco) {
        this.descripcionCeco = descripcionCeco;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getIdZona() {
        return idZona;
    }

    public void setIdZona(String idZona) {
        this.idZona = idZona;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getPuntoContacto() {
        return puntoContacto;
    }

    public void setPuntoContacto(BigDecimal puntoContacto) {
        this.puntoContacto = (puntoContacto == null ? null : puntoContacto.intValue());
    }

    public Integer getCumple() {
        return cumple;
    }

    public void setCumple(BigDecimal cumple) {
        this.cumple = (cumple == null ? null : cumple.intValue());
    }

    public Integer getPorcentajeCumplimiento() {
        return porcentajeCumplimiento;
    }

    public void setPorcentajeCumplimiento(BigDecimal porcentajeCumplimiento) {
        this.porcentajeCumplimiento = (porcentajeCumplimiento == null ? null : porcentajeCumplimiento.intValue());
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(BigDecimal idRango) {
        this.idRango = (idRango == null ? null : idRango.intValue());
    }

    public Integer getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(BigDecimal puntaje) {
        this.puntaje = (puntaje == null ? null : puntaje.intValue());
    }

    public FiltrosDTO getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosDTO filtros) {
        this.filtros = filtros;
    }

    @Override
    public String toString() {
        return "CumplimientoPuntajeNegociosTerritorialDTO{" + "idCeco=" + idCeco + ", descripcionCeco=" + descripcionCeco + ", idRegion=" + idRegion + ", region=" + region + ", idZona=" + idZona + ", zona=" + zona + ", puntoContacto=" + puntoContacto + ", cumple=" + cumple + ", idRango=" + idRango + ", porcentajeCumplimiento=" + porcentajeCumplimiento + ", puntaje=" + puntaje + ", filtros=" + filtros + '}';
    }

}
