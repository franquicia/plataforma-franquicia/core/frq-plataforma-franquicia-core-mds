package com.gs.baz.frq.accesos.sistema.rutas.mprs;

import com.gs.baz.frq.accesos.sistema.rutas.dto.Ruta;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class RutasRowMapper implements RowMapper<Ruta> {

    @Override
    public Ruta mapRow(ResultSet rs, int rowNum) throws SQLException {
        Ruta ruta = new Ruta();
        ruta.setIdRuta(rs.getInt("FIID_RUTA"));
        ruta.setNombre(rs.getString("FCDESCRIPCION"));
        ruta.setRutaAbsoluta(rs.getString("FCRUTA"));
        return ruta;
    }
}
