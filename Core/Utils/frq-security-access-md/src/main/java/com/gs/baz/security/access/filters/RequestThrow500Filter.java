/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import com.gs.baz.frq.swagger.responses.RespuestaErrorServer;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE - 9)
public class RequestThrow500Filter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String is500Back = httpServletRequest.getHeader("x-is500Back");
        if (is500Back != null && is500Back.equals("true")) {
            ResponseApiCodes apiCode;
            ResponseMessage responseMessage;
            RespuestaAPI respuestaErrorClient;
            String requestID = (String) httpServletRequest.getAttribute("requestID");
            String nameApi = (String) httpServletRequest.getAttribute("nameApi");
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_05001;
            responseMessage = ResponseMensages.getByStatus(500);
            String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
            respuestaErrorClient = new RespuestaErrorServer(codeError, responseMessage.getMessage(), requestID);
            httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            httpServletResponse.setStatus(responseMessage.getCode());
            OutputStream out = httpServletResponse.getOutputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, respuestaErrorClient);
            out.flush();
        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest httpServletRequest) throws ServletException {
        String uri = httpServletRequest.getRequestURI();
        return uri.contains("aplicaciones/llaves") || uri.contains("oauth/token");
    }

}
