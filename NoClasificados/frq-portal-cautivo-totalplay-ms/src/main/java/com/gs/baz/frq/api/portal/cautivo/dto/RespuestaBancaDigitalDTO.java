/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author MADA
 */
public class RespuestaBancaDigitalDTO {

    @JsonProperty(value = "Lista registros Banca Digital")
    private List<BancaDigitalDTO> bancaDigital;

    public List<BancaDigitalDTO> getBancaDigital() {
        return bancaDigital;
    }

    public void setBancaDigital(List<BancaDigitalDTO> bancaDigital) {
        this.bancaDigital = bancaDigital;
    }

    @Override
    public String toString() {
        return "RespuestaBancaDigitalDTO{" + "bancaDigital=" + bancaDigital + '}';
    }

}
