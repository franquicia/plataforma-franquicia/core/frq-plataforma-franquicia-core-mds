/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.disciplinas.negocios.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.disciplinas.negocios.dao.DisciplinasNegociosDAOImpl;
import com.gs.baz.disciplinas.negocios.dto.DisciplinaNegocioDTO;
import com.gs.baz.disciplinas.negocios.dto.EjeModeloDTO;
import com.gs.baz.disciplinas.negocios.dto.EjeModeloNegocioDTO;
import com.gs.baz.disciplinas.negocios.dto.NegocioDTO;
import com.gs.baz.disciplinas.negocios.dto.ProtocoloDisciplinaDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.dto.ModeloFranquiciaDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjesModelo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModelosFranquicia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceNegocio;
import com.gs.baz.model.bucket.client.services.rest.SubServiceProtocolo;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/disciplinas/negocios")
public class ServiceDisciplinasNegocios {

    @Autowired
    private DisciplinasNegociosDAOImpl disciplinasNegociosDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    private SubServiceModelosFranquicia subServiceModelosFranquicia;

    @Autowired
    private SubServiceEjesModelo subServiceEjesModelo;

    @Autowired
    private SubServiceNegocio subServiceNegocio;

    @Autowired
    private SubServiceProtocolo subServiceProtocolo;

    final VersionBI versionBI = new VersionBI();

    final private ObjectMapper objectMapper = new ObjectMapper();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/{id_modelo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioDTO> getNegociosModelo(@RequestHeader HttpHeaders headers, @PathVariable("id_modelo") Long idModelo) throws CustomException {
        List<NegocioDTO> negociosModelo;
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        try {
            subServiceModelosFranquicia.init(basePath, httpEntity);
            subServiceProtocolo.init(basePath, httpEntity);
            subServiceNegocio.init(basePath, httpEntity);
            subServiceEjesModelo.init(basePath, httpEntity);
            ModeloFranquiciaDTO dataModeloFranquiciaDTO = subServiceModelosFranquicia.getModelo(idModelo.intValue());
            Object dataNegociosFranquiciaDTO = subServiceNegocio.getNegociosFranquicia(dataModeloFranquiciaDTO.getIdFranquicia());
            negociosModelo = objectMapper.convertValue(dataNegociosFranquiciaDTO, objectMapper.getTypeFactory().constructCollectionType(List.class, NegocioDTO.class));
            List<EjeModeloDTO> ejesModelo;
            Object ejesModeloBucket = subServiceEjesModelo.getEjesModelo(idModelo.intValue());
            if (ejesModeloBucket != null) {
                ejesModelo = objectMapper.convertValue(ejesModeloBucket, objectMapper.getTypeFactory().constructCollectionType(List.class, EjeModeloDTO.class));
                if (ejesModelo != null) {
                    List<EjeModeloNegocioDTO> ejesModeloNegocios;
                    List<DisciplinaNegocioDTO> disciplinasNegociosDTO;
                    List<ProtocoloDisciplinaDTO> protocolosDisciplinaDTO;
                    Object protocolos;
                    if (dataNegociosFranquiciaDTO != null) {
                        if (negociosModelo != null) {
                            for (NegocioDTO negocioDTO : negociosModelo) {
                                Long idNegocio = negocioDTO.getIdNegocio().longValue();
                                ejesModeloNegocios = objectMapper.convertValue(ejesModelo, objectMapper.getTypeFactory().constructCollectionType(List.class, EjeModeloNegocioDTO.class));
                                if (ejesModeloNegocios != null) {
                                    for (EjeModeloNegocioDTO ejeModeloDTO : ejesModeloNegocios) {
                                        disciplinasNegociosDTO = disciplinasNegociosDAOImpl.selectRow(idModelo, ejeModeloDTO.getIdModEje().longValue(), idNegocio);
                                        if (disciplinasNegociosDTO != null) {
                                            for (DisciplinaNegocioDTO disciplinaNegocio : disciplinasNegociosDTO) {
                                                if (disciplinaNegocio.getIdDisciplinaNegocio() != null) {
                                                    Long idDisciplinaNegocio = disciplinaNegocio.getIdDisciplinaNegocio().longValue();
                                                    protocolos = subServiceProtocolo.getProtocolosDisciplinas(idDisciplinaNegocio.intValue());
                                                    if (protocolos != null) {
                                                        protocolosDisciplinaDTO = objectMapper.convertValue(protocolos, objectMapper.getTypeFactory().constructCollectionType(List.class, ProtocoloDisciplinaDTO.class));
                                                        disciplinaNegocio.setProtocolosDisciplinaDTO(protocolosDisciplinaDTO);
                                                    } else {
                                                        disciplinaNegocio.setProtocolosDisciplinaDTO(new ArrayList<>());
                                                    }
                                                } else {
                                                    disciplinaNegocio.setProtocolosDisciplinaDTO(new ArrayList<>());
                                                }
                                            }
                                        }
                                        ejeModeloDTO.setDisciplinaEjeDTO(disciplinasNegociosDTO);
                                    }
                                }
                                negocioDTO.setEjesNegocio(ejesModeloNegocios);
                            }
                        }
                    }
                }
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
        return negociosModelo;
    }
}
