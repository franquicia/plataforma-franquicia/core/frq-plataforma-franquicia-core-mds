package com.gs.baz.frq.dashboard.dao.util;

import com.gs.baz.frq.dashboard.dto.FechasPeriodoDTO.FechasPeriodoDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.dto.util.PeriodoDTO;
import com.gs.baz.frq.dashboard.mprs.util.PeriodoRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PeriodoDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectPeriodo;
    private DefaultJdbcCall jdbcSelectFechas;
    private DefaultJdbcCall jdbcSelectFechasGeneral;
    private DefaultJdbcCall jdbcSelectFechasPeriodo;

    private String schema;
    private final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectPeriodo.withSchemaName(schema);
        jdbcSelectPeriodo.withCatalogName("PKGADMFECHAS");
        jdbcSelectPeriodo.withProcedureName("SPGETFECHA");
        jdbcSelectPeriodo.returningResultSet("PA_CURSOR", new PeriodoRowMapper());

        jdbcSelectFechas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFechas.withSchemaName(schema);
        jdbcSelectFechas.withCatalogName("PAFECHADASH");
        jdbcSelectFechas.withProcedureName("SPGETFECHA");
        jdbcSelectFechas.returningResultSet("PA_CURSOR", new PeriodoRowMapper());

        jdbcSelectFechasGeneral = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFechasGeneral.withSchemaName(schema);
        jdbcSelectFechasGeneral.withCatalogName("PAFECHASGRAF");
        jdbcSelectFechasGeneral.withProcedureName("SPGETPERIODO");

        jdbcSelectFechasPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFechasPeriodo.withSchemaName(schema);
        jdbcSelectFechasPeriodo.withCatalogName("PKGADMFECHAS");
        jdbcSelectFechasPeriodo.withProcedureName("SPGETPERIODO");

    }

    public List<PeriodoDTO> selectRows(FiltrosDTO filtrosDTO) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("pa_FechaIni", filtrosDTO.getFechaInicio());
        mapSqlParameterSource.addValue("pa_FechaFin", filtrosDTO.getFechaFin());
        Map<String, Object> out = jdbcSelectPeriodo.execute(mapSqlParameterSource);
        return (List<PeriodoDTO>) out.get("PA_CURSOR");
    }

    public PeriodoDTO selectRowActual() throws CustomException {
        String dateString = format.format(new Date());
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("pa_FechaIni", dateString);
        mapSqlParameterSource.addValue("pa_FechaFin", dateString);
        Map<String, Object> out = jdbcSelectPeriodo.execute(mapSqlParameterSource);
        List<PeriodoDTO> data = (List<PeriodoDTO>) out.get("PA_CURSOR");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public PeriodoDTO selectRowPeriodosAnteriores() throws CustomException {
        Map<String, Object> out = jdbcSelectFechas.execute();
        List<PeriodoDTO> data = (List<PeriodoDTO>) out.get("PA_CURSOR");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public FechasDTO getFechas(FiltrosDTO filtrosDTO) throws CustomException {
        String fechaInicio = "";
        String fechaFin = "";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIANIO", filtrosDTO.getAnio());
        mapSqlParameterSource.addValue("PA_PERIODO", filtrosDTO.getNumeroPeriodo());
        mapSqlParameterSource.addValue("PA_IDDISC", filtrosDTO.getIdDisciplina());
        Map<String, Object> out = jdbcSelectFechasGeneral.execute(mapSqlParameterSource);
        fechaInicio = ((String) out.get("PA_FECHAINI"));
        fechaFin = ((String) out.get("PA_FECHAFIN"));

        FechasDTO fechas = new FechasDTO();
        fechas.setFechaInicio(fechaInicio);
        fechas.setFechaFin(fechaFin);
        return fechas;
    }

    public FechasPeriodoDTO selectRowFechaPeriodo(PeriodoDTO periodoDTO) throws CustomException {
        String fechaInicio = "";
        String fechaFin = "";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("pa_FIANIO", periodoDTO.getAnio());
        mapSqlParameterSource.addValue("pa_FITRIMESTRE", periodoDTO.getTrimestre());
        mapSqlParameterSource.addValue("pa_FIBIMESTRE", periodoDTO.getBimestre());
        Map<String, Object> out = jdbcSelectFechasPeriodo.execute(mapSqlParameterSource);
        fechaInicio = ((String) out.get("PA_FECHAINI"));
        fechaFin = ((String) out.get("PA_FECHAFIN"));

        FechasPeriodoDTO fechasPeriodoDTO = new FechasPeriodoDTO();
        fechasPeriodoDTO.setFechaInicio(fechaInicio);
        fechasPeriodoDTO.setFechaFin(fechaFin);

        return fechasPeriodoDTO;
    }

}
