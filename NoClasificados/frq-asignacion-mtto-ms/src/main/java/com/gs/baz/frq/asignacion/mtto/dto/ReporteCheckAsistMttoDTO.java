/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReporteCheckAsistMttoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idAsistencia")
    private int idAsistencia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private String idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "latitudEntrada")
    private double latitudEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "longitudEntrada")
    private double longitudEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "latitudSalida")
    private double latitudSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "longitudSalida")
    private double longitudSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "dia")
    private String dia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaEntrada")
    private String horaEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horaSalida")
    private String horaSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "statusEntrada")
    private String statusEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "statusSalida")
    private String statusSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "observacionesEntrada")
    private String observacionesEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "observacionesSalida")
    private String observacionesSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "lugarEntrada")
    private String lugarEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "lugarSalida")
    private String lugarSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rutaEntrada")
    private String rutaEntrada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "rutaSalida")
    private String rutaSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descuento")
    private String descuento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "semana")
    private String semana;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "totalVisitas")
    private int totalVisitas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "totalModulos")
    private int totalModulos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipoProyecto")
    private String tipoProyecto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "horasTrabajadas")
    private String horasTrabajadas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "conteoSucursales")
    private Integer conteoSucursales;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idTerritorio")
    private Integer idTerritorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idZona")
    private Integer idZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fcTerritorio")
    private String fcTerritorio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fcZona")
    private String fcZona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreJefeZona")
    private String nombreJefeZona;

    public void setNombreJefeZona(String nombreJefeZona) {
        this.nombreJefeZona = nombreJefeZona;
    }

    public String getNombreJefeZona() {
        return nombreJefeZona;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public Integer getIdTerritorio() {
        return idTerritorio;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public void setIdTerritorio(Integer idTerritorio) {
        this.idTerritorio = idTerritorio;
    }

    public String getFcTerritorio() {
        return fcTerritorio;
    }

    public String getFcZona() {
        return fcZona;
    }

    public void setFcTerritorio(String fcTerritorio) {
        this.fcTerritorio = fcTerritorio;
    }

    public void setFcZona(String fcZona) {
        this.fcZona = fcZona;
    }

    public Integer getConteoSucursales() {
        return conteoSucursales;
    }

    public void setConteoSucursales(Integer conteoSucursales) {
        this.conteoSucursales = conteoSucursales;
    }

    public double getLatitudEntrada() {
        return latitudEntrada;
    }

    public void setLatitudEntrada(double latitudEntrada) {
        this.latitudEntrada = latitudEntrada;
    }

    public double getLongitudEntrada() {
        return longitudEntrada;
    }

    public void setLongitudEntrada(double longitudEntrada) {
        this.longitudEntrada = longitudEntrada;
    }

    public double getLatitudSalida() {
        return latitudSalida;
    }

    public void setLatitudSalida(double latitudSalida) {
        this.latitudSalida = latitudSalida;
    }

    public double getLongitudSalida() {
        return longitudSalida;
    }

    public void setLongitudSalida(double longitudSalida) {
        this.longitudSalida = longitudSalida;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getStatusEntrada() {
        return statusEntrada;
    }

    public void setStatusEntrada(String statusEntrada) {
        this.statusEntrada = statusEntrada;
    }

    public String getStatusSalida() {
        return statusSalida;
    }

    public void setStatusSalida(String statusSalida) {
        this.statusSalida = statusSalida;
    }

    public String getObservacionesEntrada() {
        return observacionesEntrada;
    }

    public void setObservacionesEntrada(String observacionesEntrada) {
        this.observacionesEntrada = observacionesEntrada;
    }

    public String getObservacionesSalida() {
        return observacionesSalida;
    }

    public void setObservacionesSalida(String observacionesSalida) {
        this.observacionesSalida = observacionesSalida;
    }

    public String getLugarEntrada() {
        return lugarEntrada;
    }

    public void setLugarEntrada(String lugarEntrada) {
        this.lugarEntrada = lugarEntrada;
    }

    public String getLugarSalida() {
        return lugarSalida;
    }

    public void setLugarSalida(String lugarSalida) {
        this.lugarSalida = lugarSalida;
    }

    public String getRutaEntrada() {
        return rutaEntrada;
    }

    public void setRutaEntrada(String rutaEntrada) {
        this.rutaEntrada = rutaEntrada;
    }

    public String getRutaSalida() {
        return rutaSalida;
    }

    public void setRutaSalida(String rutaSalida) {
        this.rutaSalida = rutaSalida;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getSemana() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana = semana;
    }

    public int getTotalVisitas() {
        return totalVisitas;
    }

    public void setTotalVisitas(int totalVisitas) {
        this.totalVisitas = totalVisitas;
    }

    public int getTotalModulos() {
        return totalModulos;
    }

    public void setTotalModulos(int totalModulos) {
        this.totalModulos = totalModulos;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public String getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(String horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public int getIdAsistencia() {
        return idAsistencia;
    }

    public void setIdAsistencia(int idAsistencia) {
        this.idAsistencia = idAsistencia;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public String toString() {
        return "ReporteCheckAsistMttoDTO [idAsistencia=" + idAsistencia + ", idUsuario=" + idUsuario
                + ", nombreUsuario=" + nombreUsuario + ", latitudEntrada=" + latitudEntrada + ", longitudEntrada="
                + longitudEntrada + ", latitudSalida=" + latitudSalida + ", longitudSalida=" + longitudSalida
                + ", fecha=" + fecha + ", dia=" + dia + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
                + ", idTerritorio=" + idTerritorio + ", idZona=" + idZona + ", fcTerritorio=" + fcTerritorio + ", fcZona=" + fcZona + ", nombreJefeZona=" + nombreJefeZona
                + ", horaEntrada=" + horaEntrada + ", horaSalida=" + horaSalida + ", statusEntrada=" + statusEntrada
                + ", statusSalida=" + statusSalida + ", observacionesEntrada=" + observacionesEntrada
                + ", observacionesSalida=" + observacionesSalida + ", lugarEntrada=" + lugarEntrada
                + ", lugarSalida=" + lugarSalida + ", rutaEntrada=" + rutaEntrada + ", rutaSalida=" + rutaSalida
                + ", descuento=" + descuento + ", semana=" + semana + ", totalVisitas=" + totalVisitas
                + ", totalModulos=" + totalModulos + ", tipoProyecto=" + tipoProyecto + ", horasTrabajadas="
                + horasTrabajadas + ", periodo=" + periodo + ", zona=" + zona + ", conteoSucursales="
                + conteoSucursales + "]";
    }
    
}
