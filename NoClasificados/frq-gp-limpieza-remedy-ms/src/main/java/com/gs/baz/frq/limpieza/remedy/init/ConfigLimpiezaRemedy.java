package com.gs.baz.frq.limpieza.remedy.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.limpieza.remedy.bi.CuadrillaBI;
import com.gs.baz.frq.limpieza.remedy.bi.DatosEmpMttoBI;
import com.gs.baz.frq.limpieza.remedy.bi.IncidenciasBI;
import com.gs.baz.frq.limpieza.remedy.bi.IncidenteCuadrillaBI;
import com.gs.baz.frq.limpieza.remedy.bi.IncidenteCuadrillaLimpiezaBI;
import com.gs.baz.frq.limpieza.remedy.bi.IncidenteLimpiezaBI;
import com.gs.baz.frq.limpieza.remedy.bi.IncidenteLimpiezaDosBI;
import com.gs.baz.frq.limpieza.remedy.bi.LogBI;
import com.gs.baz.frq.limpieza.remedy.bi.ParametroBI;
import com.gs.baz.frq.limpieza.remedy.bi.PeticionesRemedyBI;
import com.gs.baz.frq.limpieza.remedy.bi.ProveedoresBI;
import com.gs.baz.frq.limpieza.remedy.bi.ServiciosRemedyLimpiezaBI;
import com.gs.baz.frq.limpieza.remedy.bi.ServiciosRemedyLimpiezaDosBI;
import com.gs.baz.frq.limpieza.remedy.dao.CuadrillaDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.DatosEmpMttoDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.IncidenciasDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.IncidenteCuadrillaDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.LogDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.ParametroDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.PeriodoReporteDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.PeticionesRemedyDAOImpl;
import com.gs.baz.frq.limpieza.remedy.dao.ProveedoresDAOImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.limpieza.remedy")
public class ConfigLimpiezaRemedy {

    public ConfigLimpiezaRemedy() {
        super();
    }

    @Bean(initMethod = "init")
    public ParametroDAOImpl parametroDAOImpl() {
        return new ParametroDAOImpl();
    }

    @Bean(initMethod = "init")
    public PeriodoReporteDAOImpl periodoReporteDAOImpl() {
        return new PeriodoReporteDAOImpl();
    }

    @Bean(initMethod = "init")
    public PeticionesRemedyDAOImpl peticionesRemedyDAOImpl() {
        return new PeticionesRemedyDAOImpl();
    }

    @Bean(initMethod = "init")
    public IncidenciasDAOImpl incidenciasDAOImpl() {
        return new IncidenciasDAOImpl();
    }

    @Bean(initMethod = "init")
    public ProveedoresDAOImpl proveedoresDAOImpl() {
        return new ProveedoresDAOImpl();
    }

    @Bean(initMethod = "init")
    public IncidenteCuadrillaDAOImpl incidenteCuadrillaDAOImpl() {
        return new IncidenteCuadrillaDAOImpl();
    }

    @Bean(initMethod = "init")
    public CuadrillaDAOImpl cuadrillaDAOImpl() {
        return new CuadrillaDAOImpl();
    }

    @Bean(initMethod = "init")
    public LogDAOImpl logDAOImpl() {
        return new LogDAOImpl();
    }

    @Bean(initMethod = "init")
    public DatosEmpMttoDAOImpl datosEmpMttoDAOImpl() {
        return new DatosEmpMttoDAOImpl();
    }

    @Bean()
    public IncidenciasBI incidenciasBI() {
        return new IncidenciasBI();
    }

    @Bean()
    public ParametroBI parametroBI() {
        return new ParametroBI();
    }

    @Bean()
    public PeticionesRemedyBI peticionesRemedyBI() {
        return new PeticionesRemedyBI();
    }

    @Bean()
    public ServiciosRemedyLimpiezaBI serviciosRemedyLimpiezaBI() {
        return new ServiciosRemedyLimpiezaBI();
    }

    @Bean()
    public ProveedoresBI proveedoresBI() {
        return new ProveedoresBI();
    }

    @Bean()
    public IncidenteCuadrillaLimpiezaBI incidenteCuadrillaLimpiezaBI() {
        return new IncidenteCuadrillaLimpiezaBI();
    }

    @Bean()
    public CuadrillaBI cuadrillaBI() {
        return new CuadrillaBI();
    }

    @Bean()
    public LogBI logBI() {
        return new LogBI();
    }

    @Bean()
    public IncidenteLimpiezaBI incidenteLimpiezaBI() {
        return new IncidenteLimpiezaBI();
    }

    @Bean()
    public DatosEmpMttoBI datosEmpMttoBI() {
        return new DatosEmpMttoBI();
    }

    @Bean()
    public IncidenteCuadrillaBI incidenteCuadrillaBI() {
        return new IncidenteCuadrillaBI();
    }

    @Bean()
    public IncidenteLimpiezaDosBI incidenteLimpiezaDosBI() {
        return new IncidenteLimpiezaDosBI();
    }

    @Bean()
    public ServiciosRemedyLimpiezaDosBI serviciosRemedyLimpiezaDosBI() {
        return new ServiciosRemedyLimpiezaDosBI();
    }

}
