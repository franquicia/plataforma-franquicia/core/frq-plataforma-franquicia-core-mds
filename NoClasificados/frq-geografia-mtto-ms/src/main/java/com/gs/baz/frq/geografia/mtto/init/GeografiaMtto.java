package com.gs.baz.frq.geografia.mtto.init;

import com.gs.baz.frq.geografia.mtto.dao.GeografiaMttoDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.geografia.mtto")
public class GeografiaMtto {

    private final Logger logger = LogManager.getLogger();

    public GeografiaMtto() {
        logger.info("Loading " + this.getClass().getName() + "...!");
    }

    @Bean(initMethod = "init", name = "frqGeografiaMttoDAOImpl")
    public GeografiaMttoDAOImpl geografiaMttoDAOImpl() {
        return new GeografiaMttoDAOImpl();
    }
}
