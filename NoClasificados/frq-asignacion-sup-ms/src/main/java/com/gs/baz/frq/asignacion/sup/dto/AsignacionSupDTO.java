/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.asignacion.sup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AsignacionSupDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Integer inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    //nuevos eliminar lo anterior
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fecha")
    private String fecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "acronimo")
    private String acronimo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status")
    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCheckList")
    private Integer idCheckList;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idVersion")
    private Integer idVersion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idNegocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "parametro")
    private Integer parametro;
    //tabla version Supervision

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idUsuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idGerente")
    private Integer idGerente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idPerfil")
    private Integer idPerfil;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreUsuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "eco")
    private String eco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicio")
    private String fechaInicio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFin")
    private String fechaFin;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio")
    private String negocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "zona")
    private String zona;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "territorio")
    private String territorio;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public String getEco() {
        return eco;
    }

    public void setEco(String eco) {
        this.eco = eco;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public AsignacionSupDTO() {
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public Integer getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(Integer idCheckList) {
        this.idCheckList = idCheckList;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public Integer getInserted() {
        return inserted;
    }

    public void setInserted(Integer inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getParametro() {
        return parametro;
    }

    public void setParametro(Integer parametro) {
        this.parametro = parametro;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(Integer idGerente) {
        this.idGerente = idGerente;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    @Override
    public String toString() {
        return "AsignacionSupDTO [inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + ", periodo="
                + periodo + ", fecha=" + fecha + ", descripcion=" + descripcion + ", acronimo=" + acronimo + ", status="
                + status + ", idCheckList=" + idCheckList + ", idVersion=" + idVersion + ", idNegocio=" + idNegocio
                + ", parametro=" + parametro + ", idUsuario=" + idUsuario + ", idGerente=" + idGerente + ", idCeco="
                + idCeco + ", idPerfil=" + idPerfil + ", nombreUsuario=" + nombreUsuario + ", nombreCeco=" + nombreCeco
                + ", eco=" + eco + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", negocio=" + negocio
                + "]";
    }

}
