package com.gs.baz.tipo.medicion.mprs;

import com.gs.baz.tipo.medicion.dto.TipoMedicionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class TipoMedicionRowMapper implements RowMapper<TipoMedicionDTO> {

    private TipoMedicionDTO tipoMedicionDTO;

    @Override
    public TipoMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        tipoMedicionDTO = new TipoMedicionDTO();
        tipoMedicionDTO.setIdTipoMedicion(((BigDecimal) rs.getObject("FIIDTIPO")));
        tipoMedicionDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        return tipoMedicionDTO;
    }
}
