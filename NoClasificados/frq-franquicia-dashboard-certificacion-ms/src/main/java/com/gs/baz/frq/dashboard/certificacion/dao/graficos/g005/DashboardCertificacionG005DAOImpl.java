package com.gs.baz.frq.dashboard.certificacion.dao.graficos.g005;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g005.DashboardCertificacionG005DTO;
import com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g005.DashboardCertificacionG005RowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG005DAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectCoberturaBarras;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectCoberturaBarras = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCoberturaBarras.withSchemaName(schema);
        jdbcSelectCoberturaBarras.withCatalogName("PADASHCCCOBERT");
        jdbcSelectCoberturaBarras.withProcedureName("SPGCOBERXSEM");
        jdbcSelectCoberturaBarras.returningResultSet("PA_CDATOS", new DashboardCertificacionG005RowMapper());

    }

    public List<DashboardCertificacionG005DTO> selectRowDashboardCertificacionG005(Long entityDTO1, Long entityDTO2, Long entityDTO3, String entityDTO4) throws CustomException {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        mapSqlParameterSource.addValue("PA_FIGERENTEID", entityDTO1);
        mapSqlParameterSource.addValue("PA_FINEGOCIO_ID", entityDTO2);
        mapSqlParameterSource.addValue("PA_SEMANASATRAS", entityDTO3);
        mapSqlParameterSource.addValue("PA_FECHA_ACTUAL", entityDTO4);
        Map<String, Object> out = jdbcSelectCoberturaBarras.execute(mapSqlParameterSource);
        return (List<DashboardCertificacionG005DTO>) out.get("PA_CDATOS");

    }

}
