/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del Rango", value = "Rango")
public class Rango extends RangoBase {

    @JsonProperty(value = "idRango")
    @ApiModelProperty(notes = "Identificador del Rango", example = "1", position = -1)
    private Integer idRango;

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    @Override
    public String toString() {
        return "Rango{" + "idRango=" + idRango + '}';
    }

}
