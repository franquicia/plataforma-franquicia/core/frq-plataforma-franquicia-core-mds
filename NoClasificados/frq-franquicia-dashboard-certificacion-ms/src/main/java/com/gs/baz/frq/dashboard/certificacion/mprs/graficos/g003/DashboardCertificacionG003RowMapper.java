package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g003;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g003.DashboardCertificacionG003DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG003RowMapper implements RowMapper<DashboardCertificacionG003DTO> {

    private DashboardCertificacionG003DTO dashboardCertificacionG002DTO;

    @Override
    public DashboardCertificacionG003DTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        dashboardCertificacionG002DTO = new DashboardCertificacionG003DTO();
        dashboardCertificacionG002DTO.setIdGerente((BigDecimal) rs.getObject("FIGERENTEID"));
        dashboardCertificacionG002DTO.setNombre(rs.getString("NOMBRE"));
        dashboardCertificacionG002DTO.setTerritorio(rs.getString("FCTERRITORIO"));
        dashboardCertificacionG002DTO.setTotalAsignados((BigDecimal) rs.getObject("TOTAL_ASIGNADOS"));
        dashboardCertificacionG002DTO.setTotalCertificados((BigDecimal) rs.getObject("TOTAL_CERTIFICADOS"));
        dashboardCertificacionG002DTO.setRango((BigDecimal) rs.getObject("RANGO"));
        dashboardCertificacionG002DTO.setRangoColor(rs.getString("RANGOCOLOR"));

        return dashboardCertificacionG002DTO;
    }
}
