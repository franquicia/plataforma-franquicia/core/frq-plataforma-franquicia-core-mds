/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.rest;

import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dao.CecoProyectoFolioDAOImpl;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.CecoProyectoFolio;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.CecoProyectoFolioFormato;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.Formato;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.RequestActualiza;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.RequestConsulta;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.RequestElimina;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.RequestInserta;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.ResponseConsulta;
import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fjaramillo
 */
@Api(tags = "cecoProyectoFolio", value = "cecoProyectoFolio", description = "Api para la gestión del catalogo de relacion Ceco, Proyecto, Folio")
@RestController
@RequestMapping("/api-local/documento-sucursal/ceco-proyecto-folio/v1")
public class CecoProyectoFolioApi {

    @Autowired
    private CecoProyectoFolioDAOImpl cecoProyectoFolioDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    @ApiOperation(value = "Obtiene datos", notes = "Obtiene los datos de la tabla de relacion Ceco, Proyecto, Folio", nickname = "obtieneDatos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 200, message = "Información no encontrada", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseConsulta obtieneDatos(
            @ApiParam(name = "RequestConsulta", value = "Objeto que contiene los campos por los cual se realizara la consulta", required = true)
            @RequestBody RequestConsulta request
    ) throws DataNotFoundException {

        ResponseConsulta response = new ResponseConsulta(cecoProyectoFolioDAOImpl.selectRows(request.getCentroCostos(), request.getIdProyecto(), request.getIdFormato()));

        if (response.getData() == null || response.getData().isEmpty()) {

            throw new DataNotFoundException();

        }

        return response;

    }

    @ApiOperation(value = "Obtiene datos Formato Ceco", notes = "Obtiene los datos de la tabla de Formato por Ceco", nickname = "obtieneDatosCeco")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 200, message = "Información no encontrada", response = Respuesta404.class)
    })

    @RequestMapping(value = "/formato/ceco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Formato> selectRowProyectoUltimo(@ApiParam(name = "formato", value = "Paramentros para formatos por ceco", required = true) @RequestBody Formato formato) throws CustomException, DataNotFoundException {
        return (List<Formato>) cecoProyectoFolioDAOImpl.selectRowsCeco(formato);
    }

    @ApiOperation(value = "Inserta datos", notes = "Inserta datos en la tabla de relacion Ceco, Proyecto, Folio", nickname = "insertaRelacion")
    @RequestMapping(value = "/inserta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado insertaRelacion(
            @ApiParam(name = "RequestInserta", value = "Objeto que contiene los datos a insertar en la tabla", required = true)
            @RequestBody RequestInserta request
    ) throws DataNotInsertedException {

        cecoProyectoFolioDAOImpl.insertRow(new CecoProyectoFolio(
                request.getCentroCostos(),
                request.getIdProyecto(),
                request.getFolio(),
                request.getIdFormato()));

        return new SinResultado();

    }

    @ApiOperation(value = "Inserta datos", notes = "Inserta datos en la tabla de relacion Ceco, Proyecto, Folio", nickname = "eliminaRelacion")
    @RequestMapping(value = "/", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaRelacion(
            @ApiParam(name = "RequestElimina", value = "Objeto que contiene los datos a elminar en la tabla", required = true)
            @RequestBody RequestElimina request
    ) throws DataNotDeletedException {

        cecoProyectoFolioDAOImpl.deleteRow(new CecoProyectoFolio(
                request.getCentroCostos(),
                request.getIdProyecto(),
                request.getFolio(),
                request.getIdFormato()));

        return new SinResultado();

    }

    @ApiOperation(value = "Actualiza datos", notes = "Actualiza datos en la tabla de relacion Ceco, Proyecto, Folio", nickname = "insertaRelacion")
    @RequestMapping(value = "/actualiza", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado updateRelacion(
            @ApiParam(name = "RequestActualiza", value = "Objeto que contiene los datos a actualizar en la tabla", required = true)
            @RequestBody RequestActualiza request
    ) throws DataNotUpdatedException {

        cecoProyectoFolioDAOImpl.updateRow(new CecoProyectoFolioFormato(
                request.getCentroCostos(),
                request.getIdProyecto(),
                request.getFolio(),
                request.getIdFormato(),
                request.getIdDocumento(),
                request.getIdEstatus()));

        return new SinResultado();

    }

}
