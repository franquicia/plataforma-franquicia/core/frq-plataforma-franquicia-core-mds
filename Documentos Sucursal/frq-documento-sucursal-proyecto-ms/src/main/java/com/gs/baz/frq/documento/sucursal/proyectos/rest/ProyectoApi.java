/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.proyectos.rest;

import com.gs.baz.frq.documento.sucursal.proyectos.dao.ProyectoDAOImpl;
import com.gs.baz.frq.documento.sucursal.proyectos.dto.AltaProyecto;
import com.gs.baz.frq.documento.sucursal.proyectos.dto.ParametrosProyecto;
import com.gs.baz.frq.documento.sucursal.proyectos.dto.ProyectoBase;
import com.gs.baz.frq.documento.sucursal.proyectos.dto.Proyectos;
import com.gs.baz.frq.documento.sucursal.proyectos.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "proyectos", value = "proyectos", description = "Api para la gestión del catalogo de proyectos")
@RestController
@RequestMapping("/api-local/documento-sucursal/proyecto/v1")
public class ProyectoApi {

    @Autowired
    private ProyectoDAOImpl proyectoDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idProyecto
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene proyecto", notes = "Obtiene un proyecto", nickname = "obtieneProyecto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idProyecto}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProyectoBase obtieneProyecto(@ApiParam(name = "idProyecto", value = "Identificador del proyecto", example = "1", required = true) @PathVariable("idProyecto") Long idProyecto) throws CustomException, DataNotFoundException {
        ProyectoBase proyectoBase = proyectoDAOImpl.selectRow(idProyecto);
        if (proyectoBase == null) {
            throw new DataNotFoundException();
        }
        return proyectoBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene proyectos", notes = "Obtiene todos los proyectos", nickname = "obtieneProyectos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Proyectos obtieneProyectos() throws CustomException, DataNotFoundException {
        Proyectos proyectos = new Proyectos(proyectoDAOImpl.selectAllRows());
        if (proyectos.getProyectos() != null && proyectos.getProyectos().isEmpty()) {
            throw new DataNotFoundException();
        }
        return proyectos;
    }

    /**
     *
     * @param parametrosProyecto
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear proyecto", notes = "Agrega un proyecto", nickname = "creaProyecto")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaProyecto creaProyecto(@ApiParam(name = "ParametrosProyecto", value = "Paramentros para el alta del proyecto", required = true) @RequestBody ParametrosProyecto parametrosProyecto) throws DataNotInsertedException {
        return proyectoDAOImpl.insertRow(parametrosProyecto);
    }

    /**
     *
     * @param parametrosProyecto
     * @param idProyecto
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar proyecto", notes = "Actualiza un proyecto", nickname = "actualizaProyecto")
    @RequestMapping(value = "/{idProyecto}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaProyecto(@ApiParam(name = "ParametrosProyecto", value = "Paramentros para la actualización del proyecto", required = true) @RequestBody ParametrosProyecto parametrosProyecto, @ApiParam(name = "idProyecto", value = "Identificador del proyecto", example = "1", required = true) @PathVariable("idProyecto") Integer idProyecto) throws DataNotUpdatedException {
        parametrosProyecto.setIdProyecto(idProyecto);
        proyectoDAOImpl.updateRow(parametrosProyecto);
        return new SinResultado();
    }

    /**
     *
     * @param idProyecto
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar proyecto", notes = "Elimina un item de los proyectos", nickname = "eliminaProyecto")
    @RequestMapping(value = "/{idProyecto}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaProyecto(@ApiParam(name = "idProyecto", value = "Identificador del proyecto", example = "1", required = true) @PathVariable("idProyecto") Integer idProyecto) throws DataNotDeletedException {
        proyectoDAOImpl.deleteRow(idProyecto);
        return new SinResultado();
    }

}
