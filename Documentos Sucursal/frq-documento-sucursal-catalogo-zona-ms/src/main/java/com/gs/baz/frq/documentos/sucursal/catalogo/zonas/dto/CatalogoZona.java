/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.catalogo.zonas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del catalogo de Zona", value = "CatalogoZonaBase")
public class CatalogoZona extends CatalogoZonaBase {

    @JsonProperty(value = "idCatalogoZona")
    @ApiModelProperty(notes = "Identificador de catalogo zona", example = "1", position = -1)
    private Integer idCatalogoZona;

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del catalogo Zona", example = "Patio Bancario")
    private String descripcion;

    @JsonProperty(value = "estatus")
    @ApiModelProperty(notes = "Descripcion del estatus", example = "1")
    private Integer estatus;

    @JsonProperty(value = "idTipoInformacion")
    @ApiModelProperty(notes = "Descripcion del tipo de informacion si es banco (1) o elektra (2)", example = "1")
    private Integer idTipoInformacion;

    public Integer getIdCatalogoZona() {
        return idCatalogoZona;
    }

    public void setIdCatalogoZona(Integer idCatalogoZona) {
        this.idCatalogoZona = idCatalogoZona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Integer getIdTipoInformacion() {
        return idTipoInformacion;
    }

    public void setIdTipoInformacion(Integer idTipoInformacion) {
        this.idTipoInformacion = idTipoInformacion;
    }

}
