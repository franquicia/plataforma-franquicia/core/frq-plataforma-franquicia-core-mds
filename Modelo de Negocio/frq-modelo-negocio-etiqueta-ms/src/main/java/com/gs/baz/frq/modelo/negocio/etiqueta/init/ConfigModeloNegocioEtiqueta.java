package com.gs.baz.frq.modelo.negocio.etiqueta.init;

import com.gs.baz.frq.modelo.negocio.etiqueta.dao.EtiquetaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.etiqueta")
public class ConfigModeloNegocioEtiqueta {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioEtiqueta() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public EtiquetaDAOImpl etiquetaDAOImpl() {
        return new EtiquetaDAOImpl();
    }

}
