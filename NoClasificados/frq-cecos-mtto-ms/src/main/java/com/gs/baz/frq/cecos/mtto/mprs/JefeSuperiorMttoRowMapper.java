package com.gs.baz.frq.cecos.mtto.mprs;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gs.baz.frq.cecos.mtto.dto.JefeSuperiorMttoDTO;

/**
 *
 * @author cescobarh
 */
public class JefeSuperiorMttoRowMapper implements RowMapper<JefeSuperiorMttoDTO> {

    private JefeSuperiorMttoDTO status;

    @Override
    public JefeSuperiorMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new JefeSuperiorMttoDTO();

        status.setIdUsuarioJefe(rs.getInt("FIID_SUPERIOR"));;
        status.setIdUsuario(rs.getInt("FIID_USUARIO"));
        status.setIdNivel(rs.getInt("FIID_NIVEL"));
        status.setObservaciones(rs.getString("FCOBSERVACIONES"));
        status.setPeriodo(rs.getString("FCPERIODO"));

        return status;
    }
}
