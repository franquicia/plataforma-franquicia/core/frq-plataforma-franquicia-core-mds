package com.gs.baz.frq.pregunta.visita.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.pregunta.visita.dao.util.GenericDAO;
import com.gs.baz.frq.pregunta.visita.dto.PreguntaVisitaDTO;
import com.gs.baz.frq.pregunta.visita.mprs.PreguntaVisitaRowMapper;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class PreguntaVisitaDAOImpl extends DefaultDAO implements GenericDAO<PreguntaVisitaDTO> {

    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMPREGVISIT");
        jdbcSelect.withProcedureName("SPGETPREGVISITA");
        jdbcSelect.returningResultSet("PA_CDATOS", new PreguntaVisitaRowMapper());
    }

    @Override
    public List<PreguntaVisitaDTO> selectRows(PreguntaVisitaDTO entityDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_BITACORA", entityDTO.getIdBitacora());
        mapSqlParameterSource.addValue("PA_FIID_PROTOCOLO", entityDTO.getIdProtocolo());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", entityDTO.getIdNegocioDisciplina());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<PreguntaVisitaDTO>) out.get("PA_CDATOS");
    }
}
