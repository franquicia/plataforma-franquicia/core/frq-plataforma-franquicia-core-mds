/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.arboles.mprs;

import com.gs.baz.frq.modelo.negocio.arboles.dto.ArbolBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ArbolRowMapper implements RowMapper<ArbolBase> {

    private ArbolBase arbolBase;

    @Override
    public ArbolBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        arbolBase = new ArbolBase();
        arbolBase.setIdModeloNegocioPadre(rs.getInt("FIIDPADRE"));
        arbolBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        arbolBase.setNombre(rs.getString("FCNOMBRE"));
        arbolBase.setIcono(rs.getString("FCICONO"));
        arbolBase.setIdNivel(rs.getInt("FIIDNIVEL"));
        return arbolBase;
    }
}
