/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.geografia.mtto.rest;

import com.gs.baz.frq.geografia.mtto.dao.GeografiaMttoDAOImpl;
import com.gs.baz.frq.geografia.mtto.dto.GeografiaMttoDTO;
import com.gs.baz.frq.geografia.mtto.dto.JefeSuperiorMttoDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/geografia-mtto")
@Component("FRQServiceGeografiaMtto")
public class ServiceGeografiaJefe {

    @Autowired
    private GeografiaMttoDAOImpl geografiaMttoDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

//negocio
    @RequestMapping(value = "secure/get/geografia", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GeografiaMttoDTO> getGeografia(@RequestBody GeografiaMttoDTO negocio) throws CustomException {

        return (List<GeografiaMttoDTO>) geografiaMttoDAOImpl.selectRowDTO(negocio);

    }

    @RequestMapping(value = "secure/post/alta/geografia", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postAltaGeografia(@RequestBody GeografiaMttoDTO asignacion) throws CustomException {

        if (asignacion.getDescripcion() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Geografia"));
        }
        return geografiaMttoDAOImpl.insertRow(asignacion);
    }

    @RequestMapping(value = "secure/put/geografia", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public GeografiaMttoDTO putUpdateGeografia(@RequestBody GeografiaMttoDTO updateNego) throws CustomException {
        if (updateNego.getIdGeografia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update Geografia"));
        }

        return geografiaMttoDAOImpl.updateRow(updateNego);
    }

    @RequestMapping(value = "secure/delete/geografia", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaGeografia(@RequestBody GeografiaMttoDTO elimina) throws CustomException {
        if (elimina.getIdGeografia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Dep geografia"));
        }

        return geografiaMttoDAOImpl.eliminaRows(elimina);
    }

    //Jefe-Usuario
    @RequestMapping(value = "secure/get/jefeResponsable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JefeSuperiorMttoDTO> getJefeResponsable(@RequestBody JefeSuperiorMttoDTO negocio) throws CustomException {

        return (List<JefeSuperiorMttoDTO>) geografiaMttoDAOImpl.selectRowJefe(negocio);

    }

    @RequestMapping(value = "secure/post/alta/jefeResponsable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int postJefeResponsable(@RequestBody JefeSuperiorMttoDTO asignacion) throws CustomException {

        if (asignacion.getIdUsuarioJefe() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("alta Jefe-Usuario"));
        }
        return geografiaMttoDAOImpl.insertJefeRow(asignacion);
    }

    @RequestMapping(value = "secure/put/jefeResponsable", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public JefeSuperiorMttoDTO putJefeResponsable(@RequestBody JefeSuperiorMttoDTO updateNego) throws CustomException {
        if (updateNego.getIdUsuarioJefe() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Update Jefe-Usuario"));
        }

        return geografiaMttoDAOImpl.updateJefeRow(updateNego);
    }

    @RequestMapping(value = "secure/delete/jefeResponsable", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int eliminaJefeResponsable(@RequestBody JefeSuperiorMttoDTO elimina) throws CustomException {
        if (elimina.getIdUsuarioJefe() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("Dep Jefe-Usuario"));
        }

        return geografiaMttoDAOImpl.eliminaJefeRows(elimina);
    }

}
