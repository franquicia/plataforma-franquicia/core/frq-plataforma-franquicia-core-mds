/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.unidad.negocio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos de la unidad de negocio", value = "UnidadNegocio")
public class UnidadNegocio extends UnidadNegocioBase {

    @JsonProperty(value = "idUnidadNegocio")
    @ApiModelProperty(notes = "Identificador de la unidad de negocio", example = "1", position = -1)
    private Integer idUnidadNegocio;

    public Integer getIdUnidadNegocio() {
        return idUnidadNegocio;
    }

    public void setIdUnidadNegocio(Integer idUnidadNegocio) {
        this.idUnidadNegocio = idUnidadNegocio;
    }

}
