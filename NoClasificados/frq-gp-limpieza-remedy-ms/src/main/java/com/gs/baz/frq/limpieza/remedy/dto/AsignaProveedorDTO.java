/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.limpieza.remedy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author leodan1991
 */
@ApiModel(description = "Asigna a un Proveedor", value = "AsignaProveedor")
public class AsignaProveedorDTO {

    @JsonProperty(value = "idIncidente", required = true)
    @ApiModelProperty(notes = "Clave del incidente", example = "13007962", position = -1, required = true)
    private Integer idIncidente;

    @JsonProperty(value = "proveedor", required = true)
    @ApiModelProperty(notes = "Proveedor para el incidente", example = "Lavatap SA de CV", required = true)
    private String proveedor;

    public AsignaProveedorDTO() {
        super();

    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public String toString() {
        return "AsignaProveedorDTO{" + "idIncidente=" + idIncidente + ", proveedor=" + proveedor + '}';
    }

}
