package com.gs.baz.frq.modelo.negocio.folio.etiquetas.init;

import com.gs.baz.frq.modelo.negocio.folio.etiquetas.dao.FolioEtiquetaDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.folio.etiquetas")
public class ConfigModeloNegocioFolioEtiqueta {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioFolioEtiqueta() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public FolioEtiquetaDAOImpl FolioEtiquetaDAOImpl() {
        return new FolioEtiquetaDAOImpl();
    }

}
