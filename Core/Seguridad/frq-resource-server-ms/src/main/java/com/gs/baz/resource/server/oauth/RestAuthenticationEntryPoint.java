/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.resource.server.oauth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.swagger.constants.ResponseApiCodes;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.responses.RespuestaErrorClient;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @param e
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        ResponseApiCodes apiCode;
        if (e.getMessage().contains("Cannot convert access token to JSON")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04001;
        } else if (e.getMessage().contains("Full authentication is required to access this resource")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04031;
        } else if (e.getMessage().contains("Access token expired:")) {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_04011;
        } else {
            apiCode = ResponseApiCodes.RESPONSE_MESSAGE_00000;
        }
        String requestID = (String) httpServletRequest.getAttribute("requestID");
        String nameApi = (String) httpServletRequest.getAttribute("nameApi");
        ResponseMessage responseMessage = ResponseMensages.getByStatus(401);
        String codeError = responseMessage.getCode() + "." + nameApi + "." + apiCode.code();
        RespuestaErrorClient respuestaErrorClient = new RespuestaErrorClient(codeError, responseMessage.getMessage(), requestID);
        httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        OutputStream out = httpServletResponse.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, respuestaErrorClient);
        out.flush();
    }

}
