/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.bi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.security.access.dto.AccesoAsimetricoDTO;
import com.gs.baz.security.access.util.DecryptException;
import com.gs.baz.security.access.util.SecurityJsonReaderUtil;
import com.gs.baz.security.access.util.SecurityUtilsException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;

/**
 *
 * @author cescobarh
 */
public class RequestSecurityWrapper extends HttpServletRequestWrapper {

    private final Logger logger = LogManager.getLogger();
    private String body;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private SecurityJsonReaderUtil securityJsonReaderUtil;

    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws CustomException
     */
    public RequestSecurityWrapper(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws CustomException {
        super(httpServletRequest);
        try {
            body = new BufferedReader(new InputStreamReader(httpServletRequest.getInputStream())).lines().parallel().collect(Collectors.joining("\n"));
        } catch (IOException ex) {
            throw new CustomException(ex);
        }
        String mt = httpServletRequest.getContentType();
        if (body != null && !body.isEmpty()) {
            if (mt != null && mt.equals(MediaType.APPLICATION_JSON_VALUE)) {
                JsonNode jsonNode;
                try {
                    jsonNode = objectMapper.readValue(body, JsonNode.class);
                } catch (Exception ex) {
                    logger.info(ex);
                    throw new CustomException(ex);
                }
                Boolean hasSensibleData = (Boolean) httpServletRequest.getAttribute("has_sensible_attributes");
                if (hasSensibleData) {
                    AccesoAsimetricoDTO accesoAsimetricoDTO = (AccesoAsimetricoDTO) httpServletRequest.getAttribute("rsa_keys");
                    if (accesoAsimetricoDTO != null) {
                        try {
                            List<String> sensibleAttributesIn = (List<String>) httpServletRequest.getAttribute("sensible_attributes_in");
                            securityJsonReaderUtil = new SecurityJsonReaderUtil(accesoAsimetricoDTO, sensibleAttributesIn);
                            jsonNode = securityJsonReaderUtil.decryptJsonNodeValues(jsonNode);
                            try {
                                body = objectMapper.writeValueAsString(jsonNode);
                            } catch (JsonProcessingException ex) {
                                throw new CustomException(ex);
                            }
                        } catch (SecurityUtilsException | DecryptException ex) {
                            throw new CustomException(ex);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @return @throws IOException
     */
    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new ServletInputStreamRequestWrapper(body.getBytes());
    }

    /**
     *
     * @return @throws IOException
     */
    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }
}
