/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.filtros;

import com.gs.baz.frq.dashboard.dao.filtros.FiltrosDashboardDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroAnioDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroCecoDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroNegocioDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltroNivelGeograficoDTO;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceFiltrosDashboard {

    @Autowired
    private FiltrosDashboardDAOImpl filtrosDashboardDAOImpl;

    @RequestMapping(value = "secure/filter/get/anio/{disciplina}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FiltroAnioDTO> getAnios(@PathVariable("disciplina") Long disciplina) throws CustomException {
        return filtrosDashboardDAOImpl.selectRowsAnio(disciplina);
    }

    @RequestMapping(value = "secure/filter/get/periodo/{anio}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FiltrosDTO> getPeriodos(@PathVariable("anio") Integer anio, @RequestBody FiltrosDTO filtroPeriodo) throws CustomException {
        filtroPeriodo.setAnio(anio);
        return filtrosDashboardDAOImpl.selectRowsPeriodo(filtroPeriodo);
    }

    @RequestMapping(value = "secure/filter/get/negocio/{id_disciplina}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FiltroNegocioDTO> getNegocios(@PathVariable("id_disciplina") Long idDisciplina) throws CustomException {
        return filtrosDashboardDAOImpl.selectRowsNegocios(idDisciplina);
    }

    @RequestMapping(value = "secure/filter/get/nivel/geografico/{id_negocio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FiltroNivelGeograficoDTO> getNivelesGeograficos(@PathVariable("id_negocio") Long idNegocio) throws CustomException {
        List<FiltroNivelGeograficoDTO> nivelesGeograficos = new ArrayList<>();
        nivelesGeograficos.add(new FiltroNivelGeograficoDTO(4, "Nacional"));
        if (idNegocio.equals(3l)) {
            nivelesGeograficos.add(new FiltroNivelGeograficoDTO(3, "Operación"));
        } else {
            nivelesGeograficos.add(new FiltroNivelGeograficoDTO(3, "Territorio"));
        }
        if (idNegocio.equals(3l)) {
            nivelesGeograficos.add(new FiltroNivelGeograficoDTO(2, "Plaza"));
        } else if (idNegocio.equals(5l)) {
            nivelesGeograficos.add(new FiltroNivelGeograficoDTO(2, "Cuartel"));
        } else {
            nivelesGeograficos.add(new FiltroNivelGeograficoDTO(2, "Zona"));
        }
        nivelesGeograficos.add(new FiltroNivelGeograficoDTO(1, "Región"));
        nivelesGeograficos.add(new FiltroNivelGeograficoDTO(0, "Punto de contacto"));
        return nivelesGeograficos;
    }

    @RequestMapping(value = "secure/filter/get/nivel/geografico/hijo/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FiltroCecoDTO> getHijosNivelGeografico(@RequestBody FiltroCecoDTO filtroCeco) throws CustomException {
        return filtrosDashboardDAOImpl.selectRowsNivelGeograficoHijos(filtroCeco);
    }

}
