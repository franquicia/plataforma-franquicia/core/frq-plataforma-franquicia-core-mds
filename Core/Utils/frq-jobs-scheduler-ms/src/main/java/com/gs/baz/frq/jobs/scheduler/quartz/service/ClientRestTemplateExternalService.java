package com.gs.baz.frq.jobs.scheduler.quartz.service;

import com.gs.baz.frq.jobs.scheduler.dto.Execution;
import com.gs.baz.frq.jobs.scheduler.dto.ExecutionURL;
import com.gs.baz.frq.jobs.scheduler.dto.ScheduleJob;
import com.gs.baz.frq.jobs.scheduler.rest.client.internal.SubServiceGenericExternalEntity;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class ClientRestTemplateExternalService {

    private final Logger logger = LogManager.getLogger();
    private String basePath;
    private String body;
    private String requestMethod;
    private ExecutionURL executionURL;
    private final SubServiceGenericExternalEntity subServiceGenericExternalEntity = new SubServiceGenericExternalEntity();

    private String getEntity() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
        subServiceGenericExternalEntity.init(basePath, httpEntity);
        return subServiceGenericExternalEntity.getEntity();
    }

    private String postEntity() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
        subServiceGenericExternalEntity.init(basePath, httpEntity);
        return subServiceGenericExternalEntity.addEntity();
    }

    private String putEntity() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
        subServiceGenericExternalEntity.init(basePath, httpEntity);
        return subServiceGenericExternalEntity.updateEntity();
    }

    private String deleteEntity() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        final HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
        subServiceGenericExternalEntity.init(basePath, httpEntity);
        return subServiceGenericExternalEntity.deleteEntity();
    }

    public void run(ScheduleJob scheduleJob) {
        this.executionURL = scheduleJob.getExecutionURL();
        this.basePath = executionURL.getUri();
        this.body = executionURL.getRequestBody();
        this.requestMethod = executionURL.getRequestMethod();
        Execution execution = scheduleJob.getExecution();
        if (execution == null) {
            execution = new Execution();
        }
        String response = "none";
        try {
            if (this.requestMethod.toLowerCase().equals("get")) {
                response = this.getEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            if (this.requestMethod.toLowerCase().equals("post")) {
                response = this.postEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            if (this.requestMethod.toLowerCase().equals("put")) {
                response = this.putEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            if (this.requestMethod.toLowerCase().equals("delete")) {
                response = this.deleteEntity();
                logger.info("job name " + scheduleJob.getJobName() + " excuted success, result: " + response);
            }
            execution.setLastExecutionSucccess(new Date());
            execution.setLastExecutionSucccessResult(response);
        } catch (Exception ex) {
            response = ex.getMessage();
            execution.setLastExecutionError(new Date());
            execution.setLastExecutionErrorResult(response);
            logger.trace(response, ex);
        }
        scheduleJob.setExecution(execution);
    }
}
