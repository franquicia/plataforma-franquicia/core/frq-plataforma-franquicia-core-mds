/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.frecuencia.medicion.rest;

import com.gs.baz.frq.frecuencia.medicion.dao.FrecuenciaMedicionDAOImpl;
import com.gs.baz.frq.frecuencia.medicion.dto.FrecuenciaMedicionDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/frecuencia/medicion")
@Component("FRQServiceFrecuenciaMedicion")
public class ServiceFrecuenciaMedicion {

    @Autowired
    private FrecuenciaMedicionDAOImpl frecuenciaMedicionDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO postFrecuenciaMedicion(@RequestBody FrecuenciaMedicionDTO frecuenciaMedicion) throws CustomException {
        if (frecuenciaMedicion.getIdFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia"));
        }
        if (frecuenciaMedicion.getFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("frecuencia"));
        }
        return frecuenciaMedicionDAOImpl.insertRow(frecuenciaMedicion);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO putFrecuenciaMedicion(@RequestBody FrecuenciaMedicionDTO frecuenciaMedicion) throws CustomException {
        if (frecuenciaMedicion.getIdFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia"));
        }
        if (frecuenciaMedicion.getFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("frecuencia"));
        }
        return frecuenciaMedicionDAOImpl.updateRow(frecuenciaMedicion);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FrecuenciaMedicionDTO> getFrecuenciaMedicion() throws CustomException {
        return frecuenciaMedicionDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/{id_frecuencia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO getFrecuenciaMedicion(@PathVariable("id_frecuencia") Long id_frecuencia) throws CustomException {
        return frecuenciaMedicionDAOImpl.selectRow(id_frecuencia);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FrecuenciaMedicionDTO deleteFrecuenciaMedicion(@RequestBody FrecuenciaMedicionDTO frecuenciaMedicion) throws CustomException {
        if (frecuenciaMedicion.getIdFrecuencia() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_frecuencia"));
        }
        return frecuenciaMedicionDAOImpl.deleteRow(frecuenciaMedicion);
    }
}
