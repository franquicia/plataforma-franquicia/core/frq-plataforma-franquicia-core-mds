/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.archivos.rest;

import com.gs.baz.frq.archivos.bi.ArchivosApiBI;
import com.gs.baz.frq.archivos.dto.ElementoDTO;
import com.gs.baz.frq.archivos.dto.ParamentrosArchivoDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Path;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/api-local/archivos/")
@Component("APIArchivos")
public class APIArchivos {

    @Autowired
    private ArchivosApiBI archivosApiBI;

    @ApiOperation(value = "Carga archivo", notes = "Servicio para cargar archivo", nickname = "cargaArchivo")
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void cargaArchivo(@ApiParam(value = "Archivo origen", required = true) @RequestParam("archivo") final MultipartFile inputFile, @ApiParam("Parametro destino.") @RequestParam("destino") String destino) throws Exception {
        ParamentrosArchivoDTO paramentrosArchivoDTO = new ParamentrosArchivoDTO(destino, inputFile);
        archivosApiBI.writeFile(paramentrosArchivoDTO);
    }

    @ApiOperation(value = "Actualiza archivo", notes = "Servicio para actualizar archivo", nickname = "actualizaArchivo")
    @RequestMapping(value = "/", method = RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void actualizaArchivo(@ApiParam(value = "Archivo origen", required = true) @RequestParam("archivo") final MultipartFile inputFile, @ApiParam("Parametro destino.") @RequestParam("destino") String destino) throws Exception {
        ParamentrosArchivoDTO paramentrosArchivoDTO = new ParamentrosArchivoDTO(destino, inputFile);
        archivosApiBI.updateFile(paramentrosArchivoDTO);
    }

    @ApiOperation(value = "Visualiza archivo", notes = "Servicio para visualizar archivo", nickname = "visualizaArchivo")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public ResponseEntity visualizaArchivo(@ApiParam(name = "ruta", value = "Información para la visualizacion del archivo", required = true) @RequestParam("ruta") String pathFile) throws Exception {
        try {
            HttpHeaders headers = new HttpHeaders();
            Path path = archivosApiBI.readFile(pathFile);
            URLConnection urlConnection = path.toUri().toURL().openConnection();
            MediaType mediaType = MediaType.asMediaType(MimeType.valueOf(urlConnection.getContentType()));
            Resource resource = new ByteArrayResource(StreamUtils.copyToByteArray(urlConnection.getInputStream()));
            headers.set("Content-Disposition", "inline;filename=" + path.getFileName().toString());
            return ResponseEntity.status(HttpStatus.OK).headers(headers).contentType(mediaType).body(resource);
        } catch (IOException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Borra archivo", notes = "Servicio para borrar archivo", nickname = "borraArchivo")
    @RequestMapping(value = "/", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void borraArchivo(@ApiParam(name = "ruta", value = "Información para borrar el archivo", required = true) @RequestParam("ruta") String pathFile) throws Exception {
        archivosApiBI.deleteFile(pathFile);
    }

    @ApiOperation(value = "Visualiza directorio", notes = "Servicio para visualizar directorio", nickname = "visualizaDirectorio")
    @RequestMapping(value = "/directorios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ElementoDTO> visualizaDirectorio(@ApiParam(name = "ruta", value = "Información para la visualización del directorio", required = true) @RequestParam("ruta") String pathDir) throws Exception {
        return archivosApiBI.readDirectory(pathDir);
    }

    @ApiOperation(value = "Crea directorio", notes = "Servicio para crear directorio", nickname = "creaDirectorio")
    @RequestMapping(value = "/directorios", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void creaDirectorio(@ApiParam(name = "ruta", value = "Información para la creación del directorio", required = true) @RequestParam("ruta") String pathDir) throws Exception {
        archivosApiBI.makeDirectory(pathDir);
    }

}
