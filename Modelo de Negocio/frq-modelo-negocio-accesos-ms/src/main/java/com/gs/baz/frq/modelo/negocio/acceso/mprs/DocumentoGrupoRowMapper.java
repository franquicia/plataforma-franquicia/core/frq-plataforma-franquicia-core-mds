/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.acceso.mprs;

import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Documento;
import com.gs.baz.frq.modelo.negocio.acceso.client.dto.Folio;
import com.gs.baz.frq.modelo.negocio.acceso.dto.grupo.DocumentoGrupo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class DocumentoGrupoRowMapper implements RowMapper<DocumentoGrupo> {

    private DocumentoGrupo documentoGrupo;

    @Override
    public DocumentoGrupo mapRow(ResultSet rs, int rowNum) throws SQLException {
        documentoGrupo = new DocumentoGrupo();
        Folio folio = new Folio();
        folio.setIdFolio(rs.getInt("FIIDFOLIO"));
        Documento documento = new Documento();
        documento.setIdDocumento(rs.getInt("FIIDDOCUMENTO"));
        documento.setTitulo(rs.getString("FCTITULO"));
        documento.setVersion(rs.getString("FCVERSION"));
        documento.setObjetivo(rs.getString("FCOBJETIVO"));
        documento.setControlCambios(rs.getString("FCCONTROLCAMBIOS"));
        folio.setDocumento(documento);
        documentoGrupo.setFolio(folio);
        documentoGrupo.setIdDocumentoGrupo(rs.getInt("FIIDGPODOCTO"));
        return documentoGrupo;
    }
}
