package com.gs.baz.ceco.mprs;

import com.gs.baz.ceco.dto.CecoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CecoRowMapper {

    private CecoDTO ceco;

    public class consultaGeneral implements RowMapper<CecoDTO> {

        @Override
        public CecoDTO mapRow(ResultSet rs, int i) throws SQLException {
            ceco = new CecoDTO();
            ceco.setIdCeco(rs.getString("FCID_CECO"));
            ceco.setIdCecoSuperior(((BigDecimal) rs.getObject("FICECO_SUPERIOR")));
            ceco.setDescripcion(rs.getString("FCNOMBRE"));
            ceco.setCalle(rs.getString("FCCALLE"));
            ceco.setCiudad(rs.getString("FCCIUDAD"));
            ceco.setCodigoPostal(rs.getString("FCCP"));
            ceco.setNombreContacto(rs.getString("FCNOMBRE_CTO"));
            return ceco;
        }
    }

    public class consultaEspecifica implements RowMapper<CecoDTO> {

        @Override
        public CecoDTO mapRow(ResultSet rs, int i) throws SQLException {
            ceco = new CecoDTO();
            ceco.setIdCeco(rs.getString("FCID_CECO"));;
            ceco.setDescripcion(rs.getString("FCNOMBRE"));
            return ceco;
        }

    }
}
