package com.gs.baz.frq.dashboard.certificacion.mprs.punto.venta;

import com.gs.baz.frq.dashboard.certificacion.dto.punto.venta.DashboardCertificacionPuntoVentaDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionPuntoVentaRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

    private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

    @Override
    public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
        dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("CECOBASE_ID"));
        dashboardCertificacionPuntoVentaDTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionPuntoVentaDTO.setNombreCeco(rs.getString("NOMBRE_CECO"));
        dashboardCertificacionPuntoVentaDTO.setFechaVisita(rs.getString("FECHA_VISITA"));
        dashboardCertificacionPuntoVentaDTO.setNumeroEmpleado((BigDecimal) rs.getObject("NUMERO_EMPLEADO"));
        dashboardCertificacionPuntoVentaDTO.setNombreEmpleado(rs.getString("NOMBRE_EMPLEADO"));
        return dashboardCertificacionPuntoVentaDTO;
    }

    public class DashboardCertificacionPuntoVentaDetalleCertRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

        private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

        @Override
        public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
            dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
            dashboardCertificacionPuntoVentaDTO.setIdCeco(rs.getString("FCID_CECO"));
            dashboardCertificacionPuntoVentaDTO.setNombreCeco(rs.getString("NOMBRE_CECO"));
            dashboardCertificacionPuntoVentaDTO.setFecha(rs.getString("FDFECHA"));
            dashboardCertificacionPuntoVentaDTO.setIdUsuario((BigDecimal) rs.getObject("ID_USUARIO"));
            dashboardCertificacionPuntoVentaDTO.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
            dashboardCertificacionPuntoVentaDTO.setIdProtocolo((BigDecimal) rs.getObject("ID_PROTOCOLO"));
            dashboardCertificacionPuntoVentaDTO.setNombreProtocolo(rs.getString("NOMBRE_PROTOCOLO"));
            dashboardCertificacionPuntoVentaDTO.setCalificacionProtocolo((BigDecimal) rs.getObject("CALIFICACION_PROTOCOLO"));
            dashboardCertificacionPuntoVentaDTO.setIdRango((BigDecimal) rs.getObject("RANGO_ID"));
            dashboardCertificacionPuntoVentaDTO.setRangoColor(rs.getString("RANGOCOLOR"));
            dashboardCertificacionPuntoVentaDTO.setIdVisita((BigDecimal) rs.getObject("ID_VISITA"));
            dashboardCertificacionPuntoVentaDTO.setCalificacionLineal((BigDecimal) rs.getObject("FICALIF_LINEAL"));
            dashboardCertificacionPuntoVentaDTO.setPuntosEsperados((BigDecimal) rs.getObject("FIPUNTOSESP"));

            return dashboardCertificacionPuntoVentaDTO;
        }
    }

    public class DashboardCertificacionPuntoVentaRespCertRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

        private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

        @Override
        public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
            dashboardCertificacionPuntoVentaDTO.setIdRespuesta((BigDecimal) rs.getObject("FIID_RESPUESTA"));
            dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("ID_VISITA"));
            dashboardCertificacionPuntoVentaDTO.setIdPregunta((BigDecimal) rs.getObject("ID_PREGUNTA"));
            dashboardCertificacionPuntoVentaDTO.setDescripcionPregunta(rs.getString("DESCRIPCION_PREGUNTA"));
            dashboardCertificacionPuntoVentaDTO.setRespuesta(rs.getString("RESPUESTA"));
            dashboardCertificacionPuntoVentaDTO.setNegocio((BigDecimal) rs.getObject("NEGOCIO"));
            dashboardCertificacionPuntoVentaDTO.setPonderacion((BigDecimal) rs.getObject("PONDERACION"));
            dashboardCertificacionPuntoVentaDTO.setModulo(rs.getString("MODULO"));
            dashboardCertificacionPuntoVentaDTO.setIncumplimiento((BigDecimal) rs.getObject("INCUMPLIMIENTO"));
            dashboardCertificacionPuntoVentaDTO.setChecklist((BigDecimal) rs.getObject("CHECKLIST"));
            dashboardCertificacionPuntoVentaDTO.setImperdonable((BigDecimal) rs.getObject("IMPERDONABLE"));
            return dashboardCertificacionPuntoVentaDTO;
        }
    }

    public class DashboardCertificacionPuntoVentaCalifCecoRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

        private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

        @Override
        public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
            dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
            dashboardCertificacionPuntoVentaDTO.setIdCeco(rs.getString("FCID_CECO"));
            dashboardCertificacionPuntoVentaDTO.setCalificacion((BigDecimal) rs.getObject("CALIFICACION"));
            dashboardCertificacionPuntoVentaDTO.setIdRango((BigDecimal) rs.getObject("RANGO_ID"));
            dashboardCertificacionPuntoVentaDTO.setRangoColor(rs.getString("RANGOCOLOR"));
            return dashboardCertificacionPuntoVentaDTO;
        }
    }

    public class DashboardCertificacionPuntoVentaEvidenciaRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

        private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

        @Override
        public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
            dashboardCertificacionPuntoVentaDTO.setIdVisita((BigDecimal) rs.getObject("ID_VISITA"));
            dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
            dashboardCertificacionPuntoVentaDTO.setIdPregunta((BigDecimal) rs.getObject("ID_PREGUNTA"));
            dashboardCertificacionPuntoVentaDTO.setIdRespuesta((BigDecimal) rs.getObject("ID_RESPUESTA"));
            dashboardCertificacionPuntoVentaDTO.setTipoEvidencia((BigDecimal) rs.getObject("TIPO_EVIDENCIA"));
            dashboardCertificacionPuntoVentaDTO.setRutaEvidencia(rs.getString("RUTA_EVIDENCIA"));
            return dashboardCertificacionPuntoVentaDTO;
        }
    }

    public class DashboardCertificacionPuntoVentaInformeCertRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

        private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

        @Override
        public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
            dashboardCertificacionPuntoVentaDTO.setIdVisita((BigDecimal) rs.getObject("ID_VISITA"));
            dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
            dashboardCertificacionPuntoVentaDTO.setIdUsuario((BigDecimal) rs.getObject("FIID_USUARIO"));
            dashboardCertificacionPuntoVentaDTO.setRutaInformeGeneral(rs.getString("RUTAINFOGRAL"));
            dashboardCertificacionPuntoVentaDTO.setRutaRetro(rs.getString("RUTARETRO"));
            return dashboardCertificacionPuntoVentaDTO;
        }
    }

    public class DashboardCertificacionPuntoVentaCertPreviasRowMapper implements RowMapper<DashboardCertificacionPuntoVentaDTO> {

        private DashboardCertificacionPuntoVentaDTO dashboardCertificacionPuntoVentaDTO;

        @Override
        public DashboardCertificacionPuntoVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

            dashboardCertificacionPuntoVentaDTO = new DashboardCertificacionPuntoVentaDTO();
            dashboardCertificacionPuntoVentaDTO.setIdCecoBase((BigDecimal) rs.getObject("FICECOBASE_ID"));
            dashboardCertificacionPuntoVentaDTO.setIdCeco(rs.getString("FCID_CECO"));
            dashboardCertificacionPuntoVentaDTO.setNombreCeco(rs.getString("NOMBRE_CECO"));
            dashboardCertificacionPuntoVentaDTO.setFecha(rs.getString("FDFECHA"));
            dashboardCertificacionPuntoVentaDTO.setIdUsuario((BigDecimal) rs.getObject("ID_USUARIO"));
            dashboardCertificacionPuntoVentaDTO.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
            dashboardCertificacionPuntoVentaDTO.setCalificacion((BigDecimal) rs.getObject("CALIFICACION"));
            dashboardCertificacionPuntoVentaDTO.setIdRango((BigDecimal) rs.getObject("RANGO_ID"));
            dashboardCertificacionPuntoVentaDTO.setRangoColor(rs.getString("RANGOCOLOR"));
            return dashboardCertificacionPuntoVentaDTO;
        }
    }
}
