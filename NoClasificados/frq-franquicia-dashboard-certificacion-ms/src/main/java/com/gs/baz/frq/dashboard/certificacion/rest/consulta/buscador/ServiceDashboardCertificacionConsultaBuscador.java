/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.certificacion.rest.consulta.buscador;

import com.gs.baz.frq.dashboard.certificacion.dao.consulta.buscador.DashboardCertificacionConsultaBuscadorDAOImpl;
import com.gs.baz.frq.dashboard.certificacion.dto.consulta.buscador.DashboardCertificacionConsultaBuscadorDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard/consulta")
public class ServiceDashboardCertificacionConsultaBuscador {

    @Autowired
    private DashboardCertificacionConsultaBuscadorDAOImpl dashboardCertificacionConsultaBuscadorDAOImpl;

    @RequestMapping(value = "secure/buscador/get/gerente", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionConsultaBuscadorDTO> getRowsPorGerenteG010() throws CustomException {
        return dashboardCertificacionConsultaBuscadorDAOImpl.selectRowsDashboardCertificacionGerenteConsultaBuscador();
    }

    @RequestMapping(value = "secure/buscador/get/gerente/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionConsultaBuscadorDTO> getPorGerenteG010(
            @RequestParam(value = "idGerente", required = true) Long idGerente) throws CustomException {
        if (idGerente == null || idGerente.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("idGerente"));
        } else {
            return dashboardCertificacionConsultaBuscadorDAOImpl.selectRowDashboardCertificacionGerenteConsultaBuscador(idGerente);
        }
    }

    @RequestMapping(value = "secure/buscador/get/certificador", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionConsultaBuscadorDTO> getRowsPorCertificadorG010() throws CustomException {
        return dashboardCertificacionConsultaBuscadorDAOImpl.selectRowsDashboardCertificacionCertificadorConsultaBuscador();
    }

    @RequestMapping(value = "secure/buscador/get/certificador/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionConsultaBuscadorDTO> getPorCertificadorG010(
            @RequestParam(value = "idCertificador", required = true) Long idCertificador) throws CustomException {
        if (idCertificador == null || idCertificador.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("idCertificador"));
        } else {
            return dashboardCertificacionConsultaBuscadorDAOImpl.selectRowDashboardCertificacionCertificadorConsultaBuscador(idCertificador);
        }
    }

    @RequestMapping(value = "secure/buscador/get/ceco", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionConsultaBuscadorDTO> getRowsPorCecoG010() throws CustomException {
        return dashboardCertificacionConsultaBuscadorDAOImpl.selectRowsDashboardCertificacionCecoConsultaBuscador();
    }

    @RequestMapping(value = "secure/buscador/get/ceco/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DashboardCertificacionConsultaBuscadorDTO> getPorCecoG010(
            @RequestParam(value = "idCeco", required = true) String idCeco) throws CustomException {
        if (idCeco == null || idCeco.equals("")) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("idCeco"));
        } else {
            return dashboardCertificacionConsultaBuscadorDAOImpl.selectRowDashboardCertificacionCecoConsultaBuscador(idCeco);
        }
    }
}
