/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.prefijos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del NegocioPrefijo", value = "NegocioPrefijo")
public class Prefijo extends PrefijoBase {

    @JsonProperty(value = "idNegocioPrefijo")
    @ApiModelProperty(notes = "Identificador del NegocioPrefijo", example = "1", position = -1)
    private Integer idNegocioPrefijo;

    public Integer getIdNegocioPrefijo() {
        return idNegocioPrefijo;
    }

    public void setIdNegocioPrefijo(Integer idNegocioPrefijo) {
        this.idNegocioPrefijo = idNegocioPrefijo;
    }

}
