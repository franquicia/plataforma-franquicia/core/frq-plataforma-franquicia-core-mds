/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.ubicacion.sucursal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UbicacionCecoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "identificadorCentroCostos")
    private Integer centroCostos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "canalNegocio")
    private Integer canal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ciudad")
    private String ciudad;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "codigoPostal")
    private String codigoPostal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "longitud")
    private Double longitud;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "latitud")
    private Double latitud;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "numeroEconomico")
    private Integer numeroEconomico;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreSucursal")
    private String nombreSucursal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "pais")
    private Integer pais;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "responsableSucursal")
    private String responsable;

    public Integer getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(Integer centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public Integer getCanal() {
        return canal;
    }

    public void setCanal(Integer canal) {
        this.canal = canal;
    }

    public Integer getNumeroEconomico() {
        return numeroEconomico;
    }

    public void setNumeroEconomico(Integer numeroEconomico) {
        this.numeroEconomico = numeroEconomico;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    @Override
    public String toString() {
        return "{ \'identificadorCentroCostos\':" + centroCostos + ", \'nombreCeco\':"
                + nombreCeco + ", \'canalNegocio\':" + canal + ", \'numeroEconomico\':" + numeroEconomico
                + ", \'ciudad\':" + ciudad + ", \'codigoPostal\':" + codigoPostal + ", \'nombreSucursal\':"
                + nombreSucursal + ", \'longitud\':" + longitud + ", \'latitud\':" + latitud
                + ", \'pais\':" + pais + "}";
    }

}
