/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocio.zona.mprs;

import com.gs.baz.frq.documentos.sucursal.negocio.zona.dto.NegocioZonaBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class NegocioZonaRowMapper implements RowMapper<NegocioZonaBase> {

    private NegocioZonaBase negocioZonaBase;

    @Override
    public NegocioZonaBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        negocioZonaBase = new NegocioZonaBase();
        negocioZonaBase.setIdZona(rs.getInt("FIIDLYZONAS"));
        negocioZonaBase.setEstado(rs.getInt("FIESTADO"));
        negocioZonaBase.setDescripcionZonaLayout(rs.getString("FCDESCRIPCION"));

        return negocioZonaBase;
    }
}
