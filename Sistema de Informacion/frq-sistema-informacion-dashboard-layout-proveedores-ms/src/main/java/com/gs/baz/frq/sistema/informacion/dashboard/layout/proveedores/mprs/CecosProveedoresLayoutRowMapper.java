/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto.CentroCostosProveedor;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class CecosProveedoresLayoutRowMapper implements RowMapper<CentroCostosProveedor> {

    @Override
    public CentroCostosProveedor mapRow(ResultSet rs, int rowNum) throws SQLException {
        CentroCostosProveedor centroCostosProveedor = new CentroCostosProveedor();
        centroCostosProveedor.setIdNegocio(rs.getInt("IDNEGOCIO"));
        centroCostosProveedor.setNombreNegocio(rs.getString("NEGOCIO"));
        centroCostosProveedor.setIdCeco(rs.getInt("FIID_CECO"));
        centroCostosProveedor.setNombreCentroCostos(rs.getString("CECO"));
        centroCostosProveedor.setIdProveedor(rs.getInt("FIIDPROVEEDOR"));
        centroCostosProveedor.setFormato(rs.getString("FORMATO"));
        centroCostosProveedor.setIdFolio(rs.getInt("FIIDFOLIO"));
        centroCostosProveedor.setNombreEstado(rs.getString("ESTADOFOLIO"));
        centroCostosProveedor.setIdEstado(rs.getInt("FIESTADOCARGA"));
        centroCostosProveedor.setFechaFolio(rs.getString("FDFECHAFOLIO"));
        centroCostosProveedor.setFechaActualizacion(rs.getString("FECHA_ACTUALIZACION"));
        centroCostosProveedor.setIdTerritorio(rs.getString("IDTERRITORIO"));
        centroCostosProveedor.setTerritorio(rs.getString("TERRITORIO"));
        centroCostosProveedor.setComentario(rs.getString("COMENTARIO"));
        return centroCostosProveedor;
    }
}
