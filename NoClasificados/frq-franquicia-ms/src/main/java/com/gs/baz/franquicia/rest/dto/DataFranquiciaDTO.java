/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.franquicia.dto.FranquiciaDTO;
import com.gs.baz.model.bucket.client.services.dto.Metadata;
import java.util.List;

/**
 *
 * @author MADA
 */
public class DataFranquiciaDTO {

    @JsonProperty(value = "metadata")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Metadata metadata;

    @JsonProperty(value = "franquicia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private FranquiciaDTO franquiciaDTO;

    @JsonProperty(value = "franquicias")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<FranquiciaDTO> franquiciasDTO;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public FranquiciaDTO getFranquiciaDTO() {
        return franquiciaDTO;
    }

    public void setFranquiciaDTO(FranquiciaDTO franquiciaDTO) {
        this.franquiciaDTO = franquiciaDTO;
    }

    public List<FranquiciaDTO> getFranquiciasDTO() {
        return franquiciasDTO;
    }

    public void setFranquiciasDTO(List<FranquiciaDTO> franquiciasDTO) {
        this.franquiciasDTO = franquiciasDTO;
    }

    @Override
    public String toString() {
        return "DataFranquiciaDTO{" + "metadata=" + metadata + ", franquiciaDTO=" + franquiciaDTO + ", franquiciasDTO=" + franquiciasDTO + '}';
    }

}
