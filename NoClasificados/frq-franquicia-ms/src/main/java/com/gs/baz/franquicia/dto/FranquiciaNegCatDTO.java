/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.franquicia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author MADA
 */
//CategoriaNegocio
@JsonIgnoreProperties(ignoreUnknown = true)
public class FranquiciaNegCatDTO {

    @JsonProperty(value = "id_franquiciaNegCat")
    protected Integer idFranquiciaNegCat;

    @JsonProperty(value = "id_franquicia")
    protected Integer idFranquicia;

    @JsonProperty(value = "id_negocio")
    protected Integer idNegocio;

    @JsonProperty(value = "id_categoria")
    protected Integer idCategoria;

    public Integer getIdFranquiciaNeg() {
        return idFranquiciaNegCat;
    }

    public void setIdFranquiciaNeg(BigDecimal idFranquiciaNeg) {
        this.idFranquiciaNegCat = (idFranquiciaNeg == null ? null : idFranquiciaNeg.intValue());
    }

    public Integer getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(BigDecimal idFranquicia) {
        this.idFranquicia = (idFranquicia == null ? null : idFranquicia.intValue());
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(BigDecimal idCategoria) {
        this.idCategoria = (idCategoria == null ? null : idCategoria.intValue());
    }

}
