/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.mprs;

import com.gs.baz.frq.accesos.sistema.usuarios.dto.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class UsuariosRowMapper implements RowMapper<Usuario> {

    @Override
    public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
        Usuario usuario = new Usuario();
        usuario.setNumeroEmpleado(rs.getInt("FIID_EMPLEADO"));
        usuario.setTipoEmpleado(rs.getString("FCTIPOEMPLEADO"));
        usuario.setNombreUsuario(rs.getString("FCNOMBRE"));
        usuario.setCentroCostos(rs.getString("FCID_CECO"));
        usuario.setCorreo(rs.getString("FCCORREO"));
        return usuario;
    }
}
