/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.datos.zona.rest;

import com.gs.baz.frq.documentos.sucursal.datos.zona.dao.DocumentoSucursalDatosZonaDAOImpl;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.AltaDatoZona;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ArchivoUltimos;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.DatoZonaBase;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.DatosZona;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ParametrosDatoZona;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.SinResultado;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ValidaCecoFormatoProyecto;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorCeco;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorDocumento;
import com.gs.baz.frq.documentos.sucursal.datos.zona.dto.ZonasPorVersion;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "datosZona", value = "datosZona", description = "Api para la gestión de datos de zonas")
@RestController
@RequestMapping("/api-local/documento-sucursal/dato-zona/v1")
public class DatoZonaApi {

    @Autowired
    private DocumentoSucursalDatosZonaDAOImpl datosZonaDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene datoZonas", notes = "Obtiene los datos de una zona", nickname = "obtieneDatoZona")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idDatoZona}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DatoZonaBase obtieneTipoInformacion(@ApiParam(name = "idDatoZona", value = "Identificador del dato Zona", example = "1", required = true) @PathVariable("idDatoZona") Integer idDatoZona) throws CustomException, DataNotFoundException {
        DatoZonaBase datoZonaBase = datosZonaDAOImpl.selectRow(idDatoZona);
        if (datoZonaBase == null) {
            throw new DataNotFoundException();
        }
        return datoZonaBase;
    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene datosZona", notes = "Obtiene Datos de la zona", nickname = "obtieneDatosZona")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DatosZona obtieneTiposInformacion() throws CustomException, DataNotFoundException {
        DatosZona datosZona = new DatosZona(datosZonaDAOImpl.selectAllRows());
        if (datosZona.getDatosDatozona() != null && datosZona.getDatosDatozona().isEmpty()) {
            throw new DataNotFoundException();
        }
        return datosZona;
    }

    /**
     *
     * @param parametrosDatoZona
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear datoZona", notes = "Agrega un datoZona", nickname = "datoZona")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaDatoZona creaTipoInformacion(@ApiParam(name = "parametrosDatoZona", value = "Paramentros para el alta del tipoInformacion", required = true) @RequestBody ParametrosDatoZona parametrosDatoZona) throws DataNotInsertedException {
        return datosZonaDAOImpl.insertRow(parametrosDatoZona);
    }

    /**
     *
     * @param parametrosDatoZona
     * @param idTipoInformacion
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar datoZona", notes = "Actualiza un datoZona", nickname = "actualizaDatoZona")
    @RequestMapping(value = "/{idDatoZona}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaTipoInformacion(@ApiParam(name = "parametrosDatoZona", value = "Paramentros para la actualización del tipoInformacion", required = true) @RequestBody ParametrosDatoZona parametrosDatoZona, @ApiParam(name = "idDatoZona", value = "Identificador del dato zona", example = "1", required = true) @PathVariable("idDatoZona") Integer idDatoZona) throws DataNotUpdatedException {
        parametrosDatoZona.setIdDatoZona(idDatoZona);
        datosZonaDAOImpl.updateRow(parametrosDatoZona);
        return new SinResultado();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar datoZona", notes = "Elimina un item de los tiposInformacion", nickname = "eliminaDatoZona")
    @RequestMapping(value = "/{idDatoZona}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaTipoInformacion(@ApiParam(name = "idDatoZona", value = "Identificador del DatoZona", example = "1", required = true) @PathVariable("idDatoZona") Integer idDatoZona) throws DataNotDeletedException {
        datosZonaDAOImpl.deleteRow(idDatoZona);
        return new SinResultado();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ObtieneValidacion", notes = "Obtiene validacion para Ceco Formato  proyecto y documento", nickname = "ObtieneValidacion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })

    @RequestMapping(value = "/validaDatos/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ValidaCecoFormatoProyecto> obtieneValidacion(@ApiParam(name = "validaCecoFormatoProyecto", value = "Paramentros para la consulta ", required = true) @RequestBody ValidaCecoFormatoProyecto validaCecoFormatoProyecto) throws CustomException, DataNotFoundException {
        return (List<ValidaCecoFormatoProyecto>) datosZonaDAOImpl.selectRowValida(validaCecoFormatoProyecto);

    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ObtieneZonasDocumento", notes = "Obtiene validacion para Ceco Formato  proyecto y documento", nickname = "ObtieneZonasDocumento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })

    @RequestMapping(value = "/zona/documentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ZonasPorDocumento> obtieneZonasDocumento(@ApiParam(name = "zonasPorDocumento", value = "Paramentros para la consulta ", required = true) @RequestBody ZonasPorDocumento zonasPorDocumento) throws CustomException, DataNotFoundException {
        return (List<ZonasPorDocumento>) datosZonaDAOImpl.selectRowZonasDocumento(zonasPorDocumento);

    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ObtieneZonasPorVersion", notes = "Obtiene validacion para ObtieneZonasPorVersion", nickname = "ObtieneZonasPorVersion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })

    @RequestMapping(value = "/zona/version", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ZonasPorVersion> obtieneZonasVersion(@ApiParam(name = "zonasPorVersion", value = "Paramentros para la consulta ObtieneZonasPorVersion", required = true) @RequestBody ZonasPorVersion zonasPorVersion) throws CustomException, DataNotFoundException {
        return (List<ZonasPorVersion>) datosZonaDAOImpl.selectRowZonasVersion(zonasPorVersion);

    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene ObtieneZonasPorCeco", notes = "Obtiene validacion para ObtieneZonasPorCeco", nickname = "ObtieneZonasPorCeco")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })

    @RequestMapping(value = "/zona/ceco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ZonasPorCeco> obtieneZonasCeco(@ApiParam(name = "zonasPorCeco", value = "Paramentros para la consulta ObtieneZonasPorCeco", required = true) @RequestBody ZonasPorCeco zonasPorCeco) throws CustomException, DataNotFoundException {
        return (List<ZonasPorCeco>) datosZonaDAOImpl.selectRowZonasCeco(zonasPorCeco);
    }

    @RequestMapping(value = "/protoyecto/ceco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArchivoUltimos> selectRowProyectoUltimo(@ApiParam(name = "archivoUltimos", value = "Paramentros para traer ultimo proyecto", required = true) @RequestBody ArchivoUltimos archivoUltimos) throws CustomException, DataNotFoundException {
        return (List<ArchivoUltimos>) datosZonaDAOImpl.selectRowProyectoUltimo(archivoUltimos);
    }

}
