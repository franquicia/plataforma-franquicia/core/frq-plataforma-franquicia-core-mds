package com.gs.baz.frq.dashboard.mprs.graficos.g003;

import com.gs.baz.frq.dashboard.dto.graficos.g003.DefectoPorMillonDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DefectoPorMillonRowMapper implements RowMapper<DefectoPorMillonDTO> {

    private DefectoPorMillonDTO defectoPorMillonDTO;

    @Override
    public DefectoPorMillonDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        defectoPorMillonDTO = new DefectoPorMillonDTO();
        defectoPorMillonDTO.setDefectoPorMillon((BigDecimal) rs.getObject("DEFECTOSXMILLON"));
        defectoPorMillonDTO.setIdDisciplina((BigDecimal) rs.getObject("FIDISCIPLINA_ID"));
        defectoPorMillonDTO.setIdRango((BigDecimal) rs.getObject("FIRANGO_ID"));
        defectoPorMillonDTO.setSigma(new BigDecimal(rs.getString("SIGMA")));
        return defectoPorMillonDTO;
    }
}
