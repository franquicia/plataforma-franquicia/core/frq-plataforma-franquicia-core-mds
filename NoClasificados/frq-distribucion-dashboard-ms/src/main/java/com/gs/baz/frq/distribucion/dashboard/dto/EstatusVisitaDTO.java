/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.frq.model.commons.Specifications;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Respuesta de Estatus Visitas", value = "DatosSalida")
public class EstatusVisitaDTO {

    @JsonProperty("centroCostos")
    @ApiModelProperty(notes = "Centro de costos. `Descifre el valor de éste campo con la llave privada (accesoPrivado) con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "237425")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String centroCostos;

    @JsonProperty("centroCostosBase")
    @ApiModelProperty(notes = "Centro de costos Base. `Descifre el valor de éste campo con la llave privada (accesoPrivado) con cifrado RSA/ECB/PKCS1Padding encoding UTF-8`", example = "237425")
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String centroCostosBase;

    @JsonProperty(value = "fecha", required = true)
    @ApiModelProperty(notes = "Fecha de consulta", example = "02/02/2021", required = true)
    private String fecha;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "El número de empleado que levantó el folio.`Cifre el valor de éste campo con la llave pública (accesoPublico) con cifrado RSA/ECB/PKCS1Padding encoding UTF-8` ", example = "202622", required = true)
    @Specifications(typeOfString = Specifications.TypeOfString.Base64, isSensible = true)
    private String numeroEmpleado;

    @JsonProperty(value = "idInfo", required = true)
    @ApiModelProperty(notes = "Identificador del ", example = "13", required = true)
    private Integer idInfo;

    @JsonProperty(value = "idProtocolo", required = true)
    @ApiModelProperty(notes = "Identificador del protocolo", example = "13", required = true)
    private Integer idProtocolo;

    @JsonProperty(value = "imperdonable", required = true)
    @ApiModelProperty(notes = "", example = "", required = true)
    private String imperdonable;

    @JsonProperty(value = "numeroImperdonables", required = true)
    @ApiModelProperty(notes = "Cantidad de imperdonables.", example = "13", required = true)
    private Integer numeroImperdonables;

    @JsonProperty(value = "defectos", required = true)
    @ApiModelProperty(notes = "Defectos.", example = "", required = true)
    private Integer defectos;

    @JsonProperty(value = "respuestas", required = true)
    @ApiModelProperty(notes = "Respuestas.", example = "", required = true)
    private String respuestas;

    @JsonProperty(value = "totalRespuestas", required = true)
    @ApiModelProperty(notes = "Total de Respuestas.", example = "22", required = true)
    private Integer totalRespuestas;

    @JsonProperty(value = "calificacion", required = true)
    @ApiModelProperty(notes = "Calificación .", example = "8", required = true)
    private Integer calificacion;

    @JsonProperty(value = "calificacionFinal", required = true)
    @ApiModelProperty(notes = "Calificación final.", example = "8", required = true)
    private Integer calificacionFinal;

    @JsonProperty(value = "estatusCalificacion", required = true)
    @ApiModelProperty(notes = "Estatus de la calificación.", example = "1", required = true)
    private String estatusCalificacion;

    @JsonProperty(value = "estatusRespuestas", required = true)
    @ApiModelProperty(notes = "Estatus de las respuestas.", example = "", required = true)
    private String estatusRespuestas;

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getCentroCostosBase() {
        return centroCostosBase;
    }

    public void setCentroCostosBase(String centroCostosBase) {
        this.centroCostosBase = centroCostosBase;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(String numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdInfo() {
        return idInfo;
    }

    public void setIdInfo(Integer idInfo) {
        this.idInfo = idInfo;
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(Integer idProtocolo) {
        this.idProtocolo = idProtocolo;
    }

    public String getImperdonable() {
        return imperdonable;
    }

    public void setImperdonable(String imperdonable) {
        this.imperdonable = imperdonable;
    }

    public Integer getNumeroImperdonables() {
        return numeroImperdonables;
    }

    public void setNumeroImperdonables(Integer numeroImperdonables) {
        this.numeroImperdonables = numeroImperdonables;
    }

    public Integer getDefectos() {
        return defectos;
    }

    public void setDefectos(Integer defectos) {
        this.defectos = defectos;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public Integer getTotalRespuestas() {
        return totalRespuestas;
    }

    public void setTotalRespuestas(Integer totalRespuestas) {
        this.totalRespuestas = totalRespuestas;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getCalificacionFinal() {
        return calificacionFinal;
    }

    public void setCalificacionFinal(Integer calificacionFinal) {
        this.calificacionFinal = calificacionFinal;
    }

    public String getEstatusCalificacion() {
        return estatusCalificacion;
    }

    public void setEstatusCalificacion(String estatusCalificacion) {
        this.estatusCalificacion = estatusCalificacion;
    }

    public String getEstatusRespuestas() {
        return estatusRespuestas;
    }

    public void setEstatusRespuestas(String estatusRespuestas) {
        this.estatusRespuestas = estatusRespuestas;
    }

    @Override
    public String toString() {
        return "EstatusVisitaDTO{" + "centroCostos=" + centroCostos + ", centroCostosBase=" + centroCostosBase + ", fecha=" + fecha + ", numeroEmpleado=" + numeroEmpleado + ", idInfo=" + idInfo + ", idProtocolo=" + idProtocolo + ", imperdonable=" + imperdonable + ", numeroImperdonables=" + numeroImperdonables + ", defectos=" + defectos + ", respuestas=" + respuestas + ", totalRespuestas=" + totalRespuestas + ", calificacion=" + calificacion + ", calificacionFinal=" + calificacionFinal + ", estatusCalificacion=" + estatusCalificacion + ", estatusRespuestas=" + estatusRespuestas + '}';
    }

}
