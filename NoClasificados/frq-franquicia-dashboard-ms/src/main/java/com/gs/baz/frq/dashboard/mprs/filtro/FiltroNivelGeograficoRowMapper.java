package com.gs.baz.frq.dashboard.mprs.filtro;

import com.gs.baz.frq.dashboard.dto.filtros.FiltroNivelGeograficoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FiltroNivelGeograficoRowMapper implements RowMapper<FiltroNivelGeograficoDTO> {

    private FiltroNivelGeograficoDTO nivelGeograficoDTO;

    @Override
    public FiltroNivelGeograficoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        nivelGeograficoDTO = new FiltroNivelGeograficoDTO();
        nivelGeograficoDTO.setNumero((BigDecimal) rs.getObject("IDGENERAL"));
        nivelGeograficoDTO.setDescripcion(rs.getString("DESCRIPCION"));
        return nivelGeograficoDTO;
    }
}
