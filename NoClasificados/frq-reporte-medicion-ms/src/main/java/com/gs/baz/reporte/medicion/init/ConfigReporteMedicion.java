package com.gs.baz.reporte.medicion.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.model.bucket.client.services.init.ConfigBucketServices;
import com.gs.baz.reporte.medicion.dao.NegocioProtocoloDAOImpl;
import com.gs.baz.reporte.medicion.dao.ProtocoloDisciplinaDAOImpl;
import com.gs.baz.reporte.medicion.dao.ReporteMedicion7sDAO;
import com.gs.baz.reporte.medicion.dao.ReporteMedicionDAO;
import com.gs.baz.reporte.medicion.dao.VersionChecklistDAOImpl;
import com.gs.baz.reporte.medicion.dao.bi.ReporteIntermedia7SBI;
import com.gs.baz.reporte.medicion.dao.bi.ReporteIntermediaBI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Configuration
@Import(ConfigBucketServices.class)
@ComponentScan("com.gs.baz.reporte.medicion")
public class ConfigReporteMedicion {

    private final Logger logger = LogManager.getLogger();

    public ConfigReporteMedicion() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(name = "reporteMedicionDAO", initMethod = "init")
    public ReporteMedicionDAO reporteMedicionDAO() {
        return new ReporteMedicionDAO();
    }

    @Bean(name = "reporteMedicion7sDAO", initMethod = "init")
    public ReporteMedicion7sDAO reporteMedicion7sDAO() {
        return new ReporteMedicion7sDAO();
    }

    @Bean(name = "negocioProtocoloDAOImpl", initMethod = "init")
    public NegocioProtocoloDAOImpl negocioProtocoloDAOImpl() {
        return new NegocioProtocoloDAOImpl();
    }

    @Bean(name = "reporteMedicionVersionChecklistDAOImpl", initMethod = "init")
    public VersionChecklistDAOImpl reporteMedicionVersionChecklistDAOImpl() {
        return new VersionChecklistDAOImpl();
    }

    @Bean(name = "reporteMedicionProtocoloDisciplinaDAOImpl", initMethod = "init")
    public ProtocoloDisciplinaDAOImpl protocoloDisciplinaDAOImpl() {
        return new ProtocoloDisciplinaDAOImpl();
    }

    @Bean(name = "reporteIntermediaBI", initMethod = "init")
    public ReporteIntermediaBI reporteIntermediaBI() throws Exception {
        return new ReporteIntermediaBI();
    }

    @Bean(name = "reporteIntermedia7SBI", initMethod = "init")
    public ReporteIntermedia7SBI reporteIntermedia7SBI() throws Exception {
        return new ReporteIntermedia7SBI();
    }

}
