package com.gs.baz.formato.mprs;

import com.gs.baz.formato.dto.FormatoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FormatoRowMapper implements RowMapper<FormatoDTO> {

    private FormatoDTO formato;

    @Override
    public FormatoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        formato = new FormatoDTO();
        formato.setIdFormato(((BigDecimal) rs.getObject("FIIDFORMATO")));
        formato.setDescripcion(rs.getString("FCDESCRIPCION"));
        formato.setIdTipoFormato((BigDecimal) rs.getObject("FIIDTIFORM"));
        return formato;
    }
}
