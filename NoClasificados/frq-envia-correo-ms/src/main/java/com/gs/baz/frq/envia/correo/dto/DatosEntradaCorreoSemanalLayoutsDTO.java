package com.gs.baz.frq.envia.correo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

public class DatosEntradaCorreoSemanalLayoutsDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "asunto")
    private String asunto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "contactosCopiados")
    private ArrayList<String> contactosCopiados;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "destinatario")
    private String destinatario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaFinalReporte")
    private String fechaFinalReporte;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "fechaInicialReporte")
    private String fechaInicialReporte;

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public ArrayList<String> getContactosCopiados() {
        return contactosCopiados;
    }

    public void setContactosCopiados(ArrayList<String> contactosCopiados) {
        this.contactosCopiados = contactosCopiados;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getFechaFinalReporte() {
        return fechaFinalReporte;
    }

    public void setFechaFinalReporte(String fechaFinalReporte) {
        this.fechaFinalReporte = fechaFinalReporte;
    }

    public String getFechaInicialReporte() {
        return fechaInicialReporte;
    }

    public void setFechaInicialReporte(String fechaInicialReporte) {
        this.fechaInicialReporte = fechaInicialReporte;
    }

}
