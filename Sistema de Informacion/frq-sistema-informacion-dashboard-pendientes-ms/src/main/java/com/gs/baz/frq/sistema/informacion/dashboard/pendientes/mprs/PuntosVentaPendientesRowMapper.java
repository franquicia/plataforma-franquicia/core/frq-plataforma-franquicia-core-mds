/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.pendientes.mprs;

import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.PuntoVentaPendiente;
import com.gs.baz.frq.sistema.informacion.dashboard.pendientes.dto.VisitaPendiente;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PuntosVentaPendientesRowMapper implements RowMapper<PuntoVentaPendiente> {

    @Override
    public PuntoVentaPendiente mapRow(ResultSet rs, int rowNum) throws SQLException {
        PuntoVentaPendiente puntoVentaPendiente = new PuntoVentaPendiente();
        puntoVentaPendiente.setCentroCostos(rs.getString("FCID_CECO"));
        puntoVentaPendiente.setNombreCentroCostos(rs.getString("FCNOMBRE_CECO"));
        puntoVentaPendiente.setNumeroEmpleadoCertificador(rs.getInt("FIID_USUARIO_CERTIFICADOR"));
        puntoVentaPendiente.setNombreGerente(rs.getString("FCNOMBRE_GERENTE"));
        puntoVentaPendiente.setFecha(rs.getString("FDFECHA"));
        puntoVentaPendiente.setOrdenGrupo(rs.getInt("FIORDEN_GRUPO"));
        puntoVentaPendiente.setTotalPendientes(rs.getInt("TOTAL_PENDIENTES"));
        return puntoVentaPendiente;
    }
}
