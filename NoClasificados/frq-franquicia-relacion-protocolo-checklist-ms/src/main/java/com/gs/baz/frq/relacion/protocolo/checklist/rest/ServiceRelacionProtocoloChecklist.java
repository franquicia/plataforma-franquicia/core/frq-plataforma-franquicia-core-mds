/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.relacion.protocolo.checklist.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.relacion.protocolo.checklist.dao.RelacionProtocoloChecklistDAOImpl;
import com.gs.baz.frq.relacion.protocolo.checklist.dto.RelacionProtocoloChecklistDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/protocolo/checklist")
public class ServiceRelacionProtocoloChecklist {

    @Autowired
    private RelacionProtocoloChecklistDAOImpl relacionProtocoloChecklistDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RelacionProtocoloChecklistDTO> getRelacionProtocoloChecklist() throws CustomException {
        return relacionProtocoloChecklistDAOImpl.selectRows();
    }

    @RequestMapping(value = "secure/get/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RelacionProtocoloChecklistDTO> getRespuestaReporteMedicion(@RequestParam(value = "idProtocolo", required = false) Long idProtocolo, @RequestParam(value = "idChecklist", required = false) Long idChecklist) throws CustomException {
        return relacionProtocoloChecklistDAOImpl.selectRow(idProtocolo, idChecklist);
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RelacionProtocoloChecklistDTO postRelacionProtocoloChecklist(@RequestBody RelacionProtocoloChecklistDTO relacionProtocoloChecklist) throws CustomException {
        if (relacionProtocoloChecklist.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (relacionProtocoloChecklist.getIdChecklist() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_checklist"));
        }
        if (relacionProtocoloChecklist.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicio"));
        }
        if (relacionProtocoloChecklist.getVigente() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("vigente"));
        }

        return relacionProtocoloChecklistDAOImpl.insertRow(relacionProtocoloChecklist);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public RelacionProtocoloChecklistDTO putRelacionProtocoloChecklist(@RequestBody RelacionProtocoloChecklistDTO relacionProtocoloChecklist) throws CustomException {
        if (relacionProtocoloChecklist.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (relacionProtocoloChecklist.getIdChecklist() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_checklist"));
        }
        if (relacionProtocoloChecklist.getFechaInicio() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("fechaInicio"));
        }
        if (relacionProtocoloChecklist.getVigente() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("vigente"));
        }

        return relacionProtocoloChecklistDAOImpl.updateRow(relacionProtocoloChecklist);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RelacionProtocoloChecklistDTO deleteRelacionProtocoloChecklist(@RequestBody RelacionProtocoloChecklistDTO relacionProtocoloChecklist) throws CustomException {
        if (relacionProtocoloChecklist.getIdProtocolo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_protocolo"));
        }
        if (relacionProtocoloChecklist.getIdChecklist() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_checklist"));
        }

        return relacionProtocoloChecklistDAOImpl.deleteRow(relacionProtocoloChecklist);
    }
}
