/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.reportes.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.CalificacionGeografia;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.CalificacionGeografiaRedUnica;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.CalificacionPromedio;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Disciplina;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Imperdonable;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.ImperdonableTotal;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Incumplimiento;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.IncumplimientoNegocioRedUnica;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.IncumplimientoSeccionResponsableRedUnica;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.Negocio;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NegocioDisciplina;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NegocioIncumplimiento;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NivelCumplimiento;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NivelCumplimientoTotal;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.NivelGeografico;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.ParametrosReporte;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.PorcentajePuntoVentaAprobado;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.PuntoVentaAprobadoGeografia;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.PuntoVentaAprobadoGeografiaRedUnica;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.ResponsableIncumplimiento;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.dto.SeccionIncumplimiento;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.CalificacionesGeografiaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.CalificacionesPromedioRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.DisciplinasRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.ImperdonablesRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.IncumplimientosNegociosRedUnicaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.IncumplimientosRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.IncumplimientosSeccionesResponsablesRedUnicaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.NegociosDisciplinaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.NegociosIncumplimientoRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.NegociosRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.NivelesCumplimientoRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.NivelesGeograficosRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.PorcentajePuntosVentaAprobadosRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.PuntosVentaAprobadosGeografiaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.ResponsablesIncumplimientoRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.reportes.mprs.SeccionesIncumplimientoRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionDashboardReportesDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectFiltroNegocios;
    private DefaultJdbcCall jdbcSelectFiltroNivelGeografico;
    private DefaultJdbcCall jdbcSelectDisciplinas;
    private DefaultJdbcCall jdbcSelectCalificacionPromedioGenerico;
    private DefaultJdbcCall jdbcSelectDisciplinasNegocio;
    private DefaultJdbcCall jdbcSelectPuntosVentaAprobados;
    private DefaultJdbcCall jdbcSelectNivelesCumplimiento;
    private DefaultJdbcCall jdbcCalificacionesGeografiaRedUnica;
    private DefaultJdbcCall jdbcCalificacionesGeografia;
    private DefaultJdbcCall jdbcImperdonables;
    private DefaultJdbcCall jdbcIncumplimientosNegociosRedUnica;
    private DefaultJdbcCall jdbcIncumplimientos;
    private DefaultJdbcCall jdbcIncumplimientosSeccionesResponsablesRedUnica;
    private DefaultJdbcCall jdbcNegociosIncumplimiento;
    private DefaultJdbcCall jdbcPuntosVentaAprobadosGeografiaRedUnica;
    private DefaultJdbcCall jdbcPuntosVentaAprobadosGeografia;
    private DefaultJdbcCall jdbcResponsablesIncumplimiento;
    private DefaultJdbcCall jdbcSeccionesIncumplimiento;
    private final String schema = "FRANQUICIA";

    public void init() {
        jdbcSelectDisciplinas = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDisciplinas.withSchemaName(schema);
        jdbcSelectDisciplinas.withCatalogName("PADASHB40GRAFICAS");
        jdbcSelectDisciplinas.withProcedureName("SPGETGRA01DISCXPERIODO");
        jdbcSelectDisciplinas.returningResultSet("PA_CDATOS", new DisciplinasRowMapper());

        jdbcSelectDisciplinasNegocio = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectDisciplinasNegocio.withSchemaName(schema);
        jdbcSelectDisciplinasNegocio.withCatalogName("PADASHB40GRAFICAS");
        jdbcSelectDisciplinasNegocio.withProcedureName("SPGETGRA01DISCXNEGOCIO");
        jdbcSelectDisciplinasNegocio.returningResultSet("PA_CDATOS", new NegociosDisciplinaRowMapper());

        jdbcSelectFiltroNegocios = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFiltroNegocios.withSchemaName(schema);
        jdbcSelectFiltroNegocios.withCatalogName("PADASHB40FILTROS");
        jdbcSelectFiltroNegocios.withProcedureName("SPGETNEGOCIOXDISC");
        jdbcSelectFiltroNegocios.returningResultSet("PA_CDATOS", new NegociosRowMapper());

        jdbcSelectFiltroNivelGeografico = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectFiltroNivelGeografico.withSchemaName(schema);
        jdbcSelectFiltroNivelGeografico.withCatalogName("PADASHB40FILTROS");
        jdbcSelectFiltroNivelGeografico.withProcedureName("SPGETGEOGRAFIA");
        jdbcSelectFiltroNivelGeografico.returningResultSet("PA_CDATOS", new NivelesGeograficosRowMapper());

        jdbcSelectCalificacionPromedioGenerico = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectCalificacionPromedioGenerico.withSchemaName(schema);
        jdbcSelectCalificacionPromedioGenerico.withCatalogName("PADASHB40GRAFICAS");
        jdbcSelectCalificacionPromedioGenerico.withProcedureName("SPGETGRA03PROMCALIF");
        jdbcSelectCalificacionPromedioGenerico.returningResultSet("PA_CDATOS", new CalificacionesPromedioRowMapper());

        jdbcSelectPuntosVentaAprobados = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectPuntosVentaAprobados.withSchemaName(schema);
        jdbcSelectPuntosVentaAprobados.withCatalogName("PADASHB40GRAFICAS");
        jdbcSelectPuntosVentaAprobados.withProcedureName("SPGETGRA04PDVAPROB");
        jdbcSelectPuntosVentaAprobados.returningResultSet("PA_CDATOS", new PorcentajePuntosVentaAprobadosRowMapper());

        jdbcSelectNivelesCumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectNivelesCumplimiento.withSchemaName(schema);
        jdbcSelectNivelesCumplimiento.withCatalogName("PADASHB40GRAFICAS");
        jdbcSelectNivelesCumplimiento.withProcedureName("SPGETGRA05NIVELCUMPL");
        jdbcSelectNivelesCumplimiento.returningResultSet("PA_CDATOS", new NivelesCumplimientoRowMapper());

        jdbcCalificacionesGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcCalificacionesGeografia.withSchemaName(schema);
        jdbcCalificacionesGeografia.withCatalogName("PADASHB40GRAFIC3");
        jdbcCalificacionesGeografia.withProcedureName("SPGETGRA06CALXGEO");
        jdbcCalificacionesGeografia.returningResultSet("PA_CDATOS", new CalificacionesGeografiaRowMapper());

        jdbcPuntosVentaAprobadosGeografia = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcPuntosVentaAprobadosGeografia.withSchemaName(schema);
        jdbcPuntosVentaAprobadosGeografia.withCatalogName("PADASHB40GRAFIC3");
        jdbcPuntosVentaAprobadosGeografia.withProcedureName("SPGETGRA07PDVAPROBXGEO");
        jdbcPuntosVentaAprobadosGeografia.returningResultSet("PA_CDATOS", new PuntosVentaAprobadosGeografiaRowMapper());

        jdbcImperdonables = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcImperdonables.withSchemaName(schema);
        jdbcImperdonables.withCatalogName("PADASHB40GRAFIC3");
        jdbcImperdonables.withProcedureName("SPGETGRA11topinperdonable");
        jdbcImperdonables.returningResultSet("PA_CDATOS", new ImperdonablesRowMapper());

        jdbcIncumplimientos = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcIncumplimientos.withSchemaName(schema);
        jdbcIncumplimientos.withCatalogName("PADASHB40GRAFIC2");
        jdbcIncumplimientos.withProcedureName("SPGETGRA10topincump");
        jdbcIncumplimientos.returningResultSet("PA_CDATOS", new IncumplimientosRowMapper());

        jdbcCalificacionesGeografiaRedUnica = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcCalificacionesGeografiaRedUnica.withSchemaName(schema);
        jdbcCalificacionesGeografiaRedUnica.withCatalogName("PADASHB40GRAFIC3");
        jdbcCalificacionesGeografiaRedUnica.withProcedureName("SPGETGRA06CALXGEO");
        jdbcCalificacionesGeografiaRedUnica.returningResultSet("PA_CDATOS", new CalificacionesGeografiaRowMapper());

        jdbcPuntosVentaAprobadosGeografiaRedUnica = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcPuntosVentaAprobadosGeografiaRedUnica.withSchemaName(schema);
        jdbcPuntosVentaAprobadosGeografiaRedUnica.withCatalogName("PADASHB40GRAFIC3");
        jdbcPuntosVentaAprobadosGeografiaRedUnica.withProcedureName("SPGETGRA07PDVAPROBXGEO");
        jdbcPuntosVentaAprobadosGeografiaRedUnica.returningResultSet("PA_CDATOS", new PuntosVentaAprobadosGeografiaRowMapper());

        jdbcIncumplimientosNegociosRedUnica = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcIncumplimientosNegociosRedUnica.withSchemaName(schema);
        jdbcIncumplimientosNegociosRedUnica.withCatalogName("PADASHB40GRAFINC");
        jdbcIncumplimientosNegociosRedUnica.withProcedureName("SPGETGRA08INCXNEGOCIO");
        jdbcIncumplimientosNegociosRedUnica.returningResultSet("PA_CDATOS", new IncumplimientosNegociosRedUnicaRowMapper());

        jdbcIncumplimientosSeccionesResponsablesRedUnica = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcIncumplimientosSeccionesResponsablesRedUnica.withSchemaName(schema);
        jdbcIncumplimientosSeccionesResponsablesRedUnica.withCatalogName("PADASHB40GRAFINC");
        jdbcIncumplimientosSeccionesResponsablesRedUnica.withProcedureName("SPGETGRA09INCXDISC");
        jdbcIncumplimientosSeccionesResponsablesRedUnica.returningResultSet("PA_CDATOS", new IncumplimientosSeccionesResponsablesRedUnicaRowMapper());

        jdbcNegociosIncumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcNegociosIncumplimiento.withSchemaName(schema);
        jdbcNegociosIncumplimiento.withCatalogName("PADASHB40GRAFIC2");
        jdbcNegociosIncumplimiento.withProcedureName("SPGETGRA08INCNEGOCIOS");
        jdbcNegociosIncumplimiento.returningResultSet("PA_CDATOS", new NegociosIncumplimientoRowMapper());

        jdbcResponsablesIncumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcResponsablesIncumplimiento.withSchemaName(schema);
        jdbcResponsablesIncumplimiento.withCatalogName("PADASHB40GRAFIC2");
        jdbcResponsablesIncumplimiento.withProcedureName("SPGETGRA09RESPONSABLEXDISC");
        jdbcResponsablesIncumplimiento.returningResultSet("PA_CDATOS", new ResponsablesIncumplimientoRowMapper());

        jdbcSeccionesIncumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSeccionesIncumplimiento.withSchemaName(schema);
        jdbcSeccionesIncumplimiento.withCatalogName("PADASHB40GRAFIC2");
        jdbcSeccionesIncumplimiento.withProcedureName("SPGETGRA09MODULOSXDISC");
        jdbcSeccionesIncumplimiento.returningResultSet("PA_CDATOS", new SeccionesIncumplimientoRowMapper());

    }

    public List<Disciplina> selectDisciplinas() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        Map<String, Object> out = jdbcSelectDisciplinas.execute(mapSqlParameterSource);
        return (List<Disciplina>) out.get("PA_CDATOS");
    }

    public List<NegocioDisciplina> selectNegociosDisciplina(Integer idDisciplina) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", idDisciplina);
        Map<String, Object> out = jdbcSelectDisciplinasNegocio.execute(mapSqlParameterSource);
        return (List<NegocioDisciplina>) out.get("PA_CDATOS");
    }

    public List<Negocio> selectNegocios(Integer idDisciplina) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", idDisciplina);
        Map<String, Object> out = jdbcSelectFiltroNegocios.execute(mapSqlParameterSource);
        return (List<Negocio>) out.get("PA_CDATOS");
    }

    public List<NivelGeografico> selectNivelesGeograficos(ParametrosReporte parametrosReporte) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        Map<String, Object> out = jdbcSelectFiltroNivelGeografico.execute(mapSqlParameterSource);
        return (List<NivelGeografico>) out.get("PA_CDATOS");
    }

    public List<CalificacionPromedio> selectCalificacionPromedioGenerico(ParametrosReporte parametrosReporte) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcSelectCalificacionPromedioGenerico.execute(mapSqlParameterSource);
        return (List<CalificacionPromedio>) out.get("PA_CDATOS");
    }

    public List<PorcentajePuntoVentaAprobado> selectPorcentajePuntosVentaAprobados(ParametrosReporte parametrosReporte) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcSelectPuntosVentaAprobados.execute(mapSqlParameterSource);
        return (List<PorcentajePuntoVentaAprobado>) out.get("PA_CDATOS");
    }

    public NivelCumplimientoTotal selectNivelesCumplimiento(ParametrosReporte parametrosReporte) throws CustomException {
        NivelCumplimientoTotal nivelCumplimientoTotal = new NivelCumplimientoTotal();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FISEMAFORO", parametrosReporte.getIdSemaforo());
        Map<String, Object> out = jdbcSelectNivelesCumplimiento.execute(mapSqlParameterSource);
        nivelCumplimientoTotal.setPuntosVenta(((BigDecimal) out.get("PA_CTOTAL")).intValue());
        nivelCumplimientoTotal.setNivelesCumplimiento((List<NivelCumplimiento>) out.get("PA_CDATOS"));
        return nivelCumplimientoTotal;
    }

    public List<CalificacionGeografia> selectCalificacionesGeografia(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcCalificacionesGeografia.execute(mapSqlParameterSource);
        return (List<CalificacionGeografia>) out.get("PA_CDATOS");
    }

    public List<PuntoVentaAprobadoGeografia> selectPuntosVentaAprobadosGeografia(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcPuntosVentaAprobadosGeografia.execute(mapSqlParameterSource);
        return (List<PuntoVentaAprobadoGeografia>) out.get("PA_CDATOS");
    }

    public List<Incumplimiento> selectIncumplimientos(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcIncumplimientos.execute(mapSqlParameterSource);
        return (List<Incumplimiento>) out.get("PA_CDATOS");
    }

    public ImperdonableTotal selectImperdonables(ParametrosReporte parametrosReporte) {
        ImperdonableTotal imperdonableTotal = new ImperdonableTotal();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcImperdonables.execute(mapSqlParameterSource);
        imperdonableTotal.setPuntosVenta(((BigDecimal) out.get("PA_CTOTAL")).intValue());
        imperdonableTotal.setImperdonables((List<Imperdonable>) out.get("PA_CDATOS"));
        return imperdonableTotal;
    }

    public CalificacionGeografiaRedUnica selectCalificacionesGeografiaRedUnica(ParametrosReporte parametrosReporte) {
        CalificacionGeografiaRedUnica calificacionGeografiaRedUnica = new CalificacionGeografiaRedUnica();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcCalificacionesGeografiaRedUnica.execute(mapSqlParameterSource);
        calificacionGeografiaRedUnica.setMeta(((BigDecimal) out.get("PA_CMETA")).intValue());
        calificacionGeografiaRedUnica.setCalificacionesGeografia((List<CalificacionGeografia>) out.get("PA_CDATOS"));
        return calificacionGeografiaRedUnica;
    }

    public PuntoVentaAprobadoGeografiaRedUnica selectPuntosVentaAprobadosGeografiaRedUnica(ParametrosReporte parametrosReporte) {
        PuntoVentaAprobadoGeografiaRedUnica puntoVentaAprobadoGeografiaRedUnica = new PuntoVentaAprobadoGeografiaRedUnica();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcPuntosVentaAprobadosGeografiaRedUnica.execute(mapSqlParameterSource);
        puntoVentaAprobadoGeografiaRedUnica.setMeta(((BigDecimal) out.get("PA_CMETA")).intValue());
        puntoVentaAprobadoGeografiaRedUnica.setPuntosVentaAprobadosGeografia((List<PuntoVentaAprobadoGeografia>) out.get("PA_CDATOS"));
        return puntoVentaAprobadoGeografiaRedUnica;
    }

    public List<IncumplimientoNegocioRedUnica> selectIncumplimientosNegocioRedUnica(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        mapSqlParameterSource.addValue("PA_FCNEGOCIOPREG", parametrosReporte.getNegocioPregunta());
        Map<String, Object> out = jdbcIncumplimientosNegociosRedUnica.execute(mapSqlParameterSource);
        return (List<IncumplimientoNegocioRedUnica>) out.get("PA_CDATOS");
    }

    public List<NegocioIncumplimiento> selectNegociosIncumplimientos(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        Map<String, Object> out = jdbcNegociosIncumplimiento.execute(mapSqlParameterSource);
        return (List<NegocioIncumplimiento>) out.get("PA_CDATOS");
    }

    public List<IncumplimientoSeccionResponsableRedUnica> selectIncumplimientosSeccionResponsableRedUnica(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        mapSqlParameterSource.addValue("pa_fcmodulo", parametrosReporte.getModulo());
        mapSqlParameterSource.addValue("pa_FCRESPONSABLE", parametrosReporte.getResponsable());
        Map<String, Object> out = jdbcIncumplimientosSeccionesResponsablesRedUnica.execute(mapSqlParameterSource);
        return (List<IncumplimientoSeccionResponsableRedUnica>) out.get("PA_CDATOS");
    }

    public List<SeccionIncumplimiento> selectSeccionesIncumplimiento(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcSeccionesIncumplimiento.execute(mapSqlParameterSource);
        return (List<SeccionIncumplimiento>) out.get("PA_CDATOS");
    }

    public List<ResponsableIncumplimiento> selectResponsablesIncumplimiento(ParametrosReporte parametrosReporte) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FECHAINI", parametrosReporte.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAFIN", parametrosReporte.getFechaFin());
        mapSqlParameterSource.addValue("PA_FCID_CECO", parametrosReporte.getNivelCentroCostos());
        mapSqlParameterSource.addValue("PA_FIDISCIPLINA_ID", parametrosReporte.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_FINEGOCIODISCID", parametrosReporte.getIdNegocio());
        Map<String, Object> out = jdbcResponsablesIncumplimiento.execute(mapSqlParameterSource);
        return (List<ResponsableIncumplimiento>) out.get("PA_CDATOS");
    }

}
