/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.comentarios.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author carlos
 */
public class ParametrosComentrioFirma {

    @JsonProperty(value = "idComentario")
    private transient Integer idComentario;

    @JsonProperty(value = "idFirma")
    private transient Integer idFirma;

    @JsonProperty(value = "comentario")
    @ApiModelProperty(notes = "Comentario de la firma", example = "El documento tiene faltas de ortografia", required = true)
    @NotEmpty
    private String comentario;

    @JsonProperty(value = "idEstadoFirma")
    @ApiModelProperty(notes = "Identificador del estado de la firma", example = "3", required = true)
    @NotNull
    private Integer idEstadoFirma;

    public Integer getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(Integer idComentario) {
        this.idComentario = idComentario;
    }

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getIdEstadoFirma() {
        return idEstadoFirma;
    }

    public void setIdEstadoFirma(Integer idEstadoFirma) {
        this.idEstadoFirma = idEstadoFirma;
    }

    @Override
    public String toString() {
        return "ParametrosComentrioFirma{" + "idComentario=" + idComentario + ", idFirma=" + idFirma + ", comentario=" + comentario + '}';
    }

}
