/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ColorimetriaDTO {

    @JsonProperty(value = "id_colorimetria")
    private Integer idColorimetria;

    @JsonProperty(value = "tema_web")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String temaWeb;

    @JsonProperty(value = "color_primario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String colorPrim;

    @JsonProperty(value = "color_segundario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String colorSegund;

    @JsonProperty(value = "color_terciario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String colorTerc;

    @JsonProperty(value = "img_icono")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String icono;

    public ColorimetriaDTO() {
    }

    public Integer getIdColorimetria() {
        return idColorimetria;
    }

    public void setIdColorimetria(BigDecimal idColorimetria) {
        this.idColorimetria = (idColorimetria == null ? null : idColorimetria.intValue());
    }

    public String getTemaWeb() {
        return temaWeb;
    }

    public void setTemaWeb(String temaWeb) {
        this.temaWeb = temaWeb;
    }

    public String getColorPrim() {
        return colorPrim;
    }

    public void setColorPrim(String colorPrim) {
        this.colorPrim = colorPrim;
    }

    public String getColorSegund() {
        return colorSegund;
    }

    public void setColorSegund(String colorSegund) {
        this.colorSegund = colorSegund;
    }

    public String getColorTerc() {
        return colorTerc;
    }

    public void setColorTerc(String colorTerc) {
        this.colorTerc = colorTerc;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    @Override
    public String toString() {
        return "ColorimetriaDTO{" + "idColorimetria=" + idColorimetria + ", temaWeb=" + temaWeb + ", colorPrim=" + colorPrim + ", colorSegund=" + colorSegund + ", colorTerc=" + colorTerc + ", icono=" + icono + '}';
    }

}
