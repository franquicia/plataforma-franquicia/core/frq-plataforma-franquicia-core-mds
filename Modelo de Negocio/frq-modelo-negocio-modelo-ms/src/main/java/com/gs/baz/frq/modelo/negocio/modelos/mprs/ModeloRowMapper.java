/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.modelos.mprs;

import com.gs.baz.frq.modelo.negocio.modelos.dto.ModeloBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class ModeloRowMapper implements RowMapper<ModeloBase> {

    private ModeloBase modeloBase;

    @Override
    public ModeloBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        modeloBase = new ModeloBase();
        modeloBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        modeloBase.setAcronimo(rs.getString("FCACRONIMO"));
        return modeloBase;
    }
}
