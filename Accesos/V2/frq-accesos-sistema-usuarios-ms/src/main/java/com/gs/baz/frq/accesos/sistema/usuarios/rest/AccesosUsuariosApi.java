/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.usuarios.rest;

import com.gs.baz.frq.accesos.sistema.usuarios.dao.AccesosUsuariosDAOImpl;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.AltaUsuario;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.AltaUsuarioVistas;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.ParametrosBusquedaUsuario;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.ParametrosUsuario;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.ParametrosUsuarioVistas;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.SinResultado;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.UsuarioBase;
import com.gs.baz.frq.accesos.sistema.usuarios.dto.Usuarios;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "accesos", value = "accesos", description = "Api para la gestión del catalogo de usuarios")
@RestController
@RequestMapping("/api-local/accesos/sistema/usuarios/v1")
public class AccesosUsuariosApi {

    @Autowired
    private AccesosUsuariosDAOImpl accesosUsuariosDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param numeroEmpleado
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuario", notes = "Obtiene un usuario", nickname = "obtieneUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{numeroEmpleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioBase obtieneUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Long numeroEmpleado) throws DataNotFoundException {
        UsuarioBase usuarioBase = accesosUsuariosDAOImpl.selectRow(numeroEmpleado);
        if (usuarioBase == null) {
            throw new DataNotFoundException();
        }
        return usuarioBase;
    }

    /**
     *
     * @param idPerfil
     * @return @throws DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/perfiles/{idPerfil}/fijos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuariosFijosPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable Integer idPerfil) throws DataNotFoundException {
        Usuarios usuarios = new Usuarios(accesosUsuariosDAOImpl.selectAllUsuariosFijosPerfil(idPerfil));
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param idPerfil
     * @return @throws DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/perfiles/{idPerfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuariosPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable Integer idPerfil) throws DataNotFoundException {
        Usuarios usuarios = new Usuarios(accesosUsuariosDAOImpl.selectAllUsuariosPerfil(idPerfil));
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param idPerfil
     * @param parametrosBusquedaUsuario
     * @return @throws DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/perfiles/{idPerfil}/busquedas/disponibles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuariosDisponiblesPerfil(@ApiParam(name = "idPerfil", value = "Identificador del perfil", example = "1", required = true) @PathVariable Integer idPerfil, @ApiParam(name = "ParametrosBusquedaUsuario", value = "Paramentros para la busqueda de usuarios", required = true) @RequestBody ParametrosBusquedaUsuario parametrosBusquedaUsuario) throws DataNotFoundException {
        Usuarios usuarios = new Usuarios(accesosUsuariosDAOImpl.selectAllUsuariosDisponibles(idPerfil, parametrosBusquedaUsuario.getUsuario()));
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param idVista
     * @return @throws DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/vistas/{idVista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuariosVista(@ApiParam(name = "idVista", value = "Identificador del la vista", example = "1", required = true) @PathVariable Integer idVista) throws DataNotFoundException {
        Usuarios usuarios = new Usuarios(accesosUsuariosDAOImpl.selectAllUsuariosVista(idVista));
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param numeroEmpleado
     * @param parametrosUsuarioVistas
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear vistas usuario", notes = "Agrega vistas al usuario", nickname = "creaVistasUsuario")
    @RequestMapping(value = "/{numeroEmpleado}/vistas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaUsuarioVistas creaVistasUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado, @ApiParam(name = "ParametrosUsuarioVistas", value = "Paramentros para el alta de las vistas", required = true) @RequestBody ParametrosUsuarioVistas parametrosUsuarioVistas) throws DataNotInsertedException {
        parametrosUsuarioVistas.setNumeroEmpleado(numeroEmpleado);
        return accesosUsuariosDAOImpl.insertRowUsuarioVistas(parametrosUsuarioVistas);
    }

    /**
     *
     * @param numeroEmpleado
     * @param idVista
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Borra vista usuario", notes = "Borra vista al usuario", nickname = "borraVistaUsuario")
    @RequestMapping(value = "/{numeroEmpleado}/vistas/{idVista}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado borraVistaUsuario(@ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado, @ApiParam(name = "idVista", value = "Identificador del la vista", example = "1", required = true) @PathVariable("idVista") Integer idVista) throws DataNotInsertedException {
        accesosUsuariosDAOImpl.deleteRowUsuarioVista(numeroEmpleado, idVista);
        return new SinResultado();
    }

    /**
     *
     * @return @throws DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuarios", notes = "Obtiene todos los usuarios", nickname = "obtieneUsuarios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuarios() throws DataNotFoundException {
        Usuarios usuarios = new Usuarios(accesosUsuariosDAOImpl.selectAllRows());
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param parametrosBusquedaUsuario
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene usuario", notes = "Obtiene un usuario", nickname = "obtieneUsuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/busqueda", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuarios obtieneUsuariosBusqueda(@ApiParam(name = "ParametrosBusquedaUsuario", value = "Paramentros para la busqueda de usuarios", required = true) @RequestBody ParametrosBusquedaUsuario parametrosBusquedaUsuario) throws DataNotFoundException {
        Usuarios usuarios = new Usuarios(accesosUsuariosDAOImpl.selectBusqueda(parametrosBusquedaUsuario.getUsuario()));
        if (usuarios.getUsuarios() != null && usuarios.getUsuarios().isEmpty()) {
            throw new DataNotFoundException();
        }
        return usuarios;
    }

    /**
     *
     * @param parametrosUsuario
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear usuario", notes = "Agrega un usuario", nickname = "creaUsuario")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaUsuario creaUsuario(@ApiParam(name = "ParametrosUsuario", value = "Paramentros para el alta del usuario", required = true) @RequestBody ParametrosUsuario parametrosUsuario) throws DataNotInsertedException {
        return accesosUsuariosDAOImpl.insertRow(parametrosUsuario);
    }

    /**
     *
     * @param parametrosUsuario
     * @param numeroEmpleado
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar usuario", notes = "Actualiza un usuario", nickname = "actualizaUsuario")
    @RequestMapping(value = "/{numeroEmpleado}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado actualizaUsuario(@ApiParam(name = "ParametrosUsuario", value = "Paramentros para la actualización del usuario", required = true) @RequestBody ParametrosUsuario parametrosUsuario, @ApiParam(name = "numeroEmpleado", value = "Identificador del usuario", example = "1", required = true) @PathVariable("numeroEmpleado") Integer numeroEmpleado) throws DataNotUpdatedException {
        parametrosUsuario.setNumeroEmpleado(numeroEmpleado);
        accesosUsuariosDAOImpl.updateRow(parametrosUsuario);
        return new SinResultado();
    }

}
