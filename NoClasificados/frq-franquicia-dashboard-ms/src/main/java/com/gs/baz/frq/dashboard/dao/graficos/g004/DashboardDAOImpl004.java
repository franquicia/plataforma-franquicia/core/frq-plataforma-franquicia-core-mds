package com.gs.baz.frq.dashboard.dao.graficos.g004;

import com.gs.baz.frq.dashboard.dao.util.PeriodoDAOImpl;
import com.gs.baz.frq.dashboard.dto.filtros.FiltrosDTO;
import com.gs.baz.frq.dashboard.dto.graficos.g004.HistogramaNacionalDTO;
import com.gs.baz.frq.dashboard.dto.util.FechasDTO;
import com.gs.baz.frq.dashboard.mprs.graficos.g004.HistogramaNacionalRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class DashboardDAOImpl004 extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectHistogramaNacional;
    private DefaultJdbcCall jdbcSelectHistogramaNivel;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    @Autowired
    PeriodoDAOImpl periodoDAOImpl;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectHistogramaNacional = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectHistogramaNacional.withSchemaName(schema);
        jdbcSelectHistogramaNacional.withCatalogName("PACONDASHGRAL");
        jdbcSelectHistogramaNacional.withProcedureName("SPHISTNACIONAL");
        jdbcSelectHistogramaNacional.returningResultSet("RCL_INFO", new HistogramaNacionalRowMapper());

        jdbcSelectHistogramaNivel = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectHistogramaNivel.withSchemaName(schema);
        jdbcSelectHistogramaNivel.withCatalogName("PAADMINPUNTOC");
        jdbcSelectHistogramaNivel.withProcedureName("SPFGRAFICA");
        jdbcSelectHistogramaNivel.returningResultSet("RCL_INFO", new HistogramaNacionalRowMapper());
    }

    private List<HistogramaNacionalDTO> selectRowsHistogramaNacional(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_ANIO", filtros.getAnio());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectHistogramaNacional.execute(mapSqlParameterSource);
        return (List<HistogramaNacionalDTO>) out.get("RCL_INFO");

    }

    private List<HistogramaNacionalDTO> selectRowsHistogramaNivel(FiltrosDTO filtros) throws CustomException {
        FechasDTO fechas = periodoDAOImpl.getFechas(filtros);
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_IDDISCI", filtros.getIdDisciplina());
        mapSqlParameterSource.addValue("PA_IDNEG", filtros.getIdNegocio());
        mapSqlParameterSource.addValue("PA_NIVEL", filtros.getNivelGeografico());
        mapSqlParameterSource.addValue("PA_CECO", filtros.getIdCeco());
        mapSqlParameterSource.addValue("PA_FECHAI", fechas.getFechaInicio());
        mapSqlParameterSource.addValue("PA_FECHAF", fechas.getFechaFin());
        Map<String, Object> out = jdbcSelectHistogramaNivel.execute(mapSqlParameterSource);
        return (List<HistogramaNacionalDTO>) out.get("RCL_INFO");
    }

    public List<HistogramaNacionalDTO> selectRowsHistogramaGeneral(HistogramaNacionalDTO entityDTO) throws CustomException {
        FiltrosDTO filtros = entityDTO.getFiltros();
        if (filtros.getNivelGeografico().equals(4)) {
            return this.selectRowsHistogramaNacional(filtros);
        } else {
            return this.selectRowsHistogramaNivel(filtros);
        }
    }

}
