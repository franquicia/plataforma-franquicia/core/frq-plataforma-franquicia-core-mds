/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.autorization.server.apigee.bi;

/**
 *
 * @author cescobarh
 */
public class TokenApiValidate {

    private String token;
    private String usuario;
    private String llavePublica;

    public TokenApiValidate() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getLlavePublica() {
        return llavePublica;
    }

    public void setLlavePublica(String llavePublica) {
        this.llavePublica = llavePublica;
    }

}
