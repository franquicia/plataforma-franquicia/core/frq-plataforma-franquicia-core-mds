package com.gs.baz.frq.limpieza.remedy.mprs;

import com.gs.baz.frq.limpieza.remedy.dto.LogDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class LogRowMapper implements RowMapper<LogDTO> {

    public LogDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        LogDTO logDTO = new LogDTO();

        logDTO.setIdLog(rs.getInt("FILOG_ID"));
        logDTO.setFechaError(rs.getString("FDREGISTRO"));
        logDTO.setCodigoError(rs.getString("FICODIGO_ERROR"));
        logDTO.setMensajeError(rs.getString("FCMENSAJE_ERROR"));
        logDTO.setOrigenError(rs.getString("FCORIGEN_ERROR"));

        return logDTO;
    }
}
