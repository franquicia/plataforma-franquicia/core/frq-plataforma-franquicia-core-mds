package com.gs.baz.frq.limpieza.remedy.bi;

import com.gs.baz.frq.limpieza.remedy.dto.DetalleAprobacionesResponseDTO;
import com.gs.baz.frq.limpieza.remedy.dto.ParametroDTO;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ActualizarIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Aprobacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ArrayOfInformacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Authentication;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarAprobaciones;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarAprobacionesResponse;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.ConsultarIncidentes;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Filtro;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.Incidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsCAprobacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsCIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub.RsUIncidente;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._Accion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._Calificacion;
import com.gs.baz.frq.limpieza.remedy.stub.RemedyStub._TipoCierre;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import javax.activation.DataHandler;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiciosRemedyLimpiezaDosBI {

    @Autowired
    private ParametroBI parametroBi;

    @Autowired
    private PeticionesRemedyBI peticionesRemedyBI;

    @Autowired
    private LogBI logbi;

    private static final Logger LOGGER = LogManager.getLogger();
    private final Base64.Decoder decoder = Base64.getDecoder();
    private final Base64.Encoder encoder = Base64.getEncoder();
    private static final Integer DOS = 2, TRES = 3, CUATRO = 4, CINCO = 5, SEIS = 6, SIETE = 7, OCHO = 8;

    public boolean asignarProveedor(int idIncidente, String proveedor) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("asignarProveedor()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            actIncitenteRequest.setAccion(action.AsignaProveedor);
            actIncitenteRequest.setAuthentication(accesoRemedy);

            Incidente incide = new Incidente();
            incide.setAdjunto1Nombre("");
            incide.setAdjunto2Nombre("");
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion("");

            incide.setProveedor(proveedor);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "AsignarProveedor()");
            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al crear el incidente ", e);

        }

        return result;
    }

    public boolean getPermisoPeticionesBD() {

        boolean respuesta = false;

        try {
            List<ParametroDTO> listaParam = parametroBi.obtieneParametros("ejecutaInsertBD");

            if (listaParam != null && listaParam.size() > 0) {

                ParametroDTO parametro = listaParam.get(0);

                if (parametro != null && parametro.getValor().equals("1")) {
                    respuesta = true;
                } else {
                    respuesta = false;
                }

            }

            return respuesta;

        } catch (Exception e) {
            LOGGER.log(Level.INFO, "FALLO ALGO", e);
            return false;
        }

    }

    public String validaNuloBase64(DataHandler imagen) {

        String respuesta = "";

        try {

            if (imagen != null) {

                InputStream in = imagen.getInputStream();
                byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);

                String encoded = new String(encoder.encode(byteArray), StandardCharsets.UTF_8);
                respuesta = encoded;

            } else {
                respuesta = "";
            }

        } catch (Exception e) {
            respuesta = "";
            LOGGER.log(Level.INFO, "Fallo Algo", e);
        }

        return respuesta;

    }

    public DetalleAprobacionesResponseDTO getInfoAprobaciones(int incidente, int opcion) {

        DetalleAprobacionesResponseDTO respuesta = null;
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("getInfoAprobaciones()" + " LIMPIEZA");
        }

        try {

            if (opcion == 1) {

                AprobacionesRemedyLimpiezaHilos cierreTecnico = new AprobacionesRemedyLimpiezaHilos(incidente, 6);
                cierreTecnico.start();

                while (cierreTecnico.isAlive()) {
                    // Se ejecuta mientras este vivo el hilo
                }

                respuesta = new DetalleAprobacionesResponseDTO();

                if (cierreTecnico.getRespuesta() != null) {
                    respuesta.setJustificacionCierre(cierreTecnico.getRespuesta().getJustificacion());
                    respuesta.setArchivoCierreUno(validaNuloBase64(cierreTecnico.getRespuesta().getA1_Base()));
                    respuesta.setArchivoCierreDos(validaNuloBase64(cierreTecnico.getRespuesta().getA2_Base()));
                    respuesta.setFechaCierre(formato.format(cierreTecnico.getRespuesta().getFechaModificacion().getTime()));
                    respuesta.setJustificacionCierreRechazado("");
                    respuesta.setArchivoCierreRechazado1("");
                    respuesta.setFechaCierreRechazo("0001-01-01 00:00:00");
                    respuesta.setJustificacionCalificacion("");
                }

            } else if (opcion == 2) {

                AprobacionesRemedyLimpiezaHilos cierreTecnico = new AprobacionesRemedyLimpiezaHilos(incidente, SEIS);
                cierreTecnico.start();

                AprobacionesRemedyLimpiezaHilos cierreTecnicoRechazado = new AprobacionesRemedyLimpiezaHilos(incidente,
                        SIETE);
                cierreTecnicoRechazado.start();

                while (cierreTecnico.isAlive() || cierreTecnicoRechazado.isAlive()) {
                    // Se ejecuta mientras este vivov cualquier hilo
                }

                respuesta = new DetalleAprobacionesResponseDTO();

                if (cierreTecnico.getRespuesta() != null) {
                    respuesta.setJustificacionCierre(cierreTecnico.getRespuesta().getJustificacion());
                    respuesta.setArchivoCierreUno(validaNuloBase64(cierreTecnico.getRespuesta().getA1_Base()));
                    respuesta.setArchivoCierreDos(validaNuloBase64(cierreTecnico.getRespuesta().getA2_Base()));
                    respuesta.setFechaCierre(formato.format(cierreTecnico.getRespuesta().getFechaModificacion().getTime()));
                }

                if (cierreTecnicoRechazado.getRespuesta() != null) {
                    respuesta.setJustificacionCierreRechazado(cierreTecnicoRechazado.getRespuesta().getJustificacion());
                    respuesta.setArchivoCierreRechazado1(
                            validaNuloBase64(cierreTecnicoRechazado.getRespuesta().getA1_Base()));
                    respuesta.setFechaCierreRechazo(
                            formato.format(cierreTecnicoRechazado.getRespuesta().getFechaModificacion().getTime()));
                    respuesta.setJustificacionCalificacion("");
                }

            } else if (opcion == 8) {

                AprobacionesRemedyLimpiezaHilos cierreTecnico = new AprobacionesRemedyLimpiezaHilos(incidente, 6);
                cierreTecnico.start();
                AprobacionesRemedyLimpiezaHilos cierreTecnicoAceptado = new AprobacionesRemedyLimpiezaHilos(incidente,
                        8);
                cierreTecnicoAceptado.start();

                while (cierreTecnico.isAlive() || cierreTecnicoAceptado.isAlive()) {
                    // Se ejecuta mientras este vivo el hilo
                }

                respuesta = new DetalleAprobacionesResponseDTO();

                if (cierreTecnico.getRespuesta() != null) {
                    respuesta.setJustificacionCierre(cierreTecnico.getRespuesta().getJustificacion());
                    respuesta.setArchivoCierreUno(validaNuloBase64(cierreTecnico.getRespuesta().getA1_Base()));
                    respuesta.setArchivoCierreDos(validaNuloBase64(cierreTecnico.getRespuesta().getA2_Base()));
                    respuesta.setFechaCierre(formato.format(cierreTecnico.getRespuesta().getFechaModificacion().getTime()));
                    respuesta.setJustificacionCierreRechazado("");
                    respuesta.setArchivoCierreRechazado1("");
                    respuesta.setFechaCierreRechazo("0001-01-01 00:00:00");
                }

                if (cierreTecnicoAceptado.getRespuesta() != null) {
                    respuesta.setJustificacionCalificacion(cierreTecnicoAceptado.getRespuesta().getJustificacion());
                    respuesta.setFechaCalificacion(formato.format(cierreTecnicoAceptado.getRespuesta().getFechaModificacion().getTime()));
                }

            } else {

                respuesta = new DetalleAprobacionesResponseDTO();

                respuesta.setJustificacionCierre("");
                respuesta.setArchivoCierreUno("");
                respuesta.setArchivoCierreDos("");
                respuesta.setFechaCierre("0001-01-01 00:00:00");
                respuesta.setJustificacionCierreRechazado("");
                respuesta.setArchivoCierreRechazado1("");
                respuesta.setFechaCierreRechazo("0001-01-01 00:00:00");
                respuesta.setJustificacionCalificacion("");

            }

        } catch (Exception e) {

            LOGGER.info(e);
            respuesta = null;

        }

        return respuesta;
    }

    public ArrayOfInformacion consultaFoliosSupervidor(Integer numSupervisor, String opcEst) {

        ArrayOfInformacion arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaFoliosSupervidor()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            LOGGER.log(Level.INFO, "Today is ", today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al consultar la bandeja de supervisor ", e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public ArrayOfInformacion consultaFoliosProveedor(String proveedor, String opcEst) {
        ArrayOfInformacion arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaFoliosProveedor()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setProveedor(proveedor);
            org.apache.axis2.databinding.types.UnsignedByte auxB = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcEst);
            nFiltro.setOpcionEstado(auxB);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al adjuntar archivo al incidente ", e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public Aprobacion consultarAprobaciones(int idIncidente, int opcion) {
        Aprobacion result = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultarAprobaciones()" + " LIMPIEZA");
        }

        try {
            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(accesoRemedy);

            org.apache.axis2.databinding.types.UnsignedByte auxZ = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcion);

            aprobacionesRequest.setTipo(auxZ);

            RemedyStub consulta = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consulta.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            logbi.insertaerror(0, aprobacionesResult.getMensaje(), "ConsultarAprobaciones()");

            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al crear el incidente ", e);

            return null;
        }

        return result;
    }

    @SuppressWarnings("static-access")
    public boolean cierreSinOT(int idIncidente, String resolucion, String montivoEstado) {
        boolean result = false;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("cierreSinOT()" + " LIMPIEZA");
        }

        try {

            ActualizarIncidente actIncitenteRequest = new ActualizarIncidente();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            if (montivoEstado.equalsIgnoreCase("rechazado proveedor")) {
                actIncitenteRequest.setAccion(action.CancelacionRechazo);
            } else if (montivoEstado.equalsIgnoreCase("por asignar")) {
                actIncitenteRequest.setAccion(action.Cancelacion);
            }

            actIncitenteRequest.setAuthentication(accesoRemedy);

            Incidente incide = new Incidente();
            incide.setAdjunto1Nombre("");
            incide.setAdjunto2Nombre("");
            _Calificacion cal = null;
            incide.setCalificacion(cal.NA);
            _TipoCierre TC = null;
            incide.setCierre(TC.NA);
            incide.setIdIncidente(idIncidente);
            incide.setJustificacion(resolucion);

            actIncitenteRequest.setIncidente(incide);

            RemedyStub consulta = new RemedyStub();

            RemedyStub.ActualizarIncidenteResponse response = new RemedyStub.ActualizarIncidenteResponse();

            response = consulta.actualizarIncidente(actIncitenteRequest);

            RsUIncidente actIncidenteResult = response.getActualizarIncidenteResult();

            logbi.insertaerror(0, actIncidenteResult.getMensaje(), "CierreSinOT()");

            if (actIncidenteResult.getExito()) {
                result = true;
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al crear el incidente ", e);

        }

        return result;
    }

    public ArrayOfInformacion ConsultaDetalleFolio(int idFolio) {
        ArrayOfInformacion arrIncidentes = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("ConsultaDetalleFolio()" + " LIMPIEZA");
        }

        try {

            Calendar today = Calendar.getInstance();
            today.set(2016, 1, 1);
            LOGGER.log(Level.INFO, "Today is ", today.getTime());

            ConsultarIncidentes consultarIncitenteRequest = new ConsultarIncidentes();

            Filtro nFiltro = new Filtro();
            nFiltro.setIdIncidente(idFolio);

            Authentication autenticatiosRemedy = new Authentication();
            autenticatiosRemedy.setPassword("$eY07KUld*@ZcG");
            autenticatiosRemedy.setUserName("UsrWSLimpieza");
            consultarIncitenteRequest.setAuthentication(autenticatiosRemedy);
            consultarIncitenteRequest.setFiltro(nFiltro);

            RemedyStub consulta = new RemedyStub();
            RemedyStub.ConsultarIncidentesResponse response = new RemedyStub.ConsultarIncidentesResponse();

            response = consulta.consultarIncidentes(consultarIncitenteRequest);

            RsCIncidente incidentes = response.getConsultarIncidentesResult();

            if (incidentes.getExito()) {
                arrIncidentes = incidentes.getIncidentes();
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO, "Ocurrio algo al adjuntar archivo al incidente ", e);

            return arrIncidentes;
        }

        return arrIncidentes;
    }

    public Aprobacion consultaAprobacion(int idIncidente, int opcion) {

        Aprobacion result = null;

        if (getPermisoPeticionesBD()) {
            peticionesRemedyBI.insertaPeticion("consultaAprobacion()" + " LIMPIEZA");
        }

        try {

            ConsultarAprobaciones aprobacionesRequest = new ConsultarAprobaciones();

            Authentication accesoRemedy = new Authentication();
            accesoRemedy.setPassword("$eY07KUld*@ZcG");
            accesoRemedy.setUserName("UsrWSLimpieza");

            _Accion action = null;

            aprobacionesRequest.setNoIncidente(idIncidente);
            aprobacionesRequest.setAuthentication(accesoRemedy);

            org.apache.axis2.databinding.types.UnsignedByte auxZ = new org.apache.axis2.databinding.types.UnsignedByte(
                    opcion);

            aprobacionesRequest.setTipo(auxZ);

            RemedyStub consultaAprobacion = new RemedyStub();

            ConsultarAprobacionesResponse response = new RemedyStub.ConsultarAprobacionesResponse();

            response = consultaAprobacion.consultarAprobaciones(aprobacionesRequest);

            RsCAprobacion aprobacionesResult = response.getConsultarAprobacionesResult();

            logbi.insertaerror(0, aprobacionesResult.getMensaje(), "ConsultaAprobacion()");

            if (aprobacionesResult.getExito()) {
                result = aprobacionesResult.getOAprobacion();
            }

        } catch (Exception e) {

            LOGGER.log(Level.INFO,
                    "AprobacionesRemedyHilos||consultarAprobaciones||Ocurrio algo al consultar la aprobacion el incidente: ",
                    e);
            result = null;
        }

        return result;

    }

}
