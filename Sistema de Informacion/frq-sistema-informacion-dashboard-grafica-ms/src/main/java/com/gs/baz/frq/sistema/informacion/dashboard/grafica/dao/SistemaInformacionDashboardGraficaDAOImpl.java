/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.grafica.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.AltaGrafica;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.Grafica;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.GraficaBase;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.dto.ParametrosGrafica;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs.GraficaClaveRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs.GraficaRowMapper;
import com.gs.baz.frq.sistema.informacion.dashboard.grafica.mprs.GraficasRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class SistemaInformacionDashboardGraficaDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectClave;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcDelete;
    private final String schema = "FRANQUICIA";

    public void init() {
        String catalogo = "PAADMDASHGRAFI";
        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName(catalogo);
        jdbcInsert.withProcedureName("SPINSDASHGRAFICA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName(catalogo);
        jdbcUpdate.withProcedureName("SPACTDASHGRAFICA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName(catalogo);
        jdbcDelete.withProcedureName("SPDELDASHGRAFICA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName(catalogo);
        jdbcSelect.withProcedureName("SPGETDASHGRAFICA");
        jdbcSelect.returningResultSet("PA_CDATOS", new GraficaRowMapper());

        jdbcSelectClave = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectClave.withSchemaName(schema);
        jdbcSelectClave.withCatalogName(catalogo);
        jdbcSelectClave.withProcedureName("SPGETDASHGRAFICA");
        jdbcSelectClave.returningResultSet("PA_CDATOS", new GraficaClaveRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName(catalogo);
        jdbcSelectAll.withProcedureName("SPGETDASHGRAFICA");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new GraficasRowMapper());
    }

    public GraficaBase selectRow(Integer id) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRAFICA", id);
        mapSqlParameterSource.addValue("PA_FCCVEGRAFICA", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<GraficaBase> data = (List<GraficaBase>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public Grafica selectRow(String clave) throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRAFICA", null);
        mapSqlParameterSource.addValue("PA_FCCVEGRAFICA", clave);
        Map<String, Object> out = jdbcSelectClave.execute(mapSqlParameterSource);
        List<Grafica> data = (List<Grafica>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<Grafica> selectAllRows() throws CustomException {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDGRAFICA", null);
        mapSqlParameterSource.addValue("PA_FCCVEGRAFICA", null);
        Map<String, Object> out = jdbcSelectAll.execute(mapSqlParameterSource);
        return (List<Grafica>) out.get("PA_CDATOS");
    }

    public AltaGrafica insertRow(ParametrosGrafica entityDTO) throws DataNotInsertedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCNOMBREGRAFICA", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCNOMBRESP", entityDTO.getNotas());
            mapSqlParameterSource.addValue("PA_FCCVEGRAFICA", entityDTO.getClave());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == -1) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDGRAFICA");
                return new AltaGrafica(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(ParametrosGrafica entityDTO) throws DataNotUpdatedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRAFICA", entityDTO.getIdGrafica());
            mapSqlParameterSource.addValue("PA_FCNOMBREGRAFICA", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FCNOMBRESP", entityDTO.getNotas());
            mapSqlParameterSource.addValue("PA_FCCVEGRAFICA", entityDTO.getClave());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == -1) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDGRAFICA", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == -1) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
