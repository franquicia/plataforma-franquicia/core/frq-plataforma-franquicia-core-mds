package com.gs.baz.frq.asignacion.mprs;

import com.gs.baz.frq.asignacion.dto.AsignacionDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class AsignacionRowMapper implements RowMapper<AsignacionDTO> {

    private AsignacionDTO status;

    @Override
    public AsignacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new AsignacionDTO();
        status.setProyecto((BigDecimal) rs.getObject("FIID_PROYECTO"));
        status.setNombreProyecto(rs.getString("NOMBRE_PROYECTO"));
        status.setNegocio(rs.getString("NEGOCIO"));
        status.setTipoProyecto(rs.getInt("FIIDTIPOPROY"));
        status.setIdCargaInicial(rs.getInt("FIID_EDOCARGAINI"));
        status.setIdCargaFin(rs.getInt("FCEDOHALLAFIN"));
        status.setCeco(rs.getString("FCID_CECO"));
        status.setFaseActual(rs.getInt("FIID_FASEACT"));
        status.setStatus(rs.getInt("FCSTATUS"));
        status.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
        status.setResponsableAsigno(rs.getInt("RESPONSABLE_ASIGNO"));
        status.setIdUsuario(rs.getInt("FIID_USUARIO"));
        status.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
        status.setObsAsignacion(rs.getString("OBS_ASIGNA"));
        status.setIdFase(rs.getInt("FIID_FASE"));
        status.setOrdenFase(rs.getInt("FIID_ORDENFASE"));
        status.setNombreFase(rs.getString("NOMBRE_FASE"));

        return status;
    }
}
