package com.gs.baz.frq.dashboard.mprs.graficos.g008;

import com.gs.baz.frq.dashboard.dto.graficos.g008.NegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegociosRowMapper implements RowMapper<NegocioDTO> {

    private NegocioDTO negociosDTO;

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        negociosDTO = new NegocioDTO();
        negociosDTO.setIdNegocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));
        return negociosDTO;
    }
}
