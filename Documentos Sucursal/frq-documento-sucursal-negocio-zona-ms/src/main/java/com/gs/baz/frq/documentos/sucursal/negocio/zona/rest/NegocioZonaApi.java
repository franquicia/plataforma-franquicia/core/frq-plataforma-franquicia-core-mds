/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.negocio.zona.rest;

import com.gs.baz.frq.documentos.sucursal.negocio.zona.dao.DocumentoSucursalNegocioZonasDAOImpl;
import com.gs.baz.frq.documentos.sucursal.negocio.zona.dto.AltaNegocioZona;
import com.gs.baz.frq.documentos.sucursal.negocio.zona.dto.NegocioZonaBase;
import com.gs.baz.frq.documentos.sucursal.negocio.zona.dto.NegociosZona;
import com.gs.baz.frq.documentos.sucursal.negocio.zona.dto.ParametrosNegocioZona;
import com.gs.baz.frq.documentos.sucursal.negocio.zona.dto.SinResultado;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.RespuestaLessResult200;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author carlos
 */
@Api(tags = "negocioZona", value = "negocioZona", description = "Api para la gestión de zonas y negocios de Layout")
@RestController
@RequestMapping("/api-local/documento-sucursal/negocio-zona/v1")
public class NegocioZonaApi {

    @Autowired
    private DocumentoSucursalNegocioZonasDAOImpl documentoSucursalNegocioZonasDAOImpl;

    //estatus de la API
    @ApiOperation(value = "status", hidden = true)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RespuestaLessResult200 status() {
        return new RespuestaLessResult200();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene Negocio Zona", notes = "Obtiene relacion Negocio Zona", nickname = "obtieneNegocioZona")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/{idNegocio}/zona/{idZona}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NegocioZonaBase> obtieneNegocioZona(@ApiParam(name = "idZona", value = "Identificador de la zona", example = "1", required = true) @PathVariable("idZona") Long idZona, @ApiParam(name = "idNegocio", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idNegocio") Long idNegocio) throws CustomException, DataNotFoundException {
        //public CatalogoZonaBase obtieneTipoInformacion(@ApiParam(name = "idZona", value = "Identificador de la zona", example = "1", required = true) @PathVariable("idZona") Long idZona) throws CustomException, DataNotFoundException {
        // CatalogoZonaBase catalogoZonaBase = documentoSucursalCatalogoZonasDAOImpl.selectRow(idZona);

        return (List<NegocioZonaBase>) documentoSucursalNegocioZonasDAOImpl.selectRowNegocioZona(idZona, idNegocio);

    }

    /**
     *
     * @return @throws CustomException
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @ApiOperation(value = "Obtiene Relacion Negocio Zona", notes = "Obtiene relacion de negocios con  zonas", nickname = "obtieneNegociosZonas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Proceso ejecutado correctamente."),
        @ApiResponse(code = 404, message = "Información no encontrada.", response = Respuesta404.class)
    })
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NegociosZona obtieneNegociosZonas() throws CustomException, DataNotFoundException {
        NegociosZona negociosZona = new NegociosZona(documentoSucursalNegocioZonasDAOImpl.selectAllRowsNegocioZona());

        if (negociosZona.getNegocioZona() != null && negociosZona.getNegocioZona().isEmpty()) {
            throw new DataNotFoundException();
        }
        return negociosZona;
    }

    /**
     *
     * @param parametrosNegocioZona
     * @return
     * @throws DataNotInsertedException
     */
    @ApiOperation(value = "Crear catalogoZona", notes = "Agrega un tipoInformacion", nickname = "creaZona")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AltaNegocioZona creaNegocioZona(@ApiParam(name = "ParametrosNegocioZona", value = "Paramentros para el alta del negocio zona", required = true) @RequestBody ParametrosNegocioZona parametrosNegocioZona) throws DataNotInsertedException {
        return documentoSucursalNegocioZonasDAOImpl.insertNegocioZona(parametrosNegocioZona);
    }

    /**
     *
     * @param parametrosCatalogoZona
     * @param idTipoInformacion
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotUpdatedException
     */
    @ApiOperation(value = "Actualizar negocio zona", notes = "Actualiza estatus de negocio zona", nickname = "actualizaNegocioZona")
    @RequestMapping(value = "/{idTipoInformacion}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    //   public SinResultado actualizaTipoInformacion(@ApiParam(name = "parametrosNegocioZona", value = "Paramentros para la actualización del zona", required = true) @RequestBody ParametrosNegocioZona parametrosNegocioZona, @ApiParam(name = "idZona", value = "Identificador del catalogo zona", example = "1", required = true) @PathVariable("idNegocio") Integer idNegocio,@PathVariable("idNegocio") Integer idZona) throws DataNotUpdatedException {
    public SinResultado actualizaNegocioZona(@ApiParam(name = "ParametrosNegocioZona", value = "Paramentros para el baja del negocio zona", required = true) @RequestBody ParametrosNegocioZona parametrosNegocioZona) throws DataNotUpdatedException {

        try {
            documentoSucursalNegocioZonasDAOImpl.updateNegocioZona(parametrosNegocioZona);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new SinResultado();
    }

    /**
     *
     * @param idTipoInformacion
     * @return
     * @throws DataNotDeletedException
     */
    @ApiOperation(value = "Eliminar negocio zona", notes = "Elimina negocio zona", nickname = "eliminaNegocioZona")
    @RequestMapping(value = "/{idZona}/negocio/{idNegocio}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SinResultado eliminaNegociozona(@ApiParam(name = "idZona", value = "Identificador de la zona", example = "1", required = true) @PathVariable("idZona") Long idZona, @ApiParam(name = "idNegocio", value = "Identificador del negocio", example = "1", required = true) @PathVariable("idNegocio") Long idNegocio) throws CustomException, DataNotFoundException, Exception {

        documentoSucursalNegocioZonasDAOImpl.deleteRowNegocioZona(idZona, idNegocio);

        return new SinResultado();
    }

}
