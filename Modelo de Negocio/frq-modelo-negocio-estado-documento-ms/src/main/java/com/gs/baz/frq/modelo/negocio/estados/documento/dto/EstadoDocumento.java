/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.documento.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Estado del Documento", value = "EstadoDocumento")
public class EstadoDocumento extends EstadoDocumentoBase {

    @JsonProperty(value = "idEstadoDocumento")
    @ApiModelProperty(notes = "Identificador del Estado del Documento", example = "1", position = -1)
    private Integer idEstadoDocumento;

    public Integer getIdEstadoDocumento() {
        return idEstadoDocumento;
    }

    public void setIdEstadoDocumento(Integer idEstadoDocumento) {
        this.idEstadoDocumento = idEstadoDocumento;
    }

    @Override
    public String toString() {
        return "EstadoDocumento{" + "idEstadoDocumento=" + idEstadoDocumento + '}';
    }

}
