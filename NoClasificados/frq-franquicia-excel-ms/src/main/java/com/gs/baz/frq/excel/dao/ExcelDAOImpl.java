package com.gs.baz.frq.excel.dao;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.gs.baz.frq.excel.dto.DashboardDTO;
import com.gs.baz.frq.excel.dto.GeografiaDTO;
import com.gs.baz.frq.excel.dto.Pregunta7sDTO;
import com.gs.baz.frq.excel.dto.Respuesta7sDTO;
import com.gs.baz.frq.excel.dto.SucursalFecha;
import com.gs.baz.frq.excel.dto.Visita7sDTO;
import com.gs.baz.frq.excel.mprs.Pregunta7sRowMapper;
import com.gs.baz.frq.excel.mprs.Respuesta7sRowMapper;
import com.gs.baz.frq.excel.mprs.Visita7sRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.tool.GeneraExcel;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class ExcelDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectConfiguracionDisciplina;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    private ArrayList<String> cabecera = null;

    public void init() {

        schema = "FRANQUICIA";

        jdbcSelectConfiguracionDisciplina = (DefaultJdbcCall) new DefaultJdbcCall(this.frqJdbcTemplate);
        jdbcSelectConfiguracionDisciplina.withSchemaName(schema);
        jdbcSelectConfiguracionDisciplina.withCatalogName("PAREPORTE_MED");
        jdbcSelectConfiguracionDisciplina.withProcedureName("SPREPMED");
        jdbcSelectConfiguracionDisciplina.returningResultSet("PA_PREGUNTAS", new Pregunta7sRowMapper());
        jdbcSelectConfiguracionDisciplina.returningResultSet("PA_VISITAS", new Visita7sRowMapper());
        jdbcSelectConfiguracionDisciplina.returningResultSet("PA_RESPUESTAS", new Respuesta7sRowMapper());

    }

    public ArrayList<Object> generaReporteMedicion7sArray(Integer disc, Integer anio, Integer periodo) throws CustomException {
        ArrayList<Object> temp = new ArrayList<>();
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ID_DISIP", disc);
        mapSqlParameterSource.addValue("PA_ANIO", anio);
        mapSqlParameterSource.addValue("PA_PERIODO", periodo);
        Map<String, Object> out = jdbcSelectConfiguracionDisciplina.execute(mapSqlParameterSource);
        ArrayList<Pregunta7sDTO> listaPregunta = (ArrayList<Pregunta7sDTO>) out.get("PA_PREGUNTAS");
        ArrayList<Visita7sDTO> listaVisita = (ArrayList<Visita7sDTO>) out.get("PA_VISITAS");
        ArrayList<Respuesta7sDTO> listaRespuesta = (ArrayList<Respuesta7sDTO>) out.get("PA_RESPUESTAS");
        temp.add(listaPregunta);
        temp.add(listaVisita);
        temp.add(listaRespuesta);
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        temp.add(success);
        return temp;
    }

    public Workbook generaReporteMedicion7s(Integer disc, Integer anio, Integer periodo) throws CustomException {
        Workbook reporte = null;
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_ID_DISIP", disc);
        mapSqlParameterSource.addValue("PA_ANIO", anio);
        mapSqlParameterSource.addValue("PA_PERIODO", periodo);
        Map<String, Object> out = jdbcSelectConfiguracionDisciplina.execute(mapSqlParameterSource);
        List<Pregunta7sDTO> listaPregunta = (ArrayList<Pregunta7sDTO>) out.get("PA_PREGUNTAS");
        List<Visita7sDTO> listaVisita = (ArrayList<Visita7sDTO>) out.get("PA_VISITAS");
        List<Respuesta7sDTO> listaRespuesta = (ArrayList<Respuesta7sDTO>) out.get("PA_RESPUESTAS");
        boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
        if (success && listaPregunta != null && listaPregunta.size() > 0 && listaVisita != null && listaVisita.size() > 0 && listaRespuesta != null && listaRespuesta.size() > 0) {
            try {
                String tituloPestana = "ReporteMedicion";
                List<String> encabezado = new ArrayList<>();
                Map<Integer, ArrayList<Object>> cuerpo;
                String[] colum = {"Fecha que se Realizo", "Quien lo realizo", "Número de Usuario", "Puesto", "País", "Territorio", "Zona", "Regional", "Sucursal", "Nombre Sucursal", "Canal", "Calificacion BAZ", "Calificacion EKT", "Calificacion PPR", "Ponderacion", "Calificación", "Conteo de SI", "Conteo de NO", "Imperdonables"};

                encabezado.addAll(Arrays.asList(colum));
                cuerpo = generaCuerpo7s(listaPregunta, listaVisita, listaRespuesta);
                if (cabecera != null) {
                    encabezado.addAll(cabecera);
                }
                if (cuerpo != null) {
                    GeneraExcel excel = new GeneraExcel(tituloPestana, encabezado, cuerpo);
                    reporte = excel.getWorkbook();
                }
            } catch (Exception ex) {
                throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("Exception Error to generaReporteMedicion"), ex);
            }
        }
        return reporte;
    }
    int i = 0;

    public Boolean encuentraDuplicados(ArrayList<Visita7sDTO> visitasUnicas, Visita7sDTO visita) {

        boolean existe = false;

        for (Visita7sDTO visitaAux : visitasUnicas) {
            if (visitaAux.getIdCeco().equalsIgnoreCase(visita.getIdCeco()) && visitaAux.getId_negocio() == visita.getId_negocio() && visitaAux.getFecha().equalsIgnoreCase(visita.getFecha())) {
                existe = true;
            }
        }

        return existe;
    }
    int countGek = 0;

    public void generaReporteDashboardFromFile() throws CustomException, IOException {
        Workbook reporte = null;
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File("/Users/MADA/Downloads/JSON/responseDashboard.json");
        ObjectNode jsonIn = objectMapper.readValue(file, ObjectNode.class);

        List<DashboardDTO> listaDashboard = objectMapper.readValue(jsonIn.get("data").get(0).toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, DashboardDTO.class));

        Map<String, GeografiaDTO> cecos = new HashMap<>();

        for (DashboardDTO dashboard : listaDashboard) {

            if (!cecos.containsKey(dashboard.getId_ceco())) {
                GeografiaDTO geografia = new GeografiaDTO();
                geografia.setId_ceco(dashboard.getId_ceco());
                geografia.setId_region(dashboard.getId_region());
                geografia.setRegion(dashboard.getRegion());
                geografia.setId_territorio(dashboard.getId_territorio());
                geografia.setTerritorio(dashboard.getTerritorio());
                geografia.setId_zona(dashboard.getId_zona());
                geografia.setZona(dashboard.getZona());
                cecos.put(dashboard.getId_ceco(), geografia);
            }
        }

        ArrayList<GeografiaDTO> listaGeografia = new ArrayList<GeografiaDTO>();

        cecos.forEach((k, v) -> {

            listaGeografia.add(v);
        });

        Gson json = new Gson();
        String cadena = json.toJson(listaGeografia);

        System.out.println(cadena);

    }

    int id_configuracion_disciplina_BAZ = 20;
    int id_configuracion_disciplina_EKT = 21;
    int id_configuracion_disciplina_PPR = 22;
    int id_configuracion_disciplina_GEK = 23;
    int id_disciplina = 5;
    int id_protocolo = 48;

    public void generaReporteMedicion7sFromFile() throws CustomException, IOException {
        Workbook reporte = null;
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File("/Users/MADA/Downloads/JSON/ExcelReporteMedicion_1641.json");
        ObjectNode jsonIn = objectMapper.readValue(file, ObjectNode.class);
        List<Pregunta7sDTO> listaPregunta = objectMapper.readValue(jsonIn.get("data").get(0).toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Pregunta7sDTO.class));
        List<Visita7sDTO> listaVisita = objectMapper.readValue(jsonIn.get("data").get(1).toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Visita7sDTO.class));
        List<Respuesta7sDTO> listaRespuesta = objectMapper.readValue(jsonIn.get("data").get(2).toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Respuesta7sDTO.class));

        List<DashboardDTO> listaDashboard = new ArrayList<DashboardDTO>();

        System.out.println("Count R = " + listaRespuesta.size());
        listaRespuesta.removeIf(obj -> obj.getRespuesta().equals(""));
        System.out.println("Count Clear R = " + listaRespuesta.size());

        System.out.println("Count V = " + listaVisita.size());
        listaVisita.removeIf(obj -> obj.getCalif_esperada() == 0);
        System.out.println("LISTA VISITA SIZE = " + listaVisita.size());

        //   System.out.println(listaVisita.size());
        //aki la lectura del base con su fecha
        File fechaSucursal = new File("E:/Users/kramireza/Documents/DEPURA EXCEL/convertcsv.json");
        ObjectMapper objectMapper1 = new ObjectMapper();
        List<SucursalFecha> sucursalFecha = objectMapper.readValue(fechaSucursal, new TypeReference<List<SucursalFecha>>() {
        });

        for (SucursalFecha sff : sucursalFecha) {
            String a = sff.getFecha();
            String b = " 00:00:00";
            String[] arrOfStr = a.split("/");
            //System.out.println("arrOfStr " + arrOfStr[1]);
            String ffinal = "";
            int i = arrOfStr.length;
            ffinal = ffinal.concat(arrOfStr[i - 1]).concat("-").concat(arrOfStr[i - 2]).concat("-").concat(arrOfStr[i - 3]).concat(b);
            sff.setFecha(ffinal);
            a = null;
            b = null;
            arrOfStr = null;
            ffinal = null;
            //   System.out.println("ssf:: " + sff.toString());

        }
        System.out.println("TAMANIO: " + sucursalFecha.size());
        ArrayList<Visita7sDTO> visitasRepetidas = new ArrayList<>();
        for (SucursalFecha sff : sucursalFecha) {
            for (Visita7sDTO visi : listaVisita) {
                if (Integer.valueOf(visi.getIdCeco()).intValue() == sff.getSucursal().intValue()) {
                    //        System.out.println("COMP" + sff.getFecha() + ":::: " + visi.getFecha());
                    if (!sff.getFecha().equals(visi.getFecha())) {
                        visitasRepetidas.add(visi);
                    }
                } else {
                    if (Integer.valueOf(visi.getIdCeco()).intValue() == 486211
                            || Integer.valueOf(visi.getIdCeco()).intValue() == 487541
                            || Integer.valueOf(visi.getIdCeco()).intValue() == 480974) {
                        visitasRepetidas.add(visi);
                    }
                }
            }
        }

        System.out.println("no:: " + visitasRepetidas.size());

        for (Visita7sDTO no : visitasRepetidas) {
            listaVisita.remove(no);

        }

        System.out.println("LISTA VISITA SIZE = " + listaVisita.size());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ObjectMapper mapper = new ObjectMapper();

        mapper.writeValue(out, listaVisita);

        final byte[] data = out.toByteArray();
        //  System.out.println(new String(data));

        Files.write(new File("E:/Users/kramireza/Documents/DEPURA EXCEL/Nuevo.json").toPath(), data);
        if (listaVisita.size() > 0 && listaRespuesta != null && listaRespuesta.size() > 0) {
            try {
                String tituloPestana = "ReporteMedicion";
                List<String> encabezado = new ArrayList<>();
                Map<Integer, ArrayList<Object>> cuerpo;
                String[] colum = {"Fecha que se Realizo", "Quien lo realizo", "Número de Usuario", "Puesto", "País", "Territorio", "Zona", "Regional", "Sucursal", "Nombre Sucursal", "Canal", "Calificacion BAZ", "Calificacion EKT", "Calificacion PPR", "Ponderacion", "Calificación", "Conteo de SI", "Conteo de NO", "Imperdonables"};

                encabezado.addAll(Arrays.asList(colum));
                // aqui se tendria que generar la información sin los duplicados

                cuerpo = generaCuerpo7s(listaPregunta, listaVisita, listaRespuesta);
                if (cabecera != null) {
                    encabezado.addAll(cabecera);
                }

                if (cuerpo != null) {

                    GeneraExcel excel = new GeneraExcel(tituloPestana, encabezado, cuerpo);
                    reporte = excel.getWorkbook();
                    File temp = new File("/Users/MADA/Downloads/JSON/ExcelReporteMedicion_1641_No9Pts.xlsx");
                    temp.createNewFile();
                    try (FileOutputStream fileOut = new FileOutputStream(temp)) {
                        reporte.write(fileOut);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("Exception Error to generaReporteMedicion"), ex);
            }
        }

    }

    public static void main(String[] args) throws CustomException, IOException {
        //new ExcelDAOImpl().generaReporteMedicion7sFromFile();
        //    new ExcelDAOImpl().generaReporteDashboardFromFile();
        new ExcelDAOImpl().procesaHtml();
    }

    private Map<Integer, ArrayList<Object>> generaCuerpo7s(List<Pregunta7sDTO> listaPregunta, List<Visita7sDTO> listaVisita, List<Respuesta7sDTO> listaRespuesta) {
        Map<Integer, ArrayList<Object>> retorno = new HashMap();
        ArrayList<Object> cuerpoArray;
        this.cabecera = new ArrayList<>();
        int intento = 0;

        ArrayList<Visita7sDTO> incluidos = new ArrayList<>();
        int contador = 0;
        for (Visita7sDTO visita : listaVisita) {
            System.out.println("Contador = " + contador);
            contador++;
            if (!incluidos.contains(visita)) {
                Boolean bandera = true;
                int contno = 0, conteosi = 0, imperdon = 0, resp = 0;
                Double esp1 = 0.0, esp2 = 0.0, esp3 = 0.0, esp4 = 0.0;
                Double lin1 = 0.0, lin2 = 0.0, lin3 = 0.0, lin4 = 0.0;
                Double cale = 0.0, cali = 0.0, calt = 0.0, calG = 0.0, caliG = 0.0, caleG = 0.0;
                List<Visita7sDTO> listaIds = listaVisita.stream().filter(r -> (Objects.equals(visita.getFecha(), r.getFecha()) && Objects.equals(r.getIdCeco(), visita.getIdCeco()))).collect(Collectors.toList());//prueba
                for (Visita7sDTO v : listaIds) {
                    conteosi = conteosi + v.getConteoSi();
                    contno = contno + v.getConteoNo();
                    imperdon = imperdon + v.getConteoImperdonable();
                }
                Visita7sDTO visitaBAZ = listaIds.stream().filter(r -> (Objects.equals(1, r.getId_negocio()))).findFirst().orElse(null);
                Visita7sDTO visitaEKT = listaIds.stream().filter(r -> (Objects.equals(2, r.getId_negocio()))).findFirst().orElse(null);
                Visita7sDTO visitaPPR = listaIds.stream().filter(r -> (Objects.equals(3, r.getId_negocio()))).findFirst().orElse(null);
                Visita7sDTO visitaComunes = listaIds.stream().filter(r -> (Objects.equals(4, r.getId_negocio()))).findFirst().orElse(null);
                if (visitaComunes != null) {
                    esp4 = visitaComunes.getCalif_esperada().doubleValue();
                    lin4 = visitaComunes.getCalif_lineal().doubleValue();
                }
                if (visitaBAZ != null) {
                    esp1 = visitaBAZ.getCalif_esperada().doubleValue();
                    lin1 = visitaBAZ.getCalif_lineal().doubleValue();

                }
                if (visitaEKT != null) {
                    esp2 = visitaEKT.getCalif_esperada().doubleValue();
                    lin2 = visitaEKT.getCalif_lineal().doubleValue();
                }
                if (visitaPPR != null) {
                    esp3 = visitaPPR.getCalif_esperada().doubleValue();
                    lin3 = visitaPPR.getCalif_lineal().doubleValue();
                }

                cuerpoArray = new ArrayList<>();
                cuerpoArray.add(visita.getFecha());
                cuerpoArray.add(visita.getNombreQuienRealizo());
                cuerpoArray.add(visita.getIdUsuario());
                cuerpoArray.add(visita.getDescripcionPuesto());
                cuerpoArray.add(visita.getNombrePais());
                cuerpoArray.add(visita.getTerritorio());
                cuerpoArray.add(visita.getZona());
                cuerpoArray.add(visita.getRegion());
                cuerpoArray.add(visita.getIdCeco());
                cuerpoArray.add(visita.getNombreCeco());
                cuerpoArray.add(visita.getDescripcionCanal());
                if (visitaBAZ != null) {
                    cali = lin1 + lin4;
                    cale = esp1 + esp4;
                    calt = (cali / cale) * 100;
                    if (imperdon > 0) {
                        cuerpoArray.add(0);
                    } else {
                        cuerpoArray.add((int) Math.round(calt));
                    }

                } else {
                    cuerpoArray.add("");
                }
                if (visitaEKT != null) {
                    cali = lin2 + lin4;
                    cale = esp2 + esp4;
                    calt = (cali / cale) * 100;
                    cuerpoArray.add((int) Math.round(calt));
                } else {
                    cuerpoArray.add("");
                }
                if (visitaPPR != null) {
                    cali = lin3 + lin4;
                    cale = esp3 + esp4;
                    calt = (cali / cale) * 100;
                    cuerpoArray.add((int) Math.round(calt));
                } else {
                    cuerpoArray.add("");
                }
                caliG = lin1 + lin2 + lin3 + lin4;
                caleG = esp1 + esp2 + esp3 + esp4;
                calG = (caliG / caleG) * 100;
                cuerpoArray.add((int) Math.round(calG));
                if (imperdon > 0) {
                    cuerpoArray.add(0);
                } else {
                    cuerpoArray.add((int) Math.round(calG));
                }

                cuerpoArray.add(conteosi);
                cuerpoArray.add(contno);
                cuerpoArray.add(imperdon);

                for (Pregunta7sDTO pregunta : listaPregunta) {
                    if (intento == 0) {
                        this.cabecera.add(pregunta.getDescripcion());
                    }
                    for (Visita7sDTO list : listaIds) {
                        incluidos.add(list);

                        Respuesta7sDTO respuesta = listaRespuesta.stream()
                                .filter(r -> (Objects.equals(pregunta.getIdPregunta(), r.getIdPregunta()) && Objects.equals(r.getIdVisita(), list.getIdVisita())))
                                .findFirst()
                                .orElse(null);
                        if (respuesta != null && resp == 0) {
                            resp = 1;

                            if ((pregunta.getIdPregunta() == 26867 || pregunta.getIdPregunta() == 27439 || pregunta.getIdPregunta() == 27009) && false) {    // Regla 7

                                if (visitaComunes == null) {

                                    bandera = false;
                                    if (respuesta.getRespuesta().equalsIgnoreCase("SI")) {
                                        conteosi = conteosi - 1;
                                        if (pregunta.getIdPregunta() == 26867) {
                                            lin1 = lin1 - 1.0;
                                            esp1 = esp1 - 1.0;
                                        } else if (pregunta.getIdPregunta() == 27439) {
                                            lin2 = lin2 - 1.0;
                                            esp2 = esp2 - 1.0;
                                        } else if (pregunta.getIdPregunta() == 27009) {
                                            lin3 = lin3 - 1.0;
                                            esp3 = esp3 - 1.0;
                                        }
                                    } else {
                                        contno = contno - 1;
                                        if (pregunta.getIdPregunta() == 26867) {

                                            esp1 = esp1 - 1.0;
                                        } else if (pregunta.getIdPregunta() == 27439) {

                                            esp2 = esp2 - 1.0;
                                        } else if (pregunta.getIdPregunta() == 27009) {

                                            esp3 = esp3 - 1.0;
                                        }
                                    }

                                } else {
                                    Respuesta7sDTO respuesta25 = listaRespuesta.stream()
                                            .filter(r -> (Objects.equals(24843, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaComunes.getIdVisita())))
                                            .findFirst()
                                            .orElse(null);
                                    if (respuesta25 == null) {
                                        bandera = false;
                                        if (respuesta.getRespuesta().equalsIgnoreCase("SI")) {
                                            conteosi = conteosi - 1;
                                            if (pregunta.getIdPregunta() == 26867) {
                                                lin1 = lin1 - 1.0;
                                                esp1 = esp1 - 1.0;
                                            } else if (pregunta.getIdPregunta() == 27439) {
                                                lin2 = lin2 - 1.0;
                                                esp2 = esp2 - 1.0;
                                            } else if (pregunta.getIdPregunta() == 27009) {
                                                lin3 = lin3 - 1.0;
                                                esp3 = esp3 - 1.0;
                                            }
                                        } else {
                                            contno = contno - 1;
                                            if (pregunta.getIdPregunta() == 26867) {

                                                esp1 = esp1 - 1.0;
                                            } else if (pregunta.getIdPregunta() == 27439) {

                                                esp2 = esp2 - 1.0;
                                            } else if (pregunta.getIdPregunta() == 27009) {

                                                esp3 = esp3 - 1.0;
                                            }
                                        }
                                    }
                                }

                            }
                            if (pregunta.getRegal_nueve() != 0 && bandera && false) {
                                if (generaCuerpoOrdenado7s(listaPregunta, listaVisita, listaRespuesta, pregunta.getRegal_nueve(), visitaBAZ, visitaEKT, visitaPPR, visitaComunes)) {

                                    if (respuesta.getRespuesta().equalsIgnoreCase("NO")) {
                                        cuerpoArray.add("SI");
                                        conteosi = conteosi + 1;
                                        cuerpoArray.set(16, conteosi);
                                        contno = contno - 1;
                                        cuerpoArray.set(17, contno);
                                        if (pregunta.getRegal_nueve() == 1 || pregunta.getRegal_nueve() == 4) {
                                            lin1 = lin1 + 1.0;
                                        } else if (pregunta.getRegal_nueve() == 2 || pregunta.getRegal_nueve() == 5) {
                                            lin2 = lin2 + 1.0;
                                        } else if (pregunta.getRegal_nueve() == 3 || pregunta.getRegal_nueve() == 6) {
                                            lin3 = lin3 + 1.0;
                                        } else if (pregunta.getRegal_nueve() == 7) {

                                            if (pregunta.getIdPregunta() == 26867) {
                                                lin1 = lin1 + 1.0;
                                            } else if (pregunta.getIdPregunta() == 27439) {
                                                lin2 = lin2 + 1.0;
                                            } else if (pregunta.getIdPregunta() == 27009) {
                                                lin3 = lin3 + 1.0;
                                            }
                                        }
                                    } else {

                                        cuerpoArray.add(respuesta.getRespuesta());

                                    }

                                } else {

                                    if (respuesta.getRespuesta().equalsIgnoreCase("SI")) {

                                        cuerpoArray.add("NO");
                                        conteosi = conteosi - 1;
                                        cuerpoArray.set(16, conteosi);
                                        contno = contno + 1;
                                        cuerpoArray.set(17, contno);
                                        if (pregunta.getRegal_nueve() == 1 || pregunta.getRegal_nueve() == 4) {
                                            lin1 = lin1 - 1.0;
                                        } else if (pregunta.getRegal_nueve() == 2 || pregunta.getRegal_nueve() == 5) {
                                            lin2 = lin2 - 1.0;
                                        } else if (pregunta.getRegal_nueve() == 3 || pregunta.getRegal_nueve() == 6) {
                                            lin3 = lin3 - 1.0;
                                        } else if (pregunta.getRegal_nueve() == 7) {

                                            if (pregunta.getIdPregunta() == 26867) {
                                                lin1 = lin1 - 1.0;
                                            } else if (pregunta.getIdPregunta() == 27439) {
                                                lin2 = lin2 - 1.0;
                                            } else if (pregunta.getIdPregunta() == 27009) {
                                                lin3 = lin3 - 1.0;
                                            }
                                        }
                                    } else {
                                        cuerpoArray.add(respuesta.getRespuesta());
                                    }
                                }
                            } else {
                                if (bandera) {
                                    cuerpoArray.add(respuesta.getRespuesta());
                                } else {
                                    bandera = true;
                                    cuerpoArray.add("");
                                }

                            }

                        }
                    }
                    if (resp == 0) {
                        cuerpoArray.add("");
                    } else {
                        resp = 0;
                    }
                }

                if (visitaBAZ != null) {
                    cali = lin1 + lin4;
                    cale = esp1 + esp4;
                    calt = (cali / cale) * 100;
                    if (imperdon > 0) {
                        cuerpoArray.set(11, 0);
                    } else {
                        cuerpoArray.set(11, (int) Math.round(calt));
                    }

                } else {
                    cuerpoArray.set(11, "");
                }
                if (visitaEKT != null) {
                    cali = lin2 + lin4;
                    cale = esp2 + esp4;
                    calt = (cali / cale) * 100;
                    cuerpoArray.set(12, (int) Math.round(calt));
                } else {
                    cuerpoArray.set(12, "");
                }
                if (visitaPPR != null) {
                    cali = lin3 + lin4;
                    cale = esp3 + esp4;
                    calt = (cali / cale) * 100;
                    cuerpoArray.set(13, (int) Math.round(calt));
                } else {
                    cuerpoArray.set(13, "");
                }
                caliG = lin1 + lin2 + lin3 + lin4;
                caleG = esp1 + esp2 + esp3 + esp4;
                calG = (caliG / caleG) * 100;
                cuerpoArray.set(14, (int) Math.round(calG));
                if (imperdon > 0) {

                } else {
                    cuerpoArray.set(15, (int) Math.round(calG));
                }
                intento = 1;
                retorno.put(visita.getIdVisita(), cuerpoArray);

            }

        }

        return retorno;

    }

    private Map<Integer, ArrayList<Object>> ordenarCurpo7s(Map<Integer, ArrayList<Object>> cuerpo) {
        Map<Integer, ArrayList<Object>> retorno = new HashMap();
        int fila = 0;
        if (cuerpo != null) {
            for (Map.Entry<Integer, ArrayList<Object>> entry : cuerpo.entrySet()) {
                retorno.equals(entry);
                retorno.containsValue(entry.getKey());

            }
        }
        return retorno;

    }

    private Boolean generaCuerpoOrdenado7s(List<Pregunta7sDTO> listaPregunta, List<Visita7sDTO> listaVisita, List<Respuesta7sDTO> listaRespuesta, int regla, Visita7sDTO visitaBAZ, Visita7sDTO visitaEKT, Visita7sDTO visitaPPR, Visita7sDTO visitaComunes) {
        Boolean retorno = true;
        switch (regla) {
            case 1:
                int idPreg8 = listaPregunta.get(7).getIdPregunta();
                int idPreg10 = listaPregunta.get(9).getIdPregunta();
                Respuesta7sDTO respuesta1 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg8, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaBAZ.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                Respuesta7sDTO respuesta2 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg10, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaBAZ.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                if (respuesta1 != null && respuesta2 != null) {
                    if (respuesta1.getRespuesta().equalsIgnoreCase("SI") && respuesta2.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }

                break;
            case 2:
                int idPreg9 = listaPregunta.get(8).getIdPregunta();
                Respuesta7sDTO respuesta9 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg9, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaEKT.getIdVisita())))
                        .findFirst()
                        .orElse(null);

                if (respuesta9 != null) {
                    if (respuesta9.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 3:
                int idPreg13 = listaPregunta.get(12).getIdPregunta();
                Respuesta7sDTO respuesta13 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg13, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaPPR.getIdVisita())))
                        .findFirst()
                        .orElse(null);

                if (respuesta13 != null) {
                    if (respuesta13.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 4:
                int idPreg21 = listaPregunta.get(20).getIdPregunta();
                int idPreg23 = listaPregunta.get(22).getIdPregunta();
                Respuesta7sDTO respuesta21 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg21, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaBAZ.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                Respuesta7sDTO respuesta23 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg23, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaBAZ.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                if (respuesta21 != null && respuesta23 != null) {
                    if (respuesta21.getRespuesta().equalsIgnoreCase("SI") && respuesta23.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 5:
                int idPreg19 = listaPregunta.get(18).getIdPregunta();
                Respuesta7sDTO respuesta19 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg19, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaEKT.getIdVisita())))
                        .findFirst()
                        .orElse(null);

                if (respuesta19 != null) {
                    if (respuesta19.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 6:
                int idPreg25 = listaPregunta.get(24).getIdPregunta();
                int idPreg26 = listaPregunta.get(25).getIdPregunta();
                int idPreg27 = listaPregunta.get(26).getIdPregunta();
                Respuesta7sDTO respuesta25 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg25, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaPPR.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                Respuesta7sDTO respuesta26 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg26, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaPPR.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                Respuesta7sDTO respuesta27 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg27, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaPPR.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                if (respuesta26 != null && respuesta27 != null && respuesta25 != null) {
                    if (respuesta25.getRespuesta().equalsIgnoreCase("SI") && respuesta26.getRespuesta().equalsIgnoreCase("SI") && respuesta27.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }

                break;
            case 7:
                int idPreg32 = listaPregunta.get(31).getIdPregunta();
                int idPreg34 = listaPregunta.get(33).getIdPregunta();
                int idPreg36 = listaPregunta.get(35).getIdPregunta();
                Respuesta7sDTO respuesta32 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg32, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaComunes.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                Respuesta7sDTO respuesta34 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg34, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaComunes.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                Respuesta7sDTO respuesta36 = listaRespuesta.stream()
                        .filter(r -> (Objects.equals(idPreg36, r.getIdPregunta()) && Objects.equals(r.getIdVisita(), visitaComunes.getIdVisita())))
                        .findFirst()
                        .orElse(null);
                if (respuesta32 != null && respuesta34 != null && respuesta36 != null) {
                    if (respuesta32.getRespuesta().equalsIgnoreCase("SI") && respuesta34.getRespuesta().equalsIgnoreCase("SI") && respuesta36.getRespuesta().equalsIgnoreCase("SI")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;

        }

        return retorno;

    }

    public boolean procesaHtml() {
        ArrayList<Integer> cecos = new ArrayList();
        cecos.add(480116);
        cecos.add(480121);
        cecos.add(480144);
        cecos.add(480150);
        cecos.add(480200);
        cecos.add(480214);
        cecos.add(480266);
        cecos.add(480547);
        cecos.add(480550);
        cecos.add(480563);
        cecos.add(480602);
        cecos.add(480737);
        cecos.add(480744);
        cecos.add(480783);
        cecos.add(480800);
        cecos.add(480933);
        cecos.add(480990);
        cecos.add(481012);
        cecos.add(481083);
        cecos.add(481119);
        cecos.add(481122);
        cecos.add(481481);
        cecos.add(481508);
        cecos.add(481544);
        cecos.add(481724);
        cecos.add(481745);
        cecos.add(481758);
        cecos.add(481769);
        cecos.add(481779);
        cecos.add(481892);
        cecos.add(482001);
        cecos.add(482011);
        cecos.add(482047);
        cecos.add(482082);
        cecos.add(482102);
        cecos.add(482124);
        cecos.add(482144);
        cecos.add(482170);
        cecos.add(482175);
        cecos.add(482191);
        cecos.add(482197);
        cecos.add(482204);
        cecos.add(482213);
        cecos.add(482312);
        cecos.add(482354);
        cecos.add(482397);
        cecos.add(482615);
        cecos.add(482668);
        cecos.add(482695);
        cecos.add(482697);
        cecos.add(482771);
        cecos.add(482776);
        cecos.add(482846);
        cecos.add(482914);
        cecos.add(482948);
        cecos.add(483036);
        cecos.add(483081);
        cecos.add(483094);
        cecos.add(483874);
        cecos.add(483892);
        cecos.add(484000);
        cecos.add(484093);
        cecos.add(484097);
        cecos.add(484183);
        cecos.add(484197);
        cecos.add(484213);
        cecos.add(484215);
        cecos.add(484310);
        cecos.add(484336);
        cecos.add(484420);
        cecos.add(484423);
        cecos.add(484442);
        cecos.add(484452);
        cecos.add(484596);
        cecos.add(484597);
        cecos.add(484606);
        cecos.add(484653);
        cecos.add(484684);
        cecos.add(484692);
        cecos.add(484718);
        cecos.add(484722);
        cecos.add(484776);
        cecos.add(484815);
        cecos.add(484912);
        cecos.add(484925);
        cecos.add(484941);
        cecos.add(485021);
        cecos.add(485060);
        cecos.add(485112);
        cecos.add(485128);
        cecos.add(485147);
        cecos.add(485423);
        cecos.add(485495);
        cecos.add(485496);
        cecos.add(485575);
        cecos.add(485660);
        cecos.add(485665);
        cecos.add(485678);
        cecos.add(485688);
        cecos.add(485694);
        cecos.add(485733);
        cecos.add(485784);
        cecos.add(485818);
        cecos.add(485840);
        cecos.add(485885);
        cecos.add(485907);
        cecos.add(485924);
        cecos.add(485976);
        cecos.add(486019);
        cecos.add(486067);
        cecos.add(486075);
        cecos.add(486078);
        cecos.add(486110);
        cecos.add(486127);
        cecos.add(486144);
        cecos.add(486160);
        cecos.add(486185);
        cecos.add(486198);
        cecos.add(486201);
        cecos.add(486255);
        cecos.add(486381);
        cecos.add(486512);
        cecos.add(486542);
        cecos.add(486543);
        cecos.add(486812);
        cecos.add(487126);
        cecos.add(487184);
        cecos.add(487359);
        cecos.add(487362);
        cecos.add(487509);
        cecos.add(487540);
        cecos.add(487663);
        cecos.add(487728);
        cecos.add(487793);
        cecos.add(487794);
        cecos.add(488031);
        cecos.add(488176);
        cecos.add(488186);
        cecos.add(488192);
        cecos.add(488241);
        cecos.add(488287);
        cecos.add(488323);
        cecos.add(488358);
        cecos.add(488372);
        cecos.add(488390);
        cecos.add(488439);
        cecos.add(488442);
        cecos.add(488481);
        cecos.add(488510);
        cecos.add(488533);
        cecos.add(488536);
        cecos.add(488542);
        cecos.add(488581);
        cecos.add(488594);
        cecos.add(488620);
        cecos.add(488646);
        cecos.add(488651);
        cecos.add(488668);
        cecos.add(488673);
        cecos.add(488692);
        cecos.add(488765);
        cecos.add(488795);
        cecos.add(488798);
        cecos.add(488836);
        cecos.add(488840);
        cecos.add(488853);
        cecos.add(488866);
        cecos.add(488950);
        cecos.add(488951);
        cecos.add(488963);
        cecos.add(489043);
        cecos.add(489056);
        cecos.add(489093);
        cecos.add(489247);
        cecos.add(489379);
        cecos.add(489505);
        cecos.add(489534);
        cecos.add(489536);
        cecos.add(489588);
        cecos.add(489659);
        cecos.add(489668);
        cecos.add(489671);
        cecos.add(489737);
        cecos.add(489788);
        cecos.add(489802);
        cecos.add(489851);
        cecos.add(489855);
        cecos.add(489882);
        try {
            ArrayList<GeografiaDTO> geografiaFinal = new ArrayList();
            File fileGeografia = new File("E:/Users/kramireza/Documents/DEPURA EXCEL/GEOGRAFIA.json");
            ObjectMapper objectMapper1 = new ObjectMapper();
            List<GeografiaDTO> geografia = objectMapper1.readValue(fileGeografia, new TypeReference<List<GeografiaDTO>>() {
            });

            for (int i = 0; i < cecos.size(); i++) {
                for (GeografiaDTO g : geografia) {
                    if (cecos.get(i).intValue() == Integer.valueOf(g.getId_ceco()).intValue()) {
                        geografiaFinal.add(g);
                    }
                }
            }
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final ObjectMapper mapper = new ObjectMapper();

            mapper.writeValue(out, geografiaFinal);

            final byte[] data = out.toByteArray();
            //  System.out.println(new String(data));

            Files.write(new File("E:/Users/kramireza/Documents/DEPURA EXCEL/GeografiaP20.json").toPath(), data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}
