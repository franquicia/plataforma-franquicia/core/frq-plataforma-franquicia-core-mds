/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author kramireza
 */
@ApiModel(description = "Datos de atributo sensible", value = "AtributoSensible")
public class AtributoSensible extends AtributoSensibleBase {

    @JsonProperty(value = "idAtributo")
    @ApiModelProperty(notes = "Identificador del atributo sensible", example = "1", position = -1)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idAtributo;

    public Integer getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(Integer idAtributo) {
        this.idAtributo = idAtributo;
    }

}
