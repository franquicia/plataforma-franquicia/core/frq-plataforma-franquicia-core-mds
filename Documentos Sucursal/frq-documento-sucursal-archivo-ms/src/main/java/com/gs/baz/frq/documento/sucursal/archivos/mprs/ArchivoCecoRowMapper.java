/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.mprs;

import com.gs.baz.frq.documento.sucursal.archivos.dto.ArchivoCeco;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author b191312
 */
public class ArchivoCecoRowMapper implements RowMapper<ArchivoCeco> {

    private ArchivoCeco archivosCeco;

    @Override
    public ArchivoCeco mapRow(ResultSet rs, int rowNum) throws SQLException {

        archivosCeco = new ArchivoCeco();
        archivosCeco.setIdCeco(rs.getInt("FCID_CECO"));
        archivosCeco.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        archivosCeco.setIdFormato(rs.getInt("FIID_FORMATO"));
        archivosCeco.setNombreFormato(rs.getString("FORMATO"));
        archivosCeco.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        archivosCeco.setNombreProyecto(rs.getString("NOM_PROY"));
        archivosCeco.setIdArchivo(rs.getInt("FIID_ARCHIVO"));
        archivosCeco.setIdArchivoPadre(rs.getInt("FIID_ARCHPADRE"));
        archivosCeco.setDescripcionArchivo(rs.getString("ARCHIVO"));
        archivosCeco.setNombreArchivo(rs.getString("NOM_ARCH"));
        archivosCeco.setRuta(rs.getString("FCRUTA"));
        archivosCeco.setExtension(rs.getString("FCEXTENSION"));
        archivosCeco.setIdUsuario(rs.getInt("FIID_USUARIO"));
        archivosCeco.setIdFolio(rs.getInt("FIID_FOLIO"));

        //Nuevos Campos
        archivosCeco.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        archivosCeco.setNombreDocumento(rs.getString("FCNOMBRE"));
        archivosCeco.setGeneraVersion(rs.getInt("FIID_GENHISTO"));
        archivosCeco.setIdVersion(rs.getInt("FIID_VERSION"));
        archivosCeco.setFecha(rs.getString("FCPERIODO"));

        return archivosCeco;
    }

}
