/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.rutas.mprs;

import com.gs.baz.frq.modelo.negocio.rutas.dto.RutaNivelBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class RutaNivelRowMapper implements RowMapper<RutaNivelBase> {

    private RutaNivelBase rutaNivel;

    @Override
    public RutaNivelBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        rutaNivel = new RutaNivelBase();
        rutaNivel.setIdNivel(rs.getInt("FIIDMODENEGOC"));
        return rutaNivel;
    }
}
