package com.gs.baz.frq.modelo.negocio.unidad.negocio.init;

import com.gs.baz.frq.modelo.negocio.unidad.negocio.dao.UnidadNegocioDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.modelo.negocio.unidad.negocio")
public class ConfigModeloNegocioUnidadNegocio {

    private final Logger logger = LogManager.getLogger();

    public ConfigModeloNegocioUnidadNegocio() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public UnidadNegocioDAOImpl UnidadNegocioDAOImpl() {
        return new UnidadNegocioDAOImpl();
    }

}
