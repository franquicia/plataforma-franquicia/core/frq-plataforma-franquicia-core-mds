/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.semaforo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Datos del Semaforo", value = "Semaforo")
public class Semaforo extends SemaforoBase {

    @JsonProperty(value = "idSemaforo")
    @ApiModelProperty(notes = "Identificador del Semaforo", example = "1", position = -1)
    private Integer idSemaforo;

    public Integer getIdSemaforo() {
        return idSemaforo;
    }

    public void setIdSemaforo(Integer idSemaforo) {
        this.idSemaforo = idSemaforo;
    }

    @Override
    public String toString() {
        return "Semaforo{" + "idSemaforo=" + idSemaforo + '}';
    }

}
