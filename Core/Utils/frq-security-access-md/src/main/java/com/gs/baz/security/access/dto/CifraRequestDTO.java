/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Estructura de entrada para cifrar algun valor", value = "CifraRequest")
public class CifraRequestDTO {

    @JsonProperty(value = "accesoPublico")
    @ApiModelProperty(notes = "Llave asimétrica pública", example = "<Cadena con la llave pública>")
    private String accesoPublico;

    @JsonProperty(value = "valor")
    @ApiModelProperty(notes = "Valor en texto plano a ser cifrado", example = "dato sensible")
    private String valor;

    public String getAccesoPublico() {
        return accesoPublico;
    }

    public void setAccesoPublico(String accesoPublico) {
        this.accesoPublico = accesoPublico;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
