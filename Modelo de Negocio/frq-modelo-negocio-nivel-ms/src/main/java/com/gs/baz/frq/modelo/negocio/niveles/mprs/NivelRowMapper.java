/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.niveles.mprs;

import com.gs.baz.frq.modelo.negocio.niveles.dto.NivelBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class NivelRowMapper implements RowMapper<NivelBase> {

    private NivelBase nivelBase;

    @Override
    public NivelBase mapRow(ResultSet rs, int rowNum) throws SQLException {
        nivelBase = new NivelBase();
        nivelBase.setDescripcion(rs.getString("FCDESCRIPCION"));
        return nivelBase;
    }
}
