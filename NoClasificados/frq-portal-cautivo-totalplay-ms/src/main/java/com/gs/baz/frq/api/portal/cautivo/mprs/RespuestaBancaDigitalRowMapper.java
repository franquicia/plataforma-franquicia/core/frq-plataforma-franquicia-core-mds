/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.api.portal.cautivo.mprs;

import com.gs.baz.frq.api.portal.cautivo.dto.BancaDigitalDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author MADA
 */
public class RespuestaBancaDigitalRowMapper implements RowMapper<BancaDigitalDTO> {

    @Override
    public BancaDigitalDTO mapRow(ResultSet rs, int i) throws SQLException {

        BancaDigitalDTO gobiernoDatosDTO = new BancaDigitalDTO();

        gobiernoDatosDTO.setNombreUsuario(rs.getString("FCUSERNAME"));
        gobiernoDatosDTO.setInicioSesion(rs.getString("FDINICIO_SESION"));
        gobiernoDatosDTO.setSucursal(rs.getString("FCSUCURSAL"));
        gobiernoDatosDTO.setMac(rs.getString("FCMAC"));
        gobiernoDatosDTO.setIdPortal(rs.getInt("FIUSUARIO_ID"));;

        return gobiernoDatosDTO;
    }

}
