/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Folio", value = "Folio")
public class Folio extends FolioBase {

    @JsonProperty(value = "idFolio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificador del Folio", example = "1", position = -1)
    protected Integer idFolio;

    public Folio() {
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Folio builFolio(Folio folio) {
        this.creador = folio.creador;
        this.documento = folio.documento;
        this.estado = folio.estado;
        this.etiquetas = folio.etiquetas;
        this.fechaCreacion = folio.fechaCreacion;
        this.fechaModificacion = folio.fechaModificacion;
        this.firmas = folio.firmas;
        if (folio.idFolio != null) {
            this.idFolio = folio.idFolio;
        }
        this.indicePaso = folio.indicePaso;
        this.rutas = folio.rutas;
        return this;
    }

    @Override
    public String toString() {
        return "Folio{" + "idFolio=" + idFolio + '}';
    }

}
