/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.cecos.mtto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CecosMttoDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Integer inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "periodo")
    private String periodo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocio")
    private String negocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCecoSuperior")
    private Integer idCecoSuperior;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "idCeco")
    private Integer idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "observaciones")
    private String observaciones;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombreCeco")
    private String nombreCeco;

    public Integer getInserted() {
        return inserted;
    }

    public void setInserted(Integer inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public Integer getIdCecoSuperior() {
        return idCecoSuperior;
    }

    public void setIdCecoSuperior(Integer idCecoSuperior) {
        this.idCecoSuperior = idCecoSuperior;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    @Override
    public String toString() {
        return "GeografiaMttoDTO [inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + ", periodo="
                + periodo + ", negocio=" + negocio + ", idCecoSuperior=" + idCecoSuperior + ", idCeco=" + idCeco
                + ", observaciones=" + observaciones + ", nombreCeco=" + nombreCeco + "]";
    }

}
