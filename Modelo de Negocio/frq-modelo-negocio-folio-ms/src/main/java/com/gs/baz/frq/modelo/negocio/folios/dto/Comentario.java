/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class Comentario {

    @JsonProperty(value = "idComentario")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "Identificador del comentario", example = "1", position = -1)
    private Integer idComentario;

    @JsonProperty(value = "calificacion")
    @ApiModelProperty(notes = "Calificacion del documento", example = "1", required = true)
    private Integer calificacion;

    @JsonProperty(value = "comentario")
    @ApiModelProperty(notes = "Comentario del documento", example = "1", required = true)
    private String comentario;

    @JsonProperty(value = "visualizaciones")
    @ApiModelProperty(notes = "Visualizacion del documento por usuario", example = "1", required = true)
    private Integer visualizaciones;

    @JsonProperty(value = "fechaUltimaVista")
    @ApiModelProperty(notes = "Fecha de ultima vista", example = "01-03-2021", required = true)
    private String fechaUltimaVista;

    @JsonProperty(value = "fechaComentario")
    @ApiModelProperty(notes = "Fecha del comentario", example = "01-03-2021", required = true)
    private String fechaComentario;

    public Integer getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(Integer idComentario) {
        this.idComentario = idComentario;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(Integer visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public String getFechaUltimaVista() {
        return fechaUltimaVista;
    }

    public void setFechaUltimaVista(String fechaUltimaVista) {
        this.fechaUltimaVista = fechaUltimaVista;
    }

    public String getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(String fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    @Override
    public String toString() {
        return "Comentario{" + "calificacion=" + calificacion + ", comentario=" + comentario + ", visualizaciones=" + visualizaciones + ", fechaUltimaVista=" + fechaUltimaVista + ", fechaComentario=" + fechaComentario + '}';
    }

}
