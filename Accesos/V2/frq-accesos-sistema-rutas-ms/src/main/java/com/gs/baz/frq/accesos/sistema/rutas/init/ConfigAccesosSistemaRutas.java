package com.gs.baz.frq.accesos.sistema.rutas.init;

import com.gs.baz.frq.accesos.sistema.rutas.dao.AccesosSistemaRutasDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.frq.accesos.sistema.rutas")
public class ConfigAccesosSistemaRutas {

    private final Logger logger = LogManager.getLogger();

    public ConfigAccesosSistemaRutas() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public AccesosSistemaRutasDAOImpl accesosSistemaRutasDAOImpl() {
        return new AccesosSistemaRutasDAOImpl();
    }

}
