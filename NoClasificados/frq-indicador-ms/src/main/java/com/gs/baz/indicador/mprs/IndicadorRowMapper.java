package com.gs.baz.indicador.mprs;

import com.gs.baz.indicador.dto.IndicadorDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class IndicadorRowMapper implements RowMapper<IndicadorDTO> {

    private IndicadorDTO indicadorDTO;

    @Override
    public IndicadorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        indicadorDTO = new IndicadorDTO();
        indicadorDTO.setIdIndicador(((BigDecimal) rs.getObject("FIIDINDC")));
        indicadorDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        return indicadorDTO;
    }
}
