/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.propiedades.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author carlos
 */
public class ParametrosVistaPropiedad {

    @JsonProperty(value = "idVista")
    private transient Integer idVista;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre de la propiedad", example = "menu.hide")
    @NotEmpty
    private String nombre;

    @JsonProperty(value = "valor")
    @ApiModelProperty(notes = "Valor de la propiedad", example = "1")
    @NotEmpty
    private String valor;

    @JsonProperty(value = "tipo")
    @ApiModelProperty(notes = "Tipo de valor de la propiedad", example = "String")
    @NotEmpty
    private String tipo;

    public Integer getIdVista() {
        return idVista;
    }

    public void setIdVista(Integer idVista) {
        this.idVista = idVista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "ParametrosVistaPropiedad{" + "idVista=" + idVista + ", nombre=" + nombre + ", valor=" + valor + ", tipo=" + tipo + '}';
    }

}
