package com.gs.baz.estatus.usuario.dao;

import com.gs.baz.estatus.usuario.dto.EstatusUsuarioDTO;
import com.gs.baz.estatus.usuario.mprs.EstatusUsuarioRowMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class EstatusUsuarioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcUpdate;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAESTAT_US");
        jdbcInsert.withProcedureName("SPINSAESTAT_USUA");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAESTAT_US");
        jdbcDelete.withProcedureName("SPDELAESTAT_USUA");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAESTAT_US");
        jdbcUpdate.withProcedureName("SPACTAESTAT_USUA");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAESTAT_US");
        jdbcSelect.withProcedureName("SPGETAESTAT_USUA");
        jdbcSelect.returningResultSet("PA_CDATOS", new EstatusUsuarioRowMapper());
    }

    public EstatusUsuarioDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<EstatusUsuarioDTO> data = (List<EstatusUsuarioDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<EstatusUsuarioDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<EstatusUsuarioDTO> data = (List<EstatusUsuarioDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    public EstatusUsuarioDTO insertRow(EstatusUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", entityDTO.getIdEstatusUsuario());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdEstatusUsuario((BigDecimal) out.get("PA_NRESEJECUCION"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Estatus Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Estatus Usuario"), ex);
        }
    }

    public EstatusUsuarioDTO updateRow(EstatusUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", entityDTO.getIdEstatusUsuario());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getIdEstatus());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Estatus Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Estatus Usuario"), ex);
        }
    }

    public EstatusUsuarioDTO deleteRow(EstatusUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", entityDTO.getIdEstatusUsuario());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error  not success to delete row of Estatus Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Exception error  to delete row of Estatus Usuario"), ex);
        }
    }

}
