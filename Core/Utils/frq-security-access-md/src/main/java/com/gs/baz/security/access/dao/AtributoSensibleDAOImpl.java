/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.security.access.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.DataNotDeletedException;
import com.gs.baz.frq.model.commons.DataNotInsertedException;
import com.gs.baz.frq.model.commons.DataNotUpdatedException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.security.access.dto.AtributoSensible;
import com.gs.baz.security.access.dto.AtributoSensibleAgrupado;
import com.gs.baz.security.access.dto.AtributoSensibleBase;
import com.gs.baz.security.access.dto.AtributoSensibleFiltroDTO;
import com.gs.baz.security.access.dto.AtributoSensiblePostResponse;
import com.gs.baz.security.access.mprs.AtributosSensiblesAgrupadoRowMapper;
import com.gs.baz.security.access.mprs.AtributosSensiblesRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author kramireza
 */
public class AtributoSensibleDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelect2;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINCMPSENSIBLES");
        jdbcInsert.withProcedureName("SP_INS_CMPSENSIBLES");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINCMPSENSIBLES");
        jdbcUpdate.withProcedureName("SP_ACT_CMPSENSIBLES");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINCMPSENSIBLES");
        jdbcDelete.withProcedureName("SP_DEL_CMPSENSIBLES");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINCMPSENSIBLES");
        jdbcSelect.withProcedureName("SP_SEL_CMPSENSIBLES");
        jdbcSelect.returningResultSet("RCL_INFO", new AtributosSensiblesAgrupadoRowMapper());

        jdbcSelect2 = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect2.withSchemaName(schema);
        jdbcSelect2.withCatalogName("PAADMINCMPSENSIBLES");
        jdbcSelect2.withProcedureName("SP_SEL_CMPSENSIBLES");
        jdbcSelect2.returningResultSet("RCL_INFO", new AtributosSensiblesRowMapper());
    }

    public AtributoSensible selectRow(Long idCampoSensible) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCAMPO", idCampoSensible);
        mapSqlParameterSource.addValue("PA_FCORIGENDATO", null);
        mapSqlParameterSource.addValue("PA_FCSCOPEDATO", null);
        mapSqlParameterSource.addValue("PA_FCNOMBRRECURSO", null);
        Map<String, Object> out = jdbcSelect2.execute(mapSqlParameterSource);
        List<AtributoSensible> data = (List<AtributoSensible>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<AtributoSensible> selectRows(AtributoSensibleFiltroDTO camposSensiblesRequestDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCAMPO", null);
        mapSqlParameterSource.addValue("PA_FCORIGENDATO", camposSensiblesRequestDTO.getTipo());
        mapSqlParameterSource.addValue("PA_FCSCOPEDATO", camposSensiblesRequestDTO.getNombreApi());
        mapSqlParameterSource.addValue("PA_FCNOMBRRECURSO", camposSensiblesRequestDTO.getNombreRecurso());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<AtributoSensible>) out.get("RCL_INFO");
    }

    public List<AtributoSensibleAgrupado> selectList(AtributoSensibleFiltroDTO camposSensiblesRequestDTO) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCAMPO", null);
        mapSqlParameterSource.addValue("PA_FCORIGENDATO", camposSensiblesRequestDTO.getTipo());
        mapSqlParameterSource.addValue("PA_FCSCOPEDATO", camposSensiblesRequestDTO.getNombreApi());
        mapSqlParameterSource.addValue("PA_FCNOMBRRECURSO", camposSensiblesRequestDTO.getNombreRecurso());
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<AtributoSensibleAgrupado>) out.get("RCL_INFO");
    }

    public List<AtributoSensible> selectAllRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDCAMPO", null);
        mapSqlParameterSource.addValue("PA_FCORIGENDATO", null);
        mapSqlParameterSource.addValue("PA_FCSCOPEDATO", null);
        mapSqlParameterSource.addValue("PA_FCSCOPEDATO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<AtributoSensible>) out.get("RCL_INFO");
    }

    public AtributoSensiblePostResponse insertRow(AtributoSensibleBase entityDTO) throws DataNotInsertedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCNOMBRECAMPO", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCORIGENDATO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_FCSCOPEDATO", entityDTO.getApiOrigen());
            mapSqlParameterSource.addValue("PA_FCNOMBRRECURSO", entityDTO.getNombreRecurso());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotInsertedException();
            } else if (success == 2) {
                throw new DataNotInsertedException(DataNotInsertedException.Cause.VIOLATION_KEY);
            } else {
                BigDecimal dato = (BigDecimal) out.get("PA_FIIDCAMPO");
                return new AtributoSensiblePostResponse(dato.intValue());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void updateRow(AtributoSensible entityDTO) throws DataNotUpdatedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCAMPO", entityDTO.getIdAtributo());
            mapSqlParameterSource.addValue("PA_FCNOMBRECAMPO", entityDTO.getNombre());
            mapSqlParameterSource.addValue("PA_FCORIGENDATO", entityDTO.getTipo());
            mapSqlParameterSource.addValue("PA_FCSCOPEDATO", entityDTO.getApiOrigen());
            mapSqlParameterSource.addValue("PA_FCNOMBRRECURSO", entityDTO.getNombreRecurso());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotUpdatedException();
            } else if (success == 2) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.VIOLATION_KEY);
            } else if (success == 3) {
                throw new DataNotUpdatedException(DataNotUpdatedException.Cause.ROW_NOT_FOUND);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void deleteRow(Integer entityDTO) throws DataNotDeletedException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDCAMPO", entityDTO);
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_EJECUCION")).intValue();
            if (success == 0) {
                throw new DataNotDeletedException();
            } else if (success == 3) {
                throw new DataNotDeletedException(DataNotDeletedException.Cause.ROW_NOT_FOUND);
            }
        } catch (DataNotDeletedException ex) {
            throw new DataNotDeletedException(ex);
        }
    }

}
