/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.cecos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author b191312
 */
@ApiModel(description = "Informacion de la ficha tecnica del punto de venta", value = "Ficha")
public class Ficha {

    @JsonProperty(value = "idFicha")
    @ApiModelProperty(notes = "identificador de la region", example = "236399")
    private Integer id;

    @JsonProperty(value = "idNegocio")
    @ApiModelProperty(notes = "identificador de la region", example = "236399")
    private Integer idNegocio;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "identificador de la region", example = "236399")
    private Integer idFormato;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "identificador de la region", example = "236399")
    private Integer idFolio;

    @JsonProperty(value = "latitud")
    @ApiModelProperty(notes = "Latitud del punto de venta", example = "17.0979", position = -3)
    private String latitud;

    @JsonProperty(value = "longitud")
    @ApiModelProperty(notes = "Longitud del punto de venta", example = "-99.000", position = -2)
    private String longitud;

    @JsonProperty(value = "direccion")
    @ApiModelProperty(notes = "Obtiene informacion adicional con datos de la fuhca tecnica", example = "Objeto tipo Ficha", position = -1)
    private String direccion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
