/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.distribucion.dashboard.mprs;

import com.gs.baz.frq.distribucion.dashboard.dto.EstatusVisitaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author jfernandor
 */
public class EstatusVisitasRowMapper implements RowMapper<EstatusVisitaDTO> {

    @Override
    public EstatusVisitaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        EstatusVisitaDTO datos = new EstatusVisitaDTO();

        datos.setCentroCostos(rs.getString("FCID_CECO"));
        datos.setCentroCostosBase(rs.getString("FICECOBASE_ID"));
        datos.setFecha(rs.getString("FDFECHA"));
        datos.setNumeroEmpleado(rs.getString("FIID_USUARIO"));
        datos.setIdInfo(rs.getInt("FIID_INFO"));
        datos.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
        datos.setImperdonable(rs.getString("Fcimperd_Frtadatinfo"));
        datos.setNumeroImperdonables(rs.getInt("FICONTEO_IMP_FRTARESUMVISITA"));
        datos.setDefectos(rs.getInt("FIDEFECTOS_FRTARESUMVISITA"));
        datos.setRespuestas(rs.getString("FCTOTRESP"));
        datos.setTotalRespuestas(rs.getInt("TOTALRESP"));
        datos.setCalificacion(rs.getInt("FICALIF_FRTADATINFO"));
        datos.setCalificacionFinal(rs.getInt("FICALIF_FINAL_FRTARESUMVISITA"));
        datos.setEstatusCalificacion(rs.getString("ESTATUS_CALIFICACION"));
        datos.setEstatusRespuestas(rs.getString("ESTATUS_RESPUESTAS"));
        return datos;
    }
}
