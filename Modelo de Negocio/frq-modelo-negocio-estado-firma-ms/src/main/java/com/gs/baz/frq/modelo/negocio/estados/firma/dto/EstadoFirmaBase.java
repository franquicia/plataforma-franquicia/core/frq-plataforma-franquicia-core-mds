/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.estados.firma.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos base del Estado de la firma", value = "EstadoFirmaBase")
public class EstadoFirmaBase {

    @JsonProperty(value = "descripcion")
    @ApiModelProperty(notes = "Descripcion del estado de la firma", example = "Rechaza el paso del documento y reinicia todas las firmas previamente autorizadas")
    private String descripcion;

    @JsonProperty(value = "nombre")
    @ApiModelProperty(notes = "Nombre del estatus de la firma", example = "Rechazar")
    private String nombre;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "EstadoFirmaBase{" + "descripcion=" + descripcion + ", nombre=" + nombre + '}';
    }

}
