/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.rangos.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.frq.rangos.dao.RangosDAOImpl;
import com.gs.baz.frq.rangos.dto.RangosDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/franquicia/rango")
@Component("FRQServiceRangos")
public class ServiceRangos {

    @Autowired
    private RangosDAOImpl rangosDAOImpl;

    final VersionBI versionBI = new VersionBI();

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RangosDTO postRango(@RequestBody RangosDTO rangos) throws CustomException {
        if (rangos.getIdRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_rango"));
        }
        if (rangos.getNumeroRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_rango"));
        }
        if (rangos.getLimiteInferior() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("limite_inferior"));
        }
        if (rangos.getLimiteSuperior() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("limite_superior"));
        }
        if (rangos.getValorRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("valor_rango"));
        }
        if (rangos.getDetalle() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("detalle"));
        }
        if (rangos.getRegistroActivo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("registro_activo"));
        }
        return rangosDAOImpl.insertRow(rangos);
    }

    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public RangosDTO putRango(@RequestBody RangosDTO rangos) throws CustomException {
        if (rangos.getIdRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_rango"));
        }
        if (rangos.getNumeroRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_rango"));
        }
        if (rangos.getLimiteInferior() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("limite_inferior"));
        }
        if (rangos.getLimiteSuperior() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("limite_superior"));
        }
        if (rangos.getValorRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("valor_rango"));
        }
        if (rangos.getDetalle() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("detalle"));
        }
        if (rangos.getRegistroActivo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("registro_activo"));
        }
        return rangosDAOImpl.updateRow(rangos);
    }

    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RangosDTO> getRangos() throws CustomException {
        return rangosDAOImpl.selectRows(null, null);
    }

    @RequestMapping(value = "secure/get/{id_rango}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RangosDTO> getRangos(@PathVariable("id_rango") Long idRango) throws CustomException {
        return rangosDAOImpl.selectRows(idRango, null);
    }

    @RequestMapping(value = "secure/get/numero/{id_numero}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RangosDTO> getRangosTipo(@PathVariable("id_numero") Long idNumero) throws CustomException {
        return rangosDAOImpl.selectRows(null, idNumero);
    }

    @RequestMapping(value = "secure/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RangosDTO deleteRango(@RequestBody RangosDTO rangos) throws CustomException {
        if (rangos.getIdRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_rango"));
        }
        if (rangos.getNumeroRango() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("numero_rango"));
        }
        return rangosDAOImpl.deleteRow(rangos);
    }
}
