package com.gs.baz.status.dao;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.status.dao.util.GenericDAO;
import com.gs.baz.status.dto.StatusDTO;
import com.gs.baz.status.mprs.StatusRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class StatusDAOImpl extends DefaultDAO implements GenericDAO<StatusDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINESTATUSCA");
        jdbcInsert.withProcedureName("SP_INS_ESTATUS");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINESTATUSCA");
        jdbcUpdate.withProcedureName("SP_ACT_ESTATUS");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINESTATUSCA");
        jdbcSelect.withProcedureName("SP_SEL_ESTATUS");
        jdbcSelect.returningResultSet("RCL_INFO", new StatusRowMapper());
    }

    @Override
    public StatusDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("FIIDESTATUS", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<StatusDTO> data = (List<StatusDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public StatusDTO selectRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StatusDTO> selectRows(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StatusDTO> selectRows() throws CustomException {
        List<StatusDTO> lista;
        Map<String, Object> out = jdbcSelect.execute();
        lista = (List<StatusDTO>) out.get("RCL_INFO");
        return lista;
    }

    @Override
    public StatusDTO insertRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StatusDTO insertRow(StatusDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdStatus((BigDecimal) out.get("PA_FIIDESTATUS"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Status Catalogo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Status Catalogo"), ex);
        }
    }

    @Override
    public StatusDTO updateRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StatusDTO updateRow(StatusDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDESTATUS", entityDTO.getIdStatus());
            mapSqlParameterSource.addValue("PA_FCDESCRIPCION", entityDTO.getDescripcion());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Status Catalogo"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Status Catalogo"), ex);
        }
    }

    @Override
    public StatusDTO deleteRow(ObjectNode jsonParams) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StatusDTO deleteRow(StatusDTO entityDTO) throws CustomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
