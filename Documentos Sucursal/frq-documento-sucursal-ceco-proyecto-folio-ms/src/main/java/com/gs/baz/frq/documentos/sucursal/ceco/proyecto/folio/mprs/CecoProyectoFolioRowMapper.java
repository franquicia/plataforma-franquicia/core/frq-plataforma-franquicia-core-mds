package com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.mprs;

import com.gs.baz.frq.documentos.sucursal.ceco.proyecto.folio.dto.CecoProyectoFolio;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoProyectoFolioRowMapper implements RowMapper<CecoProyectoFolio> {

    @Override
    public CecoProyectoFolio mapRow(ResultSet rs, int rowNum) throws SQLException {

        CecoProyectoFolio cecoProyectoFolio = new CecoProyectoFolio();

        cecoProyectoFolio.setIdCeco(rs.getString("FCID_CECO"));
        cecoProyectoFolio.setIdProyecto(rs.getInt("FIID_PROYECTO"));
        cecoProyectoFolio.setIdFolio(rs.getInt("FIID_FOLIO"));
        cecoProyectoFolio.setNombreProyecto(rs.getString("NOM_PROY"));

        return cecoProyectoFolio;
    }

}
