/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.resource.server.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 *
 * @author cescobarh
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class NameApiFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        try {
            if (applicationContext != null) {
                RequestMappingHandlerMapping requestMappingHandlerMapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
                if (requestMappingHandlerMapping != null) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    HandlerExecutionChain handlerExecutionChain = requestMappingHandlerMapping.getHandler(httpServletRequest);
                    Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
                    List<ObjectNode> endpoints = new ArrayList<>();
                    ObjectNode endpoint;
                    for (Map.Entry<RequestMappingInfo, HandlerMethod> entrySet : handlerMethods.entrySet()) {
                        HandlerMethod handlerMethod = entrySet.getValue();
                        endpoint = objectMapper.createObjectNode();
                        String apiName = null;
                        String[] basesPath = {};
                        Api api = handlerMethod.getMethod().getDeclaringClass().getAnnotation(Api.class);
                        if (api != null) {
                            apiName = api.value();
                        }
                        RequestMapping controller = handlerMethod.getMethod().getDeclaringClass().getAnnotation(RequestMapping.class);
                        if (controller != null) {
                            basesPath = controller.value();
                        }
                        RequestMapping resourcesName = handlerMethod.getMethodAnnotation(RequestMapping.class);
                        if (resourcesName != null) {
                            for (String basePath : basesPath) {
                                for (String resourceName : resourcesName.value()) {
                                    endpoint.put("apiName", apiName);
                                    endpoint.put("basePath", basePath);
                                    endpoint.put("resourceName", resourceName);
                                    endpoints.add(endpoint);
                                }
                            }
                        }
                    }
                    httpServletRequest.setAttribute("endpointsDefinition", endpoints);
                    if (handlerExecutionChain != null) {
                        HandlerMethod handlerMethod = (HandlerMethod) handlerExecutionChain.getHandler();
                        if (handlerMethod != null) {
                            Api api = handlerMethod.getMethod().getDeclaringClass().getAnnotation(Api.class);
                            if (api != null) {
                                httpServletRequest.setAttribute("nameApi", "sistemas-franquicia-" + api.value());
                            } else {
                                httpServletRequest.setAttribute("nameApi", "sistemas-franquicia");
                            }
                            RequestMapping controller = handlerMethod.getMethod().getDeclaringClass().getAnnotation(RequestMapping.class);
                            if (controller != null) {
                                httpServletRequest.setAttribute("tagsBasePathTemplate", controller.value());
                            }
                            RequestMapping resourceName = handlerMethod.getMethodAnnotation(RequestMapping.class);
                            if (resourceName != null) {
                                httpServletRequest.setAttribute("tagsResourceNameTemplate", resourceName.value());
                            }
                        } else {
                            httpServletRequest.setAttribute("nameApi", "sistemas-franquicia");
                        }
                    } else {
                        httpServletRequest.setAttribute("nameApi", "sistemas-franquicia");
                    }
                } else {
                    httpServletRequest.setAttribute("nameApi", "sistemas-franquicia");
                }
            } else {
                httpServletRequest.setAttribute("nameApi", "sistemas-franquicia");
            }
        } catch (Exception ex) {
            httpServletRequest.setAttribute("nameApi", "sistemas-franquicia-api-name");
        }
        chain.doFilter(httpServletRequest, httpServletResponse);
    }

}
