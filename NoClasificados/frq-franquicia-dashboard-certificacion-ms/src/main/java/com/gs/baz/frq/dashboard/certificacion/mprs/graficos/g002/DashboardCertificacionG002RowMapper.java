package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g002;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g002.DashboardCertificacionG002DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG002RowMapper implements RowMapper<DashboardCertificacionG002DTO> {

    private DashboardCertificacionG002DTO dashboardCertificacionG002DTO;

    @Override
    public DashboardCertificacionG002DTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        dashboardCertificacionG002DTO = new DashboardCertificacionG002DTO();
        dashboardCertificacionG002DTO.setSemana(rs.getString("FISEMANA"));
        dashboardCertificacionG002DTO.setTotalCertificaciones((BigDecimal) rs.getObject("TOTAL_CERTIFICACIONES"));
        //dashboardCertificacionG002DTO.setRango((BigDecimal) rs.getObject("RANGO"));
        //dashboardCertificacionG002DTO.setRangoColor(rs.getString("RANGOCOLOR"));

        return dashboardCertificacionG002DTO;
    }
}
