package com.gs.baz.frq.dashboard.mprs.filtro;

import com.gs.baz.frq.dashboard.dto.filtros.FiltroAnioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FiltroAnioRowMapper implements RowMapper<FiltroAnioDTO> {

    private FiltroAnioDTO anioDTO;

    @Override
    public FiltroAnioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        anioDTO = new FiltroAnioDTO();
        anioDTO.setAnio((BigDecimal) rs.getObject("ANIO"));
        return anioDTO;
    }
}
