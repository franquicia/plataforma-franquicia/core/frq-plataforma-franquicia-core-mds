/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.usuarios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Lista de correo usuarios", value = "usuariosCorreo")
public class UsuariosCorreo {

    @JsonProperty(value = "usuariosCorreo")
    @ApiModelProperty(notes = "usuariosCorreo")
    private List<UsuarioCorreo> usuarios;

    public UsuariosCorreo(List<UsuarioCorreo> usuarios) {
        this.usuarios = usuarios;
    }

    public List<UsuarioCorreo> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioCorreo> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public String toString() {
        return "Usuarios{" + "usuarios=" + usuarios + '}';
    }

}
