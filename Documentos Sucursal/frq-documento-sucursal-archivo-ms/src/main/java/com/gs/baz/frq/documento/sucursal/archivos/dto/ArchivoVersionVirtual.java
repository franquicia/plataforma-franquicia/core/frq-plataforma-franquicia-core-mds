/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documento.sucursal.archivos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
@ApiModel(description = "Datos del Archivo", value = "Archivo")
public class ArchivoVersionVirtual extends ArchivoVirtualBase {

    @JsonProperty(value = "idArchivo")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private Integer idArchivo;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private Integer idProyecto;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private String nombreProyecto;

    @JsonProperty(value = "idDocumento")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private Integer idDocumento;

    @JsonProperty(value = "nombreDocumento")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private String nombreDocumento;

    @JsonProperty(value = "generaVersion")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private Integer generaVersion;

    @JsonProperty(value = "fecha")
    @ApiModelProperty(notes = "Identificador del Archivo", example = "1", position = -1)
    private String fecha;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public Integer getGeneraVersion() {
        return generaVersion;
    }

    public void setGeneraVersion(Integer generaVersion) {
        this.generaVersion = generaVersion;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    @Override
    public String toString() {
        return "Archivo{" + "idArchivo=" + idArchivo + '}';
    }

}
