/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.documentos.sucursal.tipos.informacion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosTipoInformacion {

    @JsonProperty(value = "idTipoInformacion")
    private transient Integer idTipoInformacion;

    @JsonProperty(value = "descripcion", required = true)
    @ApiModelProperty(notes = "Descripcion nombre del tipoInformacion", example = "AREAS", required = true)
    private String descripcion;

    public Integer getIdTipoInformacion() {
        return idTipoInformacion;
    }

    public void setIdTipoInformacion(Integer idTipoInformacion) {
        this.idTipoInformacion = idTipoInformacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
