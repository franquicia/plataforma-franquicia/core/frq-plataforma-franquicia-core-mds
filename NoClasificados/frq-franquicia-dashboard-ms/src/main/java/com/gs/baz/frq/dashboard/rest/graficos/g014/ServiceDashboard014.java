/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.dashboard.rest.graficos.g014;

import com.gs.baz.frq.dashboard.dao.graficos.g014.DashboardDAOImpl014;
import com.gs.baz.frq.dashboard.dto.graficos.g014.CalificacionDisciplinaArbolDTO;
import com.gs.baz.frq.model.commons.CustomException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/franquicia/dashboard")
public class ServiceDashboard014 {

    @Autowired
    private DashboardDAOImpl014 dashboardDAOImpl014;

    @RequestMapping(value = "secure/grafico/get/calificacion/disciplina/arbol", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CalificacionDisciplinaArbolDTO> getCalificacionDisciplinaArbol(@RequestBody CalificacionDisciplinaArbolDTO CalificacionDisciplinaArbol) throws CustomException {
        return dashboardDAOImpl014.selectRowsCalificacionDisciplinaArbol(CalificacionDisciplinaArbol);
    }
}
