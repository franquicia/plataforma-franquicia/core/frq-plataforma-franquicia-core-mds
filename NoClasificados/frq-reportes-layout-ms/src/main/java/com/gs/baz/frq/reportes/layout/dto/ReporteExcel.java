/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.reportes.layout.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ReporteExcel {

    @JsonProperty(value = "idNegocio")
    private transient String idNegocio;

    @JsonProperty(value = "idFolio")
    @ApiModelProperty(notes = "Identificador del folio", example = "1")
    private Integer idFolio;

    @JsonProperty(value = "idCeco")
    @ApiModelProperty(notes = "Identificador del idCeco", example = "920100")
    private Integer idCeco;

    @JsonProperty(value = "idProyecto")
    @ApiModelProperty(notes = "Identificador del proyecto", example = "1")
    private Integer idProyecto;

    @JsonProperty(value = "idFormato")
    @ApiModelProperty(notes = "Identificador del Formato", example = "1")
    private Integer idFormato;

    @JsonProperty(value = "Fecha")
    @ApiModelProperty(notes = "Fecha del proyecto", example = "23/08/2021")
    private String fecha;

    @JsonProperty(value = "nombreFormato")
    @ApiModelProperty(notes = "Nombre del Formato", example = "Operativa")
    private String nombreFormato;

    @JsonProperty(value = "nombreNegocio")
    @ApiModelProperty(notes = "Nombre del Negocio", example = "EKT")
    private String nombreNegocio;

    @JsonProperty(value = "nombreProyecto")
    @ApiModelProperty(notes = "Nombre del nombreProyecto", example = "Reconstrucciones")
    private String nombreProyecto;

    @JsonProperty(value = "nombreCeco")
    @ApiModelProperty(notes = "Nombre del nombreCeco", example = "MEGA LA LUNA")
    private String nombreCeco;

    @JsonProperty(value = "conteoProyecto")
    @ApiModelProperty(notes = "Conteo de proyectos por ceco", example = "1")
    private Integer conteoProyecto;

    @JsonProperty(value = "fechaInicio")
    @ApiModelProperty(notes = " fecha Inicio reporte", example = "23/08/21")
    private String fechaInicio;

    @JsonProperty(value = "fechaFin")
    @ApiModelProperty(notes = " fecha fin reporte", example = "23/08/21")
    private String fechaFin;

    @JsonProperty(value = "idZona")
    @ApiModelProperty(notes = " Itentificador de la zona", example = "1")
    private Integer idZona;

    @JsonProperty(value = "valor")
    @ApiModelProperty(notes = " Indica el valor de la zona", example = "1")
    private String valor;

    @JsonProperty(value = "observaciones")
    @ApiModelProperty(notes = " Indica el observaciones de la zona", example = "1")
    private String observaciones;

    @JsonProperty(value = "idVersion")
    @ApiModelProperty(notes = " Indica la version  de la zona", example = "1")
    private Integer idVersion;

    @JsonProperty(value = "nombreZona")
    @ApiModelProperty(notes = " Indica el nombre  de la zona", example = "Patio Bancario")
    private String nombreZona;

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Integer idVersion) {
        this.idVersion = idVersion;
    }

    public String getNombreZona() {
        return nombreZona;
    }

    public void setNombreZona(String nombreZona) {
        this.nombreZona = nombreZona;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(Integer idCeco) {
        this.idCeco = idCeco;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public Integer getConteoProyecto() {
        return conteoProyecto;
    }

    public void setConteoProyecto(Integer conteoProyecto) {
        this.conteoProyecto = conteoProyecto;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

}
