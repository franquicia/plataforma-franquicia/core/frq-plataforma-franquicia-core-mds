/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.accesos.sistema.vistas.mprs;

import com.gs.baz.frq.accesos.sistema.rutas.dto.Ruta;
import com.gs.baz.frq.accesos.sistema.vistas.dto.Vista;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author carlos
 */
public class VistasFinalesRowMapper implements RowMapper<Vista> {

    @Override
    public Vista mapRow(ResultSet rs, int rowNum) throws SQLException {
        Vista vista = new Vista();
        Ruta ruta = new Ruta();
        vista.setIdVista(rs.getInt("FIIDVISTA"));
        vista.setIdVistaPadre(rs.getInt("FIIDPADRE"));
        vista.setDescripcion(rs.getString("FCDESCRIPCION"));
        vista.setNombre(rs.getString("FCNOMBRE"));
        ruta.setIdRuta(rs.getInt("FIIDRUTA"));
        ruta.setNombre(rs.getString("FCNOMRUTA"));
        ruta.setRutaAbsoluta(rs.getString("FCRUTA"));
        vista.setRuta(ruta);
        vista.setImagen(rs.getString("FCIMAGEN"));
        return vista;
    }
}
