package com.gs.baz.frq.excel.mprs;

import com.gs.baz.frq.excel.dto.Visita7sDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class Visita7sRowMapper implements RowMapper<Visita7sDTO> {

    private Visita7sDTO status;

    @Override
    public Visita7sDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new Visita7sDTO();
        status.setIdVisita((BigDecimal) rs.getObject("FIVISITA_ID"));
        status.setFecha(rs.getString("FDFECHA"));
        status.setNombreQuienRealizo(rs.getString("NOM_REALIZO"));
        status.setIdUsuario((BigDecimal) rs.getObject("FIID_USUARIO"));
        status.setDescripcionPuesto(rs.getString("DESC_PUESTO"));
        status.setNombrePais(rs.getString("NOM_PAIS"));
        status.setTerritorio(rs.getString("FCTERRITORIO"));
        status.setZona(rs.getString("FCZONA"));
        status.setRegion(rs.getString("FCREGION"));
        status.setIdCeco(rs.getString("FCID_CECO"));
        status.setNombreCeco(rs.getString("NOM_CECO"));
        status.setDescripcionCanal(rs.getString("DESC_CANAL"));
        status.setNegocioDisciplina(rs.getString("FCNEGOCIODISC"));
        status.setPrecalificacion((BigDecimal) rs.getObject("FIPRECALIFICA"));
        status.setCalificacionFinal((BigDecimal) rs.getObject("FICALIF_FINAL"));
        status.setConteoSi((BigDecimal) rs.getObject("FICONTEO_SI"));
        status.setConteoNo((BigDecimal) rs.getObject("FICONTEO_NO"));
        status.setConteoImperdonable((BigDecimal) rs.getObject("FICONTEO_IMP"));
        status.setCalif_esperada((BigDecimal) rs.getObject("FIPUNTOSESP"));
        status.setCalif_lineal((BigDecimal) rs.getObject("FICALIF_LINEAL"));
        status.setId_negocio((BigDecimal) rs.getObject("FINEGOCIODISCID"));

        return status;
    }
}
