/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.ddl.oracle.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class Parameter {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "oracle_type")
    private OracleDataType oracleDataType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "name")
    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "value")
    private Object value;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "type")
    private ParamType paramType;

    public Parameter() {
    }

    public Parameter(OracleDataType oracleDataType, String name, Object value, ParamType type) {
        this.oracleDataType = oracleDataType;
        this.name = name;
        this.value = value;
        this.paramType = type;
    }

    public Parameter(OracleDataType oracleDataType, String name, ParamType type) {
        this.oracleDataType = oracleDataType;
        this.name = name;
        this.paramType = type;
    }

    public OracleDataType getOracleDataType() {
        return oracleDataType;
    }

    public void setOracleDataType(OracleDataType oracleDataType) {
        this.oracleDataType = oracleDataType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ParamType getParamType() {
        return paramType;
    }

    public void setParamType(ParamType paramType) {
        this.paramType = paramType;
    }

    @Override
    public String toString() {
        return "Parameter{" + "oracleDataType=" + oracleDataType + ", name=" + name + ", value=" + value + ", paramType=" + paramType + '}';
    }

}
