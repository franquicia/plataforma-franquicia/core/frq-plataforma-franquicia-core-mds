/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.modelo.negocio.folios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author carlos
 */
public class ParametrosEnviaCorreoFolio {

    @JsonProperty(value = "idFolio")
    private transient Integer idFolio;

    @JsonProperty(value = "idEstadoFolio")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1", required = true)
    private Integer idEstadoFolio;

    @JsonProperty(value = "numeroEmpleado")
    @ApiModelProperty(notes = "Identificador del estado del folio", example = "1", required = true)
    private Integer numeroEmpleado;

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public Integer getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(Integer idFolio) {
        this.idFolio = idFolio;
    }

    public Integer getIdEstadoFolio() {
        return idEstadoFolio;
    }

    public void setIdEstadoFolio(Integer idEstadoFolio) {
        this.idEstadoFolio = idEstadoFolio;
    }

}
