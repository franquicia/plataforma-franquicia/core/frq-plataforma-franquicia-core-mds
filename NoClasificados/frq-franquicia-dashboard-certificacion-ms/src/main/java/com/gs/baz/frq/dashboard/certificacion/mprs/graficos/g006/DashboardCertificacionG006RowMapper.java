package com.gs.baz.frq.dashboard.certificacion.mprs.graficos.g006;

import com.gs.baz.frq.dashboard.certificacion.dto.graficos.g006.DashboardCertificacionG006DTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class DashboardCertificacionG006RowMapper implements RowMapper<DashboardCertificacionG006DTO> {

    private DashboardCertificacionG006DTO dashboardCertificacionG006DTO;

    @Override
    public DashboardCertificacionG006DTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        dashboardCertificacionG006DTO = new DashboardCertificacionG006DTO();
        dashboardCertificacionG006DTO.setIdCeco(rs.getString("FCID_CECO"));
        dashboardCertificacionG006DTO.setPuntoContacto(rs.getString("PUNTO_CONTACTO"));
        dashboardCertificacionG006DTO.setCertificador((BigDecimal) rs.getObject("CERTIFICADOR"));
        dashboardCertificacionG006DTO.setNombreCertificador(rs.getString("NOMBRE_CERTIFICADOR"));
        dashboardCertificacionG006DTO.setUltimaCertificacion(rs.getString("ULTIMA_FECHA"));
        dashboardCertificacionG006DTO.setIdCecoBase(rs.getString("FICECOBASE_ID"));
        dashboardCertificacionG006DTO.setIdGerente((BigDecimal) rs.getObject("GERENTE"));

        return dashboardCertificacionG006DTO;
    }
}
