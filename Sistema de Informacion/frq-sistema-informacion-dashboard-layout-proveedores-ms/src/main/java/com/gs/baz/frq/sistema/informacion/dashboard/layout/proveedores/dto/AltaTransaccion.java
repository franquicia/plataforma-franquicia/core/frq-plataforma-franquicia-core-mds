/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.sistema.informacion.dashboard.layout.proveedores.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@ApiModel(description = "Alta de transacción", value = "AltaTransaccion")
public class AltaTransaccion {


    @JsonProperty(value = "idTransaccion")
    @ApiModelProperty(notes = "Identificador de la transacción", example = "1", position = -2)
    private Integer idTransaccion;

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    @Override
    public String toString() {
        return "AltaTransaccion{" + "idTransaccion=" + idTransaccion + '}';
    }

}
