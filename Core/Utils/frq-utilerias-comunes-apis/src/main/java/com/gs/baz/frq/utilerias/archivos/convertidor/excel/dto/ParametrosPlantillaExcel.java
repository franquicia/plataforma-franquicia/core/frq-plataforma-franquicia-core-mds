/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.utilerias.archivos.convertidor.excel.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author cescobarh
 */
public class ParametrosPlantillaExcel {

    @JsonProperty(value = "nombre")
    @NotBlank
    private String nombre;

    @JsonProperty(value = "hojas")
    @NotEmpty
    @Valid
    private List<Hoja> hojas;

    public ParametrosPlantillaExcel() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Hoja> getHojas() {
        return hojas;
    }

    public void setHojas(List<Hoja> hojas) {
        this.hojas = hojas;
    }

    public void addHoja(Hoja hoja) {
        if (hojas == null) {
            hojas = new ArrayList<>();
        }
        hojas.add(hoja);
    }

    @Override
    public String toString() {
        return "ParametrosPlantillaExcel{" + "nombre=" + nombre + ", hojas=" + hojas + '}';
    }

}
