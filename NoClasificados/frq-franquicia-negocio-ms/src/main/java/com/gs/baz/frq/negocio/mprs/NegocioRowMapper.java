package com.gs.baz.frq.negocio.mprs;

import com.gs.baz.frq.negocio.dto.NegocioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class NegocioRowMapper implements RowMapper<NegocioDTO> {

    private NegocioDTO status;

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        status = new NegocioDTO();
        status.setIdNegocioFranquicia(((BigDecimal) rs.getObject("FINEGOCIODISCID")));
        status.setNegocioDisciplinaFranquicia(rs.getString("FCNEGOCIODISC"));
        return status;
    }
}
